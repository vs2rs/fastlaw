<?php

/**
 * Static class for geting some of the content templates
 * @author vs2rs
*/

class FastlawTemplates {

	/**
	 * Get the home page profesionals
	 * @return html
	*/
	public static function subscribe() {

		// get the pages
		$pages = FrontendContent::get_pages();
		$lang = Config::read('lang_current');

		// check for subscribe
		if(isset($pages->$lang->children->subscribe)) {

			// set the page
			$page = $pages->$lang->children->subscribe;

			// setup images
			$page->img_desktop = get_image($page->img_desktop);
			$page->img_mob = get_image($page->img_mob);

			// echo our template
			return get_html(array(

				// images
				'img_desktop'		=> $page->img_desktop ? $page->img_desktop->src : false,
				'img_desktop_alt'	=> $page->img_desktop ? $page->img_desktop->alt : '',
				'img_mob'			=> $page->img_mob ? $page->img_mob->src : false,
				'img_mob_alt'		=> $page->img_mob ? $page->img_mob->alt : '',

				// title of the block
				'title'		=> line_breaks($page->name),

				// template
				'template' 	=> APP_VIEWS . '/_global/subscribe.html'

			));
			
		}

		// nothing found
		return '';

	}

	/**
	 * Get the featues images list data
	 * @param $list_id - int - id of the list we need to get
	 * @return html
	*/
	public static function features_images_list($list_id) {

		// get the parent data
		if($features_images = FastlawData::module_features_images_category($list_id)) {

			// item output
			$items_html = '';

			// get the children
			if($features_images_items = FastlawData::module_features_images_items(false, $list_id)) {

				foreach($features_images_items as $item) {

					// image
					$item->img = get_image($item->img);

					// check readmore
					$readmore = false;
					if($item->readmore_text != ''
					&& $item->readmore_text != NULL
					&& $item->readmore_link != ''
					&& $item->readmore_link != NULL) {
						$readmore = true;
					}

					// echo our template
					$items_html .= get_html(array(

						// title of the block
						'title'			=> $item->name,
						'text'			=> $item->info,

						// img
						'image'			=> isset($item->img->src) ? $item->img->src : false,
						'image_alt'		=> isset($item->img->alt) ? $item->img->alt : '',

						'readmore_text'	=> $readmore ? $item->readmore_text : false,
						'readmore_link'	=> $readmore ? $item->readmore_link : '',

						// template
						'template' 		=> APP_VIEWS . '/_global/features-images-item.html'

					));

				}

				// echo our template
				return get_html(array(

					// title of the block
					'title'		=> $features_images->name,
					'text'		=> $features_images->info,

					// item list
					'items'		=> $items_html != '' ? $items_html : false,

					// template
					'template' => APP_VIEWS . '/_global/features-images.html'

				));

			}

		}

		// nothing found
		return '';

	}

	/**
	 * Get the features icon list data
	 * @param $list_id - int - id of the list we need to get
	 * @return html
	*/
	public static function features_icons_list($list_id) {

		// get the parent data
		if($features_icons = FastlawData::module_features_icons_category($list_id)) {

			// item output
			$items_html = '';

			// get the children
			if($features_images_items = FastlawData::module_features_icons_items(false, $list_id)) {

				// set it up
				foreach($features_images_items as $item) {

					// setup image
					$item->img = get_image($item->img);

					// echo our template
					$items_html .= get_html(array(

						// title of the block
						'text'		=> $item->name,

						// img
						'icon'		=> $item->img ? $item->img->template : '',

						// template
						'template' 	=> APP_VIEWS . '/_global/features-icons-item.html'

					));

				}

				// get the background image
				$features_icons->img = get_image($features_icons->img, 'features-block__bg');

				// return our template
				return get_html(array(

					// title of the block
					'title'		=> line_breaks($features_icons->name),

					// img
					'image'		=> $features_icons->img ? $features_icons->img->template : '',

					// item list
					'items'		=> $items_html != '' ? $items_html : false,

					// template
					'template' 	=> APP_VIEWS . '/_global/features-icons.html'

				));

			}

		}

		// nothing found
		return '';

	}

	/**
	 * Get the features icon list data
	 * @param $faq_id - int - id of the list we need to get
	 * @return html
	*/
	public static function faq_inner_pages($faq_id) {

		// get the parent data
		if($faq = FastlawData::module_faq_category($faq_id)) {

			// get the children
			if($items_html = self::faq_items(false, $faq_id)) {

				// return our template
				return get_html(array(

					// title of the block
					'title'		=> $faq->title != '' && $faq->title != NULL ? $faq->title : $faq->name,
					'text'		=> $faq->info,

					// item list
					'items'		=> $items_html ? $items_html : '',

					// template
					'template' 	=> APP_VIEWS . '/features/features-faq.html'

				));

			}

		}

		// nothing found
		return '';

	}

	/**
	 * Get the faq template for the main faq page
	 * @param $active_alias - string - active faq section
	 * @return html string
	*/
	public static function faq($active_alias = false) {

		// get the parent data
		if($categories = FastlawData::module_faq_category()) {

			// item output
			$category_html = '';
			$items_html = '';
			$active_class = ' class="faq__menu__active"';

			// get current page
			$page = FrontendContent::get_current_page();

			// set it up
			foreach($categories as $key => $cat) {

				if($cat->show_main != NULL
				&& $cat->show_main
				&& $items = self::faq_items(false, $cat->id)) {

					// add to items list
					$items_html .= $items;

					// update active status
					$active = false;
					if($active_alias && $cat->alias == $active_alias) {
						$active = true;
					} else if($active_alias === false) {
						if($key == 0) $active = true;
						else $active = false;
					}

					// create the url
					$faq_url = FrontendContent::get_lang_url(
						Config::read('lang_current'), $page
					);

					// echo our template
					$category_html .= get_html(array(

						// title of the block
						'name'		=> $cat->name,
						'link'		=> $faq_url . '/' . $cat->alias,
						'active'	=> $active ? $active_class : '',
						'id'		=> $cat->id,

						// template
						'template' 	=> APP_VIEWS . '/faq/faq-menu-item.html'

					));

				}

			}

			return get_html(array(

				// our menu and items
				'menu'			=> $category_html,
				'items' 		=> $items_html,

				// page title before search and serach placeholder
				'title'			=> $page->name,
				'search'		=> isset($page->search_text) ? clean_up_string($page->search_text) : '',
				'search_term'	=> !is_default_lang() ? 'Meklēt' : 'Search',
				'no_results'	=> clean_up_string($page->no_results),
				'valid_term'	=> clean_up_string($page->valid_term),
				'length'		=> clean_up_string($page->length),
				'url'			=> FrontendContent::get_lang_url(false, $page),

				// litle text box
				'subtitle'		=> isset($page->subtitle) ? $page->subtitle : false,
				'content'		=> $page->content,

				// template
				'template' 		=> APP_VIEWS . '/faq/faq.html'

			));

		}

		// nothing found
		return '';

	}

	/**
	 * Get the faq template for the main faq page
	 * @param $id - int - id of single item
	 * @param $faq_id - int - parent id
	 * @param $active - int - active parent id
	 * @return mixd - html string or bool:false
	*/
	public static function faq_items($id = false, $faq_id = 1) {

		if($faq_items = FastlawData::module_faq_items(false, $faq_id)) {

			// set var
			$items_html = '';

			// check for faq page
			$page = FrontendContent::get_current_page();
			if($page->accesskey == "faq") $class = '  question--hidden';
			else $class = '';

			// set it up
			foreach($faq_items as $item) {

				// echo our template
				$items_html .= get_html(array(

					// title of the block
					'title'		=> stripslashes($item->name),
					'text'		=> $item->info,
					'parent'	=> $faq_id,
					'class'		=> $class,

					// template
					'template' 	=> APP_VIEWS . '/_global/faq-item.html'

				));

			}

			// return html
			return $items_html;

		}

		// nothing found
		return false;

	}

	/**
	 * Get the home Why Us block
	 * @return html
	*/
	public static function home_why_us() {

		// get the children
		if($items = FastlawData::module_whyus_items()) {

			// output
			$items_html = '';

			// set it up
			foreach($items as $item) {

				// setup image
				$item->img = get_image($item->img);

				// echo our template
				$items_html .= get_html(array(

					// title of the block
					'title'		=> $item->name,
					'text'		=> $item->info != '' && $item->info != NULL ? $item->info : false,

					// img
					'icon'		=> $item->img ? $item->img->template : '',

					// template
					'template' 	=> APP_VIEWS . '/home/home-why-item.html'

				));

			}

			// get the current page
			$page = FrontendContent::get_current_page();
			$page = isset($page->children->home_why) ? $page->children->home_why : false;

			// return our template
			return get_html(array(

				// title of the block
				'title'		=> $page ? line_breaks($page->name) : false,
				'text'		=> $page && $page->content != '' ? line_breaks($page->content) : false,

				// item list
				'items'		=> $items_html != '' ? $items_html : false,

				// template
				'template' 	=> APP_VIEWS . '/home/home-why.html'

			));

		}

		// nothing found
		return '';

	}

	/**
	 * Get the home page profesionals
	 * @return html
	*/
	public static function home_team() {

		// get the children
		if($team = FastlawData::module_team_items()) {

			// member list
			$member_html[1] = '';
			$member_html[2] = '';
			$member_html[3] = '';

			foreach($team as $member) {

				// get the image
				$member->img = get_image($member->img);

				// check for youtube
				$video = parse_youtube_url($member->video);

				// create link if youtube
				if($video) {

					// set the url
					$video = 'https://www.youtube.com/watch?v=' . $video;

				} else {

					// check for vimeo
					$video = parse_vimeo_url($member->video);
					if($video) $video = 'https://vimeo.com/' . $video;

				}

				// setup template
				$member_html[$member->id] = get_html(array(

					// title of the block
					'name'			=> $member->name,
					'position'		=> $member->position,

					// video
					'video'			=> $video ? $video : false,

					'video_text'	=> Config::read('lang_current') == 'en' ? 'Watch video' : 'Skatīties video',

					// img
					'image'			=> $member->img ? $member->img->src : false,
					'image_alt'		=> $member->img ? $member->img->alt : '',

					// template
					'template' 		=> APP_VIEWS . '/home/home-people-' . $member->id . '.html'

				));

			}

			// get the current page
			$page = FrontendContent::get_current_page();
			$page = isset($page->children->home_people) ? $page->children->home_people : false;

			// return our template
			return get_html(array(

				// title of the block
				'title'		=> $page ? line_breaks($page->name) : false,
				'text'		=> $page && $page->content != '' ? line_breaks($page->content) : false,

				// member list
				'member_1'	=> $member_html[1],
				'member_2'	=> $member_html[2],
				'member_3'	=> $member_html[3],

				// template
				'template' 	=> APP_VIEWS . '/home/home-people.html'

			));

		}

		// nothing found
		return '';

	}

	/**
	 * Get the home page profesionals
	 * @return html
	*/
	public static function socialnetworks() {

		// variable
		$items_html = '';

		// get the children
		if($social = FastlawData::module_socialnetworks_items()) {

			foreach($social as $item) {

				// get the icon
				$item->svg = get_svg('assets/social/' . $item->network . '.svg');

				// setup template
				$items_html .= get_html(array(

					// content
					'link'			=> $item->link,
					'icon'			=> $item->svg,

					// template
					'template' 		=> APP_VIEWS . '/_global/social-item.html'

				));

			}

		}

		// nothing found
		return $items_html;

	}

	/**
	 * Pricing plan block
	 * @return html
	*/
	public static function price_plan($id, $features) {

		// get the children
		if($plan = FastlawData::module_pricing_items($id)) {

			// split up price
			$price = explode(".", $plan->price_year);

			// button
			$button = false;
			if($plan->button_text != '' && $plan->button_text != NULL
			&& $plan->button_link != '' && $plan->button_link != NULL) {
				$button = $plan->button_text;
				$button_link = $plan->button_link;
			}

			// features
			$features_list = '';
			foreach($plan->features as $feature_id => $feature) {
				if(isset($features[$feature_id])) {
					$features_list .= '<li><span>' . $features[$feature_id] . '</span></li>';
				}
			}

			// check current page
			$page = FrontendContent::get_current_page();


			// setup template
			return get_html(array(

				// content
				'price_month'		=> $plan->price_month,
				'price_year'		=> $plan->price_year,
				'name'				=> $plan->name,
				'button'			=> $button,
				'button_link'		=> $button ? $button_link : '',

				// default price
				'price1'			=> $price[0],
				'price2'			=> isset($price[1]) ? $price[1] : '00',

				// recomended
				'recomended'		=> $plan->recomended,
				'recomended_text'	=> $page->accesskey == 'pricing' && $plan->recomended && $plan->recomended_text != '' ? $plan->recomended_text : false,

				// feature list
				'features'			=> $features_list,

				// template
				'template' 			=> APP_VIEWS . '/pricing/pricing-plan.html'

			));

		}

		// nothing found
		return '';

	}

	/**
	 * Pricing plan block
	 * @return html
	*/
	public static function pricing_details() {

		// set language
		$lang = Config::read('lang_current');

		// get the plans
		$plan1 = FastlawData::module_pricing_items(1);
		$plan2 = FastlawData::module_pricing_items(2);
		$plan3 = FastlawData::module_pricing_items(3);

		// do anything only if atleast one plan set
		if($plan1 || $plan2 || $plan3) {

			if($features = FastlawData::module_pricing_features_items()) {
				
				$features_html = '';

				foreach($features as $feature_id => $feature) {

					if($plan1) {
						$plan1_feature = isset($plan1->features->$feature_id)
						? get_html(array('template' => APP_VIEWS . '/pricing/pricing-more-check.html'))
						: get_html(array('template' => APP_VIEWS . '/pricing/pricing-more-x.html'));
					}

					if($plan2) {
						$plan2_feature = isset($plan2->features->$feature_id)
						? get_html(array('template' => APP_VIEWS . '/pricing/pricing-more-check.html'))
						: get_html(array('template' => APP_VIEWS . '/pricing/pricing-more-x.html'));
					}

					if($plan3) {
						$plan3_feature = isset($plan3->features->$feature_id)
						? get_html(array('template' => APP_VIEWS . '/pricing/pricing-more-check.html'))
						: get_html(array('template' => APP_VIEWS . '/pricing/pricing-more-x.html'));
					}
					
					$features_html .= get_html(array(

						'feature'	=> $feature,

						'plan1_feature' => isset($plan1_feature) ? $plan1_feature : '', 
						'plan2_feature' => isset($plan2_feature) ? $plan2_feature : '', 
						'plan3_feature' => isset($plan3_feature) ? $plan3_feature : '', 

						// template
						'template' 			=> APP_VIEWS . '/pricing/pricing-more-row.html'

					));

				}

				// setup template
				return get_html(array(

					// header
					'plan1_header'		=> $plan1 ? get_html(
						array(
							'name' => $plan1->name,
							'price' => $plan1->price_year . '&euro;' . ( $lang == 'lv' ? ' mēnesī' : ' per month' ),
							'template' => APP_VIEWS . '/pricing/pricing-more-header.html'
						)
					) : '',

					// header
					'plan2_header'		=> $plan2 ? get_html(
						array(
							'name' => $plan2->name,
							'price' => $plan2->price_year . '&euro;' . ( $lang == 'lv' ? ' mēnesī' : ' per month' ),
							'template' => APP_VIEWS . '/pricing/pricing-more-header.html'
						)
					) : '',

					// header
					'plan3_header'		=> $plan3 ? get_html(
						array(
							'name' => $plan3->name,
							'price' => $plan3->price_year . '&euro;' . ( $lang == 'lv' ? ' mēnesī' : ' per month' ),
							'template' => APP_VIEWS . '/pricing/pricing-more-header.html'
						)
					) : '',

					'features'			=> $features_html,

					// template
					'template' 			=> APP_VIEWS . '/pricing/pricing-more.html'

				));

			}

		}

		// nothing found
		return '';

	}

}





// -- class.FastlawTemplates.php