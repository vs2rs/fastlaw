<?php

/**
 * Static class for input fields
 * @author vs2rs
*/

class FastlawForm extends Form {

	// define some classes
	// public static $class_button = '  button  text-upper';
	// public static $class_label  = '  text-upper';

	// get the inputs
	public static function inputs($form) {
		include("inputs.$form.php");
		return $inputs;
	}

	/**
	 * Send the form
	 * @param $input_post_data - array - our post data
	 * @param $redirect_section - string - section to redirect to
	 * @param $params - array - params for our send mail function
	 * @return void
	*/
	public static function send_contact_form($input_post_data, $redirect_section, $params) {

		// setup our email template
		$email_template = FastlawEmailTemplate::contacts($input_post_data);
		$email_attachments = self::create_file_list($input_post_data);

		// send the mail
		$email_sent = send_mail(
			$params,
			$email_template,
			$email_attachments
		);

		// default to error
		$redirect_url = '?send=error';

		// check if email sent
		if($email_sent) {
			$redirect_url = '?send=success';
		}

		// get lang and pages
		$lang = Config::read('lang_current');
		if($lang != Config::read('lang_default')) {
			$redirect_url = $lang . $redirect_url;
		}

		// redirect
		header('Location: ' . $redirect_url); exit;

	}

	/**
	 * Some language stuff
	 * @param $lang - string - lang code
	 * @return string
	*/
	public static function lang($lang, $param) {

		// params
		$params = array(

			"en" => array(
				"name_label" 			=> "Your name",
				"name_placeholder"		=> "Name Surname",
				"email_label" 			=> "Your e-mail",
				"email_placeholder"		=> "email@address.com",
				"phone_label" 			=> "Phone number",
				"phone_placeholder"		=> "+371 12345678",
				"company_label" 		=> "Organization",
				"company_placeholder"	=> "Company name",
				"position_label" 		=> "Position",
				"position_placeholder"	=> "Position name",
				"message_label" 		=> "Message",
				"message_placeholder"	=> "Enter your message",
				"company_size"			=> "Enterprise size",
				"company_size_1"		=> "10 to 49",
				"company_size_2"		=> "50 to 249",
				"company_size_3"		=> "250 or more",
				"submit_button"			=> "Send"
			),

			"lv" => array(
				"name_label" 			=> "Tabs vārds",
				"name_placeholder"		=> "Vārds Uzvārds",
				"email_label" 			=> "Tavs e-pasts",
				"email_placeholder"		=> "email@address.com",
				"phone_label" 			=> "Telefona numurs",
				"phone_placeholder"		=> "+371 12345678",
				"company_label" 		=> "Uzņēmums",
				"company_placeholder"	=> "Uzņēmuma nosaukums",
				"position_label" 		=> "Amats",
				"position_placeholder"	=> "Amata nosaukums",
				"message_label" 		=> "Ziņojums",
				"message_placeholder"	=> "Ziņojuma teksts",
				"company_size"			=> "Darbinieku skaits",
				"company_size_1"		=> "10 līdz 49",
				"company_size_2"		=> "50 līdz 249",
				"company_size_3"		=> "250 un vairāk",
				"submit_button"			=> "Nosūtīt"
			)

		);

		// return
		return $params[$lang][$param];

	}

}





// -- class.SpiceAtsauksmesForm.php