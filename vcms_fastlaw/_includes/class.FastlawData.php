<?php

/**
 * Static class for geting data from db
 * @author vs2rs
*/

class FastlawData {

	/**
	 * Get the data
	 * @param $what - string - what to get
	 * @return array
	*/
	public static function get_data($what, $id = false, $parent_id = false) {

		// get db
		$db = Database::getInstance();

		// get current language
		$lang = Config::read('lang_current');
		$active_param = 'active_' . $lang;

		// check the language
		$lang_param = '';
		if($lang && $lang != Config::read('lang_default')) {
			$lang_param = '_' . $lang;
		}

		// setup where for id
		$where_id = '';
		if($id !== false) $where_id = " AND id = $id";

		// default parent id is one
		if($parent_id === false) $parent_id = 1;

		// check what to get
		switch ($what) {

			case 'features':
				$query = "SELECT
					id,
					name$lang_param as name,
					alias$lang_param as alias,
					title$lang_param as title,
					info$lang_param as info,
					features_images_id,
					features_icons_id,
					faq_id,
					img,
					meta_title$lang_param as meta_title,
					meta_description$lang_param as meta_description,
					meta_keywords$lang_param as meta_keywords,
					facebook_title$lang_param as facebook_title,
					facebook_description$lang_param as facebook_description,
					facebook_image$lang_param as facebook_image
				FROM {{prefix}}module_features_items 
				WHERE active = 1 AND parent = $parent_id AND $active_param = 1
				  AND name$lang_param <> '' AND name$lang_param IS NOT NULL
				  $where_id
				ORDER BY item_order ASC";
				break;

			case 'features_images_cat':
				$query = "SELECT
					id,
					name$lang_param as name,
					info$lang_param as info
				FROM {{prefix}}module_features_images_category 
				WHERE active = 1 AND name$lang_param <> '' AND name$lang_param IS NOT NULL
				  $where_id
				ORDER BY item_order ASC";
				break;

			case 'features_images_items':
				$query = "SELECT
					id,
					parent,
					name$lang_param as name,
					info$lang_param as info,
					readmore_text$lang_param as readmore_text,
					readmore_link$lang_param as readmore_link,
					img
				FROM {{prefix}}module_features_images_items 
				WHERE active = 1 AND parent = $parent_id AND $active_param = 1
				  AND name$lang_param <> '' AND name$lang_param IS NOT NULL
				  $where_id
				ORDER BY item_order ASC";
				break;

			case 'features_icons_cat':
				$query = "SELECT
					id,
					name$lang_param as name,
					img
				FROM {{prefix}}module_features_icons_category 
				WHERE active = 1 AND name$lang_param <> '' AND name$lang_param IS NOT NULL
				  $where_id
				ORDER BY item_order ASC";
				break;

			case 'features_icons_items':
				$query = "SELECT
					id,
					parent,
					name$lang_param as name,
					img
				FROM {{prefix}}module_features_icons_items 
				WHERE active = 1 AND parent = $parent_id
				  AND name$lang_param <> '' AND name$lang_param IS NOT NULL
				  $where_id
				ORDER BY item_order ASC";
				break;

			case 'faq_cat':
				$query = "SELECT
					id,
					name$lang_param as name,
					alias$lang_param as alias,
					title$lang_param as title,
					info$lang_param as info,
					show_main$lang_param as show_main
				FROM {{prefix}}module_faq_category 
				WHERE active = 1 AND $active_param = 1
				  AND name$lang_param <> '' AND name$lang_param IS NOT NULL
				  $where_id
				ORDER BY item_order ASC";
				break;

			case 'faq_items':
				$query = "SELECT
					id,
					parent,
					name$lang_param as name,
					info$lang_param as info
				FROM {{prefix}}module_faq_items 
				WHERE active = 1 AND parent = $parent_id AND $active_param = 1
				  AND name$lang_param <> '' AND name$lang_param IS NOT NULL
				  $where_id
				ORDER BY item_order ASC";
				break;

			case 'whyus':
				$query = "SELECT
					id,
					name$lang_param as name,
					info$lang_param as info,
					img
				FROM {{prefix}}module_whyus_items 
				WHERE active = 1 AND parent = $parent_id AND $active_param = 1
				  AND name$lang_param <> '' AND name$lang_param IS NOT NULL
				  $where_id
				ORDER BY item_order ASC";
				break;

			case 'team':
				$query = "SELECT
					id,
					name,
					position$lang_param as position,
					video,
					img
				FROM {{prefix}}module_team_items 
				WHERE active = 1 AND parent = $parent_id $where_id
				ORDER BY item_order ASC";
				break;

			case 'social':
				$query = "SELECT
					id,
					name,
					link,
					network
				FROM {{prefix}}module_socialnetworks_items 
				WHERE active = 1 AND parent = $parent_id $where_id
				ORDER BY item_order ASC";
				break;

			case 'pricing_features':

				// query
				$parent_id = '';
				if($id !== false) $parent_id = " AND parent = $id";
				$query = "SELECT
					id,
					name$lang_param as name
				FROM {{prefix}}module_pricing_features_items 
				WHERE active = 1 $parent_id
				ORDER BY parent ASC, item_order ASC";

				// default results
				$resutls = false;
				
				// get the data
				if($data = $db->get_rows($query, 'data')) {

					// our array
					$results = array();

					// setup result array
					while ($row = $data->fetch_object()) {
						$results[$row->id] = $row->name;
					}

				}

				// results
				return $results;

				// end
				break;

			case 'pricing':
				$query = "SELECT
					id,
					name$lang_param as name,
					recomended$lang_param as recomended,
					recomended_text$lang_param as recomended_text,
					price_month$lang_param as price_month,
					price_year$lang_param as price_year,
					button_text$lang_param as button_text,
					button_link$lang_param as button_link,
					features
				FROM {{prefix}}module_pricing_items 
				WHERE active = 1 AND parent = $parent_id AND $active_param = 1
				  AND name$lang_param <> '' AND name$lang_param IS NOT NULL
				  $where_id
				ORDER BY item_order ASC";
				break;
			
			default:
				$query = false;
				break;

		}

		// set our get function based on if id is set
		$get_function = "get_rows";
		if($id !== false) $get_function = "get_row";

		// get results and return
		return $query ? $result = $db->$get_function($query) : false;
		
	}

	/**
	 * Alias - get_data() - features module
	 * @param $id - int - if we need to get single object
	 * @return array
	*/
	public static function module_features_items($id = false) {
		return self::get_data("features", $id);
	}

	/**
	 * Get first item from features db
	 * @return int - id of the element
	*/
	public static function module_features_default_item() {

		// get current language
		$lang = Config::read('lang_current');
		$active_param = 'active_' . $lang;

		// check the language
		$lang_param = '';
		if($lang && $lang != Config::read('lang_default')) {
			$lang_param = '_' . $lang;
		}
		
		// set query
		$query = "SELECT id
		FROM {{prefix}}module_features_items 
		WHERE active = 1 AND parent = 1 AND $active_param = 1
		  AND name$lang_param <> '' AND name$lang_param IS NOT NULL
		ORDER BY item_order ASC LIMIT 1";

		// get db
		$db = Database::getInstance();

		// get results and return
		return $result = $db->get_one($query);

	}

	/**
	 * Aliases for get_data()
	 * @param $id - int - if we need to get single object
	 * @param $parent_id - int - id of parent
	 * @return mix - array or bool:false
	*/
	public static function module_features_images_category($id = false) {
		return self::get_data("features_images_cat", $id);
	}

	public static function module_features_images_items($id = false, $parent_id = 1) {
		return self::get_data("features_images_items", $id, $parent_id);
	}

	public static function module_features_icons_category($id = false) {
		return self::get_data("features_icons_cat", $id);
	}

	public static function module_features_icons_items($id = false, $parent_id = 1) {
		return self::get_data("features_icons_items", $id, $parent_id);
	}

	public static function module_faq_category($id = false) {
		return self::get_data("faq_cat", $id);
	}

	public static function module_faq_items($id = false, $parent_id = 1) {
		return self::get_data("faq_items", $id, $parent_id);
	}

	public static function module_whyus_items() {
		return self::get_data("whyus");
	}

	public static function module_team_items() {
		return self::get_data("team");
	}

	public static function module_socialnetworks_items() {
		return self::get_data("social");
	}

	public static function module_pricing_features_items($id = false) {
		return self::get_data("pricing_features", $id);
	}

	public static function module_pricing_items($id = false) {

		// get the daata
		$data = self::get_data("pricing", $id);

		// check for featurs
		if(isset($data->features)
		&& $data->features != ''
		&& $data->features != NULL) {
			$data->features = json_decode($data->features);
		}

		// return
		return $data;

	}

	/**
	 * Get item alias based on language
	 * @param $lang - string - lang code
	 * @return string
	*/
	public static function get_lang_alias($lang, $module_item) {

		// set the name of our module and id to check
		$module = $module_item->module;
		$module = '{{prefix}}module_' . $module->module_name . '_' . $module->content;
		$item_id = $module_item->id;

		// setup lang and alias
		$lang_param = '';
		$alias_param = 'alias';
		if($lang != Config::read('lang_default')) {
			$lang_param = '_' . $lang;
			$alias_param = 'alias_' . $lang;
		}

		$db_query = "SELECT $alias_param
		FROM $module
		WHERE id = $item_id AND active = 1 AND active$lang_param = 1
		LIMIT 1";

		// get db
		$db = Database::getInstance();

		// return results
		return $db->get_one($db_query);

	}

}





// -- class.FastlawData.php