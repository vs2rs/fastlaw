<?php

/**
 * Static class for email templates
 * @author vs2rs
*/

class FastlawEmailTemplate {

	/**
	 * Get hte url for images in e-mails
	*/
	private static function url() {

		// default to production
		$url = 'https://fastlaw.goodtimes.lv';

		// return
		return $url;

	}

	/**
	 * Forest Value E-mail Form
	 * @param array $inputs - submited inputs
	*/
	public static function contacts($inputs) {

		// set the template
		$email_template = '';

		// get the logo
		$email_template .= get_html(array(
			'url'		=> self::url(),
			'template'	=> APP_VIEWS . '/_email/email-logo.html'
		));

		$email_contact = '';
		$email_company = '';

		// go through inputs
		foreach ($inputs as $input) {

			// ignore files and privacy checkbox
			if($input['input_type'] == 'input_file') continue;
			if($input['input_type'] == 'input_checkbox') continue;

			// check if value is set
			if(isset($input['value']) && $input['value'] != '') {

				// contacts
				if($input['name'] == 'fastlaw_name'
				|| $input['name'] == 'fastlaw_phone'
				|| $input['name'] == 'fastlaw_email') {
					$email_contact .= '<div>' . $input['value'] . '</div>';
				}

				// contacts
				if($input['name'] == 'fastlaw_company') {
					$email_company .= '<div>Nosaukums: ' . $input['value'] . '</div>';
				}

				// contacts
				if($input['name'] == 'fastlaw_enterprise_size') {
					if($input['extra']['checked']) {
						$email_company .= '<div>Darbinieki: ' . $input['label'] . '</div>';
					}
				}

				// contacts
				if($input['name'] == 'fastlaw_position') {
					$email_company .= '<div>Amats: ' . $input['value'] . '</div>';
				}

				if($input['name'] == 'fastlaw_text') {
					$email_message = $input['value'];
				}

			}

		}
			
		// get the row
		$email_template .= get_html(array(
			'label'			=> 'Kantakpersona',
			'value'			=> $email_contact,
			'value_size'	=> '16px',
			'template'		=> APP_VIEWS . '/_email/email-form-row.html'
		));

		// get the row
		$email_template .= get_html(array(
			'label'			=> 'Kompānijas informācija',
			'value'			=> $email_company,
			'value_size'	=> '16px',
			'template'		=> APP_VIEWS . '/_email/email-form-row.html'
		));
			
		// get the row
		if(isset($email_message)) {
			$email_template .= get_html(array(
				'label'			=> 'Ziņojums',
				'value'			=> $email_message,
				'value_size'	=> '16px',
				'template'		=> APP_VIEWS . '/_email/email-form-row.html'
			));
		}

		// add everythin to the template
		$email_template = get_html(array(
			'content'	=> $email_template,
			'template'	=> APP_VIEWS . '/_email/email.html'
		));

		// return
		return $email_template;

	}

}




