<?php

// current lang
$lang = Config::read('lang_current');

// inputs
$inputs = array(
	
	// array(
	// 	// define input type
	// 	'input_type'	=> 'hidden',
	// 	// values
	// 	'name' 			=> 'contact_form',
	// 	'label' 		=> '',
	// 	'value'			=> 1,
	// ),

	array(
		// define input type
		'input_type'	=> 'text',
		// values
		'name' 			=> 'fastlaw_name',
		'label'			=> FastlawForm::lang($lang, 'name_label'),
		'placeholder' 	=> FastlawForm::lang($lang, 'name_placeholder'),
		'color' 		=> false,
		// validation
		'required' 		=> true,
		'length'		=> 50,
		'validate'		=> 'letters_utf8',
	),

	array(
		// define input type
		'input_type'	=> 'input_email',
		// values
		'name' 			=> 'fastlaw_email',
		'label'			=> FastlawForm::lang($lang, 'email_label'),
		'placeholder' 	=> FastlawForm::lang($lang, 'email_placeholder'),
		// validation
		'required' 		=> true,
		'length'		=> 128,
		'validate'		=> 'email',
	),

	array(
		// define input type
		'input_type'	=> 'input_phone',
		// values
		'name' 			=> 'fastlaw_phone',
		'label'			=> FastlawForm::lang($lang, 'phone_label'),
		'placeholder' 	=> FastlawForm::lang($lang, 'phone_placeholder'),
		// validation
		'required' 		=> false,
		'length'		=> 32,
		'validate'		=> 'phone',
	),

	array(
		// define input type
		'input_type'	=> 'text',
		// values
		'name' 			=> 'fastlaw_company',
		'label'			=> FastlawForm::lang($lang, 'company_label'),
		'placeholder' 	=> FastlawForm::lang($lang, 'company_placeholder'),
		'color' 		=> false,
		// validation
		'required' 		=> true,
		'length'		=> 50,
		'validate'		=> 'letters_utf8',
	),

	array(
		// define input type
		'input_type'	=> 'text',
		// values
		'name' 			=> 'fastlaw_position',
		'label'			=> FastlawForm::lang($lang, 'position_label'),
		'placeholder' 	=> FastlawForm::lang($lang, 'position_placeholder'),
		'color' 		=> false,
		// validation
		'required' 		=> true,
		'length'		=> 50,
		'validate'		=> 'letters_utf8',
	),

	array(
		// define input type
		'input_type'	=> 'textarea',
		// values
		'name' 			=> 'fastlaw_text',
		'label'			=> FastlawForm::lang($lang, 'message_label'),
		'placeholder' 	=> FastlawForm::lang($lang, 'message_placeholder'),
		// validation
		'required' 		=> false,
		'validate'		=> 'text',
	),

	// ------------------------------------
	// radio buttons
	// ------------------------------------

	array(
		// define input type
		'input_type'	=> 'input_radio',
		'class'			=> '',
		// values
		'name' 			=> 'fastlaw_enterprise_size',
		'label'			=> FastlawForm::lang($lang, 'company_size_1'),
		'value'			=> 1,
		// validation
		'required' 		=> true,
		'validate'		=> array('1','2','3'),
		// extras
		'extra'			=> array(
			'parent_name'	=> FastlawForm::lang($lang, 'company_size'),
			'checked'		=> true
		)
	),

	array(
		// define input type
		'input_type'	=> 'input_radio',
		'class'			=> '',
		// values
		'name' 			=> 'fastlaw_enterprise_size',
		'label'			=> FastlawForm::lang($lang, 'company_size_2'),
		'value'			=> 2,
		// validation
		'required' 		=> true,
		'validate'		=> array('1','2','3'),
		// extras
		'extra'			=> array(
			'parent_name'	=> FastlawForm::lang($lang, 'company_size'),
		)
	),

	array(
		// define input type
		'input_type'	=> 'input_radio',
		'class'			=> '',
		// values
		'name' 			=> 'fastlaw_enterprise_size',
		'label'			=> FastlawForm::lang($lang, 'company_size_3'),
		'value'			=> 3,
		// validation
		'required' 		=> true,
		'validate'		=> array('1','2','3'),
		// extras
		'extra'			=> array(
			'parent_name'	=> FastlawForm::lang($lang, 'company_size'),
		)
	),

	// ------------------------------------
	// privacy checkbox
	// ------------------------------------

	// array(
	// 	// define input type
	// 	'input_type'	=> 'input_checkbox',
	// 	'class'			=> '',
	// 	'label_class'	=> '',
	// 	// values
	// 	'name' 			=> 'spice_atsauksmes_privacy',
	// 	'label'			=> 'Izmantojot šo lapu piekrītat, ka ar Jums sazināsies par iesniegto ierosinājumu,
	// 						atsauksmi vai sūdzību, izmantojot iesniegtos personas datus, un, apliecināt,
	// 						ka esat iepazinies ar “SPICE” <a href="https://www.spice.lv/lv/privatuma-politika" target="_blank">Privātuma politiku.</a>',
	// 	// 'comment'		=> '<a class="link-border" href="privatuma-politika">Privacy Policy</a>',
	// 	'value'			=> 1,
	// 	// validation
	// 	'required' 		=> true,
	// 	'validate'		=> 'checked',
	// )

);




