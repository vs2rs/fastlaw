<?php

$lang["friendlyname"] = "Sākumlapa: Kāpēc izvēlēties mūs";
$lang["moddescription"] = "Sākumlapa: Kāpēc izvēlēties mūs";
$lang["admindescription"] = "Sākumlapa: Kāpēc izvēlēties mūs";

// advance permission name
$lang["permissions_advanced"] = "%s - %s - Full Access";
$lang["permissions_manage_category"] = "%s - %s - Manage Categories";
$lang["permissions_manage_items"] = "%s - %s - Manage Items";

// strings for category
$lang["add_category"] = "Add Category";
$lang["add_category_title"] = "Create a New Category";
$lang["edit_category"] = "Edit Category";
$lang["category"] = "Category";
$lang["category_plural"] = "Categories";

// strings for items
$lang["add_items"] = "Pievienot iemeslu";
$lang["add_items_title"] = "Pievienot iemeslu";
$lang["edit_items"] = "Labot iemeslu";
$lang["items"] = "Iemesls";
$lang["items_plural"] = "Iemesli";
$lang["items_info"] = "Apraksts (EN)";
$lang["items_info_lv"] = "Apraksts (LV)";
$lang["items_img"] = "Ikona (SVG vai PNG bez fona)";
$lang["items_name_en"] = "Virsraksts (EN)";
$lang["items_name_lv"] = "Virsraksts (LV)";

// strings for general fields
$lang["id"] = "id";
$lang["name"] = "Name";
$lang["alias"] = "URL";
$lang["alias_comment"] = "URL is a unique identifier for this object.";
$lang["isdefault"] = "Is default?";
$lang["active"] = "Active";
$lang["parent"] = "Parent";
$lang["submit"] = "Submit";
$lang["cancel"] = "Cancel";
$lang["nbchildren"] = "Nb of items";
$lang["date_modified"] = "Last Modified";
$lang["date_created"] = "Date Created";





// -- lv_LV.php