<?php

$lang["friendlyname"] = "Home: Why Us";
$lang["moddescription"] = "Home: Why Us";
$lang["admindescription"] = "Home: Why Us";

// advance permission name
$lang["permissions_advanced"] = "%s - %s - Full Access";
$lang["permissions_manage_category"] = "%s - %s - Manage Categories";
$lang["permissions_manage_items"] = "%s - %s - Manage Items";

// strings for category
$lang["add_category"] = "Add Category";
$lang["add_category_title"] = "Create a New Category";
$lang["edit_category"] = "Edit Category";
$lang["category"] = "Category";
$lang["category_plural"] = "Categories";

// strings for items
$lang["add_items"] = "Add Item";
$lang["add_items_title"] = "Create a New Item";
$lang["edit_items"] = "Edit Item";
$lang["items"] = "Item";
$lang["items_plural"] = "Items";
$lang["items_info"] = "Info (EN)";
$lang["items_info_lv"] = "Info (LV)";
$lang["items_img"] = "Icon (SVG or transparent PNG)";
$lang["items_name_en"] = "Title (EN)";
$lang["items_name_lv"] = "Title (LV)";
	
// strings for general fields
$lang["id"] = "id";
$lang["name"] = "Name";
$lang["alias"] = "URL";
$lang["alias_comment"] = "URL is a unique identifier for this object.";
$lang["isdefault"] = "Is default?";
$lang["active"] = "Active";
$lang["parent"] = "Parent";
$lang["submit"] = "Submit";
$lang["cancel"] = "Cancel";
$lang["nbchildren"] = "Nb of items";
$lang["date_modified"] = "Last Modified";
$lang["date_created"] = "Date Created";





// -- en_US.php