<?php
if(!isset($gCms)) exit;

// Typical Database Initialization
$db = &$this->cms->db;
$dict = NewDataDictionary($db);



	$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_whyus_category");
	$dict->ExecuteSQLArray($sqlarray);
	$db->DropSequence(cms_db_prefix()."module_whyus_category_seq");
	

	$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_whyus_items");
	$dict->ExecuteSQLArray($sqlarray);
	$db->DropSequence(cms_db_prefix()."module_whyus_items_seq");
		
	$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_whyus_saved_queries");
	$dict->ExecuteSQLArray($sqlarray);
	$db->DropSequence(cms_db_prefix()."module_whyus_saved_queries_seq");

// permissions
	$this->RemovePermission("module_whyus_manage_category");
	$this->RemovePermission("module_whyus_manage_items");
	$this->RemovePermission("module_whyus_advanced");
	$this->RemovePreference();

// put mention into the admin log
	$this->Audit( 0, $this->Lang("friendlyname"), $this->Lang("uninstalled"));

?>
