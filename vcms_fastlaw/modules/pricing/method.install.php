<?php
if(!isset($gCms)) exit;

// Typical Database Initialization
$db = &$this->cms->db;
$dict = NewDataDictionary($db);
		
// mysql-specific, but ignored by other database
$taboptarray = array("mysql" => "TYPE=MyISAM");
		

// Creates the category table
$flds = "
	id I NOTNULL AUTOINCREMENT KEY,
	name C(255),
	alias C(255),
	item_order I,
	active L,
	isdefault L,
    date_modified ".CMS_ADODB_DT.",
	date_created ".CMS_ADODB_DT."
	";

$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_pricing_category", $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);


// Creates the items table
$flds = "
	price_month C(255),
	price_year C(255),
	button_text C(255),
	button_link C(255),
	features C(255),
	parent I,
	id I NOTNULL AUTOINCREMENT KEY,
	name C(255),
	alias C(255),
	item_order I,
	active L,
	isdefault L,
    date_modified ".CMS_ADODB_DT.",
	date_created ".CMS_ADODB_DT."
	";

$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_pricing_items", $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);



// CREATING PERMISSIONS :

// permissions
$this->CreatePermission("module_pricing_advanced", "Module - Pricing: Main (Advanced)");
	$this->CreatePermission("module_pricing_manage_category", "Module - Pricing: Main (Category)");
	$this->CreatePermission("module_pricing_manage_items", "Module - Pricing: Main (Items)");
// prepare information for an eventual upgrade
$this->SetPreference("makerversion","1.8.9.3");

// put mention into the admin log
$this->Audit( 0, $this->Lang("friendlyname"), $this->Lang("installed",$this->GetVersion()));

?>
