<?php

$lang["friendlyname"] = "Pricing: Main";
$lang["moddescription"] = "Pricing: Main";
$lang["admindescription"] = "Pricing: Main";

// advance permission name
$lang["permissions_advanced"] = "%s - %s - Full Access";
$lang["permissions_manage_category"] = "%s - %s - Manage Categories";
$lang["permissions_manage_items"] = "%s - %s - Manage Plans";

// strings for category
$lang["add_category"] = "Add Category";
$lang["add_category_title"] = "Create a New Category";
$lang["edit_category"] = "Edit Category";
$lang["category"] = "Category";
$lang["category_plural"] = "Categories";

// strings for items
$lang["add_items"] = "Add Plan";
$lang["add_items_title"] = "Create a New Plan";
$lang["edit_items"] = "Edit Plan";
$lang["items"] = "Plan";
$lang["items_plural"] = "Plans";
$lang["items_price_month"] = "Price Monthly (EN)";
$lang["items_price_month_lv"] = "Price Monthly (LV)";
$lang["items_price_year"] = "Price Year (EN)";
$lang["items_price_year_lv"] = "Price Year (LV)";
$lang["items_button_text"] = "Buttont Text (EN)";
$lang["items_button_text_lv"] = "Buttont Text (LV)";
$lang["items_button_link"] = "Buttont Link (EN)";
$lang["items_button_link_lv"] = "Buttont Link (LV)";
$lang["items_features"] = "Features";
$lang["items_name_en"] = "Name (EN)";
$lang["items_name_lv"] = "Name (LV)";
$lang["items_recomended_en"] = "Recomended plan (EN)";
$lang["items_recomended_lv"] = "Recomended plan (LV)";
$lang["items_recomended_text"] = "Recomended text (EN)";
$lang["items_recomended_text_lv"] = "Recomended text (LV)";

$lang["items_tab_1"] = "Plan features";
	
// strings for general fields
$lang["id"] = "id";
$lang["name"] = "Name";
$lang["alias"] = "URL";
$lang["alias_comment"] = "URL is a unique identifier for this object.";
$lang["isdefault"] = "Is default?";
$lang["active"] = "Active";
$lang["parent"] = "Parent";
$lang["submit"] = "Submit";
$lang["cancel"] = "Cancel";
$lang["nbchildren"] = "Nb of items";
$lang["date_modified"] = "Last Modified";
$lang["date_created"] = "Date Created";
