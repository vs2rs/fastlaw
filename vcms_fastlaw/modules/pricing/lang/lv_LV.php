<?php

$lang["friendlyname"] = "Cenu plāni: Galvenais";
$lang["moddescription"] = "Cenu plāni: Galvenais";
$lang["admindescription"] = "Cenu plāni: Galvenais";

// advance permission name
$lang["permissions_advanced"] = "%s - %s - Full Access";
$lang["permissions_manage_category"] = "%s - %s - Manage Categories";
$lang["permissions_manage_items"] = "%s - %s - Manage Plans";

// strings for category
$lang["add_category"] = "Add Category";
$lang["add_category_title"] = "Create a New Category";
$lang["edit_category"] = "Edit Category";
$lang["category"] = "Category";
$lang["category_plural"] = "Categories";

// strings for items
$lang["add_items"] = "Pievienot plānu";
$lang["add_items_title"] = "Pievienot plānu";
$lang["edit_items"] = "Labot plānu";
$lang["items"] = "Plāns";
$lang["items_plural"] = "Plāni";
$lang["items_price_month"] = "Cena mēnesī (EN)";
$lang["items_price_month_lv"] = "Cena mēnesī (LV)";
$lang["items_price_year"] = "Cena gadā (EN)";
$lang["items_price_year_lv"] = "Cena gadā (LV)";
$lang["items_button_text"] = "Pogas teksts (EN)";
$lang["items_button_text_lv"] = "Pogas teksts (LV)";
$lang["items_button_link"] = "Pogas links (EN)";
$lang["items_button_link_lv"] = "Pogas links (LV)";
$lang["items_features"] = "Funkcijas";
$lang["items_name_en"] = "Nosaukums (EN)";
$lang["items_name_lv"] = "Nosaukums (LV)";
$lang["items_recomended_en"] = "Ieskam šo plānu (EN)";
$lang["items_recomended_lv"] = "Ieskam šo plānu (LV)";
$lang["items_recomended_text"] = "Ieskam šo plānu teksts (EN)";
$lang["items_recomended_text_lv"] = "Ieskam šo plānu teksts (LV)";

$lang["items_tab_1"] = "Plāna funkciju saraksts";
	
// strings for general fields
$lang["id"] = "id";
$lang["name"] = "Name";
$lang["alias"] = "URL";
$lang["alias_comment"] = "URL is a unique identifier for this object.";
$lang["isdefault"] = "Is default?";
$lang["active"] = "Active";
$lang["parent"] = "Parent";
$lang["submit"] = "Submit";
$lang["cancel"] = "Cancel";
$lang["nbchildren"] = "Nb of items";
$lang["date_modified"] = "Last Modified";
$lang["date_created"] = "Date Created";
