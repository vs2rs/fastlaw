<?php

$lang["friendlyname"] = "Features: List With Images";
$lang["moddescription"] = "Features: List With Images";
$lang["admindescription"] = "Features: List With Images";

// advance permission name
$lang["permissions_advanced"] = "%s - %s - Full Access";
$lang["permissions_manage_category"] = "%s - %s - Manage Categories";
$lang["permissions_manage_items"] = "%s - %s - Manage List items";

// strings for category
$lang["add_category"] = "Add Category";
$lang["add_category_title"] = "Create a New Category";
$lang["edit_category"] = "Edit Category";
$lang["category"] = "Category";
$lang["category_plural"] = "Categories";
$lang["category_info"] = "Info (EN)";
$lang["category_info_lv"] = "Info (LV)";
$lang["category_name_en"] = "Name (EN)";
$lang["category_name_lv"] = "Name (LV)";

// strings for items
$lang["add_items"] = "Add List item";
$lang["add_items_title"] = "Create a New List item";
$lang["edit_items"] = "Edit List item";
$lang["items"] = "List item";
$lang["items_plural"] = "List items";
$lang["items_info"] = "Info (EN)";
$lang["items_info_lv"] = "Info (LV)";
$lang["items_img"] = "Image";
$lang["items_name_en"] = "Name (EN)";
$lang["items_name_lv"] = "Name (LV)";
$lang["items_readmore_text"] = "Read More Text (EN)";
$lang["items_readmore_text_lv"] = "Read More Text (LV)";
$lang["items_readmore_link"] = "Read More Link (EN)";
$lang["items_readmore_link_lv"] = "Read More Link (LV)";

// strings for general fields
$lang["id"] = "id";
$lang["name"] = "Name";
$lang["alias"] = "URL";
$lang["alias_comment"] = "URL is a unique identifier for this object.";
$lang["isdefault"] = "Is default?";
$lang["active"] = "Active";
$lang["parent"] = "Parent";
$lang["submit"] = "Submit";
$lang["cancel"] = "Cancel";
$lang["nbchildren"] = "Nb of items";
$lang["date_modified"] = "Last Modified";
$lang["date_created"] = "Date Created";





// -- en_US.php