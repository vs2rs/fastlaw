<?php

$lang["friendlyname"] = "Funkcijas: Saraksts ar bildēm";
$lang["moddescription"] = "Funkcijas: Saraksts ar bildēm";
$lang["admindescription"] = "Funkcijas: Saraksts ar bildēm";

// advance permission name
$lang["permissions_advanced"] = "%s - %s - Full Access";
$lang["permissions_manage_category"] = "%s - %s - Manage Categories";
$lang["permissions_manage_items"] = "%s - %s - Manage List items";

// strings for category
$lang["add_category"] = "Pievienot funckiju bloku";
$lang["add_category_title"] = "Jauns funkciju bloks";
$lang["edit_category"] = "Labot funkciju bloku";
$lang["category"] = "Funkciju bloks";
$lang["category_plural"] = "Funkciju bloki";
$lang["category_info"] = "Apraksts (EN)";
$lang["category_info_lv"] = "Apraksts (LV)";
$lang["category_name_en"] = "Virsraksts (EN)";
$lang["category_name_lv"] = "Virsraksts (LV)";

// strings for items
$lang["add_items"] = "Pievienot funkciju";
$lang["add_items_title"] = "Pievienot funkciju";
$lang["edit_items"] = "Edit List item";
$lang["items"] = "Funkcijas";
$lang["items_plural"] = "Funkciju saraksts";
$lang["items_info"] = "Apraksts (EN)";
$lang["items_info_lv"] = "Apraksts (LV)";
$lang["items_img"] = "Bilde";
$lang["items_name_en"] = "Virsraksts (EN)";
$lang["items_name_lv"] = "Virsraksts (LV)";
$lang["items_readmore_text"] = "Lasīt vairāk teksts (EN)";
$lang["items_readmore_text_lv"] = "Lasīt vairāk teksts (LV)";
$lang["items_readmore_link"] = "Lasīt vairāk links (EN)";
$lang["items_readmore_link_lv"] = "Lasīt vairāk links (LV)";