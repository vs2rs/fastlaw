<?php
$lang["friendlyname"] = "Cenu plāni: Funkciju saraksts";
$lang["moddescription"] = "Cenu plāni: Funkciju saraksts";
$lang["admindescription"] = "Cenu plāni: Funkciju saraksts";

// advance permission name
$lang["permissions_advanced"] = "%s - %s - Full Access";
$lang["permissions_manage_category"] = "%s - %s - Manage Categories";
$lang["permissions_manage_items"] = "%s - %s - Manage Features";

// strings for category
$lang["add_category"] = "Add Category";
$lang["add_category_title"] = "Create a New Category";
$lang["edit_category"] = "Edit Category";
$lang["category"] = "Kategorija";
$lang["category_plural"] = "Kategorijas";

// strings for items
$lang["add_items"] = "Pievienot funkciju";
$lang["add_items_title"] = "Pievienot funkciju";
$lang["edit_items"] = "Labot funkciju";
$lang["items"] = "Funkcija";
$lang["items_plural"] = "Funkcijas";
$lang["items_name_en"] = "Nosaukums (EN)";
$lang["items_name_lv"] = "Nosaukums (LV)";

// strings for general fields
$lang["id"] = "id";
$lang["name"] = "Name";
$lang["alias"] = "URL";
$lang["alias_comment"] = "URL is a unique identifier for this object.";
$lang["isdefault"] = "Is default?";
$lang["active"] = "Active";
$lang["parent"] = "Parent";
$lang["submit"] = "Submit";
$lang["cancel"] = "Cancel";
$lang["nbchildren"] = "Nb of items";
$lang["date_modified"] = "Last Modified";
$lang["date_created"] = "Date Created";
