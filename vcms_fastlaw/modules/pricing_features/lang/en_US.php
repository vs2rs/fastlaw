<?php
$lang["friendlyname"] = "Pricing: Features";
$lang["moddescription"] = "Pricing: Features";
$lang["admindescription"] = "Pricing: Features";

// advance permission name
$lang["permissions_advanced"] = "%s - %s - Full Access";
$lang["permissions_manage_category"] = "%s - %s - Manage Categories";
$lang["permissions_manage_items"] = "%s - %s - Manage Features";

// strings for category
$lang["add_category"] = "Add Category";
$lang["add_category_title"] = "Create a New Category";
$lang["edit_category"] = "Edit Category";
$lang["category"] = "Category";
$lang["category_plural"] = "Categories";

// strings for items
$lang["add_items"] = "Add Feature";
$lang["add_items_title"] = "Create a New Feature";
$lang["edit_items"] = "Edit Feature";
$lang["items"] = "Feature";
$lang["items_plural"] = "Features";
$lang["items_name_en"] = "Name (EN)";
$lang["items_name_lv"] = "Name (LV)";

// strings for general fields
$lang["id"] = "id";
$lang["name"] = "Name";
$lang["alias"] = "URL";
$lang["alias_comment"] = "URL is a unique identifier for this object.";
$lang["isdefault"] = "Is default?";
$lang["active"] = "Active";
$lang["parent"] = "Parent";
$lang["submit"] = "Submit";
$lang["cancel"] = "Cancel";
$lang["nbchildren"] = "Nb of items";
$lang["date_modified"] = "Last Modified";
$lang["date_created"] = "Date Created";
