<?php

$lang["friendlyname"] = "Sākumlapa: Profesionāļi";
$lang["moddescription"] = "Sākumlapa: Profesionāļi";
$lang["admindescription"] = "Sākumlapa: Profesionāļi";

// advance permission name
$lang["permissions_advanced"] = "%s - %s - Full Access";
$lang["permissions_manage_category"] = "%s - %s - Manage Categories";
$lang["permissions_manage_items"] = "%s - %s - Manage Members";

// strings for category
$lang["add_category"] = "Add Category";
$lang["add_category_title"] = "Create a New Category";
$lang["edit_category"] = "Edit Category";
$lang["category"] = "Category";
$lang["category_plural"] = "Categories";

// strings for items
$lang["add_items"] = "Pievienot dalībnieku";
$lang["add_items_title"] = "Pievienot dalībnieku";
$lang["edit_items"] = "Labot dalībnieku";
$lang["items"] = "Dalībnieks";
$lang["items_plural"] = "Dalībnieki";
$lang["items_position"] = "Amats (EN)";
$lang["items_position_lv"] = "Amats (LV)";
$lang["items_img"] = "Bilde";
$lang["items_video"] = "Video embed links";
$lang["items_name"] = "Vārds, uzvārds";
	
// strings for general fields
$lang["id"] = "id";
$lang["name"] = "Name";
$lang["alias"] = "URL";
$lang["alias_comment"] = "URL is a unique identifier for this object.";
$lang["isdefault"] = "Is default?";
$lang["active"] = "Active";
$lang["parent"] = "Parent";
$lang["submit"] = "Submit";
$lang["cancel"] = "Cancel";
$lang["nbchildren"] = "Nb of items";
$lang["date_modified"] = "Last Modified";
$lang["date_created"] = "Date Created";





// -- en_US.php