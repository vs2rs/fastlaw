<?php

$lang["friendlyname"] = "Home: Professionals";
$lang["moddescription"] = "Home: Professionals";
$lang["admindescription"] = "Home: Professionals";

// advance permission name
$lang["permissions_advanced"] = "%s - %s - Full Access";
$lang["permissions_manage_category"] = "%s - %s - Manage Categories";
$lang["permissions_manage_items"] = "%s - %s - Manage Members";

// strings for category
$lang["add_category"] = "Add Category";
$lang["add_category_title"] = "Create a New Category";
$lang["edit_category"] = "Edit Category";
$lang["category"] = "Category";
$lang["category_plural"] = "Categories";

// strings for items
$lang["add_items"] = "Add Member";
$lang["add_items_title"] = "Create a New Member";
$lang["edit_items"] = "Edit Member";
$lang["items"] = "Member";
$lang["items_plural"] = "Members";
$lang["items_position"] = "Position (EN)";
$lang["items_position_lv"] = "Position (LV)";
$lang["items_img"] = "Image";
$lang["items_video"] = "Video Embed Link";
$lang["items_name"] = "Name and surname";
	
// strings for general fields
$lang["id"] = "id";
$lang["name"] = "Name";
$lang["alias"] = "URL";
$lang["alias_comment"] = "URL is a unique identifier for this object.";
$lang["isdefault"] = "Is default?";
$lang["active"] = "Active";
$lang["parent"] = "Parent";
$lang["submit"] = "Submit";
$lang["cancel"] = "Cancel";
$lang["nbchildren"] = "Nb of items";
$lang["date_modified"] = "Last Modified";
$lang["date_created"] = "Date Created";





// -- en_US.php