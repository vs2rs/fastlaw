<?php
if (!isset($gCms)) exit;

$newparams = isset($params["levelname"])?array("active_tab"=>$params["levelname"]):array();

if (!isset($params["move"]) || !isset($params["tablename"]) || !isset($params["prefix"]) || !isset($params["currentorder"]) )
$this->Redirect($id, "defaultadmin", $returnid, $newparams);

$db =& $this->GetDb();
$dbtable = cms_db_prefix()."module_".$params["tablename"];
$order = $params["currentorder"];
$itemid = $params[$params["prefix"]."id"];
$wparent = isset($params["parent"])?" AND parent='".$params["parent"]."'":"";

switch($params["move"]){
	
	case "delete":
		if(isset($params["sharechildren"]) && $params["sharechildren"]){
			// this level share its children... we delete the item's links to children
			if($childlevel = $this->get_nextlevel($params["levelname"])){
				$linktable = $dbtable."_has_".$childlevel;
				$query = "DELETE FROM $linktable WHERE ".$params["levelname"]."_id=?";
				$db->Execute($query, array($itemid));
			}
		}elseif(isset($params["child"]) && $params["child"] != ""){
			// this level doesn't share its children... we delete the item's children
			$childtable = cms_db_prefix()."module_".$params["child"];
			$query = "DELETE FROM $childtable WHERE parent = ?";
			$db->Execute($query, array($itemid));
		}
		if(isset($params["sharedbyparents"]) && $params["sharedbyparents"]){
			// this item is shared by its parents... we delete links from parents to this child
			if($parentlevel = $this->get_nextlevel($params["levelname"], false)){
				$linktable = cms_db_prefix()."module_features_icons_".$parentlevel."_has_".$params["levelname"];
				$query = "DELETE FROM $linktable WHERE ".$params["levelname"]."_id=?";
				$db->Execute($query, array($itemid));
			}
		}
			
		
		$query = "DELETE FROM $dbtable WHERE id = ? LIMIT 1";
		$db->Execute($query, array($itemid));

		// UPDATE THE ORDER OF THE ITEMS
		$query = "UPDATE $dbtable SET item_order=(item_order-1) WHERE item_order > ? ".$wparent;
		$db->Execute($query, array($order));

		$newparams["module_message"] = $this->lang("message_deleted");

		// delete ==> gallery and images

		// set the fields function to get galleries
		$level_fileds_function = "get_level_" . $params["levelname"] . "_fields";

		if($delete_galleries = $this->$level_fileds_function("gallery")) {

			foreach ($delete_galleries as $delete_gallery_id => $delete_gallery_folder) {
				
				// image database
				$imagetable = cms_db_prefix() . "images";
				
				// set the path
				$delete_item_gallery_folder =
				$config["uploads_path"] . $delete_gallery_folder . $itemid . "/";

				// delete if it's there
				if(is_dir($delete_item_gallery_folder) === true) {
					// check if there are files in the folder
					if(count(glob($delete_item_gallery_folder."*")) > 0) {
			        	// cycle true the files and delete one by on
			    		foreach(glob($delete_item_gallery_folder."*") as $filename) {
			    			unlink($filename);
			    		}
					}
				    rmdir($delete_item_gallery_folder);
				}

				// set the parent for db delete
				$imageparent = $this->GetName() . "_"
							 . $params["levelname"] . "_"
							 . $delete_gallery_id
							 . "_id" . $itemid;

				// delete
				$query = "DELETE FROM $imagetable WHERE parent = ?";
				$db->Execute($query, array($imageparent));

			}

		}

		break;	

	case "up":
		if ($order != 1){
			$query = "UPDATE $dbtable SET item_order=(item_order+1) WHERE item_order = ? $wparent LIMIT 1;";
			$db->Execute($query, array($order-1));
			$query = "UPDATE $dbtable SET item_order=(item_order-1) WHERE id = ? LIMIT 1;";
			$db->Execute($query, array($itemid));
		}
		break;
		
	case "down":
		$query = "UPDATE $dbtable SET item_order=(item_order-1) WHERE item_order = ? $wparent LIMIT 1;";
		if( $db->Execute($query, array($order+1)) ){
			$query = "UPDATE $dbtable SET item_order=(item_order+1) WHERE id = ? LIMIT 1;";
			$db->Execute($query, array($itemid));
		}
		break;
}

$this->Redirect($id, "defaultadmin", $returnid, $newparams);
?>