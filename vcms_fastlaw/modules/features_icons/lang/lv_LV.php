<?php

$lang["friendlyname"] = "Funkcijas: Saraksts ar ikonām";
$lang["moddescription"] = "Funkcijas: Saraksts ar ikonām";
$lang["admindescription"] = "Funkcijas: Saraksts ar ikonām";

// advance permission name
$lang["permissions_advanced"] = "%s - %s - Full Access";
$lang["permissions_manage_category"] = "%s - %s - Manage Categories";
$lang["permissions_manage_items"] = "%s - %s - Manage List items";
	
// strings for category
$lang["add_category"] = "Pievienot funckiju bloku";
$lang["add_category_title"] = "Pievienot funckiju bloku";
$lang["edit_category"] = "Labot funckiju bloku";

$lang["category"] = "Funkciju bloks";
$lang["category_plural"] = "Funkciju bloki";
$lang["category_name_en"] = "Virsraksts (EN)";
$lang["category_name_lv"] = "Virsraksts (LV)";
$lang["category_img"] = "Bloka vizuālis";
	
// strings for items
$lang["add_items"] = "Pievienot funkciju";
$lang["add_items_title"] = "Pievienot funkciju";
$lang["edit_items"] = "Labot funkciju";

$lang["items"] = "Funkcijas";
$lang["items_plural"] = "Funkciju saraksts";
$lang["items_img"] = "Ikona (SVG vai PNG bez fona)";
$lang["items_name_en"] = "Apraksts (EN)";
$lang["items_name_lv"] = "Apraksts (LV)";

// strings for general fields
$lang["id"] = "id";
$lang["name"] = "Name";
$lang["alias"] = "URL";
$lang["alias_comment"] = "URL is a unique identifier for this object.";
$lang["isdefault"] = "Is default?";
$lang["active"] = "Active";
$lang["parent"] = "Parent";
$lang["submit"] = "Submit";
$lang["cancel"] = "Cancel";
$lang["nbchildren"] = "Nb of items";
$lang["date_modified"] = "Last Modified";
$lang["date_created"] = "Date Created";





// -- lv_LV.php