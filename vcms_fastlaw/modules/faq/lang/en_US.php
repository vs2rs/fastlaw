<?php

$lang["friendlyname"] = "Frequently Asked Questions";
$lang["moddescription"] = "Frequently Asked Questions";
$lang["admindescription"] = "Frequently Asked Questions";

// advance permission name
$lang["permissions_advanced"] = "%s - %s - Full Access";
$lang["permissions_manage_category"] = "%s - %s - Manage Sections";
$lang["permissions_manage_items"] = "%s - %s - Manage Questions";

// strings for category
$lang["add_category"] = "Add Section";
$lang["add_category_title"] = "Create a New Section";
$lang["edit_category"] = "Edit Section";

$lang["category"] = "Section";
$lang["category_plural"] = "Sections";
$lang["category_name_en"] = "Menu Text (EN)";
$lang["category_name_lv"] = "Menu Text (LV)";
$lang["category_info"] = "Info (EN)";
$lang["category_info_lv"] = "Info (LV)";
$lang["category_show_en"] = "Show this section in the main FAQ page (EN)";
$lang["category_show_lv"] = "Show this section in the main FAQ page (LV)";
$lang["categroy_alias_en"] = "URL (EN)";
$lang["categroy_alias_lv"] = "URL (LV)";
$lang["category_title"] = "Title (EN)";
$lang["category_title_lv"] = "Title (LV)";

// strings for items
$lang["add_items"] = "Add Question";
$lang["add_items_title"] = "Create a New Question";
$lang["edit_items"] = "Edit Question";

$lang["items"] = "Question";
$lang["items_plural"] = "Questions";
$lang["items_info"] = "Info (EN)";
$lang["items_info_lv"] = "Info (LV)";
$lang["items_name_en"] = "Name (EN)";
$lang["items_name_lv"] = "Name (LV)";
$lang["items_tab_0"] = "Content of the question";
	
// strings for general fields
$lang["id"] = "id";
$lang["name"] = "Name";
$lang["alias"] = "URL";
$lang["alias_comment"] = "URL is a unique identifier for this object.";
$lang["isdefault"] = "Is default?";
$lang["active"] = "Active";
$lang["parent"] = "Parent";
$lang["submit"] = "Submit";
$lang["cancel"] = "Cancel";
$lang["nbchildren"] = "Nb of items";
$lang["date_modified"] = "Last Modified";
$lang["date_created"] = "Date Created";




