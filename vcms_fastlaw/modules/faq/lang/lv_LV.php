<?php

$lang["friendlyname"] = "Biežāk uzdotie jautājumi";
$lang["moddescription"] = "Biežāk uzdotie jautājumi";
$lang["admindescription"] = "Biežāk uzdotie jautājumi";

// advance permission name
$lang["permissions_advanced"] = "%s - %s - Full Access";
$lang["permissions_manage_category"] = "%s - %s - Manage Sections";
$lang["permissions_manage_items"] = "%s - %s - Manage Questions";

// strings for category
$lang["add_category"] = "Pievienot grupu";
$lang["add_category_title"] = "Pievienot grupu";
$lang["edit_category"] = "Labot grupu";

$lang["category"] = "Jautājumu grupa";
$lang["category_plural"] = "Jautājumu grupas";
$lang["category_name_en"] = "Teksts izvēlnē (EN)";
$lang["category_name_lv"] = "Teksts izvēlnē (LV)";
$lang["category_info"] = "Apraksts (EN)";
$lang["category_info_lv"] = "Apraksts (LV)";
$lang["category_show_en"] = "Rādīt galvenajā biežāk uzdoto jautājumu sadaļā (EN)";
$lang["category_show_lv"] = "Rādīt galvenajā biežāk uzdoto jautājumu sadaļā (LV)";
$lang["category_title"] = "Virsraksts (EN)";
$lang["category_title_lv"] = "Virsraksts (LV)";

// strings for items
$lang["add_items"] = "Pievienot jautājumu";
$lang["add_items_title"] = "Pievienot jautājumu";
$lang["edit_items"] = "Labot jautājumu";

$lang["items"] = "Jautājums";
$lang["items_plural"] = "Jautājumi";
$lang["items_info"] = "Jautājuma teksts (EN)";
$lang["items_info_lv"] = "Jautājuma teksts (LV)";
$lang["items_name_en"] = "Virsraksts (EN)";
$lang["items_name_lv"] = "Virsraksts (LV)";
$lang["items_tab_0"] = "Jautājuma saturs";
	
// strings for general fields
$lang["id"] = "id";
$lang["name"] = "Name";
$lang["alias"] = "URL";
$lang["alias_comment"] = "URL is a unique identifier for this object.";
$lang["isdefault"] = "Is default?";
$lang["active"] = "Active";
$lang["parent"] = "Parent";
$lang["submit"] = "Submit";
$lang["cancel"] = "Cancel";
$lang["nbchildren"] = "Nb of items";
$lang["date_modified"] = "Last Modified";
$lang["date_created"] = "Date Created";




