<?php
$lang["friendlyname"] = "Funkcijas: Galvenais";
$lang["moddescription"] = "Funkcijas: Galvenais";
$lang["admindescription"] = "Funkcijas: Galvenais";

// advance permission name
$lang["permissions_advanced"] = "%s - %s - Full Access";
$lang["permissions_manage_category"] = "%s - %s - Manage Categories";
$lang["permissions_manage_items"] = "%s - %s - Manage Features";
	
// strings for category
$lang["category"] = "Category";
$lang["category_plural"] = "Categories";
$lang["add_category"] = "Add Category";
$lang["add_category_title"] = "Create a New Category";
$lang["edit_category"] = "Edit Category";
	
// strings for items
$lang["items"] = "Sadaļa";
$lang["items_plural"] = "Sadaļas";
$lang["add_items"] = "Pievienot sadaļu";
$lang["add_items_title"] = "Pievienot sadaļu";
$lang["edit_items"] = "Labot sadaļu";

$lang["items_tab_0"] = "Angļu valoda";
$lang["items_tab_1"] = "Latviešu valoda";

$lang["items_name_lv"] = "Nosaukums izvēlnē (LV)";
$lang["items_alias_lv"] = "URL (LV)";
$lang["items_title"] = "Virsraksts (EN)";
$lang["items_title_lv"] = "Virsraksts (LV)";
$lang["items_info"] = "Īss apraksts (EN)";
$lang["items_info_lv"] = "Īss apraksts (LV)";
$lang["items_img"] = "Image";
$lang["items_features_images_id"] = "Funkciju bilžu saraksts";
$lang["items_features_icons_id"] = "Funkciju ikonu saraksts";
$lang["items_faq_id"] = "Biežāk uzdotie jautājumi";
	
// strings for general fields
$lang["id"] = "id";
$lang["name"] = "Nosaukums izvēlnē (EN)";
$lang["alias"] = "URL (EN)";
$lang["alias_comment"] = "URL is a unique identifier for this object.";
$lang["isdefault"] = "Is default?";
$lang["active"] = "Active";
$lang["parent"] = "Parent";
$lang["submit"] = "Submit";
$lang["cancel"] = "Cancel";
$lang["nbchildren"] = "Nb of items";
$lang["date_modified"] = "Last Modified";
$lang["date_created"] = "Date Created";





// -- en_US.php