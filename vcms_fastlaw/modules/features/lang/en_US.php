<?php
$lang["friendlyname"] = "Features: Main";
$lang["moddescription"] = "Features: Main";
$lang["admindescription"] = "Features: Main";

// advance permission name
$lang["permissions_advanced"] = "%s - %s - Full Access";
$lang["permissions_manage_category"] = "%s - %s - Manage Categories";
$lang["permissions_manage_items"] = "%s - %s - Manage Features";
	
// strings for category
$lang["category"] = "Category";
$lang["category_plural"] = "Categories";
$lang["add_category"] = "Add Category";
$lang["add_category_title"] = "Create a New Category";
$lang["edit_category"] = "Edit Category";
	
// strings for items
$lang["items"] = "Feature";
$lang["items_plural"] = "Features";
$lang["add_items"] = "Add Feature";
$lang["add_items_title"] = "Create a New Feature";
$lang["edit_items"] = "Edit Feature";

$lang["items_tab_0"] = "Enlgish";
$lang["items_tab_1"] = "Latvian";

$lang["items_name_lv"] = "Menu Text (LV)";
$lang["items_alias_lv"] = "URL (LV)";
$lang["items_title"] = "Title (EN)";
$lang["items_title_lv"] = "Title (LV)";
$lang["items_info"] = "Info (EN)";
$lang["items_info_lv"] = "Info (LV)";
$lang["items_img"] = "Image";
$lang["items_features_images_id"] = "Features Image List";
$lang["items_features_icons_id"] = "Features Icon List";
$lang["items_faq_id"] = "FAQ Block";
	
// strings for general fields
$lang["id"] = "id";
$lang["name"] = "Menu Text (EN)";
$lang["alias"] = "URL (EN)";
$lang["alias_comment"] = "URL is a unique identifier for this object.";
$lang["isdefault"] = "Is default?";
$lang["active"] = "Active";
$lang["parent"] = "Parent";
$lang["submit"] = "Submit";
$lang["cancel"] = "Cancel";
$lang["nbchildren"] = "Nb of items";
$lang["date_modified"] = "Last Modified";
$lang["date_created"] = "Date Created";





// -- en_US.php