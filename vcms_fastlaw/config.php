<?php

/* --------------------------------- *\
 
	Check Allowed Domains

\* --------------------------------- */

$config['domain_default'] = "localhost";
$config['domain_allowed'] = in_array(APP_HOSTNAME, array(
	"localhost",
	"www.fastlaw.lv",
	"fastlaw.lv"
));

/* --------------------------------- *\
 
	Is this dev server?

\* --------------------------------- */

// set dev domains
$config["dev"] = in_array(APP_HOSTNAME, array(
	"localhost"
));

// do we minify our css?
$config["minify_html"] = !$config["dev"] ? true : false;

/* --------------------------------- *\
 
	Database

\* --------------------------------- */

$config['dbms'] 		= 'mysqli';
$config['db_hostname'] 	= 'localhost';
$config['db_prefix'] 	= 'fl_';
$config["db_port"] 		= false;
$config['db_username'] 	= '';
$config['db_password'] 	= '';
$config['db_name'] 		= '';

/* --------------------------------- *\
 
	Admin Auth Config

\* --------------------------------- */

// session name
$config['vcms_auth_name'] = 'set_the_session_name';

// login with email or username (value:email|username)
$config['vcms_auth_type'] = 'email';

// login tries till ip is blocked (value:false|int)
$config['vcms_auth_tries'] = 3;

// true or false, if true then vcms_auth_type will be changed to email
$config['vcms_auth_2fa'] = true;

/* --------------------------------- *\
 
	Global e-mail settings

\* --------------------------------- */

// send mail through smtp
$config["email_smtp"] 	= true;

// setup smtp
$config["email_host"]	= '';
$config["email_port"]	= 465;
$config["email_secure"]	= 'ssl';
$config["email_auth"]	= true;
$config["email_user"]	= '';
$config["email_pass"]	= '';

// show info in the error log
$config["email_debug"] 	= false; // 1, 2 or 3

// email to/from info for contact form
$config["email_to"] = '';
$config["email_from"] = '';
$config["email_name"] = 'Fastlaw';
$config["email_subject"] = 'Aizpildīta kontaforma mājaslapā www.fastlaw.lv';

/* --------------------------------- *\
 
	Directories

\* --------------------------------- */

// folder structure
$config["folder_views"]		= '/app/views';
$config["folder_uploads"]	= 'uploads';
$config["folder_css"]		= '/assets/css';
$config["folder_js"]		= '/assets/js';

/* --------------------------------- *\
 
	Some defaults for content

\* --------------------------------- */

// language setup
$config["lang_current"] = "en";
$config["lang_default"] = "en";
$config["languages"] 	= array("en", "lv");
$config["homepage"] 	= 'home';

// productin url
$config["production_www"] = "www.fastlaw.lv";

/* --------------------------------- *\
 
	Classes and functions

\* --------------------------------- */

$config["functions"] = array(
	'get_html',
	'get_svg',
	'get_image',
	'process_template_vars',
	'get_header_element',
	'validate_string',
	'get_meta',
	'send_mail'
);

$config["classes"] = array(
	'Database',
	'Layout',
	'FrontendContent',
	'Form',
	'FastlawForm',
	'FastlawEmailTemplate',
	'FastlawData',
	'FastlawTemplates'
);

/* --------------------------------- *\
 
	Error reporting

\* --------------------------------- */

if($config["dev"]) error_reporting(-1);
else error_reporting(0);

/* --------------------------------- *\
 
	Extra config if needed

\* --------------------------------- */

// set the file and check the file
$config_file_extra = dirname(__FILE__) . '/config.fastlaw.php';
if(file_exists($config_file_extra)) {
	include($config_file_extra);
}





// -- config.php --