﻿$(document).ready(function(){

	// todo v/cms folder vajadzētu no config kaut kā paņemt
	const baseHref = document.querySelector("base").href.replace("v/cms/", "");

	/**
	 * Use CKEditor on textares
	 * Toolbars:
	 * https://ckeditor.com/latest/samples/old/toolbar/toolbar.html
	 */

	document.querySelectorAll(".text-editor textarea, textarea.text-editor").forEach((editor) => {

		CKEDITOR.replace( editor, {
			
			height: editor.dataset.height,

			contentsCss: [ "assets/css/--3-1-text.css", "assets/css/--3-2-text-links.css" ],

			toolbarGroups: [
				// {"name": "clipboard",     "groups":["clipboard","undo"]},
				{"name": "basicstyles",   "groups":["basicstyles","cleanup"]},
				{"name": "paragraph",     "groups":["list","align"]},
				// "/",
				{"name": "links",         "groups":["links"]},
				{"name": "styles",        "groups":["styles"]},
				{"name": "document",      "groups":["mode"]},
				// {"name": "insert",        "groups":["insert"]}
			],

			removeButtons: "Strike,Subscript,Anchor,SpecialChar,JustifyBlock,Superscript,HorizontalRule,Styles",
			format_tags: "p;h2",
			bodyClass: "text  ck-editor",
			allowedContent: "*[class](*); p; h2; ul; ol; li; a[href,class,target]; strong; u; em; table; td; tr; img[src,alt,class];",
			
			// extraPlugins: "autogrow",
			// autoGrow_onStartup: true,

			filebrowserImageBrowseUrl: 'uploader/popup.php?folder='+editor.dataset.folder+'&inputid=ckeditor',
			filebrowserWindowWidth: '1000',
    		filebrowserWindowHeight: '700',

    		baseHref: baseHref

		});

	});

	/* ------------------------------ *\

		Character editor

	\* ------------------------------ */

	// get it and set it up
	document.querySelectorAll(".text-editor-basic textarea, textarea.text-editor-basic").forEach((editor) => {

		CKEDITOR.replace(editor, {

			// get height from data set atributie
			height: editor.dataset.height,

			// load our style sheet
			contentsCss: [ "assets/css/--1-colors.css", "assets/css/--2-text.css" ],

			// toolbars
			toolbarGroups: [
				{"name": "basicstyles",   "groups":["basicstyles","cleanup"]},
				{"name": "links",         "groups":["links"]},
				{"name": "document",      "groups":["mode"]},
			],

			// extra options
			removeButtons: "Strike,Subscript,Anchor,SpecialChar,JustifyBlock,Superscript,HorizontalRule,Format",
			format_tags: "p",
			bodyClass: "text  ck-editor",
			allowedContent: "a[href,target]; strong; u;",
    		baseHref: baseHref,
    		enterMode: CKEDITOR.ENTER_BR

		});

	});

	/* ------------------------------ *\

		Title Editor

	\* ------------------------------ */

	// get it and set it up
	document.querySelectorAll(".text-editor-title textarea, textarea.text-editor-title").forEach((editor) => {

		CKEDITOR.replace(editor, {

			// get height from data set atributie
			height: editor.dataset.height,

			// load our style sheet
			contentsCss: [ "assets/css/--1-colors.css", "assets/css/--2-text.css" ],

			// toolbars
			toolbarGroups: [
				{"name": "basicstyles",   "groups":["basicstyles","cleanup"]},
				{"name": "styles",        "groups":["styles"]},
				{"name": "document",      "groups":["mode"]},
			],

			// extra options
			removeButtons: "Strike,Subscript,Anchor,SpecialChar,JustifyBlock,Superscript,HorizontalRule",
			format_tags: "p",
			bodyClass: "text  ck-editor  title-default  text-strong",
			allowedContent: "span[class](*);",
    		baseHref: baseHref,
    		enterMode: CKEDITOR.ENTER_BR

		});

	});

	/* ------------------------------ *\

		Full editor

	\* ------------------------------ */

	// set the class
	let editor_full = '.text-editor-full';
	editor_full = editor_full + " textarea, textarea" + editor_full;

	// get all editor textareas
	document.querySelectorAll(editor_full).forEach((editor) => {

		// setup ck editor
		CKEDITOR.replace(editor, {
			
			// get height from data set atributie
			height: editor.dataset.height,

			// load our style sheet
			contentsCss: [ "assets/css/--1-colors.css", "assets/css/--2-text.css" ],

			// toolbars
			toolbarGroups: [
				{"name": "clipboard",     "groups":["clipboard","undo"]},
				{"name": "basicstyles",   "groups":["basicstyles","cleanup"]},
				{"name": "paragraph",     "groups":["list","align"]},
				"/",
				{"name": "links",         "groups":["links"]},
				{"name": "styles",        "groups":["styles"]},
				{"name": "document",      "groups":["mode"]},
				{"name": "insert",        "groups":["insert"]}
			],

			// extra options
			removeButtons: "Strike,Subscript,Anchor,SpecialChar,JustifyBlock,Superscript,HorizontalRule",
			format_tags: "p;h2;h3",
			bodyClass: "text  ck-editor",
			allowedContent: "*[class](*); p; h2; h3; ul; ol; li; a[href,target]; strong; u; em; span;",
    		baseHref: baseHref

		});

	});

	/* ------------------------------ *\

		Some Dialog Stuff

	\* ------------------------------ */

	CKEDITOR.on( 'dialogDefinition', function( ev ) {

		const dialogName = ev.data.name;
		const dialogDefinition = ev.data.definition;

		if(dialogName === 'image') {

			// todo man vajag savu uztaisīt
			// https://ckeditor.com/docs/ckeditor4/latest/guide/dev_dialog_add_file_browser.html

			// get the tab content
			const infoTab = dialogDefinition.getContents('info');

			// // disable
			// infoTab.remove('txtBorder');
			// infoTab.remove('txtHSpace');
			// infoTab.remove('txtVSpace');
			// infoTab.remove('cmbAlign');

			// remove preview
			// infoTab.remove('htmlPreview');

			// remove link tab
			dialogDefinition.removeContents('Link');

			// add style dropdown
			// Daļu atradu te: http://stackoverflow.com/questions/25130772/ckeditor-link-dialog-modification
			// Bet svarīgākais bija te: http://stackoverflow.com/questions/5024371/ckeditor-customized-html-on-inserting-an-image
			// no image.js paņemam setup un comit
			// {type:"text", id:"txtGenClass", requiredContent:"img(cke-xyz)", label:c.lang.common.cssClass, "default":"",
			// setup:function(a,b){a==f&&this.setValue(b.getAttribute("class"))},
			// commit:function(a,b){a==f&&(this.getValue()||this.isChanged())&&b.setAttribute("class",this.getValue())}}
			infoTab.add({
				type: 'select',
				label: 'Image alignment',
				id: 'buttonAlign',
				'default': 'img-full',
				setup: function(type, element) {
					this.setValue(element.getAttribute('class'));
			    },
				items: [
					['Full width',  'img-full'],
					['Align left',  'img-left'],
					['Align right', 'img-right']
				],
				commit: function(type, element) {
					if(this.getValue() || this.isChanged()) {
            			element.setAttribute('class', this.getValue());
            		}
				},
				onChange: function() {
					// update preview window
					// pie image.js kur preview html pieliku id wxPreview
					const test = infoTab.get('htmlPreview');
					var preview_image = CKEDITOR.document.getById("wxPreview").getElementsByTag('img').getItem(0);
					// const preview_image = CKEDITOR.document.querySelector(".ImagePreviewBox img");
					// uzsetojam preview logā kā jāizskatās
					if(this.getValue() == "img-full") { preview_image.setAttribute("style","width:100%;height:auto;display:block;"); }
					if(this.getValue() == "img-left") { preview_image.setAttribute("style","width:50%;height:auto;float:left;margin-right:10px;"); }
					if(this.getValue() == "img-right") { preview_image.setAttribute("style","width:50%;height:auto;float:right;margin-left:10px;"); }
					// updeitoa ir updatePreview funkcija (g=function minificētajā)
					// ielikts: a.preview.setAttribute("style","width:100%;height:auto;display:block;");if(a.preview.getAttribute("class")=="img-left"){a.preview.setAttribute("style","width:50%;height:auto;float:left;margin-right:10px;");}if(a.preview.getAttribute("class")=="img-right"){a.preview.setAttribute("style","width:50%;height:auto;float:right;margin-left:10px;");}
					// https://gist.github.com/Partoo/5413263
				},
			});

		}

	});

	/* ------------------------------ *\

		Remove &nbsp;
		- on paste
		- on remove format button

	\* ------------------------------ */

	// remove the space function
	const removeUnbreakableSpace = function(node){
		return node.replace(/&nbsp;/g, ' ');
	};

	// when instance is ready
	CKEDITOR.on('instanceReady', function(ck) {

		// on paste
		ck.editor.on("paste", function(event) {
			event.data.dataValue = removeUnbreakableSpace(event.data.dataValue);
		});

		// on command execute
		ck.editor.on('afterCommandExec', function(event){

			// if remove format function
			if(event.data.name == 'removeFormat') {

				// get current selection
				const selection = ck.editor.getSelection();
				
				// get current body
				const body = selection.document.$.activeElement;

				// do we have children?
				if(body.children !== null
				&& body.children.length > 0) {

					// go throgh children
					for (i = 0; i < body.children.length; i++) {

						// child
						const child = body.children[i];

						// remove &nbsp;
						child.innerHTML = removeUnbreakableSpace(child.innerHTML);

						// normalize
						child.normalize();

					}

				}
			
			}

		});

	});

});