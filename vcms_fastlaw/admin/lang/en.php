<?php

$lang['admin']['active_en'] = 'Show on English version of the page';
$lang['admin']['active_lv'] = 'Show on Latvian version of the page';

$lang['admin']['img'] = 'Image';
$lang['admin']['img_desktop'] = 'Image (Desktop)';
$lang['admin']['img_mob'] = 'Image (Mobile)';

$lang['admin']['pricing_modules'] = 'Pricing Plans';
$lang['admin']['features_modules'] = 'Features';
$lang['admin']['homepage_modules'] = 'Homepage';





// -- en.php