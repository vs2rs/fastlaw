/* --------------------------------- *\

	Forms and Inputs

\* --------------------------------- */

document.addEventListener("DOMContentLoaded", function(){

	/* --------------------------------- *\

		Find parent

	\* --------------------------------- */

	const findParent = function(element, parent_class) {
		while ((element = element.parentElement) && !element.classList.contains(parent_class));
		return element;
	};

	/* --------------------------------- *\

		Error message

	\* --------------------------------- */

	const formErrorMessages = function(element, message) {

		// add error class to hidden input
		if(element.type == 'hidden') {
			if(message === false) {
				element.classList.remove("form__row--error");
			} else {
				element.classList.add("form__row--error");
			}
			return;
		}

		// get parent
		const parent = findParent(element, "form__row");

		// get error
		const error = parent.querySelector(".form__error");

		// add or remove class to the parent
		if(message === false) {
			error.innerHTML = '';
			parent.classList.remove("form__row--error");
		} else {
			error.innerHTML = message;
			parent.classList.add("form__row--error");	
		}

	};

	/* --------------------------------- *\

		Customize file upload input

	\* --------------------------------- */

	// get file inputs
	const fileInputs = document.querySelectorAll('input[data-custom-input="file"]');

	fileInputs.forEach((input) => {

		// get parent
		const parent = findParent(input, "form__row");

		// get the file name div
		const fileName = parent.querySelector(".form__file");

		// update file name d div
		if(fileName !== undefined) {

			// add change event
			input.addEventListener("change", function(){

				// clear errors
				formErrorMessages(this, false);

				/**
				 * Update Selected File Text
				 */

				// check the count and update text
				if(this.files && this.files.length > 1) {

					// get file text
					const fileText = this.dataset.fileText;

					// update text
					fileName.innerHTML = fileText.replace('{{count}}', this.files.length);

				} else {

					// add file name
					fileName.innerHTML = this.files[0].name;

				}

				/**
				 * Validate max files
				 */

				// check the count and update text
				if(this.files && this.files.length > 1) {

					let max = this.dataset.maxFiles;
					if(max === undefined || max === null) max = 5;

					// validate file input
					if(this.files.length > max) {
						formErrorMessages(this, `Atļauts pievienot ${max} failus!`);
					}

				}

				/**
				 * Validate file size
				 */

				// check the count and update text
				if(this.files && this.files.length > 0) {

					let size = this.dataset.sizeFiles;
					if(size === undefined || size === null) size = 2;
		
					// get all files sizes
					let fileSize = 0;
					for(let i = 0; i < this.files.length; i++) {
						fileSize = fileSize + this.files[i].size;
					}

					// convert file sizes to mb
					fileSize = fileSize / 1024 / 1024;

					// check the size
					if(fileSize > size) {
						formErrorMessages(this, `Pārsniegti atļautie ${size} mb!`);
					}

				}

			});

		}

	});

	/* --------------------------------- *\

		Add focus to icon

	\* --------------------------------- */

	// get them
	const formIcons = document.querySelectorAll('.form__icon');

	// set them
	formIcons.forEach((icon) => {
		icon.addEventListener("click", function(){
			const parent = findParent(icon, 'form__input');
			const input = parent.querySelector("input");
			input.focus();
		});
	});

	/* --------------------------------- *\

		Validate form on submit

	\* --------------------------------- */

	const forms = document.querySelectorAll('.form-validate-js');

	forms.forEach((form) => {
		
		// get all inputs
		const inputs = form.querySelectorAll("input, select, textarea");
		const errorsFound = form.querySelector(".form__errors");

		// function to remove error message on change
		const removeErrorsOnChnage = function() {

			// skip for file inputs
			if(this.type == 'file') return;

			// single error message
			formErrorMessages(this, false);
				
			// remove the global error message
			if(errorsFound !== undefined && errorsFound !== null) {

				// check for errors
				const errorMessages = form.querySelectorAll(".form__row--error");

				// if no errors found, remove the main message
				if(errorMessages.length == 0) {
					errorsFound.classList.remove("form__errors--show");
				}
			}

		}

		// create change events to remove error messages
		inputs.forEach((input) => {
			input.addEventListener("keyup", removeErrorsOnChnage);
			input.addEventListener("change", removeErrorsOnChnage);
		});

		// setup submit action
		form.addEventListener("submit", function(event) {

			// prevent default submition
			event.preventDefault();

			// this form
			const form = this;

			// add class when sending			
			form.classList.add('form--sending');

			// validate array
			const validate = [];
			const validate_inputs = [];

			inputs.forEach((input) => {

				// default is to validate
				let add_to_validate = true;
				
				// is checkbox checked
				if(input.type == 'checkbox' && input.checked === false) {
					add_to_validate = false;
				}
				
				// is checkbox checked
				if(input.type == 'radio' && input.checked === false) {
					add_to_validate = false;
				}
				
				// add to the array of validation
				if(add_to_validate) {
					validate.push({
						"name"  : input.name,
						"value" : input.value
					});
					validate_inputs.push(input);
				}

			});

			// clear errors
			validate_inputs.forEach((input) => {
				formErrorMessages(input, false);	
			});

			// create new data
			const data = new FormData();
			data.append("validate", JSON.stringify(validate));
			
			// send get request for files
			const xhr = new XMLHttpRequest;

			// on load listener
			xhr.addEventListener("load", function(){

				// console.log(this.response);
				const response = JSON.parse(this.response);

				// get the errors
				const inputs_with_errors = Object.keys(response.errors);

				// check for error count
				if(inputs_with_errors.length > 0) {

					// remove sending class			
					form.classList.remove('form--sending');

					// update error messages
					inputs_with_errors.forEach((name) => {
						const input = form.querySelector('[name="'+name+'"]');
						formErrorMessages(input, response.errors[name]);
					});

					// check for errors
					const errorMessages = form.querySelectorAll(".form__row--error");

					// how many found?
					if(errorMessages.length > 0) {
						
						// show the global error message
						if(errorsFound !== undefined && errorsFound !== null) {
							errorsFound.classList.add("form__errors--show");

							// check if we have a container for list of errors
							// in this global error box
							const errorList = errorsFound.querySelector(".form__errors__list");

							// setup the list
							if(errorList !== undefined && errorList !== null) {
								errorList.innerHTML = '';
								inputs_with_errors.forEach((name) => {
									const li = document.createElement("li");
									li.innerHTML = response.errors[name];
									errorList.appendChild(li);
								});
							}
						}

					}

				} else {

					// remove global error message
					if(errorsFound !== undefined && errorsFound !== null) {
						errorsFound.classList.remove("form__errors--show");
					}

					// if we send with the js
					if(form.classList.contains("form-submit-js")) {

						// create form data from our form
						const data_valid = new FormData(form);
						data_valid.append("send", true);
						
						// send get request for files
						const xhr_valid = new XMLHttpRequest;
						
						// on load listener
						xhr_valid.addEventListener("load", function(){

							// decode response
							const response = JSON.parse(this.response);

							// check for success and redirect
							if(response.status == 'success') {
								window.location.href = form.action + '?send=success';
							} else {
								window.location.href = form.action + '?send=error';
							}

						});

						// send data with post
						xhr_valid.open("POST", form.action, true);
						xhr_valid.send(data_valid);

					} else {

						// submit our form
						form.submit();

					}

				}

			});

			// send data with post
			xhr.open("POST", form.action, true);
			xhr.send(data);

		});

	});

});




