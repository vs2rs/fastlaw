/* --------------------------------- *\

	Small Helper Functions

\* --------------------------------- */

// check if portrait
function isPortrait(){
	return window.innerWidth < window.innerHeight;
};





/* --------------------------------- *\

	Get element by class

\* --------------------------------- */

function getElementByClass(class_name) {

	// get the element
	const element = document.querySelector(class_name);

	// check if set
	if(element !== null && element !== undefined) {
		return element;
	}

	// false default
	return false;

};




