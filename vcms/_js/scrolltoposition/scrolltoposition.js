/* ------------------------------

	Scroll to a position (id)
	in a document

------------------------------ */

function scrollToPosition(position, margin, duration) {

	// check if # is at the start of the position and remove it
	const check_positon_id = new RegExp('^#');
	if(check_positon_id.test(position)){
	    position = position.substring(1);
	}

	// check if we have fixed header element
	let header_space = 0;
	const header = document.querySelector(".scroll-to-header-js");
	if(header !== undefined && header !== null) {
		header_space = header.offsetHeight - 1; // -1px to correct for some error when using viewport units
	}

	// check if position already set or we have an id and need to set
	if(typeof position != 'number') {

		// set element
		const element = document.getElementById(position);
		
		// if no element end the script
		if(element === null) {
			console.error("No DOM element with the ID #" + position);
			return;
		}

		if(typeof(margin) !== "undefined") {
			// get style
			const element_style = element.currentStyle || window.getComputedStyle(element);
			// calculate postion
			position = element.offsetTop - element_style.marginBottom.replace("px","") - header_space;
		} else {
			position = element.offsetTop - header_space;
		}

	}

	// set the window
	var body = window.scrollY || window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
	
	// set default if nothing given
	if(typeof(duration) == "undefined") var duration = 350;

    // we have reached the end
    if (duration <= 0) return;

    // calculate
    var difference = position - body,
    	perTick = difference / duration * 10;

    // do the scroll
    setTimeout(function() {
        window.scroll(0, body + perTick);
        scrollToPosition(position, margin, duration - 10);
    }, 10);

}




