/* ------------------------------

	Toggle slide class

------------------------------ */

function toggleSlide(event) {

	// find parent
	const findParent = function(element, parent_class) {
		while ((element = element.parentElement) && !element.classList.contains(parent_class));
		return element;
	};

	// set event
	event = event || window.event;

	// prevent default action
	event.preventDefault();

	// define our elements
	const parent = findParent(this, "slide-toggle-parent");
	const element = parent.querySelector(".slide-toggle");

    // check open class
	if(element.classList.contains("slide-toggle--open")) {

		element.classList.remove("slide-toggle--open");
    	element.classList.add("slide-toggle--closed");

		parent.classList.remove("slide-toggle-parent--open");
    	parent.classList.add("slide-toggle-parent--closed");

	} else {

    	element.classList.remove("slide-toggle--closed");
    	parent.classList.remove("slide-toggle-parent--closed");
		parent.classList.add("slide-toggle-parent--open");

		// --open class removes the overflow hidden
		// it needs to be added after slide animation
		// otherwise slide animation visually breaks
		// open element doesn't need overflow hidden

		// create event listener for transition
	    element.addEventListener('transitionend', function(e) {
	        // remove this event listener so it only gets triggered once
	        element.removeEventListener('transitionend', arguments.callee);
	        // toggle open
		    element.classList.add("slide-toggle--open");
	    });

	}

}




