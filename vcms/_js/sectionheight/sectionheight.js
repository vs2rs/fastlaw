/* --------------------------------- *\

	Set 100% height for sections

	This is for mobiles

\* --------------------------------- */

document.addEventListener("DOMContentLoaded", function(){

	// check if this is mobile device
	if(isMobile()) {

		// set the section name
		const section_class = '.section-set-100-height-js';

		// get all sections that need to set height
		const sections = document.querySelectorAll(section_class);

		// we need to get current width for referance if oriantion changes
		let device_width = window.innerWidth;

		// function for seting height
		const setSectionHeight = function() {

			// check if portrait
			if(window.innerHeight > window.innerWidth) {

				// get window height
				const height = window.innerHeight;

				// set height to window height
				sections.forEach((section) => {
					section.style.height = `${height}px`;
				});

			} else {

				// landscape

				// reset height to auto
				sections.forEach((section) => {
					section.style.height = `auto`;
				});

			}

		};

		// this is caled on resize
		const checkDeviceWidth = function() {

			// if device has changed it's width
			if(device_width != window.innerWidth) {

				// reset param
				device_width = window.innerWidth;

				// call the resize
				setSectionHeight();
				
			}

		};

		// first init on load
		setSectionHeight();

		// add listener to check for oriantiation changes
		window.addEventListener('resize', checkDeviceWidth);

	}

});