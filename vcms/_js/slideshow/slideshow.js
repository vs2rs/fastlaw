/* ------------------------------

	On load

------------------------------ */

// on dom ready
window.addEventListener('DOMContentLoaded', function(){

	// show or hide navigation
	const slidshowNavigation = function(slideshow, totalSlides) {

		const visibleSlides = slideshow.dataset.visibleSlides;

		if(totalSlides > visibleSlides) {
			slideshow.classList.remove("slideshow--no-nav");
		} else {
			slideshow.classList.add("slideshow--no-nav");
		}

	};

	// set active slides
	const slidshowSetActiveSlides = function(slides, visibleSlides) {

		// default is 0
		let firstSlideIndex = 0;

		// check if more then total
		if(slides.length > visibleSlides) {

			// go through and find index
			slides.forEach(function(slide, index){
				if(slide.dataset.active == 1) {
					firstSlideIndex = index;
				}
			});

		}

		// set the last slide index
		const lastSlideIndex = firstSlideIndex + (visibleSlides - 1);

		// set the count
		let activeSlideCount = 1;
		let checkActiveCount = 0;

		// setup active
		slides.forEach(function(slide, index){
			// check if it's in the interval
			if(index >= firstSlideIndex
			&& index <= lastSlideIndex) {
				checkActiveCount++;
				// set active
				slide.dataset.active = activeSlideCount++;
			} else {
				// remove active
				slide.removeAttribute("data-active");
			}
		});

		// now we need to check if all are set
		if(checkActiveCount < visibleSlides) {

			// update active count
			activeSlideCount = checkActiveCount + 1;

			// go through check indexes set active
			slides.forEach(function(slide, index){
				if(index <= visibleSlides - checkActiveCount - 1) {
					slide.dataset.active = activeSlideCount++;
				}
			});

		}

	};

	const setSlideCount = function(slideshow, slides) {

		// check if slide count needs to be changed on resize
		if(slideshow.dataset.slideCount === null
		|| slideshow.dataset.slideCount === undefined
		|| slideshow.dataset.slideCount === "") {
			return false;
		}

		// parse json info
		const slideCount = JSON.parse(slideshow.dataset.slideCount);

		// get the default count
		const defaultCount = slideshow.dataset.visibleSlides;

		// check the order
		// https://stackoverflow.com/a/1129270
		const compare = function(a, b) {
			if (a.width < b.width) {
				return -1;
			}
			if (a.width > b.width) {
				return 1;
			}
			return 0;
		}

		// call the compare function
		slideCount.sort(compare);

		const setCount = function(){

			// get the window width
			const windowWidth = window.innerWidth;

			// count var
			let count = false;

			// get the first match
			slideCount.forEach(function(resize){

				// do nothing if we found something
				if(count) { return false; }

				// check the window width
				if(windowWidth <= resize.width) {
					count = resize.count;
				}

			});

			// if nothing found set back to default count
			if(!count) { count = defaultCount; }

			// check if count has changed
			if(slideshow.dataset.visibleSlides != count) {

				// set how many slides are visible
				slideshow.dataset.visibleSlides = count;

				// set the slides
				slidshowSetActiveSlides(slides, count);

				// check if navigation is necessary
				slidshowNavigation(slideshow, slides.length);

			}

		};

		// call it
		setCount();

		// check if navigation is necessary
		slidshowNavigation(slideshow, slides.length);

		// add listener
		window.addEventListener('resize', setCount);
		window.addEventListener('orientationchange', setCount);

	};

	const setSlideshowHeight = function(slides){

		const setHeight = function() {

			// set the height var
			let getHeight = 0;

			// set the last slide index
			const lastSlideIndex = slides.length - 1;

			// go thorugh all the slides and get the largest
			slides.forEach(function(slide, index){

				const slideContent = slide.querySelector(".slide__content");

				if(slideContent !== null
				&& slideContent !== undefined) {
				
					getHeight = slideContent.scrollHeight > getHeight ? slideContent.scrollHeight : getHeight;

					if(index == lastSlideIndex) {
						slide.parentNode.style.height = getHeight + 'px';
					}

				}

			});

		}

		setHeight();

		// add listener
		window.addEventListener('resize', setHeight);
		window.addEventListener('orientationchange', setHeight);

	};

	// get all the slideshows
	const slideshows = document.querySelectorAll(".slideshow");

	if(slideshows !== null
	&& slideshows !== undefined
	&& slideshows.length > 0) {

		slideshows.forEach(function(slideshow){

			// get the visible slides count
			let visibleSlides = parseInt(slideshow.dataset.visibleSlides);

			// get the slides
			const slides = slideshow.querySelectorAll(".slide");

			// check if slides count is less then visible
			if(slides.length < visibleSlides) {
				// update slideshow to the new count
				slideshow.dataset.visibleSlides = slides.length;
				// update the variable
				visibleSlides = slides.length;
			}

			// check nav
			slidshowNavigation(slideshow, slides.length);

			// set the active slides
			slidshowSetActiveSlides(slides, visibleSlides);

			// setup slide count
			setSlideCount(slideshow, slides);

			// setup slide count
			if(slideshow.classList.contains("slideshow--set-height")) {
				setSlideshowHeight(slides);
			}

		});

	}

});





/* ------------------------------

	Slideshow
	Slide navigation

------------------------------ */

var slideshowAnimationInProgress = false;

function slideshow(nav, element, slideshow = false) {

	// if animation in progress cancel everything
	if(slideshowAnimationInProgress) {
		return;
	}
	
	// get the parent
	const findParent = function(element, parent_class) {
		while ((element = element.parentElement) && !element.classList.contains(parent_class));
		return element;
	}

	// set current slideshow if not already set
	if(!slideshow) {
		slideshow = findParent(element, "slideshow");
	}
	const visibleSlides = parseInt(slideshow.dataset.visibleSlides);

	// set slides and classes
	const slides = slideshow.getElementsByClassName("slide");

	// array to collect active slides
	const activeSlides = [];

	// go through the slides
	for (let i = 0; i < slides.length; i++) {
		// find active slide
		if(slides[i].dataset.active !== undefined) {
			activeSlides.push(i);
		}
	}

	// get active slide based on data set value
	// check - is the data set value to check for
	const getActiveSlide = function(check) {
		for (let i = 0; i < activeSlides.length; i++) {
			if(slides[activeSlides[i]].dataset.active == check) {
				return activeSlides[i];
			}
		}
	}

	// slide next and active for calculations
	let slide_next = false;
	let slide_active = 0;

	// if next
	if(nav == "next") {

		// get active slide
		slide_active = getActiveSlide(1); 

		// check value
		if(slide_active + visibleSlides == slides.length) {
			slide_next = 0;
		} else if(slide_active + visibleSlides > slides.length) {
			slide_next = getActiveSlide(visibleSlides) + 1;
		}

		// set slide next
		slide_next = slide_next !== false ? slide_next : slide_active + visibleSlides;

	}

	// if prev
	if(nav == "prev") {

		// get active slide
		slide_active = getActiveSlide(visibleSlides);
		
		// check value
		if(slide_active - visibleSlides == -1) {
			slide_next = slides.length - 1;
		} else if(slide_active - visibleSlides < -1) {
			slide_next = getActiveSlide(1) - 1;
		}

		// set slide next
		slide_next = slide_next !== false ? slide_next : slide_active - visibleSlides;

	}

	// if prev
	if(nav == "nav") {

		// no action if clicked on current active bullet
		if(element.hasAttribute("data-active")) {
			return;
		}

		// get active slide and all the bullets
		slide_active = getActiveSlide(visibleSlides);
		const bullets = element.parentNode.querySelectorAll("div");

		// go thourgh the bullets
		bullets.forEach(function(bullet, i){
			// if bullet matches clicked element set next
			if(bullet == element) { slide_next = i; }
			// else we remove the data-active attr
			else { bullet.removeAttribute("data-active"); }
		});

		// now check do we animate forward or backwards
		if(slide_next > slide_active) {
			nav = "next";
		} else {
			nav = "prev";
		}

	}

	// get the bullet div and check if it's there
	const slideshowBullets = slideshow.querySelector(".slideshow__bullets");
	if(slideshowBullets !== undefined && slideshowBullets !== null) {
		// run the update function
		slideshowUpdateBullets(slideshowBullets, slide_next);
	}

	// set next and active slide vars
	const nextSlide = slides[slide_next];
	const activeSlide = slides[slide_active];

	// add active class to next slide
	nextSlide.classList.add("slide--active");
	activeSlide.classList.remove("slide--active");

	// add the classes for the animation
	requestAnimationFrame(function() {

		// set the class on next item
		if(nav == "next" || nav == "nav") {
			nextSlide.dataset.active = visibleSlides + 1;
		} else if(nav == "prev") {
			nextSlide.dataset.active = 0;
		}

		// add the classes for the animation
		requestAnimationFrame(function() {

			// check what to do and do it
			if(nav == "next" || nav == "nav") {
				nextSlide.dataset.active = visibleSlides;
			} else if(nav == "prev") {
				nextSlide.dataset.active = 1;
			}

			// go through all the active slides
			for (let i = 0; i < activeSlides.length; i++) {
				// get slide data
				const slide = slides[activeSlides[i]];
				const slideNumber = parseInt(slide.dataset.active);
				// set data slide for animation
				if(nav == "next" || nav == "nav") {
					slide.dataset.active = slideNumber - 1;
				} else if(nav == "prev") {
					slide.dataset.active = slideNumber + 1;
				}
			}

			// we are animating
			slideshowAnimationInProgress = true;

			// create event listener for transition
		    nextSlide.addEventListener('transitionend', function(e) {
		    	// unset animation in progress
		    	slideshowAnimationInProgress = false;
		        // remove this event listener so it only gets triggered once
		        nextSlide.removeEventListener('transitionend', arguments.callee);
		        // remove data slide attr from current active slide so it's not active
			    activeSlide.removeAttribute('data-active');
		    });

		});

	});

};

const slideshowUpdateBullets = function(slideshowBullets, slide_next) {

	// get the classes
	const bulletClass = slideshowBullets.dataset.class;
	const bulletActiveClass = slideshowBullets.dataset.activeClass;
	
	// get the bullets
	const bullets = slideshowBullets.querySelectorAll("div");
	
	// run through and update class
	for (let i = 0; i < bullets.length; i++) {
		if(i == slide_next) {
			if(bulletClass !== undefined) bullets[i].classList.remove(bulletClass);
			if(bulletActiveClass !== undefined) bullets[i].classList.add(bulletActiveClass);
			bullets[i].dataset.active = 1;
		} else {
			if(bulletClass !== undefined) bullets[i].classList.add(bulletClass);
			if(bulletActiveClass !== undefined) bullets[i].classList.remove(bulletActiveClass);
			bullets[i].removeAttribute('data-active');
		}
	}

};


/* ------------------------------

	Set the height
	of the slideshow

------------------------------ */

// on dom ready
document.addEventListener('DOMContentLoaded', setSlideshowHeight);
window.addEventListener('resize', setSlideshowHeight);
window.addEventListener('orientationchange', setSlideshowHeight);

// the function
function setSlideshowHeight() {
		
	// get the slideshow
	const slideshow = document.querySelector(".set-slideshow-height-js");

	// check if it's there
	if(slideshow !== null && slideshow !== undefined) {

		// do this only on mobiles and tablets
		if(window.matchMedia("(pointer: coarse)").matches
		&& window.matchMedia("(hover: none)").matches) {

			// window width
			const windowHeight = document.body.clientHeight;
			const windowWidth = document.body.clientWidth;

			// get current window height
			const currentWindowWidth = slideshow.dataset.windowWidth;

			// set style only if width has changed
			if(currentWindowWidth != windowWidth) {

				// set container height
				slideshow.style.height = windowHeight + 'px';

				// save current width to data attr
				slideshow.dataset.windowWidth = windowWidth;

			}

		} else {

			// remove any style if not mobile
			slideshow.removeAttribute("style");

		}

	}

}