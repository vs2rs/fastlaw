<?php

require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'misc.functions.php');
debug_buffer('', 'Start of include');

# sanitize $_GET
array_walk_recursive($_GET, 'sanitize_get_var'); 

#Make a new CMS object
require(cms_join_path(dirname(__FILE__),'lib','classes','class.global.inc.php'));
require(cms_join_path(dirname(__FILE__),'lib','classes','class.cms_config.php'));

// create the cms object
global $gCms;
$gCms = new CmsObject();

if (isset($starttime))
{
  $gCms->set_variable('starttime',$starttime);
}

#Version
$CMS_VERSION = "1.9.4.3";
$CMS_VERSION_NAME = "Faanui";
$CMS_SCHEMA_VERSION = "34";
define('CMS_VERSION', $CMS_VERSION);
define('CMS_VERSION_NAME', $CMS_VERSION_NAME);
define('CMS_SCHEMA_VERSION', $CMS_SCHEMA_VERSION);

#Load the config file (or defaults if it doesn't exist)
require(cms_join_path(dirname(__FILE__),'lib','config.functions.php'));

#Grab the current configuration
$config = $gCms->GetConfig();

#Set the timezone
if( $config['timezone'] != '' )
  {
    @date_default_timezone_set(trim($config['timezone']));
  }

#Adjust the url stuff if we're using HTTPS
if( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' )
{
  // adjust the root url
//   if( !isset($config['ssl_url']) )
//   { 
//     $config['ssl_url'] = str_replace('http://','https://',$config['root_url']);
//   }
//   $config['root_url'] = $config['ssl_url'];
}
else if( startswith($config['root_url'],'https://') )
{
  // okay, not using SSL, but the root url is https...
  if( !isset($config['non_ssl_url']) )
    {
      $config['non_ssl_url'] = str_replace('https://','http://',$config['root_url']);
    }
  $config['root_url'] = $config['non_ssl_url'];

  if( !isset($config['non_ssl_uploads_url']) )
    {
      $config['non_ssl_uploads_url'] = str_replace('https://','http://',$config['uploads_url']);
    }
  $config['uploads_url'] = $config['non_ssl_uploads_url'];
}

#Attempt to override the php memory limit
if( isset($config['php_memory_limit']) && !empty($config['php_memory_limit'])  )
  {
    ini_set('memory_limit',trim($config['php_memory_limit']));
  }

#Add users if they exist in the session
$gCms->set_variable('user_id','');
$gCms->set_variable('username','');
if (isset($_SESSION['cms_admin_user_id']))
{
  $gCms->set_variable('user_id',$_SESSION['cms_admin_user_id']);
}
if (isset($_SESSION['cms_admin_username']))
{
  $gCms->set_variable('username',$_SESSION['cms_admin_username']);
}

if ($config["debug"] == true)
  {
    @ini_set('display_errors',1);
    @error_reporting(E_ALL);
  }


debug_buffer('loading adodb');
require(cms_join_path(dirname(__FILE__),'lib','adodb.functions.php'));
load_adodb();
debug_buffer('loading page functions');
require_once(cms_join_path(dirname(__FILE__),'lib','page.functions.php'));
debug_buffer('loading content functions');
require_once(cms_join_path(dirname(__FILE__),'lib','content.functions.php'));
debug_buffer('loading translation functions');
require_once(cms_join_path(dirname(__FILE__),'lib','translation.functions.php'));
debug_buffer('loading php4 entity decode functions');
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'autoloader.php');

require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.pageinfo.inc.php');
$gCms->set_variable('pageinfo',new PageInfo());

debug_buffer('done loading files');

#Load them into the usual variables.  This'll go away a little later on.
global $DONT_LOAD_DB;
if (!isset($DONT_LOAD_DB))
{
  $gCms->GetDb();
}

#Stupid magic quotes...
// if(get_magic_quotes_gpc())
// {
//     stripslashes_deep($_GET);
//     stripslashes_deep($_POST);
//     stripslashes_deep($_REQUEST);
//     stripslashes_deep($_COOKIE);
//     stripslashes_deep($_SESSION);
// }

#Fix for IIS (and others) to make sure REQUEST_URI is filled in
if (!isset($_SERVER['REQUEST_URI']))
{
    $_SERVER['REQUEST_URI'] = $_SERVER['SCRIPT_NAME'];
    if(isset($_SERVER['QUERY_STRING']))
    {
        $_SERVER['REQUEST_URI'] .= '?'.$_SERVER['QUERY_STRING'];
    }
}

#Setup the object sent to modules
$gCms->set_variable('pluginnum',1);
if (isset($page))
{
  $gCms->set_variable('page',$page);
}

#Set a umask
// $global_umask = '022';
// if( $global_umask != '' )
// {
//   @umask( octdec($global_umask) );
// }

#Set the locale if it's set
#either in the config, or as a site preference.
if (isset($config['locale']) && $config['locale'] != '')
{
  $str = trim($config['locale']);
  $res = @setlocale(LC_ALL, $str);
  if( $res === FALSE )
    {
      debug_buffer('IMPORTANT: SetLocale failed');
    }
}
$frontendlang = get_site_preference('frontendlang');

if (isset($CMS_ADMIN_PAGE) || isset($CMS_STYLESHEET))
{
	#This will only matter on upgrades now.  All new stuff (0.13 on) will be UTF-8.
	if (is_file(cms_join_path(dirname(__FILE__),'lib','convert','ConvertCharset.class.php')))
	{
		include(cms_join_path(dirname(__FILE__),'lib','convert','ConvertCharset.class.php'));
		$gCms->set_variable('convertclass',new ConvertCharset());
	}
}

#Load all installed module code
if (! isset($CMS_INSTALL_PAGE))
  {
    $modload =& $gCms->GetModuleLoader();
    $modload->LoadModules(isset($LOAD_ALL_MODULES), !isset($CMS_ADMIN_PAGE));
  }


function sanitize_get_var(&$value, $key)
{
  if (version_compare(phpversion(),"5.3.0","<")) {
    $value = eregi_replace('\<\/?script[^\>]*\>', '', $value);
  } else {
    $value = preg_replace('/\<\/?script[^\>]*\>/i', '', $value); //the i makes it caseinsensitive
  }
}

# vim:ts=4 sw=4 noet
?>
