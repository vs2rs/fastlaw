<?php

/* --------------------------------- *\
 
	Get host name

\* --------------------------------- */

/**
 * @source https://stackoverflow.com/a/14701491
 */

if(isset($_SERVER['HTTP_X_FORWARDED_HOST'])
&& $host = $_SERVER['HTTP_X_FORWARDED_HOST']) {
	
	$host = explode(',', $host);
	$host = trim(end($host));

} else {

	if(!$host = $_SERVER['HTTP_HOST']) {
		
		if(!$host = $_SERVER['SERVER_NAME']) {
			$host = !empty($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : '';
		}

	}

}

if(isset($host) && $host) {

	// get the port
	preg_match("/:\d+$/", $host, $port);
	$port = isset($port[0]) ? $port[0] : '';

	// Remove port number from host
	$host = preg_replace('/:\d+$/', '', $host);
	$host = trim($host);

} else {

	$host = false; // need to set up manualy // todo: get from config
	if(!$host) die("No hostname found.");

}

// define the server name
define("APP_HOSTNAME", $host);
define("APP_PORT", $port);

// remove vars
unset($port);
unset($host);





// -- vcsm_setup_host.php --