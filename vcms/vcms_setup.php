<?php

// set the include folder name
define("VCMS_INCLUDE_FOLDER", "/_includes");

// load the helper functions
require_once(VCMS_PATH . VCMS_INCLUDE_FOLDER . '/_helpers.inc.php');

/* --------------------------------- *\
 
	Find the project dir

\* --------------------------------- */

// get the parent from app name
list($app_parent_dir, $app_name) = array_pad(split_url(APP_NAME), 2, '');

// check if app name is empty if so use parent dir as name
if($app_name == '') $app_name = $app_parent_dir;

// add slash before parent
if(!preg_match('/^\//', $app_parent_dir)) $app_parent_dir = '/' . $app_parent_dir;

// where to search for folders
$directories = array(
	APP_PATH . '/vcms_' . $app_name,
	dirname(VCMS_PATH) . '/vcms_' . $app_name,
	dirname(VCMS_PATH) . '/vcms__projects/vcms_' . $app_name,
	dirname(VCMS_PATH) . '/vcms__projects' . $app_parent_dir . '/vcms_' . $app_name
);

// go through paths and check them 
foreach ($directories as $project_dir) {
	if(is_dir($project_dir)) break;
}

// define project path
define("PROJECT_PATH", $project_dir);

// echo PROJECT_PATH;

unset($directories);
unset($app_parent_dir);
unset($project_dir);

/* --------------------------------- *\
 
	Set the config file

\* --------------------------------- */

// where to search for the config file
$config_file_name = '/config.php';
$config_locations = array(
	APP_PATH . $config_file_name,
	APP_PATH . '/vcms_' . $app_name . $config_file_name,
	PROJECT_PATH . $config_file_name
);

// go through paths and check them
$config_file_found = false;
foreach ($config_locations as $config_file) {
	if(file_exists($config_file)) {
		$config_file_found = $config_file;
		break;
	}
}

// is it there?
if($config_file_found) define("APP_CONFIG_FILE", $config_file);
else die("No config.php file found.");

// load the config class and init
require_once(VCMS_PATH . VCMS_INCLUDE_FOLDER . '/class.Config.php');
require_once(VCMS_PATH . VCMS_INCLUDE_FOLDER . '/class.Session.php');
require_once(VCMS_PATH . VCMS_INCLUDE_FOLDER . '/class.vcmsAuth.php');

unset($config_file);
unset($app_name);

/* --------------------------------- *\
 
	Define app URL

\* --------------------------------- */

if(Config::read('domain_allowed')) {
	define("APP_URL", '//' . APP_HOSTNAME . APP_PORT . APP_FOLDER);
} else {
	define("APP_URL", '//' . Config::read('domain_default') . APP_PORT . APP_FOLDER);
}

/* --------------------------------- *\
 
	Definitions

\* --------------------------------- */

define("APP_VIEWS", APP_PATH . Config::read('folder_views'));
define("APP_UPLOADS", Config::read('folder_uploads'));
define("MINIFY_HTML", Config::read('minify_html'));

// add current url to config
$url = split_url(strip_tags($_SERVER['REQUEST_URI']), APP_FOLDER);
Config::write('url', $url);

/* --------------------------------- *\
 
	Load required functions and classes

\* --------------------------------- */

// load the config class and init
require_once(VCMS_PATH . VCMS_INCLUDE_FOLDER . '/class.LoadOperations.php');

// get from config
Load::functions(Config::read('functions'));
Load::classes(Config::read('classes'));

/* --------------------------------- *\
 
	vcms - loading admin files

\* --------------------------------- */

if(isset($url[0]) && $url[0] == "v"
&& isset($url[1]) && $url[1] == "cms") {

	// get the function to load the file
	require_once(VCMS_PATH . VCMS_INCLUDE_FOLDER . '/function.get_admin_file.php');

	// setup some constants we need for admin
	define('CONFIG_FILE_LOCATION', APP_CONFIG_FILE);

	// check for login data
	if(isset($_POST['vcms_username'])
	&& isset($_POST['vcms_password'])) {

		// do the login
		vcmsAuth::login(
			$_POST['vcms_username'],
			$_POST['vcms_password']
		);

	}

	// check for 2fa code
	if(isset($_POST['vcms_2fa_code'])) {
		vcmsAuth::login_2fa($_POST['vcms_2fa_code']);
	}

	// default admin file
	$admin_file = '/admin/index.php';

	// check for specific file in url
	if(count($url) > 2) {
		
		// admin directory
		$admin_file = '/admin';

		// go through url
		foreach ($url as $key => $u) {

			// add php at the end for admin php files
			if(in_array($u, $admin_files)) $u = "$u.php";

			// create the file path
			if($key > 1) $admin_file .= "/" . $u;

		}

	}

	// check the file and output it
	get_admin_file($admin_file);

	exit;

	// todo error page if somthing else requested

}

/* --------------------------------- *\
 
	vcms - loading glogal js file

\* --------------------------------- */

if(isset($url[0]) && $url[0] == "assets"
&& isset($url[1]) && ($url[1] == "js" || $url[1] == "css")
&& isset($url[2]) && isset($url[3])) {

	// set the js file
	$js_file = VCMS_PATH . "/_js/" . $url[2] . "/" . $url[3];
	
	
	// load it if it exists and exit
	if(file_exists($js_file)) {
		
		// extension
		$extension = pathinfo($js_file, PATHINFO_EXTENSION);

		if($extension == "js") {
			header("Content-type: application/javascript");
		}

		if($extension == "css") {
			header("Content-type: text/css");
		}

		readfile($js_file);
		exit;
		
	}

}





// -- vcsm_setup.php --