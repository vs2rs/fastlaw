<?php

/* --------------------------------- *\
 
	Helpers

\* --------------------------------- */

/**
 * ------------------------------------
 * Get the current project name
 * @return string
 * ------------------------------------
 */

function app_name() {
	$split_name = split_url(APP_NAME);
	return isset($split_name[1]) ? $split_name[1] : $split_name[0];
}

/**
 * ------------------------------------
 * Get user constants
 * @return array
 * ------------------------------------
 */

function get_user_constants() {
	return get_defined_constants(true)['user'];
}

function user_constants() {
	return print_it(get_defined_constants(true)['user']);
}

/**
 * ------------------------------------
 * Split URL function
 * @param $url - url to split
 * @param $remove - part to remove
 * ------------------------------------
 */

function split_url($url, $remove = false) {

	// default is empty array
	$result = array();

	// remove query string
	list($url) = explode('?', $url);

	// remove slashes from begining and end
	$url = preg_replace(array("/^\//", "/\/$/"), '', $url);

	// check if we need to remove somting from the url
	if($remove) {

		// remove trailing slash
		$remove = preg_replace(array("/^\//", "/\/$/"), '', $remove);

		// remove it from the url
		$url = str_replace($remove, '', $url);

	}

	// setupu
	foreach(explode('/', $url) as $part) {
		if(strlen($part) > 0) $result[] = $part;
	}

	return $result;

}

/**
 * ------------------------------------
 * Prints out an array with <pre>
 * @return void
 * ------------------------------------
 */

function print_it($array) {
	echo '<pre style="background:#c0c0c0; font-family:monospace; font-size:10px; color:#000; line-height:12px; position:relative; z-index:20001; padding: 20px;">';
		print_r($array);
	echo '</pre>';
}

/**
 * Backtrace file that called a function
 * @return array - info form debug_backtrace()
 */

function backtrace_file() {
	
	// get the last one
	$debug = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
	$result = end($debug);

	return array(
		$result['file'],
		$result['line'],
		$result['function'] != 'php_error_log' ? $result['function'] : false
	);
}

/**
 * Custom error log function
 * @param string $message text message
 * @return void
 */

function php_error_log($message) {

	// get the file info
	$info = backtrace_file();

	// update message (3 param is the function name)
	$message = ( $info[2] ? '{{function}}() ' : '' ) . $message . ' in {{file}} on line {{line}}';

	error_log(str_replace(
		array("{{file}}", "{{line}}", "{{function}}"), $info, $message
	), 0);

}

/**
 * Format date helper
 * @param $date date to format
 * @param $pattern format pattern
 * @return datetime formated
 */

function format_date($date, $pattern) {

	// format date
	$time = new DateTime($date);

	// return formated
	return $time->format($pattern);

}

/**
 * Output json
 * @param $response - array to encode
 * @return void
 */

function json_output($response) {

	// set header
	header('Content-type: application/json; charset=utf-8');

	// encode json
	$response = json_encode($response);

	// output
	echo $response; exit;

}

/**
 * Output json
 * @param $response - array to encode
 * @return void
 */

function update_filename($file) {

	// we need munge function to rename the files
	require_once(VCMS_PATH . "/lib/misc.functions.php");

	// get extension
	$extension = strtolower(pathinfo($file, PATHINFO_EXTENSION));

	// reset jpeg to jpg
	if($extension == "jpeg") $extension = "jpg";

	// create new filename and return it
	return munge_string_to_url(pathinfo($file, PATHINFO_FILENAME)) . "." . $extension;

}

/**
 * Output json
 * @param $response - array to encode
 * @return void
 */

function line_breaks($text) {
	return str_replace("\n", '<span class="line-break"></span>', $text);
}

/**
 * Split up email in too parts
 * @param $email - address
 * @param $return - string - what to return - array or a:href tag
 * @return array of parts
 * @source https://stackoverflow.com/a/41566570
 * @source https://stackoverflow.com/a/36297137
 */
function crypted_mail($email, $return = 'array') {

	// check if valid email
	if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		return $return == 'tag' ? '' : false;
	}

	// split up email
	$name   = explode('@', $email);
	$domain = array_pop($name);
	
	// get the .tld from the domain name
	// this wont work for example with .co.uk
	// so if that is needed then this function need update
	$url = 'https://' . $domain;
	$url = explode(".", parse_url($url, PHP_URL_HOST));
	$tld = end($url);

	// remove the tld from domain
	$domain = str_replace('.'.$tld, '', $domain);

	// set onclick script
	$onclick = 'window.location.href = \'mailto:\' + this.dataset.name + \'@\' + this.dataset.domain + \'.\' + this.dataset.tld; return false;';
	$onclick = '';

	// // check if tag
	if($return == 'tag') {

		// return the tag
		return '<a class="cryptedmail" href="#mailgo"' .
			   'data-address="'.$name[0].'"' .
			   'data-domain="'.$domain.'.'.$tld.'"' .
			   'data-tld="'.$tld.'"' .
			   'onclick="'.$onclick.'">' .
			   '</a>';

	} else {

		// return array
		return array('name' => $name[0], 'domain' => $domain, 'tld' => $tld, 'onclick' => $onclick);

	}

}

/**
 * Get a:mailto and replace them with crypted tags
 * @param $text - html string
 * @return html string
 * @source 
 */
function encrypt_email_links($text) {

	// new dom
	$doc = new DomDocument();
	// $doc->loadHTML($text);
	$doc->loadHTML('<?xml encoding="utf-8" ?>' . $text);

	// get the a tags
	foreach ($doc->getElementsByTagName("a") as $a) {

		// check for href attr
		if($a->hasAttribute("href")) {

			// check if it's mailto
			$href = trim($a->getAttribute("href"));
			if(strtolower(substr($href, 0, 7)) === 'mailto:') {

				// get the email parts
				$email = crypted_mail($a->nodeValue);
				
				// create a new link
				$link = $doc->createElement('a');
				$link->setAttribute('class', 'cryptedmail');
				$link->setAttribute('href', '#mailgo');
				$link->setAttribute('data-address', $email['name']);
				$link->setAttribute('data-domain', $email['domain'] . '.' . $email['tld']);
				$link->setAttribute('data-tld', $email['tld']);
				$link->setAttribute('onclick', $email['onclick']);

				// replace the old with the new
				$a->parentNode->replaceChild($link, $a);

			}
		
		}

	}

	// return our updated html
	return $doc->SaveHTML();

}

/**
 * Remove tags and encode special chars
 * @param $text - html string
 * @return string - clean
 */
function clean_up_string($text) {
	$text = strip_tags($text);
	$text = htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
	return $text;
}

/**
 * Check if default lang from the config file
 * @param $lang - string
 * @return bool
 */
function is_default_lang($lang = false) {

	// use current if not set
	if(!$lang) $lang = Config::read('lang_current');

	// check if default and return results
	return $lang == Config::read('lang_default');

}

/**
 *  Check if input string is a valid YouTube URL
 *  and try to extract the YouTube Video ID from it.
 *  @author  Stephan Schmitz <eyecatchup@gmail.com>
 *  @source    https://stackoverflow.com/a/10527599
 *  @param   $url  => string The string that shall be checked.
 *  @return  mixed => Returns YouTube Video ID, or (boolean) false.
 */        
function parse_youtube_url($url) {

	$pattern = '#^(?:https?://)?';    # Optional URL scheme. Either http or https.
	$pattern .= '(?:www\.)?';         #  Optional www subdomain.
	$pattern .= '(?:';                #  Group host alternatives:
	$pattern .=   'youtu\.be/';       #    Either youtu.be,
	$pattern .=   '|youtube\.com';    #    or youtube.com
	$pattern .=   '(?:';              #    Group path alternatives:
	$pattern .=     '/embed/';        #      Either /embed/,
	$pattern .=     '|/v/';           #      or /v/,
	$pattern .=     '|/watch\?v=';    #      or /watch?v=,    
	$pattern .=     '|/watch\?.+&v='; #      or /watch?other_param&v=
	$pattern .=   ')';                #    End path alternatives.
	$pattern .= ')';                  #  End host alternatives.
	$pattern .= '([\w-]{11})';        # 11 characters (Length of Youtube video ids).
	$pattern .= '(?:.+)?$#x';         # Optional other ending URL parameters.
	preg_match($pattern, $url, $matches);

	// return id
	return (isset($matches[1])) ? $matches[1] : false;

}

/**
* Get Vimeo video id from url
* @source https://gist.github.com/anjan011/1fcecdc236594e6d700f
* 
* @param string $url The URL
* @return string the video id extracted from url
*/

function parse_vimeo_url($url = '') {

	$regs = array();

	$id = '';

	if (preg_match('%^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)(?:[?]?.*)$%im', $url, $regs)) {
		$id = $regs[3];
	}

	return $id;

}





// -- _helpers.inc.php --