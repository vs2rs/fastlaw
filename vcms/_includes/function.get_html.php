<?php

/**
 * Get HTML file
 * @param array $params list to replace in template
 * @return string - html file or empty string if no file
*/
function get_html($params) {

	// check if template set
	if(isset($params['template'])) {
		$template = $params['template'];
		unset($params['template']);
	} else {
		php_error_log("template not set");
	}

	// is the template set?
	if(isset($template) && file_exists($template)) {

		// get the template
		$template = file_get_contents($template);
		$template = process_template_vars($template, $params);

		// minify our html output
		if(Config::read('minify_html')) {

			$search = array(
				'/\>[^\S ]+/s',     // strip whitespaces after tags, except space
				'/[^\S ]+\</s',     // strip whitespaces before tags, except space
				'/(\s)+/s',         // shorten multiple whitespace sequences
				'/<!--(.|\s)*?-->/' // Remove HTML comments
			);

			$replace = array('>','<','\\1','');

			$template = preg_replace($search, $replace, $template);
			$template = str_replace('" />', '"/>', $template);

		}

		// results
		return $template;

	}

	// no file found
	php_error_log("file $template not found");

	// return empty
	return '';

}





// -- function.get_html.php --