<?php

class Config {

	// instance
	private static $_instance;
	private static $_config;

	/**
	 * Get an instance of the Database
	 * @return Instance
	*/
	public static function getInstance() {
		if(!self::$_instance) { // If no instance then make one
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Constructor - get config.php
	 * @return array - config params
	*/
	private function __construct() {

		// check if config file exists
		if(file_exists(APP_CONFIG_FILE)) {

			// array of data
			$config = array();
			
			// include
			include(APP_CONFIG_FILE);

			// set to config var
			self::$_config = $config;

		} else {

			// no file
			die("No config.php file!");

		}

	}

	/**
	 * Read a param
	 * @param string $name - param name
	 * @return mixed - our param value from config.php or false
	*/
	public static function read($name) {

		// is string
		if(!is_string($name)) die('Config param must be string value.');

		// instance
		$instance = self::getInstance();
		$config = $instance::$_config;

		// is the param set?
		if(isset($config[$name])) return $config[$name];

		// nothing found
		return false;

	}

	/**
	 * Read a param
	 * @param string $name - param name
	 * @param mixed $value - whatever we need to set
	 * @return void
	*/
	public static function write($name, $value) {

		// is string
		if(!is_string($name)) die('Config param must be string value.');

		// set our new param
		$instance = self::getInstance();
		$instance::$_config[$name] = $value;

	}

	/**
	 * Read a param
	 * @param string $name - param name
	 * @return void
	*/
	public static function delete($name) {
		
		// instance
		$instance = self::getInstance();

		// is the param set?
		if(isset($instance::$_config[$name])) {
			unset($instance::$_config[$name]);
		}

	}

	/**
	 * Print out our config.php
	 * @return void
	*/
	public static function print() {
		$instance = self::getInstance();
		print_it($instance::$_config);
	}

}





?>