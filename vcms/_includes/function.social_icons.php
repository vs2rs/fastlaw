<?php

/**
 * Get SVG code from file
 * @param string $url - location of the svg element
 * @return string - svg tag or empty string if no file
*/
function social_icons($params = array()) {

	// check what to return, default html
	$return = 'html';
	if(isset($params['return'])
	&& $params['return'] == 'array') {
		$return = 'array';
	}

	// db
	$db = Database::getInstance();

	// array and output
	$items = array();
	$items_html = '';

	// query
	$db_query = "SELECT A.link, A.network as name
	FROM {{prefix}}module_socialnetworks_items A
	WHERE A.active = 1 ORDER BY A.item_order";

	// check results
	if($result = $db->getRows($db_query)) {

		// check for icon type
		$icon = '';
		if(isset($params['icon_type']) && $params['icon_type'] == 'circle') {
			$icon = '-circle';
		}

		// run true results
		while ($row = $result->fetch_object()) {
			$row->svg = VCMS_PATH . '/admin/templates/social/' . $row->name . $icon . '.svg';
			$items[] = $row;
		}

		// go thorugh properties
		foreach ($items as $network) {

			// get the svg
			$network->svg = get_svg(
				$network->svg,
				( isset($params['icon_class']) ? $params['icon_class'] : false )
			);

			// setup template
			$html_template = array(

				// params
				'name'		=> $network->name,
				'icon'		=> $network->svg,
				'link'		=> $network->link,
				
				// template
				'template'	=> $params['template']

			);

			// check if extra params set for template
			if(isset($params['template_params'])
			&& count($params['template_params']) > 0) {
				foreach ($params['template_params'] as $key => $param) {
					$html_template[$key] = $param;
				}
			}

			// setup propertie html
			$items_html .= get_html($html_template);

		}

	}

	// return our icons
	return $return == 'html' ? $items_html : $items;

}





// -- function.get_svg.php --