<?php

// TODO: will this shit work?
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// PHPMailer:
// https://github.com/PHPMailer
require_once VCMS_PATH . '/vendor/PHPMailer/src/PHPMailer.php';
require_once VCMS_PATH . '/vendor/PHPMailer/src/Exception.php';
require_once VCMS_PATH . '/vendor/PHPMailer/src/SMTP.php';

// we need munge function to rename the files
require_once(VCMS_PATH . "/lib/misc.functions.php");

/**
 * Send e-mail
 * @param array $params - email params like send to, from etc.
 * @param string $message - message to be sent in the email
 * @return bool - all good all bad
*/
function send_mail($params = array(), $message, $files = false) {

	// Create a new PHPMailer instance
	$mail = new PHPMailer;

	// set char set
	$mail->CharSet = "UTF-8";

	// if we are on the local server use my email
	if(APP_HOSTNAME == 'localhost') {
		$email_to = Config::read("email_to");
	}

	// setup email address
	$email_to 			= isset($params['email_to']) ? $params['email_to'] : false;
	$email_from 		= isset($params['email_from']) ? $params['email_from'] : false;
	$email_from_name 	= isset($params['email_name']) ? $params['email_name'] : false;

	// from
	$mail->setFrom($email_from, $email_from_name);

	// to
	$mail->addAddress(str_replace(' ', '', $email_to));

	// subject
	$mail->Subject = isset($params['subject']) ? $params['subject'] : false;

	// read an HTML message body from an external file,
	// convert referenced images to embedded,
	// convert HTML into a basic plain-text alternative body
	$mail->msgHTML($message);

	// attach the files
	if($files) {

		// go through files and attach
		foreach ($files['name'] as $key => $file) {

			// attach
			$mail->addAttachment(
				$files['tmp_name'][$key],
				munge_string_to_url(pathinfo($file, PATHINFO_FILENAME))
				. "." . pathinfo($file, PATHINFO_EXTENSION)
			);

		}

	}

	// check if we are using smtp
	if(Config::read("email_smtp")) {
		$mail->isSMTP();
		$mail->Host 		= Config::read("email_host");
		$mail->Port 		= Config::read("email_port");
		$mail->SMTPSecure 	= Config::read("email_secure");
		$mail->SMTPAuth 	= Config::read("email_auth");
		$mail->Username 	= Config::read("email_user");
		$mail->Password 	= Config::read("email_pass");
	}

	// check for debug
	if(Config::read("email_debug")) {
		
		// 0 - off
		// 1 - client msgs
		// 2 - cl and serv msgs

		$mail->SMTPDebug = Config::read("email_debug");
		$mail->Debugoutput = "error_log";

	}

	// send the message, check for errors
	if($mail->send()) {
	    return true;
	}

	error_log("Mail not sent!");

	return false;

}





// -- function.send_mail.php