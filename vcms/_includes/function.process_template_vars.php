<?php

/**
 * Get SVG code from file
 * @param string $template - our html template
 * @return string - updated $template
*/
function process_template_vars($template, $params) {

	// get everyting between {{tag}} ... {{/tag}}
	preg_match_all("/{{(.*?)}}(.*?){{\/\\1}}/s", $template, $result);

	// run the clean up
	foreach ($result[0] as $key => $tag) {

		// set tag name and content
		$tag_name = $result[1][$key];
		$tag_content = $result[2][$key];

		// check if tag name is set and is true
		if(isset($params[$tag_name]) && $params[$tag_name] !== false) {
			$template = str_replace($tag, $tag_content, $template);
		} else {
			$template = str_replace($tag, '', $template);
		}

	}

	// colloect the remaining tags
	preg_match_all("/{{(.*?)}}/", $template, $results);
	$tags = array_unique($results[0]);
	$tag_names = array_unique($results[1]);

	// replace the tags
	foreach ($tags as $key => $tag) {

		if(is_array($tag)) { return ''; }

		if(is_array($template)) { return ''; }

		if(isset($params[$tag_names[$key]])
		&& is_array($params[$tag_names[$key]])) {
			return $tag_names[$key];
		}

		if(isset($params[$tag_names[$key]])) {
			$template = str_replace(
				$tag, $params[$tag_names[$key]], $template
			);
		}
	}

	return $template;

}