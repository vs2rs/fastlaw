<?php

/**
 * Static class for input fields
 * @author vs2rs
*/

class Form {

	// other colors and classes
	public static $color_input = 'white';
	public static $color_error = 'error';
	public static $color_required = 'error';
	public static $class_label = '';
	public static $class_button = '';
	public static $class_button_file = 'button';
	public static $class_comment = '';

	/**
	 * Validate our input
	 * @param $name - string - name of the input
	 * @param $value - mixed - value of the input (string, number)
	 * @param $params - array of all the input params
	*/
	public static function validate($name, $value, $params) {

		// check for required
		$required = isset($params['required']) && $params['required'] ? true : false;

		// param for unsuported file types
		$unsuported_file_type = false;

		// update label
		// check for parent name if this is a part
		// of a array inputs like radio buttons
		$label = isset($params['extra']['parent_name'])
			   ? $params['extra']['parent_name']
			   : $params['label'];

		// check if empty
		if($required && $value == '') {

			// check for custom error message
			// TODO: make custom errors
			if(isset(static::$custom_errors[Config::read('lang_current')][$name . '_required'])) {
				$message = static::$custom_errors[Config::read('lang_current')][$name . '_required'];
			} else if(isset($params['validate']) && $params['validate'] == 'checked') {
				$message = self::message('not_checked');
			} else {
				$message = self::message('required');
			}

			// return error message
			return str_replace('{{name}}', ' "' . $label . '"', $message);
			
		}

		// validate length
		if(isset($params['length']) && $value != ''
		&& mb_strlen($value, 'UTF-8') > $params['length']) {

			// check for custom error message
			// TODO: make custom errors
			if(isset(static::$custom_errors[Config::read('lang_current')][$name . '_too_long'])) {
				$message = static::$custom_errors[Config::read('lang_current')][$name . '_too_long'];
			} else {
				$message = self::message('too_long');
			}

			return str_replace(
				array('{{name}}', '{{chars}}'),
				array(' "' . $label . '"', $params['length']),
				$message
			);

		}

		// check validate param
		if(isset($params['validate']) && $value != '') {

			// check if validate is an array
			// if so, then this is a lisit of valid values
			if(is_array($params['validate'])) {

				// check if the value in array
				if(!in_array($value, $params['validate'], true)) {

					// check for custom error message
					// TODO: make custom errors
					if(isset(static::$custom_errors[Config::read('lang_current')][$name . '_required'])) {
						$message = static::$custom_errors[Config::read('lang_current')][$name . '_required'];
					} else {
						$message = self::message('required');
					}
						
					// return error message
					return str_replace('{{name}}', ' "' . $label . '"', $message);

				}

				// all good no errors found
				return false;

			}

			if(!validate_string($value, $params['validate'])) {

				// check for custom error message
				// TODO: make custom errors
				if(isset(static::$custom_errors[Config::read('lang_current')][$name . '_not_valid'])) {
					
					$message = static::$custom_errors[Config::read('lang_current')][$name . '_not_valid'];

				} else {
					
					// check what we validate
					switch ($params['validate']) {
						case 'number':
							$message = 'not_number';
							break;

						case 'email':
							$message = 'not_email';
							break;
						
						default:
							$message = 'not_valid';
							break;
					}

					$message = self::message($message);

				}

				return str_replace('{{name}}', ' "' . $label . '"', $message);

			}

		}

		// if we are validating files
		if($params['input_type'] == 'input_file' && $_FILES[$name]['name'] != '') {
			
			// set some stuff up
			$files = $_FILES[$name];
			$file_types = explode(",", $params['extra']['file_types']);
			$file_size  = $params['extra']['file_size'];
			$file_size_post = 0;

			// first check if multiplie set
			if(isset($params['extra']['multiple']) && $params['extra']['multiple'] > 1) {

				// set params
				$file_count = $params['extra']['multiple'];
				
				// check if count larger
				if(count($files['name']) > $file_count) {
					return str_replace('{{count}}', $file_count, self::message('too_many_files'));
				}

				// get file sizes
				foreach ($files['name'] as $key => $file) {

					// check if file is set
					if($file != '') {

						// set size
						$file_size_post = $file_size_post + $files['size'][$key];

						// check file type
						if(!validate_file($files['tmp_name'][$key], $file_types)) {
							$unsuported_file_type = true;
						}

					}

				}

			} else {

				// set the file size for singe file upload
				$file_size_post = $files['size'];

				// check file type
				if(!validate_file($files['tmp_name'], $file_types)) {
					$unsuported_file_type = true;
				}

			}

			// check total file size
			if($file_size_post / 1024 / 1024 > $file_size) {
				return str_replace('{{size}}', $file_size, self::message('too_many_mb'));
			}

			// if we have a unsuported file type
			if($unsuported_file_type) {
				return self::message('unsupported');
			}

		}

		// no errors found
		return false;

	}

	/**
	 * Validate $_POST tada
	 * @param $valid_post_data - array - list of all valid post keys
	 * @param $errors - array - list of erros
	*/
	public static function validate_post($valid_post_data, $errors) {

		// go thorugh post keys and check if set
		foreach($_POST as $post_key => $post_value) {
			if(in_array($post_key, $valid_post_data)) $valid_post = true;
			else $valid_post = false;
		}

		// go thorugh files keys and check if set
		foreach($_FILES as $file_key => $file_value) {
			if(in_array($file_key, $valid_post_data)) $valid_post = true;
			else $valid_post = false;
		}

		// check if validate post is set
		if(isset($valid_post)) {
			if($valid_post && count($errors) == 0) {
				return true;
			} else {
				return false;
			}
		}

		// default
		return false;

	}

	/**
	 * Validate $_POST sent from javascript
	 * @param $post - array - $_POST data
	 * @return array - list of erros
	*/
	public static function validate_post_js($post, $inputs) {

		// set some arrays
		$errors = array();
		$post_data = array();

		// update post data array
		foreach ($post as $input) {
			$post_data[$input->name] = $input->value;
		}

		// setup valid input list
		foreach ($inputs as $input) {

			// validate basic inputs
			if($input['input_type'] != "input_file"
			&& $input['input_type'] != "input_checkbox") {

				$error = false;

				if(isset($post_data[$input['name']])) {

					$error = self::validate(
						$input['name'], $post_data[$input['name']], $input
					);

				} else {

					if(isset($input['required']) && $input['required']) {
						$error = self::validate(
							$input['name'], '', $input
						);
					}

				}

				if($error) {
					$errors[$input['name']] = $error;
				}

			}

			// validate checkboxes
			if($input['input_type'] == "input_checkbox") {

				// check if required
				if(isset($input['required'])
				&& $input['required']
				&& !isset($post_data[$input['name']])) {
					$error = self::validate($input['name'], '', $input);
					$errors[$input['name']] = $error;
				}

			}

		}

		// return errors array
		return $errors;

	}

	/**
	 * Validate if passwords match
	 * @param $post - array - $_POST data
	 * @param $errors - array - list of the errors
	 * @param pass1 - string - name of the first password field
	 * @param pass2 - string - name of the second password field
	 * @return bool
	*/
	public static function validate_new_password($post, $errors, $pass1, $pass2) {

		// default
		$password_error = false;

		// get the keys for password in the post
		$pass1_key = array_search($pass1, array_column($post, 'name'));
		$pass2_key = array_search($pass2, array_column($post, 'name'));

		// check if all is good
		if(!isset($errors[$pass1]) && !isset($errors[$pass2])
		&& md5($post[$pass1_key]->value) != md5($post[$pass2_key]->value)) {
			$password_error[$pass1] = '';
			$password_error[$pass2] = self::message("password_match");
		}

		// return the error
		return $password_error;

	}

	/**
	 * Create one file array from all submited file inputs
	 * @param $name - string - name of the post
	 * @param $post - array - list of post data
	 * @return mix - value found in the post
	*/
	public static function get_input_value($name, $post) {

		// go throuh post
		foreach ($post as $input_key => $input) {
			if($input->name == $name) return $input->value;
		}

		// if nothign found
		return false;

	}

	/**
	 * Get the selected option name
	 * @param $select - select input array with values
	 * @return string - option name
	*/
	public static function get_option_name($select) {

		// go throuh post
		foreach ($select['options'] as $option) {
			if($option['value'] == $select['value']) return $option['name'];
		}

		// if nothign found
		return '';

	}

	/**
	 * Create one file array from all submited file inputs
	 * @param $params - array of all the input params
	 * @return mix - array or bool
	*/
	public static function create_file_list($inputs) {

		// set our array
		$file_list = array(
			'name' => array(),
			'type' => array(),
			'tmp_name' => array(),
			'error' => array(),
			'size' => array(),
			'input_name' => array(),
		);

		// go through inputs
		foreach ($inputs as $input) {

			// check if file
			if($input['input_type'] == 'input_file') {

				// set the files
				$files = $input['value'];

				// check if multiply files or single file
				if(is_array($files['name'])) {

					// check if email array set
					foreach ($files as $key => $value) {
						$file_list[$key] = array_merge($file_list[$key], $value);
					}

					// add input name to the list
					foreach ($files['name'] as $file_name) {
						$file_list['input_name'][] = $input['name'];
					}

				} else {

					// setup keys
					foreach ($files as $key => $value) {
						$file_list[$key][] = $value;
					}

					// add input name
					$file_list['input_name'][] = $input['name'];

				}

			}

		}

		// check if any files found and return the array or false
		return count($file_list['name']) > 0 ? $file_list : false;

	}

	/**
	 * Label HTML
	 * @param $params - array of all the input params
	*/
	public static function label($params) {

		// set template
		$template = VCMS_PATH . '/_templates/form/other/label.html';
		if(isset($params['label_template'])) {
			$template = $params['label_template'];
		}

		// check if label set
		if(isset($params['label']) && $params['label']) {
			return get_html(array(

				'id'		=> $params['id'],
				'text'  	=> $params['label'],
				'class'		=> isset($params['label_class']) ? $params['label_class'] : static::$class_label,
				'required'  => isset($params['required']) ? $params['required'] : false,
				'required_color' => static::$color_required,

				// template
				'template'	=> $template

			));
		}

		// default is empty string
		return '';

	}

	/**
	 * Comment HTML
	 * @param $params - array of all the input params
	*/
	public static function comment($params) {

		// check if comment set and return html
		if(isset($params['comment']) && $params['comment']) {
			return get_html(array(
				'comment'  	=> $params['comment'],
				'class'		=> isset($params['comment_class']) ? $params['comment_class'] : static::$class_comment,
				'template'	=> VCMS_PATH . '/_templates/form/other/comment.html'
			));
		}

		// default is empty string
		return '';

	}

	/**
	 * Error HTML
	 * @param $params - array of all the input params
	*/
	public static function error($error) {

		// return error div
		return get_html(array(
			'error'  	=> $error ? $error : '',
			'template'	=> VCMS_PATH . '/_templates/form/other/error.html'
		));

	}

	/**
	 * Default Error Messages
	 * @param $param - string - name of the message to display
	*/
	public static function message($param) {

		$message = array(
			"en" => array(
				"required"   		=> "Field{{name}} is required!",
				"too_long"   		=> "Field{{name}} exceeds the maximum character length ({{chars}})",
				"not_valid"  		=> "Field{{name}} contains unsuported characters.",
				"not_number" 		=> "Field{{name}} can contain only numbers.",
				"not_email"  		=> "Please enter a valid e-mail address!",
				"files_added"		=> "{{count}} files selected",
				"choose" 			=> "Select",
				"choose_one" 		=> "Select file...",
				"choose_more" 		=> "Select files...",
				"too_many_files"	=> "Exceeded allowed file count ({{count}})",
				"too_many_mb"		=> "Exceeded allowed file size ({{count}})",
				"unsupported"		=> "Unsupported file type in the field.",
				"not_checked"		=> "This field needs to be checked.",
				"errors_found"		=> "Please correct the errors found in the form!",
				"send_error"		=> "Error sending the form!",
				"send_error_text"	=> "Try again later or contact us directly if you have any questions.",
				"password_match"	=> "Passwords don't match!",
			),
			"ru" => array(
				"required"   		=> "Поле{{name}} должно быть заполнено обязательно.",
				"too_long"   		=> "Поле{{name}} превышает допустимое количество символов ({{chars}})",
				"not_valid"  		=> "Поле{{name}} содержит не разрешенные символы.",
				"not_number" 		=> "Field{{name}} can contain only numbers.",
				"not_email"  		=> "Пожалуйста, введите правильный адрес эл. почты.",
				"files_added"		=> "{{count}} files selected",
				"choose" 			=> "Select",
				"choose_one" 		=> "Select file...",
				"choose_more" 		=> "Select files...",
				"too_many_files"	=> "Exceeded allowed file count ({{count}})",
				"too_many_mb"		=> "Exceeded allowed file size ({{count}})",
				"unsupported"		=> "Unsupported file type in the field.",
				"not_checked"		=> "This field needs to be checked.",
				"errors_found"		=> "Please correct the errors found in the form!",
				"send_error"		=> "Error sending the form!",
				"send_error_text"	=> "Try again later or contact us directly if you have any questions.",
				"password_match"	=> "Passwords don't match!",
			),
			"lv" => array(
				"required"   		=> "Lauks{{name}} ir obligāti jāaizpilda!",
				"too_long"   		=> "Lauks{{name}} pārsniedz atļauto simbolu skaitu ({{chars}})!",
				"not_valid"  		=> "Lauks{{name}} satur neatļautus simbolus.",
				"not_number" 		=> "Lauks{{name}} drīkst saturēt tikai veselus skaitļus.",
				"not_email"  		=> "Lūdzu ievadiet pareizu e-pasta adresi.",
				"files_added"		=> "Pievienoti {{count}} faili",
				"choose" 			=> "Izvēlēties",
				"choose_one" 		=> "Izvēlies failu...",
				"choose_more" 		=> "Izvēlies failus...",
				"too_many_files"	=> "Pārsniegts atļauto failu skaits ({{count}})",
				"too_many_mb"		=> "Pārsniegts atļautais failu izmērs ({{size}}mb)",
				"unsupported"		=> "Neatļauts faila formāts atrasts ievades laukā.",
				"not_checked"		=> "Šim laukam jābūt obligāti atzīmētam.",
				"errors_found"		=> "Aizpildot formu, atrastas kļūdas. Lūdzu pārbaudiet ievadīto informāciju!",
				"send_error"		=> "Kļūda nosūtot formu!",
				"send_error_text"	=> "Mēģiniet nosūtīt vēlreiz vai neskaidrību gadījumā sazinaties ar mums.",
				"password_match"	=> "Ievadītās paroles nesakrīt!",
			)
		);

		// set param to be returned
		if(isset($message[Config::read("lang_current")][$param])) {
			$result = $message[Config::read("lang_current")][$param];
		} else {
			$result = $message[Config::read("lang_default")][$param];
		}

		return $result;

	}

	public static function submit($params) {

		// get the html
		return get_html(array(

			'text'  	=> $params['text'],
			'class'		=> isset($params['class']) ? $params['class'] : static::$class_button,

			// template
			'template'	=> VCMS_PATH . '/_templates/form/other/submit.html'

		));

	}

	public static function input($params) {

		// set up name and id, use name if id not set
		$name = isset($params['name']) ? $params['name'] : '';
		$params['id'] = isset($params['id']) ? $params['id'] : ( $name != '' ? $name : false );
		$id = $params['id'];

		// some default values
		$value = '';
		$error = false;

		// value for hidden fields
		if($params['input_type'] == "hidden") {
			$value = isset($params['value']) ? $params['value'] : '';
			if(isset($_POST[$name])) {
				$value = $_POST[$name];
				$params['value'] = $value;
				$error = self::validate($name, $value, $params);
			}
		}

		// udpate value for input fields but skip files and checks
		if(isset($_POST[$name])
		&& $params['input_type'] != "input_file"
		&& $params['input_type'] != "input_checkbox"
		&& $params['input_type'] != "input_radio") {
			$value = $_POST[$name];
			$params['value'] = $value;
			$error = self::validate($name, $value, $params);
		}

		// update value for file inputs
		if($params['input_type'] == "input_file" && isset($_FILES[$id])) {
			$params['value'] = $_FILES[$id];
			$error = self::validate($id, $params['value'], $params);
		}

		// update checked status
		if($params['input_type'] == "input_checkbox") {
			$value = $params['value'];
			if(isset($_POST[$name])) {
				$params['extra']['checked'] = true;
			} else {
				if(isset($params['required']) && $params['required']
				&& isset($_POST) && count($_POST) > 0 && !isset($_POST[$name])) {
					$error = self::validate($name, '', $params);
				}
			}
		}

		// update checked status
		if($params['input_type'] == "input_radio") {
			$value = $params['value'];
			if(isset($_POST[$name])) {
				$params['extra']['checked'] = false;
				if($_POST[$name] == $value) {
					$params['extra']['checked'] = true;
				} else {
					// check the post value if it's in the list
					$error = self::validate($name, $_POST[$name], $params);
				}
			} else {
				if(isset($params['required']) && $params['required'] && isset($_POST[$name])) {
					$error = self::validate($name, '', $params);
				}
			}
		}

		// check if we need to use diferent template
		$template = VCMS_PATH . '/_templates/form/inputs/input-default.html';
		if(isset($params['template'])) $template = $params['template'];

		// set our params
		$html_params = array(

			// basic
			'id'			=> $id,
			'name'			=> $name,
			'value'			=> $value,
			'length'		=> isset($params['length']) ? $params['length'] : false,
			'class'			=> isset($params['class']) ? $params['class'] : '',
			'color'			=> isset($params['color']) ? $params['color'] : static::$color_input,

			// šo vajag noņemt nost, man ir nepareizs type zem input-default.html
			// tāpēc pagaidām uzliku šeit type, jāparbauda visās lapās un jāaizvāc
			'type'			=> $params['input_type'],

			// text
			'label'			=> self::label($params),
			'placeholder'	=> isset($params['placeholder']) ? $params['placeholder'] : false,
			'comment'		=> self::comment($params),

			// error
			'error_message'	=> self::error($error),
			'error'			=> $error,

			// template
			'template'		=> $template

		);

		// check if anything extra needs to be added
		if(isset($params['extra'])
		&& is_array($params['extra'])
		&& count($params['extra']) > 0) {

			// go thorugh params and add to the list
			foreach ($params['extra'] as $extra_param_name => $extra_param_value) {
				$html_params[$extra_param_name] = $extra_param_value;
			}

		}

		return array(
			'html' 		=> get_html($html_params),
			'params' 	=> $params,
			'error' 	=> $error
		);
		
	}

	public static function text($params) {
		return self::input($params);
	}

	public static function textarea($params) {
		
		// change template
		$params['template'] = VCMS_PATH . '/_templates/form/inputs/textarea.html';
		
		// get the input
		return self::input($params);

	}

	public static function input_number($params) {
		
		// change template
		$params['template'] = VCMS_PATH . '/_templates/form/inputs/input-number.html';
		
		// get the input
		return self::input($params);

	}

	public static function input_number_step($params) {
		
		// change template
		$params['template'] = VCMS_PATH . '/_templates/form/inputs/input-number-step.html';
		$params['extra'] = array(
			'step' => isset($params['step']) ? $params['step'] : true
		);
		
		// get the input
		return self::input($params);

	}

	public static function input_email($params) {
		
		// change template
		$params['template'] = VCMS_PATH . '/_templates/form/inputs/input-email.html';
		
		// get the input
		return self::input($params);

	}

	public static function input_phone($params) {
		
		// change template
		$params['template'] = VCMS_PATH . '/_templates/form/inputs/input-phone.html';
		
		// get the input
		return self::input($params);

	}

	public static function input_file($params) {

		// set type to default type
		$params['input_type'] = 'input_file';

		// check for multiple files
		$multiple = false;
		if(isset($params['extra']['file_count']) &&
		$params['extra']['file_count'] > 1) {
			$multiple = $params['extra']['file_count'];
		}

		$choose_text = $multiple ? 'choose_more' : 'choose_one';
		
		// change template
		$params['template'] = VCMS_PATH . '/_templates/form/inputs/input-file.html';
		$params['label_template'] = VCMS_PATH . '/_templates/form/other/label-file.html';
		
		// extra stuff
		$params['extra'] = array(

			// what do we allow to upload?
			'file_types'	=> isset($params['extra']['file_types']) ? $params['extra']['file_types'] : true,
			'file_size'		=> isset($params['extra']['file_size']) ? $params['extra']['file_size'] : 2,
			'multiple'		=> $multiple,

			// file button
			'class_button'	=> isset($params['class_button']) ? $params['class_button'] : static::$class_button_file,

			// some texts we need for buttons, errors etc.
			'files_added'	=> self::message('files_added'),
			'choose_button'	=> isset($params['choose_button']) ? $params['choose_button'] : self::message('choose'),
			'choose_text'	=> isset($params['choose_text']) ? $params['choose_text'] : self::message($choose_text),

		);
		
		// get the input
		return self::input($params);

	}

	public static function input_checkbox($params) {
		
		// change template
		$params['template'] = VCMS_PATH . '/_templates/form/inputs/checkbox.html';
		$params['label_template'] = VCMS_PATH . '/_templates/form/other/label-checkbox.html';
		
		// get the input
		return self::input($params);

	}

	public static function input_radio($params) {
		
		// change template
		$params['template'] = VCMS_PATH . '/_templates/form/inputs/radio.html';
		$params['label_template'] = VCMS_PATH . '/_templates/form/other/label-radio.html';
		
		// get the input
		return self::input($params);

	}

	public static function input_select($params) {

		$select_name = isset($params['name']) ? $params['name'] : '';
		
		// change template
		$params['template'] = VCMS_PATH . '/_templates/form/inputs/select.html';

		// set selected
		$selected_option = '';

		if(isset($_POST[$select_name])) {
			$selected_option = $_POST[$select_name];
		} else {
			if(isset($params['selected']) && $params['selected']) {
				$selected_option = $params['selected'];
			}
		}

		// options output
		$options = '';

		// populate options
		if(isset($params['options']) && is_array($params['options'])) {
			foreach ($params['options'] as $option) {

				// just for convinience
				$value = $option['value'];
				$name = $option['name'];

				// check selected
				$selected = $selected_option == $value ? ' selected' : '';

				// add to the tempalte
				$options .= "<option value=\"$value\"$selected>$name</option>";

			}
		}

		// update options to html
		$params['extra']['options'] = $options;
		
		// get the input
		return self::input($params);

	}

	public static function input_password($params) {
		
		// change template
		$params['template'] = VCMS_PATH . '/_templates/form/inputs/input-password.html';
		
		// get the input
		return self::input($params);

	}

	public static function hidden($params) {
		
		// change template
		$params['template'] = VCMS_PATH . '/_templates/form/inputs/hidden.html';
		
		// get the input
		return self::input($params);

	}

}





// -- class.Form.php