<?php

/**
 * Create value for <title> tag
 * @param page = object of current pate
 */

function get_meta_title($page) {

	if(isset($page->meta)
	&& isset($page->meta->title)
	&& $page->meta->title != '') {
		return $page->meta->title;
	}

	// get site name
	$site_name = Config::read('site_name');

	// retunr page title and site name combo
	return $page->name . ( $site_name ? ' | ' . $site_name : '' );

}

/**
 * Create meta tags html
 * @param meta = array of meta tags
 * @param facebook = array of facebook meta tags
 * @param url = current url
 * @param params = extras we need to setup meta
 */

function get_meta($page) {

	// get current page url
	$set_meta_url = '';
	$url = Config::read('url');

	// set it as an url
	if(isset($url[0]) && $url[0] && $url[0] != "") {
		foreach ($url as $add_to_url) {
			$set_meta_url .= "/" . $add_to_url;
		}
	} else {
		// check for slash at the end
		if(!preg_match('/\/$/', $set_meta_url)) {
			$set_meta_url = $set_meta_url . '/';
		}
	}

	/**

	 * Basic meta

	*/

	/**

	 * Check for author

	*/

	if(isset($page->meta) && !isset($page->meta->author)) {
		if(Config::read('meta_author')) {
			$page->meta->author = Config::read('meta_author');
		}
	}

	$meta_output = '';

	if(is_object($page->meta) && count((array)$page->meta) > 0) {

		$meta_output .= "<!-- basic meta tags -->" . "\n";

		foreach ($page->meta as $key => $value) {
			if($value != "" && $key != "title") {
				$meta_output .= "\t" . '<meta name="'.$key.'" content="'.htmlspecialchars($value, ENT_QUOTES, 'UTF-8').'" />' . "\n";
			}
		}

	}

	/**

	 * Facebook

	*/

	/**

	 * Check for siet name

	*/

	if(isset($page->facebook) && !isset($page->facebook->site_name)) {
		if(Config::read('site_name')) {
			$page->facebook->site_name = Config::read('site_name');
		}
	}

	$meta_output .= "\n";

	if(is_object($page->facebook) && count((array)$page->facebook) > 0) {

		$meta_output .= "\t" . "<!-- facebook meta tags -->" . "\n";

		foreach ($page->facebook as $key => $value) {
			// set width and height keys
			if($key == "image_width") $key = "image:width";
			if($key == "image_height") $key = "image:height";
			// check value - except description - we nedd to output even if empty
			if($value != "" || $key == "description") {
				// check if different url needs to be set
				if($key == "url") { $value = $value . $set_meta_url; }
				// output
				$meta_output .=
				"\t" . '<meta property="og:'.$key.'" content="'
				. htmlspecialchars($value, ENT_QUOTES, 'UTF-8')
				. '" />' . "\n";
			}

		}

	}

	return $meta_output;

}





// -- __function.get_meta.php