<?php

/**
 * Load the basic layout of the website
 * @author vs2rs
*/

class Layout {

	/**
	 * Head
	*/
	public static function error_message($message) {
		echo '<div style="color: red; padding: 20px; text-align: center; font-size: 12px; border: 1px solid;">Error - '.$message.' - not found!</div>';
	}

	/**
	 * Head
	*/
	public static function head($section = false) {

		// check for section and do we need to laod extra head
		if($section && file_exists(APP_VIEWS . "/$section/___head.php")) {
			include_once(APP_VIEWS . "/$section/___head.php");
		}

		// set the file
		$file = APP_VIEWS . "/_global/_head.php";

		// check the file
		if(file_exists($file)) include_once($file);
		else self::error_message($file);

	}
	
	/**
	 * End
	*/
	public static function end() {

		// set the file
		$file = APP_VIEWS . "/_global/_end.php";

		// check the file
		if(file_exists($file)) include_once($file);
		else self::error_message($file);
		
	}

	/**
	 * Header
	*/
	public static function header($file = false) {

		// set header location
		$header = $file ? $file : APP_VIEWS . "/_global/_header.php";

		// check if it exists and include it
		if(file_exists($header)) include_once($header);
		else self::error_message($header);

	}

	/**
	 * Footer
	*/
	public static function footer($file = false) {

		// set header location
		$footer = $file ? $file : APP_VIEWS . "/_global/_footer.php";

		// check if it exists and include it
		if(file_exists($footer)) include_once($footer);
		else self::error_message($footer);

	}

	/**
	 * Section
	*/
	public static function section($section) {

		// set default path
		$path = APP_VIEWS . "/$section/__$section.php";
		
		// update path if a full path given
		if(file_exists($section)) $path = $section;

		// inlcude
		if(file_exists($path)) include_once($path);
		else self::error_message($path);

	}

	/**
	 * Load our layout
	*/
	public static function load_section($section = false) {

		// if no section set we get it from our url
		if(!$section) {
			$section = FrontendContent::get_page_from_url();
		}

		// error page
		if($section == 'error') {
			http_response_code(404);
			if(Config::read('dev')) {
				php_error_log('Request not found: ' . $_SERVER['REQUEST_URI']);
			}
		}

		// load it up
		Layout::head($section);
		Layout::header();
		Layout::section($section);
		Layout::footer();
		Layout::end();

		// end everything else
		exit;
		
	}

}





// -- class.Dashboard.php --