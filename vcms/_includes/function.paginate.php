<?php

/**
 * Create pagination
 * @param int $page - current page
 * @param int $limit - how many elements show on one page
 * @param int $total - total elements
 * @return array of pagination elements
*/
function paginate($params) {

	// tempalte
	$html = '';
	// $showDots = output_block(array(), file_get_contents(APP_VIEWS . '/blocks/pagination-page-dots.html'));
	$showDots = '<span>...</span>';

	// get pages.. ceil - round the number up
	$total_pages = ceil($params['total'] / $params['limit']);
	$current_page = $params['page'];
	$link = $params['link'];

	// start
	$page = 1;

	// how many pages to show if the count is to large
	$page_limit = 5; // needs to be an odd number

	// if page count larger then limit + 3
	if($total_pages > $page_limit + 3) {

		if($current_page <= $page_limit - 1) {

			// show the first elements
			// example: 1 2 3 4 5 .. [last]

			while ($page <= $page_limit) {
				$html .= showPageNumber($page, $current_page, $link);
				$page++;
			}

			// dots
			$html .= $showDots;

			// show the last page number
			$html .= showPageNumber($total_pages, false, $link);

		} else if ($current_page > $page_limit - 1 && $current_page < $total_pages - $page_limit + 2) {

			// we are some where in the middle of pages
			// example: 1 .. 3 4 5 .. 7

			// show the first page number
			$html .= showPageNumber(1, false, $link);

			// dots
			$html .= $showDots;

			// set interval
			$page = $current_page - 2;
			$page_end = $current_page + 2;

			while ($page <= $page_end) {
				$html .= showPageNumber($page, $current_page, $link);
				$page++;
			}

			// dots
			$html .= $showDots;

			// show the last page number
			$html .= showPageNumber($total_pages, false, $link);

		} else if ($current_page >= $total_pages - $page_limit + 2) {

			// show the last elements
			// example: 1 .. 10 11 12 13 14

			// show the first page number
			$html .= showPageNumber(1, false, $link);

			// dots
			$html .= $showDots;

			// set interval
			$page = $total_pages - $page_limit + 1;

			while ($page <= $total_pages) {
				$html .= showPageNumber($page, $current_page, $link);
				$page++;
			}

		}

	} else {

		while ($page <= $total_pages) {

			// if we don't have more then limit, then we show all the page numbers

			// set html
			$html .= showPageNumber($page, $current_page, $link);

			// increas
			$page++;

		}

	}

	// setup next page block
	// $next = $current_page == $total_pages ? '' : output_block(array(
	// 	'link' => $link . "/$page_url/" . ($current_page + 1),
	// 	'text' => $params['news_older']
	// ), file_get_contents(APP_VIEWS . '/blocks/pagination-next.html'));

	// // setup prev page block
	// $prev = $current_page == 1 ? '' : output_block(array(
	// 	'link' => $current_page - 1 == 1 ? $link : $link . "/$page_url/" . ($current_page - 1),
	// 	'text' => $params['news_newer']
	// ), file_get_contents(APP_VIEWS . '/blocks/pagination-prev.html'));

	// html wraper
	if(isset($params["template"]["main"])) {

		$html = get_html(array(
			'next'  => false,
			'prev'  => false,
			'pages' => $html,
			'template' => $params["template"]["main"]
		));

	} else {

		$html = process_template_vars('<div>{{pages}}</div>', array(
			'pages' => $html
		));

	}

	// return template
	return $html;

}

function showPageNumber($page, $current_page, $link) {

	// $pageNumberTemplate = file_get_contents(APP_VIEWS . '/blocks/pagination-page.html');
	// $pageCurrentTemplate = file_get_contents(APP_VIEWS . '/blocks/pagination-page-current.html');

	$template = '<a data-page="{{page}}" href="{{link}}{{page}}">{{page}}</a>';
	$template_no_link = '<span data-page="{{page}}">{{page}}</span>';

	if($page == $current_page) $template = $template_no_link;

	// return
	return process_template_vars($template, array(
		'link' 		=> $link,
		'page' 		=> $page
	));

}

function paginate_limit($page, $limit) {
	
	// calculate start position
	$start = ($page - 1) * $limit;

	// return values set for query
	return "$start,$limit";

}





// -- function.paginate.php --