<?php

class FrontendContentOld {

	// content
	private $content;

	// language params
	public $lang = false;
	public $lang_default = false;
	public $lang_current = false;

	// db connection and config
	private $db;

	function __construct() {

		// start the db
		$this->db = Database::getInstance();

		// tables
		$content_table  = "{{prefix}}content";
		$template_table = "{{prefix}}templates";

		// get the content
		$db_query = "SELECT A.content_id as id,
							A.content_alias as alias,
							A.content_name as name,
							A.type,
							A.menu_text,
							A.prop_names as props,
							A.show_in_menu,
							A.accesskey,
							A.modified_date as date_modified,
							A.parent_id,
							A.hierarchy_path,
							A.default_content,
							(SELECT template_name FROM $template_table WHERE template_id = A.template_id) as template
					 FROM $content_table A
					 WHERE A.active = 1
					 ORDER BY hierarchy ASC";

		// check results
		if($result = $this->db->getRows($db_query)) {

			// set content
			$this->content = new stdClass();

			// run true results
			while ($row = $result->fetch_object()) {

				// setup properties
				$row = $this->setupProperties($row);

				// temporary key
				// set alias as key for the object
				// this is later change to accesskey
				$alias = $this->setupAliasAsKey($row->alias);

				// check access key for language
				if($row->accesskey == "lang") {
					// is anything set?
					if($this->lang == false) {
						$this->lang = new stdClass();
						$this->lang_default = $alias;
						Config::write("lang_default", $alias);
					}
					// set lang alias
					$this->lang->$alias = $row;
				}

				// new array for the content
				$this->content->$alias = new stdClass();

				// set in the content var
				$this->content->$alias = $row;

			}
			
			// add to the config all the lanuages
			Config::write("lang_list", $this->lang);

		}

	}

	/**
	 * ----
	 * @param
	 */
	private function setupAliasAsKey($alias) {
		return str_replace("-", "_", $alias);
	}

	/**
	 * ----
	 * @param
	 */
	private function setupProperties($row) {

		// split it in a array
		$props = explode(",", $row->props);

		// tables
		$content_props_table = $this->db->prefix . "content_props";
		$content_id = $row->id;

		// let's go
		foreach ($props as $prop) {
	
			// get the prop content
			$db_query = "SELECT content
					 	 FROM $content_props_table
					 	 WHERE content_id = $content_id
					 	 AND prop_name = '$prop'";

			// run the query
			$result = $this->db->connect()->query($db_query);

			// check results
			if(isset($result) && $result && $result->num_rows > 0) {
				if($prop_value = $result->fetch_object()) {
					if($prop == "content_en") {
						$prop = "content";
					}
					$row->$prop = $prop_value->content;
				}
			}
			
		}

		// remove props col
		unset($row->props);

		// return the row
		return $row;

	}

	/**
	 * arange pages in a page tree
	 * @param $pid = parent id
	 */
	public function getPageTree($pid = -1) {

		// -1 is the parent id of first level
		// items that don't have a parent
		
		// set the new object
		$contentTree = new stdClass();

		foreach($this->content as $item) {

			// we need to clone the item
			// so that the original array
			// remains untouched
			$newItem = clone $item;

			// find the parent object
			if($newItem->parent_id == $pid) {

				// setup accesskey
				$accesskey = $newItem->accesskey != "" && $newItem->accesskey != "lang"
						   ? $newItem->accesskey
						   : $newItem->alias;

				// add to the new array
				$contentTree->$accesskey = $newItem;

				// now check for children for this item
				$children = $this->getPageTree($newItem->id);

				// if we have children, add to the item
				if(count((array)$children) > 0) {
					$contentTree->$accesskey->children = $children;
				}

			}

		}

		// return the new array
		return $contentTree;

	}

	// get default page
	public function getDefaultPage() {
		$default_page = false;
		foreach ($this->content as $alias => $params) {
			if($params->default_content == 1) {
				return $params;
			}
		}
	}

	// get default parent
	public function getDefaultParent() {

		// get the default page
		$default_page = $this->getDefaultPage();

		// split hierarchy path to get alias
		$alias = explode("/", $default_page->hierarchy_path);
		$alias = $alias[0];

		// return the page array
		return $this->content->$alias;

	}

	/**
	 * Check the URL for language and return language code
	 * Also update $this->current_lang
	 * @param $url - current url
	 */
	public function getCurrentLang($url) {

		// set to default
		$lang = $this->lang_default;

		// is anything in the url?
		if(isset($url[0])) {
			// set
			$lang_code = $url[0];
			// check
			if(isset($this->lang->$lang_code)) {
				// update
				$lang = $lang_code;
			}
		}

		// update current lang
		$this->lang_current = $lang;

		// add lang current to the config
		Config::write("lang_current", $lang);

		// return lang code
		return $lang;

	}

	/**
	 * Get the page list of current language
	 * @param $url - current url
	 */
	public function getPages($lang = false) {

		// use current language if no language set
		$lang = $lang ? $lang : $this->lang_current;
			
		// setup page tree
		$pageTree = $this->getPageTree();

		// is lang param set and is it in our list
		if($lang && isset($this->lang->$lang)) {

			// check if a page we looking for is there
			if(isset($pageTree->$lang)) {
				return $pageTree->$lang->children;
			}

		} else {

			// return all pages if no lang
			return $pageTree;

		}

	}

	/**
	 * Check the content
	 * @param $url - current url
	 * @return $page - current open page object
	 */
	public function isThisValidContent($url) {

		// default to false
		$page = false;

		// is language code in the url?
		// if not we use default language
		$url_lang = false;

		if($url && is_array($url)) {

			// set the last key of the array
			$last_key = count($url) - 1;

			// get the full page tree
			$pageTree = $this->getPageTree();
			
			// let go
			foreach ($url as $url_key => $section) {

				// first, is this language?
				// only first child can be language
				if($url_key == 0 && $this->isLanguage($section)) {

					// language is in the url
					$url_lang = $section;
					
					// set the page to the language
					$page = $pageTree->$section;
					
					// is this the end of the url?
					if($url_key == $last_key) {
						// get the default page
						// todo --> need to get this from $this->content
						$page = $page->children->home;
					}

				} else if($accesskey = $this->isPage($section)) {

					// if no lang in the url
					// update page to default language
					if(!$url_lang) {
						$url_lang = $this->lang_default;
						$page = $pageTree->$url_lang;
					}

					// this is a valid section
					// but is it a valid child of the current parent page
					if($this->isChild($accesskey, $section, $page)) {

						// set the page
						$page = $page->children->$accesskey;

					} else {
						
						// not valid so set to false
						$page = false;

					}

				} else {

					// check children and look for module
					if(isset($page->children)
					&& is_object($page->children)
					&& count((array)$page->children) > 0) {

						// set the parent
						$parent = $page;

						foreach ($parent->children as $child) {
							if($child->template == "_module") {
								$module = $child->content;
								break;
							}
						}

						// new module
						$module = new FrontendModule($module);

						// get the results
						$page = $module->getSingleItem($section, $parent);

					}

				}

			} // end of foreach

		} else {

			$page = $this->getDefaultPage();

		}

		// add language to page object
		if($page) {
			$page->lang = $url_lang;
		}

		// return the page
		return $page;

	}

	public function isLanguage($url) {
		if(isset($this->lang->$url)) {
			return true;
		} else {
			return false;
		}
	}

	public function isPage($url) {
		
		// setup alias for checking
		$url = $this->setupAliasAsKey($url);
		
		// check if it's set and that it's not a module
		if(isset($this->content->$url)
		&& $this->content->$url->template != "_module") {
			// success we found it
			return $this->content->$url->accesskey;
		}

		return false;

	}

	public function isChild($accesskey, $alias, $parent) {
		if(isset($parent->children->$accesskey)
		&& $parent->children->$accesskey->alias == $alias) {
			return true;
		} else {
			return false;
		}
	}

	public function getLanuageList() {
		return $this->lang;
	}

}




