<?php

/**
 * Load our functions, classes etc.
 * @author vs2rs
*/

class Load {
	
	/**
	 * Load all the files
	 * @param string $type - function or class
	 * @param array $list - list of files to load
	 * @return boolean
	*/
	private static function load_files($type, $list) {

		// check if type is correct
		if($type != 'function' && $type != 'class') return false;

		// check if list is defined
		if(!is_array($list) || count($list) == 0) return false;

		// load the files
		foreach ($list as $file) {

			// set the paths
			$file_paths = array(
				$file,
				PROJECT_PATH . VCMS_INCLUDE_FOLDER . "/$type.$file.php",
				VCMS_PATH . VCMS_INCLUDE_FOLDER . "/$type.$file.php"
			);

			// result
			$file_found = false;

			// go through paths and check them
			foreach ($file_paths as $file_path) {

				// break on first find
				if(file_exists($file_path)) {
					$file_found = true;
					require_once($file_path);
					break;
				}

			}
			
			// if nothing found end all
			if(!$file_found) die("File $type.$file.php not found");

		}
		
		return true;

	}
	
	/**
	 * Get functions
	 * @param array $function_names
	 * @return void
	*/
	public static function functions($function_names = false) {
		if(!$function_names) $function_names = Config::read('functions');
		Load::load_files('function', $function_names);
	}

	/**
	 * Get classes
	 * @param array $class_names
	 * @return void
	*/
	public static function classes($class_names = false) {
		if(!$class_names) $class_names = Config::read('classes');
		Load::load_files('class', $class_names);
	}

}





// -- class.LoadOperations.php