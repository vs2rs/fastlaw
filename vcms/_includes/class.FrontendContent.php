<?php

class FrontendContent {

	// instance
	private static $_instance;

	// our frontend content from the db
	private static $_pages = array();
	private static $_pages_default = false;

	// language list
	private static $_languages = array();

	/**
	 * Get an instance of the Database
	 * @return Instance
	*/
	public static function get_instance() {
		if(!self::$_instance) { // If no instance then make one
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Get our content pages from the db and save to instance
	 * @return void
	*/
	private function __construct() {

		// start the db
		$db = Database::getInstance();

		// tables
		$content_table  = "{{prefix}}content";
		$template_table = "{{prefix}}templates";

		// get the content
		$db_query = "SELECT A.content_id as id,
							A.content_alias as alias,
							A.content_name as name,
							A.type,
							A.owner_id,
							A.menu_text,
							A.prop_names as props,
							A.show_in_menu,
							A.accesskey,
							A.modified_date as date_modified,
							A.parent_id,
							A.hierarchy_path,
							A.default_content,
							(SELECT template_name FROM $template_table WHERE template_id = A.template_id) as template
					 FROM $content_table A
					 WHERE A.active = 1
					 ORDER BY hierarchy ASC";

		// check results
		if($result = $db->getRows($db_query)) {

			$pages = array();

			// run true results
			while ($row = $result->fetch_object()) {

				// set the default page
				if($row->default_content == 1) {
					self::$_pages_default = $row;
				}

				// add to language list
				if($row->type == 'lang') {
					self::$_languages[$row->alias] = $row;
				}
				
				// split it in a array
				$props = explode(",", $row->props);

				// tables
				$content_props_table = "{{prefix}}content_props";
				$content_id = $row->id;

				// let's go
				foreach ($props as $prop) {
			
					// get the prop content
					$db_query = "SELECT content
							 	 FROM $content_props_table
							 	 WHERE content_id = $content_id
							 	 AND prop_name = '$prop'";

					// run the query
					if($prop_value = $db->getRow($db_query)) {
						if($prop == "content_en") $prop = "content";
						$row->$prop = $prop_value->content;
					}
					
				}

				// remove props col
				unset($row->props);

				// create meta tags
				$row->meta = self::set_meta_default($row);
				$row->facebook = self::set_meta_facebook($row);

				// add to pages
				self::$_pages[$row->id] = $row;

			}

		}

	}

	/**
	 * Get a nested list with all the content pages
	 * - parent
	 * - - children
	 * - - - children
	 * - parent
	 * - - children
	 * 
	 * @param integer - parent id (-1 is the parent id of first level)
	 * @return array - nested array of page objects
	*/
	public static function get_pages($parent_id = -1) {

		// check if accesskey given not id
		if(floor($parent_id) == 0) {

			// start the db
			$db = Database::getInstance();

			$parent_id = $db->get_one("
				SELECT content_id FROM {{prefix}}content
				WHERE accesskey = '$parent_id' LIMIT 1
			");

		}

		// get the pages frome the instance
		$current_instance = self::get_instance();
		
		// set the new object
		$pages = new stdClass();

		if(count($current_instance::$_pages) > 0) {

			foreach($current_instance::$_pages as $page) {

				// find the parent object
				if($page->parent_id == $parent_id) {

					// setup accesskey - use alias by default if no accesskey set
					$accesskey = str_replace("-", "_", $page->alias);
					if($page->accesskey != "") $accesskey = $page->accesskey;

					// add to the new array
					$pages->$accesskey = $page;

					// now check for children for this item
					$children = self::get_pages($page->id);

					// if we have children, add to the item
					if(count((array)$children) > 0) {
						$pages->$accesskey->children = $children;
					}

				}

			}

		}

		return $pages;

	}

	/**
	 * Get the template of our default page
	 * @return string - name of the template
	*/
	public static function get_default_page($lang = false) {

		// get the instance
		$current_instance = self::get_instance();
		
		// check the instance
		if($current_instance::$_pages_default) {

			// update default page
			$page_default = $current_instance::$_pages_default;

			// update our config
			Config::write('page_default', $page_default->id);
			Config::write('page_current', $page_default->id);

			// return the template name
			return $page_default->template;

		}

		// get the page from config and set it to current
		$page_default = Config::read('page_default');
		Config::write('page_current', $page_default);

		// return default from config
		return $page_default;

	}

	/**
	 * Get current page
	 * @return array
	*/
	public static function get_current_page() {

		// get the instance
		$current_instance = self::get_instance();

		// get pages
		$pages = $current_instance::$_pages;

		// get current page from config
		$page_current = Config::read('page_current');

		// check in pages
		if(count($pages) > 0 && isset($pages[$page_current])) {
			$page_current = $pages[$page_current];
		}

		// return
		return $page_current;

	}

	/**
	 * Return the array with our languages
	 * @return array
	*/
	public static function get_languages() {

		// get the instance
		$current_instance = self::get_instance();

		// default from config
		$languages = Config::read('languages');

		// check the languages
		if(count($current_instance::$_languages) > 0) {
			$languages = $current_instance::$_languages;
		}

		// return
		return $languages;

	}

	/**
	 * Return the alias of default language
	 * @return array
	*/
	public static function get_default_lang() {
		
		// get instance
		$current_instance = self::get_instance();

		// get from config
		$lang_default = Config::read('lang_default');

		// get pages
		$pages = $current_instance::$_pages;
		$default_page = $current_instance::$_pages_default;

		// did we get anything from the db?
		if(count($pages) > 0 && $default_page) {

			// check if language set in pages
			if(isset($pages[$default_page->owner_id])
			&& $pages[$default_page->owner_id]->type == 'lang') {

				// update return value
				$lang_default = $pages[$default_page->owner_id]->alias;

				// update config
				Config::write('lang_default', $lang_default);
				Config::write('lang_current', $lang_default);

			}

		}

		// return from config
		return $lang_default;

	}

	/**
	 * Check the content
	 * @param array $url - current url
	 * @return string - template of the page
	 */
	public static function get_page_from_url() {
		
		// get the pages frome the instance
		$current_instance = self::get_instance();

		// get config params
		$url = Config::read('url');

		if($url && is_array($url)) {

			// set the last key of the array
			$last_key = count($url) - 1;

			// get some of the stuff we need
			$pages = self::get_pages();
			$languages = self::get_languages();

			// let go
			foreach ($url as $url_key => $page_alias) {

				// check the first element
				if($url_key == 0) {

					// first, is this language?
					// only first child can be language
					if($languages
					&& (isset($languages[$page_alias])) || in_array($page_alias, $languages)) {

						// update lang
						Config::write('lang_current', $url[0]);
						
						// set the page to the language
						$page = isset($pages->$page_alias) ? $pages->$page_alias : false;
					
						// is this the end of the url?
						if($url_key == $last_key) {

							// get default page for the lang
							return self::get_default_page($page_alias);

						}

					} else {

						// this is not a language so let's check if it's a page
						$page = self::get_page_form_alias($url[0]);
					
						// is this the end of the url?
						if($page && $url_key == $last_key) {
							
							// update current
							Config::write('page_current', $page->id);

							// return the page
							return $page->template;

						}

					}

				} else {

					// only if we have set the parent page
					if($page) {

						// check for children
						if(isset($page->children)) {

							// go through
							foreach ($page->children as $child) {

								// check alias
								if($child->alias == $page_alias) {

									// found it
									$page = $child;
									
									// is this the last key?
									if($url_key == $last_key) {

										// update current
										Config::write('page_current', $page->id);

										// return
										return $page->template;
										
									}

								} else if($child->type == 'modulepage') {

									// get db
									$db = Database::getInstance();
									$db_module =
										'module_' .
										$child->module_name . '_' .
										$child->content;

									$alias_param = 'alias';
									if(Config::read("lang_current") != Config::read("lang_default")) {
										$alias_param = $alias_param . '_' . Config::read("lang_current");
									}

									// query
									$db_query = "
									SELECT * FROM {{prefix}}$db_module
									WHERE active = 1
									AND ( $alias_param = '$page_alias' OR alias = '$page_alias' )
									LIMIT 1";

									// do the lookup
									if($module_item = $db->getRow($db_query)) {

										// add to the page
										$page->module_item = $module_item;
										$page->module_item->module = $child;

										// create meta tags
										$page->module_item->meta = self::set_meta_default($module_item);
										$page->module_item->facebook = self::set_meta_facebook($module_item);
										
										// update current
										Config::write('page_current', $page->id);

										// return template
										return $child->template;

									}

								}

							}

						}

					}

				}

			}

		} else {

			// return default page
			return self::get_default_page();

		}

		// return
		return 'error';

	}

	/**
	 * Look through all the pages for an alias
	 * @param page_alias - string - current page alias
	 */

	public static function get_page_form_alias($page_alias) {
		
		// get the pages frome the instance
		$current_instance = self::get_instance();

		// let do the check in the simple page list
		$pages = $current_instance::$_pages;

		// did we get anything from the db?
		if(count($pages) > 0) {

			foreach ($pages as $page) {

				// return if found
				if($page->alias == $page_alias) {

					// update current
					Config::write('page_current', $page->id);

					// return the page
					return $page;
					
				}

			}

		}

		// nothing found
		return false;

	}

	/**
	 * Create array of default meta data
	 * @param page = current page object
	 */

	public static function set_meta_default($page) {

		// set the array
		$meta = new stdClass();

		// get lang
		$lang = Config::read('lang_current');

		// check the names of the params
		$meta_names = array("meta_title", "meta_description", "meta_keywords", "meta_author");
		foreach ($meta_names as $meta_name) {
			$meta_name_lang = $meta_name . '_' . $lang;
			if(!isset($page->$meta_name) && isset($page->$meta_name_lang)) {
				$page->$meta_name = $page->$meta_name_lang;
			}
		}

		// default
		$meta->title = '';
		$meta->description = '';
		$meta->keywords = '';

		// check for meta title and if it's not empty
		if(isset($page->meta_title) && $page->meta_title != '') {
			$meta->title = strip_tags($page->meta_title);
		}

		// check for meta description and if it's not empty
		if(isset($page->meta_description) && $page->meta_description != '') {
			$meta->description = strip_tags($page->meta_description);
		}

		// check for meta keywords and if it's not empty
		if(isset($page->meta_keywords) && $page->meta_keywords != '') {
			$meta->keywords = strip_tags($page->meta_keywords);
		}

		// check for meta author and if it's not empty
		if(isset($page->meta_author) && $page->meta_author != '') {
			$meta->author = strip_tags($page->meta_author);
		}

		// return results
		return $meta;

	}

	/**
	 * Create array of facebook meta data
	 * @param page = current page object
	 */

	public static function set_meta_facebook($page) {

		// setup http or https
		$http = "http:";
		if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
			$http = "https:";
		}

		// set the array
		$facebook = new stdClass();

		// get lang
		$lang = Config::read('lang_current');

		// check the names of the params
		$meta_names = array("facebook_title", "facebook_description", "facebook_image");
		foreach ($meta_names as $meta_name) {
			$meta_name_lang = $meta_name . '_' . $lang;
			if(!isset($page->$meta_name) && isset($page->$meta_name_lang)) {
				$page->$meta_name = $page->$meta_name_lang;
			}
		}

		// default
		$facebook->title = $page->meta->title;
		$facebook->description = $page->meta->title;

		// check for facebook title and if it's not empty
		if(isset($page->facebook_title) && $page->facebook_title != '') {
			$facebook->title = strip_tags($page->facebook_title);
		}

		// check for facebook description and if it's not empty
		if(isset($page->facebook_description) && $page->facebook_description != '') {
			$facebook->description = strip_tags($page->facebook_description);
		}

		// check for facebook image and if it's not empty
		if(isset($page->facebook_image) && $page->facebook_image != '') {
			$page->facebook_image = get_image($page->facebook_image);
		} else {
			// let's check if there is a default image for the page
			if(isset($page->img) && $page->img != '') {
				$page->facebook_image = get_image($page->img);
			} else {
				$page->facebook_image = false;
			}
		}

		// now we need to set it up for the tags 
		if($page->facebook_image && $page->facebook_image->src) {

			// set path
			$facebook_image_path = APP_PATH . '/' . $page->facebook_image->src;

			// set the image url
			$facebook->image = $http . APP_URL . '/' . $page->facebook_image->src;
			
			// set the height and width
			list($facebook->image_width, $facebook->image_height) =
			getimagesize($facebook_image_path);

		}

		// check for siet name and if it's not empty
		if(isset($page->site_name) && $page->site_name != '') {
			$facebook->site_name = strip_tags($page->site_name);
		}

		// and some other tags
		$facebook->url = $http . APP_URL;
		$facebook->type = "website";

		// return results
		return $facebook;

	}

	/**
	 * Setup url for given language
	 * @param page = current page object
	 * @return string
	 */

	public static function get_lang_url($lang = false, $page = false) {

		// if not set use current
		if(!$lang) $lang = Config::read('lang_current');

		// no param in the url by default
		$lang_url = ''; 

		// if not default add lang to the url
		if($lang != Config::read('lang_default')) {
			$lang_url = $lang;
		}

		// check if we need to check for page
		if($page) {
			$lang_url .= ( $lang_url != '' ? '/' : '' ) . $page->alias;
		}

		// return url
		return $lang_url;

	}

}




