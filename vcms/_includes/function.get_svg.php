<?php

/**
 * Get SVG code from file
 * @param string $url - location of the svg element
 * @return string - svg tag or empty string if no file
*/
function get_svg($svg_path, $class = false) {

	// is the template set?
	if(file_exists($svg_path)) {

		// read the file
		$svg_file = file_get_contents($svg_path);

		// add class to the tag
		if($class) {
			$svg_file = str_replace('<svg', '<svg class="'.$class.'"', $svg_file);
		}

		// new dom element and load the file
		$doc = new DOMDocument();
		$doc->loadXML($svg_file);
		$svg = $doc->getElementsByTagName('svg');
		
		// return the tag
		return $svg->item(0)->C14N();

	}

	// no file found
	php_error_log("file $svg_path not found");

	// return empty
	return '';

}





// -- function.get_svg.php --