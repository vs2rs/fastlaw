<?php

$admin_files = array("listmodules", "moduleinterface");

/**
 * Create array of default meta data
 * @param config = site config
 * @param url = requested url
 * @param admin_file_loc = location of the file to check
 */

function get_admin_file($file) {

	// return result
	$result = false;

	// set our paths
	$admin_file_path = VCMS_PATH . $file;
	$admin_file_path_alt = PROJECT_PATH . $file;

	// now we check if the file exists
	$admin_file = false;
	
	// check files
	if(file_exists($admin_file_path)) $admin_file = $admin_file_path;
	if(file_exists($admin_file_path_alt)) $admin_file = $admin_file_path_alt;

	// if we have the file
	if($admin_file) {

		// get extension
		$admin_file_ext = pathinfo($admin_file, PATHINFO_EXTENSION);

		// check the type
		switch ($admin_file_ext) {
			case 'php':
				$admin_file_type = 'php';
				break;
			case 'css':
				$admin_file_type = 'text/css';
				break;
			case 'js':
				$admin_file_type = 'application/javascript';
				break;
			case 'png':
				$admin_file_type = 'image/png';
				break;
			case 'jpg':
				$admin_file_type = 'image/jpeg';
				break;
			case 'jpeg':
				$admin_file_type = 'image/jpeg';
				break;
			case 'gif':
				$admin_file_type = 'image/gif';
				break;
			case 'svg':
				$admin_file_type = 'image/svg+xml';
				break;
			case 'ico':
				$admin_file_type = 'image/x-icon';
				break;
			default:
				$admin_file_type = false;
				break;
		}

		// do we have a supported type?
		if($admin_file_type) {

			// check the type
			switch ($admin_file_ext) {
				case 'php':
					include_once($admin_file);
					break;
				default:
					header("Content-type: " . $admin_file_type);
					readfile($admin_file);
					break;
			}
		
		}

	} else {

		// logError(
		// 	"File not found: {{file}}, function {{function}}().",
		// 	$admin_file_path,
		// 	__FUNCTION__
		// );

	}

	// return $result;

}

/**
 * Check for file and include it
 * @param string $file_path - path to file
 */
function get_vcms_file($file_path) {

	// no file at first
	$admin_file = false;

	// set our paths
	$admin_file_path = VCMS_PATH . $file_path;
	$admin_file_path_alt = PROJECT_PATH . $file_path;

	// now we check if the default file exists
	if(file_exists($admin_file_path)) {
		$admin_file = $admin_file_path;
	}

	// now check for alternative version
	if(file_exists($admin_file_path_alt)) {
		$admin_file = $admin_file_path_alt;
	}

	// return
	return $admin_file ? $admin_file : $file_path;

}





// -- function.get_admin_file.php