<?php

class Authentication {

	protected $error = false;
	protected $session;
	private static $_instance;

	/**
	 * Get the current instance
	 * @return instance
	*/
	public static function getInstance() {
		if(!self::$_instance) { // If no instance then make one
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Constructor
	 * @return void
	 */
	protected function __construct() {
		$this->session = Session::start();
	}

	// return error message
	public function get_error() {
		return $this->error;
	}

	// login
	public function is_loged_in() {

		// check the finger print
		if($this->session->read("fingerprint") == $this->fingerprint()) {
			return true;
		}

		return false;

	}

	// login
	public function login($username, $password) {

		// check the data
		if($this->check_username($username)
		&& $this->check_password($password)) {

			// regenreate id on login
			$this->session->regenerate_id();

			// add user to the session
			$this->session->write("user", $username);
			$this->session->write("fingerprint", $this->fingerprint());
			
			// all good
			return true;

		}

		// error message
		$this->set_error('Login failed!');

		// fail
		return false;

	}

	// set error message
	protected function set_error($message) {
		$this->error = $message;
	}

	protected function check_username($username) {
		return false;
	}

	protected function check_password($password) {
		return false;
	}

	protected function fingerprint() {
		return md5(
			$_SERVER['HTTP_USER_AGENT'] .
			FINGERPRINT . $this->session->read('user') .
			$_SERVER['REMOTE_ADDR']
		);
	}

}