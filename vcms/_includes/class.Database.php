<?php

class Database {

	// instance and connection
	private static $_instance;
	private $_connection;

	// db prefix
	public $prefix;

	// Get mysqli connection
	public function connect() {
		return $this->_connection;
	}

	// Get mysqli connection
	public static function newInstance($connection) {
		return new self($connection);
	}

	/**
	 * Get an instance of the Database
	 * @return Instance
	 */
	public static function getInstance() {
		if(!self::$_instance) { // If no instance then make one
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Constructor
	 * @param $connection - host db etc.
	 * @return void
	 */
	private function __construct($connection = false) {

		// get the config file
		$config = Config::getInstance();

		// set values
		$hostname = $config->read('db_hostname');
		$username = $config->read('db_username');
		$password = $config->read('db_password');
		$database = $config->read('db_name');
		$port 	  = $config->read('db_port');
		$prefix   = $config->read('db_prefix');

		// check if specific connection set
		if($connection) {
			$hostname = $connection['db_hostname'];
			$username = $connection['db_username'];
			$password = $connection['db_password'];
			$database = $connection['db_name'];
			$port = false;
			if(isset($connection['db_port'])) {
				$port = $connection['db_port'];
			}
			$prefix = '';
			if(isset($connection['db_prefix'])) {
				$prefix = $connection['db_prefix'];
			}
		}

		// set the prefix
		$this->prefix = $prefix;

		// new connection
		$this->_connection = new mysqli();

		// if port is set
		if($port) $this->_connection->port = $port;

		// connect
		$this->_connection->connect(
			$hostname, $username, $password, $database
		);
		
		// change character set to utf8
		$this->_connection->set_charset("utf8");
	
		// error handling
		if(mysqli_connect_error()) {
			trigger_error(
				"Failed to conenct to MySQL: " .
				mysqli_connect_error() . "(" .
				mysqli_connect_errno() . ")",

				E_USER_ERROR
			);
		}

	}

	// old
	public function getRows($query, $debug = false) {

		// replace prefix
		$query = str_replace("{{prefix}}", $this->prefix, $query);

		// if we debuging
		if($debug) {
			print_it($query);
		}

		// run the query
		$result = $this->connect()->query($query);

		// check results
		if(isset($result)
		&& $result
		&& $result->num_rows > 0) {
			return $result;
		}

		// show last error
		if($debug) {
			print_it($this->connect()->error);
		}

		return false;

	}

	// new
	public function get_rows($query, $return = 'array') {

		// replace prefix
		$query = str_replace("{{prefix}}", $this->prefix, $query);

		// run the query
		$result = $this->connect()->query($query);

		// check results
		if(isset($result)
		&& $result
		&& $result->num_rows > 0) {

			// check if we return array of rows
			if($return == 'array') {

				// our array
				$results = array();

				// setup result array
				while ($row = $result->fetch_object()) {
					$results[] = $row;
				}

				// return the array
				return $results;

			} else {

				// return query results
				return $result;

			}
			
		}

		return false;

	}

	public function getRow($query) {

		// get the query
		$result = $this->getRows($query);

		// do we have anything
		if($result) {
			return $result->fetch_object();
		}

		return false;

	}

	public function get_row($query) {
		return self::getRow($query);
	}

	public function get_one($query, $debug = false) {

		// get the query
		$result = $this->getRows($query, $debug);

		// do we have anything
		if($result) {
			$row = $result->fetch_array();
			return $row[0];
		}

		return false;

	}

	// TODO:
	// need to remove this form all projects (use insert_row instead)
	// and need to redo the insert_id return as seperate function
	// because there could be table without AUTU_INCR
	public function insertRow($query, $debug = false) {

		// replace prefix
		$query = str_replace("{{prefix}}", $this->prefix, $query);

		// if we debuging
		if($debug) {
			print_it($query);
		}

		// run the query and return insert id
		if($this->connect()->query($query) === true
		&& $this->connect()->affected_rows > 0) {
			if($debug) {
				print_it($this->connect()->insert_id);
			}
			return $this->connect()->insert_id;
		}

		// show last error
		if($debug) {
			print_it($this->connect()->error);
		}

		return false;

	}

	public function insert_row($query, $debug = false) {
		return self::insertRow($query, $debug);
	}

	public function deleteRow($query) {

		// replace prefix
		$query = str_replace("{{prefix}}", $this->prefix, $query);

		// run the query and return insert id
		if($this->connect()->query($query) === true
		&& $this->connect()->affected_rows > 0) {
			return true;
		}

		return false;

	}

	public function delete_row($query) {
		return self::deleteRow($query);
	}

	public function update($query, $debug = false) {

		// replace prefix
		$query = str_replace("{{prefix}}", $this->prefix, $query);

		// if we debuging
		if($debug) {
			print_it($query);
		}

		// run the query and return insert id
		if($this->connect()->query($query) === true
		&& $this->connect()->affected_rows > 0) {
			return true;
		}

		// show last error
		if($debug) {
			print_it($this->connect()->error);
		}

		return false;

	}

	public function get_next_id($table, $debug = false) {

		// default
		$result = false;

		// get current database name
		$db_name = Config::read('db_name');

		// wx get next auto_increment to use with return
		$db_query =
		"SELECT AUTO_INCREMENT FROM information_schema.TABLES
		WHERE TABLE_SCHEMA = '$db_name'
		AND TABLE_NAME = '{{prefix}}$table'";

		// get it
		$result = $this->get_one($db_query, $debug);

		// return result
		return $result;

	}

	public function escape($string) {

		// add slashes to name
		$string = addcslashes($string, "%_");
		$string = $this->connect()->real_escape_string($string);

		// return result
		return $string;

	}

	public function timestamp_current() {

		// current date
		$time = new DateTime();
		$time->setTimezone(new DateTimeZone('Europe/Riga'));
		$timestamp = $time->format('Y-m-d H:i:s');

		// return result
		return $timestamp;

	}

	// Magic method clone is empty to prevent duplication of connection
	private function __clone() { }

}





?>