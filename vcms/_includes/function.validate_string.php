<?php

/**
 * ------------------------------------
 * Validate data
 * @param $string - text to validate
 * @param $type - type of validation to do
 * @example for preg_match https://catswhocode.com/php-regex/
 * ------------------------------------
 */

function validate_string($string, $type) {

	// default is false
	$result = false;

	// alias can contain only latin chars and numbers, and dash (no spaces)
	// alias is used for creating URL's
	if($type == "alias" || $type == "url") {
		$result = preg_match("/^[a-zA-Z0-9\-]+$/", $string);
	}

	// name can be numbers, latin, extended latin, cyrilic, space and dash
	// todo - šim ir jābūt title nevis name - mums adminā jāsamian
	// un tad name izmantojam priekš Name un Surname ar utf čariem
	if($type == "name") {
		$result = preg_match("/^[a-zA-Z0-9\p{L}\s\-]+$/u", $string);
	}

	// username --> letters, numbers, _, -, @, .
	if($type == "username") {
		$result = preg_match("/^[a-zA-Z0-9\.\_\-\@\+]+$/", $string);
	}

	// only whole numbers
	if($type == "number" || $type == "id") {
		$result = preg_match('/^[0-9]+$/', $string);
	}

	// only whole numbers
	if($type == "float") {
		$result = filter_var($string, FILTER_VALIDATE_FLOAT);
	}

	// only letters space and dash
	if($type == "letters") {
		$result = preg_match("/^[a-zA-Z\s\-]+$/", $string);
	}

	if($type == "letters_utf8") {
		$result = preg_match("/^[a-zA-Z\p{L}\s\-]+$/u", $string);
	}

	// persone code in latvia 123456-12345
	if($type == "perscode") {
		$result = preg_match("/[0-9]{6}-[0-9]{5}$/", $string);
	}

	// valideate email.. this is not perfect
	if($type == "email") {
		if(filter_var($string, FILTER_VALIDATE_EMAIL)) {
			$result = preg_match("/^[a-zA-Z0-9\.\_\-\@\+]+$/", $string);
		}
	}

	// phone number
	if($type == "phone") {
		$result = preg_match("/^[+]?[0-9\s]+$/", $string);
	}
	
	// text field
	if($type == "text" || $type == "subject" || $type == "title") {
		// first check if any letter character has been entered
		if(preg_match('/\w/u', $string)) {
			$result = preg_match("/^[a-zA-Z0-9\p{L}\s\-\'\"\?\!\:\;\)\(\.\,\+\/]+$/u", $string);
		}
	}

	// phone number
	if($type == "color") {
		$result = preg_match('/#([a-fA-F0-9]{3}){1,2}\b/', $string);
	}

	/**
	 * Latitude and Langtitiude
	 * @source https://stackoverflow.com/a/22007205
	 */

	if($type == "lat") {
		$result = preg_match('/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/', $string);
	}

	if($type == "lng") {
		$result = preg_match('/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/', $string);
	}

	/**
	 * Check URL
	 * @source https://gist.github.com/dperini/729294
	 */

	if($type == "url") {
		$result = preg_match('%^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@|\d{1,3}(?:\.\d{1,3}){3}|(?:(?:[a-z\d\x{00a1}-\x{ffff}]+-?)*[a-z\d\x{00a1}-\x{ffff}]+)(?:\.(?:[a-z\d\x{00a1}-\x{ffff}]+-?)*[a-z\d\x{00a1}-\x{ffff}]+)*(?:\.[a-z\x{00a1}-\x{ffff}]{2,6}))(?::\d+)?(?:[^\s]*)?$%iu', $string);
	}

	// --------------------------------
	// return the result
	// --------------------------------
	return $result;

}





// -- function.validate_string.php