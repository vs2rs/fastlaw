<?php

/**
 * Get img src from db
 * @param int $id - image id to get from db
*/
function get_image($id, $class = false) {

	// check if id is set
	if($id != NULL && $id != '') {

		// get db
		$db = Database::getInstance();

		// db query
		$db_query = "SELECT *
		FROM {{prefix}}images
		WHERE id = $id LIMIT 1";

		if($image = $db->getRow($db_query)) {

			$folder_id = $image->parent;
			
			$db_query = "SELECT folder
			FROM {{prefix}}images_folders
			WHERE id = '$folder_id' LIMIT 1";

			if($folder = $db->get_one($db_query)) {

				// setup src
				$image->src = APP_UPLOADS . $folder . $image->src;

				// alt tag setup
				if(isset($image->alt) && $image->alt != '') {
					
					// default
					$image->alt = htmlspecialchars(stripslashes($image->alt), ENT_QUOTES, 'UTF-8');

				} else {

					// create an empty tag
					$image->alt = '';

					// set the lang alt tag
					$alt_tag = "alt_" . Config::read("lang_current");

					// check it
					if(isset($image->$alt_tag)) {
						$image->alt = htmlspecialchars(stripslashes($image->$alt_tag), ENT_QUOTES, 'UTF-8');
					}

				}

				// get extension
				$image->extension = pathinfo($image->src, PATHINFO_EXTENSION);

				// check if file exists
				if(file_exists($image->src)) {

					// setup class if set
					if($class) {
						$class = ' class="'.$class.'"';
					} else {
						$class = '';
					}

					// create svg tag
					if($image->extension == 'svg') {

						// read the file
						$svg_file = file_get_contents($image->src);

						// add class to the tag
						if($class) {
							$svg_file = str_replace("<svg", "<svg$class", $svg_file);
						}

						// new dom element and load the file
						$doc = new DOMDocument();
						$doc->loadXML($svg_file);
						$svg = $doc->getElementsByTagName('svg');
						
						// return the tag
						$image->svg = $svg->item(0)->C14N();

					}

					// mime content type
					$image->mime = mime_content_type($image->src);

					// get the sizes
					list($image->width, $image->height, $image->type, $image->attr) = getimagesize($image->src);

					// create imgae tag
					$image->tag = '<img' . $class . ' src="' . $image->src . '" alt="' . $image->alt . '" />';

					// set up function
					$image->template = isset($image->svg) ? $image->svg : $image->tag;

					// return
					return $image;

				} else {

					// error log
					php_error_log("file " . $image->src . " not found");

				}

			} else {

				// no folder
				php_error_log("folder with id \"$folder_id\" not found in the database");

			}

		} else {

			// no image
			php_error_log("image with id \"$id\" not found in the database");

		}

	}

	// return empty
	return false;

}





// -- function.get_image.php --