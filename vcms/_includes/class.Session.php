<?php

/**
 * Create a browser session
 * @source https://gist.github.com/Nilpo/5449999
*/

class Session {

	// vars
	private static $SESSION_NAME = false;
	private static $SESSION_AGE = false;

	/**
	 * Initializes a new secure session or resumes an existing session.
	 * 
	 * @return boolean Returns true upon success and false upon failure.
	 */
	private static function _init($session_name = false) {

		// Check if sessions are enabled in php
		// Requires PHP 5.4.0
		if(function_exists('session_status')) {
			if(session_status() == PHP_SESSION_DISABLED) {
				die('Sessions are disabled in PHP');
			}
		}
		
		// Check if session set
		if(session_id() === '') {

			// https and restricted http access only
			$secure = true;
			$httponly = true;

			// Disallow session passing as a GET parameter.
			// Requires PHP 4.3.0
			if(ini_set('session.use_only_cookies', 1) === false) {
				die("Can't set session.use_only_cookies");
			}

			// Mark the cookie as accessible only through the HTTP protocol.
			// Requires PHP 5.2.0
			if(ini_set('session.cookie_httponly', 1) === false) {
				die("Can't set session.cookie_httponly");
			}

			// Ensure that session cookies are only sent using SSL.
			// Requires a properly installed SSL certificate.
			// Requires PHP 4.0.4 and HTTPS
			if(ini_set('session.cookie_secure', 1) === false) {
				die("Can't set session.cookie_secure");
			}

			// set session name if needed
			if(self::$SESSION_NAME) {
				session_name(self::$SESSION_NAME);
			}

			$params = session_get_cookie_params();
			session_set_cookie_params(
				$params['lifetime'],
				$params['path'],
				$params['domain'],
				$secure,
				$httponly
			);

			// start the session
			return session_start();

		}

		// session already started
		return session_id();
		
	}

	/**
	* Expires a session if it has been inactive for a specified amount of time.
	* 
	* @return void
	*/
	private static function _age() {

		// get last active param
		$last = isset($_SESSION['LAST_ACTIVE']) ? $_SESSION['LAST_ACTIVE'] : false;

		// check it time has passed and destroy session
		if(false !== $last && (time() - $last > self::$SESSION_AGE)) {
		    self::destroy();
		}

		// update param
		$_SESSION['LAST_ACTIVE'] = time();

	}

	/**
	 * Reads a specific value from the current session data.
	 * 
	 * @param string $key String identifier.
	 * @param boolean $child Optional child identifier for accessing array elements.
	 * @return mixed Returns a string value upon success.  Returns false upon failure.
	 */
	public static function read($key, $child = false) {
		
		// key is string
		if(!is_string($key)) {
			die ("Session key must be string value");
		}

		// init
		self::_init();

		// check for key
		if(isset($_SESSION[$key])) {
			if(false == $child) {
				return $_SESSION[$key];
			} else {
				if(isset($_SESSION[$key][$child])) {
					return $_SESSION[$key][$child];
				}
			}
		}

		// nothing found
		return false;

	}

	/**
	 * Writes a value to the current session data.
	 * 
	 * @param string $key String identifier.
	 * @param mixed $value Single value or array of values to be written.
	 * @return mixed Value or array of values written.
	 */
	public static function write($key, $value) {

		// key is string
		if(!is_string($key)) {
			die("Session key must be string value");
		}

		// init
		self::_init();

		// set the key
		$_SESSION[$key] = $value;

		// return
		return $value;

	}

	/**
     * Deletes a value from the current session data.
     * 
     * @param string $key String identifying the array key to delete.
     * @return void
     */
    public static function delete($key)
    {
        if(!is_string($key)){
            die('Session key must be string value');
        }

        // init
        self::_init();

        // remove
        unset($_SESSION[$key]);

    }

	/**
	 * Echos current session data.
	 * 
	 * @return void
	 */
	public static function dump() {
		print_it($_SESSION);
	}

	/**
	 * Removes session data and destroys the current session.
	 * 
	 * @return void
	 */
    public static function destroy() {

        if(session_id() !== '') {
            
            $_SESSION = array();

            // If it's desired to kill the session, also delete the session cookie.
            // Note: This will destroy the session, and not just the session data!
            if(ini_get("session.use_cookies")) {
                $params = session_get_cookie_params();
                setcookie(
                	( self::$SESSION_NAME ? self::$SESSION_NAME : session_name() ),
                	'', time() - 42000,
                    $params["path"],$params["domain"],
                    $params["secure"], $params["httponly"]
                );
            }

            session_destroy();

        }

    }
	
	/**
	 * Update session name var
	 * 
	 * @param $session_name - string - name of the session
	 * @return void
	 */
    public static function session_name($session_name) {
    	self::$SESSION_NAME = $session_name;
    }
	
	/**
	 * Update session age var
	 * 
	 * @param $session_age - int - seconds
	 * @return void
	 */
    public static function session_age($session_age) {
    	self::$SESSION_AGE = $session_age;
    }

}





// -- class.Session.php --