<?php

/**
 * ------------------------------------
 * Validate data
 * @param $file - file to validate
 * ------------------------------------
 */

function validate_file($file, $mimetypes) {

	// our list
	$supported_mimetypes = array();

	// set it up first we need to check some stuff
	foreach ($mimetypes as $type) {

		// check what we got
		if($type == 'image/*') {
			// add all the supported images
			$supported_mimetypes[] = 'image/jpeg';
			$supported_mimetypes[] = 'image/png';
			$supported_mimetypes[] = 'image/gif';
			$supported_mimetypes[] = 'image/tiff';
			$supported_mimetypes[] = 'image/webp';
			$supported_mimetypes[] = 'image/bmp';
		} else {
			// add to the list
			$supported_mimetypes[] = $type;
		}

	}

	// result
	$result = false;
	
	// get the mime type with finfo
	$check_mime = new finfo();
	$check_mime = $check_mime->file($file, FILEINFO_MIME_TYPE);

	if(in_array($check_mime, $supported_mimetypes)) {
		return true;
	}

	return false;

}





// -- function.validate_file.php