<?php

/**
 * Authenticate users 
 * @author vs2rs
*/

class vcmsAuth {

	/**
	 * Vars
	*/

	private static $error = false;
	private static $password_options = ['cost' => 10];

	/**
	 * Login stuff
	*/

	/**
	 * Check if user is loged in
	 * 
	 * @return mixed - true or load login screen
	*/
	public static function is_loged_in() {

		// check ip
		self::check_ip();

		// set session name
		self::session_name();

		// check the finger print
		if(Session::read("vcms_fingerprint") === self::fingerprint()) {

			// todo - what do we do and where?
			if(Session::read("vcms_2fa_auth") === true) {

				// include the login form
				include_once(VCMS_PATH . '/admin/login_2fa.php');

				// exit
				exit;

			} else {
			
				// all good
				return true;

			}

		} else {

			// destroy session
			self::destroy_session();

			// include the login form
			include_once(VCMS_PATH . '/admin/login.php');

			// exit
			exit;

		}

	}

	/**
	 * Fingerprint
	 * 
	 * @return string
	*/
	private static function fingerprint() {
		return md5(
			$_SERVER['HTTP_USER_AGENT'] .
			Session::read('vcms_auth_id') .
			ip2long(self::get_ip())
		);
	}

	/**
	 * Unique Session ID generated randomly on first load
	 * 
	 * @return string
	*/
	private static function vcms_auth_id() {
		return substr(str_shuffle(md5(APP_NAME.time().session_id())),-8);
	}

	/**
	 * Login function that is called on POST
	 * 
	 * @param $username - string - username to check
	 * @param $password - string - password to check
	 * @return bool
	*/
	public static function login($username, $password) {

		// check if ip is not banned
		self::check_ip();

		// get the db
		$db = Database::getInstance();

		// default auth state
		$auth = false;

		// read config what we are using
		$auth_type = Config::read('vcms_auth_type');

		// if 2fa then use email as auth type since we need to send a email
		if($auth_2fa = Config::read('vcms_auth_2fa')) {
			$auth_type = 'email';
		}

		// check if valid username
		switch ($auth_type) {
			case 'email':
				$username_valid = validate_string($username, 'email');
				break;
			case 'username':
				$username_valid = validate_string($username, 'username');
				break;
			default:
				$username_valid = false;
				break;
		}

		// check username or email
		if($username_valid) {

			// set query
			$db_query = "SELECT user_id as id, username, password, email
			FROM {{prefix}}users
			WHERE $auth_type = '$username'
			AND active = 1 LIMIT 1";

			// if user exists
			if($user = $db->get_row($db_query)) {

				// first let's check md5 - old passwords
				if(strpos($user->password, '$') !== 0) {
					
					// old md5
					$auth = md5($password) === $user->password;

				} else {

					// check for new hashed password
					$auth = password_verify($password, $user->password);

				}

				// check if passwords need rehashing
				if($auth && self::password_check_rehash($user->password)) {

					// new hash
					$password_new = self::password_create($password);

					// update database
					$db->update("
						UPDATE {{prefix}}users
						SET password = '$password_new'
						WHERE user_id = " . $user->id
					);

				}

				// create session if auth success
				if($auth) {

					// set session name
					self::session_name();

					// add user to the session
					Session::write("cms_admin_user_id", $user->id);
					Session::write("cms_admin_username", $username);
					Session::write("login_user_username", $user->username);

					// generate random session id
					Session::write('vcms_auth_id', self::vcms_auth_id());

					// create a fingerprint for this user
					Session::write("vcms_fingerprint", self::fingerprint());

					// remove ip from blocked ip's if a unsecsefull try was loged
					$ip = self::get_ip();
					$db->delete_row("
						DELETE FROM {{prefix}}users_banned
						WHERE ip = '$ip'
					");

					// check for 2FA
					if($auth_2fa) {

						// set login to 2fa
						Session::write("vcms_2fa_auth", true);

						// create a 2fa password
						self::password_2fa($user->id, $user->email);

					}

					// add to admin log if 2fa not active
					// if 2fa then login is not yet complete here
					if(!$auth_2fa) {
						self::admin_log('Login');
					}
					
					// regenerate session id
					session_regenerate_id(true);

					// all good
					return true;

				}

			}

		}

		// set the error message
		self::$error = "Fail! Check your login information!";

		// check if we count the login tries
		if(Config::read('vcms_auth_tries')) {

			// set params
			$ip = self::get_ip();

			// set query
			$db_query = "SELECT ip, tries FROM {{prefix}}users_banned
			WHERE ip = '$ip' LIMIT 1";

			// if user exists
			if($access = $db->get_row($db_query)) {

				// add to count
				$tries = $access->tries + 1;

				// update database
				$db->update("UPDATE {{prefix}}users_banned
				SET tries = $tries WHERE ip = '" . $access->ip . "'");

			} else {

				// insert database
				$db->insert_row("INSERT INTO {{prefix}}users_banned
				(ip, tries) VALUES ('$ip', 1)");

			}

		}

		// no user found
		return false;

	}

	/**
	 * Login with the 2fa code
	 * 
	 * @param $code - string - code to check
	 * @return bool
	*/
	public static function login_2fa($code) {

		// set session name
		self::session_name();

		// get the user id
		$user_id = Session::read('cms_admin_user_id');

		// get the db
		$db = Database::getInstance();

		// password query
		$db_query = "SELECT password_2fa
		FROM {{prefix}}users
		WHERE user_id = $user_id
		AND active = 1 LIMIT 1";

		// run the query
		if($password_2fa = $db->get_one($db_query)) {

			// check password match
			if($auth = password_verify($code, $password_2fa)) {

				// remove the auth param
				Session::delete('vcms_2fa_auth');

				// regenerate session fingerprint
				Session::write('vcms_auth_id', self::vcms_auth_id());
				Session::write("vcms_fingerprint", self::fingerprint());

				// add to the log
				self::admin_log('Login');
					
				// regenerate session id
				session_regenerate_id(true);

				// all good
				return true;

			}

		}

		// set the error message
		self::$error = "Fail! Check the e-mail for the correct code!";

		// error
		return false;

	}

	/**
	 * Logout stuff
	*/

	/**
	 * Destroy current session
	 * 
	 * @return void
	*/
	private static function destroy_session() {
		
		// set session name
		self::session_name();

		// destroy
		Session::destroy();

	}

	/**
	 * Logout function called on logout in admin
	 * 
	 * @return void
	*/
	public static function logout() {

		// add to the admin log
		self::admin_log('Logout');

		// destroy session
		self::destroy_session();

	}

	/**
	 * Password stuff
	*/

	/**
	 * Create password
	 * 
	 * @param $password - string
	 * @return string - password hash
	*/
	public static function password_create($password) {
		return password_hash(
			$password,
			PASSWORD_DEFAULT,
			self::$password_options
		);
	}

	/**
	 * Check if password nees to be rehashed
	 * 
	 * @param $password - string - password hash
	 * @return bool - true or false
	*/
	private static function password_check_rehash($password) {
		return password_needs_rehash(
			$password,
			PASSWORD_DEFAULT,
			self::$password_options
		);
	}

	/**
	 * Create a 6 random numbers password and add it to db
	 *
	 * @return void
	*/
	private static function password_2fa($user_id, $user_email) {

		// get the db
		$db = Database::getInstance();
		
		// create the number
		$number = mt_rand(100000, 999999);
		
		// create pass for db
		$password_2fa = self::password_create($number);

		// update database
		$db->update("
			UPDATE {{prefix}}users
			SET password_2fa = '$password_2fa'
			WHERE user_id = $user_id
		");

		// setup our email template
		$email_template = get_html(array(

			// content
			'code'	=> $number,
			'www'	=> Config::read('production_www'),

			// template file
			'template' => VCMS_PATH . '/admin/templates/email-2fa.html'

		));

		// set sender etc.
		$email_params = array(
			'subject'		=> 'Administration verification code for ' . Config::read('production_www'),
			'email_to'		=> $user_email,
			'email_from'	=> Config::read('email_from'),
			'email_name'	=> Config::read('email_name'),
		);

		// send the mail
		send_mail($email_params, $email_template);

	}

	/**
	 * IP Stuff
	*/

	/**
	 * Get the user ip address
	 * @return strings - ip address
	*/
    public static function get_ip() {

    	// check for ip
		if(!empty($_SERVER['HTTP_CLIENT_IP'])) {

			// check ip from share internet
			$ip = $_SERVER['HTTP_CLIENT_IP'];

		} else if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			
			// to check ip is pass from proxy
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

		} else {

			// default
			$ip = $_SERVER['REMOTE_ADDR'];

		}

		// results
		return $ip;

	}

	/**
	 * Get the user ip address
	 * 
	 * @return strings - ip address
	*/
    private static function check_ip() {

    	// check if we count the login tries
		if(Config::read('vcms_auth_tries') !== false) {

			// get the db
			$db = Database::getInstance();

			// set params
			$ip = self::get_ip();
			$tries = Config::read('vcms_auth_tries');

			// check if your variable is an integer default to 3
			if($tries === true || !preg_match('/^\d+$/', $tries)) {
				$tries = 3;
			}

			// set query
			$db_query = "SELECT ip FROM {{prefix}}users_banned
			WHERE ip = '$ip' AND tries >= $tries LIMIT 1";
			
			// if found show blocked screen
			if($ip_blocked = $db->get_row($db_query)) {
				die("IP $ip blocked.");
			}

		}

	}

	/**
	 * Misc functions
	*/

	/**
	 * Set session name
	 * 
	 * @return void
	*/
	private static function session_name() {
		if(Config::read('vcms_auth_name')) {
			Session::session_name('vcms_' . Config::read('vcms_auth_name'));
		}
	}

	/**
	 * Add login/logout info to the admin log
	 * 
	 * @param $what - string - login or logout
	 * @return void
	*/
    private static function admin_log($what) {

    	// check value
    	if($what == 'Login' || $what == 'Logout') {

    		// set session name
			self::session_name();

			// get the user
			$user_id = Session::read("cms_admin_user_id");
			$username = Session::read("login_user_username");

			// get the db
			$db = Database::getInstance();

			// insert
			$db->insert_row("
				INSERT INTO {{prefix}}adminlog
				(timestamp, user_id, username, item_id, item_name, action)
				VALUES ('".time()."', $user_id, '$username', $user_id, '$username', 'User $what')
			");

		}

    }

	/**
	 * Return error message
	 * 
	 * @return string
	*/
	public static function get_error() {
		return self::$error;
	}

	/**
	 * Return user id from session
	 * 
	 * @return int
	*/
	public static function get_userid() {
		
		// set session name
		self::session_name();

		// return user id
		return Session::read('cms_admin_user_id');

	}

}





// -- class.vcmsAuth.php --