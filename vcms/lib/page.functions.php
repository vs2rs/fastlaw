<?php
#CMS - CMS Made Simple
#(c)2004 by Ted Kulp (wishy@users.sf.net)
#This project's homepage is: http://cmsmadesimple.sf.net
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#$Id: page.functions.php 6771 2010-11-09 19:40:05Z calguy1000 $

/**
 * Page related functions.  Generally these are functions not necessarily
 * related to content, but more to the underlying mechanisms of the system.
 *
 * @package CMS
 */

/**
 * Gets the userid of the currently logged in user.
 * @return mixed - int or bool
 */
function get_userid() {
	return vcmsAuth::get_userid();
}

/**
 * Loads all permissions for a particular user into a global variable so we don't hit the db for every one.
 *
 * @internal
 * @access private
 * @since 0.8
 * @param int The user id
 * @return void
 */
function load_all_permissions($userid)
{
	global $gCms;
	$db = &$gCms->GetDb();
	$variables = &$gCms->variables;

	$perms = array();

	$query = "SELECT DISTINCT permission_name FROM ".cms_db_prefix()."user_groups ug INNER JOIN ".cms_db_prefix()."group_perms gp ON gp.group_id = ug.group_id INNER JOIN ".cms_db_prefix()."permissions p ON p.permission_id = gp.permission_id INNER JOIN ".cms_db_prefix()."groups gr ON gr.group_id = ug.group_id WHERE ug.user_id = ? AND gr.active = 1";
	$result = &$db->Execute($query, array($userid));
	while ($result && !$result->EOF)
	{
		$perms[] =& $result->fields['permission_name'];
		$result->MoveNext();
	}
	
	if ($result) $result->Close();

	$variables['userperms'] = $perms;
}

/**
 * Checks to see that the given userid has access to the given permission.
 * Members of the admin group have all permissions.
 *
 * @since 0.1
 * @param int The user id
 * @param string The permission name
 * @return boolean
 */
function check_permission($userid, $permname)
{
	$check = false;

	global $gCms;
	$userops =& $gCms->GetUserOperations();
	$adminuser = $userops->UserInGroup($userid,1);

	if (!isset($gCms->variables['userperms']))
	{
		load_all_permissions($userid);
	}

	if (isset($gCms->variables['userperms']))
	{
		if (in_array($permname, $gCms->variables['userperms']) || 
		    $adminuser || ($userid == 1) )
		{
			$check = true;
		}
	}

	return $check;
}


/**
 * Checks that the given userid is the owner of the given contentid.
 * (members of the admin group have all permission)
 *
 * @internal
 * @since 0.1
 * @param   integer  The User ID
 * @param   integer  The content id
 * @param   boolean  use strict checking (ignored)
 * @return  boolean 
 */
function check_ownership($userid, $contentid = '', $strict = false)
{
	$check = false;
	global $gCms;

	$userops =& $gCms->GetUserOperations();
	$adminuser = $userops->UserInGroup($userid,1);
	if( $adminuser ) return true;

	if (!isset($gCms->variables['ownerpages']))
	{
		$db =& $gCms->GetDb();

		$variables = &$gCms->variables;
		$tmpa = array();

		$query = "SELECT content_id FROM ".cms_db_prefix()."content WHERE owner_id = ?";
		$result = &$db->Execute($query, array($userid));

		while ($result && !$result->EOF)
		{
			$tmpa[] = $result->fields['content_id'];
			$result->MoveNext();
		}
		$gCms->variables['ownerpages'] = $tmpa;

		if ($result) $result->Close();
	}

	if (isset($gCms->variables['ownerpages']))
	{
		if (in_array($contentid, $gCms->variables['ownerpages']))
		{
			$check = true;
		}
	}

	return $check;
}

/**
 * Checks that the given userid has access to modify the given
 * pageid.  This would mean that they were set as additional
 * authors/editors by the owner.
 *
 * @internal
 * @since 0.2
 * @param  integer The admin user id
 * @param  integer A valid content id.
 * @return boolean
 */
function check_authorship($userid, $contentid = '')
{
	$check = false;
	global $gCms;

	if (!isset($gCms->variables['authorpages']))
	{
	  author_pages($userid);
	}

	if (isset($gCms->variables['authorpages']))
	{
	  if (in_array($contentid, $gCms->variables['authorpages']))
	    {
	      $check = true;
	    }
	}

	return $check;
}

/**
 * Prepares an array with the list of the pages $userid is an author of
 *
 * @internal
 * @since 0.11
 * @param  integer The user id.
 * @return array   An array of pages this user is an author of.
 */
function author_pages($userid)
{
	global $gCms;
	$db =& $gCms->GetDb();
	$userops =& $gCms->GetUserOperations();
        $variables = &$gCms->variables;
	if (!isset($variables['authorpages']))
	{
		// Get all of the pages this user owns
		$query = "SELECT content_id FROM ".cms_db_prefix()."content WHERE owner_id = ?";
		$data = $db->GetCol($query, array($userid));
		$variables['authorpages'] = $data;
	}

	return $variables['authorpages'];
}

/**
 * Quickly checks that the given userid has access to modify the given
 * pageid.  This would mean that they were set as additional
 * authors/editors by the owner.
 *
 * @since 0.11
 * @internal
 * @param   integer The content id to test with
 * @param   array   A list of the authors pages.
 * @return  boolean
 */
function quick_check_authorship($contentid, $hispages)
{
	$check = false;

	if (in_array($contentid, $hispages))
	{
		$check = true;
	}

	return $check;
}


/**
 * Put an event into the audit (admin) log.  This should be
 * done on most admin events for consistency.
 *
 * @since 0.3
 * @param integer The item id (perhaps a content id, or a record id from a module)
 * @param string  The item name (perhaps Content, or the module name)
 * @param string  The action that needs to be audited
 * @return void
 */
function audit($itemid, $itemname, $action)
{
	global $gCms;
	$db =& $gCms->GetDb();

	$userid = 0;
	$username = '';

	if (isset($_SESSION["cms_admin_user_id"]))
	{
		$userid = $_SESSION["cms_admin_user_id"];
	}
	else
	{
	    if (isset($_SESSION['login_user_id']))
	    {
		$userid = $_SESSION['login_user_id'];
		$username = $_SESSION['login_user_username'];
	    }
	}

	if (isset($_SESSION["cms_admin_username"]))
	{
		$username = $_SESSION["cms_admin_username"];
	}

	if (!isset($userid) || $userid == "") {
		$userid = 0;
	}

	$query = "INSERT INTO ".cms_db_prefix()."adminlog (timestamp, user_id, username, item_id, item_name, action) VALUES (?,?,?,?,?,?)";
	$db->Execute($query,array(time(),$userid,$username,$itemid,$itemname,$action));
}


/**
 * Loads a cache of site preferences so we only have to do it once.
 *
 * @since 0.6
 * @internal
 * @return void
 */
function load_site_preferences()
{
	global $gCms;
	$db = &$gCms->GetDb();
	$siteprefs = &$gCms->siteprefs;

	if ($db)
	{
		$query = "SELECT sitepref_name, sitepref_value from ".cms_db_prefix()."siteprefs";
		$result = &$db->Execute($query);

		while ($result && !$result->EOF)
		{
			$siteprefs[$result->fields['sitepref_name']] = $result->fields['sitepref_value'];
			$result->MoveNext();
		}
		
		if ($result) $result->Close();
	}
}


/**
 * Gets the given site prefernce
 *
 * @since 0.6
 * @param string The preference name
 * @param mixed  The default value if the preference does not exist
 * @return mixed
 */
function get_site_preference($prefname, $defaultvalue = '') {

	$value = $defaultvalue;

	global $gCms;
	$siteprefs =& $gCms->siteprefs;
	
	if (count($siteprefs) == 0)
	{
		load_site_preferences();
	}

	if (isset($siteprefs[$prefname]))
	{
		$value = $siteprefs[$prefname];
	}

	return $value;
}


/**
 * Removes the given site preference
 *
 * @param string Preference name to remove
 * @param boolean Wether or not to remove all preferences that are LIKE the supplied name
 * @return void
 */
function remove_site_preference($prefname,$uselike=false)
{
	global $gCms;
	$db =& $gCms->GetDb();
$db->debug = true;
	$siteprefs = &$gCms->siteprefs;

	$query = "DELETE from ".cms_db_prefix()."siteprefs WHERE sitepref_name = ?";
	if( $uselike == true )
	  {
	    $query = "DELETE from ".cms_db_prefix()."siteprefs WHERE sitepref_name LIKE ?";
		$prefname .= '%';
	  }
	$result = $db->Execute($query, array($prefname));

	if (isset($siteprefs[$prefname]))
	{
		unset($siteprefs[$prefname]);
	}
	
	if ($result) $result->Close();
}


/**
 * Sets the given site perference with the given value.
 *
 * @since 0.6
 * @param string The preference name
 * @param mixed  The preference value (will be stored as a string)
 * @return void
 */
function set_site_preference($prefname, $value)
{
	$doinsert = true;

	global $gCms;
	$db =& $gCms->GetDb();

	$siteprefs = &$gCms->siteprefs;

	$query = "SELECT sitepref_value from ".cms_db_prefix()."siteprefs WHERE sitepref_name = ".$db->qstr($prefname);
	$result = $db->Execute($query);

	if ($result && $result->RecordCount() > 0)
	{
		$doinsert = false;
	}
	
	if ($result) $result->Close();

	if ($doinsert)
	{
		$query = "INSERT INTO ".cms_db_prefix()."siteprefs (sitepref_name, sitepref_value) VALUES (".$db->qstr($prefname).", ".$db->qstr($value).")";
		$db->Execute($query);
	}
	else
	{
		$query = "UPDATE ".cms_db_prefix()."siteprefs SET sitepref_value = ".$db->qstr($value)." WHERE sitepref_name = ".$db->qstr($prefname);
		$db->Execute($query);
	}
	$siteprefs[$prefname] = $value;
}


/**
 * A function to load all user preferences for the specified user ID
 *
 * @internal
 * @access private
 * @param integer The User ID
 * @return void
 */
function load_all_preferences($userid)
{
	global $gCms;
	$db = &$gCms->GetDb();
	$variables = &$gCms->userprefs;

	$query = 'SELECT preference, value FROM '.cms_db_prefix().'userprefs WHERE user_id = ?';
	$result = &$db->Execute($query, array($userid));

	while ($result && !$result->EOF)
	{
		$variables[$result->fields['preference']] = $result->fields['value'];
		$result->MoveNext();
	}
	
	if ($result) $result->Close();
}

/**
 * Gets the given preference for the given userid.
 *
 * @since 0.3
 * @param integer The user id
 * @param string  The preference name
 * @param mixed   The default value if the preference is not set for the given user id.
 * @return mixed.
 */
function get_preference($userid, $prefname, $default='')
{
	global $gCms;
	$db =& $gCms->GetDb();

	$result = '';

	if (!isset($gCms->userprefs))
	{
		load_all_preferences($userid);
	}

	$userprefs = &$gCms->userprefs;
	if (isset($gCms->userprefs))
	{
		if (isset($userprefs[$prefname]))
		{
			$result = $userprefs[$prefname];
		}
		else
		{
			$result = $default;
		}
	}

	return $result;
}

/**
 * Sets the given perference for the given userid with the given value.
 *
 * @since 0.3
 * @param integer The user id
 * @param string  The preference name
 * @param mixed   The preference value (will be stored as a string)
 * @return void
 */
function set_preference($userid, $prefname, $value)
{
	$doinsert = true;

	global $gCms;
	$db =& $gCms->GetDb();

	if (!isset($gCms->userprefs))
	{
		load_all_preferences($userid);
	}


	$userprefs = &$gCms->userprefs;
	$userprefs[$prefname] = $value;

	$query = "SELECT value from ".cms_db_prefix()."userprefs WHERE user_id = ? AND preference = ?";
	$result = $db->Execute($query, array($userid, $prefname));

	if ($result && $result->RecordCount() > 0)
	{
		$doinsert = false;
	}
	
	if ($result) $result->Close();

	if ($doinsert)
	{
		$query = "INSERT INTO ".cms_db_prefix()."userprefs (user_id, preference, value) VALUES (?,?,?)";
		$db->Execute($query, array($userid, $prefname, $value));
	}
	else
	{
		$query = "UPDATE ".cms_db_prefix()."userprefs SET value = ? WHERE user_id = ? AND preference = ?";
		$db->Execute($query, array($value, $userid, $prefname));
	}
}

/**
 * Strips slashes from an array of values.
 *
 * @internal
 * @param array A reference to an array of strings
 * @return reference to the cleaned values
 */
function & stripslashes_deep(&$value) 
{ 
        if (is_array($value)) 
        { 
                $value = array_map('stripslashes_deep', $value); 
        } 
        elseif (!empty($value) && is_string($value)) 
        { 
                $value = stripslashes($value); 
        } 
        return $value;
}

/**
 * A method to create a text area control
 *
 * @internal
 * @access private
 * @param boolean Wether the currently selected wysiwyg area should be enabled (depends on user, and site preferences
 * @param string  The contents of the text area
 * @param string  The name of the text area
 * @param string  An optional class name
 * @param string  An optional ID (HTML ID) value
 * @param string  The optional encoding
 * @param string  Optional style information
 * @param integer Width (the number of columns) (CSS can and will override this)
 * @param integer Hieght (the number of rows) (CSS can and will override this)
 * @param string  A flag to indicate that the wysiwyg should be forced to a different type independant of user settings
 * @param string  The name of the syntax hilighter to use (if empty it is assumed that a wysiwyg text area is requested instead of a syntax hiliter)
 * @param string  Optional additional text to include in the textarea tag
 * @return string
 */
function create_textarea($enablewysiwyg, $text, $name, $classname='', $id='', $encoding='', $stylesheet='', $width='80', $height='15',$forcewysiwyg='',$wantedsyntax='',$addtext='')
{
	global $gCms;
	$result = '';
	
  if (($result=="") && ($wantedsyntax!=''))
	{	  
		reset($gCms->modules);
		// wx old line ==> while (list($key) = each($gCms->modules))
		foreach ($gCms->modules as $key => $value)
		{
			// wx hide: $value =& $gCms->modules[$key];
			if ($gCms->modules[$key]['installed'] == true && //is the module installed?
				$gCms->modules[$key]['active'] == true &&			 //us the module active?
				$gCms->modules[$key]['object']->IsSyntaxHighlighter())   //is it a syntaxhighlighter module module?
			{
				if ($forcewysiwyg=='') {
					if ($gCms->modules[$key]['object']->GetName()==get_preference(get_userid(false), 'syntaxhighlighter')) {
					  $result=$gCms->modules[$key]['object']->SyntaxTextarea($name,$wantedsyntax,$width,$height,$encoding,$text,$addtext);
					}
				} else {
					if ($gCms->modules[$key]['object']->GetName()==$forcewysiwyg) {
					  $result=$gCms->modules[$key]['object']->SyntaxTextarea($name,$wantedsyntax,$width,$height,$encoding,$text,$addtext);
					}
				}
			}
		}
	}

	if ($result == '')
	{
		$result = '<textarea data-url="' . APP_URL . '" name="'.$name.'" cols="'.$width.'" rows="'.$height.'"';
		if ($classname != '')
		{
			$result .= ' class="'.$classname.'"';
		}
		else
                {
		  $result .= ' class="cms_textarea"';
		}
		if ($id != '')
		{
			$result .= ' id="'.$id.'"';
		}
		if( !empty( $addtext ) )
		  {
		    $result .= ' '.$addtext;
		  }

		$result .= '>'.cms_htmlentities($text,ENT_NOQUOTES,get_encoding($encoding)).'</textarea>';
	}

	return $result;
}

/*
 * creates a textarea that does syntax highlighting on the source code.
 * The following also needs to be added to the <form> tag for submit to work.
 * if($use_javasyntax){echo 'onSubmit="textarea_submit(
 * this, \'custom404,sitedown\');"';}
 */
/*
OBSOLETE!!!

function textarea_highlight($use_javasyntax, $text, $name, $class_name="syntaxHighlight", $syntax_type="HTML (Complex)", $id="", $encoding='')
{
    if ($use_javasyntax)
	{
        $text = ereg_replace("\r\n", "<CMSNewLine>", $text);
        $text = ereg_replace("\r", "<CMSNewLine>", $text);
        $text = cms_htmlentities(ereg_replace("\n", "<CMSNewLine>", $text));

        // possible values for syntaxType are: Java, C/C++, LaTeX, SQL,
        // Java Properties, HTML (Simple), HTML (Complex)

        $output = '<applet name="CMSSyntaxHighlight"
            code="org.CMSMadeSimple.Syntax.Editor.class" width="100%">
                <param name="cache_option" VALUE="Plugin">
                <param name="cache_archive" VALUE="SyntaxHighlight.jar">
                <param name="cache_version" VALUE="612.0.0.0">
                <param name="content" value="'.$text.'">
                <param name="syntaxType" value="'.$syntax_type.'">
                Sorry, the syntax highlighted textarea will not work with your
                browser. Please use a different browser or turn off syntax
                highlighting under user preferences.
            </applet>
            <input type="hidden" name="'.$name.'" value="">';

    }
	else
	{
        $output = '<textarea name="'.$name.'" cols="80" rows="24"
            class="'.$class_name.'"';
        if ($id<>"")
            $output.=' id="'.$id.'"';
        $output.='>'.cms_htmlentities($text,ENT_NOQUOTES,get_encoding($encoding)).'</textarea>';
    }

    return $output;
}
*/


/**
 * Creates a string containing links to all the pages.
 *
 * @deprecated
 * @param page - the current page to display
 * @param totalrows - the amount of items being listed
 * @param limit - the amount of items to list per page
 * @return a string containing links to all the pages (ex. next 1,2 prev)
 */
 function pagination($page, $totalrows, $limit)
 {
   	$url = Config::read('url');
	$page_string = "";

	// check if file is set in the url
	if(isset($url[2])) {

		$from = ($page * $limit) - $limit;
		$numofpages = (int)($totalrows / $limit);
		if( ($totalrows % $limit) != 0 ) ++$numofpages;
		if ($numofpages > 1)
		{
			if($page != 1)
			{
				$pageprev = $page-1;
				$page_string .= '<a href="'.$url[2].'?page=1">'.lang('first').'</a>&nbsp;';
				$page_string .= "<a href=\"".$url[2]."?page=$pageprev\">".lang('previous')."</a>&nbsp;";
			}
			else
			{
				$page_string .= lang('first')." ";
				$page_string .= lang('previous')." ";
			}

			$page_string .= '&nbsp;'.lang('page')."&nbsp;$page&nbsp;".lang('of')."&nbsp;$numofpages&nbsp;";

			if(($totalrows - ($limit * $page)) > 0)
			{
				$pagenext = $page+1;
				$page_string .= "<a href=\"".$url[2]."?page=$pagenext\">".lang('next')."</a>&nbsp;";
				$page_string .= '<a href="'.$url[2].'?page='.$numofpages.'">'.lang('last').'</a>';
			}
			else
			{
				$page_string .= lang('next')." ";
				$page_string .= lang('last')." ";
			}
		}

	}
	return $page_string;
 }



/**
 * Returns the currently configured database prefix.
 *
 * @since 0.4
 * @return string
 */
function cms_db_prefix() {
  global $gCms;
  $config = $gCms->GetConfig();
  return $config["db_prefix"];
}


/**
 * Create a dropdown form element containing a list of files that match certain conditions
 *
 * @internal
 * @param string The name for the select element.
 * @param string The directory name to search for files.
 * @param string The name of the file that should be selected
 * @param string A comma separated list of extensions that should be displayed in the list
 * @param string An optional string with which to prefix each value in the output by
 * @param boolean Wether 'none' should be an allowed option
 * @param string Text containing additional parameters for the dropdown element
 * @param string A prefix to use when filtering files
 * @param boolean A flag indicating wether the files matching the extension and the prefix should be included or excluded from the result set
 * @return string
 */
function create_file_dropdown($name,$dir,$value,$allowed_extensions,$optprefix='',$allownone=false,$extratext='',
			      $fileprefix='',$excludefiles=1)
{
  $files = array();
  $files = get_matching_files($dir,$allowed_extensions,true,true,$fileprefix,$excludefiles);
  if( $files === false ) return false;
  $out = "<select name=\"{$name}\" {$extratext}>\n";
  if( $allownone )
    {
      $txt = '';
      if( empty($value) )
	{
	  $txt = 'selected="selected"';
	}
      $out .= "  <option value=\"-1\" $txt>--- ".lang('none')." ---</option>\n";
    }

  foreach( $files as $file )
    {
      $txt = '';
      $opt = $file;
      if( !empty($optprefix) )
	{
	  $opt = $optprefix.'/'.$file;
	}
      if( $opt == $value )
	{
	  $txt = 'selected="selected"';
	}
      $out .= "  <option value=\"{$opt}\" {$txt}>{$file}</option>\n";
    }
  $out .= "</select>";
  return $out;
}

/**
 * Function to return the current page id.
 * 
 * Will return FALSE if in an admin action.
 *
 * @return integer pageid indicating the current page, or FALSE.
 */
function cms_get_current_pageid()
{
  global $gCms;

  if( isset($gCms->variables['page_id']) )
    {
      return (int)$gCms->variables['page_id'];
    }
  return FALSE;
}


/**
 * Create google search & facebook preview HTML by wx
 * @param $params - array of data attributies
 * @return html
 */
function setup_google_search_preview($params) {

	$result = '
	<div class="module-row">
		<div class="module-label">Google Search Preview</div>
		<div class="google-search-preview"';
			foreach ($params as $name => $value) {
				$result .= 'data-'.$name.'="'.$value.'"' . "\n";
			}
	$result .= '></div>
	</div>';

	return $result;

}
function setup_facebook_preview($params, $image_upload = '') {

	$result = '<div class="module-row">';
		$result .= '<div class="module-label">Facebook Share Preview:</div>';
		$result .= '<div class="facebook-preview-container">';
			$result .= '<div class="facebook-share-preview"';
				foreach ($params as $name => $value) {
					$result .= 'data-'.$name.'="'.$value.'"' . "\n";
				}
			$result .= '></div>';
			$result .= $image_upload;
		$result .= '</div>';
	$result .= '</div>';

	return $result;

}

/**
 * Create image upload form wx
 * @param $input - array of input [0] - label [1] - <input> [2] - comment
 * @param $params - array of data attributies
 * @return html
 */
function setup_upload_form(
	$type,
	$input,
	$folder,
	$image_id,
	$images_class,
	$params = false
	) {

	// get cms shit
	global $gCms;
	$config = $gCms->GetConfig();

	// default button text and upload class
	$button_select_text = "Select";
	$button_remove_text = "Remove";
	$image_upload_class = "";

	// get folder data
	$folder_data = setup_file_folder($folder);

	// check if file types is set
	if(!isset($folder_data['file_types'])
	|| $folder_data['file_types'] == "") {
		$file_type = "jpg,png,gif,svg,webp";
	} else {
		$file_type = $folder_data['file_types'];
	}

	if($params) {
		if(isset($params['select_text'])) {
			$button_select_text = $params['select_text'];
		} if(isset($params['remove_text'])) {
			$button_remove_text = $params['remove_text'];
		} if(isset($params['class'])) {
			$image_upload_class = "  " . $params['class'];
		}
	}

	$input[2] = $input[2] == '' ? $input[2] : '<div class="image-upload__comment">' . $input[2] . '</div>';


	if($type == "file") {
		
		$image = '<span>'
			   . setup_image_src($image_id)
			   . '</span>';

	} else {

		$image = isset($image_id) && $image_id != ""
		
			? '<img src="' . APP_URL . '/' . $config['uploads_url'] .
			$folder_data['folder'] .
			setup_image_src($image_id) .
			'" />'
			
			: '';

	}


	// default label
	$label = $input[0];

	// check label language
	if(isset($input['field_name'])
	&& !preg_match('/^-- Add Me/', lang($input['field_name']))) {
		$label = lang($input['field_name']) . ':';
	}

	return '
	<div class="module-col">
		<div class="image-upload'.$image_upload_class.'">
			
			<div class="image-upload__label">'.$label.'</div>
			
			'.$input[2].'
			
			<input type="button" value="'.$button_select_text.'"
			class="button  color-one-bg  select-image  margin-r-half"
			data-folder="'.$folder.'" /><!--

		 --><input type="button" value="'.$button_remove_text.'"
		 	class="button  color-one-bg  remove-image" />

		 	<div class="image-upload__preview 
		 	image-container  ' . $images_class . '">
				'.$image.'
			</div>

			<div class="image-upload__input  image-input">
				'.$input[1].'
			</div>

    	</div>
	</div>
	';

}

function setup_image_src($id) {

	global $gCms;
	$db =& $gCms->GetDb();

	$get_image_src_query = "SELECT src FROM ".cms_db_prefix()."images where id = ?";
	$get_image_src = $db->GetOne($get_image_src_query, array($id));

	return $get_image_src;

}

function setup_file_folder($id) {

	global $gCms;
	$db =& $gCms->GetDb();

	$get_folder_query = "SELECT folder, file_types FROM ".cms_db_prefix()."images_folders where id = ?";
	$get_folder = $db->GetRow($get_folder_query, array($id));

	return $get_folder;

}

/* -------------------------------------------------- *\
 *
 * 		Setup Dropzone Form (wx/vcms)
 *
 * 		@param $gallery - array of gallery info
 * 		@return html
 *
 * -------------------------------------------------- */

function setup_dropzone_new_item($gallery) {
	return '
		<div class="module-container  module-container--no-tabs  dropzone-upload-container  margin-t">
			<div>Augšuplādes forma <strong>"'.$gallery['label'].'"</strong> būs pieejama,
			pēc objekta izveidošanas un saglabāšanas.</div>
		</div>
	';
}

function setup_dropzone($id, $gallery) {

	// if new item
	if($gallery['id'] == 0) {
		return setup_dropzone_new_item($gallery);
	}

	// get cms shit
	global $gCms;
	$config = $gCms->GetConfig();

	if(isset($gallery['max_mb'])
	&& $gallery['max_mb']
	&& $gallery['max_mb'] != "") {
		$post_max_size = $gallery['max_mb'];
	} else {
		$post_max_size = $config['max_upload_size'];
	}

	$gallery_output = '
	<div class="margin-t">
		<div class="module-container  module-container--no-tabs  dropzone-upload-container">
			
			<!-- title -->
			<div class="module-title">'.$gallery['label'].'</div>

			<div class="dropzone-upload-info">

				<div>Max upload size is ' . $post_max_size . ' MB
				<!-- // Server Memmory limit is ' . str_replace("M", "", ini_get("memory_limit")) . ' MB --></div>';

				if(isset($config['image_sizes'])
				&& is_array($config['image_sizes'])
				&& count($config['image_sizes']) > 0) {
					
					$gallery_output .=
					'<div class="dropzone-dropdown">
					<select onchange="dropzoneSetResizeValue(this);">
						<option value="0">Do not resize images</option>
					';

					foreach ($config['image_sizes'] as $key => $value) {
						$gallery_output .=
						'<option value="'.$key.'">Resize: '.$value['size'].' '.$value['label'].'</option>';
					}

					$gallery_output .=
					'</select></div>';

				}
			
			$gallery_output .=
			'</div> <!-- end upload info -->

			<!-- form -->
			<form action="uploader/index.php?action=upload"
			class="dropzone"
			id="' . $id . '">';
					
				if(isset($gallery['image_db'])
				&& $gallery['image_db']
				&& $gallery['image_db'] != "") {
					$gallery_output .=
					'<input type="hidden" name="module"
					value="' . $gallery['image_db'] . '" />';
				}

				if(isset($gallery['folder'])
				&& $gallery['folder']
				&& $gallery['folder'] != "") {
					$gallery_output .=
					'<input type="hidden" name="folder"
					value="' . $gallery['folder'] . '" />';
				}

				if(isset($gallery['file_types'])
				&& $gallery['file_types']
				&& $gallery['file_types'] != "") {
					$gallery_output .= 
					'<input type="hidden" name="file_types"
					value="' . $gallery['file_types'] . '" />';
				}

				if(isset($gallery['sort'])
				&& $gallery['sort']
				&& $gallery['sort'] != "") {
					$gallery_output .= 
					'<input type="hidden" name="sort"
					value="' . $gallery['sort'] . '" />';
				}

				$gallery_output .=
				'<input type="hidden" name="max_mb"
				value="' . $post_max_size . '" />';

				$gallery_output .=
				'<input type="hidden" name="parent"
				value="' . $id . '" />';

				$gallery_output .=
				'<input type="hidden" name="resize" value="0" />';

			// end
			$gallery_output .=
			'</form>';

			if(isset($gallery['reorder']) && $gallery['reorder']) {

				$gallery_output .= '
				<div class="dropzone-sort-buttons">
					<button class="button  color-one-bg" onclick="dropzoneSortable(this);">Reorder</button>
				</div>
				';

			}

		$gallery_output .= '</div>
	</div>
	';

	return $gallery_output;

}


# vim:ts=4 sw=4 noet
?>
