<?php // -*- mode:php; tab-width:4; indent-tabs-mode:t; c-basic-offset:4; -*-
#CMS - CMS Made Simple
#(c)2004-2010 by Ted Kulp (ted@cmsmadesimple.org)
#This project's homepage is: http://cmsmadesimple.org
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#BUT withOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#$Id: class.admintheme.inc.php 6786 2010-11-13 21:09:11Z calguy1000 $

/**
 * @package CMS 
 */

/**
 * Class for Admin Theme
 *
 * @package CMS
 * @version $Revision$
 * @license GPL
 */
class AdminTheme
{

	/**
	 * CMS handle
	 */
	var $cms;

	/**
	 * Title
	 */
	var $title;

	/**
	 * Subtitle, for use in breadcrumb trails
	 */
	var $subtitle;

	/**
	 * Url
	 */
	var $url;
	
	/**
	 * Script
	 */
	var $script;

	/**
	 * Query String, for use in breadcrumb trails
	 */
	var $query;

	/**
	 * Aggregation of modules by section
	 */
	var $modulesBySection;

	/**
	 * count of modules in each section
	 */
	var $sectionCount;

	/**
	 * Aggregate Permissions
	 */
	var $perms;

	/**
	 * Recent Page List
	 */
	var $recent;

	/**
	 * Current Active User
	 */
	var $user;

	/**
	 * Admin Section Menu cache
	 */
	var $menuItems;

	/**
	 * Admin Section Image cache
	 */
	var $imageLink;

	/**
	 * Theme Name
	 */
	var $themeName;

	/**
	 * Breadcrumbs Array
	 */
	var $breadcrumbs;

	/**
	 * Notification Items
	 */
	var $_notificationitems;

	/**
	 * TODO:
	 * pie __construct
	 * $cms nav mums vajadzīgs šeit, jo mums ir gCms globālais
	 * $themeName - vajadzēs aizvākt jo mums būs tikai viens theme
	 * userid mēs varam dabūt caur get_userid()
	 */

	/**
	 * Generic constructor.  Runs the SetInitialValues fuction.
	 */
	function __construct($cms = false, $userid = false, $themeName = 'NCleanGrey')
	{
		// get user if not set
		if($userid == false) { $userid = get_userid(); }

		// init
		$this->SetInitialValues($cms, $userid, $themeName);
	}

	/**
	 * Sets object to some sane initial values
	 */
	function SetInitialValues($cms = false, $userid, $themeName = 'NCleanGrey')
	{
	  global $gCms;
	  $contentops = $gCms->GetContentOperations();
	  $dflt_content_id = $contentops->GetDefaultContent();
	  $content_obj = $contentops->LoadContentFromId($dflt_content_id);

		$this->title = '';
		$this->subtitle = '';
		$this->cms = $gCms;
		$this->url = $_SERVER['SCRIPT_NAME'];
		$this->query = (isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'');
		if( $this->query == '' && isset($_POST['mact']) )
		  {
			$tmp = explode(',',$_POST['mact']);
			$this->query = 'module='.$tmp[0];
		  }
		if ($this->query == '' && isset($_POST['module']) && $_POST['module'] != '')
		  {
		  $this->query = 'module='.$_POST['module'];
		  }
		$this->userid = $userid;
		$this->themeName = $themeName;
		$this->perms = array();
		$this->recent = array();
		$this->menuItems = array();
		$this->breadcrumbs = array();
		$this->imageLink = array();
		$this->modulesBySection = array();
		$this->sectionCount = array();
		$this->SetModuleAdminInterfaces();
		$this->SetAggregatePermissions();

		// wx/vcms get the script name
		$this->script = parse_url(
			basename($_SERVER['REQUEST_URI']),
			PHP_URL_PATH
		);
		if($this->script == "cms") {
			$this->script = "index.php";
		}

	}
	
	/**
	 * MenuListSectionModules
	 * This method reformats module information for display in menus. When passed the
	 * name of the admin section, it returns an array of associations:
	 * array['module-name']['url'] is the link to that module, and
	 * array['module-name']['description'] is the language-specific short description of
	 *   the module.
	 *
	 * @param section - section to display
	 */
	function MenuListSectionModules($section)
	{
		$modList = array();
		if (isset($this->sectionCount[$section]) && $this->sectionCount[$section] > 0)
			{
			# Sort modules by name
			$names = array();
			foreach($this->modulesBySection[$section] as $key => $row)
			{
				$names[$key] = $this->modulesBySection[$section][$key]['name'];
			}
			array_multisort($names, SORT_ASC, $this->modulesBySection[$section]);

			foreach($this->modulesBySection[$section] as $sectionModule)
		  {
				$modList[$sectionModule['key']]['url'] = "moduleinterface.php?module=".
		  $sectionModule['key'];
				$modList[$sectionModule['key']]['description'] = $sectionModule['description'];
				$modList[$sectionModule['key']]['name'] = $sectionModule['name'];
		  }
			}
		return $modList;
	}

	/**
	 * SetModuleAdminInterfaces
	 *
	 * This function sets up data structures to place modules in the proper Admin sections
	 * for display on section pages and menus.
	 *
	 */
	function SetModuleAdminInterfaces()
	{
	  global $gCms;
		# Are there any modules with an admin interface?
		$cmsmodules =& $gCms->modules;
		reset($cmsmodules);
		// wx: chagned this line ==> while (list($key) = each($cmsmodules)) to ==>
		foreach($cmsmodules as $key => $value)
		{
			// wx: hide ==> $value =& $cmsmodules[$key];
			if (isset($cmsmodules[$key]['object'])
				&& $cmsmodules[$key]['installed'] == true
				&& $cmsmodules[$key]['active'] == true
				&& $cmsmodules[$key]['object']->HasAdmin()
				&& $cmsmodules[$key]['object']->VisibleToAdminUser())
				{
				$section = $cmsmodules[$key]['object']->GetAdminSection();
				if (! isset($this->sectionCount[$section]))
					{
					$this->sectionCount[$section] = 0;
					}
				$this->modulesBySection[$section][$this->sectionCount[$section]]['key'] = $key;
				if ($cmsmodules[$key]['object']->GetFriendlyName() != '')
					{
					$this->modulesBySection[$section][$this->sectionCount[$section]]['name'] =
					   $cmsmodules[$key]['object']->GetFriendlyName();
					}
				else
					{
					$this->modulesBySection[$section][$this->sectionCount[$section]]['name'] = $key;
					}
				if ($cmsmodules[$key]['object']->GetAdminDescription() != '')
					{
					$this->modulesBySection[$section][$this->sectionCount[$section]]['description'] =
						$cmsmodules[$key]['object']->GetAdminDescription();
					}
				else
					{
					$this->modulesBySection[$section][$this->sectionCount[$section]]['description'] = "";
					}
				$this->sectionCount[$section]++;
				}
			}
	}

	/**
	 * SetAggregatePermissions
	 *
	 * This function gathers disparate permissions to come up with the visibility of
	 * various admin sections, e.g., if there is any content-related operation for
	 * which a user has permissions, the aggregate content permission is granted, so
	 * that menu item is visible.
	 *
	 */
	function SetAggregatePermissions()
	{

		$this->perms['pagePerms'] = (
				check_permission($this->userid, 'pages_edit') ||
				check_permission($this->userid, 'pages_add') ||
				check_permission($this->userid, 'pages_delete')
		);
		$thisUserPages = author_pages($this->userid);
		if (count($thisUserPages) > 0)
			{
			$this->perms['pagePerms'] = true;
			}
		$this->perms['contentPerms'] = $this->perms['pagePerms'] | 
				(isset($this->sectionCount['content']) && $this->sectionCount['content'] > 0);

		# layout        

		$this->perms['templatePerms'] = check_permission($this->userid, 'templates_add') |
				check_permission($this->userid, 'templates_modify') |
				check_permission($this->userid, 'templates_remove');

		$this->perms['layoutPerms'] = $this->perms['templatePerms'] |
				(isset($this->sectionCount['layout']) && $this->sectionCount['layout'] > 0);
	
		# user/group
		$this->perms['userPerms']  = check_permission($this->userid, 'users_add_remove');
		$this->perms['groupPerms'] = check_permission($this->userid, 'users_groups_add_remove');
		$this->perms['groupPermPerms'] =
				check_permission($this->userid, 'users_groups_add_remove');
		$this->perms['groupMemberPerms'] = check_permission($this->userid, 'users_groups_add_remove');
		$this->perms['usersGroupsPerms'] = $this->perms['userPerms'] |
				$this->perms['groupPerms'] |
				$this->perms['groupPermPerms'] |
				$this->perms['groupMemberPerms'] |
				(isset($this->sectionCount['usersgroups']) &&
					$this->sectionCount['usersgroups'] > 0);

		# admin
		$this->perms['sitePrefPerms'] = check_permission($this->userid, 'admin_modify_site_preferences') |
			(isset($this->sectionCount['preferences']) && $this->sectionCount['preferences'] > 0);
		$this->perms['adminPerms'] = $this->perms['sitePrefPerms'] |
			(isset($this->sectionCount['admin']) && $this->sectionCount['admin'] > 0);
		$this->perms['siteAdminPerms'] = $this->perms['sitePrefPerms'] |
				$this->perms['adminPerms'] |
				(isset($this->sectionCount['admin']) &&
					$this->sectionCount['admin'] > 0);


		# extensions
		$this->perms['modulePerms'] = check_permission($this->userid, 'admin_modify_modules');
		$this->perms['extensionsPerms'] = $this->perms['modulePerms'] |
		(isset($this->sectionCount['extensions']) && $this->sectionCount['extensions'] > 0);
	}
	
	/**
	 * HasPerm
	 *
	 * Check if the user has one of the aggregate permissions
	 * 
	 * @param permission the permission to check.
	 */
	function HasPerm($permission)
	{
		if (isset($this->perms[$permission]) && $this->perms[$permission])
		   {
			return true;
		   }
		else
		   {
			return false;
		   }
	}

	/**
	 * OutputHeaderJavascript
	 * This method can be used to dump out any javascript you'd like into the
	 * Admin page header. In fact, it can be used to put just about anything into
	 * the page header. It's recommended that you leave or copy the javascript
	 * below into your own method if you override this -- it's used by the dropdown
	 * menu in IE.
	 */
	function OutputHeaderJavascript()
	{
		echo '<script type="text/javascript" src="//code.jquery.com/jquery-latest.min.js"></script>';
	}

	/**
	 * FixSpaces
	 * This method converts spaces into a non-breaking space HTML entity.
	 * It's used for making menus that work nicely
	 *
	 * @param str string to have its spaces converted
	 */
	function FixSpaces($str)
	{
		$tmp = preg_replace('/\s+/u',"&nbsp;",$str); // PREG UTF8
		if(!empty($tmp)) return $tmp;
		else return preg_replace('/\s+/',"&nbsp;",$str); // bad UTF8
	}
	/**
	 * UnFixSpaces
	 * This method converts non-breaking space HTML entities into char(20)s.
	 *
	 * @param str string to have its spaces converted
	 */
	function UnFixSpaces($str)
	{
		return preg_replace('/&nbsp;/'," ",$str);
	}

	/**
	 * PopulateAdminNavigation
	 * This method populates a big array containing the Navigation Taxonomy
	 * for the admin section. This array is then used to create menus and
	 * section main pages. It uses aggregate permissions to hide sections for which
	 * the user doesn't have permissions, and highlights the current section so
	 * menus can show the user where they are.
	 *
	 * @param subtitle any info to add to the page title
	 *
	 */
	function PopulateAdminNavigation($subtitle='') {
		
		if (count($this->menuItems) > 0) {
			// we have already created the list
			return;
		}

		$this->subtitle = $subtitle;

		debug_buffer('before menu items');
			
		$this->menuItems = array(
			
			// base content menu ---------------------------------------------------------

			'content'=>array('url'=>'index.php','parent'=>-1,
					'title'=>$this->FixSpaces(lang('dashboard')),
					'description'=>lang('contentdescription'),'show_in_menu'=>$this->HasPerm('contentPerms')),

			'pages'=>array(
				'url'=>'pages.php',
				'parent'=>-1,
				'title'=>$this->FixSpaces(lang('pages')),
				'description'=>lang('pagesdescription'),
				'show_in_menu'=>$this->HasPerm('pagePerms')),

			'addcontent'=>array('url'=>'addcontent.php','parent'=>'pages',
					'title'=>$this->FixSpaces(lang('addcontent')),
					'description'=>lang('addcontent'),'show_in_menu'=>false),

			'editpage'=>array('url'=>'editcontent.php','parent'=>'pages',
					'title'=>$this->FixSpaces(lang('editpage')),
					'description'=>lang('editpage'),'show_in_menu'=>false),

			 // wx template menu ---------------------------------------------------------

			'templates' => array(
				'url'			=> 'templates.php',
				'parent'		=> -1,
				'title'			=> $this->FixSpaces(lang('templates')),
				'description'	=> lang('layoutdescription'),
				'show_in_menu'	=> $this->HasPerm('layoutPerms')),

			 // base user/groups menu ---------------------------------------------------------
			'usersgroups' => array(
				'url'           => 'users.php',
				'parent'        => -1,
				'title'         => $this->FixSpaces(lang('users')),
				'description'   => lang('users'),
				'show_in_menu'  => check_permission($this->userid, 'users_add_remove')
			),

				'users' => array(
					'url'           => 'users.php',
					'parent'        => 'usersgroups',
					'title'         => $this->FixSpaces(lang('users')),
					'description'   => lang('users'),
					'show_in_menu'  => check_permission($this->userid, 'users_add_remove')
									&& check_permission($this->userid, 'users_groups_add_remove')
				),

				'groups' => array(
					'url'           => 'users_groups.php',
					'parent'        => 'usersgroups',
					'title'         => $this->FixSpaces(lang('groups')),
					'description'   => lang('groups'),
					'show_in_menu'  => check_permission($this->userid, 'users_groups_add_remove')
				),

				'groupperms' => array(
					'url'           => 'users_groups_permissions.php',
					'parent'        => 'usersgroups',
					'title'         => $this->FixSpaces(lang('group_perms')),
					'description'   => lang('group_perms'),
					'show_in_menu'  => check_permission($this->userid, 'users_groups_add_remove')
				),

			 // base extensions menu ---------------------------------------------------------
			'extensions'=>array('url'=>'listmodules.php','parent'=>-1,
					'title'=>$this->FixSpaces(lang('extensions')),
					'description'=>lang('extensionsdescription'),'show_in_menu'=>$this->HasPerm('extensionsPerms')),
			'modules_list'=>array('url'=>'listmodules.php','parent'=>'extensions',
					'title'=>$this->FixSpaces(lang('modules_title')),
					'description'=>lang('moduledescription'),'show_in_menu'=>$this->HasPerm('modulePerms')),
		// base admin menu ---------------------------------------------------------
		
		'siteadmin'=>array('url'=>'siteprefs.php','parent'=>-1,
			'title'=>$this->FixSpaces(lang('admin')),
			'description'=>lang('admindescription'),
			'show_in_menu'=>$this->HasPerm('siteAdminPerms')
		),
			'siteprefs'=>array('url'=>'siteprefs.php','parent'=>'siteadmin',
				'title'=>$this->FixSpaces(lang('globalconfig')),
				'description'=>lang('preferencesdescription'),
				'show_in_menu'=>$this->HasPerm('sitePrefPerms')
			),
			'adminlog'=>array('url'=>'adminlog.php','parent'=>'siteadmin',
				'title'=>$this->FixSpaces(lang('adminlog')),
				'description'=>lang('adminlogdescription'),
				'show_in_menu'=>$this->HasPerm('adminPerms')
			),
			
		// base my prefs menu ---------------------------------------------------------

			'myprefs' => array(
				'url'           => 'users_add_edit.php?id=' . get_userid(),
				'parent'        => -1,
				'title'         => $this->FixSpaces(lang('user_profile')),
				'description'   => lang('myprefsdescription'),
				'show_in_menu'  => true
			),

			'myprefs_sub' => array(
				'url'           => 'users_add_edit.php?id=' . get_userid(),
				'parent'        => 'myprefs',
				'title'         => $this->FixSpaces(lang('user_profile')),
				'description'   => lang('myprefsdescription'),
				'show_in_menu'  => check_permission($this->userid, 'users_edit_prefs')
			),

			'preferences' => array(
				'url'           => 'users_prefs.php',
				'parent'        => 'myprefs',
				'title'         => $this->FixSpaces(lang('user_prefs')),
				'description'   => lang('adminprefsdescription'),
				'show_in_menu'  => check_permission($this->userid, 'users_edit_prefs')
			),

		);

	// add in all of the 'system' modules too
	global $gCms;
		foreach ($this->menuItems as $sectionKey=>$sectionArray)
	  {
			$tmpArray = $this->MenuListSectionModules($sectionKey);
			$first = true;
			foreach ($tmpArray as $thisKey=>$thisVal)
		  {
				$thisModuleKey = $thisKey;
				$counter = 0;

				// don't clobber existing keys
				if (array_key_exists($thisModuleKey,$this->menuItems))
		  {
			while (array_key_exists($thisModuleKey,$this->menuItems))
			  {
			$thisModuleKey = $thisKey.$counter;
			$counter++;
			  }
		  }

		// if it's not a system module...
		if (array_search($thisModuleKey, $gCms->cmssystemmodules) !== FALSE)
		  {
			$this->menuItems[$thisModuleKey]=array('url'=>$thisVal['url'],
							   'parent'=>$sectionKey,
							   'title'=>$this->FixSpaces($thisVal['name']),
							   'description'=>$thisVal['description'],
							   'show_in_menu'=>true);

//          Commenting out this code ensures that the module is thought of as (built in)
//          if ($first)
//            {
//          $this->menuItems[$thisModuleKey]['firstmodule'] = 1;
//          $first = false;
//            }
//          else
//            {
//          $this->menuItems[$thisModuleKey]['module'] = 1;
//            }
		  }
		  }
	  }
	
	debug_buffer('before module menu items');

	// add in all of the modules
	foreach ($this->menuItems as $sectionKey=>$sectionArray) {

		$tmpArray = $this->MenuListSectionModules($sectionKey);

		$first = true;
		foreach ($tmpArray as $thisKey=>$thisVal) {

			$thisModuleKey = $thisKey;
			$counter = 0;

			// don't clobber existing keys
			if(array_key_exists($thisModuleKey,$this->menuItems)) {
				while (array_key_exists($thisModuleKey,$this->menuItems)) {
					$thisModuleKey = $thisKey.$counter;
					$counter++;
				}
				if( $counter > 0 ) {
					continue;
				}
			}
			
			$this->menuItems[$thisModuleKey]=array('url'=>$thisVal['url'],
							   'parent'=>$sectionKey,
							   'title'=>$this->FixSpaces($thisVal['name']),
							   'description'=>$thisVal['description'],
							   'show_in_menu'=>true);

			if($first) {
				$this->menuItems[$thisModuleKey]['firstmodule'] = 1;
				$first = false;
			} else {
				$this->menuItems[$thisModuleKey]['module'] = 1;
			}
		
		}

	}
	
	debug_buffer('after module menu items');	

	// resolve the tree to be doubly-linked,
	// and make sure the selections are selected            
	foreach ($this->menuItems as $sectionKey=>$sectionArray)
	  {
		  // link the children to the parents; a little clumsy since we can't
		  // assume php5-style references in a foreach.
		  $this->menuItems[$sectionKey]['children'] = array();
		  foreach ($this->menuItems as $subsectionKey=>$subsectionArray)
			  {
				  if ($subsectionArray['parent'] == $sectionKey)
					  {
						  $this->menuItems[$sectionKey]['children'][] = $subsectionKey;
					  }
			  }

		  // set selected
			  // echo "<pre>"; print_r($this->query); echo "</pre>";
		  if ($this->script == 'moduleinterface.php') {
				  $a = preg_match('/(module|mact)=([^&,]+)/',$this->query,$matches);
				  if ($a > 0 && $matches[2] == $sectionKey)
					  {
						  $this->menuItems[$sectionKey]['selected'] = true;
						  $this->title .= $sectionArray['title'];
						  if ($sectionArray['parent'] != -1)
							  {
								  $parent = $sectionArray['parent'];
								  while ($parent != -1)
									  {
										  $this->menuItems[$parent]['selected'] = true;
										  $parent = $this->menuItems[$parent]['parent'];
									  }
							  }
					  }
				  else
					  {
						  $this->menuItems[$sectionKey]['selected'] = false;
					  }
			} else if (strstr($sectionArray['url'],$this->script) !== FALSE &&
				   (!isset($sectionArray['type']) || $sectionArray['type'] != 'external'))
			  {
				  
			  		if($sectionKey == 'myprefs') {

					  	$this->menuItems[$sectionKey]['selected'] = false;

					  	if(isset($_GET['id'])) {
				  			
				  			if($sectionArray['url'] == $this->script . '?id=' . floor($_GET['id'])) {
						  		$this->menuItems[$sectionKey]['selected'] = true;
						  	}

					  	}

			  		} else {
			  			$this->menuItems[$sectionKey]['selected'] = true;	
		  			}

				  $this->title .= $sectionArray['title'];
				  if ($sectionArray['parent'] != -1)
					  {
						  $parent = $sectionArray['parent'];
						  while ($parent != -1)
							  {
								  $this->menuItems[$parent]['selected'] = true;
								  $parent = $this->menuItems[$parent]['parent'];
							  }
					  }
			  }
		  else
			  {
				  $this->menuItems[$sectionKey]['selected'] = false;
			  }
	  }
	// fix subtitle, if any
	if ($subtitle != '')
	  {
		$this->title .= ': '.$subtitle;
	  }
	
	}
	
	/**
	 *  BackUrl
	 *  "Back" Url - link to the next-to-last item in the breadcrumbs
	 *  for the back button.
	 */
	function BackUrl() {
		return 'index.php';
	}

	/**
	 * DoTopMenu
	 * Setup function for displaying the top menu.
	 *
	 */
	function DoTopMenu()
	{
		$this->DisplayTopMenu();
	}

	/**
	 * HasDisplayableChildren
	 * This method returns a boolean, based upon whether the section in question
	 * has displayable children.
	 *
	 * @param section - section to test
	 */
	 function HasDisplayableChildren($section)
	 {
		$displayableChildren=false;
		foreach($this->menuItems[$section]['children'] as $thisChild)
			{
			$thisItem = $this->menuItems[$thisChild];
			if ($thisItem['show_in_menu'])
				{
				$displayableChildren = true;
				}
			}
		return $displayableChildren;
	 }

	/**
	 * TopParent
	 * This method returns the menu node that is the top-level parent of the node you pass
	 * to it.
	 *
	 * @param section - section (menu tag) to find top-level parent
	 */
	 function TopParent($section)
	 {
		$next = $section;
		$node = $this->menuItems[$next];
		while ($node['parent'] != -1)
			{
			$next = $node['parent'];
			$node = $this->menuItems[$next];
			}
		return $next;
	 }



	/**
	 * DisplayAllSectionPages
	 *
	 * Shows all admin section pages and modules. This is used to display the
	 * admin "main" page.
	 *
	 */
	function DisplayAllSectionPages() {

		// get config
		global $gCms;
		$config = $gCms->GetConfig();

		// start
		echo '<div class="module-list-wrap">';
		echo '<div class="module-list">';

		// output var
		$output_content = array();
		$output_content['left'] = array();
		$output_content['right'] = array();

		foreach ($this->menuItems as $thisSection => $menuItem) {

			if($menuItem['parent'] != -1) {
				continue;
			}
			
			if(!$menuItem['show_in_menu']) {
				continue;
			}

			if($thisSection == "pages") {

				$item = $this->menuItems[$thisSection];

				// get the module icon
				$module_icon = $this->DisplayAllSectionIcon("pages");

				$pages_output = 
				'<div class="module-list__item  module-list__item--pages">' .
					'<div class="module-list__icon">' .
						$module_icon .
					'</div>' .
					'<div class="module-list__content">' .
						'<span>' . $item["title"] . '</span>' .
					'</div>' .
					'<a class="module-list__link" href="'.$item["url"].'"></a>' . 
				'</div>';

			}

			// we display only content part
			if($thisSection == "content") {

				if (isset($this->menuItems[$thisSection]['children'])
				&& count($this->menuItems[$thisSection]['children']) > 0
				&& $this->HasDisplayableChildren($thisSection)) {

					// setup
					foreach($this->menuItems[$thisSection]['children'] as $child) {

						// set item
						$item = $this->menuItems[$child];

						if($item["show_in_menu"] == 1) {

							// get the module icon
							$module_icon = $this->DisplayAllSectionIcon($child);

							// set the col
							$module_col = $this->setupModuleGroups($child);
							
							// add to the output
							$output_content[$module_col][] = 
							'<div class="module-list__item  module-list__item--'.$module_col.'">' .
								'<div class="module-list__icon">' .
									$module_icon .
								'</div>' .
								'<div class="module-list__content">' .
									'<span>' . $item["title"] . '</span>' .
								'</div>' .
								'<a class="module-list__link" href="'.$item["url"].'"></a>' . 
							'</div>';

						}

					}
					
				}

				continue;

			} else {
				
				continue;

			}

		}

		// output
		foreach ($output_content as $group_id => $output_group) {
			
			// skip right col
			if($group_id == 'right') continue;

			if($group_id != 'left') {
				echo '<div class="module-list__item  module-list__item--group">' .
					'<div class="module-list__content">' .
						'<span>' . lang($group_id) . '</span>' .
					'</div>' .
				'</div>';
			}

			// output right content
			foreach ($output_group as $section_output) {
				echo $section_output;
			}
		}

		echo '</div>';

		// right col
		echo '<div class="module-list">';

			// output pages link
			echo $pages_output;

			// output right content
			foreach ($output_content['right'] as $section_output) {
				echo $section_output;
			}

		echo '</div>';

		echo '</div>'; // .module-list-wrap

	}

	function DisplayAllSectionIcon($section) {

		// result
		$result = '';

		// get config
		global $gCms;
		$config = $gCms->GetConfig();

		// set the module folder and icon
		$module_folder = $config['modules_project'] . '/' . $section;

		// check if it's a system module
		if(in_array($section, $gCms->cmssystemmodules)) {
			$module_folder = $config['modules_system'] . '/' . $section;
		}
		
		// set icon
		$module_icon = $module_folder . '/icon.svg';

		// update location for pages
		if($section == "pages") {
			$module_folder = VCMS_PATH . '/admin/themes/NCleanGrey/icons';
			$module_icon = $module_folder . '/pages.svg';
		}

		// check the file
		if(file_exists($module_icon)) {
			return get_svg($module_icon);
		}

	}



	function renderMenuSection($section, $depth, $maxdepth)
	{
		if ($maxdepth > 0 && $depth> $maxdepth)
			{
			return;
			}
		if (! $this->menuItems[$section]['show_in_menu'])
			{
			return;
			}
		if (strlen($this->menuItems[$section]['url']) < 1)
			{
			echo "<li>".$this->menuItems[$section]['title']."</li>";
			return;
			}
		echo "<li><a href=\"";
		echo $this->menuItems[$section]['url'];
		echo "\"";
		if (array_key_exists('target', $this->menuItems[$section]))
			{
			echo " target=" . $this->menuItems[$section]['target'];
			}
		if ($this->menuItems[$section]['selected'])
			{
			echo " class=\"selected\"";
			}
		echo ">";
		echo $this->menuItems[$section]['title'];
		echo "</a>";
		if ($this->HasDisplayableChildren($section))
			{
			echo "<ul>";
			foreach ($this->menuItems[$section]['children'] as $child)
				{
				$this->renderMenuSection($child, $depth+1, $maxdepth);
				}
			echo "</ul>";
			}
		echo "</li>";
		return;
	}


	/**
	 * DisplayTopMenu
	 * Output Top Menu data. Over-ride this to alter display of the top menu.
	 *
	 * @param menuItems an array of associated items; each element has a section, title,
	 * url, and selection where title and url are strings, and selection is a boolean
	 * to indicate this is the current selection. You can use the "section" to trap for
	 * javascript links, etc.
	 *
	 * Cruftily written to only support a depth of two levels
	 *
	 */
	function DisplayTopMenu()
	{
		echo "<div id=\"TopMenu\"><ul id=\"nav\">\n";
		foreach ($this->menuItems as $key=>$menuItem)
			{
			if ($menuItem['parent'] == -1)
				{
				$this->renderMenuSection($key, 0, -1);
				}
			}
		echo "</ul></div>\n";
	}

	/**
	 * DisplayFooter
	 * Displays an end-of-page footer.
	 */
	function DisplayFooter() {

		echo '</div><!--end ncleangrey-container-->'."\n";
		//FOOTER
		echo '<div id="footer">&copy; ' . date('Y') . '. All rights reserved.</div>';
		//END

	}
	

	/**
	 * DisplayDocType
	 * If you rewrite the admin section to output pure, beautiful, unadulterated XHTML, you can
	 * change the body tag so that it proudly proclaims that there is none of the evil transitional
	 * cruft.
	 */
	function DisplayDocType()
	{
		echo '<!DOCTYPE html>' . "\n";
	}

	/**
	 * DisplayHTMLStartTag
	 * Outputs the html open tag. Override at your own risk :)
	 */
	function DisplayHTMLStartTag()
	{
		global $gCms;
		$nls =& $gCms->nls;
		echo '<html>' . "\n";
	}
	/**
	 * @since 1.9
	 * ThemeHeader
	 * This method outputs the HEAD section of the html page in the admin Theme section,
	 * after OutputHeaderJavascript() and before $addt.
	 */
	function ThemeHeader(){

		global $gCms;
		$config = $gCms->GetConfig();

		// favicons
		echo '<link rel="shortcut icon" href="favicon_cms.ico" />'."\n";
		echo '<link rel="Bookmark" href="favicon_cms.ico" />';

		// javascripts
		$theme_js_folder = 'js';
		echo '<script src="'.$theme_js_folder.'/admin.js" type="text/javascript"></script>'."\n";
		echo '<script src="'.$theme_js_folder.'/meta-preview.js" type="text/javascript"></script>'."\n";

		// todo: this we need to check what is this shit
		echo '<script src="'.$theme_js_folder.'/standard.js" type="text/javascript"></script>'."\n";

		// ck editor main js file
		$ckeditor_folder = $theme_js_folder . '/ckeditor';
		echo '<script src="'.$ckeditor_folder.'/ckeditor.js?v='.filemtime($config['root_path'] . "/admin/" . $ckeditor_folder.'/ckeditor.js').'" type="text/javascript"></script>'."\n";
		
		// ck editor setup file
		$ck_setup_file = PROJECT_PATH . "/admin/" . $ckeditor_folder . '/setup.js';
		if(file_exists($ck_setup_file)) {

			// timestamp setup file
			$ck_setup_file_timestapm = filemtime($ck_setup_file);

			// output script file
			echo '<script src="'.$ckeditor_folder.'/setup.js?v='.$ck_setup_file_timestapm.'"' .
				 ' type="text/javascript"></script>' . "\n";

		}

		// sortable
		echo '<script src="'.$theme_js_folder.'/sortable.min.js" type="text/javascript"></script>'."\n";

		// dropzone.js
		$dropszone_folder = 'uploader';
		echo '<!-- dropzone and upload shit -->' . "\n";
		echo '<script src="'.$dropszone_folder.'/dropzone.js" type="text/javascript"></script>'."\n";
		echo '<script src="'.$dropszone_folder.'/uploads.js" type="text/javascript"></script>'."\n";
		echo '<link href="'.$dropszone_folder.'/dropzone.css" type="text/css" rel="stylesheet" />'."\n";

		// stylesheets
		$theme_css_folder = 'themes/NCleanGrey/css';
		echo '<!-- css files for the admin panel -->' . "\n";
		echo '<link href="'.$theme_css_folder.'/style.css" type="text/css" rel="stylesheet" media="all" />' . "\n";
		echo '<link href="'.$theme_css_folder.'/002-colors.css" type="text/css" rel="stylesheet" media="all" />' . "\n";
		echo '<link href="'.$theme_css_folder.'/003-header.css" type="text/css" rel="stylesheet" media="all" />' . "\n";
		echo '<link href="'.$theme_css_folder.'/004-buttons.css" type="text/css" rel="stylesheet" media="all" />' . "\n";
		echo '<link href="'.$theme_css_folder.'/005-menu.css" type="text/css" rel="stylesheet" media="all" />' . "\n";
		echo '<link href="'.$theme_css_folder.'/006-module.css" type="text/css" rel="stylesheet" media="all" />' . "\n";
		echo '<link href="'.$theme_css_folder.'/007-google-facebook-preview.css" type="text/css" rel="stylesheet" media="all" />' . "\n";
		echo '<link href="'.$theme_css_folder.'/008-image-upload.css" type="text/css" rel="stylesheet" media="all" />' . "\n";
		echo '<link href="'.$theme_css_folder.'/009-pages-table.css" type="text/css" rel="stylesheet" media="all" />' . "\n";
		echo '<link href="'.$theme_css_folder.'/-001-new-layout.css" type="text/css" rel="stylesheet" media="all" />' . "\n";
		echo '<link href="'.$theme_css_folder.'/-002-new-list.css" type="text/css" rel="stylesheet" media="all" />' . "\n";
		echo '<link href="'.$theme_css_folder.'/-003-new-form.css" type="text/css" rel="stylesheet" media="all" />' . "\n";
		echo '<link href="'.$theme_css_folder.'/-004-new-messages.css" type="text/css" rel="stylesheet" media="all" />' . "\n";
		echo '<link href="'.$theme_css_folder.'/-005-new-tabs.css" type="text/css" rel="stylesheet" media="all" />' . "\n";
		echo '<link href="'.$theme_css_folder.'/-006-new-text.css" type="text/css" rel="stylesheet" media="all" />' . "\n";
		echo '<link href="'.$theme_css_folder.'/-007-new-tables.css" type="text/css" rel="stylesheet" media="all" />' . "\n";
		echo '<link href="'.$theme_css_folder.'/-008-new-pagination.css" type="text/css" rel="stylesheet" media="all" />' . "\n";

	}
	/**
	 * DisplayHTMLHeader
	 * This method outputs the HEAD section of the html page in the admin section.
	 */
	function DisplayHTMLHeader($showielink = false, $addt = '')
	{
		global $gCms;
		$config = $gCms->GetConfig();
?><head>
<base href="<?php echo $config['root_url'] . '/' . $config['admin_dir'] . '/'; ?>" />
<meta name="Generator" content="CMS Made Simple - Copyright (C) 2004-9 Ted Kulp. All rights reserved." />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="noindex, nofollow" />
<title><?php echo $this->cms->siteprefs['sitename'] ." - ". strip_tags(str_replace(array("<br/>","<br>","<br />"), " ", $this->title)) ?></title>
<!-- THIS IS WHERE HEADER STUFF SHOULD GO -->
<?php $this->OutputHeaderJavascript(); ?>
<?php $this->ThemeHeader(); ?>
<?php echo $addt ?>

</head>
<?php
	}

	/**
	 * DisplayBodyTag
	 * Outputs the admin page body tag. Leave in the funny text if you want this
	 * to work properly.
	 */
	function DisplayBodyTag()
	{
		echo "<body>\n";
	}

	
	/**
	 * DisplayMainDivStart
	 *
	 * Used to output the start of the main div that contains the admin page content
	 */
	function DisplayMainDivStart()
	{
		echo "<div>\n";
	}


	/**
	 * DisplayMainDivEnd
	 *
	 * Used to output the end of the main div that contains the admin page content
	 */
	function DisplayMainDivEnd()
	{
	  echo '<div class="clearb"></div>';
	  echo "</div><!-- end MainContent -->\n";
	}


	/**
	 * DisplaySectionMenuDivStart
	 * Outputs the open div tag for the main section pages.
	 */
	function DisplaySectionMenuDivStart()
	{
		echo "<div>\n";
	}


	/**
	 * DisplaySectionMenuDivEnd
	 * Outputs the close div tag for the main section pages.
	 */
	function DisplaySectionMenuDivEnd()
	{
		echo "</div><!-- END .MainMenu-->\n";
	}


	/**
	 * AddToDashboard
	 */
	function AddNotification($priority,$module,$html)
	{
	  if( !is_array($this->_notificationitems) )
	{
	  $this->_notificationitems = array(array(),array(),array());
	}
	  if( $priority < 1 ) $priority = 1;
	  if( $priority > 3 ) $priority = 3;

	  $this->_notificationitems[$priority-1][] = array($module,$html);
	}

	/**
	 * Display the available notifications
	 *
	 * @param string $priority Priority threshold
	 * @return void
	 */
	function DisplayNotifications($priority=2)
	{
	  if( !is_array($this->_notificationitems) ) return;
	  
	  // count the total number of notifications
	  $count=0;
	  for( $i = 1; $i <= $priority; $i++ )
	  {
		$count += count($this->_notificationitems[$i-1]);
	  }
	  
	  // Define that is singular or plural
	  $singular_or_plural = $count;
	  
	  if($singular_or_plural > 1)
	  {
	  $notifications = lang('notifications_to_handle',$count);
	  }
	  else
	  {
	  $notifications = lang('notification_to_handle',$count);
	  }
	  
	  echo '<div class="full-Notifications clear">'."\n";
	  echo '<div class="Notifications-title">' . $notifications . '</div>'."\n";
	  echo '<div id="notifications-display" class="notifications-show" onclick="change(\'notifications-display\', \'notifications-hide\', \'notifications-show\'); change(\'notifications-container\', \'invisible\', \'visible\');"></div>'."\n";
	 
	  echo '<div id="notifications-container" class="invisible">'."\n";
	  
	  echo "<ul id=\"Notifications-area\">\n";
	  for( $i = 1; $i <= $priority; $i++ )
	{
	  if( count($this->_notificationitems) < $i ) break;
	  if( count($this->_notificationitems[$i-1]) == 0 ) continue;
	  foreach( $this->_notificationitems[$i-1] as $data )
		{
		  echo '<li class="NotificationsItem NotificationsPriority'.$i.'">';
		  echo '<span class="NotificationsItemModuleName">'."\n";
		  echo $data[0]."\n";
		  echo "</span>\n";
		  echo '<span class="NotificationsItemData">'."\n";
		  echo $data[1]."\n";
		  echo "</span>\n";
		  echo '</li>';       
		}
	}
	  echo "</ul>";
	   echo "</div><!-- notifications-container -->\n";
	  echo "</div><!-- full-Notifications -->\n";
	   echo "<div class=\"clearb\">&nbsp;</div>\n";
	}

	/**
	 * DisplayImage will display the themed version of an image (if it exists),
	 * or the version from the default theme otherwise.
	 * @param imageName - name of image
	 * @param alt - alt text
	 * @param width
	 * @param height
	 * @param class
	 */
	function DisplayImage($imageName, $alt='', $width='', $height='', $class='')
	{
		if (! isset($this->imageLink[$imageName]))
		   {
			if (strpos($imageName,'/') !== false)
			   {
				$imagePath = substr($imageName,0,strrpos($imageName,'/')+1);
				$imageName = substr($imageName,strrpos($imageName,'/')+1);
			   }
			else
			   {
				$imagePath = '';
			   }
			
		   if (file_exists(dirname($this->cms->config['root_path'] . '/' . $this->cms->config['admin_dir'] .
				'/themes/' . $this->themeName . '/images/' . $imagePath . $imageName) . '/'. $imageName))
			   {
				$this->imageLink[$imageName] = 'themes/' .
					$this->themeName . '/images/' . $imagePath . $imageName;
			   }
		   else
			   {
			   $this->imageLink[$imageName] = 'themes/default/images/' . $imagePath . $imageName;
			   }
		   }

		$retStr = '<img src="'.$this->imageLink[$imageName].'"';
		if ($class != '')
			{
			$retStr .= ' class="'.$class.'"';
			}
		if ($width != '')
			{
			$retStr .= ' width="'.$width.'"';
			}
		if ($height != '')
			{
			$retStr .= ' height="'.$height.'"';
			}
		if ($alt != '')
			{
			$retStr .= ' alt="'.$alt.'" title="'.$alt.'"';
			}
		$retStr .= ' />';
		return $retStr;
	}

	/**
	 * DisplayIcon - show icon by WX,
	 * @param iconName - name of the icon
	 */
	function DisplayIcon($iconName)
	{

		// https://www.flaticon.com/packs/interface-icon-assets

		switch ($iconName) {

			case 'active':
				$svg = '<svg class="color-ok-fill" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 490.4 490.4" xml:space="preserve"> <path d="M309.4,185.5l-94,93.5l-34.3-34.5c-4.8-4.8-12.5-4.8-17.3-0.1c-4.8,4.7-4.8,12.5-0.1,17.3l42.9,43.2 c2.4,2.4,5.5,3.6,8.7,3.6c3.1,0,6.2-1.2,8.6-3.6l102.7-102.1c4.8-4.8,4.8-12.5,0.1-17.3C321.9,180.7,314.2,180.7,309.4,185.5z"/> </svg>';
				break;

			case 'true':
				$svg = '<svg class="color-ok-fill" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 490.4 490.4" xml:space="preserve"> <g> <path d="M245.2,0C110,0,0,110,0,245.2s110,245.2,245.2,245.2s245.2-110,245.2-245.2S380.4,0,245.2,0z M245.2,465.9 c-121.7,0-220.7-99-220.7-220.7s99-220.7,220.7-220.7s220.7,99,220.7,220.7S366.9,465.9,245.2,465.9z"/> <path d="M309.4,185.5l-94,93.5l-34.3-34.5c-4.8-4.8-12.5-4.8-17.3-0.1c-4.8,4.7-4.8,12.5-0.1,17.3l42.9,43.2 c2.4,2.4,5.5,3.6,8.7,3.6c3.1,0,6.2-1.2,8.6-3.6l102.7-102.1c4.8-4.8,4.8-12.5,0.1-17.3C321.9,180.7,314.2,180.7,309.4,185.5z"/> </g> </svg>';
				break;

			case 'false':
				$svg = '<svg class="color-cancel-fill" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 490.4 490.4" xml:space="preserve"> <g> <path d="M245.2,490.4c135.2,0,245.2-110,245.2-245.2S380.4,0,245.2,0S0,110,0,245.2S110,490.4,245.2,490.4z M245.2,24.5 c121.7,0,220.7,99,220.7,220.7s-99,220.7-220.7,220.7s-220.7-99-220.7-220.7S123.5,24.5,245.2,24.5z"/> <path d="M180.3,310.1c2.4,2.4,5.5,3.6,8.7,3.6s6.3-1.2,8.7-3.6l47.6-47.6l47.6,47.6c2.4,2.4,5.5,3.6,8.7,3.6s6.3-1.2,8.7-3.6 c4.8-4.8,4.8-12.5,0-17.3l-47.8-47.6l47.6-47.6c4.8-4.8,4.8-12.5,0-17.3s-12.5-4.8-17.3,0l-47.6,47.6l-47.6-47.6 c-4.8-4.8-12.5-4.8-17.3,0s-4.8,12.5,0,17.3l47.6,47.6l-47.6,47.6C175.5,297.6,175.5,305.3,180.3,310.1z"/> </g> </svg>';
				break;

			case 'up':
				$svg = '<svg class="color-one-fill" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 490.4 490.4" xml:space="preserve"> <g> <path d="M245.2,490.4c135.2,0,245.2-110,245.2-245.2S380.4,0,245.2,0S0,110,0,245.2S110,490.4,245.2,490.4z M245.2,24.5 c121.7,0,220.7,99,220.7,220.7s-99,220.7-220.7,220.7s-220.7-99-220.7-220.7S123.5,24.5,245.2,24.5z"/> <path d="M185,216.2l48-48v183.4c0,6.8,5.5,12.3,12.3,12.3c6.8,0,12.3-5.5,12.3-12.3V168.3l48,48c2.4,2.4,5.5,3.6,8.7,3.6 s6.3-1.2,8.7-3.6c4.8-4.8,4.8-12.5,0-17.3l-68.9-68.9c-4.8-4.8-12.5-4.8-17.3,0L167.9,199c-4.8,4.8-4.8,12.5,0,17.3 C172.4,221,180.2,221,185,216.2z"/> </g> </svg>';
				break;

			case 'down':
				$svg = '<svg class="color-one-fill" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 490.4 490.4" xml:space="preserve"> <g> <path d="M490.4,245.2C490.4,110,380.4,0,245.2,0S0,110,0,245.2s110,245.2,245.2,245.2S490.4,380.4,490.4,245.2z M24.5,245.2 c0-121.7,99-220.7,220.7-220.7s220.7,99,220.7,220.7s-99,220.7-220.7,220.7S24.5,366.9,24.5,245.2z"/> <path d="M253.9,360.4l68.9-68.9c4.8-4.8,4.8-12.5,0-17.3s-12.5-4.8-17.3,0l-48,48V138.7c0-6.8-5.5-12.3-12.3-12.3 s-12.3,5.5-12.3,12.3v183.4l-48-48c-4.8-4.8-12.5-4.8-17.3,0s-4.8,12.5,0,17.3l68.9,68.9c2.4,2.4,5.5,3.6,8.7,3.6 S251.5,362.8,253.9,360.4z"/> </g> </svg>';
				break;

			case 'delete':
				$svg = '<svg class="color-one-fill" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 482.428 482.429" xml:space="preserve"> <g> <path d="M381.163,57.799h-75.094C302.323,25.316,274.686,0,241.214,0c-33.471,0-61.104,25.315-64.85,57.799h-75.098 c-30.39,0-55.111,24.728-55.111,55.117v2.828c0,23.223,14.46,43.1,34.83,51.199v260.369c0,30.39,24.724,55.117,55.112,55.117 h210.236c30.389,0,55.111-24.729,55.111-55.117V166.944c20.369-8.1,34.83-27.977,34.83-51.199v-2.828 C436.274,82.527,411.551,57.799,381.163,57.799z M241.214,26.139c19.037,0,34.927,13.645,38.443,31.66h-76.879 C206.293,39.783,222.184,26.139,241.214,26.139z M375.305,427.312c0,15.978-13,28.979-28.973,28.979H136.096 c-15.973,0-28.973-13.002-28.973-28.979V170.861h268.182V427.312z M410.135,115.744c0,15.978-13,28.979-28.973,28.979H101.266 c-15.973,0-28.973-13.001-28.973-28.979v-2.828c0-15.978,13-28.979,28.973-28.979h279.897c15.973,0,28.973,13.001,28.973,28.979 V115.744z"/> <path d="M171.144,422.863c7.218,0,13.069-5.853,13.069-13.068V262.641c0-7.216-5.852-13.07-13.069-13.07 c-7.217,0-13.069,5.854-13.069,13.07v147.154C158.074,417.012,163.926,422.863,171.144,422.863z"/> <path d="M241.214,422.863c7.218,0,13.07-5.853,13.07-13.068V262.641c0-7.216-5.854-13.07-13.07-13.07 c-7.217,0-13.069,5.854-13.069,13.07v147.154C228.145,417.012,233.996,422.863,241.214,422.863z"/> <path d="M311.284,422.863c7.217,0,13.068-5.853,13.068-13.068V262.641c0-7.216-5.852-13.07-13.068-13.07 c-7.219,0-13.07,5.854-13.07,13.07v147.154C298.213,417.012,304.067,422.863,311.284,422.863z"/> </g> </svg>';
				break;

			case 'expand':
				$svg = '<svg class="color-one-fill" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 612 612" xml:space="preserve"> <g> <path d="M401.625,267.75H344.25v-57.375c0-21.114-17.136-38.25-38.25-38.25s-38.25,17.117-38.25,38.25v57.375h-57.375 c-21.114,0-38.25,17.117-38.25,38.25c0,21.114,17.136,38.25,38.25,38.25h57.375v57.375c0,21.114,17.136,38.25,38.25,38.25 s38.25-17.117,38.25-38.25V344.25h57.375c21.114,0,38.25-17.136,38.25-38.25S422.739,267.75,401.625,267.75z M306,0 C136.992,0,0,136.992,0,306s136.992,306,306,306s306-136.992,306-306S475.008,0,306,0z M306,554.625 C168.912,554.625,57.375,443.088,57.375,306S168.912,57.375,306,57.375S554.625,168.912,554.625,306S443.088,554.625,306,554.625 z"/> </g> </svg>';
				break;

			case 'contract':
				$svg = '<svg class="color-one-fill" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 612 612" xml:space="preserve"> <g> <path d="M401.625,267.75h-191.25c-21.114,0-38.25,17.117-38.25,38.25c0,21.114,17.136,38.25,38.25,38.25h191.25 c21.114,0,38.25-17.136,38.25-38.25S422.758,267.75,401.625,267.75z M306,0C136.992,0,0,136.992,0,306s136.992,306,306,306 s306-136.992,306-306S475.008,0,306,0z M306,554.625C168.912,554.625,57.375,443.088,57.375,306S168.912,57.375,306,57.375 S554.625,168.912,554.625,306S443.088,554.625,306,554.625z"/> </g> </svg>';
				break;

			case 'view':
				$svg = '<svg class="color-one-fill" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 511.999 511.999" xml:space="preserve"> <g> <g> <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035 c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201 C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418 c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418 C447.361,287.923,358.746,385.406,255.997,385.406z"></path> </g> </g> <g> <g> <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275 s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516 s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"></path> </g> </g> </svg>';
				break;

			case 'copy':
				$svg = '<svg class="color-one-fill" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 488.3 488.3" xml:space="preserve"> <path d="M314.25,85.4h-227c-21.3,0-38.6,17.3-38.6,38.6v325.7c0,21.3,17.3,38.6,38.6,38.6h227c21.3,0,38.6-17.3,38.6-38.6V124 C352.75,102.7,335.45,85.4,314.25,85.4z M325.75,449.6c0,6.4-5.2,11.6-11.6,11.6h-227c-6.4,0-11.6-5.2-11.6-11.6V124 c0-6.4,5.2-11.6,11.6-11.6h227c6.4,0,11.6,5.2,11.6,11.6V449.6z"/> <path d="M401.05,0h-227c-21.3,0-38.6,17.3-38.6,38.6c0,7.5,6,13.5,13.5,13.5s13.5-6,13.5-13.5c0-6.4,5.2-11.6,11.6-11.6h227 c6.4,0,11.6,5.2,11.6,11.6v325.7c0,6.4-5.2,11.6-11.6,11.6c-7.5,0-13.5,6-13.5,13.5s6,13.5,13.5,13.5c21.3,0,38.6-17.3,38.6-38.6 V38.6C439.65,17.3,422.35,0,401.05,0z"/> </svg>';
				break;

			case 'edit':
				$svg = '<svg class="color-one-fill" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 490.4 490.4" xml:space="preserve"> <path d="M245.2,490.4c135.2,0,245.2-110,245.2-245.2S380.4,0,245.2,0S0,110,0,245.2S110,490.4,245.2,490.4z M245.2,24.5 c121.7,0,220.7,99,220.7,220.7s-99,220.7-220.7,220.7s-220.7-99-220.7-220.7S123.5,24.5,245.2,24.5z"/> <g> <path d="M291.5,211.1c-1.6-1.6-4.5-1.6-6.2,0l-83,83.1c-1,1-1.4,2.4-1.2,3.8c0.2,1.4,1.1,2.6,2.4,3.2l6.8,3.4v11.5l-20.9,6 L168.3,301l6-20.9h11.5l3.4,6.8c0.6,1.3,1.8,2.1,3.2,2.4c0.2,0,0.5,0.1,0.7,0.1c1.1,0,2.3-0.5,3.1-1.3l83-83.1 c0.8-0.8,1.3-1.9,1.3-3.1c0-1.2-0.5-2.3-1.3-3.1l-16.1-16.1c-0.8-0.8-1.9-1.3-3.1-1.3h0c-1.2,0-2.3,0.5-3.1,1.3l-89.1,89.9 c0,0,0,0.1-0.1,0.2c-0.4,0.5-0.8,1-1,1.7l-7.6,26.5l-9.9,34.6c-0.4,1.5,0,3.2,1.1,4.3c0.8,0.8,1.9,1.3,3.1,1.3 c0.4,0,0.8-0.1,1.2-0.2l34.6-9.9l26.5-7.6c0.1,0,0.1-0.1,0.2-0.1c0.3-0.1,0.5-0.2,0.8-0.4c0.2-0.1,0.5-0.3,0.7-0.5 c0.1,0,0.1-0.1,0.2-0.1l89.9-89.1c0.8-0.8,1.3-1.9,1.3-3.1c0-1.2-0.5-2.3-1.3-3.1L291.5,211.1z"/> <path d="M336.1,180.3l-26.2-26.2c-6.6-6.6-18.1-6.6-24.7,0l-16,16.2c-1.7,1.7-1.7,4.5,0,6.2l44.6,44.6c0.9,0.9,2,1.3,3.1,1.3 c1.1,0,2.2-0.4,3.1-1.3l16.2-16c3.3-3.3,5.1-7.7,5.1-12.4S339.4,183.6,336.1,180.3z"/> </g> </svg>';
				break;

		}

		return $svg;

	}


   /**
	* ShowHeader
	* Outputs the page header title along with a help link to that section in the wiki.
	* 
	* @param title_name - page heading title
	* @param extra_lang_param - extra parameters to pass to lang() (I don't think this parm is needed)
	* @param link_text - Override the text to use for the help link.
	* @param module_help_type - FALSE if this is not a module, 'both' if link to
	*                           both the wiki and module help and 'builtin' if link to to the builtin help
	*/
	function ShowHeader($title_name, $extra_lang_param=array(), $link_text = '', $module_help_type = FALSE)
	{
	  $cms = $this->cms;
	  $config = $cms->GetConfig();             
	  $header  = '<div class="pageheader">';
	  if (FALSE != $module_help_type)
	{
		  $module = '';
	  if( isset($_REQUEST['module']) )
		{
		  $module = $_REQUEST['module'];
		}
	  else if( isset($_REQUEST['mact']) )
		{
		  $tmp = explode(',',$_REQUEST['mact']);
		  $module = $tmp[0];
		}
	  $icon = "modules/{$module}/images/icon.gif";
	  $path = cms_join_path($this->cms->config['root_path'],$icon);
	  if( file_exists($path) )
		{
		  $header .= "<img src=\"{$this->cms->config['root_url']}/{$icon}\" class=\"itemicon\" />&nbsp;";
		}
	  $header .= $title_name;
	}
	  else
	{
	  $header .= lang($title_name, $extra_lang_param);
	}

	  $header .= '</div>';
	  // return $header; 
	  // wx/vcms hide this shit
	  return '';   
	}


	/**
	 * _ArraySearchRecursive
	 * recursively descend an arbitrarily deep multidimensional
	 * array, stopping at the first occurence of scalar $needle.
	 * return the path to $needle as an array (list) of keys
	 * if not found, return null.
	 * (will infinitely recurse on self-referential structures)
	 * From: http://us3.php.net/function.array-search
	 */
	function _ArraySearchRecursive($needle, $haystack)
	{

		$path = NULL;

		// wx new loop
		foreach($haystack as $k => $v) {

			if(is_scalar($v)) {
				if($v===$needle) {
					$path = array($k);
				}
			} else if(is_array($v)) {
				if($path=$this->_ArraySearchRecursive($needle, $v)) {
					array_unshift($path, $k);
				}
			}

		}

	   // --------------------------------------------------------------
	   // wx: old stuff ==>
	   // 
	   // $keys = array_keys($haystack);
	   //
	   // while (!$path && (list($toss,$k)=each($keys))) {
	   //   $v = $haystack[$k];
	   //   if (is_scalar($v)) {
	   //       if ($v===$needle) {
	   //         $path = array($k);
	   //       }
	   //   } elseif (is_array($v)) {
	   //       if ($path=$this->_ArraySearchRecursive( $needle, $v )) {
	   //         array_unshift($path,$k);
	   //       }
	   //   }
	   // }

	   return $path;

	}


	/**
	 * ShowError
	 * Outputs supplied errors with a link to the wiki for troublshooting.
	 *
	 * @param errors - array or string of 1 or more errors to be shown
	 * @param get_var - Name of the _GET variable that contains the 
	 *                  name of the message lang string
	 */
	function ShowErrors($errors, $get_var = '', $class = '')
	{

		// config
		global $gCms;
		$config = $gCms->GetConfig();

		// erorr list
		$error_list = '';

		// if array
		if(is_array($errors) && count($errors) > 0) {
			
			// setup
			foreach ($errors as $key => $value) {

				// create template
				$error_list = get_html(array(
					'class' 	=> 'error  error-message--' . $key,
					'text'		=> $value,
					'template'	=> $config['root_path'] . '/admin/templates/message.html'
				));

			}

		} else {
			
			// create template
			$error_list = get_html(array(
				'class' 	=> 'error',
				'text'		=> $errors,
				'template'	=> $config['root_path'] . '/admin/templates/message.html'
			));

		}

		// retrun
		return $error_list;

	}
	
	/**
	 * ShowMessage
	 * Outputs a page status message
	 *
	 * @param message - Message to be shown
	 * @param type - error or success
	 *
	 * @return html
	 */
	function ShowMessage($message, $type = false) {
		
		// config
		global $gCms;
		$config = $gCms->GetConfig();

		// erorr list
		$error_list = '';

		// set the type of message (success - default)
		$type = $type ? $type : 'success';

		// if array
		if(is_array($message) && count($message) > 0) {
			
			// setup
			foreach ($message as $key => $value) {

				// create template
				$error_list .= get_html(array(
					'class' 	=> $type,
					'text'		=> $value,
					'template'	=> $config['root_path'] . "/admin/templates/message.html"
				));

			}

		} else {
			
			// create template
			$error_list = get_html(array(
				'class' 	=> $type,
				'text'		=> $message,
				'template'	=> $config['root_path'] . '/admin/templates/message.html'
			));

		}

		// retrun
		return $error_list;

	}

	/**
	 * Call ShowMessage function
	 */
	function SuccessMessage($message) { return $this->ShowMessage($message); }
	function ErrorMessage($message) { return $this->ShowMessage($message, 'error'); }
	
	/**
	 * Based on the current user's prefernces, get the current theme object.
	 */
	static function &GetThemeObject()
	{
		global $gCms;
		$config = $gCms->GetConfig();
		$themeName = get_preference(get_userid(), 'admintheme', 'default');
		$themeObjectName = $themeName."Theme";
		$userid = get_userid();
	
		if (file_exists(dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR.$config['admin_dir']."/themes/${themeName}/${themeObjectName}.php"))
		{
			include(dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR.$config['admin_dir']."/themes/${themeName}/${themeObjectName}.php");
			$themeObject = new $themeObjectName($gCms, $userid, $themeName);
		}
		else
		{
			$themeObject = new AdminTheme($gCms, $userid, $themeName);
		}

		$gCms->variables['admintheme'] = $themeObject;
		
		return $themeObject;
	
	}
	
	/**
	 * Returns a select list of the pages in the system for use in
	 * various admin pages.
	 *
	 * @param string $name - The html name of the select box
	 * @param string $selected - If a matching id is found in the list, that item
	 *                           is marked as selected.
	 * @return string The select list of pages
	 */
	function GetAdminPageDropdown($name,$selected)
	{
	  $opts = array();
	  $opts[ucfirst(lang('none'))] = '';

	  $depth = 0;
	  foreach( $this->menuItems as $sectionKey=>$menuItem )
		{
		  if( $menuItem['parent'] != -1 )
		{
		  continue;
		}
		  if( !$menuItem['show_in_menu'] || strlen($menuItem['url']) < 1 )
		{
		  continue;
		}
		  
		  $opts[$menuItem['title']] = $menuItem['url'];

		  if( is_array($menuItem['children']) && 
		  count($menuItem['children']) )
		{
		  foreach( $menuItem['children'] as $thisChild )
			{
			  if( $thisChild == 'home' || $thisChild == 'logout' ||
			  $thisChild == 'viewsite')
			{
			  continue;
			}

			  $menuChild = $this->menuItems[$thisChild];
			  if( !$menuChild['show_in_menu'] || strlen($menuChild['url']) < 1 )
			{
			  continue;
			}

			  $opts['&nbsp;&nbsp;'.$menuChild['title']] = cms_htmlentities($menuChild['url']);
			}
		}
		}

	  $output = '<select name="'.$name.'">';
	  foreach( $opts as $key => $value )
		{
		  if( $value == $selected )
		{
		  $output .= sprintf("<option selected=\"selected\" value=\"%s\">%s</option>",
					 $value,$key);
		}
		  else
		{
		  $output .= sprintf("<option value=\"%s\">%s</option>",
				 $value,$key);
		}
		}
	  $output .= '</select>';
	  return $output;
	}

	/**
	 * Crete Tabs and Form layout
	 *
	 * @param array $tabs - tab input data
	 * @return string The select list of pages
	 */
	function CreateTabs($tabs) {

		// get config from globals
		global $gCms;
		$config = $gCms->GetConfig();

		// set the output vards
		$tab_list = '';
		$tab_content = '';
		$tab_class = '';
		$tab_gallery = '';

		// check for class param
		if(isset($tabs['class'])) {
			$tab_class = $tabs['class'];
			unset($tabs['class']);
		}

		// go thorugh tags
		foreach ($tabs as $key => $item) {

			// set active if first
			$active = $key == 0 ? ' class="active"' : '';

			$tab_list .= get_html(array(
				'name'		=> $item['name'],
				'active'	=> $key == 0 ? ' class="active"' : '',
				'template'	=> $config['root_path'] . '/admin/templates/form-tabs-item.html'
			));

			// colect col output
			$cols = '';

			foreach ($item['content'] as $col => $col_content) {

				$tab_inputs = '';

				// check if col is array
				if(is_array($col_content)) {

					// setup inputs
					foreach ($col_content as $input) {

						if(is_array($input)) {
						
							// if json
							if($input['type'] == 'json') {

								// check title
								if(isset($input['title']) && $input['title']) {
									$tab_inputs .= '<div class="form__row">' . $input['title'] . '</div>';
								}

								// setup inputs
								foreach ($input['inputs'] as $json_input) {
									$tab_inputs .= $this->CreateTabsInput($json_input);
								}

							} else if($input['type'] == 'user_access_checkboxes') {

								// get group operations
								$groupops =& $gCms->GetGroupOperations();

								// get the groups
								$groups = $groupops->LoadGroups();

								// create user access checkbox for groups
								$rows = $this->UserPageAccessCheckboxes($groups, $input['values']);

								// get group operations
								$userops =& $gCms->GetUserOperations();

								// get the groups
								$users = $userops->LoadUsers();

								// create user access checkbox for users
								$rows .= $this->UserPageAccessCheckboxes($users, $input['values']);

								// table template
								$tab_inputs .= get_html(array(

									'title'		=> lang('template_users_label'),
									'info'		=> lang('template_users_info'),
									'uncheck'	=> lang('uncheck_all'),

									// input tag values
									'rows'		=> $rows,

									// template file
									'template'	=> $config['root_path'] . '/admin/templates/form-user-access.html'

								));

							} else {

								if($input['type'] == 'gallery') {

									$tab_gallery .= setup_dropzone(
										// the id of the gallery that is used in db
										$input["gallery_id"],
										// all the options from module settings
										$input["options"]
									);

								} else {

									// create the inputs
									$tab_inputs .= $this->CreateTabsInput($input);

								}

							}

						} else {

							// we have html
							$tab_inputs .= $input;
							
						}

					}

					$tab_inputs = '<div>' . $tab_inputs . '</div>';

				} else {

					// set the content
					$tab_inputs = '<div>' . $col_content . '</div>';

				}

				// add to the cols
				$cols .= $tab_inputs;

			} // end cols foreach

			$tab_content .= get_html(array(
				'inputs'	=> $cols,
				'active'	=> $active,
				'template'	=> $config['root_path'] . '/admin/templates/form-tabs-content.html'
			));

		}

		// our template
		$return = get_html(array(
			'tab_class'		=> $tab_class,
			'tab_list'		=> $tab_list,
			'tab_content'	=> $tab_content,
			'template'		=> $config['root_path'] . '/admin/templates/form-tabs.html'
		));

		// todo - wx - šitā nevar
		// temporary fix - need to change this
		if($tab_gallery != '') {
			$return = array($return, $tab_gallery);
		}

		// retrun html template with content
		return $return;

	}

	/**
	 * Crete Inputs/Selects/Textareas
	 *
	 * @param array $tabs - tab input data
	 * @return string The select list of pages
	 */
	function CreateTabsInput($input) {

		// get config from globals
		global $gCms;
		$config = $gCms->GetConfig();

		// setup css class
		$class = '';

		// check for extra classes to add
		// check for class param and check for errors
		if(isset($input['class'])) $class .= $input['class'];
		if(isset($errors[$input['name']])) $class .= '  color-error';

		// set the template
		$template = $input['type'];

		// for hidden inputs
		if($template == 'hidden') {
			return '<input type="hidden" name="' . $input['name'] . '"' .
				   ' value="' . ( isset($input['value']) ? $input['value'] : '' ) . '" />' . "\n";
		}

		// setup options for selects
		if($template == 'select'
		|| $template == 'select-social') {
			$options = '';
			foreach ($input['options'] as $option) {
				// check for selected
				$selected = '';
				if(isset($input['selected']) && $input['selected'] == $option["value"]) {
					$selected = ' selected="selected"';
				}
				// option 
				$options .= '<option value="'.$option["value"].'"'.$selected.'>'.$option["name"].'</option>';
			}
		}

		// date input
		if($template == 'date') {

			// check for value
			if(isset($input["value"])) {
				$date = new DateTime($input["value"]);
			} else {
				$date = new DateTime();
				$date->setTimezone(new DateTimeZone($config['timezone']));
			}

			// create month options
			$month_options = '';
			foreach (lang('months') as $month_number => $month_name) {
				
				// selected month
				$month_selected = '';
				if($month_number == $date->format('m')) $month_selected = ' selected';

				// options
				$month_options .= '<option value="'.$month_number.'"'.$month_selected.'>'.$month_name.'</option>';

			}

			$input['options'] = array(
				'd' => 31,
				'Y' => 1900,
				'H' => 23,
				'i' => 60,
				's' => 60
			);

			foreach ($input['options'] as $key => $value) {

				// where do we start and end
				$start = 0;
				$end = $value;

				// some update for year
				if($key == 'Y') {
					$start = $end = date('Y'); // use current year by default
					if(isset($input['year_start'])
					&& $input['year_start'] <= $end) {
						$start = $input['year_start'];
					}
					if(isset($input['year_end'])
					&& $input['year_end'] >= $end) {
						$end = $input['year_end'];
					}
				}

				// days can't be 0
				if($key == 'd') $start = 1;

				// var
				$current_options = '';

				// create the options
				for ($i = $start; $i <= $end; $i++) {

					// add 0 to the single numbers
					if($i < 10) $i = "0$i";

					// selected hour
					$i_selected = '';
					if($i == $date->format($key)) $i_selected = ' selected';

					// options
					$current_options .=
						'<option value="'.$i.'"'.$i_selected.'>'.$i.'</option>';
				
				}

				$input['options'][$key] = $current_options;

			}

			return get_html(array(

				// date
				'days_name'			=> $input['name'] . '_day',
				'days_options'		=> $input['options']['d'],
				'month_name'		=> $input['name'] . '_month',
				'month_options'		=> $month_options,
				'year_name'			=> $input['name'] . '_year',
				'year_options'		=> $input['options']['Y'],

				// time
				'time'				=> true,
				'hours_name'		=> $input['name'] . '_hour',
				'hours_options'		=> $input['options']['H'],
				'minutes_name'		=> $input['name'] . '_minute',
				'minutes_options'	=> $input['options']['i'],
				'seconds_name'		=> $input['name'] . '_second',
				'seconds_options'	=> $input['options']['s'],

				// comment and label if set
				'label'		=> isset($input['label']) ? $input['label'] : '',
				'comment'	=> isset($input['comment']) ? $input['comment'] : '',
				
				// extra classes
				'class'		=> $class,
				
				// template file
				'template'	=> VCMS_PATH . '/admin/templates/form-date.html'

			));

		}

		// update checkbox template if readonly set
		if($template == 'checkbox' && isset($input['readonly']) && $input['readonly']) {
			$template = $template . '-readonly';
		}

		// update checkbox template if readonly set
		if($template == 'input' && isset($input['readonly']) && $input['readonly']) {
			$template = 'readonly';
		}

		// update checkbox template if readonly set
		if($template == 'image' || $template == 'file' || $template == 'video') {

			if(isset($input['value'])) {

				$image_id = $input['value'];

				$db = $gCms->GetDb();
				$get_image_src_query = "SELECT src FROM ".cms_db_prefix()."images where id = ?";
				$image_src = $db->GetOne($get_image_src_query, array($image_id));

			}

			// set the path
			if(isset($image_src) && $image_src != '') {
				$file_path = APP_PATH . '/' . $config['uploads_dir'] . '/' . $input['folder'] . '/' . $image_src;
				$file_url = APP_URL . '/' . $config['uploads_dir'] . '/' . $input['folder'] . '/' . $image_src;
			}

			if($template == "file") {
				
				$file = '<span>' . ( isset($image_src) && $image_src != '' ? $image_src : '' ) . '</span>';

			} else if($template == "video") {
				
				$file = isset($image_src) && $image_src != "" ? '<video playsinline controls>
					<source src="' . $file_url . '" type="' . mime_content_type($file_path) . '" />
				</video>' : '';

			} else {

				$file = isset($image_src) && $image_src != ""
				? '<img src="' . $file_url . '" />'
				: '';

			}
			
			// get the input template
			return get_html(array(

				'input'		=> '<input type="hidden"
								id="' . ( isset($input['id']) ? $input['id'] : "input-" . $input['name'] ) . '"
								value="' . ( isset($input['value']) ? $input['value'] : '' ) . '"
								name="'. $input['name'] . '" />',

				// input tag values
				'image'		=> $file,

				// comment and label if set
				'label'		=> isset($input['label']) ? $input['label'] : '',
				'comment'	=> isset($input['comment']) ? $input['comment'] : '',
				'folder'	=> isset($input['folder']) ? $input['folder'] : '',

				// button
				'button_select' => isset($input['button_select']) ? $input['button_select'] : 'Select',
				'button_remove' => isset($input['button_remove']) ? $input['button_remove'] : 'Remove',

				// extra classes
				'class'		=> $class,

				// template file
				'template'	=> $config['root_path'] . '/admin/templates/form-file.html'

			)); 

		}

		// update checkbox template if readonly set
		if($template == 'google'
		|| $template == 'facebook') {

			// setup data-params=""
			$data_params = '';
			foreach ($input['data'] as $data_param => $data_value) {
				$data_params .= ' data-' . $data_param . '="' . $data_value . '"';
			}

			// return
			$html_template = array(

				// data params
				'data'		=> $data_params,

				// image for facebook preview
				'image'		=> isset($input['image']) ? $input['image'] : false,

				// template file
				'template'	=> $config['root_path'] . '/admin/templates/form-meta-'.$template.'.html'

			);

			// set some extra params for image
			if(isset($input['image']) && $input['image']) {

				// defaults
				$html_template['image_value'] = '';
				$html_template['image_src'] = '';
				$html_template['image_remove'] = '';
				
				// check value
				if(isset($input['image']['value'])) {

					$image_id = $input['image']['value'];

					$db = $gCms->GetDb();
					$get_image_src_query = "SELECT src FROM ".cms_db_prefix()."images where id = ?";
					$image_src = $db->GetOne($get_image_src_query, array($image_id));

					$image_src = isset($image_src) && $image_src != ""
					? '<img src="' . APP_URL . '/' . $config['uploads_dir'] . '/' . $input['image']['folder'] . '/' . $image_src . '" />'
					: '';
					
					$html_template['image_value'] = $input['image']['value'];
					$html_template['image_src'] = $image_src;
					$html_template['image_remove'] = $input['image']['folder'];

				}

				// add paramas
				$html_template['image_name'] = $input['image']['name'];
				$html_template['image_label'] = $input['image']['label'];
				$html_template['image_folder'] = $input['image']['folder'];

				// button text for remove button 
				$html_template['image_remove'] = "Remove";
				if(isset($input['data']['default-image'])) {
					$html_template['image_remove'] = "Use default image";
				}
				
			}

			// retunr template
			return get_html($html_template);
			
		}

		// update checkbox template if readonly set
		if($template == 'gallery') {
			error_log("gallery"); return '';
		}

		// escape input value
		if(isset($input['value'])) {
			$input_value = htmlspecialchars($input['value'], ENT_COMPAT,'UTF-8', false);
		}

		// set template
		if(isset($input['template'])) {
			$input_value = $input['value'];
			$template = $input['template'];
		} else {
			$template = $config['root_path'] . '/admin/templates/form-' . $template . '.html';
		}

		// get the input template
		return get_html(array(

			// input tag values
			'id'		=> isset($input['id']) ? $input['id'] : "input-" . $input['name'],
			'name'		=> $input['name'],
			'value'		=> isset($input_value) ? $input_value : '',
			'text'		=> isset($input['text']) ? $input['text'] : '',

			// comment and label if set
			'label'		=> isset($input['label']) ? $input['label'] : '',
			'comment'	=> isset($input['comment']) ? $input['comment'] : '',

			// url for alias and similar inputs
			'url'		=> isset($input['url']) ? $input['url'] : '',

			// add * to label if field is required
			'required'	=> isset($input['validate']['not_empty']) && $input['validate']['not_empty'],

			// height is only present for textareas
			'height'	=> isset($input['height'])
						 ? ' style="height: ' . $input['height'] . 'px;"' : '',
			'data-height' => isset($input['height'])
						 ? ' data-height="' . $input['height'] . '"' : '',
			'data-url'	=> APP_URL,

			// checked is only present for checkboxes
			'checked'	=> isset($input['checked']) && $input['checked'] === true ? ' checked="checked"' : '',

			// extra classes
			'class'		=> $class,

			// js actions
			'onclick'	=> isset($input['onclick']) ? $input['onclick'] : '',
			'onchange'	=> isset($input['onchange']) ? $input['onchange'] : '',

			// checked is only present for checkboxes
			'data'		=> isset($input['data']) ? $input['data'] : '',

			// autocomplete
			'autocomplete' => isset($input['autocomplete'])
						   ? ' autocomplete="' . $input['autocomplete'] . '"' : '',

			// options only for select
			'options'	=> isset($options) ? $options : '',

			// template file
			'template'	=> $template

		));

	}

	/**
	 * Create access checkboxes for users
	 * - edit, set active and delete functionality for pages
	 * - used in add/edit for templates and pages
	 *
	 * @return template with checkboxes for the <form>
	 */
	function UserPageAccessCheckboxes($items, $values) {

		// cms
		global $gCms;

		// config
		$config = $gCms->GetConfig();

		// is this a group?
		if(get_class($items[0]) == 'Group') {
			$items_class = 'group';
		}

		// is this a user?
		if(get_class($items[0]) == 'User') {
			$items_class = 'user';
		}

		// check for values
		if(isset($values->$items_class)) {
			$input_values = $values->$items_class;
		}
			
		// header
		$rows = get_html(array(

			// input tag values
			'title'		=> lang($items_class),

			'edit'		=> lang('edit'),
			'active'	=> lang('active'),
			'delete'	=> lang('delete'),

			'class'		=> '  color-bg  text-strong',

			// template file
			'template'	=> $config['root_path'] . '/admin/templates/form-user-access-row.html'

		));

		foreach ($items as $item) {

			// if users and user id = 1
			// that is the main boss main super user
			// he always has access to everything
			if($items_class == 'user' && $item->id == 1) {
				continue;
			}

			$cells = array();

			foreach (array('edit', 'active', 'delete') as $type) {

				// unchecked by default
				$checked = '';

				// check if value set
				if(isset($input_values->$type)
				&& is_array($input_values->$type)
				&& in_array($item->id, $input_values->$type)) {
					$checked = ' checked="checked"';
				}

				$cells[$type] = get_html(array(

					// input tag values
					'id'		=> 'input-' . $type . '-' . $items_class . '-' . $item->id,
					'name'		=> 'user_access[' . $items_class . '][' . $type . '][]',
					'value'		=> $item->id,

					// checked
					'checked'	=> $checked,

					// comment and label if set
					'label'		=> lang($type),
					'comment'	=>  '',

					// extra classes
					'class'		=> '',

					// template file
					'template'	=> $config['root_path'] . '/admin/templates/form-checkbox.html'

				));

			}
			
			// table template
			$rows .= get_html(array(

				// input tag values
				'title'		=> $item->Name() . ( $item->id == 1 ? '*' : '' ),

				'edit'		=> $cells['edit'],
				'active'	=> $cells['active'],
				'delete'	=> $cells['delete'],

				'class'		=> '',

				// template file
				'template'	=> $config['root_path'] . '/admin/templates/form-user-access-row.html'

			));

		}

		// send the template
		return $rows;

	}

	/**
	 * Create access checkboxes for users
	 * @param string $button name of the button
	 * @param object $node current object we working on
	 *
	 * @return html string
	 */
	function getIconButton($button, $node) {

		// cms
		global $gCms;

		// config
		$config = $gCms->GetConfig();

		// set the action link
		$action_link = $node->url . '?action=' . $button . '&id=' . $node->id;

		// check
		switch ($button) {

			// ------------------------------------
			// -- active
			// ------------------------------------

			case 'edit':
			case 'edit_link':

				// edit link
				$edit_link = str_replace('.php', '_add_edit.php', $node->url) . '?id=' . $node->id;

				// if only link break everything and return a tag
				if($button == 'edit_link') {
					if($node->CanEdit()) {
						return '<a class="list__item--link  color-one-text" href="' . $edit_link . '">' . $node->Name() . '</a>';
					} else {
						return $node->Name();
					}
				}

				// default
				$template = '';
				
				// check if we can edit
				if(!$node->CanEdit()) {
					$template = '-off';
				}

				// create the icon
				$result = get_html(array(
					'link'		=> $edit_link,
					'template'	=> $config['root_path'] . '/admin/templates/action-edit' . $template . '.html'
				));

				// end
				break;

			// ------------------------------------
			// -- default
			// ------------------------------------

			case 'default':

				// default
				$template = 'off';
				
				// is this default content?
				if($node->DefaultContent()) {
					$template = 'static';
				}

				// create the icon
				$result = get_html(array(
					'link'		=> $action_link,
					'onclick'	=> ' onclick="doAction.call(this); return false;"',
					'template'	=> $config['root_path'] . '/admin/templates/action-active-' . $template . '.html'
				));

				// end
				break;

			// ------------------------------------
			// -- active
			// ------------------------------------

			case 'active':

				// default
				$template = 'static';
				
				// check can we change active
				// and then check current status
				if($node->CanSetActive()) {
					if($node->active == 1) {
						$template = 'on';
					} else {
						$template = 'off';
					}
				}

				// create the icon
				$result = get_html(array(
					'link'		=> $action_link,
					'onclick'	=> ' onclick="doAction.call(this); return false;"',
					'template'	=> $config['root_path'] . '/admin/templates/action-active-' . $template . '.html'
				));

				// end
				break;

			// ------------------------------------
			// -- delete
			// ------------------------------------

			case 'delete':

				// default
				$template = '';

				// check delete
				if($node->CanDelete() === true) {
					// we can delete
					$template = '';
				} else if($node->CanDelete() === false) {
					// we can't delete
					$template = '-off';
				} else {
					// we can't delete and function returns a message in response
					// so we stop everything and return the message
					return get_html(array(
						'text'		=> $node->CanDelete(),
						'template'	=> VCMS_PATH . '/admin/templates/action-error-message.html'
					));
				}

				// get the delete button
				$result = get_html(array(
					'link'		=> $action_link,
					'confirm'	=> lang('deleteconfirm', $node->Name()),
					'template'	=> $config['root_path'] . '/admin/templates/action-delete' . $template . '.html'
				));

				// end
				break;

			// ------------------------------------
			// -- default
			// ------------------------------------
			
			default:
				// empty string
				$result = '';
				break;

		}

		// return
		return $result;

	}

	/**
	 * Create access checkboxes for users
	 * @param array $data - data to use
	 * 
	 * @return html string
	 */
	function CreateTable($data) {

		$items = $data['items'];
		$cells = $data['cells'];
		$buttons = $data['buttons'];
		$buttons_top = isset($data['buttons_top']) ? $data['buttons_top'] : false;
		$buttons_bottom = isset($data['buttons_bottom']) ? $data['buttons_bottom'] : false;
		$message = $data['message'];
		$table_class = isset($data['class']) ? $data['class'] : '';

		$show_parent = false;
		if(isset($data['show_parent']) && $data['show_parent'] == true) {
			$show_parent = true;
		}

		// setup header row
		$item_table_header = '';
		foreach ($cells as $cell) {
			$item_table_header .= get_html(array(
				'class'		=> $cell['class'],
				'content'	=> $cell['label'],
				'template'	=> VCMS_PATH . '/admin/templates/table-cell.html'
			));
		}

		// setup item list
		$item_table_list = '';

		// set current parent
		$current_parent = 0;

		// check item count
		if(count($items) > 0) {

			// go through items
			foreach ($items as $item) {

				// show parent name before rows
				// only works if sorted by parents
				if($show_parent && isset($item->parent)) {

					// check parents
					if($current_parent != $item->parent) {

						// update parent
						$current_parent = $item->parent;

						// get the row template
						$item_table_list .= get_html(array(
							'id'		=> '',
							'class'		=> '  list__item--off',
							'cells'		=> get_html(array(
								'class'		=> '  list__cell--first  list__cell--no-indicator  text-strong',
								'action'	=> '',
								'content'	=> $item->parent_name,
								'template'	=> VCMS_PATH . '/admin/templates/table-cell.html'
							)),
							'template'	=> VCMS_PATH . '/admin/templates/table-row.html'
						));
						
					}

				}

				// get the object name
				$item_class = strtolower(get_class($item));
				if(isset($item->__what)) $item_class = $item->__what;

				// create cells
				$item_cells = '';
				foreach ($cells as $cell) {

					// check if we show content for this cell
					$show_cell_content = true;
					if(isset($cell['show'])) {
						$show_cell_content = $cell['show'];
						$show_cell_content = $show_cell_content($item);
					}

					$cell_key = $cell['key'];
					$item_cells .= get_html(array(
						'class'		=> $cell['class'],
						'action'	=> $cell['action'],
						'content'	=> $show_cell_content ? $item->$cell_key : '',
						'template'	=> VCMS_PATH . '/admin/templates/table-cell.html'
					));
				}

				// get the row template
				$item_table_list .= get_html(array(
					'id'		=> $item_class . '-' . $item->id,
					'class'		=> !$item->active ? '  list__item--off' : '',
					'cells'		=> $item_cells,
					'template'	=> VCMS_PATH . '/admin/templates/table-row.html'
				));

			}

		}

		// get the item list template
		return get_html(array(
			'message'	=> isset($message) ? $message : '',
			'header' 	=> $item_table_header,
			'content'	=> $item_table_list,
			'class'		=> $table_class,

			// buttons
			'buttons'		 => $buttons,
			'buttons_top'	 => $buttons_top,
			'buttons_bottom' => $buttons_bottom,

			//template
			'template'	=> VCMS_PATH . '/admin/templates/table.html'
		));

	}

	/**
	 * Setup module groups
	 * @param object $module - module item in the menu
	 * @return string
	 */
	function setupModuleGroups($module) {
		$modules = array(
			'socialnetworks' => 'right'
		);
		return isset($modules[$module]) ? $modules[$module] : 'left';
	}

}

# vim:ts=4 sw=4 noet
?>
