<?php

/**
 * Page class
 *
 * @author vs2rs
 */
class Page {

	// public function __construct($variables) {

	//     foreach($variables as $key => $value) {
	//         $this->$key = stripslashes($value);
	//     }

	// }

	// public function MenuText() {
	//     return $this->menu_text;
	// }

	public $id;

	public function __construct() {
		$this->id = false;
	}

	public function load() {

		// load our item
		if($item = PageOperations::load_by_id($this->id)) {
			
			foreach($item as $key => $value) {
				$this->$key = stripslashes($value);
			}

			// all is good
			return true;

		}
		
		// set the error
		$this->error = 'item_not_loaded';

		// return false
		return false;

	}

	public function access_edit() {
		return check_permission(get_userid(), 'pages_edit');
	}

	public function access_add() {
		return check_permission(get_userid(), 'pages_add');
	}

	public function access_delete() {
		return check_permission(get_userid(), 'pages_delete');
	}

}





// -- class.page.inc.php