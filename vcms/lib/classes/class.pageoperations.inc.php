<?php

/**
 * Include page class
 */
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'class.page.inc.php');

/**
 * Page operation class
 *
 * @author vs2rs
 */
class vcmsOperations {

	public static function load_object($operations, $db_query, $object_id) {

		// we are editng
		if($object_id && $operations::access_edit()) {

			// get db
			$db = Database::getInstance();

			// if query is array we need to load extra props
			if(is_array($db_query)) {

				// load the main content
				if($result = $db->get_row($db_query[0])) {

					// load the props
					if($result_props = $db->get_rows($db_query[1])) {
						foreach($result_props as $prop) {
							$prop_name = $prop->name;
							$result->$prop_name = $prop->value;
						}
					}

					// return results
					return $result;

				}

			} else {

				// check results and return if success
				if($result = $db->get_row($db_query)) {
					return $result;
				}

			}

		} else {

			// no object no access to edit check for add
			return $operations::access_add();

		}

		// no object no access to add
		return false;

	}

}

class PageOperations {

	// our object name
	private static $table_name = 'content';

	// load our page
	public static function load_by_id($page_id) {

		// get the table name
		$table_name = self::$table_name;

		// query
		$db_query = array(
			"SELECT * FROM {{prefix}}$table_name WHERE content_id = $page_id",
			"SELECT prop_name as name, content as value FROM {{prefix}}content_props WHERE content_id = $page_id"	
		);

		// sen class name query and id
		return vcmsOperations::load_object(
			static::class, $db_query, $page_id
		);

	}

	public static function access_edit() {
		return check_permission(get_userid(), 'pages_edit');
	}

	public static function access_add() {
		return self::access_edit() && check_permission(get_userid(), 'pages_add');
	}

}





// -- class.pageoperations.inc.php