<?php // -*- mode:php; tab-width:4; indent-tabs-mode:t; c-basic-offset:4; -*-
#CMS - CMS Made Simple
#(c)2004-2010 by Ted Kulp (ted@cmsmadesimple.org)
#This project's homepage is: http://cmsmadesimple.org
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#$Id: class.group.inc.php 6443 2010-07-02 04:49:33Z sjg $

/**
 * @package CMS 
 */

/**
 * Generic group class. This can be used for any logged in group or group related function.
 *
 * @since 0.9
 * @package CMS
 * @version $Revision$
 * @license GPL
 */
class Group
{
	// main
	public $id;
	public $name;
	public $active;
	public $default;

	// extras
	public $toggle_active;
	public $delete;
	public $edit;
	public $edit_link;
	public $permissions;

	/**
	 * URL of the admin file that has the actions
	 */
	public $url = 'users_groups.php';

	/**
	 * Constructor
	 */
	function __construct()
	{
		$this->SetInitialValues();
	}

	// wx: keep backwards compatibility
	function Group()
	{
		self::__construct();
	}

	/**
	 * Sets up some default values
	 *
	 * @access private
	 * @return void
	 */
	function SetInitialValues()
	{
		$this->id = -1;
		$this->name = '';
		$this->active = false;
		$this->default = false;
		$this->toggle_active = '';
		$this->delete = '';
		$this->edit = '';
		$this->edit_link = '';
		$this->permissions = '';
	}

	/**
	 * Get the current group name
	 *
	 * @return string
	 */
	function Name()
	{
		return $this->name;
	}

	/**
	 * Check if default content
	 *
	 * @return bool
	 */
	function DefaultContent()
	{
		return $this->default;
	}

	/**
	 * Persists the group to the database.
	 *
	 * @return boolean true if the save was successful, false if not.
	 */
	function Save()
	{
		// default error message
		$result = lang('group_save_error');
		
		global $gCms;
		$groupops =& $gCms->GetGroupOperations();
		
		if ($this->id > -1)
		{
			$update_group = $groupops->UpdateGroup($this);

			// check if object returned or an error message
			// if object - then user update was success
			// if not an object then thats an error message
			if(is_object($update_group)) {
				$result = true;
			} else {
				$result = $update_group;
			}
		}
		else
		{
			$insert_group = $groupops->InsertGroup($this);

			// check if object returned or an error message
			// if object - then user update was success
			// if not an object then thats an error message
			if(is_object($insert_group)) {
				$this->id = $insert_group->id;
				$result = true;
			} else {
				$result = $insert_group;
			}

		}

		return $result;
	}

	/**
	 * Deletes the group from the database
	 *
	 * @return boolean True if the delete was successful, false if not.
	 */
	function Delete()
	{
		$result = false;

		if ($this->id > -1)
		{
			global $gCms;
			$groupops =& $gCms->GetGroupOperations();
			$result = $groupops->DeleteGroupByID($this->id);
			if ($result)
			{
				$this->SetInitialValues();
			}
		}

		return $result;
	}

	/**
	 * Check if group can be deleted
	 * @return true or false
	 */
	function CanDelete() {

		// validate id
		if ($this->id > -1) {

			// group operations
			global $gCms;
			$groupops = $gCms->GetGroupOperations();

			// check delete
			return $groupops->IsGroupRemovable($this->id);

		}

		// default
		return false;

	}

	/**
	 * Check if toggle active possible
	 * @return true or false
	 */
	function CanSetActive() {

		// validate id
		if ($this->id > -1) {

			// group operations
			global $gCms;
			$groupops = $gCms->GetGroupOperations();

			// check delete
			return $groupops->IsToggleActivePossible($this->id);

		}

		// default
		return false;

	}

	/**
	 * Check if edit possible
	 * @return true or false
	 */
	function CanEdit() {

		// validate id
		if ($this->id > -1) {

			// group operations
			global $gCms;
			$groupops = $gCms->GetGroupOperations();

			// check delete
			return $groupops->IsEditPossible($this->id);

		}

		// default
		return false;

	}

}

# vim:ts=4 sw=4 noet
?>