<?php // -*- mode:php; tab-width:4; indent-tabs-mode:t; c-basic-offset:4; -*-
#CMS - CMS Made Simple
#(c)2004-2010 by Ted Kulp (ted@cmsmadesimple.org)
#This project's homepage is: http://cmsmadesimple.org
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#$Id$

/**
 * @package CMS 
 */

/**
 * @ignore
 */
define( "MODULE_DTD_VERSION", "1.3" );

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'class.module.inc.php');

/**
 * "Static" module functions for internal use and module development.  CMSModule
 * extends this so that it has internal access to the functions.
 *
 * @since		0.9
 * @package		CMS
 */
class ModuleOperations
{
  /**
   * A member to hold an error string
   */
  var $error;

  /**
   * A member to hold the id of the active tab
   */
  var $mActiveTab = '';

  var $xml_exclude_files = array('^\.svn' , '^CVS$' , '^\#.*\#$' , '~$', '\.bak$' );
  var $xmldtd = '
<!DOCTYPE module [
  <!ELEMENT module (dtdversion,name,version,description*,help*,about*,requires*,file+)>
  <!ELEMENT dtdversion (#PCDATA)>
  <!ELEMENT name (#PCDATA)>
  <!ELEMENT version (#PCDATA)>
  <!ELEMENT mincmsversion (#PCDATA)>
  <!ELEMENT description (#PCDATA)>
  <!ELEMENT help (#PCDATA)>
  <!ELEMENT about (#PCDATA)>
  <!ELEMENT requires (requiredname,requiredversion)>
  <!ELEMENT requiredname (#PCDATA)>
  <!ELEMENT requiredversion (#PCDATA)>
  <!ELEMENT file (filename,isdir,data)>
  <!ELEMENT filename (#PCDATA)>
  <!ELEMENT isdir (#PCDATA)>
  <!ELEMENT data (#PCDATA)>
]>';

  /**
   * ------------------------------------------------------------------
   * Error Functions
   * ------------------------------------------------------------------
   */

  /**
   * Set an error condition
   *
   * @param string $str The string to set for the error
   * @return void
   */
  function SetError($str = '')
  {
	global $gCms;
	$gCms->variables['error'] = $str;
  }

  /**
   * Return the last error
   *
   * @return string The last error, if any
   */
  function GetLastError()
  {
	global $gCms;
	if( isset( $gCms->variables['error'] ) )
	  return $gCms->variables['error'];
	return "";
  }


  /**
   * Install a module into the database
   *
   * @param string $module The name of the module to install
   * @param boolean $loadifnecessary If true, loads the module before trying to install it
   * @return array Returns a tuple of whether the install was successful and a message if applicable
   */
  function InstallModule($module, $loadifnecessary = false)
  {
    global $gCms;
    if( !isset( $gCms->modules[$module] ) )
      {
		  if( $loadifnecessary == false )
			  {
				  return array(false,lang('errormodulenotloaded'));
			  }
		  else
			  {
				  if( !$this->LoadNewModule( $module ) )
					  {
						  return array(false,lang('errormodulewontload'));
					  }
			  }
      }
 
    $db =& $gCms->GetDb();
    if (isset($gCms->modules[$module]))
      {
	$modinstance =& $gCms->modules[$module]['object'];
	$result = $modinstance->Install();
	
        #now insert a record
	if (!isset($result) || $result === FALSE)
	  {
	    $query = "INSERT INTO ".cms_db_prefix()."modules (module_name, version, status, admin_only, active) VALUES (?,?,'installed',?,?)";
	    $db->Execute($query, array($module,$modinstance->GetVersion(),($modinstance->IsAdminOnly()==true?1:0),1));
	    
            #and insert any dependancies
	    if (count($modinstance->GetDependencies()) > 0) #Check for any deps
	      {
                #Now check to see if we can satisfy any deps
		foreach ($modinstance->GetDependencies() as $onedepkey=>$onedepvalue)
		  {
		    $time = $db->DBTimeStamp(time());
		    $query = "INSERT INTO ".cms_db_prefix()."module_deps (parent_module, child_module, minimum_version, create_date, modified_date) VALUES (?,?,?,".$time.",".$time.")";
		    $db->Execute($query, array($onedepkey, $module, $onedepvalue));
		  }
	      }
	    
	    // and we're done
	    return array(true);
	  }
	else
	  {
	    if( trim($result) == "" )
	      {
		$result = lang('errorinstallfailed');
	      }
	    return array(false,$result);
	  }
      }    
    else
      {
	return array(false,lang('errormodulenotfound'));
      }
  }


  /**
   * Load a single module from the filesystem
   *
   * @param string $modulename The name of the module to load
   * @return boolean Whether or not the module load was successful
   */
  private function LoadNewModule( $modulename )
  {

    global $gCms;
    $db =& $gCms->GetDb();
    $cmsmodules = &$gCms->modules;

    // vcms / wx ==> check where the module is located
    if(@is_file($gCms->config['modules_system'] . "/$modulename/$modulename.module.php")) {
    	// system module
    	$dir = $gCms->config['modules_system'];
    } else if(@is_file($gCms->config['modules_project'] . "/$modulename/$modulename.module.php")) {
    	// project specific module
    	$dir = $gCms->config['modules_project'];
    } else {
    	// module not found
    	unset($cmsmodules[$modulename]);
    	return false;
    }

	// question: is this a potential XSS vulnerability
	include("$dir/$modulename/$modulename.module.php");
	if( !class_exists( $modulename ) ) {
		return false;
	}

	$newmodule = new $modulename;
	$name = $newmodule->GetName();
	$cmsmodules[$name]['object'] =& $newmodule;
	$cmsmodules[$name]['installed'] = false;
	$cmsmodules[$name]['active'] = false;

	return true;
  
  }

  /**
   * Upgrade a module
   *
   * @param string $module The name of the module to upgrade
   * @param string $oldversion The version number of the existing module
   * @param string $newversion The version number of the new module
   * @return boolean Whether or not the upgrade was successful
   */
  function UpgradeModule( $module, $oldversion, $newversion )
  {
    global $gCms;
    $db =& $gCms->GetDb();

    if (!isset($gCms->modules[$module]))
      {
	return false;
      }

    $modinstance = $gCms->modules[$module]['object'];
    $result = $modinstance->Upgrade($oldversion, $newversion);
    if( $result === FALSE )
      {
	return false;
      }

    $query = "UPDATE ".cms_db_prefix()."modules SET version = ?, admin_only = ? WHERE module_name = ?";
    $db->Execute($query,array($newversion,($modinstance->IsAdminOnly()==true?1:0),$module));

    return true;
  }

  /**
   * Uninstall a module
   *
   * @param string $module The name of the module to upgrade
   * @return boolean Whether or not the upgrade was successful
   */
  function UninstallModule( $module)
  {
    global $gCms;
    $db =& $gCms->GetDb();

    if (!isset($gCms->modules[$module]))
      {
		return false;
      }

    $modinstance = $gCms->modules[$module]['object'];
    $cleanup = $modinstance->AllowUninstallCleanup();
	$result = $modinstance->Uninstall();
	// clean up
	if (!isset($result) || $result === FALSE)
	{
		// now delete the record
		$query = "DELETE FROM ".cms_db_prefix()."modules WHERE module_name = ?";
		$db->Execute($query, array($module));

		// delete any dependencies
		$query = "DELETE FROM ".cms_db_prefix()."module_deps WHERE child_module = ?";
		$db->Execute($query, array($module));
		
		// clean up, if permitted
		if ($cleanup)
			{
			$db->Execute('DELETE FROM '.cms_db_prefix().
				'module_templates where module_name=?',array($module));
			$db->Execute('DELETE FROM '.cms_db_prefix().
				"siteprefs where sitepref_name like '".
					str_replace("'",'',$db->qstr($module)).
				"_mapi_pref%'");
			}
	}
	else
	{
		$this->setError($result);
		return false;
	}
    return true;
  }


  /**
   * Returns a hash of all loaded modules.  This will include all
   * modules loaded by LoadModules, which could either be all or them,
   * or just ones that are active and installed.
   *
   * @return array The hash of all loaded modules
   */
  function & GetAllModules()
    {
      global $gCms;
      $cmsmodules = &$gCms->modules;
      return $cmsmodules;
    }


  /**
   * Returns an array of modules that have a certain capabilies
   * 
   * @param string $capability The capability name
   * @param mixed $args Capability arguments
   * @return array List of all the module with that capability
   */
  public static function get_modules_with_capability($capability, $args= '')
  {
    global $gCms;

    $output = array();
    foreach($gCms->modules as $key => $data)
      {
	if( isset($data['installed']) &&
	    isset($data['object']) && 
	    is_object($data['object']) )
	  {
	    $obj =& $data['object'];
	    if( $obj->HasCapability($capability,$args) )
	      {
		$output[] = $obj;
	      }
	  }
      }

    if( !count($output) ) return FALSE;
    return $output;
  }
}

# vim:ts=4 sw=4 noet
?>
