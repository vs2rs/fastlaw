<?php
# CMS - CMS Made Simple
# (c)2004-6 by Ted Kulp (ted@cmsmadesimple.org)
# This project's homepage is: http://cmsmadesimple.org
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# BUT withOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
#
#$Id: class.module.inc.php 6868 2011-01-22 23:20:21Z sjg $

/**
 * @package             CMS
 */

/**
 * Base module class.
 *
 * All modules should inherit and extend this class with their functionality.
 *
 * @since		0.9
 * @version             1.8
 * @package		CMS
 */
class CMSModule
{
	/**
	 * ------------------------------------------------------------------
	 * Initialization Functions and parameters
	 * ------------------------------------------------------------------
	 */

        /**
	 * Reference to the global CMS Object
	 * use: global $gCms;
	 *
	 * @deprecated
	 */
	var $cms;

	/**
	 * Reference to the config array in the global CMS Object
	 * use: global $gCms; $config = $gCms->GetConfig();
         *
	 * @deprecated
	 */
	var $config;

	/**
	 * A variable that indicates the current desired language
	 * this is effected by the (optional) lang parameter on module action
	 * calls. 
	 *
	 * @access private
	 */
	var $curlang;

	/**
	 * An array of loaded lang strings
	 *
	 * @access private
	 * @ignore
	 */
	var $langhash;

	/**
	 * A hash of the parameters passed in to the module action
	 *
	 * @access private
	 * @ignore
	 */
	var $params;

	/**
	 * @access private
	 * @ignore
	 */
	var $wysiwygactive;

	/**
	 * @access private
	 * @ignore
	 */
	var $syntaxactive;

	/**
	 * @access private
	 * @ignore
	 */
	var $error;

	/**
	 * @access private
	 * @ignore
	 */
	var $modinstall;

	/**
	 * @access private
	 * @ignore
	 */
	var $modtemplates;

	/**
	 * @access private
	 * @ignore
	 */
	var $modlang;

	/**
	 * @access private
	 * @ignore
	 */
	var $modform;

	/**
	 * @access private
	 * @ignore
	 */
	var $modredirect;

	/**
	 * @access private
	 * @ignore
	 */
	var $modmisc;

	/**
	 * @access private
	 * @ignore
	 */
	var $param_map;

	/**
	 * @access private
	 * @ignore
	 */
	var $restrict_unknown_params;

	/**
	 * Constructor
	 *
	 */
	function __construct()
	{
		global $gCms;
		
		$this->cms =& $gCms;
		$this->config = $gCms->GetConfig(); // why?

		global $CMS_STYLESHEET;
		global $CMS_ADMIN_PAGE;
		global $CMS_MODULE_PAGE;
		global $CMS_INSTALL_PAGE;
		$this->curlang = cms_current_language(); // current language for this request.
		$this->langhash = array();
		$this->params = array();
		$this->param_map = array();
		$this->restrict_unknown_params = false;
		$this->wysiwygactive = false;
		$this->error = '';
		
		$this->params[] = array(
					'name' => 'lang',
					'default' => 'en_US',
					'optional' => true);

		if( !isset($CMS_ADMIN_PAGE) && !isset($CMS_STYLESHEET) && !isset($CMS_INSTALL_PAGE))
		  {
		    $this->SetParameterType('assign',CLEAN_STRING);
		    $this->SetParameterType('module',CLEAN_STRING);
		    $this->SetParameterType('lang',CLEAN_STRING);
		    $this->SetParameterType('returnid',CLEAN_INT);
		    $this->SetParameterType('action',CLEAN_STRING);
		    $this->SetParameterType('showtemplate',CLEAN_STRING);
		    $this->SetParameterType('inline',CLEAN_INT);
		    $this->SetParameters();
		  }
		else
		  {
		    $this->params[0]['help'] = lang('langparam');
		  }
		
		$this->modinstall = false;
		$this->modtemplates = false;
		$this->modlang = false;
		$this->modform = false;
		$this->modredirect = false;
		$this->modmisc = false;
	}

	// wx: keep backwards compatibility
	function CMSModule()
	{
		self::__construct();
	}
	
	/**
	 * Private
	 *
	 * @ignore
	 */
	function LoadTemplateMethods()
	{
		if (!$this->modtemplates)
		{
			require_once(cms_join_path(dirname(__FILE__), 'module_support', 'modtemplates.inc.php'));
			$this->modtemplates = true;
		}
	}

	
	/**
	 * Private
	 *
	 * @ignore
	 */
	function LoadLangMethods()
	{
		if (!$this->modlang)
		{
			require_once(cms_join_path(dirname(__FILE__), 'module_support', 'modlang.inc.php'));
			$this->modlang = true;
		}
	}
	
	/**
	 * Private
	 *
	 * @ignore
	 */
	function LoadFormMethods()
	{
		if (!$this->modform)
		{
			require_once(cms_join_path(dirname(__FILE__), 'module_support', 'modform.inc.php'));
			$this->modform = true;
		}
	}

	/**
	 * ------------------------------------------------------------------
	 * Basic Functions.	 Name and Version MUST be overridden.
	 * ------------------------------------------------------------------
	 */

	/**
	 * Returns a sufficient about page for a module
	 *
	 * @abstract
	 * @return string The about page HTML text.
	 */
	function GetAbout()
	{
		return '';
	}

	/**
	 * Returns a sufficient help page for a module
	 * this function should not be overridden
	 *
	 * @return string The help page HTML text.
	 * @final
	 */
	function GetHelpPage()
	{
		return '';
	}

	/**
	 * Returns the name of the module
	 *
	 * @abstract
	 * @return string The name of the module.
	 */
	function GetName()
	{
		return 'unset';
	}

	/**
	 * Returns the full path to the module directory.
	 *
	 * @final
	 * @return string The full path to the module directory.
	 */
	function GetModulePath()
	{
		if (is_subclass_of($this, 'CMSModule'))
		{
			return cms_join_path($this->config['modules_system'], $this->GetName());
		}
		else
		{
			return dirname(__FILE__);
		}
	}

	/**
	 * Returns the URL path to the module directory.
	 *
	 * @final
	 * @param boolean Optional generate an URL using HTTPS path
	 * @return string The full path to the module directory.
	 */
	function GetModuleURLPath($use_ssl=false)
	{
		return false;
	}


	/**
	 * Returns a translatable name of the module.  For modulues who's names can
	 * probably be translated into another language (like News)
	 *
	 * @abstract
	 * @return string
	 */
	function GetFriendlyName()
	{
		return $this->GetName();
	}

	/**
	 * Returns the version of the module
	 *
	 * @abstract
	 * @return string
	 */
	function GetVersion()
	{
		return '0.0.0.1';
	}

	/**
	 * Returns the minimum version necessary to run this version of the module.
	 * 
	 * @abstract
	 * @return string
	 */
	function MinimumCMSVersion()
	{
		return CMS_VERSION;
	}

	/**
	 * Returns the maximum version necessary to run this version of the module.
	 *
	 * @abstract
	 * @deprecated
	 * @return string
	 */
	function MaximumCMSVersion()
	{
		return CMS_VERSION;
	}

	/**
	 * Returns the help for the module
	 *
	 * @param string Optional language that the admin is using.	 If that language
	 * is not defined, use en_US.
	 *
	 * @abstract
	 * @return string Help HTML Text.
	 */
	function GetHelp()
	{
		return '';
	}

	/**
	 * Returns XHTML that needs to go between the <head> tags when this module is called from an admin side action.
	 *
	 * This method is called by the admin theme when executing an action for a specific module.
	 *
	 * @return string XHTML text
	 */
	function GetHeaderHTML()
	{
	  return '';
	}

	/**
	 * Use this method to prevent the admin interface from outputting header, footer,
	 * theme, etc, so your module can output files directly to the administrator.
	 * Do this by returning true.
	 *
	 * @param  array The input request.  This can be used to test conditions wether or not admin output should be suppressed.
	 * @return boolean
	 */
	function SuppressAdminOutput(&$request)
	{
		return false;
	}

	/**
	 * Register a route to use for pretty url parsing
	 *
	 * @final
	 * @see SetParameters
	 * @param string Regular Expression Route to register
	 * @param array Defaults for parameters that might not be included in the url
	 * @return void
	 */
	function RegisterRoute($routeregex, $defaults = array())
	{
	}


	/**
	 * Returns a list of parameters and their help strings in a hash.  This is generally
	 * used internally.
	 *
	 * @final
	 * @internal
	 * @return array
	 */
	function GetParameters()
	{
	  if( count($this->params) == 1 && $this->params[0]['name'] == 'lang' )
	    {
	      // quick hack to load parameters if they are not already loaded.
	      $this->SetParameters();
	    }
	  return $this->params;
	}

	/**
	 * Called from within the constructor, ONLY for frontend module 
         * actions.  This method should be overridden to create routes, and
	 * set handled parameters, and perform other initialization tasks
	 * that need to be setup for all frontend actions.
	 * 
	 * @abstract
	 * @see SetParameterType
	 * @see CreateParameter
	 * @see RegisterRoute
	 * @see RestrictUnknownParams
	 * @see RegisterModulePlugin
	 * @return void
	 */
	function SetParameters()
	{
	}

	/**
	 * A method to indicate that the system should drop and optionally
	 * generate an error about unknown parameters on frontend actions.
	 * 
	 * @see SetParameterType
	 * @see CreateParameter
	 * @final
	 * @return void
	 */
	function RestrictUnknownParams($flag = true)
	{
	  $this->restrict_unknown_params = $flag;
	}


	/**
	 * Indicate the name of, and type of a parameter that is 
	 * acceptable for frontend actions.
	 *
	 * possible values for type are:
	 * CLEAN_INT,CLEAN_FLOAT,CLEAN_NONE,CLEAN_STRING
	 * 
	 * i.e:
	 * $this->SetParameterType('numarticles',CLEAN_INT);
	 *
	 * @see CreateParameter
	 * @see SetParameters
	 * @final
	 * @param string Parameter name;
	 * @param define Parameter type;
	 * @return void;
	 */
	function SetParameterType($param, $type)
	{
	  switch($type)
	    {
	    case CLEAN_INT:
	    case CLEAN_FLOAT:
	    case CLEAN_NONE:
	    case CLEAN_STRING:
	      $this->param_map[trim($param)] = $type;
	      break;
	    default:
	      trigger_error('Attempt to set invalid parameter type');
	      break;
	    }
	}


	/**
	 * Create a parameter and it's documentation for display in the
	 * module help.
	 *
	 * i.e:
	 * $this->CreateParameter('numarticles',100000,$this->Lang('help_numarticles'),true);
	 *
	 * @see SetParameters
	 * @see SetParameterType
	 * @final
	 * @param string Parameter name;
	 * @param string Default parameter value
	 * @param string Help String
	 * @param boolean Flag indicating wether this parameter is optional or required.
	 * @return void;
	 */
	function CreateParameter($param, $defaultval='', $helpstring='', $optional=true)
	{
		//was: array_unshift(
		array_push($this->params, array(
			'name' => $param,
			'default' => $defaultval,
			'help' => $helpstring,
			'optional' => $optional
		));
	}

	/**
	 * Returns a short description of the module
	 *
	 * @abstract
	 * @param string Optional language that the admin is using.	 If that language
	 * is not defined, use en_US.
	 * @return string
	 */
	function GetDescription($lang = 'en_US')
	{
		return '';
	}

	/**
	 * Returns a description of what the admin link does.
	 *
	 * @abstract
	 * @param string Optional language that the admin is using.	 If that language
	 * is not defined, use en_US.
	 * @return string
	 */
	//	function GetAdminDescription($lang = 'en_US')
	function GetAdminDescription()
	{
		return '';
	}

	/**
	 * Returns whether this module should only be loaded from the admin
	 *
	 * @abstract
	 * @return boolean
	 */
	function IsAdminOnly()
	{
		return false;
	}

	/**
	 * Returns the changelog for the module
	 *
	 * @return string HTML text of the changelog.
	 */
	function GetChangeLog()
	{
		return '';
	}

	/**
	 * Returns the name of the author
	 *
	 * @abstract
	 * @return string The name of the author.
	 */
	function GetAuthor()
	{
		return '';
	}

	/**
	 * Returns the email address of the author
	 *
	 * @abstract
	 * @return string The email address of the author.
	 */
	function GetAuthorEmail()
	{
		return '';
	}

	/**
	 * ------------------------------------------------------------------
	 * Reference functions
	 * ------------------------------------------------------------------
	 */

	/**
	 * Returns the cms->config object as a reference
	 * 
	 * @final
	 * @deprecated
	 * @return array The config hash.
	 */
	function GetConfig()
	{
		global $gCms;
		$config = $gCms->GetConfig();
		return $config;
	}

	/**
	 * Returns the cms->db object as a reference
	 *
	 * @final
	 * @deprecated
	 * @return object Adodb Database object.
	 */
	function & GetDb()
	{
		global $gCms;
		$db = &$gCms->GetDb();
		return $db;
	}

	/**
	 * Returns CMSMS temporary and state variables.
	 *
	 * @final
	 * @return array a hash of CMSMS temporary variables.
	 */
	function & GetVariables()
	{
		return $this->cms->variables;
	}

	/**
	 * ------------------------------------------------------------------
	 * Content Block Related Functions
	 * ------------------------------------------------------------------
	 */
	
	/**
	 * Get an input field for a module generated content block type.
	 *
	 * This method can be overridden if the module is providing content
         * block types to the CMSMS content objects.
	 *
	 * @abstract
	 * @param string Content block name
	 * @param mixed  Content block value
	 * @param array  Content block parameters
	 * @param boolean A flag indicating wether the content editor is in create mode (adding) vs. edit mod.
	 * @return mixed Either an array with two elements (prompt, and xhtml element) or a string containing only the xhtml input element.
	 */
	function GetContentBlockInput($blockName,$value,$params,$adding = false)
	{
	  return FALSE;
	}

	/**
	 * Return a value for a module generated content block type.
	 * Given input parameters (i.e: via _POST or _REQUEST), this method
	 * will extract a value for the given content block information.
	 *
	 * This method can be overridden if the module is providing content
         * block types to the CMSMS content objects.
	 *
	 * @abstract
	 * @param string Content block name
	 * @param array  Content block parameters
	 * @param array  input parameters
	 * @return mixed The content block value if possible.
	 */
	function GetContentBlockValue($blockName,$blockParams,$inputParams)
	{
	  return FALSE;
	}


	/**
	 * Validate the value for a module generated content block type.
	 *
	 * This method can be overridden if the module is providing content
         * block types to the CMSMS content objects.
	 *
	 * @abstract
	 * @param string Content block name
	 * @param mixed  Content block value
	 * @param arrray Content block parameters.
	 * @return string An error message if the value is invalid, empty otherwise.
	 */
	function ValidateContentBlockValue($blockName,$value,$blockparams)
	{
	  return '';
	}

	/**
	 * ------------------------------------------------------------------
	 * Content Type Related Functions
	 * ------------------------------------------------------------------
	 */

	/**
	 * Does this module support a custom content type?
	 *
	 * This method can be overridden if the module is defining one or more
	 * custom content types (note this is different than content block 
         * types)
	 *
	 * @abstract
	 * @return boolean
	 */
	function HasContentType()
	{
		return FALSE;
	}
	
	/**
	 * Register a new content type
	 *
	 * @deprecated
	 * @final
	 * @param string A name for the new content type
	 * @param string A filename containing the content type definition.
	 * @param string A friendly name for this content type.
	 * @return void
	 */
	function RegisterContentType($name, $file, $friendlyname = '')
	{
	  $contentops = cmsms()->GetContentOperations();
	  $types = $contentops->ListContentTypes();
	  if( !in_array(strtolower($name),array_keys($types)) )
	    {
	      $obj = new CmsContentTypePlaceholder();
	      $obj->class = $name;
	      $obj->type  = strtolower($name);
	      $obj->filename = $file;
	      $obj->loaded = false;
	      $obj->friendlyname = ($friendlyname != '' ? $friendlyname : $name);
	      $contentops->register_content_type($obj);
	    }
	}

	/**
	 * Return an instance of the new content type
	 *
	 * This method must be overridden if this object is providing
	 * a content type.
	 *
	 * @abstract
	 * @deprecated
	 * @return object
	 */
	function GetContentTypeInstance()
	{
		return FALSE;
	}

// 	// what is this?
// 	function IsExclusive()
// 	{
// 		return FALSE;
// 	}

	/**
	 * ------------------------------------------------------------------
	 * Installation Related Functions
	 * ------------------------------------------------------------------
	 */

	/**
	 * Function that will get called as module is installed. This function should
	 * do any initialization functions including creating database tables. It
	 * should return a string message if there is a failure. Returning nothing (FALSE)
	 * will allow the install procedure to proceed.
	 *
	 * The default behavior of this method is to include a file named method.install.php
	 * in the module directory, if one can be found.  This provides a way of splitting
	 * secondary functions into other files.
	 *
	 * @abstract
	 * @return mixed FALSE indicates no error.  Any other value will be used as an error message.
	 */
	function Install()
	{
		$filename = $this->GetModulePath().'/'.$this->GetName().'/method.install.php';
		echo $filename;
		if (@is_file($filename))
		{
			{
				global $gCms;
				$db = $gCms->GetDb();
				$config = $gCms->GetConfig();

				$res = include($filename);
				if( $res == 1 || $res == '' ) return FALSE;
				return $res;
			}
		}
		else
		{
			return FALSE;
		}
	}


	/**
	 * Display a message after a successful installation of the module.
	 *
	 * @abstract
	 * @return XHTML Text
	 */
	function InstallPostMessage()
	{
		return FALSE;
	}


	/**
	 * Function that will get called as module is uninstalled. This function should
	 * remove any database tables that it uses and perform any other cleanup duties.
	 * It should return a string message if there is a failure. Returning nothing
	 * (FALSE) will allow the uninstall procedure to proceed.
	 *
	 * The default behaviour of this function is to include a file called method.uninstall.php
	 * in your module directory to do uninstall operations.
	 *
	 * @abstract
	 * @return mixed FALSE indicates that the module uninstalled correctly, any other value indicates an error message.
	 */
	function Uninstall()
	{
		$filename = $this->GetModulePath().'/'.$this->GetName().'/method.uninstall.php';
		if (@is_file($filename))
		{
		  global $gCms;
		  $db =& $gCms->GetDb();
		  $config = $gCms->GetConfig();
		  
		  $res = include($filename);
		  if( $res == 1 || $res == '') return FALSE;
		  if( is_string($res)) 
		    {
		      $modops = $gCms->GetModuleOperations();
		      $modops->SetError($res);
		    }
		  return $res;
		}
		else
		{
			return FALSE;
		}
	}
	
	/**
	 * Function that gets called upon module uninstall, and returns a boolean to indicate whether or
	 * not the core should remove all module events, event handlers, module templates, and preferences.
	 * The module must still remove its own database tables and permissions
	 * @abstract
	 * @return boolean whether the core may remove all module events, event handles, module templates, and preferences on uninstall (defaults to true)
	 */
	function AllowUninstallCleanup()
	{
		return true;
	}

	/**
	 * Display a message and a Yes/No dialog before doing an uninstall.	 Returning noting
	 * (FALSE) will go right to the uninstall.
	 *
	 * @abstract
	 * @return XHTML Text, or FALSE.
	 */
	function UninstallPreMessage()
	{
		return FALSE;
	}

	/**
	 * Display a message after a successful uninstall of the module.
	 *
	 * @abstract
	 * @return XHTML Text, or FALSE
	 */
	function UninstallPostMessage()
	{
		return FALSE;
	}

	/**
	 * Function to perform any upgrade procedures. This is mostly used to for
	 * updating databsae tables, but can do other duties as well. It should
	 * return a string message if there is a failure. Returning nothing (FALSE)
	 * will allow the upgrade procedure to proceed. Upgrades should have a path
	 * so that they can be upgraded from more than one version back.  While not
	 * a requirement, it makes life easy for your users.
	 *
	 * The default behavior of this method is to call a function called method.upgrade.php
	 * in your module directory, if it exists.
	 *
	 * @param string The version we are upgrading from
	 * @param string The version we are upgrading to
	 * @return boolean
	 */
	function Upgrade($oldversion, $newversion)
	{
		$filename = $this->GetModulePath().'/'.$this->GetName().'/method.upgrade.php';
		if (@is_file($filename))
		{
			{
				global $gCms;
				$db =& $gCms->GetDb();
				$config = $gCms->GetConfig();

				$res = include($filename);
				if( $res == 1 || $res == '' ) return TRUE;
				$modops = $gCms->GetModuleOperations();
				$modops->SetError($res);
				return FALSE;
			}
		}
	}

	/**
	 * Returns whether or not modules should be autoupgraded while upgrading
	 * CMS versions.  Generally only useful for modules included with the CMS
	 * base install, but there could be a situation down the road where we have
	 * different distributions with different modules included in them.	 Defaults
	 * to TRUE, as there is not many reasons to not allow it.
	 *
	 * @abstract
	 * @return boolean
	 */
	function AllowAutoInstall()
	{
		return TRUE;
	}

	/**
	 * Returns whether or not modules should be autoupgraded while upgrading
	 * CMS versions.  Generally only useful for modules included with the CMS
	 * base install, but there could be a situation down the road where we have
	 * different distributions with different modules included in them.	 Defaults
	 * to TRUE, as there is not many reasons to not allow it.
	 *
	 * @abstract
	 * @return boolean
	 */
	function AllowAutoUpgrade()
	{
		return TRUE;
	}

	/**
	 * Returns a list of dependencies and minimum versions that this module
	 * requires. It should return an hash, eg.
	 * return array('somemodule'=>'1.0', 'othermodule'=>'1.1');
	 *
	 * @abstract
	 * @return array
	 */
	function GetDependencies()
	{
		return array();
	}

	/**
	 * Checks to see if currently installed modules depend on this module.	This is
	 * used by the plugins.php page to make sure that a module can't be uninstalled
	 * before any modules depending on it are uninstalled first.
	 *
	 * @internal
	 * @final
	 * @return boolean
	 */
	function CheckForDependents()
	{
		global $gCms;
		$db =& $gCms->GetDb();

		$result = false;

		$query = "SELECT child_module FROM ".cms_db_prefix()."module_deps WHERE parent_module = ? LIMIT 1";
		$tmp = $db->GetOne($query,array($this->GetName()));
		if( $tmp ) 
		  {
		    $result = true;
		  }

		return $result;
	}

	/**
	 * Return true if there is an admin for the module.	 Returns false by
	 * default.
	 *
	 * @abstract
	 * @return boolean
	 */
	function HasAdmin()
	{
		return false;
	}

	/**
	 * Should we use output buffering in the admin for this module?
	 *
	 * @abstract
	 * @return boolean
	 */
	function HasAdminBuffering()
	{
		return true;
	}

	/**
	 * Returns which admin section this module belongs to.
	 * this is used to place the module in the appropriate admin navigation
	 * section. Valid options are currently:
	 *
	 * content, layout, files, usersgroups, extensions, preferences, admin
	 *
	 * @abstract
	 * @return string
	 */
	function GetAdminSection()
	{
		return 'extensions';
	}

	/**
	 * Returns true or false, depending on whether the user has the
	 * right permissions to see the module in their Admin menus.
	 *
	 * Typically permission checks are done in the overriden version of
	 * this method.
	 *
	 * Defaults to true.
	 *
	 * @abstract
	 * @return boolean
	 */
	function VisibleToAdminUser()
	{
		// check for methods
		if(method_exists($this, 'get_modulehierarchy')
		&& method_exists($this, 'GetName')) {

			// get levels and module name
			$levels = $this->get_modulehierarchy();
			$module_name = $this->GetName();

			// go thorugh levels and check if user has access to any level
			foreach ($levels as $level_name) {
				if($this->CheckPermission("module_" . $module_name . "_manage_" . $level_name)) {
					return true;
				}
			}

			// check for advanced permisions
			return $this->CheckPermission("module_" . $module_name . "_advanced");

		}

		// default
		return true;
	}

	/**
	 * Returns true if the module should be treated as a content module.
	 * Returns false by default.
	 *
	 * @abstract
	 * @return boolean
	 */
	function IsContentModule()
	{
		return false;
	}

	/**
	 * Returns true if the module should be treated as a plugin module (like
	 * {cms_module module='name'}.	Returns false by default.
	 *
	 * @abstract
	 * @return boolean
	 */
	function IsPluginModule()
	{
		return false;
	}


	/**
	 * Returns true if the module acts as a soap server
	 *
	 * @abstract
	 * @return boolean
	 */
	function IsSoapModule()
	{
		return false;
	}

	/**
	 * Returns true if the module may support lazy loading in the front end
	 *
	 * This functionality is not completely implemented in version 1.8
	 *
	 * @abstract
	 * @return boolean
	 */
	function SupportsLazyLoading()
	{
	  return false;
	}

	/**
	 * ------------------------------------------------------------------
	 * User Related Functions
	 * ------------------------------------------------------------------
	 */

	/**
	 * Called before a user is added to the database.  Sends the user object.
	 *
	 * @deprecated
	 * @abstract
	 * @param User The user that was just created
	 * @return void
	 */
	function AddUserPre(&$user)
	{
	}

	/**
	 * Called after a user is added to the database.  Sends the user object.
	 *
	 * @deprecated
	 * @abstract
	 * @param User The user that was just created
	 * @return void
	 */
	function AddUserPost(&$user)
	{
	}

	/**
	 * Called before a user is saved to the database.  Sends the user object.
	 *
	 * @deprecated
	 * @abstract
	 * @param User The user that was just created
	 * @return void
	 */
	function EditUserPre(&$user)
	{
	}

	/**
	 * Called after a user is saved to the database.  Sends the user object.
	 *
	 * @deprecated
	 * @abstract
	 * @param User The user that was just created
	 * @return void
	 */
	function EditUserPost(&$user)
	{
	}

	/**
	 * Called before a user is deleted from the database.  Sends the user object.
	 *
	 * @deprecated
	 * @abstract
	 * @param User The user that was just created
	 * @return void
	 */
	function DeleteUserPre(&$user)
	{
	}

	/**
	 * Called after a user is deleted from the database.  Sends the user object.
	 *
	 * @deprecated
	 * @abstract
	 * @param User The user that was just created
	 * @return void
	 */
	function DeleteUserPost(&$user)
	{
	}

	/**
	 * ------------------------------------------------------------------
	 * Group Related Functions
	 * ------------------------------------------------------------------
	 */

	/**
	 * Called before a group is added to the database.	Sends the group object.
	 *
	 * @deprecated
	 * @abstract
	 * @param Group The group that was just created
	 * @return void
	 */
	function AddGroupPre(&$group)
	{
	}

	/**
	 * Called after a group is added to the database.  Sends the group object.
	 *
	 * @deprecated
	 * @abstract
	 * @param Group The group that was just created
	 * @return void
	 */
	function AddGroupPost(&$group)
	{
	}

	/**
	 * Called before a group is saved to the database.	Sends the group object.
	 *
	 * @deprecated
	 * @abstract
	 * @param Group The group that was just created
	 * @return void
	 */
	function EditGroupPre(&$group)
	{
	}

	/**
	 * Called after a group is saved to the database.  Sends the group object.
	 *
	 * @deprecated
	 * @abstract
	 * @param Group The group that was just created
	 * @return void
	 */
	function EditGroupPost(&$group)
	{
	}

	/**
	 * Called before a group is deleted from the database.	Sends the group object.
	 *
	 * @deprecated
	 * @abstract
	 * @param Group The group that was just created
	 * @return void
	 */
	function DeleteGroupPre(&$group)
	{
	}

	/**
	 * Called after a group is deleted from the database.  Sends the group object.
	 *
	 * @deprecated
	 * @abstract
	 * @param Group The group that was just created
	 * @return void
	 */
	function DeleteGroupPost(&$group)
	{
	}

	/**
	 * ------------------------------------------------------------------
	 * Template Related Functions
	 * ------------------------------------------------------------------
	 */

	/**
	 * Called before a template is added to the database.  Sends the template
	 * object.
	 *
	 * @deprecated
	 * @abstract
	 * @param Template The template object.
	 * @return void
	 */
	function AddTemplatePre(&$template)
	{
	}

	/**
	 * Called after a template is added to the database.  Sends the template
	 * object.
	 *
	 * @deprecated
	 * @abstract
	 * @param Template The template object.
	 * @return void
	 */
	function AddTemplatePost(&$template)
	{
	}

	/**
	 * Called before a template is saved to the database.  Sends the template
	 * object.
	 *
	 * @deprecated
	 * @abstract
	 * @param Template The template object.
	 * @return void
	 */
	function EditTemplatePre(&$template)
	{
	}

	/**
	 * Called after a template is saved to the database.  Sends the template
	 * object.
	 *
	 * @deprecated
	 * @abstract
	 * @param Template The template object.
	 * @return void
	 */
	function EditTemplatePost(&$template)
	{
	}

	/**
	 * Called before a template is deleted from the database.  Sends the template
	 * object.
	 *
	 * @deprecated
	 * @abstract
	 * @param Template The template object.
	 * @return void
	 */
	function DeleteTemplatePre(&$template)
	{
	}

	/**
	 * Called after a template is deleted from the database.  Sends the template
	 * object.
	 *
	 * @deprecated
	 * @abstract
	 * @param Template The template object.
	 * @return void
	 */
	function DeleteTemplatePost(&$template)
	{
	}

	/**
	 * ------------------------------------------------------------------
	 * General Content Related Functions
	 * ------------------------------------------------------------------
	 */

	/**
	 * Called before a content edit is saved to the database.
	 * Sends the content object.
	 *
	 * @deprecated
	 * @abstract
	 * @param Content A content object
	 * @return void
	 */
	function ContentEditPre(&$content)
	{
	}

	/**
	 * Called After a content edit is saved to the database.
	 * Sends the content object.
	 *
	 * @deprecated
	 * @abstract
	 * @param Content A content object
	 * @return void
	 */
	function ContentEditPost(&$content)
	{
	}

	/**
	 * Called before a content edit is deleted
	 * Sends the content object.
	 *
	 * @deprecated
	 * @abstract
	 * @param Content A content object
	 * @return void
	 */
	function ContentDeletePre(&$content)
	{
	}

	/**
	 * Called after a content edit is deleted
	 * Sends the content object.
	 *
	 * @deprecated
	 * @abstract
	 * @param Content A content object
	 * @return void
	 */
	function ContentDeletePost(&$content)
	{
	}

	/**
	 * ------------------------------------------------------------------
	 * Stylesheet Related Functions
	 * ------------------------------------------------------------------
	 */

	/**
	 * Called before a Stylesheet is added to the database.	 Sends the stylesheet
	 * object.
	 *
	 * @deprecated
	 * @abstract
	 * @param Stylesheet The stylesheet that was just created
	 * @return void
	 */
	function AddStylesheetPre(&$stylesheet)
	{
	}

	/**
	 * Called after a stylesheet is added to the database.	Sends the stylesheet
	 * object.
	 *
	 * @deprecated
	 * @abstract
	 * @param Stylesheet The stylesheet that was just created
	 * @return void
	 */
	function AddStylesheetPost(&$stylesheet)
	{
	}

	/**
	 * Called before a stylesheet is saved to the database.	 Sends the stylesheet
	 * object.
	 *
	 * @deprecated
	 * @abstract
	 * @param Stylesheet The stylesheet that was just created
	 * @return void
	 */
	function EditStylesheetPre(&$stylesheet)
	{
	}

	/**
	 * Called after a stylesheet is saved to the database.	Sends the stylesheet
	 * object.
	 *
	 * @deprecated
	 * @abstract
	 * @param Stylesheet The stylesheet that was just created
	 * @return void
	 */
	function EditStylesheetPost(&$stylesheet)
	{
	}

	/**
	 * Called before a stylesheet is deleted from the database.	 Sends the stylesheet
	 * object.
	 *
	 * @deprecated
	 * @abstract
	 * @param Stylesheet The stylesheet that was just created
	 * @return void
	 */
	function DeleteStylesheetPre(&$stylesheet)
	{
	}

	/**
	 * Called after a stylesheet is deleted from the database.	Sends the stylesheet
	 * object.
	 *
	 * @deprecated
	 * @abstract
	 * @param Stylesheet The stylesheet that was just created
	 * @return void
	 */
	function DeleteStylesheetPost(&$stylesheet)
	{
	}

	/**
	 * ------------------------------------------------------------------
	 * HTML Blob / GCB Related Functions
	 * ------------------------------------------------------------------
	 */

	/**
	 * Called before an HTML blob is added to the database.	 Sends the html blob
	 * object.
	 *
	 * @deprecated
	 * @abstract
	 * @param HtmlBlob The HTML blob
	 * @return void
	 */
	function AddHtmlBlobPre(&$htmlblob)
	{
	}

	/**
	 * Called after an HTML blob is added to the database.	Sends the html blob
	 * object.
	 *
	 * @deprecated
	 * @abstract
	 * @param HtmlBlob The HTML blob
	 * @return void
	 */
	function AddHtmlBlobPost(&$htmlblob)
	{
	}

	/**
	 * Called before an HTML blob is saved to the database.	 Sends the html blob
	 * object.
	 *
	 * @deprecated
	 * @abstract
	 * @param HtmlBlob The HTML blob
	 * @return void
	 */
	function EditHtmlBlobPre(&$htmlblob)
	{
	}

	/**
	 * Called after an HTML blob is saved to the database.	Sends the html blob
	 * object.
	 *
	 * @deprecated
	 * @abstract
	 * @param HtmlBlob The HTML blob
	 * @return void
	 */
	function EditHtmlBlobPost(&$htmlblob)
	{
	}

	/**
	 * Called before an HTML blob is deleted from the database.	 Sends the html
	 * blob object.
	 *
	 * @deprecated
	 * @abstract
	 * @param HtmlBlob The HTML blob
	 * @return void
	 */
	function DeleteHtmlBlobPre(&$htmlblob)
	{
	}

	/**
	 * Called after an HTML blob is deleted from the database.	Sends the html
	 * blob object.
	 *
	 * @deprecated
	 * @abstract
	 * @param HtmlBlob The HTML blob
	 * @return void
	 */
	function DeleteHtmlBlobPost(&$htmlblob)
	{
	}

	/**
	 * ------------------------------------------------------------------
	 * Content Related Functions
	 * ------------------------------------------------------------------
	 */


	function SmartyPreCompile(&$content)
	{
	}

	function SmartyPostCompile(&$content)
	{
	}

	/**
	 * ------------------------------------------------------------------
	 * Module capabilities, a new way of checking what a module can do
	 * ------------------------------------------------------------------
	 */
	
	/**
	 * Returns true if the modules thinks it has the capability specified
	 *
	 * @abstract
	 * @param an id specifying which capability to check for, could be "wysiwyg" etc.
	 * @param associative array further params to get more detailed info about the capabilities. Should be syncronized with other modules of same type
	 * @return boolean
	 */
	function HasCapability($capability, $params=array())
	{
	  return false;
	}
	
	/**
	 * ------------------------------------------------------------------
	 * Syntax Highlighter Related Functions
	 *
	 * These functions are only used if creating a syntax highlighter module.
	 * ------------------------------------------------------------------
	 */

	/**
	 * Returns true if this module should be treated as a Syntax Highlighting module. It
	 * returns false be default.
	 *
	 * @abstract
	 * @return boolean
	 */
	function IsSyntaxHighlighter()
	{
		return false;
	}
	
	/**
	 * Returns true if this SyntaxHighlighter should be considered active, eventhough it's
	 * not the choice of the user. Used for forcing a wysiwyg.
	 * returns false be default.
	 *
	 * @abstract
	 * @return boolean
	 */
	function SyntaxActive()
	{
		return $this->syntaxactive;
	}

	/**
	 * Returns content destined for the <form> tag.	 It's useful if javascript is
	 * needed for the onsubmit of the form.
	 *
	 * @abstract
	 * @return string
	 */
	function SyntaxPageForm()
	{
		return '';
	}

	/**
	 * This is a function that would be called before a form is submitted.
	 * Generally, a dropdown box or something similar that would force a submit
	 * of the form via javascript should put this in their onchange line as well
	 * so that the SyntaxHighlighter can do any cleanups before the actual form submission
	 * takes place.
	 *
	 * @abstract
	 * @return string
	 */
	 function SyntaxPageFormSubmit()
	 {
		return '';
	 }

	 /**
	  * Returns header code specific to this SyntaxHighlighter
	  *
	  *
	  * @abstract
	  * @param string The html-code of the page before replacing SyntaxHighlighter-stuff
	  * @return string
	  */
	  function SyntaxGenerateHeader($htmlresult='')
	  {
		return '';
	  }

	 /**
	  * Returns body code specific to this SyntaxHighlighter
	  *
	  * @abstract
	  * @return string
	  */
	  function SyntaxGenerateBody()
	  {
		return '';
	  }

	/**
	 * Returns the textarea specific for this WYSIWYG.
	 *
	 * @abstract
	 * @param string HTML name of the textarea
	 * @param string the language which the content should be highlighted as
	 * @param int Number of columns wide that the textarea should be
	 * @param int Number of rows long that the textarea should be
	 * @param string Encoding of the content
	 * @param string Content to show in the textarea
	 * @param string Stylesheet for content, if available
	 * @return string
	 */
	  function SyntaxTextarea($name='textarea',$syntax='html',$columns='80',$rows='15',$encoding='',$content='',$stylesheet='',$addtext='')
	{
		$this->syntaxactive=true;
		return '<textarea name="'.$name.'" cols="'.$columns.'" rows="'.$rows.'" '.$addtext.' >'.$content.'</textarea>';
	}

	/**
	 * Returns whether or not this module should show in any module lists generated by a WYSIWYG.
	 *
	 * @abstract
	 * @return boolean
	 */
	function ShowInSyntaxList()
	{
		return true;
	}
	
	
	
	/**
	 * ------------------------------------------------------------------
	 * WYSIWYG Related Functions
	 *
	 * These methods are only useful for creating wysiwyg editor modules.
	 * ------------------------------------------------------------------
	 */

	/**
	 * Returns true if this module should be treated as a WYSIWYG module. It
	 * returns false be default.
	 *
	 * @abstract
	 * @return boolean
	 */
	function IsWYSIWYG()
	{
		return false;
	}

	/**
	 * Returns true if this wysiwyg should be considered active, eventhough it's
	 * not the choice of the user. Used for forcing a wysiwyg.
	 * returns false be default.
	 *
	 * @abstract
	 * @return boolean
	 */
	function WYSIWYGActive()
	{
		return $this->wysiwygactive;
	}

	/**
	 * Returns content destined for the <form> tag.	 It's useful if javascript is
	 * needed for the onsubmit of the form.
	 *
	 * @abstract
	 * @return string
	 */
	function WYSIWYGPageForm()
	{
		return '';
	}

	/**
	 * This is a function that would be called before a form is submitted.
	 * Generally, a dropdown box or something similar that would force a submit
	 * of the form via javascript should put this in their onchange line as well
	 * so that the WYSIWYG can do any cleanups before the actual form submission
	 * takes place.
	 *
	 * @abstract
	 * @return string
	 */
	 function WYSIWYGPageFormSubmit()
	 {
		return '';
	 }

	 /**
	  * Returns header code specific to this WYSIWYG
	  *
	  * @abstract
	  * @param string The html-code of the page before replacing WYSIWYG-stuff
	  * @return string
	  */
	  function WYSIWYGGenerateHeader($htmlresult='')
	  {
		return '';
	  }

	 /**
	  * Returns body code specific to this WYSIWYG
	  *
	  * @abstract
	  * @return string
	  */
	  function WYSIWYGGenerateBody()
	  {
		return '';
	  }

	/**
	 * Returns the textarea specific for this WYSIWYG.
	 *
	 * @abstract
	 * @param string HTML name of the textarea
	 * @param int Number of columns wide that the textarea should be
	 * @param int Number of rows long that the textarea should be
	 * @param string Encoding of the content
	 * @param string Content to show in the textarea
	 * @param string Stylesheet for content, if available
	 * @return string
	 */
	  function WYSIWYGTextarea($name='textarea',$columns='80',$rows='15',$encoding='',$content='',$stylesheet='',$addtext='')
	{
		$this->wysiwygactive=true;
		return '<textarea name="'.$name.'" cols="'.$columns.'" rows="'.$rows.'" '.$addtext.' >'.$content.'</textarea>';
	}

	/**
	 * Returns whether or not this module should show in any module lists generated by a WYSIWYG.
	 *
	 * @abstract
	 * @return boolean
	 */
	function ShowInWYSIWYG()
	{
		return true;
	}

	/**
	 * ------------------------------------------------------------------
	 * Navigation Related Functions
	 * ------------------------------------------------------------------
	 */

	/**
	 * Used for navigation between "pages" of a module.	 Forms and links should
	 * pass an action with them so that the module will know what to do next.
	 * By default, DoAction will be passed 'default' and 'defaultadmin',
	 * depending on where the module was called from.  If being used as a module
	 * or content type, 'default' will be passed.  If the module was selected
	 * from the list on the admin menu, then 'defaultadmin' will be passed.
	 *
	 * In order to allow splitting up functionality into multiple PHP files the default
	 * behaviour of this method is to look for a file named action.<action name>.php
	 * in the modules directory, and if it exists include it.
	 *
	 * @abstract
	 * @param string Name of the action to perform
	 * @param string The ID of the module
	 * @param string The parameters targeted for this module
	 * @return string output XHTML.
	 */
	function DoAction($name, $id, $params, $returnid='')
	{
		if ($name != '')
		{
			//Just in case DoAction is called directly and it's not overridden.
			//See: http://0x6a616d6573.blogspot.com/2010/02/cms-made-simple-166-file-inclusion.html
			$name = preg_replace('/[^A-Za-z0-9\-_+]/', '', $name);

			// vcms new way ==>
			$filename = $this->GetModulePath() . '/action.' . $name . '.php';

			if (@is_file($filename))
			{
				{
					global $gCms;
					$db =& $gCms->GetDb();
					$config = $gCms->GetConfig();

					include($filename);

				}
			}
		}
	}

	/**
	 * This method prepares the data and does appropriate checks before
	 * calling a module action.
	 *
	 * @internal
	 * @final
	 * @access private
	 */
	function DoActionBase($name, $id, $params, $returnid='')
	{
          $name = preg_replace('/[^A-Za-z0-9\-_+]/', '', $name);
	  if( $returnid != '' )
	    {
	      // used to try to avert XSS flaws, this will
	      // clean as many parameters as possible according
	      // to a map specified with the SetParameterType metods.
	      $params = cleanParamHash($this->GetName(),$params,$this->param_map,
				       !$this->restrict_unknown_params);
	    }

	  if (isset($params['lang']))
	    {
	      $this->curlang = $params['lang'];
	      $this->langhash = array();
	    }
	  if( !isset($params['action']) )
	    {
	      $params['action'] = $name;
	    }
	  $params['action'] = cms_htmlentities($params['action']);
	  $returnid = cms_htmlentities($returnid);
	  $id = cms_htmlentities($id);
	  $name = cms_htmlentities($name);
	  $output = $this->DoAction($name, $id, $params, $returnid);

	  return $output;
	}



  /**
   * ------------------------------------------------------------------
   * Form and XHTML Related Methods
   * ------------------------------------------------------------------
   */

  /**
   * Returns the xhtml equivalent of a tooltip help link.
   *
   * @final
   * @param string The help text to be shown on mouse over
   * @param string The text to be shown as the link, default to a simple question mark
   * @param string Forces another width of the popupbox than the one set in admin css
   * @param string An alternative classname for the a-link of the tooltip
   * @return string
   */
  function CreateTooltip($helptext, $linktext="?", $forcewidth="", $classname="admin-tooltip admin-tooltip-box", $href="")
  {
    $result='<a class="'.$classname.'"';
    if ($href!='') $result.=' href="'.$href.'"';
    $result.='>'.$linktext.'<span';
    if ($forcewidth!="" && is_numeric($forcewidth)) $result.=' style="width:'.$forcewidth.'px"';
    $result.='>'.htmlentities($helptext)."</span></a>\n";
    return $result;
  }

  /**
   * Returns the xhtml equivalent of a tooltip-enabled href link	 This is basically a nice little wrapper
   * to make sure that id's are placed in names and also that it's xhtml compliant.
   *
   * @final
   * @param string The id given to the module on execution
   * @param string The action that this form should do when the link is clicked
   * @param string The id to eventually return to when the module is finished it's task
   * @param string The text that will have to be clicked to follow the link
   * @param string The helptext to be shown as tooltip-popup
   * @param string An array of params that should be inlucded in the URL of the link.	 These should be in a $key=>$value format.
   * @return string
   */
  function CreateTooltipLink($id, $action, $returnid, $contents, $tooltiptext, $params=array())
  {
    return $this->CreateTooltip($tooltiptext,$contents,"","admin-tooltip",$this->CreateLink($id,$action,$returnid,"admin-tooltip",$params,"",true) );
  }

  /**
   * Returns the xhtml equivalent of an fieldset and legend.  This is basically a nice little wrapper
   * to make sure that id's are placed in names and also that it's xhtml compliant.
   *
   * @final
   * @param string The id given to the module on execution (not really used yet, but might be later)
   * @param string The html name of the textbox (not really used yet, but might be later on)
   * @param string The legend_text for this fieldset, if applicaple
   * @param string Any additional text that should be added into the tag when rendered
   * @param string Any additional text that should be added into the legend tag when rendered
   * @return string
   */
  function CreateFieldsetStart( $id, $name, $legend_text='', $addtext='', $addtext_legend='' )
  {
    $this->LoadFormMethods();
    return cms_module_CreateFieldsetStart($this, $id, $name, $legend_text, $addtext, $addtext_legend);
  }

  /**
   * Returns the end of the fieldset in a  form.  This is basically just a wrapper around </form>, but
   * could be extended later on down the road.  It's here mainly for consistency.
   * 
   * @final
   * @return string
   */
  function CreateFieldsetEnd()
  {
    return '</fieldset>'."\n";
  }


  /**
   * Returns the start of a module form, optimized for frontend use
   *
   * @param string The id given to the module on execution
   * @param string The id to eventually return to when the module is finished it's task
   * @param string The action that this form should do when the form is submitted
   * @param string Method to use for the form tag.  Defaults to 'post'
   * @param string Optional enctype to use, Good for situations where files are being uploaded
   * @param boolean A flag to determine if actions should be handled inline (no moduleinterface.php -- only works for frontend)
   * @param string Text to append to the end of the id and name of the form
   * @param array Extra parameters to pass along when the form is submitted
   * @return string
   */
  function CreateFrontendFormStart($id,$returnid,$action='default',$method='post',
				   $enctype='',$inline=true,$idsuffix='',$params=array())
  {
    return $this->CreateFormStart($id,$action,$returnid,$method,$enctype,$inline,$idsuffix,$params);
  }


  /**
   * Returns the start of a module form
   *
   * @param string The id given to the module on execution
   * @param string The action that this form should do when the form is submitted
   * @param string The id to eventually return to when the module is finished it's task
   * @param string Method to use for the form tag.  Defaults to 'post'
   * @param string Optional enctype to use, Good for situations where files are being uploaded
   * @param boolean A flag to determine if actions should be handled inline (no moduleinterface.php -- only works for frontend)
   * @param string Text to append to the end of the id and name of the form
   * @param array Extra parameters to pass along when the form is submitted
   * @param string Text to append to the <form>-statement, for instanse for javascript-validation code
   * @return string
   */
  function CreateFormStart($id, $action='default', $returnid='', $method='post', $enctype='', $inline=false, $idsuffix='', $params = array(), $extra='')
  {
    $this->LoadFormMethods();
    return cms_module_CreateFormStart($this, $id, $action, $returnid, $method, $enctype, $inline, $idsuffix, $params, $extra);
  }

  /**
   * Returns the end of the a module form.  This is basically just a wrapper around </form>, but
   * could be extended later on down the road.  It's here mainly for consistency.
   *
   * @return string
   */
  function CreateFormEnd()
  {
    return '</form>'."\n";
  }

	/**
	 * Returns the xhtml equivalent of an input textbox.  This is basically a nice little wrapper
	 * to make sure that id's are placed in names and also that it's xhtml compliant.
	 *
	 * @param string The id given to the module on execution
	 * @param string The html name of the textbox
	 * @param string The predefined value of the textbox, if any
	 * @param string The number of columns wide the textbox should be displayed
	 * @param string The maximum number of characters that should be allowed to be entered
	 * @param string Any additional text that should be added into the tag when rendered
	 * @return string
	 */
	function CreateInputText($id, $name, $value='', $size='10', $maxlength='255', $addttext='')
	{
		$this->LoadFormMethods();
		return cms_module_CreateInputText($this, $id, $name, $value, $size, $maxlength, $addttext);
	}

        /**
         * Returns the xhtml equivalent of an label for input field.  This is basically a nice little wrapper
         * to make sure that id's are placed in names and also that it's xhtml compliant.
         *
         * @param string The id given to the module on execution
         * @param string The html name of the input field this label is associated to
         * @param string The text in the label
         * @param string Any additional text that should be added into the tag when rendered
	 * @return string
         */
        function CreateLabelForInput($id, $name, $labeltext='', $addttext='')
        {
	        $this->LoadFormMethods();
	        return cms_module_CreateLabelForInput($this, $id, $name, $labeltext, $addttext);
        }

	/**
	 * Returns the xhtml equivalent of an input textbox with label.  This is basically a nice little wrapper
	 * to make sure that id's are placed in names and also that it's xhtml compliant.
	 *
	 * @param string The id given to the module on execution
	 * @param string The html name of the textbox
	 * @param string The predefined value of the textbox, if any
	 * @param string The number of columns wide the textbox should be displayed
	 * @param string The maximum number of characters that should be allowed to be entered
	 * @param string Any additional text that should be added into the tag when rendered
	 * @param string The text for label 
	 * @param string Any additional text that should be added into the tag when rendered
	 * @return string
	 */
	function CreateInputTextWithLabel($id, $name, $value='', $size='10', $maxlength='255', $addttext='', $label='', $labeladdtext='')
	{
		$this->LoadFormMethods();
		return cms_module_CreateInputTextWithLabel($this, $id, $name, $value, $size, $maxlength, $addttext, $label, $labeladdtext);
	}

	/**
	 * Returns the xhtml equivalent of a file-selector field.  This is basically a nice little wrapper
	 * to make sure that id's are placed in names and also that it's xhtml compliant.
	 *
	 * @param string The id given to the module on execution
	 * @param string The html name of the textbox
	 * @param string The MIME-type to be accepted, default is all
	 * @param string The number of columns wide the textbox should be displayed
	 * @param string Any additional text that should be added into the tag when rendered
	 * @return string
	 */
	function CreateInputFile($id, $name, $accept='', $size='10',$addttext='')
	{
		$this->LoadFormMethods();
		return cms_module_CreateInputFile($this, $id, $name, $accept, $size, $addttext);
	}

	/**
	 * Returns the xhtml equivalent of an input password-box.  This is basically a nice little wrapper
	 * to make sure that id's are placed in names and also that it's xhtml compliant.
	 *
	 * @param string The id given to the module on execution
	 * @param string The html name of the textbox
	 * @param string The predefined value of the textbox, if any
	 * @param string The number of columns wide the textbox should be displayed
	 * @param string The maximum number of characters that should be allowed to be entered
	 * @param string Any additional text that should be added into the tag when rendered
	 * @return string
	 */
	function CreateInputPassword($id, $name, $value='', $size='10', $maxlength='255', $addttext='')
	{
		$this->LoadFormMethods();
		return cms_module_CreateInputPassword($this, $id, $name, $value, $size, $maxlength, $addttext);
	}

	/**
	 * Returns the xhtml equivalent of a hidden field.	This is basically a nice little wrapper
	 * to make sure that id's are placed in names and also that it's xhtml compliant.
	 *
	 * @param string The id given to the module on execution
	 * @param string The html name of the hidden field
	 * @param string The predefined value of the field, if any
	 * @param string Any additional text that should be added into the tag when rendered
	 * @return string
	 */
	function CreateInputHidden($id, $name, $value='', $addttext='')
	{
		$this->LoadFormMethods();
		return cms_module_CreateInputHidden($this, $id, $name, $value, $addttext);
	}

	/**
	 * Returns the xhtml equivalent of a checkbox.	This is basically a nice little wrapper
	 * to make sure that id's are placed in names and also that it's xhtml compliant.
	 *
	 * @param string The id given to the module on execution
	 * @param string The html name of the checkbox
	 * @param string The value returned from the input if selected
	 * @param string The current value. If equal to $value the checkbox is selected 
	 * @param string Any additional text that should be added into the tag when rendered
	 * @return string
	 */
	function CreateInputCheckbox($id, $name, $value='', $selectedvalue='', $addttext='')
	{
		$this->LoadFormMethods();
		return cms_module_CreateInputCheckbox($this, $id, $name, $value, $selectedvalue, $addttext);
	}


	/**
	 * Returns the xhtml equivalent of a submit button.	 This is basically a nice little wrapper
	 * to make sure that id's are placed in names and also that it's xhtml compliant.
	 *
	 * @param string The id given to the module on execution
	 * @param string The html name of the button
	 * @param string The predefined value of the button, if any
	 * @param string Any additional text that should be added into the tag when rendered
	 * @param string Use an image instead of a regular button
	 * @return string
	 */
	function CreateInputSubmit($id, $name, $value='', $class='', $image='', $confirmtext='')
	{
		$this->LoadFormMethods();
		return cms_module_CreateInputSubmit($this, $id, $name, $value, $class, $image, $confirmtext);
	}

	/**
	 * Returns the xhtml equivalent of a reset button.	This is basically a nice little wrapper
	 * to make sure that id's are placed in names and also that it's xhtml compliant.
	 *
	 * @param string The id given to the module on execution
	 * @param string The html name of the button
	 * @param string The predefined value of the button, if any
	 * @param string Any additional text that should be added into the tag when rendered
	 * @return string
	 */
	function CreateInputReset($id, $name, $value='Reset', $addttext='')
	{
		$this->LoadFormMethods();
		return cms_module_CreateInputReset($this, $id, $name, $value, $addttext);
	 }

	/**
	 * Returns the xhtml equivalent of a file upload input.	 This is basically a nice little wrapper
	 * to make sure that id's are placed in names and also that it's xhtml compliant.
	 *
	 * @param string The id given to the module on execution
	 * @param string The html name of the input
	 * @param string Any additional text that should be added into the tag when rendered
	 * @return string
	 */
	function CreateFileUploadInput($id, $name, $addttext='',$size='10', $maxlength='255')
	{
		$this->LoadFormMethods();
		return cms_module_CreateFileUploadInput($this, $id, $name, $addttext, $size, $maxlength);
	}


	/**
	 * Returns the xhtml equivalent of a dropdown list.	 This is basically a nice little wrapper
	 * to make sure that id's are placed in names and also that it is xhtml compliant.
	 *
	 * @param string The id given to the module on execution
	 * @param string The html name of the dropdown list
	 * @param string An array of items to put into the dropdown list... they should be $key=>$value pairs
	 * @param string The default selected index of the dropdown list.  Setting to -1 will result in the first choice being selected
	 * @param string The default selected value of the dropdown list.  Setting to '' will result in the first choice being selected
	 * @param string Any additional text that should be added into the tag when rendered
	 * @return string
	 */
	function CreateInputDropdown($id, $name, $items, $selectedindex=-1, $selectedvalue='', $addttext='')
	{
		$this->LoadFormMethods();
		return cms_module_CreateInputDropdown($this, $id, $name, $items, $selectedindex, $selectedvalue, $addttext);
	}

	/**
	 * Returns the xhtml equivalent of a multi-select list.	 This is basically a nice little wrapper
	 * to make sure that id's are placed in names and also that it is xhtml compliant.
	 *
	 * @param string The id given to the module on execution
	 * @param string The html name of the select list
	 * @param string An array of items to put into the list... they should be $key=>$value pairs
	 * @param string An array of items in the list that should default to selected.
	 * @param string The number of rows to be visible in the list (before scrolling).
	 * @param string Any additional text that should be added into the tag when rendered
	 * @param boolean indicates wether multiple selections are allowed (defaults to true)
	 * @return string
	 */
	function CreateInputSelectList($id, $name, $items, $selecteditems=array(), $size=3, $addttext='', $multiple = true)
	{
		$this->LoadFormMethods();
		return cms_module_CreateInputSelectList($this, $id, $name, $items, $selecteditems, $size, $addttext, $multiple);
	}

	/**
	 * Returns the xhtml equivalent of a set of radio buttons.	This is basically a nice little wrapper
	 * to make sure that id's are placed in names and also that it is xhtml compliant.
	 *
	 * @param string The id given to the module on execution
	 * @param string The html name of the radio group
	 * @param string An array of items to create as radio buttons... they should be $key=>$value pairs
	 * @param string The default selected index of the radio group.	 Setting to -1 will result in the first choice being selected
	 * @param string Any additional text that should be added into the tag when rendered
	 * @param string A delimiter to throw between each radio button, e.g., a <br /> tag or something for formatting
	 * @return string
	 */
	function CreateInputRadioGroup($id, $name, $items, $selectedvalue='', $addttext='', $delimiter='')
	{
		$this->LoadFormMethods();
		return cms_module_CreateInputRadioGroup($this, $id, $name, $items, $selectedvalue, $addttext, $delimiter);
	}

	/**
	 * Returns the xhtml equivalent of a textarea.	Also takes WYSIWYG preference into consideration if it's called from the admin side.
	 *
	 * @param bool Should we try to create a WYSIWYG for this textarea?
	 * @param string The id given to the module on execution
	 * @param string The text to display in the textarea's content
	 * @param string The html name of the textarea
	 * @param string The CSS class to associate this textarea to
	 * @param string The html id to give to this textarea
	 * @param string The encoding to use for the content
	 * @param string The text of the stylesheet associated to this content.	 Only used for certain WYSIWYGs
	 * @param string The number of characters wide (columns) the resulting textarea should be
	 * @param string The number of characters high (rows) the resulting textarea should be
	 * @param string The wysiwyg-system to be forced even if the user has chosen another one
	 * @param string The language the content should be syntaxhightlighted as
	 * @return string
	 */
	function CreateTextArea($enablewysiwyg, $id, $text, $name, $classname='', $htmlid='', $encoding='', $stylesheet='', $cols='80', $rows='15',$forcewysiwyg='',$wantedsyntax='',$addtext='')
	{
	  return create_textarea($enablewysiwyg, $text, $id.$name, $classname, $htmlid, $encoding, $stylesheet, $cols, $rows,$forcewysiwyg,$wantedsyntax,$addtext);
	}


	/**
	 * Returns the xhtml equivalent of a textarea.	Also takes Syntax hilighter preference 
         * into consideration if it's called from the admin side.
	 *
	 * @param string The id given to the module on execution
	 * @param string The text to display in the textarea's content
	 * @param string The html name of the textarea
	 * @param string The CSS class to associate this textarea to
	 * @param string The html id to give to this textarea
	 * @param string The encoding to use for the content
	 * @param string The text of the stylesheet associated to this content.	 Only used for certain WYSIWYGs
	 * @param string The number of characters wide (columns) the resulting textarea should be
	 * @param string The number of characters high (rows) the resulting textarea should be
	 * @param string Additional text for the text area tag.
	 * @return string
	 */
	function CreateSyntaxArea($id,$text,$name,$classname='',$htmlid='',$encoding='',
				  $stylesheet='',$cols='80',$rows='15',$addtext='')
	{
	  return create_textarea(false,$text,$id.$name,$classname,$htmlid, $encoding, $stylesheet,
				 $cols,$rows,'','html',$addtext);
	}

	/**
	 * Returns the xhtml equivalent of an href link	 This is basically a nice little wrapper
	 * to make sure that id's are placed in names and also that it's xhtml compliant.
	 *
	 * @param string The id given to the module on execution
	 * @param string The id to eventually return to when the module is finished it's task
	 * @param string The action that this form should do when the link is clicked
	 * @param string The text that will have to be clicked to follow the link
	 * @param string An array of params that should be inlucded in the URL of the link.	 These should be in a $key=>$value format.
	 * @param string Text to display in a javascript warning box.  If they click no, the link is not followed by the browser.
	 * @param boolean A flag to determine if only the href section should be returned
	 * @param boolean A flag to determine if actions should be handled inline (no moduleinterface.php -- only works for frontend)
	 * @param string Any additional text that should be added into the tag when rendered
	 * @return string
	 */
	function CreateFrontendLink( $id, $returnid, $action, $contents='', $params=array(), $warn_message='',
					 $onlyhref=false, $inline=true, $addtext='', $targetcontentonly=false, $prettyurl='' )
	{
	  return $this->CreateLink( $id, $action, $returnid, $contents, $params, $warn_message, $onlyhref,
					$inline, $addtext, $targetcontentonly, $prettyurl );
	}

	/**
	 * Returns the xhtml equivalent of an href link	 This is basically a nice little wrapper
	 * to make sure that id's are placed in names and also that it's xhtml compliant.
	 *
	 * @param string The id given to the module on execution
	 * @param string The action that this form should do when the link is clicked
	 * @param string The id to eventually return to when the module is finished it's task
	 * @param string The text that will have to be clicked to follow the link
	 * @param string An array of params that should be inlucded in the URL of the link.	 These should be in a $key=>$value format.
	 * @param string Text to display in a javascript warning box.  If they click no, the link is not followed by the browser.
	 * @param boolean A flag to determine if only the href section should be returned
	 * @param boolean A flag to determine if actions should be handled inline (no moduleinterface.php -- only works for frontend)
	 * @param string Any additional text that should be added into the tag when rendered
	 * @return string
	 */
	function CreateLink($id, $action, $returnid='', $contents='', $params=array(), $warn_message='', $onlyhref=false, $inline=false, $addttext='', $targetcontentonly=false, $prettyurl='')
	{
		$this->LoadFormMethods();
		return cms_module_CreateLink($this, $id, $action, $returnid, $contents, $params, $warn_message, $onlyhref, $inline, $addttext, $targetcontentonly, $prettyurl);
	}


	/**
	 * Returns a URL to a module action
	 *
	 */
	public function create_url($id,$action,$returnid='',$params=array(),
				   $inline=false,$targetcontentonly=false,$prettyurl='')
	{
	  $this->LoadFormMethods();
	  return cms_module_create_url($this,$id,$action,$returnid,$params,
				       $inline,$targetcontentonly,$prettyurl);
	}

	/**
	 * Return a pretty url string for a module action
	 * 
	 * @abstract
	 * @return string
	 */
	public function get_pretty_url($id,$action,$returnid='',$params=array(),$inline=false)
	{
	  return '';
	}

	/**
	* Returns the xhtml equivalent of an href link for content links.	This is basically a nice
	* little wrapper to make sure that we go back to where we want and that it's xhtml complient
	*
	* @param string the page id of the page we want to direct to
	* @return string
	*/
	function CreateContentLink($pageid, $contents='')
	{
		$this->LoadFormMethods();
		return cms_module_CreateContentLink($this, $pageid, $contents);
	}


	/**
	 * Returns the xhtml equivalent of an href link for Content links.	This is basically a nice little wrapper
	 * to make sure that we go back to where we want to and that it's xhtml compliant.
	 *
	 * @param string The id given to the module on execution
	 * @param string The id to return to when the module is finished it's task
	 * @param string The text that will have to be clicked to follow the link
	 * @param string An array of params that should be inlucded in the URL of the link.	 These should be in a $key=>$value format.
	 * @param boolean A flag to determine if only the href section should be returned
	 * @return string
	 */
	function CreateReturnLink($id, $returnid, $contents='', $params=array(), $onlyhref=false)
	{
		$this->LoadFormMethods();
		return cms_module_CreateReturnLink($this, $id, $returnid, $contents, $params, $onlyhref);
	}


  /**
   * ------------------------------------------------------------------
   * Redirection Methods
   * ------------------------------------------------------------------
   */

	/**
	 * Redirects the user to another action of the module.
	 *
	 * @param string The id given to the module on execution
	 * @param string The action that this form should do when the form is submitted
	 * @param string The id to eventually return to when the module is finished it's task
	 * @param string An array of params that should be inlucded in the URL of the link.	 These should be in a $key=>$value format.
	 * @param boolean A flag to determine if actions should be handled inline (no moduleinterface.php -- only works for frontend)
	 */
	function Redirect($id, $action, $returnid='', $params=array(), $inline=false) {

		// get stuff
		global $gCms;
		$config = $gCms->config;

		// set name
		$name = $this->GetName();

		// Suggestion by Calguy to make sure 2 actions don't get sent
		if(isset($params['action'])) { unset($params['action']); }
		if(isset($params['id'])) { unset($params['id']); }
		if(isset($params['module'])) { unset($params['module']); }

		if (!$inline && $returnid != '') $id = 'cntnt01';

		$text = '';
		if($returnid != '') {
			// gotta get the URL for this page.
			$contentops = $gCms->GetContentOperations();
			$content = $contentops->LoadContentFromId($returnid);
			if(!is_object($content))
			{
				// no destination content object
				return;
			}
			$text .= $content->GetURL();

			$parts = parse_url($text);
			if(isset($parts['query']) && $parts['query'] != '?') {
				$text .= '&';
			} else {
				$text .= '?';
			}		
		}
		else {
			$text .= 'moduleinterface.php?';
		}

		$text .= 'mact='.$name.','.$id.','.$action.','.($inline == true?1:0);
		if($returnid != '') {
			$text .= '&'.$id.'returnid='.$returnid;
		}

		foreach ($params as $key=>$value) {
			$text .= '&'.$id.$key.'='.rawurlencode($value);
		}

		// var_dump($text);
		redirect($text);

	}

	/**
	 * Redirects the user to a content page outside of the module.	The passed around returnid is
	 * frequently used for this so that the user will return back to the page from which they first
	 * entered the module.
	 *
	 * @param string Content id to redirect to.
	 * @return void
	 */
	function RedirectContent($id)
	{
	  redirect_to_alias($id);
	}

	/**
	 * ------------------------------------------------------------------
	 * Intermodule Functions
	 * ------------------------------------------------------------------
	 */

	/**
	 * Get a reference to another module object
	 *
	 * @final
	 * @param string The required module name.
	 * @return object The module object, or FALSE
	 */
	static public function &GetModuleInstance($module)
	{
		global $gCms;

		if (isset($gCms->modules[$module]) &&
			$gCms->modules[$module]['installed'] == true &&
			$gCms->modules[$module]['active'] == true)
		{
			return $gCms->modules[$module]['object'];
		}
		// Fix only variable references should be returned by reference
		$tmp = FALSE;
		return $tmp;
	}

	/**
	 * Returns an array of modulenames with the specified capability
	 * and which are installed and enabled, of course
	 *
	 * @final
	 * @param an id specifying which capability to check for, could be "wysiwyg" etc.
	 * @param associative array further params to get more detailed info about the capabilities. Should be syncronized with other modules of same type
	 * @return array
	 */
	function GetModulesWithCapability($capability, $params=array())
	{
	  global $gCms;
	  $result=array();
	  foreach ($gCms->modules as $module=>$values)
	    {
	      if ($gCms->modules[$module]['installed'] == true &&
		  $gCms->modules[$module]['active'] == true &&
		  $gCms->modules[$module]['object']->HasCapability($capability,$params))
		{
		  $result[]=$module;
		}
	    }
	  return $result;
	}


	/**
	 * ------------------------------------------------------------------
	 * Language Functions
	 * ------------------------------------------------------------------
	 */

	/**
	 * Sets the default language (usually en_US) for the module.  There
	 * should be at least a language file for this language if the Lang()
	 * function is used at all.
	 *
	 * @abstract
	 * @deprecated Modules should use en_US as their default language.
	 */
	function DefaultLanguage()
	{
		return 'en_US';
	}

	/**
	 * Returns the corresponding translated string for the id given.
	 *
	 * @final
	 * @param string Id of the string to lookup and return
	 * @param array Corresponding params for string that require replacement.
	 *		  These params use the vsprintf command and it's style of replacement.
	 * @return string
	 */
	function Lang()
	{
		$this->LoadLangMethods();

		//Push $this onto front of array
		$args = func_get_args();
		array_unshift($args,'');
		$args[0] = &$this;

		return call_user_func_array('cms_module_Lang', $args);
	}

	/**
	 * ------------------------------------------------------------------
	 * User defined tag methods.
	 * ------------------------------------------------------------------
	 */

	/**
	 * Return a list of user defined tags
	 *
	 * @final
	 * @return array
	 */
	function ListUserTags()
	{
		return false;
	}

	/**
	 * Call a specific user defined tag
	 *
	 * @final
	 * @param string Name of the user defined tag
	 * @param array  Parameters for the user defined tag.
	 * @return array
	 */
	function CallUserTag($name, $params = array())
	{
		return false;
	}


	/**
	 * ------------------------------------------------------------------
	 * Tab Functions
	 * ------------------------------------------------------------------
	 */

	/**
	 * Output a string suitable for staring tab headers
	 * i.e:  echo $this->StartTabHeaders();
	 *
	 * @final
	 * @return string
	 */ 
	function StartTabHeaders()
	{
		return '<div id="page_tabs" class="color-bg">';
	}

	/**
	 * Set a specific tab header.
	 * i.e:  echo $this->SetTabHeader('preferences',$this->Lang('preferences'));
	 *
	 * @final
	 * @param string The tab id
	 * @param string The tab title
	 * @param boolean A flag indicating wether this tab is active.
	 * @return string
	 */ 
	function SetTabHeader($tabid,$title,$active=false)
	{
		$a="";
		if (TRUE == $active)
		{
			$a=" class='active'";
			$this->mActiveTab = $tabid;
		}
		$tabid = strtolower(str_replace(' ','_',$tabid));
	  return '<div id="'.$tabid.'"'.$a.'>'.$title.'</div>';
	}

	/**
	 * Output a string to stop the output of headers and close the necessary XHTML div.
	 *
	 * @final
	 * @return string
	 */
	function EndTabHeaders()
	{
		return "</div><!-- EndTabHeaders -->";
	}

	/**
	 * Output a string to indicate the start of XHTML areas for tabs.
	 *
	 * @final
	 * @return string
	 */
	function StartTabContent()
	{
		return '<div class="clearb"></div><div id="page_content">';
	}

	/**
	 * Output a string to indicate the end of XHTML areas for tabs.
	 *
	 * @final
	 * @return string
	 */
	function EndTabContent()
	{
		return '</div> <!-- EndTabContent -->';
	}

	/**
	 * Output a string to indicate the start of the output for a specific tab
	 *
	 * @final
	 * @param string tabid (see SetTabHeader)
	 * @param arrray Parameters
	 * @return string
	 */
	function StartTab($tabid, $params = array())
	{
		if (FALSE == empty($this->mActiveTab) && $tabid == $this->mActiveTab && FALSE == empty($params['tab_message'])) {
			$message = $this->ShowMessage($this->Lang($params['tab_message']));
		} else {
			$message = '';
		}
		return '<div id="' . strtolower(str_replace(' ', '_', $tabid)) . '_c">'.$message;
	}

	/**
	 * Output a string to indicate the end of the output for a specific tab.
	 *
	 * @final
	 * @return string
	 */
	function EndTab()
	{
		return '</div> <!-- EndTab -->';
	}

	/**
	 * ------------------------------------------------------------------
	 * Other Functions
	 * ------------------------------------------------------------------
	 */

	/**
	 *
	 * Called in the admin theme for every installed module, this method allows
	 * the module to output style information for use in the admin theme.
	 *
	 * @abstract
	 * @returns string css text.
	 */
	function AdminStyle()
	{
	  return '';
	}

	/**
	 * Set the content-type header.
	 *
	 * @abstract
	 * @param string Value to set the content-type header too
	 * @return string valid content type string.
	 */
	function SetContentType($contenttype)
	{
		$variables = &$this->cms->variables;
		$variables['content-type'] = $contenttype;
	}

	/**
	 * Put an event into the audit (admin) log.	 This should be
	 * done on most admin events for consistency.
	 *
	 * @final
	 * @param string item id, useful for working on a specific record (i.e article or user)
	 * @param string item name
	 * @param string action name
	 * @return void
	 */
	function Audit($itemid, $itemname, $action)
	{
		#$userid = get_userid();
		#$username = $_SESSION["cms_admin_username"];
		audit($itemid,$itemname,$action);
	}

	/**
	 * ShowMessage
	 * Returns a formatted page status message
	 *
	 * @final
	 * @param message - Message to be shown
	 * @return string
	 */
	function ShowMessage($message, $type = false)
	{
	  global $gCms;
	  if (isset($gCms->variables['admintheme']))
	    {
	      $admintheme =& $gCms->variables['admintheme']; //php4 friendly
	      return $admintheme->ShowMessage($message, $type);
	    }
	  return '';
	}

	/**
	 * ShowErrors
	 * Outputs errors in a nice error box with a troubleshooting link to the wiki
	 *
	 * @final
	 * @param errors - array or string of errors to be shown
	 * @return string
	 */
	function ShowErrors($errors)
	{
		global $gCms;
		
		if(isset($gCms->variables['admintheme'])) {
			$admintheme =& $gCms->variables['admintheme']; //php4 friendly			
			return $admintheme->ShowErrors($errors);
		}

		return '';
	}


	/**
	 * ------------------------------------------------------------------
	 * Permission Functions
	 * ------------------------------------------------------------------
	 */


	/**
	 * Create's a new permission for use by the module.
	 *
	 * @final
	 * @param string Name of the permission to create
	 * @param string Description of the permission
	 * @return void
	 */
	function CreatePermission($permission_name, $permission_text)
	{
		global $gCms;
		$db =& $gCms->GetDB();

		// set the table
		$table = cms_db_prefix()."permissions";

		// check if permision exists
		$query = "SELECT permission_id FROM $table WHERE permission_name = ?";
		$count = $db->GetOne($query, array($permission_name));

		if(intval($count) == 0) {

			// wx/vcms we change permision table to use AUTO_INCREMENT
			
			// time
			$time = $db->DBTimeStamp(time());

			// query
			$query = "INSERT INTO $table
			(permission_name, permission_text, create_date, modified_date)
			VALUES ('$permission_name', '$permission_text', $time, $time)";

			// execute
			$db->Execute($query);

		}

	}

	/**
	 * Checks a permission against the currently logged in user.
	 *
	 * @final
	 * @param string The name of the permission to check against the current user
	 * @return boolean
	 */
	function CheckPermission($permission_name)
	{
		$userid = get_userid(false);
		return check_permission($userid, $permission_name);
	}

	/**
	 * Removes a permission from the system.  If recreated, the
	 * permission would have to be set to all groups again.
	 *
	 * @final
	 * @param string The name of the permission to remove
	 * @return void
	 */
	function RemovePermission($permission_name)
	{
	  cms_mapi_remove_permission($permission_name);
	}

	/**
	 * ------------------------------------------------------------------
	 * Preference Functions
	 * ------------------------------------------------------------------
	 */

	/**
	 * Returns a module preference if it exists.
	 *
	 * @final
	 * @param string The name of the preference to check
	 * @param string The default value, just in case it doesn't exist
	 * @return string
	 */
	function GetPreference($preference_name, $defaultvalue='')
	{
		return get_site_preference($this->GetName() . "_mapi_pref_" . $preference_name, $defaultvalue);
	}

	/**
	 * Sets a module preference.
	 *
	 * @final
	 * @param string The name of the preference to set
	 * @param string The value to set it to
	 * @return boolean
	 */
	function SetPreference($preference_name, $value)
	{
	  return set_site_preference($this->GetName() . "_mapi_pref_" . $preference_name, $value);
	}

	/**
	 * Removes a module preference.  If no preference name
	 * is specified, removes all module preferences.
	 *
	 * @final
	 * @param string The name of the preference to remove.  If empty all preferences associated with the module are removed.
	 * @return boolean
	 */
	function RemovePreference($preference_name='')
	{
	  if( $preference_name == '' )
	    {
	      return remove_site_preference($this->GetName()."_mapi_pref_",true);
	    }
	  return remove_site_preference($this->GetName() . "_mapi_pref_" . $preference_name);
	}

	/* ------------------------- *\
	 
	 * wx/vcms update

	\* ------------------------- */

	/**
	 * Info:
	 * 
	 * @param $fields => list of current level input fields
	 * @param $output => what to output
	 * @return array of keys or fields
	 */
	function get_level_fields($level, $fields, $output = false) {
		
		if($output && $output == "keys") {
			// output just the keys in an array
			$keys = array();
			foreach ($fields as $key => $value) {
				$keys[] = $key;
			}
			return $keys;
		} else if($output && $output == "text_input_keys") {
			// output keys only for text input fields
			$keys = array();
			foreach ($fields as $key => $value) {
				if(isset($value["type"]) && $value["type"] == "input" && $key != "item_order") {
					$keys[] = $key;
				}
			}
			return $keys;
		} else if($output && $output == "gallery") {
			// we check if gallery is in the field list
			$galleries = array();
			foreach ($fields as $key => $value) {
				if(isset($value["type"]) && $value["type"] == "gallery") {
					$galleries[$key] = APP_PATH . '/' . APP_UPLOADS . $value["options"]["folder"];
				}
			}
			if(count($galleries) > 0) {
				return $galleries;
			}
			// no gallery found
			return false;
		} else {
			return array(
				"name" => $level,
				"prefix" => $this->getLevelPrefix($level),
				"table" => $this->getTableName($level),
				"fields" => $fields
			);
		}

		return false;

	}

	/**
	 * Info: Meta fields are basicly the same for every module
	 * so i created this function to set them up globaly
	 * 
	 * @param $show => true or false
	 * @param $params => array with params
	 * @return array of fields or false
	 */
	function get_level_fields_meta($show = false, $params) {

		// get cms stuff
		global $gCms;

		// check if we have meta
		// default is false
		if($show) {

			// set meta options
			$meta = array(

				"meta" => array(
					// check google meta = default true
					"google" =>
						isset($params["google"]) ? $params["google"] : true,
					// check facebook meta = default true
					"facebook" =>
						isset($params["facebook"]) ? $params["facebook"] : true,
					// check for default image input name
					"default_image" =>
						isset($params["default_image"]) ? $params["default_image"] : false,
					// check for parent accesskey
					"parent_accesskey" =>
						isset($params["parent_accesskey"]) ? $params["parent_accesskey"] : false
				)

			);

			// default meta fields
			$meta_default = array(

				"meta_title" => array(
					"type" => "input",
					"size" => 50,
					"length" => 255
				),

				"meta_description" => array(
					"type" => "text"
				),

				"meta_keywords" => array(
					"type" => "input",
					"size" => 50,
					"length" => 255
				),

				"facebook_title" => array(
					"type" => "input",
					"size" => 50,
					"length" => 255
				),

				"facebook_description" => array(
					"type" => "text"
				)

			);

			if(isset($gCms->config["parents"]) && $gCms->config["parents"]) {
				// run through tags and add language
				foreach ($gCms->config["parents"] as $lang) {
					foreach ($meta_default as $meta_tag => $meta_input_info) {
						$meta[$meta_tag . "_" . $lang] = $meta_input_info;
					}
				}
			} else {
				// merge with default fields
				$meta = array_merge($meta, $meta_default);
			}

			return $meta;

		} else {

			return false;

		}

	}

	/**
	 * Info:
	 * 
	 * @param $level => current level
	 * @param $params => post params
	 * @param $item => current item
	 * @return array with all the values
	 */
	function setupPostValues($level, $params, $item) {

		// database
		$db =& $this->GetDb();

		// set values
		$level_name   = $level["name"];
		$level_prefix = $level["prefix"];
		$level_table  = $level["table"];
		$level_fields = $level["fields"];

		// wx set up vars
		$query_fields = "";
		$values = array();
		$alias_list = array();
		$item_errors = array();

		// check if __what set
		if(!isset($item->__what)) $item->__what = $level_name;

		// wx setup post data get all $params
		// setup query list
		foreach ($level_fields as $i_name => $i_value) {
			
			// skip elements that are not in the post list
			// or skip elements with skip_post set to true
			if($i_value === false
			|| isset($i_value["skip_post"]) && $i_value["skip_post"]) {
				continue;
			}

			// collect all aliases in an array
			if($i_name == "alias" || substr($i_name, 0, 6) === "alias_") {
				$alias_list[] = $i_name;
			}

			// get all $params and setup $item
			if(isset($params[$level_prefix . $i_name])) {
				$item->$i_name = $params[$level_prefix . $i_name];
			}

			// date
			if(isset($i_value["type"]) && $i_value["type"] == "date") {

				// check if all fields ar set
				if(isset($params[$level_prefix . $i_name . "_day"])
				&& isset($params[$level_prefix . $i_name . "_month"])
				&& isset($params[$level_prefix . $i_name . "_year"])) {

					// check if time is also set, if not set it to 00:00:00
					$hour   = isset($params[$level_prefix . $i_name . "_hour"])
							? $params[$level_prefix . $i_name . "_hour"] : 00;
					$minute = $params[$level_prefix . $i_name . "_minute"]
							? $params[$level_prefix . $i_name . "_minute"] : 00;;
					$second = $params[$level_prefix . $i_name . "_second"]
							? $params[$level_prefix . $i_name . "_second"] : 00;;

					// mk time
					$item->$i_name =
					mktime(
						$hour, $minute, $second,
						$params[$level_prefix . $i_name . "_month"],
						$params[$level_prefix . $i_name . "_day"],
						$params[$level_prefix . $i_name . "_year"]
					);

					// update to db timestamp
					$item->$i_name =
					str_replace("'","",$db->DBTimeStamp($item->$i_name));

				}

			}

			// check for checkbox and update status if it's in post
			if(isset($i_value["type"]) && $i_value["type"] == "checkbox") {
				$item->$i_name = 0;
				if(isset($params[$level_prefix . $i_name])) {
					$item->$i_name = 1;
				}
			}

			// if type json
			if(isset($i_value["type"]) && $i_value["type"] == "json") {
				$item->$i_name = json_encode($params[$level_prefix . $i_name]);
			}

			// setup query fields except for item_order its set up manualy bellow
			if($i_name != "item_order"
			&& $i_value["type"] != "gallery"
			&& $i_value["type"] != "facebook"
			&& $i_value["type"] != "google"
			&& $i_name != "meta") {
				
				// to be added in the query
				$query_fields .= $i_name . "=?," . "\n";

				// add to the query value array
				if($item->$i_name == '') {
					$values[$i_name] = NULL;
				} else {
					if($i_value["type"] == "input") {
						$values[$i_name] = addslashes($item->$i_name);
					} else {
						$values[$i_name] = $item->$i_name;
					}
				}

			}

			// TODO --> vai mums šo vajag
			// --> jāčeko alias/url veidošana
			// we need to check name if validate is set
			// name by default can't be empty
			// if($i_name == "name") {
			// 	if(!isset($i_value["validate"])) {
			// 		$i_value["validate"] = array(
			// 			"not_empty" => true,
			// 			"value" => "name"
			// 		);
			// 	} else {
			// 		$i_value["validate"]["not_empty"] = true;
			// 		if(!isset($i_value["validate"]["value"])) {
			// 			$i_value["validate"]["value"] = "name";
			// 		}
			// 	}
			// }

			// validation
			if(isset($i_value["validate"])) {
				
				$validate = $i_value["validate"];

				// get the values
				$validate_empty = isset($validate["not_empty"]) ? $validate["not_empty"] : false;
				$validate_value = isset($validate["value"]) ? true : false;

				// check if label set if not then set it
				if(!isset($i_value['label'])) {
					$i_value['label'] = $this->get_lang_param(
						$i_name,
						$level_name
					);
				}

				// check if we have the value
				if($validate_empty && $item->$i_name == "") {
					$item_errors[$i_name] = $this->get_lang_param(
						"error_empty",
						$level_name,
						array($i_value['label'])
					);
				}

				// check if value valid
				if($validate_value && $item->$i_name != "") {
					// if float replace , with .
					if($validate["value"] == "float") {
						$item->$i_name = str_replace(",", ".", $item->$i_name);
					}
					// validate
					if(!validate_string($item->$i_name, $validate["value"])) {
						$item_errors[$i_name] = $this->get_lang_param(
							"error_not_valid_" . $validate["value"],
							$level_name,
							array($i_value['label'])
						);
					}
				}

			}

			// check for auto increment of alias
			if($i_name == "alias"
			&& isset($i_value["autoincrement_alias"])) {
				$autoincrementalias = $i_value["autoincrement_alias"];
			}

		}

		// TODO - jāpārveido šis - jāaizvāc tas getpreferance

		// check if we auto increase alias if identical
		$autoincrementalias = isset($autoincrementalias) ? $autoincrementalias : $this->GetPreference("autoincrement_alias", false);

		// alias error array
		$alias_errors = array();

		// set up all the aliases
		foreach ($alias_list as $alias_param) {
			
			// setup the name field
			if($alias_param == "alias") {
				$name_field = "name";
			} else {
				$name_field = explode("_", $alias_param);
				$name_field = "name_" . $name_field[1];
			}
			
			// check the post for alias and create from name if empty or not set
			if(isset($params[$level_prefix . $alias_param])) {
				
				// create alias
				$item->$alias_param = $this->plcreatealias(trim($params[$level_prefix . $alias_param]));

				// check if empty and we have our language name field set
				if($item->$alias_param == "" && isset($item->$name_field)) {
					$item->$alias_param = $this->plcreatealias(strip_tags($item->$name_field));
				}

			} else {

				// create from name
				$item->$alias_param = $this->plcreatealias(strip_tags($item->$name_field));

			}

			// check if alias exists
			if($item->$alias_param != "" && false == $this->checkalias("module_" . $level_table, $item->$alias_param, isset($params[$level_prefix . "id"]) ? $params[$level_prefix . "id"] : false, "id", $alias_param)) {
				// check if we auto increse alias
				if($autoincrementalias) {
					$alias_i = 1;
					$alias_base = $item->$alias_param;
					$alias_temp = $item->$alias_param . "-" . $alias_i;
					while(!$this->checkalias("module_" . $level_table, $alias_temp, isset($params[$level_prefix . "id"]) ? $params[$level_prefix . "id"] : false, "id", $alias_param)) {
						$alias_temp = $alias_base . "-" . $alias_i;
						$alias_i++;
					}
					$item->$alias_param = $alias_temp;
				} else {
					// else set in error array
					// $alias_errors[] = $alias_param;
					$item_errors[$alias_param] = lang('error_unique',
						$this->get_lang_param($alias_param, $level_name)
					);

				}
			}
			
			// add to the values
			$values[$alias_param] = $item->$alias_param;

		}

		// add oldparent and old item order to the item if they are set
		if(isset($params["item_oldparent"])) $item->oldparent = $params["item_oldparent"];
		if(isset($params["item_old_item_order"])) $item->old_item_order = $params["item_old_item_order"];

		return array(
			"query_fields"	=> $query_fields,
			"query_values"	=> $values,
			"alias_list"	=> $alias_list,
			"alias_errors"	=> $alias_errors,
			"item"			=> $item,
			"item_errors"	=> count($item_errors) > 0 ? $item_errors : false
		);

	}

	/**
	 * Info:
	 * 
	 * @param 
	 * @return link
	 */
	function createExportButtons($current) {

		// get the module
		$module = 'module_' . $this->getName();

		// get the levels
		$levels = $this->get_levelarray();

		// setup tablaes
		$tables = '';
		foreach ($levels as $key => $level) {
			$tables .= ( $key > 0 ? ',' : '' ) . 'module_' . $this->getTableName($level);
		}

		// get the plural name of the current level
		$level_name = $this->lang($current . "_plural");

		// setup current module
		$current = 'module_' . $this->getTableName($current);

		return
		'<a class="color-one-text" href="export/index.php?what='. $module . '&tables=' . $tables . '">' .
		'Export All (SQL)</a> | ' .
		'<a class="color-one-text" href="export/index.php?what='. $current .'&tables=' . $current . '">' .
		'Export ' . $level_name . ' (SQL)</a>';

	}

	/**
	 * ------------------------------------
	 * Success message
	 * @param $level string - current level name
	 * @param $param string - name of the lang param
	 * @param $name string - name of current item editing or adding
	 * ------------------------------------
	 */

	function get_lang_param($param, $level, $replace = array()) {

		// set the parameters
		$lang_param_default = $param;
		$lang_param_module = $level . '_' . $param;

		// get default message from module
		$lang_message = $this->Lang($lang_param_module, $replace);

		// check if it's set in module file
		if(substr($lang_message, 0, 6) == "-- Add") {
			// not found - use default admin lang file in /admin/lang
			if(substr(lang($lang_param_default), 0, 6) != "-- Add") {
				$lang_message = lang($lang_param_default, $replace);
			}
		}

		// return the message
		return $lang_message;

	}

	/**
	 * ------------------------------------
	 * Get parent list for this level
	 * @param $level string - current level name
	 * @param $order string - what order to use
	 * ------------------------------------
	 */

	function get_parent_list($level, $order = false) {

		// find parent level name
		$hierarchy = $this->get_modulehierarchy();
		$parent = $hierarchy[1];

		// if parent level cancel everything
		if($level == $parent) {
			return false;
		}

		// get db and set the table name
		$db = $this->GetDb();
		$parent_table = cms_db_prefix()."module_".$this->GetName()."_".$parent;

		// setup query
		$query = "SELECT id as value, name FROM $parent_table WHERE active = 1 ORDER BY";
		$query .= $order ? " $order" : " name ASC";

		// get all the options
		$dbresult = $db->Execute($query);
		$options = array();
		while ($dbresult && $row = $dbresult->FetchRow()){
			$options[] = $row;
		}
		
		// return array
		return $options;

	}

	// -------------------------
	//  >> wx
	//  >> update item order
	// -------------------------
	function updateItemOrder($level, $table, $item) {

		// get db
		$db =& $this->GetDb();

		// set module
		$current_module = cms_db_prefix()."module_" . $table;

		// check current item count
		$item_count = $this->countsomething($level, "id", array(), isset($item->parent) ? "parent = $item->parent" : false);

		// ---------------
		//  Do the update
		// ---------------

		if((!isset($item->item_order) || $item->item_order == "")
		&& !isset($item->old_item_order)) {

			// ---
			//  1
			// ---

			// new item and order is not set in the $_POST

			// check the settings if new items apear on the list first
			if($this->newItemsFirst($level)) {

				// update all items -> order + 1
				$query = "UPDATE $current_module SET item_order=(item_order+1)";
				$query .= isset($item->parent) ? " WHERE parent = ?" : "";
				$db->Execute($query, isset($item->parent) ? array($item->parent) : false);

				// set item order
				$item->item_order = 1;

			} else {

				// item is last
				$item->item_order = $item_count + 1;

			}

		} else {

			// ---
			//  2
			// ---

			// item order has been set in the $_POST

			// -------
			//  Setup
			// -------

			// validate item order
			$item->item_order = floor($item->item_order);

			// set item to be last if user has entered larger number then count
			// and set the item to first if 0 or bellow
			if($item->item_order > $item_count) {
				if($item_count == 0) {
					$item->item_order = 1; // if no items
				} else {
					// check if parent changed
					if(isset($item->oldparent) && $item->oldparent != $item->parent) {
						$item->item_order = $item_count + 1;
					} else {
						// we don't need + 1 if we are not creating a new item
						$item->item_order = isset($item->old_item_order) ? $item_count : $item_count + 1;
					}
				}
			} else if($item->item_order <= 0) {
				// check if parent changed
				if(isset($item->oldparent) && $item->oldparent != $item->parent) {
					$item->item_order = $item_count + 1;
				} else {
					// if not a new item then use the OG item_order
					$item->item_order = isset($item->old_item_order) ? $item->old_item_order : 1;
				}
			}

			// -------
			//  Logic
			// -------

			// check if item order has been changed
			// but also need to check if parent don't change
			if((isset($item->old_item_order) && $item->old_item_order != $item->item_order)
			&& ((isset($item->oldparent) && $item->oldparent == $item->parent) || !isset($item->parent))) {

				// ---------------------------------
				// item order has changed
				// 1) item parent has not changed or
				// 2) there is no parent
				// ---------------------------------

				// all items that are bellow the old one go up
				$query = "UPDATE $current_module SET item_order=(item_order-1) WHERE item_order > ? AND id != ?";
				$query .= isset($item->parent) ? " AND parent = ?" : "";
				$values = array($item->old_item_order, $item->id);
				if(isset($item->parent)) $values[] = $item->parent;
				$db->Execute($query, $values);

				// all items that are bellow the old one go up
				$query = "UPDATE $current_module SET item_order=(item_order+1) WHERE item_order >= ? AND id != ?";
				$query .= isset($item->parent) ? " AND parent = ?" : "";
				$values = array($item->item_order, $item->id);
				if(isset($item->parent)) $values[] = $item->parent;
				$db->Execute($query, $values);

			} else if((isset($item->oldparent) && $item->oldparent != $item->parent)
			&& isset($item->old_item_order)) {

				// -----------------------
				// item parent has changed
				// -----------------------

				// update the order of the items with the old parent
				$query = "UPDATE $current_module SET item_order=(item_order-1) WHERE item_order > ? AND parent = ?";
				$db->Execute($query, array($item->old_item_order, $item->oldparent));

				// check if item goes to end or to a specific order
				// if to specific order then we need to update order for the new parent
				if($item->item_order <= $item_count) {
					$query = "UPDATE $current_module SET item_order=(item_order+1) WHERE item_order >= ? AND parent = ?";
					$db->Execute($query, array($item->item_order, $item->parent));
				}

			} else if(!isset($item->old_item_order)) {

				// -------------------------
				// new item with a order set
				// -------------------------

				// reorder items if new item order is less then total item count
				if($item->item_order <= $item_count) {

					// all items that are bellow current item order need to go + 1
					$query = "UPDATE $current_module SET item_order=(item_order+1) WHERE item_order >= ?";
					$query .= isset($item->parent) ? " AND parent = ?" : "";
					$values = array($item->item_order);
					if(isset($item->parent)) $values[] = $item->parent;
					$db->Execute($query, $values);

				}

				// else {} all good do nothing

			}

		}

		// return item order
		return $item->item_order;

	}

	function get_module_message($params) {

		// default
		$message = false;

		// check for module message in url
		if(FALSE == empty($params["message"])) {

			// check for id
			if(FALSE == empty($params["item_id"])) {

				// what level we are operating in?
				$level_get_function = "get_level_" . $params["active_tab"];

				// get the item
				$items = $this->$level_get_function(array("id" => $params["item_id"]));
				$item = $items[0];

				// set the message
				$message = $this->get_lang_param(
					$params["message"],
					$params["active_tab"],
					array(clean_up_string($item->name))
				);

			}
			

		}

		// retrun message
		return $message;

	}

	function add_button($id, $level) {

		// create the button
		return $this->CreateLink(
			$id,
			"edit" . $this->getLevelPrefix($level),
			0, // returnid,
			$this->Lang("add_" . $level),
			array(
				"class" => "button  color-ok-bg"
			)
		);

	}

	/**
	 * This is a backup for older projects that don't have
	 * this function in the module file in the project folder
	 * so we use old get prefs function
	*/
	function newItemsFirst($level) {
		return $this->GetPreference("newitemsfirst_" . $level, false);
	}

	/**
	 * ------------------------------------
	 * Delete upload dirs for galleries and other files
	 * @param $itemid int - current item id
	 * @param $level string - current level name
	 * ------------------------------------
	 */

	function delete_upload_directories($itemid, $level) {

		// get db
		$db = $this->GetDb();

		// set the fields function to get galleries
		$level_fileds_function = "get_level_" . $level . "_fields";

		if($delete_galleries = $this->$level_fileds_function("gallery")) {

			foreach ($delete_galleries as $delete_gallery_id => $delete_gallery_folder) {
				
				// image database
				$imagetable = cms_db_prefix() . "images";
				
				// add item id to the path
				$delete_item_gallery_folder = $delete_gallery_folder . $itemid . "/";

				// delete if it's there
				if(is_dir($delete_item_gallery_folder) === true) {
					// check if there are files in the folder
					if(count(glob($delete_item_gallery_folder."*")) > 0) {
			        	// cycle true the files and delete one by on
			    		foreach(glob($delete_item_gallery_folder."*") as $filename) {
			    			unlink($filename);
			    		}
					}
				    rmdir($delete_item_gallery_folder);
				}

				// set the parent for db delete
				$imageparent = $this->GetName() . "_"
							 . $level . "_"
							 . $delete_gallery_id
							 . "_id" . $itemid;

				// delete
				$query = "DELETE FROM $imagetable WHERE parent = ?";
				$db->Execute($query, array($imageparent));

			}

		}

	}

	/**
	 * ------------------------------------
	 * Add Meta params to module param array
	 * @param $fields - current list of fields
	 * @param $params - extra stuff we need
	 * @return array of params
	 * ------------------------------------
	 */

	function get_meta_params($fields, $page_accesskey, $params) {

		// set the vars
		$tab = $params["tab"];
		$lang = $params["lang"];
		$level = $params["level"];
		$name_param = $params["name"];
		$alias_param = $params["alias"];
		$lang_add = isset($params["lang_add"]) ? $params["lang_add"] : false;

		// set lang param if not default language
		$lang_param = '';
		if($lang != Config::read("lang_default") || $lang_add) {
			$lang_param  = '_' . $lang;
		}

		// get the pages we need
		$content = FrontendContent::get_pages();

		// check if language set in pages else use default
		if(isset($content->$lang)) {
			$page_defaults = $content->$lang;
			$pages = $content->$lang->children;
		}

		// set page alias
		$page_alias = '';
		if(isset($pages->$page_accesskey)) {
			$page_alias = "/" . $pages->$page_accesskey->alias;
		}

		// create meta array
		$meta = array(

			"meta_title$lang_param" => array(
				"type"		=> "input",
				"size"		=> 50,
				"length"	=> 255,
				"label"		=> lang('meta_title') . ( $lang_param != '' ? ' ('.strtoupper($lang).')' : '' ),
				// validate
				"validate"	=> array(
					"not_empty"	=> false,
					"value"		=> "title"
				),
				// position
				"col" => $params["meta_google"]["col"],
				"tab" => $params["meta_google"]["tab"]
			),

			"meta_description$lang_param" => array(
				"type"		=> "textarea",
				"height"	=> 76,
				"label"		=> lang('meta_description') . ( $lang_param != '' ? ' ('.strtoupper($lang).')' : '' ),
				// validate
				"validate"	=> array(
					"not_empty"	=> false,
					"value"		=> "title"
				),
				// position
				"col" => $params["meta_google"]["col"],
				"tab" => $params["meta_google"]["tab"]
			),

			"meta_keywords$lang_param" => array(
				"type"		=> "textarea",
				"height"	=> 76,
				"label"		=> lang('meta_keywords') . ( $lang_param != '' ? ' ('.strtoupper($lang).')' : '' ),
				// validate
				"validate"	=> array(
					"not_empty"	=> false,
					"value"		=> "title"
				),
				// position
				"col" => $params["meta_google"]["col"],
				"tab" => $params["meta_google"]["tab"]
			),

			"meta_google$lang_param" => array(
				"type" => "google",
				"data" => array(
					'site-name' 		=> isset($page_defaults->site_name) ? $page_defaults->site_name : Config::read('site_name'),
					'seperator' 		=> "|",
					'url' 				=> Config::read("production_www") . $page_alias . "/",
					'default-title' 	=> "m1_" . $this->getLevelPrefix($level) . $name_param,
					'meta-title' 		=> "m1_" . $this->getLevelPrefix($level) . "meta_title$lang_param",
					'meta-description' 	=> "m1_" . $this->getLevelPrefix($level) . "meta_description$lang_param",
					'alias' 			=> "m1_" . $this->getLevelPrefix($level) . $alias_param
				),
				"col" => $params["meta_google"]["col"],
				"tab" => $params["meta_google"]["tab"]
			),

			"facebook_title$lang_param" => array(
				"type"		=> "input",
				"size"		=> 50,
				"length"	=> 255,
				"label"		=> lang('facebook_title') . ( $lang_param != '' ? ' ('.strtoupper($lang).')' : '' ),
				// validate
				"validate"	=> array(
					"not_empty"	=> false,
					"value"		=> "title"
				),
				// position
				"col" => $params["meta_facebook"]["col"],
				"tab" => $params["meta_facebook"]["tab"]
			),

			"facebook_description$lang_param" => array(
				"type"		=> "textarea",
				"height"	=> 76,
				"label"		=> lang('facebook_description') . ( $lang_param != '' ? ' ('.strtoupper($lang).')' : '' ),
				// validate
				"validate"	=> array(
					"not_empty"	=> false,
					"value"		=> "title"
				),
				// position
				"col" => $params["meta_facebook"]["col"],
				"tab" => $params["meta_facebook"]["tab"]
			),

			"facebook_image$lang_param" => array(
				"type"		=> "image",
				"folder"	=> "projects",
				"label"		=> lang('facebook_image')
			),

			"meta_facebook$lang_param" => array(
				"type" => "facebook",
				"lang" => $lang_param,
				"data" => array(
					'site-name' 			=> isset($page_defaults->site_name) ? $page_defaults->site_name : Config::read('site_name'),
					'seperator' 			=> "|",
					'url' 					=> Config::read("production_www"),
					'default-title' 		=> "m1_" . $this->getLevelPrefix($level) . $name_param,
					'meta-title' 			=> "m1_" . $this->getLevelPrefix($level) . "meta_title$lang_param",
					'facebook-title' 		=> "m1_" . $this->getLevelPrefix($level) . "facebook_title$lang_param",
					'facebook-description'	=> "m1_" . $this->getLevelPrefix($level) . "facebook_description$lang_param",
					'facebook-image'		=> "m1_" . $this->getLevelPrefix($level) . "facebook_image$lang_param"
				),
				"col" => $params["meta_facebook"]["col"],
				"tab" => $params["meta_facebook"]["tab"]
			),

		);

		// set default image for facebook if we have one
		if(isset($params["default_image"])) {
			$meta["meta_facebook$lang_param"]["data"]["default-image"] =
				"m1_" . $this->getLevelPrefix($level) . $params["default_image"];
		}

		// return merged arrays
		return array_merge($fields, $meta);

	}

}


# vim:ts=4 sw=4 noet
?>
