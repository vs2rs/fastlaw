<?php // -*- mode:php; tab-width:4; indent-tabs-mode:t; c-basic-offset:4; -*-
#CMS - CMS Made Simple
#(c)2004-2010 by Ted Kulp (ted@cmsmadesimple.org)
#This project's homepage is: http://cmsmadesimple.org
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#$Id: Lang.inc.php 6845 2011-01-09 18:27:28Z calguy1000 $

/**
 * Define the link content type
 * @package CMS
 * @license GPL
 */

/**
 * Class for CMS Made Simple link content type
 *
 * @package CMS
 * @version $Revision$
 * @license GPL
 */
class ModulePage extends ContentBase
{

    function IsCopyable()
    {
        return TRUE;
    }

    function IsViewable()
    {
      return FALSE;
    }

    function FriendlyName()
    {
      return lang('module');
    }

    function SetProperties()
    {
      parent::SetProperties();
      $this->AddBaseProperty('template',4);

      // name of the module
      $this->AddContentProperty('module_name',10,1);

      // content we show from the module
      $this->AddBaseProperty('content_en',10,1);

      // do we have children for this content
      $this->AddContentProperty('children',10,1);


      $this->RemoveProperty('secure',0);
      $this->RemoveProperty('cachable',true);
    }

    function FillParams($params,$editing = false)
    {
		parent::FillParams($params,$editing);

		if (isset($params)) {
			$parameters = array('template','module_name','content_en','children');

			//pick up the template id before we do parameters
			if (isset($params['template_id'])) {
				if ($this->mTemplateId != $params['template_id']) {
					$this->_contentBlocksLoaded = false;
				}
				$this->mTemplateId = $params['template_id'];
			}

			foreach ($parameters as $oneparam) {
				if (isset($params[$oneparam])) {
					$this->SetPropertyValue($oneparam, $params[$oneparam]);
				}
			}
		}
    }

    function ValidateData()
    {
      $errors = parent::ValidateData();
      if( $errors === FALSE )
	{
	  $errors = array();
	}

 //      if ($this->GetPropertyValue('lang_code') == '')
	// {
	//   $errors[]= lang('nofieldgiven',array(lang('language_code')));
	//   $result = false;
	// }

      return count($errors) > 0 ? $errors : FALSE;
    }

    function TabNames()
    {
      $res = array(lang('main'));
      if( check_permission(get_userid(),'pages_edit') )
	{
	  $res[] = lang('options');
	}
    // wx add meta tab
    $res[] = "Meta";
      return $res;
    }

    function display_single_element($one,$adding) {

    	global $gCms;

		switch($one) {

			case 'template': {
				$templateops = $gCms->GetTemplateOperations();
				return array(lang('template').':', $templateops->TemplateDropdown('template_id', $this->mTemplateId, 'onchange="document.Edit_Content.submit()"'));
				break;
			}
		
			case 'module_name': {
				return array(
				lang('module') . ':',
				'<input type="text" name="module_name" size="80" value="'.cms_htmlentities($this->GetPropertyValue('module_name')).'" />'
				);
				break;
			}
			
			case 'content_en': {
				return array(
				lang('content') . ':',
				'<input type="text" name="content_en" size="80" value="'.cms_htmlentities($this->GetPropertyValue('content_en')).'" />'
				);
				break;
			}
			
			case 'children': {
				return array(
				lang('children') . ':',
				'<input type="text" name="children" size="80" value="'.cms_htmlentities($this->GetPropertyValue('children')).'" />'
				);
				break;
			}

			default:
				return parent::display_single_element($one,$adding);

		}

    }

    function EditAsArray($adding = false, $tab = 0, $showadmin = false)
    {
      global $gCms;

      switch($tab)
	{
	case '0':
	  return $this->display_attributes($adding);
	  break;
	case '1':
	  return $this->display_attributes($adding,1);
	  break;
	}
    }

    function GetURL($rewrite = true)
    {
	return ''; //cms_htmlentities($this->GetPropertyValue('lang_code'));
    }
}

# vim:ts=4 sw=4 noet
?>
