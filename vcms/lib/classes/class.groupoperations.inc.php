<?php // -*- mode:php; tab-width:4; indent-tabs-mode:t; c-basic-offset:4; -*-
#CMS - CMS Made Simple
#(c)2004-2010 by Ted Kulp (ted@cmsmadesimple.org)
#This project's homepage is: http://cmsmadesimple.org
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#$Id$

/**
 * Group related functions
 * @package CMS 
 * @license GPL
 */

/**
 * Include group class definition
 */
include_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'class.group.inc.php');

/**
 * Class for doing group related functions.  Maybe of the Group object functions are just wrappers around these.
 *
 * @since 0.6
 * @package CMS
 * @version $Revision$
 * @license GPL
 */
class GroupOperations
{
	/**
	 * Loads all the groups from the database and returns them
	 *
	 * @return array The list of groups
	 */
	public function LoadGroups() {

		global $gCms;
		$db = $gCms->GetDb();
		$userops = $gCms->GetUserOperations();

		$result = array();

		// table
		$groups_table = cms_db_prefix() . "groups";

		// query
		$query = "SELECT * FROM $groups_table ORDER BY group_id";

		if($dbresult = $db->Execute($query)) {

			while ($dbresult && $row = $dbresult->FetchRow()) {
				
				// do not show admin group to users who are not in admins
				if($row['group_id'] == 1
				&& !$userops->UserIsAdmin()) {
					continue;
				}

				// new group
				$onegroup = new Group();
				$onegroup->id = $row['group_id'];
				$onegroup->name = $row['group_name'];
				$onegroup->active = $row['active'];
				$onegroup->default = $row['group_default'];

				// setup extra buttons and add the object to the result
				$result[] = $this->setupButtons($onegroup);

			}

		}

		return $result;

	}

	/**
	 * Alias for LoadGroups()
	 */
	public function LoadItems() {
		return $this->LoadGroups();
	}

	/**
	 * Load a group from the database by its id
	 *
	 * @param integer $id The id of the group to load
	 * @return mixed The group if found. If it's not found, then false
	 */
	function LoadGroupByID($id)
	{
		// default
		$result = false;

		// get cms and db
		global $gCms;
		$db = &$gCms->GetDb();

		// table
		$groups_table = cms_db_prefix() . "groups";

		// set query
		$query = "SELECT group_id, group_name, active, group_default
				  FROM $groups_table WHERE group_id = $id ORDER BY group_id";

		if($dbresult = $db->Execute($query)) {

			while ($row = $dbresult->FetchRow()) {

				// create the group
				$onegroup = new Group();
				$onegroup->id = $row['group_id'];
				$onegroup->name = $row['group_name'];
				$onegroup->active = $row['active'];
				$onegroup->default = $row['group_default'];

				// setup extra buttons and add the object to the result
				$result = $this->setupButtons($onegroup);

			}

		}

		return $result;

	}

	/**
	 * Alias for LoadGroupByID
	 */
	function LoadByID($id) {
		return $this->LoadGroupByID($id);
	}

	/**
	 * Add extra stuff to our group object
	 *
	 * @param object $group - group object
	 * @return object
	 */
	function setupButtons($group) {

		// get the admin theme
		global $gCms;
		$themeObject = $gCms->GetAdminTheme();

		// get icons from theme
		$group->toggle_active = $themeObject->getIconButton('active', $group);
		$group->toggle_default = $themeObject->getIconButton('default', $group);
		$group->delete = $themeObject->getIconButton('delete', $group);
		$group->edit = $themeObject->getIconButton('edit', $group);
		$group->edit_link = $themeObject->getIconButton('edit_link', $group);
		
		$group->permissions = '<a class="list__item--link  color-one-text" href="users_groups_permissions.php">' . lang('permissions') . '</a>';

		return $group;

	}

	/**
	 * Given a group object, inserts it into the database.
	 *
	 * @param mixed $group The group object to save to the database
	 * @return integer The id of the newly created group. If none is created, -1
	 */
	function InsertGroup($group)
	{
		$result = lang('group_added_error');

		global $gCms;
		$db = $gCms->GetDb();
	  	$config = $gCms->GetConfig();
		$groups_table = cms_db_prefix() . "groups";
		$group_name = $group->name;

		// check for conflict in the name
		$query = "SELECT group_id FROM $groups_table WHERE group_name = '$group_name'";
		$tmp = $db->GetOne($query);
		if($tmp) return lang('group_exists', array($group->name));

		// wx get next auto_increment to use with return
		// this is the new content id
		$db_name = $config['db_name'];
		$query = "SELECT AUTO_INCREMENT FROM information_schema.TABLES
				  WHERE TABLE_SCHEMA = '$db_name' AND TABLE_NAME = '$groups_table'";
		$new_group_id = $db->GetOne($query);

		if($new_group_id == NULL) {
			die("Need to set AUTO_INCREMENT in the $groups_table!");
		}

		$time = $db->DBTimeStamp(time());
		$query = "INSERT INTO $groups_table (group_id, group_name, active, group_default, create_date, modified_date) VALUES (?,?,?,?,".$time.", ".$time.")";

		$dbresult = $db->Execute($query, array($new_group_id, $group->name, $group->active, $group->default));

		if ($dbresult !== false)
		{
			$group->id = $new_group_id;
			$result = $group;
		}

		return $result;
	}

	/**
	 * Given a group object, update its attributes in the database.
	 *
	 * @param mixed $group The group to update
	 * @return boolean True if the update was successful, false if not
	 */
	function UpdateGroup($group)
	{
		$result = lang('group_edited_error');

		global $gCms;
		$db = $gCms->GetDb();
		$groups_table = cms_db_prefix() . "groups";
		$group_id = $group->id;
		$group_name = $group->name;

		// check for conflict in the name
		$query = "SELECT group_id FROM $groups_table
		WHERE group_name = '$group_name' AND group_id <> $group_id";
		$tmp = $db->GetOne($query);
		if($tmp) return lang('group_exists', array($group->name));

		$time = $db->DBTimeStamp(time());
		$query = "UPDATE $groups_table SET group_name = ?, active = ?, group_default = ?, modified_date = ".$time." WHERE group_id = ?";
		$dbresult = $db->Execute($query, array($group->name, $group->active, $group->default, $group->id));
		if ($dbresult !== false)
		{
			$result = $group;
		}

		return $result;
	}

	/**
	 * Given a group id, delete it from the database along with all its associations.
	 *
	 * @param integer $id The group's id to delete
	 * @return boolean True if the delete was successful. False if not.
	 */
	function DeleteGroupByID($id)
	{
		$result = false;

		global $gCms;
		$db = &$gCms->GetDb();

		$query = 'DELETE FROM '.cms_db_prefix().'user_groups where group_id = ?';
		$dbresult = $db->Execute($query, array($id));

		$query = "DELETE FROM ".cms_db_prefix()."group_perms where group_id = ?";
		$dbresult = $db->Execute($query, array($id));

		$query = "DELETE FROM ".cms_db_prefix()."groups where group_id = ?";
		$dbresult = $db->Execute($query, array($id));

		if ($dbresult !== false)
		{
			$result = true;
		}

		return $result;
	}




	/**
	 * Tests if user can be deleted in the user list page
	 *
	 * @param int $group_id User ID to test
	 * @return true or false
	 */
	function IsGroupRemovable($group_id) {

		// get user operations
		global $gCms;
		$userops = $gCms->GetUserOperations();
		
		// 1 --> admin group can't be delete
		// 2 --> user can't remove group he belongs to
		// 3 --> can't remove default content
		if($group_id == 1) {
			return 'admin';
		} else if($userops->UserInGroup(get_userid(), $group_id)) {
			return 'your<br/>group';
		} else if($group_id == $this->GetDefaultID()) {
			return 'default';
		}

		// if nothing found we can remove
		return true;

	}

	/**
	 * Tests if group active status can be changed
	 *
	 * @param int $group_id Group ID to test
	 * @return true or false
	 */
	function IsToggleActivePossible($group_id) {

		// get user operations
		global $gCms;
		$userops = $gCms->GetUserOperations();
		
		// 1 --> admin group can't be toggled
		// 2 --> user can't toggle group he belongs to
		// 3 --> can't change default group
		if($group_id == 1
		|| $userops->UserInGroup(get_userid(), $group_id)
		|| $group_id == $this->GetDefaultID()) {
			return false;
		}

		return true;

	}

	/**
	 * Tests if group can be edited
	 *
	 * @param int $group_id Group ID to test
	 * @return true or false
	 */
	function IsEditPossible($group_id) {

		// get user operations
		global $gCms;
		$userops = $gCms->GetUserOperations();

		// admin group can be edited only by main admin users
		if($group_id == 1
		&& !$userops->UserInGroup(get_userid(), 1)) {
			return false;
		}

		// user is in this group - he can't edit this group
		// except if user is in main admin group
		if($userops->UserInGroup(get_userid(), $group_id)
		&& !$userops->UserInGroup(get_userid(), 1)) {

			// except if user has modify permision
			if(check_permission(get_userid(), 'users_groups_add_remove')) {
				return true;
			}

			return false;
		}

		// es esmu grupā
		// vai šai grupai ir iespēja labot grupas?
		if($userops->UserInGroup(get_userid(), $group_id)) {
			return true;
		}

		return true;

	}

	/**
	 * Change the active state of the template
	 *
	 * @param int $template_id template id
	 * @return array for json output
	 */
	public function ChangeActiveStateByID($id) {

		// load the group
		$group = $this->LoadGroupByID($id);

		// do the update
		return content_change_active_state($group);

	}

	/**
	 * Delete group by id
	 *
	 * @param int $template_id template id
	 * @return array with response for json output
	 */
	public function DeleteByID($id) {
		
		// defaults to no access
		$response = array(
			'status' => 'error',
			'message' => lang('update_error_no_access')
		);
		
		// do we have access?
		if(check_permission(get_userid(), "users_groups_add_remove")) {

			$group = $this->LoadGroupByID($id);
			
			// do the update
			$response = content_delete($group);

		}

		// return response
		return $response;

	}

	/**
	 * Change default object
	 *
	 * @param int $template_id template id
	 * @return array with response for json output
	 */
	public function GetDefaultID() {

		// get cms and db
		global $gCms;
		$db = $gCms->GetDb();

		// table and query
		$groups_table = cms_db_prefix() . "groups";
		$query = "SELECT group_id FROM $groups_table WHERE group_default = 1 LIMIT 1";

		// run the query and get the id
		if($id = $db->GetOne($query)) {
			return $id;
		}

		return false;

	}

	/**
	 * Change default object
	 *
	 * @param int $id new default id
	 * @return array with response for json output
	 */
	public function ChangeDefaultContent($id) {
		return content_change_default_content($this, $id);
	}

}

# vim:ts=4 sw=4 noet
?>