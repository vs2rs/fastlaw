<?php // -*- mode:php; tab-width:4; indent-tabs-mode:t; c-basic-offset:4; -*-
#CMS - CMS Made Simple
#(c)2004-2010 by Ted Kulp (ted@cmsmadesimple.org)
#This project's homepage is: http://cmsmadesimple.org
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
#$Id: class.user.inc.php 2961 2006-06-25 04:49:31Z wishy $

/**
 * User related functions.
 *
 * @package CMS
 * @license GPL
 */

/**
 * Include user class definition
 */
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'class.user.inc.php');

/**
 * Class for doing user related functions.  Maybe of the User object functions
 * are just wrappers around these.
 *
 * @since 0.6.1
 * @package CMS
 * @license GPL
 */
class UserOperations
{
	/**
	 * Gets a list of all users
	 *
	 * @returns array An array of User objects
	 * @since 0.6.1
	 */
	public function LoadUsers()
	{
		global $gCms;
		$db = $gCms->GetDb();

		$result = array();

		$query = "SELECT user_id, username, password, first_name, last_name, email, user_image, active, admin_access FROM ".cms_db_prefix()."users ORDER BY username";
		$dbresult = $db->Execute($query);

		while ($dbresult && $row = $dbresult->FetchRow()) {

			// main admin only visible to himself
			if($row['user_id'] == 1 && get_userid() != 1) {
				continue;
			}

			$oneuser = new User();
			$oneuser->id = $row['user_id'];
			$oneuser->username = $row['username'];
			$oneuser->firstname = $row['first_name'];
			$oneuser->lastname = $row['last_name'];
			$oneuser->fullname = $row['first_name'] . ' ' . $row['last_name'];
			$oneuser->email = $row['email'];
			$oneuser->user_image = $row['user_image'];
			$oneuser->password = $row['password'];
			$oneuser->active = $row['active'];
			$oneuser->adminaccess = $row['admin_access'];

			// setup extra buttons and add the object to the result
			$result[] = $this->setupButtons($oneuser);
		}

		return $result;
	}


	/**
	 * Gets a list of all users in a given group
	 *
	 * @param mixed $groupid Group for the loaded users
	 * @returns array An array of User objects
	 */
	function LoadUsersInGroup($groupid)
	{
		global $gCms;
		$db = &$gCms->GetDb();
		$result = array();

		$query = "SELECT u.user_id, u.username, u.password, u.first_name, u.last_name, u.email, u.user_image, u.active, u.admin_access FROM ".cms_db_prefix()."users u, ".cms_db_prefix()."groups g, ".cms_db_prefix()."user_groups cg where cg.user_id = u.user_id and cg.group_id = g.group_id and g.group_id =? ORDER BY username";
		$dbresult = $db->Execute($query, array($groupid));

		while ($dbresult && $row = $dbresult->FetchRow())
		{
			$oneuser = new User();
			$oneuser->id = $row['user_id'];
			$oneuser->username = $row['username'];
			$oneuser->firstname = $row['first_name'];
			$oneuser->lastname = $row['last_name'];
			$oneuser->fullname = $row['first_name'] . ' ' . $row['last_name'];
			$oneuser->email = $row['email'];
			$oneuser->user_image = $row['user_image'];
			$oneuser->password = $row['password'];
			$oneuser->active = $row['active'];
			$oneuser->adminaccess = $row['admin_access'];
			
			// setup extra buttons and add the object to the result
			$result[] = $this->setupButtons($oneuser);
		}

		return $result;
	}

	/**
	 * Loads a user by user id.
	 *
	 * @param mixed $id User id to load
	 *
	 * @returns mixed If successful, the filled User object.  If it fails, it returns false.
	 * @since 0.6.1
	 */
	public function LoadUserByID($id)
	{
		$result = false;

		global $gCms;
		$db = &$gCms->GetDb();

		// set the table
		$user_table = cms_db_prefix() . "users";

		// set the query
		$query = "SELECT * FROM $user_table WHERE user_id = $id";
		$dbresult = $db->Execute($query);

		while ($dbresult && $row = $dbresult->FetchRow())
		{
			$oneuser = new User();
			$oneuser->id = $id;
			$oneuser->username = $row['username'];
			$oneuser->password = $row['password'];
			$oneuser->firstname = $row['first_name'];
			$oneuser->lastname = $row['last_name'];
			$oneuser->fullname = $row['first_name'] . ' ' . $row['last_name'];
			$oneuser->email = $row['email'];
			$oneuser->user_image = $row['user_image'];
			$oneuser->adminaccess = $row['admin_access'];
			$oneuser->active = $row['active'];
			$result = $this->setupButtons($oneuser);
		}

		return $result;
	}

	/**
	 * Alias for LoadGroupByID
	 */
	public function LoadByID($id) {
		return $this->LoadUserByID($id);
	}

	/**
	 * Add extra stuff to our group object
	 *
	 * @param object $group - group object
	 * @return object
	 */
	function setupButtons($user) {

		// get the admin theme
		global $gCms;
		$themeObject = $gCms->GetAdminTheme();

		// get icons from theme
		$user->toggle_active = $themeObject->getIconButton('active', $user);
		$user->delete = $themeObject->getIconButton('delete', $user);
		$user->edit = $themeObject->getIconButton('edit', $user);
		$user->edit_link = $themeObject->getIconButton('edit_link', $user);

		return $user;

	}

	/**
	 * Saves a new user to the database.
	 *
	 * @param mixed $usre User object to save
	 *
	 * @returns mixed The new user id.  If it fails, it returns -1.
	 * @since 0.6.1
	 */
	function InsertUser($user)
	{

		// wx
		// set user image to empty
		$user->user_image = "";

		$result = lang('user_added_error');

		global $gCms;
		$db = &$gCms->GetDb();
		$config = $gCms->GetConfig();

		// check for conflict in username
		$user_table = cms_db_prefix() . 'users';
		$query = 'SELECT user_id FROM $user_table WHERE username = ?';
		$tmp = $db->GetOne($query,array($user->username));
		if( $tmp ) return lang('user_exists', array($user->username));

		// wx get next auto_increment to use with return
		// this is the new users id
		$db_name = $config['db_name'];
		$user_table = cms_db_prefix() . "users";
		$query = "SELECT AUTO_INCREMENT
				  FROM information_schema.TABLES
				  WHERE TABLE_SCHEMA = '$db_name'
				  AND TABLE_NAME = '$user_table'";
		$new_user_id = $db->GetOne($query);
		  
		$time = $db->DBTimeStamp(time());
		// wx hide $new_user_id = $db->GenID(cms_db_prefix()."users_seq");
		// wx remove user_id ($new_user_id) form value array
		// wx we delete users_seq table and use auto increment
		$query = "INSERT INTO $user_table (username, password, active, first_name, last_name, email, user_image, admin_access, create_date, modified_date) VALUES (?,?,?,?,?,?,?,?,".$time.",".$time.")";
		
		#$dbresult = $db->Execute($query, array($new_user_id, $user->username, $user->password, $user->active, $user->firstname, $user->lastname, $user->email, $user->adminaccess));
		$dbresult = $db->Execute($query, array($user->username, $user->password, $user->active, $user->firstname, $user->lastname, $user->email, $user->user_image, 1)); //Force admin access on
		
		if ($dbresult !== false)
		{
			$user->id = $new_user_id;
			$result = $user;
			
			// update groups
			$this->UpdateUserGroups($user);
		}

		return $result;
	}

	/**
	 * Updates an existing user in the database.
	 *
	 * @param mixed $user User object to save
	 *
	 * @returns mixed If successful, true.  If it fails, false.
	 * @since 0.6.1
	 */
	function UpdateUser($user)
	{
		$result = lang('user_edited_error'); 

		global $gCms;
		$db = $gCms->GetDb();

		// check for username conflict
		$user_table = cms_db_prefix() . 'users';
		$user_name = $user->username;
		$user_id = $user->id;
		$tmp = $db->GetOne(
			"SELECT user_id FROM $user_table
			 WHERE username = '$user_name' AND user_id <> $user_id"
		);
		if($tmp) return lang('user_exists', array($user->username));

		$time = $db->DBTimeStamp(time());
		$query = "UPDATE ".cms_db_prefix()."users SET username = ?, password = ?, active = ?, modified_date = ".$time.", first_name = ?, last_name = ?, email = ?, user_image = ?, admin_access = ? WHERE user_id = ?";
		$dbresult = $db->Execute($query, array($user->username, $user->password, $user->active, $user->firstname, $user->lastname, $user->email, $user->user_image, 1, $user->id));

		if ($dbresult !== false)
		{
			$result = $user;
			$this->UpdateUserGroups($user);
		}

		return $result;
	}

	/**
	 * Updates user groups
	 *
	 * @param object $user - user object to update
	 * @return void
	 */
	public function UpdateUserGroups($user) {
		
		global $gCms;
		$db = $gCms->GetDb();

		// get groups
		$groupops = $gCms->GetGroupOperations();
		$group_list = $groupops->LoadGroups();

		// table
		$user_groups_table = cms_db_prefix() . "user_groups";

		// clear old groups from db
		$group_delete_query = "DELETE FROM $user_groups_table WHERE user_id = ?";
		$db->Execute($group_delete_query, array($user->id));

		// if post groups
		if(isset($_POST['groups'])) {

			// insert query
			$group_insert_query = "
			INSERT INTO $user_groups_table (user_id, group_id)
			VALUES (?, ?)";

			// go through groups and do insert
			foreach($group_list as $group) {

				// check if group is in post
				if(in_array($group->id, $_POST['groups'])) {
					$db->Execute($group_insert_query, array($user->id, $group->id));
				}

			}

		}

	}

	function GenerateDropdown($currentuserid='', $name='ownerid')
	{
		$result = '';

		$allusers = UserOperations::LoadUsers();

		if (count($allusers) > 0)
		{
			$result .= '<select name="'.$name.'">';
			foreach ($allusers as $oneuser)
			{
				$result .= '<option value="'.$oneuser->id.'"';
				if ($oneuser->id == $currentuserid)
				{
					$result .= ' selected="selected"';
				}
				$result .= '>'.$oneuser->username.'</option>';
			}
			$result .= '</select>';
		}

		return $result;
	}


	/**
	 * Tests $uid is a member of the group identified by $gid
	 *
	 * @param int $user_id User ID to test
	 * @param int $group_id Group ID to test
	 * @returns true if test passes, false otherwise
	 */
	public function UserIsAdmin($user_id = false) {

		// get current user if no user id set
		if($user_id == false) $user_id = get_userid();

		// simple check
		// id = 1 - this is the main super duper user owner unchangeble
		// and same for groups - id = 1 is administrators
		// then we just check if user is in the admin group
		return $user_id == 1 || $this->UserInGroup($user_id, 1);

	}


	/**
	 * Tests $uid is a member of the group identified by $gid
	 *
	 * @param int $uid User ID to test
	 * @param int $gid Group ID to test
	 * @returns true if test passes, false otherwise
	 */
	public function UserInGroup($uid, $gid) {

		// get from db
		global $gCms;
		$db = $gCms->GetDb();

		// query
		$user_groups_table = cms_db_prefix() . "user_groups";
		$query = "SELECT user_id FROM $user_groups_table
		WHERE user_id = ? AND group_id = ?";

		// check results
		if(!$db->GetRow($query, array($uid, $gid))) {
			return false;
		}
		
		return true;

	}

	/**
	 * Tests if user can be deleted in the user list page
	 *
	 * @param int $user_id User ID to test
	 * @return true or false
	 */
	function IsUserRemovable($user_id) {

		// 1 --> admin can't be delete
		// 2 --> user can't remove himself
		if($user_id == 1) {
			return 'admin';
		} else if($user_id == get_userid()) {
			return 'user';
		}

		// if nothing found we can remove
		return true;

	}

	/**
	 * Tests if user active status can be changed
	 *
	 * @param int $user_id User ID to test
	 * @return true or false
	 */
	function IsToggleActivePossible($user_id) {
		
		// 1 --> admin user can't be changed
		// 2 --> user can't change himself
		if($user_id == 1 || $user_id == get_userid()) {
			return false;
		}

		return true;

	}

	/**
	 * Tests if user can be edited
	 *
	 * @param int $group_id Group ID to test
	 * @return true or false
	 */
	function IsEditPossible($id) {
		
		// 1 --> only admin can change admin
		if($id == 1 && get_userid() != 1) {
			return false;
		}

		return true;

	}

	/**
	 * Tests if user can be edited
	 *
	 * @param int $group_id Group ID to test
	 * @return true or false
	 */
	function IsUsernameChangable($id) {

		// 1 --> user can change his own username
		// 2 --> user has access to user management
		// 3 --> admin can change whatever he wants
		return $id == get_userid()
			|| check_permission(get_userid(), "users_add_remove")
			|| $this->UserIsAdmin(get_userid());

	}

	/**
	 * Change the active state of the user
	 *
	 * TODO --> vai šis varētu būt globāls update visiem?
	 * piemēram zem UpdateOperations klases
	 *
	 * @param int $user_id user id
	 * @param obj $themeObject theme object
	 * @return array for json output
	 */
	public function ChangeActiveStateByID($user_id) {
		
		// defaults to no access
		$response = array(
			'status' => 'error',
			'message' => lang('update_error_no_access')
		);
		
		// do we have access?
		if(check_permission(get_userid(), "users_add_remove")) {

			// load the user and do the update
			if($user = $this->LoadUserByID($user_id)) {

				// check if we can change the active state
				if($user->CanSetActive()) {

					// if current state active change to unactive and vice versa
					$user->active = $user->active == 1 ? 0 : 1;

					// save the user
					$user->Save();

					// all good set success
					$response['status'] = "success";
					$response['message'] = "all good";

					// get cms
					global $gCms;

					// get theme
					$theme = $gCms->GetAdminTheme();

					// get the icon
					$response['icons'] = array(
						array(
							'id'		=> 'user-' . $user->id,
							'action'	=> 'active',
							'icon'		=> $theme->getIconButton('active', $user),
							'status'	=> $user->active
						)
					);

				} else {

					// we can't change the state
					$response['message'] = lang('active_update_error');

				}
			
			} else {

				// set message no access
				$response['message'] = lang('update_error_no_element', $user->id);

			}

		}

		// return response
		return $response;

	}

	/**
	 * Deletes an existing user from the database.
	 *
	 * @param mixed $id Id of the user to delete
	 *
	 * @returns mixed If successful, true.  If it fails, false.
	 * @since 0.6.1
	 */
	function DeleteUserByID($id)
	{
 		if( $id <= 1 ) return false;
 		if( !check_permission(get_userid(),'users_add_remove') ) return false;
	
		$result = false;

		global $gCms;
		$db = &$gCms->GetDb();

		$query = "DELETE FROM ".cms_db_prefix()."user_groups where user_id = ?";
		$db->Execute($query, array($id));

		$query = "DELETE FROM ".cms_db_prefix()."users where user_id = ?";
		$dbresult = $db->Execute($query, array($id));

		$query = "DELETE FROM ".cms_db_prefix()."userprefs where user_id = ?";
		$dbresult = $db->Execute($query, array($id));

		if ($dbresult !== false)
		{
			$result = true;
		}

		return $result;
	}

	/**
	 * Delete user by id
	 *
	 * @param int $template_id template id
	 * @return array with response for json output
	 */
	public function DeleteByID($user_id) {
		
		// defaults to no access
		$response = array(
			'status' => 'error',
			'message' => lang('update_error_no_access')
		);
		
		// do we have access?
		if(check_permission(get_userid(), "users_add_remove")) {

			// load the user and do the update
			if($user = $this->LoadUserByID($user_id)) {

				// default to can't delete
				$response['message'] = lang('delete_error');

				// user name
				$user_name = $user->Name();

				// check if we can change the active state
				if($user->CanDelete()) {

					// delete
					if($user->Delete()) {

						// all good set success
						$response['status'] = "success";

						// set the message
						$response['deleted'] = lang('user_deleted', $user_name);

						// admin log
						audit($user_id, $user_name, 'User Deleted');

					}

				}
			
			} else {

				// set message no access
				$response['message'] = lang('update_error_no_element', $user_id);

			}

		}

		// return response
		return $response;

	}

}

?>
