<?php // -*- mode:php; tab-width:4; indent-tabs-mode:t; c-basic-offset:4; -*-
#CMS - CMS Made Simple
#(c)2004-2010 by Ted Kulp (ted@cmsmadesimple.org)
#This project's homepage is: http://cmsmadesimple.org
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
#$Id$

/**
 * Template related functions.
 *
 * @package CMS
 * @license GPL
 */

/**
 * Include template class definition
 */
include_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'class.template.inc.php');

/**
 * Class for doing template related functions.  Many of the Template object functions are just wrappers around these.
 *
 * @since		0.6
 * @package		CMS
 * @license GPL
 */
class TemplateOperations
{
	function LoadTemplates()
	{
		global $gCms;
		$db = &$gCms->GetDb();

		$result = array();

		$query = "SELECT * FROM ".cms_db_prefix()."templates ORDER BY template_name";
		$dbresult = &$db->Execute($query);

		while ($dbresult && !$dbresult->EOF)
		{
			$onetemplate = new PageTemplate();
			$onetemplate->id = $dbresult->fields['template_id'];
			$onetemplate->name = $dbresult->fields['template_name'];
			$onetemplate->active = $dbresult->fields['active'];
			$onetemplate->default = $dbresult->fields['default_template'];
			$onetemplate->content = $dbresult->fields['template_content'];
			$onetemplate->users = $dbresult->fields['template_users'];
			$onetemplate->encoding = $dbresult->fields['encoding'];
			$onetemplate->stylesheet = $dbresult->fields['stylesheet'];
			// $onetemplate->modified_date = $db->UnixTimeStamp($dbresult->fields['modified_date']);
			$onetemplate->modified_date = $dbresult->fields['modified_date'];
			$result[] = $onetemplate;
			$dbresult->MoveNext();
		}
		
		if ($dbresult) $dbresult->Close();

		return $result;
	}

	function & LoadTemplateByID($id) {

		// default
		$result = false;

		// check if id is not null
		if($id !== null) {

			global $gCms;
			$db = &$gCms->GetDb();
			$cache = &$gCms->TemplateCache;

			// check the cache
			if(isset($cache[$id])) {
				return $cache[$id];
			}

			// query
			$query = "SELECT template_id, template_name, template_content, template_users, stylesheet, encoding, active, default_template, modified_date FROM ".cms_db_prefix()."templates WHERE template_id = ?";
			$row = &$db->GetRow($query, array($id));

			if($row) {

				$onetemplate = new PageTemplate();
				$onetemplate->id = $row['template_id'];
				$onetemplate->name = $row['template_name'];
				$onetemplate->content = $row['template_content'];
				$onetemplate->users = $row['template_users'];
				$onetemplate->stylesheet = $row['stylesheet'];
				$onetemplate->encoding = $row['encoding'];
				$onetemplate->default = $row['default_template'];
				$onetemplate->active = $row['active'];
				$onetemplate->modified_date = $db->UnixTimeStamp($row['modified_date']);
				$result = $onetemplate;

				if(!isset($cache[$onetemplate->id])) {
					$cache[$onetemplate->id] = $onetemplate;
				}

			}

		}

		return $result;
	}

	function LoadTemplateByContentAlias($alias)
	{
		$result = false;

		global $gCms;
		$db = &$gCms->GetDb();

		$query = "SELECT t.template_id, t.template_name, t.template_content, t.template_users, t.stylesheet, t.encoding, t.active, t.default_template, t.modified_date FROM ".cms_db_prefix()."templates t INNER JOIN ".cms_db_prefix()."content c ON c.template_id = t.template_id WHERE (c.content_alias = ? OR c.content_id = ?) AND c.active = 1";
		$row = &$db->GetRow($query, array($alias, $alias));

		if ($row)
		{
			$onetemplate = new PageTemplate();
			$onetemplate->id = $row['template_id'];
			$onetemplate->name = $row['template_name'];
			$onetemplate->content = $row['template_content'];
			$onetemplate->users = $row['template_users'];
			$onetemplate->stylesheet = $row['stylesheet'];
			$onetemplate->encoding = $row['encoding'];
			$onetemplate->default = $row['default_template'];
			$onetemplate->active = $row['active'];
			$onetemplate->modified_date = $db->UnixTimeStamp($row['modified_date']);
			$result = $onetemplate;
		}

		return $result;
	}

	function LoadTemplateAndContentDates($alias)
	{
		$result = array();

		global $gCms;
		$db = &$gCms->GetDb();

		$query = "SELECT c.modified_date AS c_date, t.modified_date AS t_date FROM ".cms_db_prefix()."templates t INNER JOIN ".cms_db_prefix()."content c ON c.template_id = t.template_id WHERE (c.content_alias = ? OR c.content_id = ?) AND c.active = 1";
		$dbresult = &$db->Execute($query, array($alias, $alias));

		while ($dbresult && !$dbresult->EOF)
		{
			$result[] = $dbresult->fields['c_date'];
			$result[] = $dbresult->fields['t_date'];
			$dbresult->MoveNext();
		}
		
		if ($dbresult) $dbresult->Close();

		return $result;
	}

	function LoadDefaultTemplate()
	{
		$result = false;

		global $gCms;
		$db = &$gCms->GetDb();

		$query = "SELECT template_id, template_name, template_content, template_users, stylesheet, encoding, active, default_template FROM ".cms_db_prefix()."templates WHERE default_template = 1";
		$row = &$db->GetRow($query);

		if($row)
		{
			$onetemplate = new PageTemplate();
			$onetemplate->id = $row['template_id'];
			$onetemplate->name = $row['template_name'];
			$onetemplate->content = $row['template_content'];
			$onetemplate->users = $row['template_users'];
			$onetemplate->stylesheet = $row['stylesheet'];
			$onetemplate->encoding = $row['encoding'];
			$onetemplate->default = $row['default_template'];
			$onetemplate->active = $row['active'];
			$result = $onetemplate;
		}

		return $result;
	}

	function UsageCount($id)
	{
		$result = 0;

		global $gCms;
		$db = &$gCms->GetDb();

		$query = "SELECT count(*) as the_count FROM ".cms_db_prefix()."content WHERE template_id = ?";
		$row = &$db->GetRow($query, array($id));

		if($row)
		{
			$result = $row['the_count'];
		}
	
		return $result;
	}

	function InsertTemplate($template)
	{
		$result = -1; 

		global $gCms;
		$db = &$gCms->GetDb();
	  	$config = $gCms->GetConfig();

		$time = $db->DBTimeStamp(time());

        // wx get next auto_increment to use with return
        // this is the new content id
        $db_name = $config['db_name'];
        $templates_table = cms_db_prefix() . "templates";
        $query = "SELECT AUTO_INCREMENT
                  FROM information_schema.TABLES
                  WHERE TABLE_SCHEMA = '$db_name'
                  AND TABLE_NAME = '$templates_table'";
        $new_template_id = $db->GetOne($query);

        if($new_template_id == NULL) {
            die("Need to set AUTO_INCREMENT in the $templates_table!");
        }

		$query = "INSERT INTO ".cms_db_prefix()."templates (template_id, template_name, template_content, template_users, stylesheet, encoding, active, default_template, create_date, modified_date) VALUES (?,?,?,?,?,?,?,?,".$time.",".$time.")";
		$dbresult = $db->Execute($query, array($new_template_id, $template->name, $template->content, $template->users, $template->stylesheet, $template->encoding, $template->active, $template->default));
		if ($dbresult !== false)
		{
			$result = $new_template_id;
		}

		return $result;
	}

	function UpdateTemplate($template)
	{
		$result = false; 

		global $gCms;
		$db = &$gCms->GetDb();

		$time = $db->DBTimeStamp(time());
		$query = "UPDATE ".cms_db_prefix()."templates SET template_name = ?, template_content = ?, template_users = ?, stylesheet = ?, encoding = ?, active = ?, default_template = ?, modified_date = ".$time." WHERE template_id = ?";
		
		$dbresult = $db->Execute($query, array(
			$template->name,
			$template->content,
			$template->users,
			$template->stylesheet,
			$template->encoding,
			$template->active,
			$template->default,
			$template->id)
		);

		if ($dbresult !== false)
		{
			$result = true;
		}

		return $result;
	}

	function DeleteTemplateByID($id)
	{
		$result = false;

		global $gCms;
		$db = &$gCms->GetDb();

		$query = "DELETE FROM ".cms_db_prefix()."templates where template_id = ?";
		$dbresult = $db->Execute($query,array($id));

		if ($dbresult !== false)
		{
			$result = true;
		}

		return $result;
	}

	function CountPagesUsingTemplateByID($id)
	{
		$result = 0;

		global $gCms;
		$db = &$gCms->GetDb();

        $query = "SELECT count(*) AS count FROM ".cms_db_prefix()."content WHERE template_id = ?";
        $row = &$db->GetRow($query,array($id));

		if ($row)
		{
			if (isset($row["count"]))
			{
				$result = $row["count"];
			}
		}

		return $result;
	}

	function StylesheetsUsed()
	{
		$result = 0;

		global $gCms;
		$db = &$gCms->GetDb();

        $query = "SELECT count(*) AS count FROM ".cms_db_prefix()."templates WHERE stylesheet is not null and stylesheet != ''";
        $row = &$db->GetRow($query);

		if ($row)
		{
			if (isset($row["count"]))
			{
				$result = $row["count"];
			}
		}

		return $result;
	}

	function TouchAllTemplates($blob_name='')
	{
		$result = false;

		global $gCms;
		$db = &$gCms->GetDb();

		$dbresult = false;

		$time = $db->DBTimeStamp(time());
		if ($blob_name != '')
		{
			$query = "UPDATE ".cms_db_prefix()."templates SET modified_date = ".$time." WHERE template_content like ?";
			$dbresult = $db->Execute($query,array('%{html_blob name="'.$blob_name.'"}%'));
		}
		else
		{
			$query = "UPDATE ".cms_db_prefix()."templates SET modified_date = ".$time;
			$dbresult = $db->Execute($query);
		}

		if ($dbresult !== false)
		{
			$result = true;
		}

		return $result;
	}

	function CheckExistingTemplateName($name)
	{
		$result = false;

		global $gCms;
		$db = &$gCms->GetDb();

		$query = "SELECT template_id from ".cms_db_prefix()."templates WHERE template_name = ?";
		$attrs = array($name);
		$row = &$db->GetRow($query,$attrs);

		if ($row) {
			$result = true;
		}

		return $result;
	}
	
	function TemplateDropdown($id = 'template_id', $selected_id = -1, $othertext = '', $show_hidden = false)
	{
		$result = "";
		
		global $gCms;
		$templateops =& $gCms->GetTemplateOperations();

		$alltemplates = $templateops->LoadTemplates();
		
		if (count($alltemplates) > 0)
		{
			$result .= '<select name="'.$id.'"';
			if ($othertext != '')
			{
				$result .= ' ' . $othertext;
			}
			$result .= '>';
			#$result .= '<option value="">Select Template</option>';
			foreach ($alltemplates as $onetemplate)
			{
				if ($onetemplate->active == true || $show_hidden == true)
				{
					$result .= '<option value="'.$onetemplate->id.'"';
					if ($onetemplate->id == $selected_id || ($selected_id == -1 && $onetemplate->default == true))
					{
						$result .= ' selected="selected"';
					}
					$result .= '>'.$onetemplate->name.'</option>';
				}
			}
			$result .= '</select>';
		}
		
		return $result;
	}

	function TemplateAccess($template_id, $col) {

		// by default we can access this page with this template
		$result = true;

		// get the template
		if($template_id !== null) {
		
			// load the template and if success do the check
			if($template = $this->LoadTemplateById($template_id)) {

				// get user operations
				global $gCms;
				$userops = $gCms->GetUserOperations();

				// if admin we can stop all the check access is true
				if(get_userid() == 1
				|| $userops->UserInGroup(get_userid(), 1)) {
					return true;
				}

				// get the user access array
				$user_access = $template->Users();
				
				// first check if user has specific access settings
				if(isset($user_access->user)) {

					// set no access
					$result = false;
					
					// check the col for access
					if(isset($user_access->user->$col)
					&& is_array($user_access->user->$col)
					&& count($user_access->user->$col) > 0) {

						// go through all id's and check if user is in the list
						foreach ($user_access->user->$col as $user_id) {
							if($user_id == get_userid()) {
								$result = true; break;
							}
						}

					}

				} else {

					// if no specific user access - now we check for groups
			
					// is the groups set and is the col set?
					if(isset($user_access->group)
					&& isset($user_access->group->$col)) {

						// now we can check for groups are they set?
						if(is_array($user_access->group->$col)
						&& count($user_access->group->$col) > 0) {

							// set no access
							$result = false;

							// go through all groups and check if user is in the group
							foreach ($user_access->group->$col as $user_group_id) {
								if($userops->UserInGroup(get_userid(), $user_group_id)) {
									$result = true; break;
								}
							}

						}

					}

				}

			}
		
		}

		// return
		return $result;

	}

	/**
	 * Tests if template can be deleted in the template list page
	 *
	 * @param int $user_id User ID to test
	 * @return true or false
	 */
	function IsTemplateRemovable($template_id) {

		// load template
		if($template = $this->LoadTemplateByID($template_id)) {
		
			// 1 --> default template can't be deleted
			// 2 --> if template in use it can't be deleted
			if($template->default) {
				return 'default';
			} else if($this->CountPagesUsingTemplateByID($template->id) > 0) {
				return 'in use';
			} else {
				return true;
			}

		} 

		// if template not loaded
		return false;

	}

	/**
	 * Tests if template active status can be changed
	 *
	 * @param int $user_id User ID to test
	 * @return true or false
	 */
	function IsToggleActivePossible($template_id) {

		// load template
		if($template = $this->LoadTemplateByID($template_id)) {
		
			// 1 --> default template can't be changed
			// 2 --> if template in use it can't be changed
			if($template->default
			|| $this->CountPagesUsingTemplateByID($template->id) > 0) {
				return false;
			} else {
				return true;
			}

		}

		// if template not loaded
		return false;

	}

	/**
	 * Change the active state of the template
	 *
	 * @param int $template_id template id
	 * @param obj $themeObject theme object
	 * @return array for json output
	 */
	public function ChangeActiveStateByID($template_id) {
		
		// defaults to no access
		$response = array(
			'status' => 'error',
			'message' => lang('update_error_no_access')
		);
		
		// do we have access?
		if(check_permission(get_userid(), "templates_modify")) {

			// load the user and do the update
			if($template = $this->LoadTemplateByID($template_id)) {

				// check if we can change the active state
				if($template->CanSetActive()) {

					// if current state active change to unactive and vice versa
					$template->active = $template->active == 1 ? 0 : 1;

					// save the template
					$template->Save();

					// all good set success
					$response['status'] = "success";

					// get cms
					global $gCms;

					// get theme
					$theme = $gCms->GetAdminTheme();

					// get the icon
					$response['icons'] = array(
						array(
							'id'		=> $template->id,
							'action'	=> 'active',
							'icon'		=> $theme->getIconButton('active', $template),
							'status'	=> $template->active
						)
					);

				} else {

					// we can't change the state
					$response['message'] = lang('active_update_error');

				}
			
			} else {

				// set message no access
				$response['message'] = lang('update_error_no_element', $template->id);

			}

		}

		// return response
		return $response;

	}

	/**
	 * Delete template by id
	 *
	 * @param int $template_id template id
	 * @return array with response for json output
	 */
	public function DeleteByID($template_id) {
		
		// defaults to no access
		$response = array(
			'status' => 'error',
			'message' => lang('update_error_no_access')
		);
		
		// do we have access?
		if(check_permission(get_userid(), "Delete Templates")) {

			// load the user and do the update
			if($template = $this->LoadTemplateByID($template_id)) {

				// default to can't delete
				$response['message'] = lang('delete_error');

				// check if we can change the active state
				if($template->CanDelete()) {

					// delete
					if($template->Delete()) {

						// all good set success
						$response['status'] = "success";

						// get cms
						global $gCms;

						// get theme
						$theme = $gCms->GetAdminTheme();

						// set the message
						$response['deleted'] = lang('template_deleted', $template->Name());

						// reset current template object
						$template->SetInitialValues();

					}

				}
			
			} else {

				// set message no access
				$response['message'] = lang('update_error_no_element', $template->id);

			}

		}

		// return response
		return $response;

	}

}


?>