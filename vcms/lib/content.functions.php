<?php

/**
 * --------------------------
 *  Updating content
 * --------------------------
 */

/**
 * TODO: varbūt citur šis jāliek?
 * pagaidām liekam te
 *
 * @return boolean
 */

function output_json($response) {
	
	// set header
	header('Content-type: application/json; charset=utf-8');

	// encode json
	$response = json_encode($response);

	// output json
	echo $response; exit;

	// exit
	exit;

}

function content_action_json($action, $id, $operations) {

	// check the action and do it
	switch ($action) {

		case 'active':
		$response = $operations->ChangeActiveStateByID($id);
		break;

		case 'delete':
		$response = $operations->DeleteByID($id);
		break;

		case 'default':
		$response = $operations->ChangeDefaultContent($id);
		break;
		
		default:
		break;

	}

	// output
	output_json($response);

}

function content_change_active_state($object) {

	// check if object
	if($object) {

		// check if we can change the active state
		if($object->CanSetActive()) {

			// if current state active change to unactive and vice versa
			$object->active = $object->active == 1 ? 0 : 1;

			// save the template
			$object->Save();

			// all good set success
			$response['status'] = "success";

			// get cms
			global $gCms;

			// get theme
			$theme = $gCms->GetAdminTheme();

			// get the object name
			$object_class = strtolower(get_class($object));
			if(isset($object->__what)) $object_class = $object->__what;

			// get the icon
			$response['icons'] = array(
				array(
					'id'		=> $object_class . '-' . $object->id,
					'action'	=> 'active',
					'icon'		=> $theme->getIconButton('active', $object),
					'status'	=> $object->active
				)
			);

		} else {

			// we can't change the state
			$response['message'] = lang('active_update_error');

		}
	
	} else {

		// set message no access
		$response['message'] = lang('update_error_no_element', $object->id);

	}

	// return response
	return $response;

}

function content_delete($object) {

	// check object
	if($object) {

		// default to can't delete
		$response['message'] = lang('delete_error');

		// check if we can delete
		if($object->CanDelete()) {

			// object name and id
			$object_id = $object->id;
			$object_name = $object->Name();
			$object_class = get_class($object);

			// delete
			if($object->Delete()) {

				// all good set success
				$response['status'] = "success";

				// set the message
				$response['deleted'] = lang(strtolower($object_class) . '_deleted', $object_name);

				// admin log
				audit($object_id, $object_name, $object_class . ' Deleted');

			}

		}
	
	} else {

		// set message no access
		$response['message'] = lang('update_error_no_element', $object->id);

	}

	// return
	return $response;

}

function content_change_default_content($operations, $new_default_id) {

	// get cms
	global $gCms;
	$theme = $gCms->GetAdminTheme();

	// get old default element
	$old_default_id = $operations->GetDefaultID();

	// get objects
	$objects = $operations->LoadItems();

	// all good set success
	$response = array();
	$response['status'] = "success";
	$response['icons'] = array();

	// update default template
	foreach ($objects as $key => $object) {

		// get the object name
		if($key == 0) {
			$object_class = strtolower(get_class($object));
			if(isset($object->__what)) $object_class = $object->__what;
		}

		// update
		if($object->id == $new_default_id) {

			// update
			$object->default = 1;
			$object->active = 1;
			$object->Save();

			// change active icon
			$response['icons'][] = array(
				'id'		=> $object_class . '-' . $new_default_id,
				'action'	=> 'active',
				'icon'		=> $theme->getIconButton('active', $object),
				'status'	=> 1
			);

			// change default icon
			$response['icons'][] = array(
				'id'		=> $object_class . '-' . $new_default_id,
				'action'	=> 'default',
				'icon'		=> $theme->getIconButton('default', $object)
			);

			// change delete icon
			$response['icons'][] = array(
				'id'		=> $object_class . '-' . $new_default_id,
				'action'	=> 'delete',
				'icon'		=> $theme->getIconButton('delete', $object)
			);

		} else {

			// update
			$object->default = 0;
			$object->Save();

			// check if old default
			if($object->id == $old_default_id) {

				// change active icon
				$response['icons'][] = array(
					'id'		=> $object_class . '-' . $old_default_id,
					'action'	=> 'active',
					'icon'		=> $theme->getIconButton('active', $object),
					'status'	=> $object->active
				);

				// change default icon
				$response['icons'][] = array(
					'id'		=> $object_class . '-' . $old_default_id,
					'action'	=> 'default',
					'icon'		=> $theme->getIconButton('default', $object)
				);

				// change delete icon
				$response['icons'][] = array(
					'id'		=> $object_class . '-' . $old_default_id,
					'action'	=> 'delete',
					'icon'		=> $theme->getIconButton('delete', $object)
				);

			}

		}

	}

	// return
	return $response;

}

function create_form_input_list($input_list) {

	// create array of input names
	$form_inputs = array();

	// go through the list and setup
	foreach ($input_list as $form_input) {

		// skip password again
		if($form_input['name'] == 'passwordagain') continue;
		
		// skip label and buttons
		if(in_array($form_input['type'], array('label', 'button'))) continue;
		
		// add to the array
		$form_inputs[$form_input['name']] = $form_input;

	}

	// return array
	return $form_inputs;
	
}

?>
