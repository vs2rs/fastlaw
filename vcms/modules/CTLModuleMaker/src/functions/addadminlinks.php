	function addadminlinks($item,$params,$id,$returnid){
		// adds the admin links to the level items...
		/* $params : the base parameters for the admin links
				prefix
				tablename
				child (if the level has a child level, the name of the child level)
				levelname
				parentdefault (bool... whether or not there is a default element for each parent)
				orderbyparent (bool)
				addfiles
				sharechildren (bool)
				sharedbyparents (bool)
				files (string,csv)
		*/

		global $gCms;
		$admintheme = $gCms->variables["admintheme"];
		
		if( $this->GetPreference("restrict_permissions",false) && !$this->CheckPermission("module_".$this->GetName()."_advanced") && !$this->CheckPermission("module_".$this->GetName()."_manage_".$params["levelname"]) ){
			// no access - we don't create links
			$item->editlink = $item->name;
			$item->deletelink = "";
			$item->movelinks = "";
			$item->toggleactive = $item->active==1?$admintheme->DisplayImage("icons/system/true.gif","","","","systemicon"):$admintheme->DisplayImage("icons/system/false.gif","","","","systemicon");
			$item->toggledefault = $item->isdefault==1?$admintheme->DisplayImage("icons/system/true.gif","","","","systemicon"):$admintheme->DisplayImage("icons/system/false.gif","","","","systemicon");
			return $item;
		}
		
		$prefix = $params["prefix"];
		$moveparams = $params;
		$moveparams[$prefix."id"] = $item->id;
		$moveparams["currentorder"] = $item->item_order;
		if(isset($moveparams["files"]))	unset($moveparams["files"]);
		if($moveparams["orderbyparent"] && isset($item->parent_id)) $moveparams["parent"] = $item->parent_id;

		// edit button
		$item->editlink = $this->CreateLink(
			$id,
			"edit" . $prefix,
			$returnid,
			clean_up_string($item->name),
			array(
				$prefix. "id" => $item->id,
				"class" => "list__item--link  color-one-text"
			)
		);

		// edit button
		$item->editbutton = $this->CreateLink(
			$id,
			"edit" . $prefix,
			$returnid,
			$admintheme->DisplayIcon("edit"),
			array(
				$prefix. "id" => $item->id,
				"class" => "button-icon"
			)
		);

		// delete button
		$item->deletelink = $this->CreateLink(
			$id,
			"movesomething",
			$returnid,
			$admintheme->DisplayIcon("delete"),
			array_merge(
				array(
					"move" => "delete",
					"files" => $params["files"],
					"class" => "button-icon  button-icon--delete"
				),
				$moveparams
			),
			lang(
				"deleteconfirm",
				clean_up_string($item->name)
			)
		);

		// move item buttons
		$item->moveuplink = $this->CreateLink($id, "movesomething", $returnid, $admintheme->DisplayImage("icons/system/arrow-u.png",lang("up"),"","","systemicon"), array_merge(array("move"=>"up"),$moveparams));
		$item->movedownlink = $this->CreateLink($id, "movesomething", $returnid, $admintheme->DisplayImage("icons/system/arrow-d.png",lang("down"),"","","systemicon"), array_merge(array("move"=>"down"),$moveparams));
		$item->movelinks = $item->moveuplink ." ". $item->movedownlink;
		
		// toggle buttons

		// active

		// we rebuild the params, because we don't need so many for the toggle action
		$toggleparams = array(
			$prefix."id"=>$item->id,
			"prefix"=>$prefix,
			"tablename"=>$params["tablename"],
			"levelname"=>$params["levelname"],
			"class" => "button-icon"
		);
		if($params["parentdefault"] && isset($item->parent_id)){
			$toggleparams["parent"] = $item->parent_id;
			$toggleparams["parentdefault"] = 1;
		}
		if($item->active == 1) {
			$item->toggleactive = $this->CreateLink(
				$id,
				"toggle",
				$returnid,
				$admintheme->DisplayIcon("true"),
				array_merge(
					array("what" => "active", "newval" => 0),
					$toggleparams
				)
			);
		} else {
			$item->toggleactive = $this->CreateLink(
				$id,
				"toggle",
				$returnid,
				$admintheme->DisplayIcon("false"),
				array_merge(
					array("what" => "active", "newval" => 1),
					$toggleparams
				)
			);
		}

		// default

		if ($item->isdefault == 1){
			$item->toggledefault = $this->CreateLink($id, "toggle", $returnid, $admintheme->DisplayImage("icons/system/true.gif",lang("setfalse"),"","","systemicon"), array_merge(array("what"=>"default","newval"=>0),$toggleparams));
		}else{
			$item->toggledefault = $this->CreateLink($id, "toggle", $returnid, $admintheme->DisplayImage("icons/system/false.gif",lang("settrue"),"","","systemicon"), array_merge(array("what"=>"default","newval"=>1),$toggleparams));
		}

		return $item;

	}
