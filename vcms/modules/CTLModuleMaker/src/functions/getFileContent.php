	function getFileContent($filename){
		// returns the content of a file
		$filepath = dirname(__FILE__).DIRECTORY_SEPARATOR.$filename;
		if(file_exists($filepath)){
			$fhandle = fopen($filepath, "r+");
			$content = fread($fhandle, filesize($filepath));
			return $content;
		}else{
			return false;
		}
	}
