	function DoCheckboxes($id, $name, $choices, $selected=array(), $delimiter="<br/>"){
		// pretty much like CreateInputRadioGroup, but using checkboxes
		if(!is_array($selected))	$selected = array();
		$output = "";
		foreach($choices as $key=>$value){
			$output .= $this->CreateInputCheckbox($id, $name."[]", $value, (in_array($value, $selected)?$value:0))." ".$key.$delimiter;
		}
		return $output;
	}
