	function getWhereFromParams($params, $htmlencoded=false){
		// used for query creation and frontend advanced search action
		// transforms parameters into $where - an array of criteria:
		// $where[] = array($fieldname, $searchvalue, $comparison_type)
		$db =& $this->GetDb();
		$where = array();
		foreach($params as $key=>$value){
			if(substr($key,0,6) == "field_" && trim($value) != ""){
				$key = substr($key,6);
				if(is_array($value) && count($value)==1)	$value = $value[0];
				$compare = isset($params["compare_".$key])?$params["compare_".$key]:0;
				if($value == "__date_field"){
					if($compare != "NA"){
						// The code is such that removing any part of the time input from the form will result in using the default value
						$timeinputs = array("Hour"=>"H","Minute"=>"i","Second"=>"s","Month"=>"n","Day"=>"j","Year"=>"Y");
						$parts = array();
						foreach($timeinputs as $inputname=>$datepart){	
							$parts[] = (isset($params["date_".$key."_".$inputname]))?$params["date_".$key."_".$inputname]:date($datepart);
						}
						$value = mktime($parts[0],$parts[1],$parts[2],$parts[3],$parts[4],$parts[5]);
						$value = str_replace("\'","",$db->DBTimeStamp($value));
						
						if($compare == 5){
							$parts = array();
							foreach($timeinputs as $inputname=>$datepart){	
								$parts[] = (isset($params["date_".$key."_part2_".$inputname]))?$params["date_".$key."_part2_".$inputname]:date($datepart);
							}
							$value2 = mktime($parts[0],$parts[1],$parts[2],$parts[3],$parts[4],$parts[5]);
							$value2 = str_replace("\'","",$db->DBTimeStamp($value2));
							$where[] = array($key, array($value,$value2), $compare);
						}else{	
							$where[] = array($key, $value, $compare);
						}
					}
				}else{
					if(is_array($value)){
						foreach($value as $onevalue)	$where[] = array($key, $onevalue, 1);
					}elseif($compare == 1){
						$keywords = $this->parsekeywords($value);
						foreach($keywords as $value){
							if($htmlencoded)	$value = html_entity_decode($value);
							$value = addslashes($value);							
							$where[] = array($key, $value, $compare);
						}
					}else{
						if($htmlencoded)	$value = html_entity_decode($value);
						$value = addslashes($value);
						$where[] = array($key, $value, $compare);
					}
				}				
			}
		}		
		return $where;
	}
