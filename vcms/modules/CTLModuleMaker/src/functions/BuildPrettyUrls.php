	function BuildPrettyUrls($params, $returnid=-1){
		// transforms given params into a pretty url
		$prettyurl = $this->GetName()."/";
		if(isset($params["query"])){
			$prettyurl .= "query/".$params["query"];	
		}elseif(isset($params["alias"])){
			$prettyurl .= "detail/".$params["alias"];
		}elseif(isset($params["parent"])){
			$prettyurl .= $params["what"]."/".$params["parent"];
		}else{
			$prettyurl .= $params["what"];
		}
		if(!isset($params["alias"]) && isset($params["pageindex"]) && isset($params["nbperpage"]))	$prettyurl .= "/".$params["pageindex"]."/".$params["nbperpage"];
		$prettyurl .= "/".$returnid;
		return $prettyurl;
	}
