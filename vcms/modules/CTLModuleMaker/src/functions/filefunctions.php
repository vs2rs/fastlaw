	function plResize($fullpath, $newpath, $newwidth, $newheight=false, $transparency=false, $crop=false) {
		require "function.plresize.php";
		return $return;
	}
	
	function plGetFileInfo($filepath){
		if(!file_exists($filepath))	return false;
		require "function.plGetFileInfo.php";
		return $info;
	}
	
	function upload_checkfilename($filename, $dir){
		// if a file of that name exists, appends number to the filename
		$tmpfilename = str_replace(" ","_",$filename);
		$extension = strrchr($tmpfilename, ".");
		$cleanfilename = str_replace($extension, "", $tmpfilename);

		$destdir = $dir.$tmpfilename;
		$i = 1;
		while(file_exists($destdir)){
			$tmpfilename = $cleanfilename."_".$i.$extension;
			$destdir = $dir.$tmpfilename;
			$i++;
		}
		return $tmpfilename;
	}
	
	function plUploadFile($file, $destination="", $resize=false, $crop=false){
		global $gCms;
		$dir = $gCms->config["uploads_path"].str_replace("//","/","/".$destination."/");
		
		$filename = $this->upload_checkfilename($file["name"],$dir);
		
		if (cms_move_uploaded_file($file["tmp_name"], $dir.$filename)) {
			if($resize && !is_array($resize))	$resize = explode("x",$resize);
			if($resize && count($resize) == 2) $this->plResize($dir.$filename, '', $resize[0], $resize[1],true,$crop);
			return str_replace("//","/","/".$destination."/".$filename);
		}else{
			return false;
		}
	}
	
	function plAssignFile($filepath, $table, $itemid, $field, $thumbsize=false, $cropthumb=false){
		$tablename = cms_db_prefix().'module_'.$table;
		$filepath = str_replace("//","/",$filepath);

		if($thumbsize && !is_array($thumbsize))	$thumbsize = explode("x",$thumbsize);
		if( $filepath != "" && $thumbsize && count($thumbsize) ==2 ) {
			global $gCms;
			$basepath = $gCms->config["uploads_path"];
			$exploded = explode("/",$filepath);
			$exploded[count($exploded)-1] = "plthumb_".$exploded[count($exploded)-1];
			$thumbpath = implode("/",$exploded);
			$this->plResize($basepath."/".$filepath, $basepath."/".$thumbpath, $thumbsize[0], $thumbsize[1], true, $cropthumb);
		}

		$db = $this->GetDb();

		if( $table == $this->GetName()."_multiplefilesfields" ) {
			$newid = $db->GenID($tablename."_seq");
			$query = "INSERT INTO $tablename SET fileid=?, itemid=?, fieldname=?, filepath=?";
			$dbresult = $db->Execute( $query, array($newid, $itemid, $field, $filepath) );
		}else{
			$query = "UPDATE $tablename SET $field=? WHERE id=? LIMIT 1";
			$dbresult = $db->Execute($query,array($filepath,$itemid));
		}
		
		return $dbresult;
	}
