	function parsekeywords($string){
		// cuts the searchwords of the search form into pieces
		// bascially, this explodes the $string, but takes notice of the quotes
		
		$inside = (substr($string,0,1) == '"');
		$parts = explode('"',$string);
	
		if(count($parts) < 2){
			if($inside){
				return array(str_replace('"',"",$string));
			}else{
				return explode(" ",$string);
			}
		}
	
		$keywords = array();
		foreach($parts as $part){
			if($part != ""){
				if($inside){
					$keywords[] = $part;
				}else{
					$words = explode(" ",trim($part));
					foreach($words as $word){
						if(trim($word) != "")	$keywords[] = $word;
					}
				}
				$inside = !$inside;
			}
		}
		return $keywords;
	}

