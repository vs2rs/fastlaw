	function countsomething($tablename,$what="id",$where=array(),$wherestring=false,$wherevalues=array(),$parent=false){
		// returns the number of elements in a table corresponding to criterias
		if(!$parent && isset($where["parent"]))	unset($where["parent"]);
		$db =& $this->GetDb();
		if($wherestring){
			$wherestring = " WHERE ".$wherestring;
		}else{
			$wherestring = "";
			$wherevalues = array();
			foreach($where as $key=>$value){
				if($key == "parent"){
					$wherestring .= ($wherestring == ""?" WHERE ":" AND ")."A.parent=B.id AND B.alias=?";
				}else{
					$wherestring .= ($wherestring == ""?" WHERE ":" AND ")."A.".$key."=?";
				}
				$wherevalues[] = $value;
			}
		}
		$query = "SELECT COUNT(A.$what) ourcount FROM ".cms_db_prefix()."module_".$this->GetName()."_".$tablename." A";
		if($parent && isset($what["parent"]))	$query .= ", ".cms_db_prefix()."module_".$this->GetName()."_".$parent." B";
		$query .= $wherestring;
		$dbresult = $db->Execute($query,$wherevalues);
		if ($dbresult && $row = $dbresult->FetchRow()){
			return $row["ourcount"];
		}else{
			return 0;
		}
	}
	
	
