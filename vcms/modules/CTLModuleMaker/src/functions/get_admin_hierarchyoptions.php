	function get_admin_hierarchyoptions($end=false, $withemptyrow=true, $separator=" &gt; ", $orderbyname=false){
		// returns an array of parent options linked to a selected level
		// used for the parent and filter dropdown in the admin
		// shows empty parents too
		$levelarray = $this->get_levelarray();
		if(!in_array($end, $levelarray))	$end = "item";
		
		// building the joined query
		$tables = "";
		$fields = "";
		$orderby = "";
		$finished = false;
		$i = 1;
		foreach($levelarray as $level){
			if($level == $end)	$finished = true;
			if(!$finished){
				$tables .= ($i==1?"":" LEFT JOIN ").cms_db_prefix()."module_".$this->GetName()."_".$level." t".$i." ";
				if($i>1)	$tables .= "ON t".($i-1).".id = t".$i.".parent ";
				$fields .= ($fields == ""?"":", ")."t".$i.".id id".$i.", t".$i.".name name".$i;
				if($orderbyname){
					$orderby .= ($orderby == ""?"":", ")."t".$i.".name";
				}else{
					$orderby .= ($orderby == ""?"":", ")."t".$i.".item_order, t".$i.".name";
				}
				$i++;
			}
		}
		if($fields == "")	return array();
		
		$db =& $this->GetDb();
		$query = "SELECT ".$fields." FROM ".$tables." ORDER BY ".$orderby;
		$dbresult = $db->Execute($query);
		$options = array();
		while($dbresult && $row = $dbresult->FetchRow()){
			// each row has a full hierarchy, from top parent to final child
			$value = $row["id".($i-1)];
			$j = 1;
			$option_name = "";
			while($j < $i){
				$option_name .= ($j==1?"":$separator).$row["name".$j];
				$j++;
			}
			$options[$option_name] = $value;
		}
		if($withemptyrow)	$options[""] = "";
		return $options;
	}
