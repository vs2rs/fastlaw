	function get_pageid($alias){
		// returns the page id from an alias
		global $gCms;
		$manager =& $gCms->GetHierarchyManager();
		$node =& $manager->sureGetNodeByAlias($alias);
		if (isset($node)) {
			$content =& $node->GetContent();	
			if (isset($content))	return $content->Id();
		}else{
			$node =& $manager->sureGetNodeById($alias);
			if (isset($node)) return $alias;
		}
	}
