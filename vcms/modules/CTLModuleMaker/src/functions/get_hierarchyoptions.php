	function get_hierarchyoptions($end=false, $withemptyrow=true, $depthsymbol="&nbsp;-&nbsp;"){
		// returns an array of parent options for a linked to a selected level
		// used for the hierarchy dropdown list in search action
		$levelarray = $this->get_levelarray();
		 if(!in_array($end, $levelarray))	$end = $levelarray[count($levelarray)-1];
		
		// building the joined query
		$tables = "";
		$fields = "";
		$orderby = "";
		$where = "";
		$finished = false;
		$i = 1;
		foreach($levelarray as $level){
			if($level == $end)	$finished = true;
			if(!$finished){
				$tables .= ($tables == ""?"":", ").cms_db_prefix()."module_".$this->GetName()."_".$level." t".$i." ";
				$fields .= ($fields == ""?"":", ")."t".$i.".id id".$i.", t".$i.".name name".$i;
				$orderby .= ($orderby == ""?"":", ")."t".$i.".item_order DESC";
				if($i > 1) $where .= " AND t".($i-1).".id = t".$i.".parent ";
				$where .= ($where == ""?"":" AND ")." t".$i.".active=1";
				$i++;
			}
		}
		if($fields == "")	return false;
		
		$db =& $this->GetDb();
		$query = "SELECT ".$fields." FROM ".$tables." WHERE ".$where." ORDER BY ".$orderby;
		$dbresult = $db->Execute($query);

		// parsing results
		$current = array();
		$tmplastlevel = array();
		$results = array();
		$final = $i -1;
		while($j < $final ){
			$current[$j] = array("id"=>false,"name"=>"");
			$j++;
		}
		while($dbresult && $row = $dbresult->FetchRow()){
			// each row has a full hierarchy, from top parent to final child
			$j = $final;
			while($j > 0 ){
				if($row["id".$j] != $current[$j]["id"]){
					if($current[$j]["id"] && count($current[$j]["res"]) > 0){
						$key = str_repeat($depthsymbol, ($j -1)).$current[$j]["name"];
						$results[$key] = implode(",",$current[$j]["res"]);
					}
					$current[$j] = array("id"=>$row["id".$j], "name"=>$row["name".$j], "res"=>array());
				}
				$current[$j]["res"][] = $row["id".$final];
				$j--;
			}
			$results[str_repeat($depthsymbol, ($final -1)).$row["name".$final]] = $row["id".$final];
		}
		// we close the remaining sets
		$j = $final -1;
		while($j > 0 ){
			$key = str_repeat($depthsymbol, ($j -1)).$current[$j]["name"];
			$results[$key] = implode(",",$current[$j]["res"]);
			$j--;
		}
		if($withemptyrow)	$results[""] = "";
		return array_reverse($results);
	}
