	function get_distancetolevel($parentname,$childname=false){
		// get the distance between two levels (most likely between a level and the final level)
		$levels = $this->get_levelarray();
		if(!$childname)	$childname = $levels[count($levels)-1];
		$parentposition = false;
		$childposition = false;
		$counter = 0;
		foreach($levels as $level){
			$counter++;
			if($level == $parentname) $parentposition = $counter;
			if($level == $childname) $childposition = $counter;
		}
		if($childposition && $parentposition){
			return abs($parentposition - $childposition);
		}
	}
