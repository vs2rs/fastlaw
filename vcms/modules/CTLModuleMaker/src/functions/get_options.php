	function get_options($tablename, $orderby="item_order", $onlyactive=false, $fullobject=false){
		// returns the elements of any table as options
		// to use for a dropdown list
		// returns an array of $label=>$value
		$db =& $this->GetDb();
		$query = "SELECT ".($fullobject?"*":"id, name")." FROM ".cms_db_prefix()."module_".$this->GetName()."_".$tablename;
		if($onlyactive)	$query .= " WHERE active=1";
		$query .= " ORDER BY $orderby";
		$dbresult = $db->Execute($query);
		$options = array();
		while ($dbresult && $row = $dbresult->FetchRow()){
			if($fullobject){
				$obj = new StdClass();
				foreach($row as $key=>$value)	$obj->$key = $value;
				$options[] = $obj;
			}else{
				$options[$row["name"]] = $row["id"];
			}
		}
		
		return $options;
	}
