	function addfrontendurls($item,$params,$id,$returnid){
		// adds $item->detaillink and $item->detailurl to a given $item
		$nextlevel = $this->get_nextlevel($item->__what);
		if(!$nextlevel){
			$newparams = array("what"=>$item->__what, "alias"=>$item->alias);
		}else{
			$newparams = array("what"=>$nextlevel, "parent"=>$item->alias);
			if(isset($params["forcelist"]) && $params["forcelist"])	$newparams["forcelist"] = true;
			if(isset($params["nbperpage"]) && $params["nbperpage"] > 0)	$newparams["nbperpage"] = $params["nbperpage"];
			if(isset($params["pageindex"]) && $params["pageindex"] > 1)	$newparams["pageindex"] = $params["pageindex"];
		}
		$inline = (isset($params["inline"]) && $params["inline"])?true:false;
		if($inline)	$newparams["inline"] = true;
		$item->detaillink = $this->CreateLink($id, "default", $returnid, $item->name, $newparams, "", false, $inline, "", false, $this->BuildPrettyUrls($newparams, $returnid));
		$item->detailurl = $this->CreateLink($id, "default", $returnid, "", $newparams, "", true, $inline, "", false, $this->BuildPrettyUrls($newparams, $returnid));
		return $item;
	}
