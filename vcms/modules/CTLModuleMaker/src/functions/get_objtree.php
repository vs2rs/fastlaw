	function get_objtree($curid, $curlevel=false, $field="id"){
		// this builds an object tree ($item->parent_object->parent_object...)
		// we first put all the parents in an array
		$parents = array();
		$levels = $this->get_levelarray();
		if(!$curlevel)	$curlevel = $levels[count($levels)-1];
		$i = count($levels);
		$started = false;
		while($curid && $i > 0){
			if($levels[$i -1] == $curlevel)	$started = true;
			if($started){
				$getfunction = "get_level_".$levels[$i -1];
				$item = $this->$getfunction(array($field=>$curid));
				$item = is_array($item)?$item[0]:$item;
				$item->__what = $levels[$i-1];
				$parents[] = $item;
				$field = "id";
				$curid = isset($item->parent_id)?$item->parent_id:false;
			}
			$i--;
		}
		
		// next, we process the array of parents to build the parent tree
		$parenttree = false;
		$i = count($parents) - 1;
		while($i >= 0){
			if($parenttree){
				$newtree = $parents[$i];
				$newtree->parent_object = $parenttree;
				$parenttree = $newtree;
			}else{
				$parenttree = $parents[$i];
			}
			$i--;
		}
		$this->currenttree = $parenttree;
		return $parenttree;
	}
