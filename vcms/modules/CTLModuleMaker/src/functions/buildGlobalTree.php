	function buildGlobalTree(){
		// this saves the current tree in the module...
		$levels = $this->get_levelarray();
		$glob = $this->get_moduleGetVars();
		if(isset($glob["pageindex"]))	$this->currentpageindex = $glob["pageindex"];
		if(isset($glob["alias"])){
			$tree = $this->get_objtree($glob["alias"], $levels[count($levels)-1], "alias");
		}elseif(isset($glob["parent"]) && isset($glob["what"])){
			$tree = $this->get_objtree($glob["parent"], $this->get_nextlevel($glob["what"],false), "alias");
		}
		
		if(isset($tree) && $tree){
			$this->currenttree = $tree;
			// this saves the selected item for each level
			$this->plcurrent[$tree->__what] = $tree->alias;
			while(isset($tree->parent_object)){
				$tree = $tree->parent_object;
				$this->plcurrent[$tree->__what] = $tree->alias;
			}
		}
	}
