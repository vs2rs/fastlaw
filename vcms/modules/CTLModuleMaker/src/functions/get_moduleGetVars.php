	function get_moduleGetVars(){
		// unorthodox hack so that different calls of the module speak with each other
		// basically, we retrieve parameters in the url that were meant for other instances of the module
		// see FAQ for more info on this
		global $_GET;
		global $gCms;
		$redirection = false;
		if(isset($gCms->config["url_rewriting"])){
			// core 1.6 or more
			$redirection = $gCms->config["url_rewriting"];
		}else{
			// core below 1.6
			if($gCms->config["assume_mod_rewrite"] && isset($_GET["page"])){
				$redirection = "mod_rewrite";
			}elseif(!$gCms->config["assume_mod_rewrite"] && $gCms->config["internal_pretty_urls"] && isset($_SERVER["REQUEST_URI"])){
				$redirection = "internal";
			}
		}
		$params = array();
		$globalmodulevars = array();
		if(isset($_GET["mact"])){
			// if we aren't using pretty urls...
			$modinfo = explode(",",$_GET["mact"]);
			if(isset($modinfo) && $modinfo[0] == $this->GetName()){
				if(isset($_GET[$modinfo[1]."parent"]))
					$globalmodulevars["parent"]=$_GET[$modinfo[1]."parent"];
				if(isset($_GET[$modinfo[1]."what"]))
					$globalmodulevars["what"]=$_GET[$modinfo[1]."what"];
				if(isset($_GET[$modinfo[1]."alias"]))
					$globalmodulevars["alias"]=$_GET[$modinfo[1]."alias"];
				if(isset($_GET[$modinfo[1]."pageindex"]))
					$globalmodulevars["pageindex"]=$_GET[$modinfo[1]."pageindex"];
			}
		}elseif($redirection == "mod_rewrite" || $redirection == "internal"){
			$params = array();
			if($redirection == "mod_rewrite" && isset($_GET["page"])){
				// if we are using an external mod_rewrite, assuming you are using the very
				// basic rewrite which puts the module informations inside the page variable
				$parts = explode("/",$_GET["page"]);
				foreach($parts as $part){
					if($part != "")	$params[] = $part;
				}
			}elseif($redirection == "internal"){
				// if we are using the internal pretty urls
				$parts = explode("/",$_SERVER["REQUEST_URI"]);
				$started = false;
				foreach($parts as $part){
					if($started && $part != "")	$params[] = $part;
					if(strtolower($part) == "index.php")	$started = true;
				}
			}
			if(isset($params[0]) && strtolower($params[0]) == strtolower($this->GetName())){
				 // we are in a module action
				 // this part should be changed if you change the pretty urls structure
				if(!isset($params[1]) || strtolower($params[1]) == "query"){

				}elseif(isset($params[1]) && strtolower($params[1]) == "detail"){
					$levels = $this->get_levelarray();
					$globalmodulevars["what"] = $levels[count($levels)-1];
					$globalmodulevars["alias"] = $params[2];
				}else{
					$globalmodulevars["what"] = $params[1];
					switch(count($params)){
						case 6:
							$globalmodulevars["pageindex"] = $params[3];
							$globalmodulevars["nbperpage"] = $params[4];
						case 4:
							$globalmodulevars["parent"] = $params[2];
							break;
						case 5:
							$globalmodulevars["pageindex"] = $params[2];
							$globalmodulevars["nbperpage"] = $params[3];
							break;
					}
				}
			}
		}
		return $globalmodulevars;
	}
