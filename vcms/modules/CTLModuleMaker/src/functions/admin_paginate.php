	function admin_paginate($level,$nbperpage,$id,$returnid,$params){
		// same as paginate, but for the admin panels
		$whereclause = array();
		if(isset($params[$level."_showonly"]) && $params[$level."_showonly"] != "")	$whereclause["parent"] = $params[$level."_showonly"];
		$total = $this->countsomething($level,"id",$whereclause);
		$nextpage = false;
		$previouspage = false;
		$pageinfo = false;
		$pages = false;
		if($nbperpage > 0 && $total > $nbperpage){
			$curpage = isset($params[$level."_page"])?$params[$level."_page"]:0;
			$nbpages = ceil($total/$nbperpage);
			$pageinfo = (($curpage * $nbperpage) + 1);
			$tmpend = $pageinfo + $nbperpage - 1;
			if($tmpend > $total)	$tmpend = $total;
			if($tmpend != $pageinfo)	$pageinfo .= "-".$tmpend;
			$pageinfo .= " / ".$total;
			$pageparams = array("active_tab"=>$level);
			if(isset($params[$level."_showonly"]) && $params[$level."_showonly"] != ""){
				$pageparams[$level."_showonly"] = $params[$level."_showonly"];
			}
			if($curpage > 0){
				$pageparams[$level."_page"] = $curpage - 1;
				$previouspage = $this->CreateLink($id, "defaultadmin", $returnid, $this->Lang("previouspage"), $pageparams);
			}
			if($curpage < ($nbpages-1)){
				$pageparams[$level."_page"] = $curpage + 1;
				$nextpage = $this->CreateLink($id, "defaultadmin", $returnid, $this->Lang("nextpage"), $pageparams);
			}
			$i = 0;
			$pages = $this->Lang("pages");
			while($i < $nbpages){
				if($i > 0)	$pages .= "&nbsp; ";
				if($i == $curpage){
					$pages .= $i;
				}else{
					$pageparams[$level."_page"] = $i;
					$pages .= $this->CreateLink($id, "defaultadmin", $returnid, $i, $pageparams);
				}
				$i++;
			}
		}
		return array($pageinfo, $nextpage, $previouspage, $pages);
	}
