	function get_fieldoptions($field,$fullobject=false){
		// returns field options as options to use for a dropdown list
		$db =& $this->GetDb();
		$query = "SELECT * FROM ".cms_db_prefix()."module_".$this->GetName()."_fieldoptions WHERE field=? ORDER BY item_order";
		$dbresult = $db->Execute($query,array($field));
		$options = array();
		while ($dbresult && $row = $dbresult->FetchRow()){
			if($fullobject){
				$obj = new StdClass();
				foreach($row as $key=>$value)	$obj->$key = $value;
				$options[] = $obj;
			}else{
				$options[$row["name"]] = $row["id"];
			}
		}
		
		return $options;
	}
