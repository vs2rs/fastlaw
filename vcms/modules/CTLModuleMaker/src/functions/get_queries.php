	function get_queries($where=array(), $admin=false, $id=false, $returnid=false){
		// retrives queries from the database and returns an array of objects
		global $gCms;
		$db =& $this->GetDb();
		$query = "SELECT * FROM ".cms_db_prefix()."module_".$this->GetName()."_saved_queries";
		$whereclause = "";
		$wherevalues = array();
		if(count($where)>0){
			foreach($where as $key=>$value){
				$whereclause .= ($whereclause == ""?" WHERE ":" AND ").$key."=?";
				$wherevalues[] = $value;
			}
		}
		$query .= $whereclause;
		$dbresult = $db->Execute($query,$wherevalues);
		$itemlist = array();
		while ($dbresult && $row = $dbresult->FetchRow()){
			$item = new stdClass();
			foreach($row as $key=>$value){
				$item->$key = $value;
			}
			$item->wherevalues = unserialize($item->wherevalues);
			if(!is_array($item->values))	$item->values = array();
			if($admin){
				$item->actions = $this->CreateLink($id, "deletequery", $returnid, $gCms->variables["admintheme"]->DisplayImage("icons/system/delete.gif",lang("delete"),"","","systemicon"), array("queryid"=>$item->id));
				$item->actions .= $this->CreateLink($id, "testquery", $returnid, $gCms->variables["admintheme"]->DisplayImage("icons/system/view.gif",lang("view"),"","","systemicon"), array("queryid"=>$item->id));
			}
			$itemlist[] = $item;
		}
		return (count($itemlist)>0?$itemlist:false);
	}
