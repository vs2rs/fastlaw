	function get_parents($parentname,$childname,$childid){
		// when using the sharechildren option, retrieves the parents of a given element
		$db =& $this->GetDb();
		$query = "SELECT ".$parentname."_id parentid FROM ".cms_db_prefix()."module_".$this->GetName()."_".$parentname."_has_".$childname." WHERE ".$childname."_id=?";
		$dbresult = $db->Execute($query,array($childid));
		$parents = array();
		while ($dbresult && $row = $dbresult->FetchRow()){
			$parents[] = $row["parentid"];
		}
		return $parents;
	}
