	function getaddfiles($fieldname,$itemid,$admin=false,$addthumb=false){
		// return the file objects for fields of undefined amount of files
		global $gCms;
		if($admin)	$admintheme = $gCms->variables["admintheme"];
		$db =& $this->GetDb();
		$query = "SELECT fileid, filepath FROM ".cms_db_prefix()."module_".$this->GetName()."_multiplefilesfields WHERE itemid=? AND fieldname=?";
		$dbresult = $db->Execute($query, array( $itemid, $fieldname ));
		$files = array();
		while ($dbresult && $row = $dbresult->FetchRow()){
			$file = new stdClass();
			$file->fileid = $row["fileid"];
			$file->filepath = (substr($row["filepath"],0,1)=="/"?"":"/").$row["filepath"];
			$file->url = $gCms->config["uploads_url"].$file->filepath;
			$info = $this->plGetFileInfo($gCms->config["uploads_path"].$file->filepath);
			if($info)	foreach($info as $key=>$value)	$file->$key = $value;
			if($file->fileicon == "icons/filetypes/fpaint.gif")	$file->image = '<img src="'.$file->url.'" alt=""/>';
			if($admin)	$file->pic = $admintheme->DisplayImage($file->fileicon);
			if($addthumb)	$file->thumbnail = preg_replace("/(.+)?\\/([^\\/]+)\\.(.+)/","$1/plthumb_$2.$3",$file->url);
			array_push($files, $file);
		}
		return $files;
	}
