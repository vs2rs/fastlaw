	function get_nextlevel($curlevel,$findchild=true){
		// return the name of the level below ($findchild=true) or above ($findchild=false)
		$levels = $this->get_levelarray();
		$i = 0;
		$wantedlevel = false;
		while($i < count($levels)){
			$next = $findchild?$i+1:$i-1;
			if($levels[$i] == $curlevel && isset($levels[$next])) $wantedlevel = $levels[$next];
			$i++;
		}
		return $wantedlevel;
	}
