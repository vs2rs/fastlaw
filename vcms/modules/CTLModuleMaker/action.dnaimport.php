<?php
if(!isset($gCms)) exit;

$errors = 0;

if(isset($params['localfile'])){
	$content = file_get_contents(dirname(__FILE__).DIRECTORY_SEPARATOR.'autosaved'.DIRECTORY_SEPARATOR.$params['localfile']);
}elseif(isset($_FILES) && isset($_FILES[$id . 'uploadfile']) && $_FILES[$id . 'uploadfile']['name'] != ''){
	$content = file_get_contents($_FILES[$id . 'uploadfile']['tmp_name']);
}else{
	$errors++;
}

if($errors == 0){
	$cntarray = unserialize($content);
	if (is_array($cntarray) && count($cntarray) == 0)	$errors++;
	if(!is_array($cntarray) || !isset($cntarray['infos']) || !isset($cntarray['levels'])) $errors++;
}

if($errors > 0){
	$this->Redirect($id, 'defaultadmin', $returnid, array('module_message'=>$this->Lang('errorwithrequest').': '.$this->Lang('error_dnafile')));
}else{
	$filepath = 'temp'.DIRECTORY_SEPARATOR.'temp_'.(isset($cntarray['infos']['nameofmodule'])?$cntarray['infos']['nameofmodule']:$cntarray['infos']['name']);
	
	if($this->GetPreference('usesessions',false)){
		$_SESSION['CTLMM'] = array("infos"=>$cntarray["infos"], "levels"=>$cntarray["levels"]);
		$_SESSION['CTLMM']['olddna'] = $cntarray;
		$params = array();	
		if(isset($cntarray["step"]))	$params["step"] = $cntarray["step"];
		if(isset($cntarray["substep"]))	$params["substep"] = $cntarray["substep"];
		if(isset($cntarray["currentlevel"]))	$params["currentlevel"] = $cntarray["currentlevel"];
	}else{
		$this->CreateFile(dirname(__FILE__).DIRECTORY_SEPARATOR.$filepath,$content);
		$cntarray['infos']['tempfile'] = $filepath;
		$params = $cntarray;
		$params['infos'] = serialize($params['infos']);
		$params['levels'] = serialize($params['levels']);
	}
	
	if(isset($params['step'])){
		$params['modimport'] = true;
		$this->Redirect($id, 'creation', $returnid, $params);
	}else{
		echo '<h1>'.$this->Lang('title').'</h1><br/>';
		echo '<h2>'.$this->Lang('importmoduledna').'</h2><br/>';
		echo '<p>'.$this->Lang('prompt_import').'</p>';
		echo '<p>'.$this->CreateFormStart($id, 'createModule', $returnid, 'POST').$this->CreateInputSubmit($id, 'createmod', $this->Lang('createit'));
		if(!$this->GetPreference('usesessions',false)){
			echo $this->CreateInputHidden($id, 'levels', $params['levels']).$this->CreateInputHidden($id, 'infos', $params['infos']);
		}
		echo $this->CreateFormEnd().'</p>';
		echo '<p>'.$this->CreateFormStart($id, 'creation', $returnid, 'POST').$this->CreateInputSubmit($id, 'modimport', $this->Lang('modifyit'));
		if(!$this->GetPreference('usesessions',false)){
			echo $this->CreateInputHidden($id, 'levels', $params['levels']).$this->CreateInputHidden($id, 'infos', $params['infos']);
		}
		echo $this->CreateInputHidden($id, 'step', 2).$this->CreateInputHidden($id, 'currentlevel', 1);
		echo $this->CreateFormEnd().'</p>';
		if($this->GetPreference('doupgrade', true))	echo '<p>('.$this->Lang('prompt_importupgrade').')</p>';
	}
}


?>
