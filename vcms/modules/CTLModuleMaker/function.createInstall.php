<?php
if(!isset($levels) || !isset($linktables)) exit;

$installmethod = '<?php
if(!isset($gCms)) exit;

// Typical Database Initialization
$db = &$this->cms->db;
$dict = NewDataDictionary($db);
		
// mysql-specific, but ignored by other database
$taboptarray = array("mysql" => "TYPE=MyISAM");
		
';

$leveldepth = 0;
$nblevel = count($levels);

foreach($levels as $level){
// ###################################### BEGINNING OF LEVEL

$leveldepth++;
$installmethod .= '
// Creates the '.$level[0].' table
$flds = "';

$firstfield = true;
foreach($level[4] as $field){
	$fieldtype = $this->GetDBFieldType($field[1], isset($field[5]['listmode'])?$field[5]['listmode']:false);
	if($field[0] == "id") $fieldtype = "I NOTNULL AUTOINCREMENT KEY";
	if($field[0] == "active" || $field[0] == "isdefault") $fieldtype = "L";

	if(!in_array($field[1],array(11,13))){
		$installmethod .= ($firstfield?"":",").'
	'.$field[0].' '.$fieldtype;
		$firstfield = false;
	}
}
$installmethod .= ',
    date_modified ".CMS_ADODB_DT.",
	date_created ".CMS_ADODB_DT."
	";

$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_'.$modulename.'_'.$level[0].'", $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);

';
// ###################################### END OF LEVEL
}

foreach($linktables as $linktable){

$installmethod .= '
// Creates the link table '.$linktable[0].'_has_'.$linktable[1].'
$flds = "
	'.$linktable[0].'_id I,
	'.$linktable[1].'_id I
	";

$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_'.$modulename.'_'.$linktable[0].'_has_'.$linktable[1].'", $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);

';

}

if(count($listfields) > 0){

$installmethod .= '
// Create the fieldoptions table
$flds = "
	id I,
	field C(128),
	name C(32),
	item_order I
	";
$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_'.$modulename.'_fieldoptions", $flds, $tabopt);
$dict->ExecuteSQLArray($sqlarray);
$db->CreateSequence(cms_db_prefix()."module_'.$modulename.'_fieldoptions_seq");

';

}


if(count($multiplefiles) > 0) {

$installmethod .= '
// Creates the table for multiple files
$flds = "
    fileid I,
	itemid I,
	fieldname C(64),
	filepath C(255)
	";

$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_'.$modulename.'_multiplefilesfields", $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);
$db->CreateSequence(cms_db_prefix()."module_'.$modulename.'_multiplefilesfields_seq");


';

}

$installmethod .= '

// CREATING PERMISSIONS :

// permissions
$this->CreatePermission("module_'.$modulename.'_advanced", "Module - '.$modulfriendlyname.' (Advanced)");';

$defprefs = '';
foreach($levels as $level){
	$installmethod .= '
	$this->CreatePermission("module_'.$modulename.'_manage_'.$level[0].'", "Module - '.$modulfriendlyname.' ('.ucfirst($level[0]).')");';
}

$installmethod .= '
// prepare information for an eventual upgrade
$this->SetPreference("makerversion","'.$this->GetVersion().'");

// put mention into the admin log
$this->Audit( 0, $this->Lang("friendlyname"), $this->Lang("installed",$this->GetVersion()));

?'.'>
';

?>
