<?php

// set the post values
$add_new_post_level = "add-new-".$post_level."-level";
$add_new_post_name = "add-new-".$post_level."-name";
$add_new_post_parent = "add-new-".$post_level."-parent";

if(isset($_POST[$add_new_post_level])
&& isset($_POST[$add_new_post_name])) {

	$addNewError = false;

	// if we have a parent so we need to check some shit
	if(isset($_POST[$add_new_post_parent])) {
		if($_POST[$add_new_post_parent] == "") { // empty
			$addNewError = $this->ShowErrors($this->lang("error_missginvalue") . " (1)");
		}
		if(floor($_POST[$add_new_post_parent]) <= 0) { // not valid
			$addNewError = $this->ShowErrors($this->lang("error_missginvalue") . " (2)");
		}
		// todo -> we should check if this parent exists in db
	}

	// check if values set
	if($_POST[$add_new_post_level] == "" ||
	$_POST[$add_new_post_name] == "") {
		if($_POST[$add_new_post_level] == "") {
			$addNewError = $this->ShowErrors($this->lang("error_missginlevel") . " (3)");
		}
		if($_POST[$add_new_post_name] == "") {
			$addNewError = $this->ShowErrors($this->lang("error_missginvalue"));
		}
	}

	// if errors
	if($addNewError) {
		
		echo $addNewError;

	} else {

		// all good
		
		$newItem = new stdClass();
		$newItem->name = $_POST[$add_new_post_name];

		// check if parent
		if(isset($_POST[$add_new_post_parent])) {
			$newItem->parent = floor($_POST[$add_new_post_parent]);
		}

		// set the level
		$level_name = $_POST[$add_new_post_level];
		$level_fileds_function = "get_level_".$level_name."_fields";
		$level_get_function = "get_level_".$level_name;

		// do we have it in the module class
		if(method_exists($this, $level_fileds_function)) {
			
			$level_vars   = $this->$level_fileds_function();
			$level_name   = $level_vars["name"];
			$level_table  = $level_vars["table"];
			$level_prefix = $level_vars["prefix"];

			// set alias
			$newItem->alias = $this->plcreatealias($newItem->name);
			$alias_errors = false;

			// check if we auto increase alias if identical
			$autoincrementalias = $this->GetPreference("autoincrement_alias", false);

			if($this->checkalias("module_" . $level_table, $newItem->alias)) {
				if($autoincrementalias) {
					$alias_i = 1;
					$alias_base = $newItem->alias;
					$alias_temp = $newItem->alias . "-" . $alias_i;
					while(!$this->checkalias("module_" . $level_table, $alias_temp)) {
						$alias_temp = $alias_base . "-" . $alias_i;
						$alias_i++;
					}
					$newItem->alias = $alias_temp;
				} else {
					$alias_errors = true;
				}
			} 

			if($alias_errors == false) {

				// query
				$query = "INSERT INTO ".cms_db_prefix()."module_".$level_table." SET 
					name = ?,
					alias = ?,
					item_order = ?,
					date_created = ?,
					date_modified = ?,
					active = 1,
					isdefault = 0";

				// if parent
				if(isset($newItem->parent)) {
					$query .= ", parent = " . $newItem->parent;
				}

				// setup item order
				$newItem->item_order = $this->updateItemOrder($level_name, $level_table, $newItem);

				$db->Execute($query, array(
					$newItem->name,
					$newItem->alias,
					$newItem->item_order,
					str_replace("'","",$db->DBTimeStamp(time())),
					str_replace("'","",$db->DBTimeStamp(time()))
				));

				if($db->Affected_Rows()){

					// get id
					$newItemId = $db->Insert_ID();
					
					// send the event
					$this->Audit(
						$newItemId,
						$this->GetFriendlyName(),
						$this->Lang($level_name) . " Added"
					);

					// get the list of galleries

					$newItemGalleries = $this->$level_fileds_function("gallery");

					foreach ($newItemGalleries as $gallery_path) {

						// create gallery folder
						$gallery_path = $config['uploads_path'] . $gallery_path;
						if(file_exists($gallery_path) && is_writable($gallery_path)) {
							$itemGalleryfolder = $gallery_path . $newItemId;
							mkdir($itemGalleryfolder, 0755);
						}

					}

					// redirect

					if(!$returnid) {
						$returnid = 0;
					}

					header('Location: ' .
						'moduleinterface.php?' .
						'mact='.$this->GetName().','.$id.',edit'.$level_prefix.','.$returnid.
						'&'.$id.$level_prefix.'id='.$newItemId
					);

					exit;

				}

			} else {

				echo $this->ShowErrors($this->lang("error_alreadyexists"));

			}

		} else {

			echo $this->ShowErrors($this->lang("error_missginlevel") . " (4)");

		}

	}

}

?>