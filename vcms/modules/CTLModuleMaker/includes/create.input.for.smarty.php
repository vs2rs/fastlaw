<?php
	
	// createInputForSmarty($input)
	// wx function
	
	// database
	$db =& $this->GetDb();

	$result = new stdClass();

	$input_id = $input["id"];
	$input_name = $input["name"];
	$input_settings = $input["settings"];
	$input_value = $input["value"];
	$level_name = $input["level_name"];
	$level_prefix = $input["level_prefix"];

	// set input label
	if($input_settings["type"] != "hidden") {

		// get the param from language file
		$result->label = $this->Lang($level_name . "_" . $input_name);
		$result->type = $input_settings["type"];
		
		// alias and name
		if($input_name == "name" || $input_name == "alias") {
			
			// no custom param, use default from lang
			if(substr($result->label, 0, 6) == "-- Add") {
				$result->label = $this->Lang($input_name);
			}

		}

	}

	// setup readonly
	if(isset($input_settings["readonly"])
	&& $input_settings["readonly"] === true) {
		$readonly = "readonly";
	} else {
		$readonly = "";
	}

	// add some extra on event to the input tag
	if(isset($input_settings["event"])) {
		$event = $input_settings["event"];
	} else {
		$event = '';
	}

	if($input_settings["type"] == "input"
	|| $input_settings["type"] == "file"
	|| $input_settings["type"] == "image") {

		$result->element = $this->CreateInputText(
			$input_id,
			$level_prefix . $input_name,
			$input_value,
			isset($input_settings["size"]) ? $input_settings["size"] : 50,
			isset($input_settings["length"]) ? $input_settings["length"] : 255,
			$readonly . $event
		);

	} else if($input_settings["type"] == "hidden") {
		
		$result->element = $this->CreateInputHidden(
			$input_id,
			$level_prefix . $input_name,
			$input_value
		);

	} else if($input_settings["type"] == "text") {

		// class name for editor
		$textEditorClass = "";
		if(isset($input_settings["editor"])
		&& $input_settings["editor"] === true) {
			$textEditorClass = "text-editor";
		}

		// check for specific class
		if(isset($input_settings["class"])
		&& $input_settings["class"] !== false
		&& $input_settings["class"] != "") {
			$textEditorClass = $input_settings["class"];
		}

		// add extra stuff
		$textEditorExtra = isset($input_settings["height"]) ? 'style="height: ' . $input_settings["height"] . 'px;"' : "";

		// check for specific folder for images set
		if(isset($input_settings["folder"])
		&& $input_settings["folder"] !== false
		&& $input_settings["folder"] != "") {
			$textEditorExtra .= ' data-folder="' . $input_settings["folder"] . '"';
		}

		$result->element = $this->CreateTextArea(
			false,
			$input_id,
			$input_value,
			$level_prefix . $input_name,
			$textEditorClass,
			"", "", "", 80, 15, "", "",
			$textEditorExtra
		);

	} else if($input_settings["type"] == "date") {

		$date = new DateTime();
		if($input_settings["time"] && $input_settings["time"]) {
			$date = $date->getTimestamp();
		} else {
			$date = $date->setTime(0,0)->getTimestamp();
		}

		$result->element = $input["value"] != ''
						 ? $db->UnixTimeStamp($input["value"])
						 : $date;

	} else if($input_settings["type"] == "checkbox") {

		// check input value
		if($input_value == "") {
			// check for default value
			$input_value = isset($input_settings["checked"]) && $input_settings["checked"] == 1 ? 1 : 0;
		}
		
		// set checked or not
		$is_checked = $input_value == 1 ? " checked" : "";
		
		$result->element = '
			<div class="module-checkbox">
				<span class="module-checkbox__check">';
					
					$result->element .= '<input type="hidden" name="' . $input_id . $level_prefix . $input_name . '" value="'.$input_value.'" />';
					$result->element .= '<input id="checkbox_' . $input_id . $level_prefix . $input_name . '"
						   name="checkbox_' . $input_id . $level_prefix . $input_name . '"
						   type="checkbox" value="'.$input_value.'"
						   onchange="this.previousSibling.value=1-this.previousSibling.value;"'.$is_checked.' />';
					
					$result->element .= '<label for="checkbox_' . $input_id . $level_prefix . $input_name . '"></label>

				</span>
				<span class="module-checkbox__label">' . $result->label . '</span>
			</div>
		';

	} else if($input_settings["type"] == "select") {

		// check if options are set
		if(isset($input_settings["options"])
		&& is_array($input_settings["options"])) {

			// if on change event is set
			if(isset($input_settings["onchange"])
			&& $input_settings["onchange"]
			&& $input_settings["onchange"] != "") {
				$input_onchange = ' onchange="'.$input_settings["onchange"].'"';
			} else {
				$input_onchange = '';
			}

			// setup select
			$result->element = '<select name="' . $input_id . $level_prefix . $input_name . '"'.$input_onchange.' data-value="'.$input_value.'">';
			

			if(count($input_settings["options"]) > 0) {

				// setup options
				foreach ($input_settings["options"] as $option_value => $option_name) {
					
					$selected = $input_value == $option_value ? ' selected' : '';
					
					$result->element .= '<option value="'.$option_value.'"'.$selected.'>';
					$result->element .= $option_name;
					$result->element .= '</option>';

				}

			}

			// end of select
			$result->element .= '</select>';

		} else {

			// other wise return nothing
			$result->element = '';

		}

	} else if($input_settings["type"] == "gallery") {

		// set the folder
		$input_settings["options"]["folder"] =
		$input_settings["options"]["folder"] . $input["item_id"] . '/';

		$result->element = setup_dropzone(
			// the id of the gallery that is used in db
			$this->GetName() . '_' . $level_name . "_" . $input_name . "_id" . $input["item_id"],
			// all the options from module settings
			$input_settings["options"]
		);

	}

	// check if we have comments
	if(isset($input_settings["comment"]) && $input_settings["comment"] != "" && $input_settings["comment"] !== false) {
		$result->comment = $input_settings["comment"];
	}

	// setup image upload
	if($input_settings["type"] == "image"
	|| $input_settings["type"] == "file") {

		// check if params are set
		if(isset($input_settings["params"])) {
			$params = $input_settings["params"];
		} else {
			$params = false;
		}

		// setup element
		$result->element = setup_upload_form(
			$input_settings["type"],
			array($result->label, $result->element, isset($result->comment) ? $result->comment : ""),
			$input_settings["folder"],
			$input_value, $input_id . $level_prefix . $input_name,
			$params
		);

	}

	// return $result in the funciton

?>