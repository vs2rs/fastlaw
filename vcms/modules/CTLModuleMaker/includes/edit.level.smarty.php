<?php

// ------------------------------------
// -- define tabs
// ------------------------------------

$input_list = array();

// facebook image
$facebook_image = false;

foreach ($input_fields as $input_name => $input_params) {

	// add only if params set
	if($input_params) {

		// update parent element
		if($input_name == 'parent'
		&& isset($item)
		&& isset($item->parent)) {
			$input_params['selected'] = $item->parent;
		}

		// check if name is defined in the params list
		// if not then key is used as input name
		if(!isset($input_params['name'])) {
			$input_params['name'] = $id . $level_prefix . $input_name;
		}

		// setup label
		if(!isset($input_params['label'])) {
			$input_params['label'] = $this->Lang($level_name . "_" . $input_name);
		}

		// check if comment set in params
		if(!isset($input_params['comment'])) {

			// get comment from the lang file
			$input_params['comment'] = $this->Lang($level_name . "_" . $input_name . "_comment");

			// check if it was set in the lang file
			if(substr($input_params['comment'], 0, 6) == "-- Add") {
				$input_params['comment'] = false;
			}

		}

		// check for value in the post
		if(isset($item) && isset($item->$input_name)) {
			$input_params['value'] = $item->$input_name;
		}

		// update checkbox value and checked
		if($input_params["type"] == "checkbox") {
			$input_params['value'] = 1;
			$input_params['checked'] = isset($item) && $item->$input_name == 1 ? true  : false;
		}

		// update checkbox value and checked
		if($input_params["type"] == "select"
		|| $input_params["type"] == "select-social") {
			if($input_name != 'parent') {
				$input_params['selected'] = isset($item) ? $item->$input_name : 0;
			}
		}

		// json
		if($input_params["type"] == "json") {
			
			if(isset($item)) {

				// decod json
				$json_input_values = json_decode($item->$input_name, true);

				// create array for new input list
				$json_input_list = array();

				// go thorugh inputs
				foreach ($input_params['inputs'] as $json_key => $json_input) {

					// is this checkbox?
					if($json_input['type'] == 'checkbox') {

						// set checked or not
						$json_input['checked'] = isset($json_input_values[$json_key]) ? true  : false;
						
					} else {

						// check value
						$json_input['value'] = isset($json_input_values[$json_key]) ? $json_input_values[$json_key] : '';

					}

					// add to list
					$json_input_list[] = $json_input;

				}

				// update list
				$input_params['inputs'] = $json_input_list;

			}

		}

		// gallery add id to the params
		if($input_params["type"] == "gallery") {
			
			// add id
			$input_params["item_id"] = isset($item) && isset($item->id) ? $item->id : 0;

			// add gallery id to the options
			$input_params["options"]["id"] = $input_params["item_id"];

			// setup folder
			$input_params["options"]["folder"] =
			$input_params["options"]["folder"] . $input_params["item_id"] . '/';

			// the id of the gallery that is used in db
			$input_params["gallery_id"] = $this->GetName() . '_' . $level_name . "_" . $input_name . "_id" . $input_params["item_id"];

		}

		// facebook preview block add image
		if($input_params["type"] == "facebook") {
			$input_params["image"] = $facebook_image;
		}

		// check for tab
		if(isset($input_params['tab'])) {
			$input_tab = $input_params['tab'];
		} else {
			$input_tab = 0;
		}

		// check for col
		if(isset($input_params['col'])) {
			$input_col = $input_params['col'];
		} else {
			$input_col = 0;
		}

		// we dont add facebook to our global input list
		// we use it in our facebook preview code
		if(preg_match('/^facebook_image/', $input_name)) {

			// add to facebook param
			$facebook_image = $input_params;

		} else {

			// check tab type
			if($input_tab === 'top') {

				// add to top list
				$input_list['top'][$input_col][] = $input_params;

			} else {

				// add to the list
				$input_list[$input_tab][$input_col][] = $input_params;

			}

		}

	}

}

// if editing
if(isset($item) && isset($item->id)) {

	// add hidden id
	$input_list[0][0][] = array(
		'name'	=> $id . $level_prefix . "id",
		'type'	=> 'hidden',
		'value' => $item->id
	);

	// add parent
	if(isset($item->parent)) {
		$input_list[0][0][] = array(
			'name'	=> $id . $level_prefix . "oldparent",
			'type'	=> 'hidden',
			'value' => $item->parent
		);
	}

	// add item order
	if(isset($item->item_order)) {
		$input_list[0][0][] = array(
			'name'	=> $id . $level_prefix . "old_item_order",
			'type'	=> 'hidden',
			'value' => $item->item_order
		);
	}

}

// create tab list
$tab_list = array();
foreach ($input_list as $tab_id => $tab_content) {
	
	// default tab name
	$tab_name = $this->lang(( isset($item) ? 'edit_' : 'add_' ) . $level_name);

	// get the individual name from lang file
	$tab_individual_name = $this->lang($level_name . '_tab_' . $tab_id);

	// is it set? then update
	if(!preg_match('/^-- Add Me/', $tab_individual_name)) {
		$tab_name = $tab_individual_name;
	}

	// setup cols
	$col_content = array();
	foreach ($tab_content as $col_id => $col_inputs) {
		$col_content[$col_id] = $col_inputs;

		// set the last key
		if(end($tab_content) == $col_inputs) {
			$array_key_last = $col_id;
		} else {
			$array_key_last = false;
		}

		// check if only one col and ad empty second one
		if($col_id == 0 && $array_key_last === 0) {
			$col_content[1] = '';
		}
	}

	// check for top
	if($tab_id === 'top') {

		// tab params
		$input_top[$tab_id] = array(
			'name' 		=> $tab_name,
			'content'	=> $col_content
		);

	} else {

		// tab params
		$tab_list[$tab_id] = array(
			'name' 		=> $tab_name,
			'content'	=> $col_content
		);

	}

}

// create the tabs
$form_content = $admintheme->CreateTabs($tab_list);

if(is_array($form_content)) {
	$form_gallery = $form_content[1];
	$form_content = $form_content[0];
}

$form_top_content = false;
if(isset($input_top)) {
	$form_top_content = $admintheme->CreateTabs($input_top);
}

// ------------------------------------
// -- create the form
// ------------------------------------

// submit
$button_submit = isset($button_submit) ? $button_submit : get_html(array(
	'name'		=> $id . 'submit',
	'value'		=> lang('submit'),
	'template'	=> $config['root_path'] . '/admin/templates/button-submit.html'
));

// submit
$button_apply = isset($button_apply) ? $button_apply : get_html(array(
	'name'		=> $id . 'apply',
	'value'		=> lang('apply'),
	'template'	=> $config['root_path'] . '/admin/templates/button-submit.html'
));

// cancel
$button_cancel = isset($button_cancel) ? $button_cancel : get_html(array(
	'name'		=> $id . 'cancel',
	'value'		=> lang('cancel'),
	'template'	=> $config['root_path'] . '/admin/templates/button-cancel.html'
));

// item url
$item_url = 'moduleinterface.php?mact=' . $this->GetName() . ',' . $id;
$item_url .= ',' . $params['action'] . ',0';
foreach ($params as $key => $value) {
	if($key == 'apply' || $key == 'submit') break; // end if post or submit
	if($key == 'action') continue; // ignore action
	if($key == 'item_id') {
		$item_url .= '&' . $id . $key . '=' . $value;
	}
}

// add button
echo get_html(array(

	// where to submit
	'action'		=> $item_url,
	
	// error/success messages
	'message'		=> isset($message) ? $message : '',

	// content
	'content'		=> $form_content,
	'contnet_top'	=> $form_top_content,

	// buttons
	'submit'		=> $button_submit,
	'apply'			=> $button_apply,
	'cancel'		=> $button_cancel,
	'buttons_top' 	=> $this->showFormButtonsTop(),

	// gallery
	'gallery'		=> isset($form_gallery) ? $form_gallery : '',

	// template
	'template'		=> $config['root_path'] . '/admin/templates/form.html'

));





// end