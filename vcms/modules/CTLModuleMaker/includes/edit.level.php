<?php

if (!isset($gCms)) exit;
$admintheme = $gCms->variables["admintheme"];

// get the module info from get or post
if(isset($_GET['mact'])) {
	$moduleInfo = $_GET['mact'];
} else if(isset($_POST['mact'])) {
	$moduleInfo = $_POST['mact'];
}

$level_name = $this->getLevelNameFromPrefix($moduleInfo);
$level_fileds_function = "get_level_".$level_name."_fields";
$level_get_function = "get_level_".$level_name;

// wx get the fields and setup level prefix
$level_vars   = isset($level_vars) ? $level_vars : $this->$level_fileds_function();
$level_name   = $level_vars["name"];
$level_table  = $level_vars["table"];
$level_prefix = $level_vars["prefix"];
$input_fields = isset($input_fields) ? $input_fields : $level_vars["fields"];
$module_name  = $this->GetName();

// array for gallery and other input paths
// we use it later to create directories
$upload_directories = $this->$level_fileds_function("gallery");

if(isset($input_fields["gallery"])) {
	$gallery_folder = $input_fields["gallery"]['options']['folder'];
	$gallery_path = APP_UPLOADS . $gallery_folder;
}

if(isset($params["cancel"]) || ($this->GetPreference("restrict_permissions",false) && !$this->CheckPermission("module_" . $module_name . "_advanced") && !$this->CheckPermission("module_" . $module_name . "_manage_" . $level_name)) ){

	$newparams = array();

	// foreach ($params as $key => $value) {
	// 	if($key == 'cancel') break; // end if cancel
	// 	if($key == 'item_id') continue; // dont add to url
	// 	else $newparams[$key] = $value;
	// }

	$newparams["active_tab"] = $level_name;

	if(!isset($params["cancel"]))	$newparams["module_message"] = $this->Lang("error_denied");
	$this->Redirect($id, "defaultadmin", $returnid, $newparams);

}

$db = $this->GetDb();

// if we are working on an item that exists, we load it.
// We must do this even when the form is submitted, otherwise we won't have the file fields
if(isset($params[$level_prefix . "id"])) {
	$items = $this->$level_get_function(array("id"=>$params[$level_prefix . "id"]));
	$item = $items[0];
}

// CHECK IF THE FORM IS BEING SUBMITTED :
// (we must detect all kinds of submit buttons, including files,
// since information must be saved before we go to file submission)
if(isset($params["submit"]) || isset($params["apply"])) {
	
	debug_buffer("Edit Form has been submitted".__LINE__);

	// RETRIEVING THE FORM VALUES (and escaping it, if needed)
	if(!isset($item)) $item = new stdClass();
	
	// ---------------------
	// wx start setup fields
	// ---------------------

	$post_setup = $this->setupPostValues($level_vars, $params, $item);

	$query_fields = $post_setup["query_fields"];
	$values 	  = $post_setup["query_values"];
	$alias_list   = $post_setup["alias_list"];
	$alias_errors = $post_setup["alias_errors"];

	if(isset($item_errors) && is_array($item_errors) && count($item_errors) > 0) {
		if(is_array($post_setup["item_errors"]) && count($post_setup["item_errors"]) > 0) {
			$item_errors = $item_errors + $post_setup["item_errors"];
		}
	} else {
		$item_errors = isset($post_setup["item_errors"]) ? $post_setup["item_errors"] : false;
	}

	$item = $post_setup["item"];

	if(isset($params["old_item_order"])) { $item->old_item_order = $params["old_item_order"]; }
	if(isset($params[$level_prefix . "old_item_order"])) { $item->old_item_order = $params[$level_prefix . "old_item_order"]; }
	if(isset($params["oldparent"])) { $item->oldparent = $params["oldparent"]; }

	// -------------------
	// wx end setup fields
	// -------------------
	
	// CHECK IF THE NEEDED VALUES ARE THERE
	if(count($alias_errors) > 0){ // wx update
		
		$alias_error_text = "<ul>";
		$alias_error_text .= "<li>" . $this->Lang("error_aliasexists") . "</li>";
		
		foreach ($alias_errors as $alias_error) {
			if($alias_error == "alias") { $alias_error_text .= "<li>" . $this->Lang($alias_error) . "</li>"; }
			else { $alias_error_text .= "<li>" . $this->Lang($level_name . "_" . $alias_error) . "</li>"; }
		}

		$alias_error_text .= "</ul>";

		echo $this->ShowErrors($alias_error_text);

	} else if($item_errors && is_array($item_errors) && count($item_errors) > 0) {

		$message = $this->ShowMessage($item_errors, 'error');

	} else {

		############ DOING THE UPDATE

		// FIELDS TO UPDATE
		$query = (isset($item->id)?"UPDATE ":"INSERT INTO ").cms_db_prefix()."module_".$level_table." SET 
			$query_fields
			date_modified=?,
			isdefault=".(isset($item->isdefault)?$item->isdefault:0);
			
		// VALUES
		// wx add date modified to the values
		$values["date_modified"] = str_replace("'","",$db->DBTimeStamp(time()));

		// wx setup item order
		$item->item_order = $this->updateItemOrder($level_name, $level_table, $item);

		if(isset($item->id)) {

			// UPDATE

			// set event
			$event = $this->Lang($level_name) . " Modified";

			// add order and id to the query
			$query .= ",item_order=" . $item->item_order;

			// update current item
			$query .= " WHERE id=?;";
			array_push($values, $item->id);

			// set the message
			$success_message = "modified";

		} else {

			// NEW

			// if active is not in the post
			if(!isset($item->active)) {
				$query .= ", active=?";
				$values[] = 1; // active by default
			}

			// if date_created is not in the post
			if(!isset($item->date_created)) {
				$query .= ", date_created=?";
				$values[] = str_replace("'","",$db->DBTimeStamp(time()));
			}

			// set event
			$event = $this->Lang($level_name) . " Added";

			// add order and id to the query
			$query .= ",item_order=".$item->item_order;

			// set the message
			$success_message = "added";

		}

		$db->Execute($query, $values);

		$redirect = true;

		if($db->Affected_Rows()){

			// is id set? if not then get it from last insert
			if(!isset($item->id)) {
				$item->id = $db->Insert_ID();
			}
			
			// send the event
			if(isset($event)) {
				$this->Audit($item->id, $this->GetFriendlyName(), $event);
			}

			// wx ==> create gallery or other upload folder
			if($upload_directories) {
				foreach ($upload_directories as $gallery_path) {
					if(file_exists($gallery_path) && is_writable($gallery_path)) {
						$itemGalleryfolder = $gallery_path . $item->id;
						// make the dir if it's not there
						if(!is_dir($itemGalleryfolder)) mkdir($itemGalleryfolder, 0755);
					}
				}
			}

		} elseif(mysql_error()) {

			// do not redirect
			$redirect = false;

			// show the error
			echo $this->ShowErrors(mysql_error());

		}

		// REDIRECTING...
		if($redirect == false){
			
			// stay on the page
			// we have an error

		} elseif(isset($params["apply"])){

			// get the message
			$success_message = $this->get_lang_param(
				$success_message,
				$level_name,
				array(clean_up_string($item->name))
			);

			// show the message
			$message = $this->ShowMessage($success_message);

		} else {

			$redirect_params = array();

			// foreach ($params as $key => $value) {
			// 	if($key == 'submit') break; // end if post
			// 	else $redirect_params[$key] = $value;
			// }

			$redirect_params["item_id"] = $item->id;
			$redirect_params["message"] = $success_message;
			$redirect_params["active_tab"] = $level_name;

			$this->Redirect($id, "defaultadmin", $returnid, $redirect_params);

		}

	}
	
} // END OF FORM SUBMISSION

?>