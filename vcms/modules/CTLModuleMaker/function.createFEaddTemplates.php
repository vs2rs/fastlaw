<?php
$templatefile = '<h1>{$edittitle}</h1>
';
$templatefile .= '
<br/>
<table border="0">
	<tr>
		<td>{$name_label}* :</td>
		<td>{$name_input}</td>
	</tr>
';
if($parentinput){
	$templatefile .= '
	<tr>
		<td>{$parent_label}* :</td>
		<td>{$parent_input}</td>
	</tr>';
}

$multi = array();

foreach($level[4] as $field){
	if(!in_array($field[0],array('name','id','item_order','isdefault','active','parent','alias')) && ($field[1] < 10 || $field[1] == 12)){
		$templatefile .= '
	<tr>
		<td>{$'.$field[0].'_label}'.($field[2] == 1?'*':'').' :</td>
		<td>{$'.$field[0].'_input}</td>
	</tr>';
	}elseif($field[1] == 10){
		$templatefile .= '
	<tr>
		<td>{$'.$field[0].'_label}'.($field[2] == 1?'*':'').' :</td>
		<td>{html_select_date prefix=$'.$level[1].'_'.$field[0].'_prefix time=$'.$level[1].'_'.$field[0].' start_year="-10" end_year="+10"} {html_select_time prefix=$'.$level[1].'_'.$field[0].'_prefix time=$'.$level[1].'_'.$field[0].' }</td>
	</tr>';
	}elseif($field[1] == 11 || $field[1] == 13){
		$multi[] = $field;
	}
}

$templatefile .= '
</table><br/>
';
foreach($multi as $field){
	$templatefile .= '
<fieldset><legend>{$'.$field[0].'_label}:</legend>
	<ul>
	{foreach from=$item->'.$field[0].' item="onefile"}
		<li><a href="{$onefile->url}">{$onefile->filepath}</a> ({$onefile->size_wformat}) {$onefile->deletelink}</li>
	{/foreach}
	</ul>
	<p>{$'.$field[0].'_input}</p>
</fieldset>
';
}
$templatefile .= '
<p><div style="float:left;">{$captcha_image}</div>{$captcha_prompt}<br/>{$captcha_input}</p>
<p>{$submit}</p>
';

?>
