<?php
$form = '
<?php
		$comparechoices = array($this->Lang("contains") =>1, $this->Lang("isexactly") =>0, $this->Lang("isnot") =>2, $this->Lang("ishigherthan") =>3, $this->Lang("islowerthan") =>4);
		$datecomparechoices = array($this->Lang("queryuse") => "NA", $this->Lang("isafter") =>3, $this->Lang("isbefore") =>4, $this->Lang("isbetween") =>5);
		$orderchoices = array("ASC"=>"ASC","DESC"=>"DESC");

		$output = array();

		switch($what){
			';
$i = 0;
while($i < count($levels)){
	$level = $levels[$i];
	$level[4][] = array('date_modified',10,0,0,0,array());
	$fieldlist = "";
	$form .= 'case "'.$level[0].'":
				';
	foreach($level[4] as $field){
		if(!in_array($field[0],array('isdefault','active')) && ($field[1] < 8 || $field[1] == 10) )	$fieldlist .= ',"'.$field[0].'"=>"'.$field[0].'"';
		if(!in_array($field[0],array('id','item_order','isdefault','active','alias')) && !in_array($field[1], array(8,9,11,13)) ) {
			if($field[1] == '6' || $field[1] == '7'){
				if(false && ($field[5]['listmode'] == 2 || $field[5]['listmode'] == 4)){
					// do nothing - we don't support search in lists with multiple choices yet
				}else{
					if($field[1] == 7){
						$form .= '$tmpoptions = $this->get_fieldoptions("'.$level[0].'_'.$field[0].'");
				';
					}else{
						$form .= '$tmpoptions = $this->get_predefinedoptions("'.$level[0].'_'.$field[0].'");
				';
					}
					$form .= '$tmpoptions[""] = "";
				$output["'.$field[0].'"] = array($this->Lang("'.$level[0].'_'.$field[0].'"),$this->CreateInputSelectList($id,"field_'.$field[0].'[]",$tmpoptions));
				';
				} 
			}elseif($field[1] == 10){
				$fieldlabel = ($field[0] == 'date_modified'?'date_modified':$level[0].'_'.$field[0]);
				$form .= '
				$this->smarty->assign("tmptime", time());
				$this->smarty->assign("s_and", $this->Lang("thisandthis"));
				$this->smarty->assign("tmp_prefix", $id."date_'.$field[0].'_");
				$this->smarty->assign("tmp_prefix2", $id."date_'.$field[0].'_part2_");
				$tmptemplate = \'{html_select_date prefix=$tmp_prefix time=$tmptime start_year="-10" end_year="+10"} {html_select_time prefix=$tmp_prefix time=$tmptime }<div id="\'.$id.\''.$field[0].'_part2" style="display: none;"> {$s_and} {html_select_date prefix=$tmp_prefix2 time=$tmptime start_year="-10" end_year="+10"} {html_select_time prefix=$tmp_prefix2 time=$tmptime }</div>\';
				$tmpout = $this->CreateInputDropdown($id,"compare_'.$field[0].'",$datecomparechoices,-1,"NA"," onchange=\"dateinput_changecompare(\'".$id."'.$field[0].'\', this.value);\"")." ".$this->ProcessTemplateFromData($tmptemplate);
				$tmpout .= $this->CreateInputHidden($id,"field_'.$field[0].'", "__date_field");
				$output["'.$field[0].'"] = array($this->Lang("'.$fieldlabel.'"), $tmpout);
				';
			}elseif($field[1] == 12){
				$form .= '$output["'.$field[0].'"] = array($this->Lang("'.$level[0].'_'.$field[0].'"),$this->CreateInputCheckbox($id,"field_'.$field[0].'",1));';
			}elseif($field[0] == 'parent'){
				$form .= '$output["parent"] = array($this->Lang("parent"));
				';
				if($shared){
					$form .= '$parentoptions = $this->get_options("'.$levels[$i-1][0].'",true);
				$output["parent"][] = $this->CreateInputDropdown($id,"field_parent",$parentoptions);
				';
				}else{
					$form .= '$parentoptions = $this->get_hierarchyoptions("'.$levels[$i][0].'");
				$output["parent"][] = $this->CreateInputDropdown($id,"field_parent",$parentoptions);
				';					
				}
			}else{
				$form .= '$output["'.$field[0].'"] = array($this->Lang("'.($field[0] == 'name'?'name':$level[0].'_'.$field[0]).'"), $this->CreateInputDropdown($id,"compare_'.$field[0].'",$comparechoices,-1,1)." ".$this->CreateInputText($id,"field_'.$field[0].'","",30));
				';
			}
		}
	}		
	$form .='$orderfieldoptions = array(""=>""'.$fieldlist.');
				$output["orderby"] = array($this->Lang("orderbyfield"), $this->CreateInputDropdown($id,"order",$orderfieldoptions)." ".$this->CreateInputDropdown($id,"order_type",$orderchoices));
				break;
			';
	$i++;
}
$form .= '
		}
		echo \'<script type="text/javascript">
	function dateinput_changecompare(fieldname, newvalue){
		if(newvalue == 5){
			var new_state = "inline";
		}else{
			var new_state = "none";
		}
		if(document.getElementById(fieldname+"_part2"))	document.getElementById(fieldname+"_part2").style.display = new_state;
		return true;
	}
</script>
\';
		if($assign){
			foreach($output as $key=>$value){
				$this->smarty->assign($key."_label", $value[0]);
				$this->smarty->assign($key."_input", $value[1]);
			}
		}else{
			echo "<table style=\"border-left: 1px solid LightGray; padding-left: 10px;\">";
			foreach($output as $key=>$value){
				if($key != "orderby")	echo "
				<tr><td>".$value[0]."</td><td>".$value[1]."</td></tr>";
			}
				echo "
			</table><br/>";
			echo "<p>".$output["orderby"][0].": ".$output["orderby"][1]."</p>";
		}
?'.'>';
?>
