<?php
if(!isset($levels) || !isset($linktables)) exit;

$uninstallmethod = '<?php
if(!isset($gCms)) exit;

// Typical Database Initialization
$db = &$this->cms->db;
$dict = NewDataDictionary($db);

';

foreach($levels as $level){
	$levelname = $level[0];
	$uninstallmethod .= '

	$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_'.$modulename.'_'.$levelname.'");
	$dict->ExecuteSQLArray($sqlarray);
	$db->DropSequence(cms_db_prefix()."module_'.$modulename.'_'.$levelname.'_seq");
	';
}

foreach($linktables as $linktable){
	$levelname = $linktable[0].'_has_'.$linktable[1];
	$uninstallmethod .= '

	$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_'.$modulename.'_'.$levelname.'");
	$dict->ExecuteSQLArray($sqlarray);
	';
}

if(count($listfields) > 0){
	$uninstallmethod .= '
	$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_'.$modulename.'_fieldoptions");
	$dict->ExecuteSQLArray($sqlarray);
	$db->DropSequence(cms_db_prefix()."module_'.$modulename.'_fieldoptions_seq");
	';
}

if(count($multiplefiles) > 0) {
	$uninstallmethod .= '
	$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_'.$modulename.'_multiplefilesfields");
	$dict->ExecuteSQLArray($sqlarray);
	$db->DropSequence(cms_db_prefix()."module_'.$modulename.'_multiplefilesfields_seq");
	';
}


$uninstallmethod .= '	
	$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_'.$modulename.'_saved_queries");
	$dict->ExecuteSQLArray($sqlarray);
	$db->DropSequence(cms_db_prefix()."module_'.$modulename.'_saved_queries_seq");
';

// DELETING PERMISSIONS AND EVENTS :

$uninstallmethod .= '
// permissions';

foreach($levels as $level){
	$uninstallmethod .= '
	$this->RemovePermission("module_'.$modulename.'_manage_'.$level[0].'");';
}
$uninstallmethod .= '
	$this->RemovePermission("module_'.$modulename.'_advanced");
	$this->RemovePreference();

// put mention into the admin log
	$this->Audit( 0, $this->Lang("friendlyname"), $this->Lang("uninstalled"));

?>
';

?>
