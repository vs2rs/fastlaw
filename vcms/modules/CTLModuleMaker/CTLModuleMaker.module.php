<?php
#-------------------------------------------------------------------------
# Module: CTLModuleMaker - A module maker for catalogue-like modules...
# By Pierre-Luc Germain 
#
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2006 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
# icons also published under GNU General Public License...
#
#-------------------------------------------------------------------------

class CTLModuleMaker extends CMSModule
{
	function GetName()
	{
		return 'CTLModuleMaker';
	}

	function GetFriendlyName()
	{
		return $this->Lang('friendlyname');
	}
	
	function GetVersion()
	{
		return '1.8.9.3';
	}

	function MinimumCMSVersion()
	{
		return '1.2.1';
	}

	function GetHelp()
	{
		return $this->Lang('help');
	}

	function GetAuthor()
	{
		return 'Pierre-Luc Germain';
	}

	function GetAuthorEmail()
	{
		return '';
	}

	function IsPluginModule()
	{
		return false;
	}

	function HasAdmin()
	{
		return true;
	}

	function GetAdminSection()
	{
		return 'extensions';
	}

	function GetAdminDescription()
	{
		return $this->Lang('moddescription');
	}

	function VisibleToAdminUser()
	{
        return $this->CheckPermission('admin_use_ctl_module_maker');
	}

    function GetChangeLog()
    {
    	$changelog = $this->getFileContent("doc/changelog.txt", false);
    	return '<br/><pre>'.$changelog.'</pre>';
    }

	function Install()
	{
		$this->CreatePermission('admin_use_ctl_module_maker', 'Admin - Use CTL Module Maker');
		$this->SetPreference("usesessions", true);
		$this->SetPreference("checkversion",true);
		$this->Audit( 0, $this->Lang('friendlyname'), $this->Lang('installed',$this->GetVersion()));
	}

	function Uninstall()
	{
		$this->RemovePermission('admin_use_ctl_module_maker');
		$this->Audit( 0, $this->Lang('friendlyname'), $this->Lang('uninstalled'));
	}
     function InstallPostMessage()
     {
		 return $this->Lang('postinstall');
     }
     function UninstallPostMessage()
     {
		 return $this->Lang('postuninstall');
     }
     
	function DoAction($action, $id, $params, $returnid=-1){
		global $gCms;
		switch($action){
			case 'creation':
				require 'action.creation.php';
				break;
			case 'createModule':
				require 'action.createModule.php';
				break;
			case 'modop':
				require 'action.modop.php';
				break;
			case 'dnaimport':
				require 'action.dnaimport.php';
				break;
			case 'installmod':
				if(isset($params['isinstalled']) && $params['isinstalled']){
					echo '<p>'.$this->Lang('moduleinstalled').'</p>';
					if($this->GetPreference('usesessions',false))	echo '<p>'.$this->CreateLink($id, 'clearsession', $returnid, $this->Lang('prompt_clearsession')).'</p>';
				}else{
					$modoperations = $gCms->GetModuleOperations();
					if( isset($params["modulename"]) && $modoperationsResponse = $modoperations->InstallModule($params["modulename"],true) ){
						// vcms / wx ==> response is an array not boolean
						if($modoperationsResponse[0] == true){
							$this->Redirect($id, 'installmod', $returnid, array("isinstalled"=>true));	
						} else {
							echo "Instalation failed...<br/>";
							echo "<pre>";
							print_r($modoperationsResponse);
							echo "</pre>";
						}
					}elseif(isset($params["modulename"])){
						echo '<div class="pageerrorcontainer">'.$this->Lang('error_installfailed').'</div>';
					}
				}
				break;
			case 'clearsession':
				$newparams = array();
				if(isset($_SESSION['CTLMM'])){
					unset($_SESSION['CTLMM']);
					$newparams["module_message"] = $this->Lang("sessioncleared");
				}
				$this->Redirect($id, 'defaultadmin', $returnid, $newparams);
				break;
			case 'togglevcheck':
				if(!isset($params['newval']) || $params['newval'] == 0){
					$this->SetPreference("checkversion",false);
				}else{
					$this->SetPreference("checkversion",true);
				}
			case 'changeprefs':
				if(isset($params['submit'])){
					$myprefs = $this->get_modulePreferences();
					foreach($myprefs as $onepref){
						$this->SetPreference($onepref[0], isset($params[$onepref[0]])?$params[$onepref[0]]:0);
					}
				}
			case 'defaultadmin':
			default:
				require 'action.defaultadmin.php';
				break;
		}
	}

#-------------------------------------------------------------------------

	function getInnerDebug($params, $levels=false){
		// global $config;

		// $always = false;		// Always show the inner debug

		// if($always || ($config['debug'] && $this->GetPreference('innerdebug',false))){
		// 	echo '<hr/>YOU ARE IN DEBUG MODE<br/>PARAMS: <pre>';
		// 	print_r($params);
		// 	echo '</pre><br/><br/>';
		// 	if($levels){
		// 		echo 'LEVELS INFORMATION: <pre>';
		// 		print_r($levels);
		// 		echo '</pre><br/><br/>';
		// 	}
		// 	return true;
		// }
		return false;
	}
	
	function echo_moduleinfos($id, $returnid){
		if($this->GetPreference("checkversion", true)){
			if($vfilecontent = @file_get_contents("http://www.plger.net/ctlmm/CTLMM_versioninfo.php?v=".$this->GetVersion())){
				echo '<fieldset style="float: right; width: 300px;"><legend style="font-weight: bold;">CTLModuleMaker Informations:</legend>';
				echo '<p>'.stripslashes($vfilecontent).'<br/></p>';
				echo '<p align="right">'.$this->CreateLink($id, "togglevcheck", $returnid, $this->Lang('disvcheck')).'</p></fieldset>
			';
			}
		}else{
			echo '<p style="float: right;">'.$this->CreateLink($id, "togglevcheck", $returnid, $this->Lang('envcheck'), array("newval"=>true)).'</p>
			';
		}
	}
	
	function isreservedword($word){
		require 'reservedwords.php';
		return in_array(strtoupper($word),$reservedwords);
	}
	function starts_with_letter($string){
		return preg_match("/^([A-Za-z])$/i", substr($string,0,1));
	}

	function display_errors($errors, $head='serror'){
		if(count($errors) > 0){
			echo '<div class="pageerrorcontainer"><ul><span style="text-decoration: underline;">'.$this->Lang($head).':</span>';
			foreach ($errors as $oneerror)	echo '<li>'.$oneerror.'</li>';
			echo '</ul></div>';
		}
	}

	function DoHiddenInputs($id, $params, $levels, $infos, $currentlevel=false){
		$output = "";
		if($currentlevel) $output .= $this->CreateInputHidden($id, 'currentlevel', $currentlevel);
		if(isset($params['step'])) $output .= $this->CreateInputHidden($id, 'step', $params['step']);
		$output .= $this->CreateInputHidden($id, 'substep', isset($params['substep'])?$params['substep']:1);
		if(!$this->GetPreference('usesessions',false)){
			$output .= $this->CreateInputHidden($id, 'infos', serialize($infos));
			$output .= $this->CreateInputHidden($id, 'levels', serialize($levels));
		}
		return $output;
	}
	
	function EchoExportForm($params, $levels, $infos, $currentlevel=false, $prompt='prompt_exportdna', $final=false){
		// wx - this probably is broken, and do we need it all?
		echo '<hr/><div style="text-align: right;"><form action="/CTLModuleMaker/moduledna.export.php" method="post">';
		echo '<p>'.$this->Lang($prompt).'<br/><input type="submit" value="'.$this->Lang('exportdna').'" /></p>
		';
		echo '<input type="hidden" name="levels" value="'.urlencode(serialize($levels)).'"/>
		<input type="hidden" name="infos" value="'.urlencode(serialize($infos)).'"/>';
		if($currentlevel) echo '<input type="hidden" name="currentlevel" value="'.$currentlevel.'"/>';
		if(isset($params['step'])) echo '<input type="hidden" name="step" value="'.$params['step'].'"/>';
		if(isset($params['substep'])) echo '<input type="hidden" name="substep" value="'.$params['substep'].'"/>';
		if($final) echo '<p>'.$this->Lang("help_export").'</p>';
		echo '</form></div>';
	}
	
	function DoInputLine($prompt,$input,$mandatory=false,$addhtml='',$help=false){
		$output = '
	<div class="pageoverflow '.$addhtml.'">
		<p class="pagetext" style="width: 30em;">'.$this->Lang('prompt_'.$prompt).($help?'<br/><span style="color: red;">'.$help.'</span>':'').'</p>
		<p class="pageinput" style="padding-bottom: 5px;">'.$input.'</p>';
		$output .= '
	</div>';
		return $output;
	}
	
	function DoCheckboxes($id, $name, $choices, $selected=array(), $delimiter="<br/>"){
		$output = "";
		foreach($choices as $key=>$value){
			$output .= $this->CreateInputCheckbox($id, $name."[]", $value, (in_array($value, $selected)?$value:0))." ".$key.$delimiter;
		}
		return $output;
	}
	
	function ValidateVersion($version){
		$parts = explode('.',$version);
		if(count($parts) == 0)	return "1.0";
		$output = "";
		foreach($parts as $part){
			$part = (int) $part;
			if($part == "") $part = 0;
			$output .= ($output == ""?"":".").$part;
		}
		return $output;
	}
	
	function ValidateSize($string){
		$parts = explode("x",$string);
		if(count($parts) != 2)	return false;
		return ($parts[0] > 0 && $parts[1] > 0);
	}

	function ValidateField($fieldvalues){
		$errors = array();
		if(!is_array($fieldvalues))	$errors[] = $this->Lang('error_unknown');
		if(!isset($fieldvalues[0]) || $fieldvalues[0] == '') $errors[] = $this->Lang('error_emptyname');
		if(!$this->starts_with_letter($fieldvalues[0]))	$errors[] = $this->Lang('error_namebeginwithletter');
		$newname = munge_string_to_url(str_replace('-','',$fieldvalues[0]), false);
		if($newname != $fieldvalues[0] or in_array(strtolower($fieldvalues[0]),array('alias','id','item_order','isdefault','active','name','parent','nbchildren','is_selected','date_modified'))) $errors[] = $this->Lang('error_namealreadyused');
		if($this->isreservedword($newname))	$errors[] = $this->Lang('error_reserved').'<i>'.$newname.'</i>. '.$this->Lang('error_reserved2');
		if($fieldvalues[1] == 6 && ( !isset($fieldvalues[5]['listoptions']) || count(explode(',',$fieldvalues[5]['listoptions'])) < 2) ) $errors[] = $this->Lang('error_listoptions');
		if(in_array($fieldvalues[1],array(8,9,11,13)) && $fieldvalues[5]['upfolder'] != '') {
			$folderstrimmed = str_replace('/','',$fieldvalues[5]['upfolder']);
			$newfolder = munge_string_to_url($folderstrimmed, false);
			if($newfolder != $folderstrimmed) $errors[] = $this->Lang('error_upfolder');
		}
		if($fieldvalues[1] == 8 || $fieldvalues[1] == 13){
			if($fieldvalues[5]['size'] != '' && !$this->ValidateSize($fieldvalues[5]['size'])) 		$errors[] = $this->Lang('error_imgsize');
			if($fieldvalues[5]['thumb'] != '' && !$this->ValidateSize($fieldvalues[5]['thumb'])) 	$errors[] = $this->Lang('error_thumbsize');
		}
		return $errors;
	}

	function GetDefaultFields(){
		return array("id","name","alias","item_order","isdefault","active","parent","nbchildren");
	}
	function checkAdminFields($chosenfields,$levelfields){
		$errors = array();
		$chosenfields = explode(',',$chosenfields);
		if(!in_array('name',$chosenfields)){
			$errors[] = $this->Lang('error_adminfields_name');
		}else{
			$fieldlist = $this->GetDefaultFields();
			$fieldlist[] = 'movelinks';
			foreach($levelfields as $onefield){
				$fieldlist[] = $onefield[0];
			}
		$diff = array_diff($chosenfields, $fieldlist);
		if(count($diff) > 0) $errors[] = $this->Lang('error_general');
		}
		return $errors;
	}

	function GetFieldTypes(){
		return array(
			$this->Lang('Number')=>0,
			($this->Lang('Text').'(10 '.$this->Lang('chars').')')=>1,
			($this->Lang('Text').'(32 '.$this->Lang('chars').')')=>2,
			($this->Lang('Text').'(64 '.$this->Lang('chars').')')=>3,
			($this->Lang('Text').'(255 '.$this->Lang('chars').')')=>4,
			$this->Lang('LongText')=>5,
			$this->Lang('PredefinedList')=>6,
			$this->Lang('List')=>7,
			$this->Lang('checkbox')=>12,
			$this->Lang('Date')=>10,
			$this->Lang('Image')=>8,
			$this->Lang('File')=>9,
			$this->Lang('undefined_files')=>11,
			$this->Lang('undefined_images')=>13
		);
	}
	
	function GetDBFieldType($type, $listmode){
		$fieldtype = false;
		switch($type){
			case 0: $fieldtype = "I"; break;
			case 1: $fieldtype = "C(10)"; break;
			case 2: $fieldtype = "C(32)"; break;
			case 3: $fieldtype = "C(255)"; break; // 64 to 255
			case 4: $fieldtype = "C(255)"; break;
			case 5: $fieldtype = "X"; break;
			case 6: case 7: 
				if($listmode == 2 || $listmode == 4){
					$fieldtype = "C(255)";
				}else{
					$fieldtype = "I";
				}
				break;
			case 8: case 9:
				$fieldtype = "C(255)";
				break;
			case 10:
				$fieldtype = '".CMS_ADODB_DT."';
				break;
			case 12:
				$fieldtype = "L";
				break;
		}
		return $fieldtype;
	}
	
	function toMysqlType($adotype){
		$letter = substr($adotype,0,1);
		$new = false;
		switch($letter){
			case "I":	$new = "INT"; break;
			case "L":	$new = "TINYINT"; break;
			case "C":	$new = "VARCHAR".substr($adotype,1); break;
			case "I":	$new = "INT"; break;
			case "X":	$new = "TEXT"; break;
			case '".CMS_ADODB_DT."':	$new = "DATETIME";
		}
		return $new;
	}

	function displayResult($name='',$result=true,$what='file'){
		$message = ($what == 'file'?$this->Lang("filecreation"):$this->Lang("folderscreation")).$name;
		if($result){
			echo '
				<li>'.$message.'... <span style="color: green;">DONE!</span></li>';
			return true;
		}else{
			echo '
				<li style="color: red; font-weight: bold;">'.$message.'... ERROR</li>';
		}
	}

	function writeTest(){
		global $gCms;
		$basedir = $gCms->config['modules_project'].DIRECTORY_SEPARATOR;
		$filename = 'test';
		while(file_exists($basedir.$filename)){
			$filename = '_'.$filename;
		}
		if(mkdir($basedir.$filename)){
			if(rmdir($basedir.$filename)){
				return true;
			}else{
				die('Could not delete the test folder '.$basedir.$filename);
			}
		}else{
			return false;
		}
	}
	
	function get_modulePreferences(){
		return array(
			array('checkversion', true),
			array('autosave', true),
			array('innerdebug', false),
			array('doupgrade', true),
			array('allowsinglelevel', false),
			array('usesessions', false)
		);
	}
	
	function get_autosaved($id, $returnid, $before='<li>', $after='</li>'){
		$dir = dirname(__FILE__).DIRECTORY_SEPARATOR.'autosaved';
		$dh  = opendir($dir);
		$output = '';
		while (false !== ($filename = readdir($dh))){
			if(strtolower(strrchr($filename, ".")) == '.dna'){
				$output .= $before.$this->CreateLink($id, 'dnaimport', $returnid, str_replace('_auto_export.dna','',$filename), array('localfile'=>$filename)).$after;
			}
		}
		return $output;
	}

	function CreateFolders($modulename,$uploaddirs){
		global $gCms;
		$errors = array();
		// From the ModuleMaker module...
		$basedir = $gCms->config['modules_project'].DIRECTORY_SEPARATOR.$modulename;
		if(file_exists($basedir)){
			$errors[] = $basedir.' : '.$this->Lang('error_folderalreadythere');
		}elseif (!(@mkdir($basedir))){
			$errors[] = $basedir.' : '.$this->Lang('error_foldercreation');
		}elseif (!(@mkdir($basedir.DIRECTORY_SEPARATOR.'lang'))){
			$errors[] = $basedir.DIRECTORY_SEPARATOR.'lang : '.$this->Lang('error_foldercreation');
		}elseif (!(@mkdir($basedir.DIRECTORY_SEPARATOR.'lang'.DIRECTORY_SEPARATOR.'ext'))){
			$errors[] = $basedir.DIRECTORY_SEPARATOR.'lang'.DIRECTORY_SEPARATOR.'ext : '.$this->Lang('error_foldercreation');
		}else{
			$indexFile = $basedir.DIRECTORY_SEPARATOR.'index.html';
			@touch($indexFile);
		
			$baseupdir = $gCms->config['uploads_path'];
			foreach($uploaddirs as $onedir){
				if(!file_exists($baseupdir.DIRECTORY_SEPARATOR.$onedir)){
					$architecture = explode('/',$onedir);
					$newbase = $baseupdir;
					foreach($architecture as $folder){
						if($folder != ''){
							$newbase .= DIRECTORY_SEPARATOR.$folder;
							if(!file_exists($newbase) && !@mkdir($newbase)) 	echo $this->Lang('error_uploadfolder');
						}
					}
				}
			}
		}
		
		
		if(count($errors) > 0){
			return $errors;
		}else{
			return $basedir.DIRECTORY_SEPARATOR;
		}
	}

	function getFileContent($filename, $insrc=true){
		$filepath = dirname(__FILE__);
		if($insrc)	$filepath .= DIRECTORY_SEPARATOR."src";
		$filepath .= DIRECTORY_SEPARATOR.$filename;
		if(file_exists($filepath)){
			$fhandle = fopen($filepath, 'r+');
			$content = fread($fhandle, filesize($filepath));
			return $content;
		}else{
			return false;
		}
	}

	function CreateFile($filepath,$content='')
	{
		$fhandle = fopen($filepath, 'w');
		if(!$fhandle) return false;
		fwrite($fhandle, $content);
		fclose($fhandle);
		return true;
	}
	function CreateFieldInput($id, $innerid, $fieldvalues=array())
	{
		global $gCms;
		require 'function.CreateFieldInput.php';
	}
	function createAll($id, $returnid, $infos, $levels){
		global $gCms;
		require 'function.createAll.php';
	}
	function createInstall($modulename, $modulfriendlyname, $levels, $linktables, $listfields, $globaltemplate=false, $multiplefiles){
		require 'function.createInstall.php';
		return $installmethod;
	}
	function createUninstall($modulename, $levels, $linktables, $listfields, $multiplefiles){
		require 'function.createUninstall.php';
		return $uninstallmethod;
	}
	function createUpgrade($infos, $levels){
		require 'function.createUpgrade.php';
		return $method;
	}
	function createLangFile($infos, $levels, $inlang=false){
		require 'function.createLangFile.php';
		return $langfile;
	}
	function createEditLevel($level, $infos, $parentname=false, $sharedbyparents=false) {
		require 'function.createEditLevel.php';
		return $phpfile;
	}
	function createActions($infos, $levels, $hasdynamiclists, $multiplefiles){
		require 'function.createActions.php';
		return $actionswitch;
	}
	function createActionDefault($infos, $levels){
		require 'function.createActionDefault.php';
		return $action;
	}
	function createMoveAction($modulename, $hasfilefields){
		require 'function.createMoveAction.php';
		return $phpfile;
	}
	function createActionSearch($infos, $levels){
		require 'function.createActionSearch.php';
		return $action;
	}
	function createActionReorder($infos, $levels){
		require 'function.createActionReorder.php';
		return $action;
	}
	function createDBGetFunctions($infos, $levels, $predefinedlists, $multiplefiles){
		require 'function.createDBGetFunctions.php';
		return $dbget;
	}
	function createDefaultadmin($infos, $levels, $listfields){
		require 'function.createDefaultadmin.php';
		return $defaultadmin;
	}
	function createModuleFile($infos, $levels, $predefinedlists, $multiplefiles, $hasdynamiclists, $makerversion){
		require 'function.createModuleFile.php';
		return $moduleFile;
	}
	function createEditTemplates($level, $infos, $parentinput=false, $frontend=false){
		require 'function.createEditTemplates.php';
		return $templatefile;
	}
	function createSearchForm($level,$parent=false){
		require 'function.createSearchForm.php';
		return $template;
	}
	function createTemplateHelp($levels){
		require 'function.createTemplateHelp.php';
		return $helps;
	}
	function createFEadd($level, $infos, $parentname=false, $sharedbyparents=false) {
		require 'function.createFEadd.php';
		return $phpfile;
	}
	function createFEaddTemplates($level, $infos, $parentinput=false){
		require 'function.createFEaddTemplates.php';
		return $templatefile;
	}
	function createFieldForm($levels, $shared){
		require 'function.createFieldForm.php';
		return $form;
	}

	function compareArchitecture($old, $new){
		$i = 0;
		while($i < count($new)){
			if($new[$i][0] != $old[$i][0]) return false;
			if($new[$i][2] != $old[$i][2]) return false;
			$i++;
		}
		return true;
	}
	
	function compareLevels($old, $new){
		require 'function.compareLevels.php';
		return array($changes, $queries);
	}

}
?>
