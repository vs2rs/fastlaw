<?php
$finallevel = $levels[count($levels) - 1][0];
$modulename = $infos['nameofmodule'];
$modulfriendlyname = $infos['friendlyname'];
$levelnames = '';
foreach($levels as $level){
	$levelnames .= ($levelnames == ''?'':', ').$level[0];
}

// loading the language :
$mylang = array();
require 'createdlang/en_US.php';
if($inlang && $inlang != 'en_US'){
	require 'createdlang/'.$inlang.'.php';	
}

$defaultfields = $this->GetDefaultFields();
$description = str_replace('"','&quot;',$infos['description']);	// not useful anymore
$langfile = '<?php
	$lang["friendlyname"] = "'.htmlentities(html_entity_decode(utf8_decode($infos['friendlyname']))).'";
	$lang["moddescription"] = "'.htmlentities(html_entity_decode(utf8_decode($description))).'";
	$lang["admindescription"] = "'.htmlentities(html_entity_decode(utf8_decode($description))).'";

// advance permission name
	$lang["permissions_advanced"] = "%s - %s - Full Access";
	';

$currentlevel = 0;
foreach($levels as $level){
	if(!isset($level[9]))	$level[9] = array(ucfirst($level[0]), ucfirst(substr($level[0],-8,8)=='category'?substr($level[0],0,-1).'ies':$level[0].'s'));
	$currentlevel++;
	$langfile .= '
// strings for '.$level[0].'
	$lang["permissions_manage_'.$level[0].'"] = "%s - %s - Manage '.htmlentities(html_entity_decode(utf8_decode($level[9][1]))).'";
	$lang["'.$level[0].'"] = "'.htmlentities(html_entity_decode(utf8_decode($level[9][0]))).'";
	$lang["'.$level[0].'_plural"] = "'.htmlentities(html_entity_decode(utf8_decode($level[9][1]))).'";
	$lang["add_'.$level[0].'"] = "'.$mylang['Add'].' '.htmlentities(html_entity_decode(utf8_decode($level[9][0]))).'";
	$lang["add_'.$level[0].'_title"] = "Create a New '.htmlentities(html_entity_decode(utf8_decode($level[9][0]))).'";
	$lang["edit_'.$level[0].'"] = "'.$mylang['Edit'].' '.htmlentities(html_entity_decode(utf8_decode($level[9][0]))).'";
	';

	foreach($level[4] as $field){
		if(!isset($field[7]))	$field[7] = ucfirst($field[0]);
		if(!in_array($field[0],$defaultfields))		$langfile .= '$lang["'.$level[0].'_'.$field[0].'"] = "'.htmlentities(html_entity_decode(utf8_decode($field[7]))).'";
	';
		if($field[1] == 6){
			$optnb = 0;
			$listoptions = explode(',',$field[5]['listoptions']);
			foreach($listoptions as $option){
				$langfile .= '$lang["'.$level[0].'_'.$field[0].'_option_'.$optnb.'"] = "'.trim($option).'";
	';
				$optnb++;
			}
		}elseif($field[1] == 8 || $field[1] == 9){
			$langfile .= '$lang["'.$level[0].'_select_'.$field[0].'"] = "'.$mylang['Select'].' '.htmlentities(html_entity_decode(utf8_decode($field[7]))).'";
	';
		}
	}

}

$langfile .= $mylang['general'];
// $langfile .= '?'.'>';
?>