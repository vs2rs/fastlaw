<?php
if(!isset($gCms)) exit;
if(	(!isset($params['infos']) || !isset($params['levels'])) && 
	(!isset($_SESSION['CTLMM']['infos']) || !isset($_SESSION['CTLMM']['levels']))	){
		$this->Redirect($id, 'defaultadmin', $returnid, array('module_message'=>'create error: No module data could be retrieved.'));
	}

if($this->GetPreference('usesessions',false)){
	$infos = $_SESSION['CTLMM']['infos'];
	$levels = $_SESSION['CTLMM']['levels'];
}else{
	$infos = $params['infos'];
	$levels = $params['levels'];
	if(!is_array($infos)) $infos = unserialize($infos);
	if(!is_array($levels)) $levels = unserialize($levels);
}
	
if(isset($params["submitfinal"])){
	// process the creation options:
	$infos['version'] = $this->ValidateVersion($params['version']);
	$infos["deletefiles"] = (isset($params["deletefiles"]) && $params["deletefiles"]);
	$infos["editablealias"] = (isset($params["editablealias"]) && $params["editablealias"]);
	
	if($this->GetPreference('autosave',true)){
		// Auto-backup:
		$this->CreateFile(dirname(__FILE__).DIRECTORY_SEPARATOR.'autosaved'.DIRECTORY_SEPARATOR.(isset($infos['nameofmodule'])?$infos['nameofmodule'].'_':'').'auto_export.dna',serialize(array("infos"=>$infos, "levels"=>$levels)));
	}	

	$infos["dofeadd"] = isset($params['dofeadd'])?$params['dofeadd']:0;
	$infos["langfiles"] = isset($params['langfiles'])?$params['langfiles']:array('en_US.php');
	
	// create the module
	$this->createAll($id, $returnid, $infos, $levels);
	
}else{

	// ADDING THE TEMPLATE FIELD TO THE APPROPRIATE LEVEL
	if(!isset($infos['templatelevel'])){
		$infos['templatelevel'] = isset($params['templatelevel'])?$params['templatelevel']:0;
		if($infos['templatelevel'] != 0)	$levels[$infos['templatelevel'] - 1][4][] = array('template',5,0,0,'');
	}
	
	if(isset($params['withupgrade'])){
		$infos['doupgrade'] = true;
	}else{
		$infos['doupgrade'] = false;
		// we get rid of the upgrade info, otherwise they're going to stack up someday
		$infos['upgradequeries'] = array();
		$infos['modifications'] = array();
	}
	if(!isset($infos['nameofmodule'])) $infos['nameofmodule'] = $infos['name'];
	
	echo $this->CreateFormStart($id, 'createModule', $returnid, 'post');
	
	if($this->GetPreference('usesessions',false)){
		$_SESSION['CTLMM']['infos'] = $infos;
		$_SESSION['CTLMM']['levels'] = $levels;
	}else{
		echo $this->CreateInputHidden($id, 'infos', serialize($infos));
		echo $this->CreateInputHidden($id, 'levels', serialize($levels));
	}
	
	// CREATION OPTIONS :
	echo '<h3>'.$this->Lang('almostdone').'</h3>';
	
	/*
	$levelchoices = array();
	foreach($levels as $level)	$levelchoices[$level[0]] = $level[0];
	//echo $this->DoInputLine('frontendadd',$this->DoCheckboxes($id, 'frontendadd', $levelchoices));
	//echo $this->DoInputLine('searchaction',$this->DoCheckboxes($id, 'searchaction', $levelchoices, $levelchoices));
	*/
	
	// Lang Files
	$langoptions = array();
	$dh  = opendir(dirname(__FILE__).DIRECTORY_SEPARATOR.'createdlang');
	while (false !== ($langfile = readdir($dh))){
		if(strtolower(strrchr($langfile, ".")) == '.php'){
			$thislang = substr($langfile, 0, 5);
			$langoptions[$thislang] = $langfile;
		}
	}
	closedir($dh);

	$YesNoOptions = array($this->Lang('No')=>0,$this->Lang('Yes')=>1);
	
	$hasfilefields = false;
	foreach($levels as $level){
		foreach($level[4] as $field){
			if(in_array($field[1],array(8,9,11)))	$hasfilefields = true;
		}
	}
	if($hasfilefields)	echo $this->DoInputLine('deletefiles',$this->CreateInputDropdown($id, 'deletefiles', $YesNoOptions, -1, 0));
	
	// this is now in the settings
	//echo $this->DoInputLine('editablealias',$this->CreateInputDropdown($id, 'editablealias', $YesNoOptions, -1, 0));
	
	echo $this->DoInputLine('langfiles',$this->DoCheckboxes($id, 'langfiles', $langoptions, array('en_US.php')));
	echo $this->DoInputLine('version',$this->CreateInputText($id, 'version', isset($infos['version'])?$infos['version']:'1.0', 10));
	echo $this->DoInputLine('dofeadd',$this->CreateInputDropdown($id, 'dofeadd', $YesNoOptions, -1, 0));

	echo '<p>'.$this->CreateInputSubmit($id, 'submitfinal', $this->Lang('Next'),'','',$this->Lang('warning_laststep')).'</p>';

	echo $this->CreateFormEnd();
}

$this->getInnerDebug(array(), $levels);

?>
