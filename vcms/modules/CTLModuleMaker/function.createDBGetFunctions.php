<?php

$modulename = $infos['nameofmodule'];
$modulfriendlyname = $infos['friendlyname'];
$levelarray = '';
foreach($levels as $level) $levelarray .= ($levelarray == ''?'':',').'"'.$level[0].'"';

$dbget = '
	function get_levelarray(){
		// returns an array of the levels (top to bottom)
		return array('.$levelarray.');
	}

	function get_modulehierarchy($level=false){
		$hierarchy = array();';
	$i = 0;
	foreach($levels as $level){
		$i++;
		$dbget .= '
			$hierarchy['.$i.'] = "'.$level[0].'";';
	}
	$dbget .= '
		return $hierarchy;
	}
	
	function get_levelsearchfields($level){
		// returns the field of a level which are searchable (for the simple search action)
		$fields = array();
		switch($level){
			';
foreach($levels as $level){			
	$fields = '';
	foreach($level[4] as $field){
		$isindexed = (isset($field[5]['innersearch']) && !$field[5]['innersearch'])?false:true;
		if($isindexed && !in_array($field[0],array('id','item_order','isdefault','active','parent','alias')) && $field[1] < 6){
			$fields .= ($fields == ''?'':',').'"'.$field[0].'"';
		}
	}
	$dbget .= 'case "'.$level[0].'":
				$fields = array('.$fields.');
				break;
			';
}
$dbget .= '			
		}
		return $fields;
	}
';

if(!$infos['is_shared']){
	$dbget .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'get_hierarchyoptions.php');
	$dbget .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'get_admin_hierarchyoptions.php');
}	

if($infos['templatelevel'] != 0 && $infos['templatelevel'] != $infos['nblevels']){
	$dbget .= '
	function get_finaltemplate($currenttable, $parentid){
		// goes from parent to parent until the template level is reached, and return the according template
		$db =& $this->GetDb();
		$templatelevel = '.$infos['templatelevel'].';
		$hierarchy = $this->get_modulehierarchy();
		$currentlevel = count($hierarchy) - 1;
		while($currentlevel > $templatelevel){
			$query = "SELECT parent FROM ".cms_db_prefix()."module_'.$modulename.'_".$hierarchy[$currentlevel]." WHERE id=?";
			$dbresult = $db->Execute($query,array($parentid));
			if ($dbresult && $row = $dbresult->FetchRow()){
				$parentid = $row["parent"];
			}else{
				return false;
			}
			$currentlevel = $currentlevel - 1;
		}
		$query = "SELECT template FROM ".cms_db_prefix()."module_'.$modulename.'_".$hierarchy[$currentlevel]." WHERE id=?";
		$dbresult = $db->Execute($query,array($parentid));
		if ($dbresult && $row = $dbresult->FetchRow()){
			return $row["template"];
		}else{
			return false;
		}
	}
';
}

$dbget .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'get_moduleGetVars.php');
	
if( !isset($infos['is_shared']) || !$infos['is_shared'] ){
	// we only need these functions if we aren't sharing children
	$dbget .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'get_objtree.php');
	$dbget .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'buildGlobalTree.php');
}

$dbget .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'countsomething.php');
$dbget .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'admin_paginate.php');
$dbget .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'get_distancetolevel.php');
$dbget .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'get_nextlevel.php');
$dbget .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'get_parents.php');
$dbget .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'get_options.php');
$dbget .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'get_fieldoptions.php');
$dbget .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'get_pageid.php');
$dbget .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'addfrontendurls.php');
$dbget .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'addadminlinks.php');


$pdoptionsarr = array();
$pdinversearr = array();
foreach($predefinedlists as $list){
	// 	$list[] = array(levelprefix, fieldname, listoptions)
	$pdoptions = '';
	$pdinverse = '';
	$i = 0;
	$list[2] = explode(',',$list[2]);
	foreach($list[2] as $option){
		$pdoptions .= ($pdoptions == ''?'':', ').'$this->Lang("'.$list[0].'_'.$list[1].'_option_'.$i.'")=>"'.$i.'"';
		$pdinverse .= ($pdinverse == ''?'':', ').'"'.$i.'"=>$this->Lang("'.$list[0].'_'.$list[1].'_option_'.$i.'")';
		$i++;
	}
	$pdoptionsarr[$list[0].'_'.$list[1]] = $pdoptions;
	$pdinversearr[$list[0].'_'.$list[1]] = $pdinverse;
}

if(count($pdinversearr) > 0){
$dbget .= '
	function get_predefinedoptions($field,$retrieve=false){
		// return the options for a predefined list
		if($retrieve){
			switch($field){';
	foreach($pdinversearr as $key=>$value){
		$dbget .= '
				case "'.$key.'":
					return array('.$value.');
					break;';
	}
$dbget .= '
			}
		}else{
			switch($field){';
	foreach($pdoptionsarr as $key=>$value){
		$dbget .= '
				case "'.$key.'":
					return array('.$value.');
					break;';
	}
$dbget .= '
			}
		}
	}
';
}

if(count($multiplefiles) > 0){
	$dbget .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'getaddfiles.php');
}

$dbget .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'split_into_pages.php');
$dbget .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'get_queries.php');

$dbget .= '
	function createFieldForm($what,$id,$assign=true){
		require "function.createFieldForm.php";
	}
';

	$multiplelistfields = '';
	foreach($levels as $level){
		foreach($level[4] as $field){
			if(in_array($field[1],array(6,7)) && isset($field[5]) && isset($field[5]['listmode']) && ($field[5]['listmode'] == 2 || $field[5]['listmode'] == 4) )
				$multiplelistfields .= ($multiplelistfields == ''?'':',').'"'.$level[0].'_'.$field[0].'"';
		}
	}
$dbget .= '	

	function buildWhere($where=array(),$what=""){
		$multiplelistfields = array('.$multiplelistfields.');
		$whereclause = "A.active=1";
		$wherevalues = array();
		foreach($where as $clause){
			if(in_array($what."_".$clause[0], $multiplelistfields)){
				// list with multiple selected values are saved in a serialized array, so we have to tweak the value
				$clause[1] = \'"\'.$clause[1].\'"\';
				$clause[2] = 1;
			}
			if($clause[0] == "parent"){
			';
if(!$infos["is_shared"]){
	$dbget .= '		$criteria = explode(",",$clause[1]);
					if(count($criteria) > 1){
						$tmp = "";
						foreach($criteria as $crit){
							$tmp = ($tmp==""?"":" OR ")."A.parent=".$crit;
						}
						$tmp = " AND (".$tmp.")";
					}else{
						$tmp = " AND A.parent=?";
						$wherevalues[] = $clause[1];
					}
					$whereclause .= $tmp;
			';
}else{
	$dbget .= '	$whereclause .= " AND A.".$clause[0]."=?";
				$wherevalues[] = addslashes($clause[1]);
			';
}
	$dbget .= '	
			}else{
				switch($clause[2]){
					case 0:
						$whereclause .= " AND A.".$clause[0]."=?";
						$wherevalues[] = addslashes($clause[1]);					
						break;
					
					case 1:
						$whereclause .= " AND A.".$clause[0]." LIKE \'%".addslashes($clause[1])."%\'";
						break;
					
					case 2:
						$whereclause .= " AND A.".$clause[0]." != ?";
						$wherevalues[] = addslashes($clause[1]);
						break;
						
					case 3:
						$whereclause .= " AND A.".$clause[0]." > ?";
						$wherevalues[] = addslashes($clause[1]);					
						break;
						
					case 4:
						$whereclause .= " AND A.".$clause[0]." < ?";
						$wherevalues[] = addslashes($clause[1]);					
						break;
					case 5:
						$whereclause .= " AND (A.".$clause[0]." > ? AND A.".$clause[0]." < ?)";
						$wherevalues[] = addslashes($clause[1][0]);
						$wherevalues[] = addslashes($clause[1][1]);
						break;					
				}
			}
		}
		return array($whereclause, $wherevalues);
	}
';

$dbget .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'getWhereFromParams.php');

require 'function.createLevelGetFunctions.php';

?>
