<?php
$helps = array();
$i = 0;
while($i <= count($levels)){
	$sharedbyparents = (isset($levels[$i-1]) && $levels[$i-1][2]);
	$help = '<ul>
	<li>$leveltitle</li>
	';
	if($i == count($levels)){
		$level = $levels[$i - 1];
		$level[0] = 'final_level_template';
		$help .= '<li>$previous_item (if loaded)</li>
	<li>$next_item (if loaded)</li>
	';
	}else{
		$level = $levels[$i];
		$help .= '<li>$parentobj (if parent is specified)</li>
	';
		if(!$sharedbyparents)	$help .= '<li>$itemlist (array of items)</li>
	<li>$item-&gt;is_selected</li>
	';
	}
	if($i < (count($levels)-1))	$help .= '<li>$item-&gt;nbchildren (if loaded)</li>
	';
	$help .= '<li>$item-&gt;name</li>
	<li>$item-&gt;alias</li>
	';
	if($i < count($levels)){
		$help .= '<li>$item-&gt;detaillink</li>
	<li>$item-&gt;detailurl</li>
	';
	}
	foreach($level[4] as $field){
		if($field[0] == 'item_order' || $field[0] == 'active' || $field[0] == 'id' || $field[0] == 'name' || $field[0] == 'alias'){
		}elseif($field[0] == 'parent' && !$sharedbyparents){
			$help .= '<li>$item-&gt;parent_id</li>
	<li>$item-&gt;parent_alias</li>
	<li>$item-&gt;parent_name</li>
	<li>$item-&gt;parentlink</li>
	<li>$tiem-&gt;parenturl</li>
	';
		}elseif($field[1] == 8 || $field[1] == 9){
			$help .= '<li>$item-&gt;'.$field[0].' (file object)</li>
	';			
		}elseif($field[1] == 11 || $field[1] == 13){
			$help .= '<li>$item-&gt;'.$field[0].' (array of file objects)</li>
	';
		}else{
			$help .= '<li>$item-&gt;'.$field[0].'</li>
	';
		}
		if($field[1] == 6 || $field[1] == 7){
			$help .= '<li>$item-&gt;'.$field[0].'_namevalue</li>
	';
		}
	}
	$help .= '<li>$item-&gt;date_modified</li>
	<li>$item-&gt;date_created</li>
	';
	if($level[0] == 'final_level_template')	$help .= '<li>$labels->...</li>
	';
	$help .= '</ul>';

	$helps[$level[0]] = $help;
	$i++;
}

?>
