<?php
if(!isset($level) || !isset($infos)) exit;

$prefix = 'feadd_';

$phpfile = '<?php
if (!isset($gCms)) exit;

// we retrieve some preferences
$decode = $this->GetPreference("fe_decodeentities",false);
$wysiwyg = $this->GetPreference("fe_wysiwyg", false);
$allowfiles = $this->GetPreference("fe_allowfiles", false);
$allownamechange = $this->GetPreference("fe_allownamechange", false);
$allowaddnew = $this->GetPreference("fe_allowaddnew", false);
$filemaxsize = (int)$this->GetPreference("fe_maxfilesize", "");

$captcha = false;
if($this->GetPreference("fe_usecaptcha", false) && isset($gCms->modules["Captcha"]) && $gCms->modules["Captcha"]["active"]){
	$captcha = $this->getModuleInstance("Captcha");
}

$db =& $this->GetDb();

$item = new stdClass();
$item->id = false;

// here we retrieve the item we\'re working with.
// We can\'t work with either id or alias alone, as it is alias that are being used in the tags but alias can change...
if(!isset($params["alias"]) && isset($params["'.$prefix.'id"])){
	$query = "SELECT id, alias FROM ".cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$level[0].' WHERE id=? LIMIT 1";
	$dbresult = $db->Execute($query,array($params["'.$prefix.'id"]));
	if($dbresult && $row = $dbresult->FetchRow()){
		$item->id = $row["id"];
		$item->alias = $row["alias"];
	}
}elseif(!isset($params["'.$prefix.'id"]) && isset($params["alias"])){
	$query = "SELECT id, alias FROM ".cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$level[0].' WHERE alias=? LIMIT 1";
	$dbresult = $db->Execute($query,array($params["alias"]));
	if($dbresult && $row = $dbresult->FetchRow()){
		$item->id = $row["id"];
		$item->alias = $row["alias"];
	}
}

// we check if there is sufficient permission ( function.feadd_permcheck.php does most of the job - see FAQ)
if(	(!$allowaddnew && !$item->id)	||
	(!$this->feadd_permcheck("'.$level[0].'", $item->id, $item->id?$item->alias:false))
  ){
	echo \'<div class="feadd_form_message">\'.$this->Lang("error_feadddenied")."</div>";
	return false;
}

if(isset($params["'.$prefix.'id"]) && isset($params["feaddfiledelete"])){
	$query = "DELETE FROM ".cms_db_prefix()."module_'.$infos['nameofmodule'].'_multiplefilesfields WHERE fileid=? AND itemid=? LIMIT 1";
	$db->Execute( $query, array($params["feaddfiledelete"], $item->id) );
	unset($params["feaddfiledelete"]);
}

';

if($parentname){
	if($infos['is_shared']){
		$phpfile .= '$parentoptions = $this->get_options("'.$parentname.'");
';
	}else{
		$phpfile .= '
if($this->GetPreference("use_hierarchy",false)){
	$parentoptions = $this->get_admin_hierarchyoptions("'.$level[0].'",false);
}else{
	$parentoptions = $this->get_options("'.$parentname.'");
}';
	}
}
if($parentname != false && $sharedbyparents)	$phpfile .= '$selectedparents = ($item->id?$this->get_parents("'.$parentname.'","'.$level[0].'",$item->id):array());
';

###################################################
## PART I : FORM SUBMISSION...  You're really sure you want to get into this? Sorry for the lack of comprehension...

// parsing fields :
$filefields = array();
$neededfields = '';
$indexedfields = '';
$retrievefields = '';
$retrievefiles = '';
$sqlfields = '';
$sqlvalues = '';
$multiplefiles = array();
foreach($level[4] as $field){
	switch($field[1]){
		case 6:
			$phpfile .= '$'.$field[0].'options = $this->get_predefinedoptions("'.$level[0].'_'.$field[0].'");
';
			break;
		case 7:
			$phpfile .= '$'.$field[0].'options = $this->get_fieldoptions("'.$level[0].'_'.$field[0].'");
';
			break;
		case 8:
		case 9:
			$filefields[] = array($level[1],$field[0],($field[1] == 8?'image':'file'),$field[5]['upfolder'],$field[5]['size'],$field[5]['thumb'],$field[5]['crop'],$field[5]['cropthumb']);
			break;
		case 11:
		case 13:
			$multiplefiles[] = $field;
			break;
	}
	if(!in_array($field[0],array('id','item_order','isdefault','active','alias')) && ($field[0] != 'parent' || !$sharedbyparents)) {
		if($field[1] == 10){
			$retrievefields .= '$item->'.$field[0].' = time();
		if (isset($params["'.$prefix.'_'.$field[0].'_Month"])) {
			$item->'.$field[0].' = mktime($params["'.$prefix.'_'.$field[0].'_Hour"], $params["'.$prefix.'_'.$field[0].'_Minute"], $params["'.$prefix.'_'.$field[0].'_Second"], $params["'.$prefix.'_'.$field[0].'_Month"], $params["'.$prefix.'_'.$field[0].'_Day"], $params["'.$prefix.'_'.$field[0].'_Year"]);
		}
		$item->'.$field[0].' = str_replace("\'","",$db->DBTimeStamp($item->'.$field[0].'));
		';
		}elseif($field[1] == 12){
			$retrievefields .= '$item->'.$field[0].' = isset($params["'.$prefix.$field[0].'"])?$params["'.$prefix.$field[0].'"]:0;
		';	
		}elseif($field[4] != ''){
			$retrievefields .= '$item->'.$field[0].' = (isset($params["'.$prefix.$field[0].'"]) && $params["'.$prefix.$field[0].'"] != "")?$params["'.$prefix.$field[0].'"]:"'.$field[4].'";
		';		
		}elseif(in_array($field[1],array(8,9,11,13))){
			$imgfield = ($field[1] == 8 || $field[1] == 13);
			$multi = ($field[1] == 11 || $field[1] == 13);
			$retrievefiles .= 'if(isset($item->id) && isset($_FILES) && isset($_FILES[$id."fefile_'.$field[0].'"]) && $_FILES[$id."fefile_'.$field[0].'"]["name"] != ""){
			if($filemaxsize && $filemaxsize > 0 && $_FILES[$id."fefile_'.$field[0].'"]["size"] > $filemaxsize){
				echo \'<div class="feadd_form_message">\'.$this->Lang("error_filetoobig")."</div>";
			}else{
				$extension = strtolower(substr(strrchr($_FILES[$id."fefile_'.$field[0].'"]["name"], "."),1));
				$allowedext = ';
				if(isset($field[5]['fileext']) && $field[5]['fileext'] && trim($field[5]['fileext']) != ''){
					$allowedext = explode(',',$field[5]['fileext']);
					$tmpext = '';
					foreach($allowedext as $ext)	$tmpext .= ($tmpext==''?'':',').'"'.str_replace('"','',$ext).'"';
					$retrievefiles .= 'array('.$tmpext.');';
				}else{
					$retrievefiles .= 'false;';
				}
				$retrievefiles .= '
				if( !$allowedext || in_array($extension, $allowedext) ){
					if( $filepath = $this->plUploadFile($_FILES[$id."fefile_'.$field[0].'"], "'.$field[5]['upfolder'].'", '.($imgfield?'"'.$field[5]['size'].'"':'false').', '.($field[5]['crop']?'true':'false').') ){
						$this->plAssignFile($filepath, "'.$infos['nameofmodule'].'_'.($multi?'multiplefilesfields':$level[0]).'", $item->id, "'.($multi?$level[0].'_':'').$field[0].'", '.($imgfield?'"'.$field[5]['thumb'].'"':'false').', '.($field[5]['cropthumb']?'true':'false').');
					}
				}
			}
		}
		';
		}else{
			$retrievefields .= '$item->'.$field[0].' = $decode?html_entity_decode($params["'.$prefix.$field[0].'"]):$params["'.$prefix.$field[0].'"];
		';
		}
		if(!in_array($field[1],array(8,9,11,13))){
			$sqlfields .= ($sqlfields == ''?'':',
				').$field[0].'=?';
			$sqlvalues .= ($sqlvalues == ''?'':',
				');
			if($field[1] < 5 && $field[1] != 0){
				$sqlvalues .= 'addslashes($item->'.$field[0].')';
			}elseif( ($field[1] == 6 || $field[1] == 7) && ($field[5]['listmode'] == 2 || $field[5]['listmode'] == 4) ) {
				$sqlvalues .= 'serialize($item->'.$field[0].')';
			}else{
				$sqlvalues .= '$item->'.$field[0];
			}
			if($field[2] == 1 && $field[1] != 10 && $field[1] != 12) $neededfields .= ($neededfields == ''?'':' || ').'!isset($params["'.$prefix.$field[0].'"]) || $params["'.$prefix.$field[0].'"] == ""
			';
		}
		if($field[3] == 1 && !in_array($field[1],array(11,13)))	$indexedfields .= ($indexedfields == ''?'':' ').'$item->'.$field[0];
	}
}
$sqlfields .= ',
		alias=?,
		date_modified=?,
		active=".(isset($item->active)?$item->active:1).",
		isdefault=".(isset($item->isdefault)?$item->isdefault:0)."';
$sqlvalues .= ',$item->alias,str_replace("\'","",$db->DBTimeStamp(time()))';

$redirect = '';

if(count($multiplefiles) > 0){
$phpfile .= '
if( isset($params["deletefile"]) && $params["deletefile"] > 0 ){
	// this is for the multiple files fields (additionnal tabs)
	$query = "DELETE FROM ".cms_db_prefix()."module_'.$infos['nameofmodule'].'_multiplefilesfields WHERE fileid=? LIMIT 1";
	if($db->Execute( $query, array($params["deletefile"]) ))	echo $this->ShowMessage($this->Lang("message_modified"));
}
';
}

$phpfile .= '

// BEGIN FORM SUBMISSION
if (isset($params["feaddsubmit"])) {
	
	debug_buffer("Edit Form has been submitted".__LINE__);

	// RETRIEVING THE FORM VALUES (and escaping it, if needed)
	if(isset($params["'.$prefix.'item_order"])) $item->item_order = $params["'.$prefix.'item_order"];
	'.$retrievefields.'
	$item->alias = $this->plcreatealias($item->name);
	';
if($parentname != false && $sharedbyparents) $phpfile .= '$selectedparents = isset($params["'.$prefix.'parent"])?$params["'.$prefix.'parent"]:array();
	';

$phpfile .= '
	$autoincrementalias = $this->GetPreference("autoincrement_alias",false);

	if($captcha && !$captcha->checkCaptcha($params["captcha_input"])){
		echo \'<div class="feadd_form_message">\'.$this->Lang("error_captcha")."</div>";
	}elseif(	'.$neededfields.' )
	{
		echo \'<div class="feadd_form_message">\'.$this->Lang("error_missginvalue")."</div>";
	}elseif(!$autoincrementalias && false == $this->checkalias("module_'.$infos['nameofmodule'].'_'.$level[0].'", $item->alias, $item->id?$item->id:false)){
		echo \'<div class="feadd_form_message">\'.$this->Lang("error_alreadyexists")."</div>";
	}else{
		############ DOING THE UPDATE

		if($autoincrementalias){
			$basealias = $item->alias;
			$tmpalias = $item->alias;
			$i = 1;
			while(!$this->checkalias("module_'.$infos['nameofmodule'].'_'.$level[0].'", $tmpalias, $item->id?$item->id:false)){
				$tmpalias = $basealias."_".$i;
				$i++;
			}
			$item->alias = $tmpalias;
		}

		// FIELDS TO UPDATE
		$query = ($item->id?"UPDATE ":"INSERT INTO ").cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$level[0].' SET 
			'.$sqlfields.'";
			
		// VALUES
		$values = array('.$sqlvalues.');

		if($item->id){
			$event = "'.$infos['nameofmodule'].'_modified";
			$query .= " WHERE id=?;";
			array_push($values,$item->id);
		}else{
			// NEW ITEM
			$event = "'.$infos['nameofmodule'].'_added";
			$query .= ", date_created=?";
			$values[] = str_replace("\'","",$db->DBTimeStamp(time()));
			// get a new id from the sequence table
			$item->id = $db->GenID(cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$level[0].'_seq");
			if($this->GetPreference("newitemsfirst_'.$level[0].'",false)){
				// new items get to the top - so we must put all other items down from one step, and then set this item\'s order to 1
				$query2 = "UPDATE ".cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$level[0].' SET item_order=(item_order+1)'.(($level[6] && $parentname != false && $sharedbyparents == 0)?' WHERE parent=?':'').'";
				$db->Execute($query2'.(($level[6] && $parentname != false && $sharedbyparents == 0)?', array($item->parent)':'').');
				$query .= ",item_order=1, id=".$item->id;
			}else{
				// new items get to the bottom - so we must set the item_order to the number of items + 1
				$item_order = $this->countsomething("'.$level[0].'"'.(($level[6] && $parentname != false && $sharedbyparents == 0)?',"id",array("parent"=>$item->parent)':'').') + 1;
				$query .= ",item_order=".$item_order.", id=".$item->id;
			}
		}
		$db->Execute($query, $values);
';
if($level[6] && $parentname != false && $sharedbyparents == 0){
	$phpfile .= '
	if(isset($params["'.$prefix.'oldparent"]) && $params["'.$prefix.'oldparent"] != $item->parent){
		// the item is changing parent, and we\'re ordering by parents
		
		if($this->GetPreference("newitemsfirst_'.$level[0].'",false)){
			// new items get to the top
			
			// UPDATE THE ORDER OF THE ITEMS WITH THE OLD PARENT
			$query = "UPDATE ".cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$level[0].' SET item_order=(item_order-1) WHERE item_order > ? AND parent=?";
			$db->Execute($query, array($item->item_order, $params["'.$prefix.'oldparent"]));
			// GET NEW ITEM ORDER
			$item->item_order = $this->countsomething("'.$level[0].'","id",array("parent"=>$item->parent)) + 1;
			$query = "UPDATE ".cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$level[0].' SET item_order=? WHERE id=?";
			$db->Execute($query, array($item->item_order, $item->id));

		}else{

			// UPDATE THE ORDER OF THE ITEMS WITH THE OLD PARENT
			$query = "UPDATE ".cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$level[0].' SET item_order=(item_order-1) WHERE item_order > ? AND parent=?";
			$db->Execute($query, array($item->item_order, $params["'.$prefix.'oldparent"]));
			// UPDATE NEW PARENT
			$query = "UPDATE ".cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$level[0].' SET item_order=(item_order+1) WHERE parent=?";
			$db->Execute($query, array($item->parent));
			// GET NEW ITEM ORDER
			$item->item_order = 1;
			$query = "UPDATE ".cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$level[0].' SET item_order=? WHERE id=?";
			$db->Execute($query, array($item->item_order, $item->id));
		
		}
	}
';
}

$phpfile .= '

		'.$retrievefiles.'

		if($db->Affected_Rows()){
			if($this->GetPreference("searchmodule_index_'.$level[0].'",false)){
				// IF ANYTHING WAS MODIFIED, WE MUST UPDATE THE SEARCH INDEX
				$module =& $this->GetModuleInstance("Search");
				if($module) {
					debug_buffer("SEARHC INDEX WAS UPDATED ".__LINE__);
					$text = "'.$indexedfields.'";
					$module->AddWords($this->GetName(), $item->id, "'.$level[0].'", $text, NULL);
				}
			}
			echo \'<div class="feadd_form_message">\'.$this->Lang("message_modified")."</div>";
		}
		';

if($sharedbyparents && $parentname != false){
	$phpfile .= '$query = "DELETE FROM ".cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$parentname.'_has_'.$level[0].' WHERE '.$level[0].'_id=?";
		$db->Execute($query,array($item->id));
		foreach($selectedparents as $oneparent){
			$query = "INSERT INTO ".cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$parentname.'_has_'.$level[0].' SET '.$level[0].'_id=?, '.$parentname.'_id=?";
			$db->Execute($query,array($item->id,$oneparent));
		}

		';
}

$phpfile .= '
		// if a content redirection has been set, we redirect...
		$redirect_to_id = $this->GetPreference("fe_aftersubmit",-1);
		if( $redirect_to_id > 1 )	$this->RedirectContent($redirect_to_id);
	}
	// END OF FORM SUBMISSION
	
	if(!isset($params["'.$prefix.'id"]) && isset($item->id))	$params["'.$prefix.'id"] = $item->id;
}

if($item->id) {
	// if we are working on an item that exists, we load it. We must do this even when the form is submitted, otherwise we won\'t have the file fields
	$items = $this->get_level_'.$level[0].'(array("id"=>$item->id));
	$item = $items[0];
}

';

/*###################################################
## PART II : PREPARING INPUTS & DISPLAY
*/
$phpfile .= '
/* ## PREPARING SMARTY ELEMENTS
CreateInputText : (id,name,value,size,maxlength)
CreateTextArea : (wysiwyg,id,text,name)
CreateInputSelectList : (id,name,items,selecteditems,size)
CreateInputDropdown : (id,name,items,sindex,svalue)
*/

if(!$item->id || $allownamechange){
	$nameinput = $this->CreateInputText($id,"'.$prefix.'name",$item->id?$item->name:"",50,64);
}else{
	$nameinput = $item->name.$this->CreateInputHidden($id, "'.$prefix.'name", $item->name);
}
$this->smarty->assign("name_label", $this->Lang("name"));
$this->smarty->assign("name_input", $nameinput);';

foreach($level[4] as $field){
	if(!in_array($field[0],array('name','id','item_order','isdefault','active','parent','alias')) && $field[1] != 10) {
		$fieldlabel = $level[0].'_'.$field[0];
		$phpfile .= '
$this->smarty->assign("'.$field[0].'_label", $this->Lang("'.$fieldlabel.'"));
$this->smarty->assign("'.$field[0].'_input", ';
		switch($field[1]){
			case 0: case 1:
				$phpfile .= '$this->CreateInputText($id,"'.$prefix.$field[0].'",$item->id?$item->'.$field[0].':"",30,10));';
				break;
			case 2:
				$phpfile .= '$this->CreateInputText($id,"'.$prefix.$field[0].'",$item->id?$item->'.$field[0].':"",30,32));';				
				break;
			case 3:
				$phpfile .= '$this->CreateInputText($id,"'.$prefix.$field[0].'",$item->id?$item->'.$field[0].':"",50,64));';				
				break;
			case 5:
				$phpfile .= '$this->CreateTextArea('.($field[0] == 'template'?'false':'$wysiwyg').',$id,$item->id?$item->'.$field[0].':"","'.$prefix.$field[0].'"));';				
				break;
			case 6: case 7:
				if($field[5]['listmode'] == 1){
					$phpfile .= '$this->CreateInputDropdown($id,"'.$prefix.$field[0].'",$'.$field[0].'options,-1,$item->id?$item->'.$field[0].':0));';
				}elseif($field[5]['listmode'] == 2){
					$phpfile .= '$this->CreateInputSelectList($id,"'.$prefix.$field[0].'[]",$'.$field[0].'options,$item->id?$item->'.$field[0].':array()));';
				}elseif($field[5]['listmode'] == 3){
					$phpfile .= '$this->CreateInputRadioGroup($id, "'.$prefix.$field[0].'",$'.$field[0].'options,$item->id?$item->'.$field[0].':"", "", "<br/>"));';
				}elseif($field[5]['listmode'] == 4){
					$phpfile .= '$this->DoCheckboxes($id, "'.$prefix.$field[0].'", $'.$field[0].'options, $item->id?$item->'.$field[0].':0));';
				}
				break;
			case 8:
				$phpfile .= '((isset($item->'.$field[0].') && $item->'.$field[0].')?\'<img src="\'.$item->'.$field[0].'->url.\'" /><br/>\':"").($allowfiles?$this->CreateFileUploadInput($id,"fefile_'.$field[0].'"):""));';
				break;
			case 9:
				$phpfile .= '((isset($item->'.$field[0].') && $item->'.$field[0].')?$item->'.$field[0].'->pic.\'<a href="\'.$item->'.$field[0].'->url.\'" >\'.$item->'.$field[0].'->filepath."</a> (".$item->'.$field[0].'->size_wformat.")<br/>":"").($allowfiles?$this->CreateFileUploadInput($id,"fefile_'.$field[0].'"):""));';
				break;
			case 12:
				$phpfile .= '$this->CreateInputCheckbox($id,"'.$prefix.$field[0].'",1,$item->id?$item->'.$field[0].':0));';
				break;
			case 11:
			case 13:
				$phpfile .= '$allowfiles?$this->CreateFileUploadInput($id,"fefile_'.$field[0].'"):false);';
				break;
			default:
				$phpfile .= '$this->CreateInputText($id,"'.$prefix.$field[0].'",$item->id?$item->'.$field[0].':"",50,255));';
				break;				

		}
	}elseif($field[1] == 10){
		$fieldlabel = $level[0].'_'.$field[0];
		$phpfile .= '
$this->smarty->assign("'.$field[0].'_label", $this->Lang("'.$fieldlabel.'"));
$this->smarty->assign("'.$prefix.'_'.$field[0].'", $item->id?$db->UnixTimeStamp($item->'.$field[0].'):time());
$this->smarty->assign("'.$prefix.'_'.$field[0].'_prefix", $id."'.$prefix.'_'.$field[0].'_");';

	}

}

if($parentname){
	$phpfile .= '
$this->smarty->assign("parent_label", $this->Lang("'.$parentname.'"));
$this->smarty->assign("parent_input", ';
	if($sharedbyparents){
		$phpfile .= '$this->CreateInputSelectList($id,"'.$prefix.'parent[]",$parentoptions,isset($selectedparents)?$selectedparents:array()));';
	}else{
		$phpfile .= '$this->CreateInputDropdown($id,"'.$prefix.'parent",$parentoptions,-1,$item->id?$item->parent:0));';
	}
}

$phpfile .= '
$this->smarty->assign("itemalias",isset($item->alias)?"(alias : ".$item->alias.")":false);
$this->smarty->assign("alias_input", false);

$this->smarty->assign("edittitle", $this->Lang("edit_'.$level[0].'"));

$this->smarty->assign("submit", $this->CreateInputSubmit($id, "feaddsubmit", $this->Lang("submit")));
$this->smarty->assign("cancel", $this->CreateInputSubmit($id, "feaddcancel", $this->Lang("cancel")));
$this->smarty->assign("captcha_image", $captcha?$captcha->getCaptcha():false);
$this->smarty->assign("captcha_input", $captcha?$this->CreateInputText($id,"captcha_input","",30,10):false);
$this->smarty->assign("captcha_prompt", $captcha?$this->Lang("prompt_captcha"):false);

';

if(count($multiplefiles) > 0){
$phpfile .= '
if(isset($item) && isset($item->id)){';
foreach($multiplefiles as $field){
	$phpfile .= '
	$item->'.$field[0].' = $this->getaddfiles("'.$level[0].'_'.$field[0].'",$item->id);
	$i = 0;
	$newparams = $params;
	while($i < count($item->'.$field[0].')){
		$newparams["feaddfiledelete"] = $item->'.$field[0].'[$i]->fileid;
		$item->'.$field[0].'[$i]->deletelink = $this->CreateLink($id, "FEadd'.$level[1].'", $returnid, $this->lang("delete"), $newparams);
		$i++;
	}';
}
$phpfile .= 
'
}
';
}

$phpfile .= '
$this->smarty->assign("item", $item);


// DISPLAYING
	
echo $this->CreateFormStart($id, "FEadd'.$level[1].'", $returnid, "post", "multipart/form-data");
echo $this->ProcessTemplate("frontend_add_'.$level[0].'.tpl");

if(isset($item) && isset($item->id)){
	echo $this->CreateInputHidden($id, "'.$prefix.'id", $item->id);
	if(isset($item->parent)) echo $this->CreateInputHidden($id, "'.$prefix.'oldparent", $item->parent);
	echo $this->CreateInputHidden($id, "'.$prefix.'item_order", $item->item_order);
}
echo $this->CreateFormEnd();

?>';


?>
