<?php
if(!isset($levels)) exit;
########################################################
## LEVELS

$i = 0;
$sharedbyparents = false;
while($i < count($levels)){
	if($i != 0){
		$sharedbyparents = ($levels[$i - 1][2] == 1);
		$parentname = $levels[$i - 1][0];
	}
	$fieldnames = '';
	foreach($levels[$i][4] as $field)	$fieldnames .= ($fieldnames==''?'':',').'"'.strtolower($field[0]).'"';
	$level = $levels[$i];
	$dbget .= '
	function get_level_'.$level[0].'($where=array(),$admin=false,$id="",$returnid="",$order=false,$limit=0,$customwhere=false,$customvalues=array(),$customorder=false){
		global $gCms;
		$load_nbchildren = $this->GetPreference("load_nbchildren",true);
		if($admin)	$admintheme = $gCms->variables["admintheme"];
		if(!$order)	$order = "";
		$db =& $this->GetDb();
		
		// wx
		$fields = $this->get_level_'.$level[0].'_fields("keys");
		
		$wherestring = "";
		$wherevalues = array();
		
';
	if($i == 0){
	// THIS IS THE HIGHEST LEVEL
		$dbget .= '
		if($customwhere){
			$wherestring = $customwhere;
			$wherevalues = $customvalues;
		}else{
			foreach($where as $key=>$value){
				if(in_array(strtolower($key), $fields)){
					$wherestring .= ($wherestring == ""?"":" AND ").$key."=?";
					$wherevalues[] = $value;
				}
			}
		}
		$query = "SELECT * FROM ".cms_db_prefix()."module_'.$modulename.'_'.$level[0].' A ".($wherestring == ""?"":" WHERE ".$wherestring);
		$query .= ($customorder?" ORDER BY ".$customorder:" ORDER BY item_order");

		';
	

	}elseif($sharedbyparents == true){
	// THIS LEVEL MAY HAVE SEVERAL PARENTS
		$dbget .= '
		$parentalias = false;
		$parentid = false;
		if($customwhere){
			$wherestring = $customwhere;
			$wherevalues = $customvalues;
		}else{
			foreach($where as $key=>$value){
				if($key == "parent"){
					$parentalias = $value;
				}elseif($key == "parent_id"){
					$parentid = $value;
				}elseif(in_array($key, $fields)){
					$wherestring .= ($wherestring == ""?"":" AND ")."A.".$key."=?";
					$wherevalues[] = $value;
				}
			}
		}
		
		$query = "SELECT A.*";
		if($parentalias || $parentid) $query .= ", C.id parent_id, C.name parent_name, C.alias parent_alias";
		$query .= " FROM ".cms_db_prefix()."module_'.$modulename.'_'.$level[0].' A";
		if($parentalias || $parentid) $query .= " LEFT JOIN ".cms_db_prefix()."module_'.$modulename.'_'.$parentname.'_has_'.$level[0].' B ON A.id = B.'.$level[0].'_id LEFT JOIN ".cms_db_prefix()."module_'.$modulename.'_'.$parentname.' C ON B.'.$parentname.'_id = C.id";
		if($parentalias){
			$query .= " WHERE C.alias = \'$parentalias\'";
			if($wherestring != "") $query .= " AND ".$wherestring;
		}elseif($parentid){
			$query .= " WHERE C.id = \'$parentid\'";
			if($wherestring != "") $query .= " AND ".$wherestring;			
		}else{
			if($wherestring != "") $query .= " WHERE ".$wherestring;
		}
		
		if($customorder){
			$query .= " ORDER BY A.".$customorder;
		}elseif($order == "modified"){
			$query .= " ORDER BY A.date_modified DESC";
		}elseif($order == "created"){
			$query .= " ORDER BY A.date_created DESC";	
		}elseif( $order != "" && ($this->GetPreference("allow_complex_order",false) || in_array(strtolower($order), $fields)) ){
			$query .= " ORDER BY A.".$order;
		}else{
			$query .= " ORDER BY A.item_order";
		}

		';
	}elseif($sharedbyparents == false){
	// THIS LEVEL HAS ONLY ONE PARENT
		$dbget .= '
		if($customwhere){
			$wherestring = $customwhere;
			$wherevalues = $customvalues;
		}else{
			foreach($where as $key=>$value){
				if($key == "parent"){
					$wherestring .= ($wherestring == ""?"":" AND ")."B.alias=?";
					$wherevalues[] = $value;
				}elseif($key == "parent_id"){
					$wherestring .= ($wherestring == ""?"":" AND ")."B.id=?";
					$wherevalues[] = $value;
				}elseif(in_array(strtolower($key), $fields)){
					$wherestring .= ($wherestring == ""?"":" AND ")."A.".$key."=?";
					$wherevalues[] = $value;
				}
			}
		}
		
		$query = "SELECT A.*, B.id parent_id, B.name parent_name, B.alias parent_alias FROM ".cms_db_prefix()."module_'.$modulename.'_'.$level[0].' A, ".cms_db_prefix()."module_'.$modulename.'_'.$parentname.' B WHERE A.parent = B.id ".($wherestring == ""?"":" AND ").$wherestring;
		if($customorder){
			 $query .= " ORDER BY A.".$customorder;
		}elseif($order == "modified"){
			 $query .= " ORDER BY A.date_modified DESC";
		}elseif($order == "created"){
			 $query .= " ORDER BY A.id DESC";
		}elseif( $order != "" && ($this->GetPreference("allow_complex_order",false) || in_array(strtolower($order), $fields)) ){
			$query .= " ORDER BY A.".$order;
		}else{
			 $query .= " ORDER BY '.($level[6]?'B.item_order, ':'').'A.item_order";
		}
		';
	}

$dbget .= 'if($limit && $limit != "0" && $limit != "")	$query .= " LIMIT ".$limit;
';

// PREPARING nbchildren
	$childrencount = '
';
	if(!isset($levels[$i + 1])){
		$childrencount = '
			$item->nbchildren = false;
		';
	}elseif($level[2] == 1){
		$childrencount = '
			if($admin || $load_nbchildren){
				$item->nbchildren = $this->countsomething("'.$level[0].'_has_'.$levels[$i + 1][0].'","'.$levels[$i + 1][0].'_id",array("'.$level[0].'_id" => $item->id));
			}else{
				$item->nbchildren = false;
			}
		';
	}

// PREPARING FIELDS PARSING
	$parsedfields = '';
	$addfiles = array();
	$filefields = '';
	foreach($level[4] as $field){
		// Preparing list options
		if($field[1] == 6){
			$dbget .= '
			$options_'.$field[0].' = $this->get_predefinedoptions("'.$level[0].'_'.$field[0].'",true);';
		}elseif($field[1] == 7){
			$dbget .= '
			$options_'.$field[0].' = array_flip($this->get_fieldoptions("'.$level[0].'_'.$field[0].'"));';
		}
		if($field[1] == 8 || $field[1] == 9){
			$filefields .= ($filefields==''?'':',').$field[0];
			$parsedfields .= '
			if($item->'.$field[0].' != "" && $item->'.$field[0].' != "/"){
				$file = new StdClass();
				$file->filepath = (substr($item->'.$field[0].',0,1)=="/"?"":"/").$item->'.$field[0].';
				$file->url = $gCms->config["uploads_url"].$file->filepath;
				$info = $this->plGetFileInfo($gCms->config["uploads_path"].$file->filepath);
				foreach($info as $key=>$value)	$file->$key = $value;
				if($file->fileicon == "icons/filetypes/fpaint.gif")	$file->image = \'<img src="\'.$file->url.\'" alt=""/>\';
				if($admin)	$file->pic = $admintheme->DisplayImage($file->fileicon);
			}else{
				$file = false;
			}
			$item->'.$field[0].' = $file;';
		}
		if($field[1] < 5 && $field[1] != 0){
			
			// wx hide:
			// $parsedfields .= '
			// $item->'.$field[0].' = stripslashes($item->'.$field[0].');';

		}elseif( ($field[1] == 6 || $field[1] == 7) && ($field[5]['listmode'] == 2 || $field[5]['listmode'] == 4) ) {
			$parsedfields .= '
			$tmpoptions = unserialize($item->'.$field[0].');
			if(!is_array($tmpoptions)) $tmpoptions = array();
			$item->'.$field[0].' = array();
			$item->'.$field[0].'_namevalue = array();
			if(count($tmpoptions)>0){
				foreach($tmpoptions as $onevalue){
					if(isset($options_'.$field[0].'[$onevalue])){
						$item->'.$field[0].'[] = $onevalue;
						$item->'.$field[0].'_namevalue[] = $options_'.$field[0].'[$onevalue];
					}
				}
			}';
		}elseif( ($field[1] == 7 || $field[1] == 6) && ($field[5]['listmode'] == 1 || $field[5]['listmode'] == 3) ) {
			$parsedfields .= '
			$item->'.$field[0].' = (isset($options_'.$field[0].'[$item->'.$field[0].']))?$item->'.$field[0].':false;
			$item->'.$field[0].'_namevalue = (isset($options_'.$field[0].'[$item->'.$field[0].']))?$options_'.$field[0].'[$item->'.$field[0].']:"";';
		}elseif( $field[1] == 8 && $field[5]['thumb'] != '' ){
			$parsedfields .= '
			if($item->'.$field[0].')	$item->'.$field[0].'->thumbnail = preg_replace("/(.+)?\\/([^\\/]+)\\.(.+)/","$1/plthumb_$2.$3",$item->'.$field[0].'->filepath);';
		}elseif( $field[1] == 11 || $field[1] == 13){
			$parsedfields .= '
			$item->'.$field[0].' = $this->getaddfiles("'.$level[0].'_'.$field[0].'",$item->id,false'.(($field[1] == 13 && $field[5]['thumb'] != '')?',true':'').');
			$item->'.$field[0].'_filecount = count($item->'.$field[0].');';
			$addfiles[] = $level[0].'_'.$field[0];
		}elseif( $field[1] == 12 ){
			$parsedfields .= '
			if($admin && $item->'.$field[0].'){
				$item->'.$field[0].'_admindisplay = $admintheme->DisplayImage("icons/system/true.gif","1","","","systemicon");
			}else{
				$item->'.$field[0].'_admindisplay = "";
			}';			
		}
	}
	$addfiles = (count($addfiles) > 0)?implode(',',$addfiles):'';


	$dbget .= '
		$dbresult = $db->Execute($query,$wherevalues);
		$itemlist = array();
		$idlist = "";
		while ($dbresult && $row = $dbresult->FetchRow()){
			$item = new stdClass();
			$item->__what = "'.$level[0].'";
			foreach($row as $key=>$value){
				$item->$key = $value;
			}';

	// wx add for text input fields
	$dbget .= "\n" . '
			// wx get the text input fields
			$input_list = $this->get_level_'.$level[0].'_fields("text_input_keys");

			// wx strip slashes from the input text fields
			foreach ($input_list as $input_name) {
				$item->$input_name = stripslashes($item->$input_name);
			}

			// wx parent name strip slashes
			if(isset($item->parent_name)) {
				$item->parent_name = stripslashes($item->parent_name);
			}
			' . "\n";

	$dbget .= $parsedfields;
	$dbget .= $childrencount;

	$dbget .= '
			if($admin == true){
				// $parms will be the base for parameters of the admin links
				$parms = array(
					"prefix"=>"'.$level[1].'",
					"tablename"=>"'.$modulename.'_'.$level[0].'",
					"levelname"=>"'.$level[0].'",
					"child"=>'.(isset($levels[$i+1])?'"'.$modulename.'_'.$levels[$i+1][0].'"':'false').',
					"parentdefault"=>'.($level[7] == 1?'true':'false').',
					"orderbyparent"=>'.($level[6] == 1?'true':'false').',
					"addfiles"=>"'.$addfiles.'",
					"sharechildren"=>'.($level[2] == 1?'true':'false').',
					"sharedbyparents"=>'.($sharedbyparents?'true':'false').',
					"files"=>"'.$filefields.'"
					);
				$item = $this->addadminlinks($item,$parms,$id,$returnid);
			}
			$idlist .= ($idlist==""?"":" OR ")." parent=\'".$item->id."\'";
			array_push($itemlist,$item);
		}
';
	if(isset($levels[$i + 1]) && $level[2] == 0)	$dbget .= '
		if($admin || $load_nbchildren){
			$nbchildrens = array();
			$query = "SELECT parent, count(id) nbchildren FROM ".cms_db_prefix()."module_'.$modulename.'_'.$levels[$i + 1][0].' WHERE (".$idlist.") ".($admin?"":"AND active=1 ")."GROUP BY parent";
			$dbresult = $db->Execute($query);
			while($dbresult && $row = $dbresult->FetchRow())	$nbchildrens[$row["parent"]] = $row["nbchildren"];
			$newlist = array();
			foreach($itemlist as $item){
				$item->nbchildren = isset($nbchildrens[$item->id])?$nbchildrens[$item->id]:false;
				array_push($newlist, $item);
			}
			$itemlist = $newlist;
		}';
$dbget .= '
		return $itemlist;
	}
';

// wx : create new function that set's up all fields

/*

 Some info:

 $level[0] - level name

 // fields

 $field[0] - name of the field

 $field[1] is type of field
 0 - int
 1 - varchar(10)
 2 - varchar(32)
 3 - varchar(64)
 4 - varchar(255)
 5 - text

*/

$fields_to_false = array("id", "active", "isdefault", "date_modified", "date_created");

$dbget .= "\n";
$dbget .= "\t" . '// wx function' . "\n";
$dbget .= "\t" . 'function get_level_'.$level[0].'_fields($output=false) {' . "\n";

$dbget .= '
		/*

		Addition settings:
		
		// set to true or false or check permisions
		, "readonly" => $this->CheckPermission("module_".$this->GetName()."_advanced") ? false : true

		// text input
		"???" => array(
			"type" => "input",
			"size" => 50,
			"length" => 255,
			"validate" => array(
				"not_empty" => true,
				"value" => "name"
			)
		),

		// date
		"???" => array(
			"type" 		 => "date",
			"year_start" => 1990 // optional
		),

		// checkbox
		"???" => array(
			"type" 		=> "checkbox"
			"label"		=> $this->lang("items_"),
		),

		// select
		"???" => array(
			"type"		=> "select",
			"options"	=> array()
		),

		// image
		"img" => array(
			"type"			=> "image",
			"folder"		=> "properties",
			"class"			=> "",
			"button_select" => lang("file_select"),
			"button_remove" => lang("file_remove"),
			"tab"			=> 0,
			"col"			=> 1
		),

		// date
		"date" => array(
			"type"		=> "date",
			"year_end"	=> date("Y") + 1,
			"tab"		=> 1
		),

		// info
		"info" => array(
			"type"		=> "textarea",
			"height"	=> 200,
			"class"		=> "  text-editor"
		),

		*/' . "\n\n";

		// parent
		// only for lover levels
		if($i != 0) {
			$dbget .= '// get parent list
			$parent_list = $this->get_parent_list("'.$level[0].'");
			' . "\n\n";
		}

$dbget .= "\t\t" . '$fields = array(' . "\n\n";
$dbget .= "\t\t\t" . '// fields' . "\n";

foreach($level[4] as $field){

	// setup
	if(in_array($field[0], $fields_to_false)) {
		$dbget .= "\t\t\t" . '"'.$field[0].'" => false,' . "\n";
	} else {
		$dbget .= "\t\t\t" . '"'.$field[0].'" => ';
		if($field[1] == 5) {
			$dbget .= 'array("type" => "text"),' . "\n";
		} else {
			if($field[0] == "item_order") {
				$dbget .= 'array(
					"type" 		=> "item-order",
					"size" 		=> 10,
					"length" 	=> 10,
					"label"		=> lang("order"),
					"comment" 	=> lang("order_comment"),
					"validate" => array(
						"not_empty" => false,
						"value" => "number"
					)
				),' . "\n\n";
			} else if($field[0] == "parent") {
				$dbget .= '$parent_list ? array(
					// "class" 	=> "  form__row--hidden",
					"type"		=> "select",
					"options"	=> $parent_list,
					"label"		=> $this->Lang("items")
				) : false,' . "\n\n";
			} else if($field[0] == "alias" || substr($field[0], 0, 6) === "alias_") {
				$dbget .= 'array(
					"type" => "input",
					"size" => 50,
					"length" => 255,
					// label is optional, will look for label in module lang file
					"label" => $this->Lang("alias"),
					// comment is optional
					"comment" => $this->Lang("alias_comment"),
					// overwrite the preferances
					"autoincrement_alias" => false
				),' . "\n\n";
			}  else if($field[0] == "name") {
				$dbget .= 'array(
					"type" => "input",
					"size" => 50,
					"length" => 255,
					// label is optional, will look for label in module lang file
					"label" => $this->Lang("name"),
					// validate is optional
					// name will be validated by default
					"validate" => array(
						"not_empty" => true,
						"value" => "name" // check function validate_string() for options
					)
				),' . "\n\n";
			} else {
				$dbget .= 'array(
					"type" => "input",
					"size" => 50,
					"length" => 255
				),' . "\n\n";
			}
		}
	}
}

	$dbget .= "\t\t\t" . '"date_modified" => false,' . "\n";
	$dbget .= "\t\t\t" . '"date_created" => false' . "\n\n";
	$dbget .= "\t\t" . ');' . "\n";

	$dbget .= '
		return $this->get_level_fields("'.$level[0].'", $fields, $output);' . "\n\n";

$dbget .= "\t" . '}' . "\n\n";

// wx end

$i++;
}

$dbget .= '

	// wx create input for smarty
	// rertun an input object
	function createInputForSmarty($input) {
		require $this->config["modules_system"] . "/CTLModuleMaker/includes/create.input.for.smarty.php";
		return $result;
	}

';

?>
