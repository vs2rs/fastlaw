<?php
if(!isset($gCms)) exit;

echo '<h1>'.$this->Lang('title').'</h1><br/>';

if($this->GetPreference('usesessions',false)){
	if(!isset($_SESSION['CTLMM']))		$_SESSION['CTLMM'] = array("infos"=>array(), "levels"=>array());
	$infos = $_SESSION['CTLMM']['infos'];
	$levels = $_SESSION['CTLMM']['levels'];
}else{
	$levels = $params['levels'] = isset($params['levels'])?unserialize($params['levels']):array();
	$infos = $params['infos'] = isset($params['infos'])?unserialize($params['infos']):array();
}

echo $this->StartTabHeaders();
	echo $this->SetTabHeader("creation", $this->Lang("Modulecreation"), true);
	echo $this->SetTabHeader("help", $this->Lang("helptab"), false);		
echo $this->EndTabHeaders();


echo $this->StartTabContent();
	echo $this->StartTab("creation");

	// MODULE CREATION...
	$currentlevel = isset($params['currentlevel'])?$params['currentlevel']:1;
	if(isset($params['submitprevious']) && $params['step'] > 1){    // GOING BACK TO PREVIOUS STEP/SUBSTEP
		if($params['step'] == 3){
			$params['step'] = 2;
		}elseif($currentlevel == 1 && $params['substep'] == 1){
			$params['step'] = 1;
		}elseif($params['substep'] > 1) {
			$params['substep'] = $params['substep'] - 1;
		}else{
			$currentlevel = $currentlevel - 1;
			$params['substep'] = 3;
		}
	}elseif(isset($params['submitnext'])) {
#################################################################################
######## BEGIN FORM SUBMISSION (PROCESSING INPUTS)                              #
#################################################################################
		$errors = array();
		if($params['step'] == 1){
			// NAME OF MODULE
			if(!isset($params['nameofmodule']) || $params['nameofmodule'] == ''){
				$errors[] = $this->Lang('error_nameofmodule');
			}else{
				if(!$this->starts_with_letter($params['nameofmodule']))	$errors[] = $this->Lang('error_namebeginwithletter');
				$newname = str_replace('-','',munge_string_to_url($params['nameofmodule'], false));
				if($params['nameofmodule'] != $newname || $this->isreservedword($params['nameofmodule']) || strtolower($params['nameofmodule']) == 'products'){
					$errors[] = $this->Lang('error_nameinvalid');
				}
				$infos['name'] = strtolower($newname);
			}
			// FRIENDLY NAME
			if(!isset($params['friendlyname']) || $params['friendlyname'] == ''){
				$errors[] = $this->Lang('error_friendlyname');
			}else{
				$infos['friendlyname'] = $params['friendlyname'];
			}
			if(isset($params['moduledescription'])) $infos['description'] = $params['moduledescription'];
			
			// HOW MANY LEVELS
			$minlevels = ($this->GetPreference('allowsinglelevel', false))?1:2;
			if(!isset($params['howmanylevels']) || !($params['howmanylevels'] >= $minlevels) ){
				$errors[] = $this->Lang('error_howmanylevels');
			}else{
				$infos['nblevels'] = $params['howmanylevels'];
				$levels = array();
				$i = 0;
				while($i < $infos['nblevels']){
					$levels[] = array();
					$i++;
				}
			}
		}else{
			// WE ARE DEFINING LEVELS
			if($params['substep'] == 3) {
				// WE ARE CHOOSING ADMIN
				$levels[$currentlevel - 1][5] = $params['adminfields'];
				$errors = array_merge($errors,$this->checkAdminFields($params['adminfields'],$levels[$currentlevel - 1][4]));
			}elseif($params['substep'] == 2){
				// WE ARE DEFINING FIELDS
				$tmpfield = 1;
				$tmperrors = 0;
				$levels[$currentlevel - 1][4] = array();
				while($tmpfield <= $levels[$currentlevel - 1][3]) {
					$thislevel = isset($levels[$currentlevel - 1][4][$tmpfield - 1])?$levels[$currentlevel - 1][4][$tmpfield - 1]:array();
					$thislevel[0] = strtolower($params['field'.$tmpfield.'_name']);
					$thislevel[7] = ($params['field'.$tmpfield.'_friendlyname'] != '')?$params['field'.$tmpfield.'_friendlyname']:$thislevel[0];
					$thislevel[1] = $params['field'.$tmpfield.'_type'];
					$thislevel[2] = $params['field'.$tmpfield.'_needed'];
					$thislevel[3] = $params['field'.$tmpfield.'_indexed'];
					$thislevel[4] = $params['field'.$tmpfield.'_default'];
					$thislevel[5] = array(	"listoptions"=>$params['field'.$tmpfield.'_listoptions'],
											"listmode"=>$params['field'.$tmpfield.'_listmode'],
											"upfolder"=>$params['field'.$tmpfield.'_upfolder'],
											"thumb"=>$params['field'.$tmpfield.'_thumb'],
											"size"=>$params['field'.$tmpfield.'_size'],
											"innersearch"=>$params['field'.$tmpfield.'_indexed2'],
											"crop"=>$params['field'.$tmpfield.'_crop'],
											"fileext"=>$params['field'.$tmpfield.'_fileext'],
											"cropthumb"=>$params['field'.$tmpfield.'_cropthumb']  );
					$thislevel[6] = $this->ValidateField($thislevel);
					$levels[$currentlevel - 1][4][$tmpfield - 1] = $thislevel;
					if(count($thislevel[6])>0) $tmperrors++;
					$tmpfield++;
				}
				if($tmperrors != 0) $errors[] = $this->Lang('error_general');
			}elseif($params['substep'] == 1){
				// WE ARE DEFINING THE LEVEL INFORMATIONS
				if(!isset($params['nameoflevel']) || $params['nameoflevel'] == ''){
					$errors[] = $this->Lang('error_missingvalue');
				}else{
					$j = 0;
					$newname = str_replace('-','',munge_string_to_url($params['nameoflevel'], false));
					if(!$this->starts_with_letter($params['nameoflevel']))	$errors[] = $this->Lang('error_namebeginwithletter');
					if($params['nameoflevel'] != $newname)	$errors[] = $this->Lang('error_nameoflevel');
						while($j < ($currentlevel -1)) {
							if($newname == $levels[$j][0]) $errors[] = $this->Lang('error_namealreadyused');
							$j++;
						}
					if($this->isreservedword($newname)) $errors[] = $this->Lang('error_reserved');
					$levels[$currentlevel - 1][0] = strtolower($newname);
				}
				if(!isset($params['paramprefix']) || $params['paramprefix'] == ''){
					$errors[] = $this->Lang('error_missingvalue');
				}else{
					$newprefix = str_replace('-','',munge_string_to_url($params['paramprefix'], false));
					if($params['paramprefix'] != $newprefix)	$errors[] = $this->Lang('error_paramprefix');
					$levels[$currentlevel - 1][1] = $newprefix;
				}
				$levels[$currentlevel - 1][2] = isset($params['sharechildren'])?$params['sharechildren']:0;
				if($levels[$currentlevel - 1][2] == 1) $infos['shared'] = true;
				if(!isset($params['howmanyfields']) || !($params['howmanyfields'] > 0) || !($params['howmanyfields'] < 31)){
					$errors[] = $this->Lang('error_howmanyfields');
				}else{
					$levels[$currentlevel - 1][3] = $params['howmanyfields'];
				}
				$levels[$currentlevel - 1][6] = isset($params['itemorder'])?$params['itemorder']:0;
				$levels[$currentlevel - 1][7] = isset($params['nbdefaults'])?$params['nbdefaults']:0;
				$levels[$currentlevel - 1][8] = isset($params['newontop'])?$params['newontop']:0;
				$singular = ($params['name_singular'] != '')?$params['name_singular']:$params['nameoflevel'];
				$plural = ($params['name_plural'] != '')?$params['name_plural']:$params['name_singular'];
				$levels[$currentlevel - 1][9] = array($singular, $plural);
			}
		}

#################################################################################
######## END FORM SUBMISSION                                                    #
#################################################################################
		
		if(count($errors) == 0){
		// GOING TO NEXT STEP
			if($params['step'] == 1){
				$params['step']++;
			}elseif($params['substep'] == 3){
				if($currentlevel == count($levels)) {
					$params['step']++;
				}else{
					$currentlevel++;
					$params['substep'] = 1;
				}
			}else{
				$params['substep']++;
			}
		}else{
			$this->display_errors($errors, $head='errorwithrequest');
		}
	}
	if($params['step'] < 3) {
		echo $this->CreateFormStart($id, 'creation', $returnid, 'post');
		$YesNoOptions = array($this->Lang('No')=>0,$this->Lang('Yes')=>1);
		// HEADER
		echo '<h2>'.$this->Lang('Step').' '.$params['step'].' : '.$this->Lang('Step'.$params['step'].'title');
		if ($params['step'] == 2){
			// IF WE ARE WORKING WITH STRUCTURE
			echo '('.$this->Lang('Level').' '.$currentlevel.'/'.count($levels).')</h2><br/>';
			echo '<h3>'.$this->Lang('Currentstructure').': ';
			$tmpcounter = 0;
			// onelevel = array(name,prefix,sharechildren,#fields,fields)
			foreach($levels as $onelevel){
				$tmpcounter++;
				if($tmpcounter > 1) echo ' -&gt; ';
				$levelname = (isset($onelevel[0])?$onelevel[0].'('.$tmpcounter.')':$this->Lang('Level').' '.$tmpcounter);
				echo ($tmpcounter == $currentlevel?'<span style="color: green;">':'').$levelname.($tmpcounter == $currentlevel?'</span>':'');
			}
			echo '</h3>';
		// END OF HEADER
			// STILL WORKING WITH STRUCTURE...
			$level = $levels[$currentlevel-1];
			if(isset($params['substep']) && $params['substep'] == 3) {
				// WE ARE CHOOSING ADMIN
				echo '<p>'.$this->Lang('prompt_adminpanel').'</p>';
				echo '<p>'.$this->CreateInputText($id, 'adminfields', isset($level[5])?$level[5]:'name,active,'.(($currentlevel == $infos['nblevels'])?'isdefault,':'').'movelinks', 70).'</p>';
				echo '<p>'.$this->Lang('possiblefields').'<ul>';
				/* $defaultfields = $this->GetDefaultFields();
				foreach($defaultfields as $onefield){
					echo '<li>'.$onefield.'</li>';
				}*/
				echo $this->Lang('adminfields');
				foreach($level[4] as $onefield){
					echo '<li>'.$onefield[0].'</li>';
				}
				echo '</ul></p>';
			}elseif(isset($params['substep']) && $params['substep'] == 2) {
				// WE ARE CREATING FIELDS
				$tmpfield = 1;
				while($tmpfield <= $level[3]){
					$this->CreateFieldInput($id,$tmpfield,isset($level[4])?$level[4][$tmpfield-1]:array());
					$tmpfield++;
				}
				if($level[3] == 0) echo '<p>'.$this->Lang('nofield').'</p>';
				echo '<script type="text/javascript">
						function displayoptions(selector){
							var newvalue = "type"+selector.value;
							if(newvalue == "type1")	newvalue = "type1 ";
							var theoptions = selector.parentNode.parentNode.parentNode.getElementsByTagName("div");
							for(i=0; i<theoptions.length; i++){
								if(theoptions[i].className.indexOf("optgroup") >= 0){
										if(theoptions[i].className.indexOf(newvalue) >= 0){
											theoptions[i].style.display = "block";
										}else{
											theoptions[i].style.display = "none";
										}
								}
							}
						}
						var theinputs = document.getElementsByTagName("select");
						for(j=0; j<theinputs.length; j++){
							if(theinputs[j].className == "ctlmm_type_selector")	displayoptions(theinputs[j]);
						}
						var theinputs = null;
					</script>';
			}else{
				// BASIC LEVEL INFORMATION
				echo $this->DoInputLine('nameoflevel',$this->CreateInputText($id, 'nameoflevel', isset($level[0])?$level[0]:'', 30));
				echo $this->DoInputLine('name_singular',$this->CreateInputText($id, 'name_singular', isset($level[9])?$level[9][0]:'', 30));
				echo $this->DoInputLine('name_plural',$this->CreateInputText($id, 'name_plural', isset($level[9])?$level[9][1]:'', 30));
				echo $this->DoInputLine('paramprefix',$this->CreateInputText($id, 'paramprefix', isset($level[1])?$level[1]:'', 30,6));
				if($currentlevel != $infos['nblevels']) echo $this->DoInputLine('sharechildren',$this->CreateInputDropdown($id, 'sharechildren', $YesNoOptions, '', isset($level[2])?$level[2]:0,false?' disabled=true':''));
				echo $this->DoInputLine('howmanyfields',$this->CreateInputText($id, 'howmanyfields', isset($level[3])?$level[3]:'', 30, 2));
				echo $this->DoInputLine('itemorder',$this->CreateInputDropdown($id, 'itemorder', $YesNoOptions, '', isset($level[6])?$level[6]:1));
				echo $this->DoInputLine('newontop',$this->CreateInputDropdown($id, 'newontop', $YesNoOptions, '', isset($level[8])?$level[8]:0));
				echo $this->DoInputLine('nbdefaults',$this->CreateInputDropdown($id, 'nbdefaults', array($this->Lang('onlyonedefault')=>0,$this->Lang('defaultbyparent')=>1), '', isset($level[7])?$level[7]:0));
			}
		}else{
			// WE ARE IN THE FIRST STEP... COLLECTING BASIC MODULE INFORMATION:
			echo '</h2>';
			echo $this->DoInputLine('nameofmodule',$this->CreateInputText($id, 'nameofmodule', isset($infos['name'])?$infos['name']:'', 30));
			echo $this->DoInputLine('moduledescription',$this->CreateInputText($id, 'moduledescription', isset($infos['description'])?$infos['description']:'', 60));
			echo $this->DoInputLine('friendlyname',$this->CreateInputText($id, 'friendlyname', isset($infos['friendlyname'])?$infos['friendlyname']:'', 30));
			echo $this->DoInputLine('howmanylevels',$this->CreateInputText($id, 'howmanylevels', isset($infos['nblevels'])?$infos['nblevels']:2, 30, 1));
			echo '<p>'.$this->Lang('help_think').'</p><br/>';
		}
		echo $this->DoHiddenInputs($id, $params, $levels, $infos, isset($currentlevel)?$currentlevel:false);
		echo '<br/><p>'.$this->CreateInputSubmit($id, 'submitnext', $this->Lang('Next')).'</p>';
		if($params['step'] > 1){
			echo '<p>'.$this->CreateInputSubmit($id, 'submitprevious', $this->Lang('Previous'), '', '', isset($params['submitprevious'])?'':$this->Lang('warning_previous')).'</p>';
			echo $this->CreateFormEnd();
			$this->EchoExportForm($params, $levels, $infos, isset($currentlevel)?$currentlevel:false);
		}else{
			echo $this->CreateFormEnd();
		}

	}else{
		//FINISHING STEP (STEP 3)
		$leveloptions = array($this->Lang('onlyonetemplate') => 0);
		$tmpflag = 0;
		foreach($levels as $onelevel){
			$tmpflag++;
			if($tmpflag < $infos['nblevels'])		$leveloptions[$onelevel[0]] = $tmpflag;
		}
		echo '<h2>'.$this->Lang('Step').' 3 : '.$this->Lang('Step3title').'</h2><br/>';
		echo $this->CreateFormStart($id, ($this->GetPreference('doupgrade', true)?'modop':'createModule'), $returnid, 'post');
		echo '<p>'.$this->Lang('prompt_templatelevel').' '.$this->CreateInputDropdown($id, 'templatelevel', $leveloptions, '', 0,(isset($infos['shared']) && $infos['shared'])?' disabled=true':'').'</p>';
		echo $this->DoHiddenInputs($id, $params, $levels, $infos, isset($currentlevel)?$currentlevel:false);
		echo '<br/><p>'.$this->CreateInputSubmit($id, 'submitnext', $this->Lang('Next')).'</p>';
		echo $this->CreateFormEnd();
		echo $this->CreateFormStart($id, 'creation', $returnid, 'post');
		echo $this->DoHiddenInputs($id, $params, $levels, $infos, isset($currentlevel)?$currentlevel:false);
		echo '<p>'.$this->CreateInputSubmit($id, 'submitprevious', $this->Lang('Previous')).'</p>';
		echo $this->CreateFormEnd();
		$this->EchoExportForm($params, $levels, $infos, $params['currentlevel']);
	}

	if($this->GetPreference('usesessions',false)){
		$_SESSION['CTLMM']['infos'] = $infos;
		$_SESSION['CTLMM']['levels'] = $levels;
	}
	$this->getInnerDebug($params, $levels);

echo $this->EndTab();

echo $this->StartTab("help");
		echo "<h2>".$this->Lang("helptab")."</h2><br/>";

		$shownhelp = "help_none";
		if(!isset($params["step"])){
		}elseif( $params["step"] == 1 ){
			$shownhelp = "help_structure";
		}elseif( $params["step"] == 2 ){
			if(!isset($params["substep"]) || $params["substep"] == 1){
				$shownhelp = "help_structure";
			}elseif($params["substep"] == 2) {
				$shownhelp = "help_fields";
			}elseif($params["substep"] == 3) {
				$shownhelp = "help_adminlinks";
			}
		}elseif( $params["step"] == 3 ){
			$shownhelp = "help_templatelevel";
		}
		echo $this->Lang($shownhelp);

echo $this->EndTab();

echo $this->EndTabContent();

?>
