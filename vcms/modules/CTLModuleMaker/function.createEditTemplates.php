<?php

$templatefile = '<!--

	Add/Edit Template

-->
<div class="module-wrap">

<div class="module-submit  submit-buttons">
	{$submit}{$apply}{$cancel}
</div>

<!-- Globals -->
<div class="module-container  module-container--no-tabs">
	<div class="module-col">';

// check if we have a parent
if($parentinput){
	$templatefile .= '
		<!-- parent -->
		<div class="module-row  module-row--hidden">
			<div class="module-label">{$parent_label} *</div>
			<div class="module-input  module-input--small">{$parent_input}</div>
		</div>';
}

$templatefile .= '
		<!-- item_order -->
		{if isset($item_order_label)}<div class="module-row">
			<div class="module-label">{$item_order_label}</div>
			<div class="module-input  module-input--small">{$item_order_input}</div>
			{if isset($item_order_comment)}<div class="module-comment">{$item_order_comment}</div>{/if}
		</div>{else}{$item_order_input}{/if}
		<!-- date created -->
		<div class="module-row  module-row--hidden">
			<div class="module-label">{$created_label}</div>
			<div class="module-input  module-input--date">
				{html_select_date prefix=$created_prefix time=$created_input start_year="-10" end_year="+10"} &nbsp; {html_select_time prefix=$created_prefix time=$created_input}
			</div>
		</div>
	</div>
</div>

<!-- Tabs -->
<div class="module-tabs">
	<span onclick="navigateTab(this);" data-tab="module-tab-1" class="module-tabs__item  tab-nav  tab-nav-active">{$edittitle}</span>
	<span onclick="navigateTab(this);" data-tab="module-tab-2" class="module-tabs__item  tab-nav">Help</span>
</div>

<div class="module-container  tab-content  tab-content-active" id="module-tab-1">
	<div class="module-col">
		
		{* --- Input list  --- *}
		{assign var="inputList" value=", "|explode:"name, alias"}

		{foreach from=$inputList item=current_param}
			{include file="`$module_path`/templates/row.tpl"}
		{/foreach}
		
		';

foreach($level[4] as $field){
	if(!in_array($field[0],array('name','alias','id','item_order','isdefault','active','parent')) && ($field[1] < 10 || $field[1] == 12)){
		$templatefile .= '
		<!-- '.$field[0].' -->
		{if isset($'.$field[0].'_label)}<div class="module-row">
			<div class="module-label">{$'.$field[0].'_label}'.($field[2] == 1?' *':'').'</div>
			<div class="module-input">{$'.$field[0].'_input}</div>
			{if isset($'.$field[0].'_comment)}<div class="module-comment">{$'.$field[0].'_comment}</div>{/if}
		</div>{else}{$'.$field[0].'_input}{/if}';
	}elseif($field[1] == 10){
		$templatefile .= '
		<div class="module-row">
			<div class="module-label">{$'.$field[0].'_label}'.($field[2] == 1?'*':'').' :</div>
			<div class="module-input">{html_select_date prefix=$'.$level[1].'_'.$field[0].'_prefix time=$'.$level[1].'_'.$field[0].' start_year="-10" end_year="+10"} {html_select_time prefix=$'.$level[1].'_'.$field[0].'_prefix time=$'.$level[1].'_'.$field[0].' }</div>
		</div>';
	}
}

$templatefile .= '
	</div>
	<div class="module-col">
	</div>
</div>
<div class="module-container  tab-content" id="module-tab-2">
	<div class="module-col">
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed purus magna, consequat lacinia metus a, maximus porta sem. Praesent neque metus, accumsan nec iaculis ut, commodo eget ante.</p>
	</div>
	<div class="module-col">
	</div>
</div>
';

$templatefile .= '
<div class="module-submit  submit-buttons">
	{$submit}{$apply}{$cancel}
</div>

</div>
';

?>
