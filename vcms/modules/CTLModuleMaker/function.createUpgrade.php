<?php
if(!isset($infos))	exit;

$modulename = $infos["nameofmodule"];
$modulfriendlyname = $infos['friendlyname'];

$tmplists = "";
$defprefs = "";
foreach($levels as $level){
	foreach($level[4] as $field){
		if($field[1] == 7)	$tmplists .= ($tmplists == ""?"":",").'array("'.$level[1].'", "'.$level[0].'", "'.$field[0].'")';
	}
	$defprefs .= '"tabdisplay_'.$level[0].'","searchmodule_index_'.$level[0].'",';
	if($level[8])	$defprefs .= '"newitemsfirst_'.$level[0].'",';
}
if(isset($infos["editablealias"]) && $infos["editablealias"])	$defprefs .= '"editablealias",';
if(isset($infos["deletefiles"]) && $infos["deletefiles"])	$defprefs .= '"deletefiles",';

$method = '<?php
if (!isset($gCms)) exit;

$db =& $this->GetDb();
$dict = NewDataDictionary($db);
$tabopt = array("mysql" => "TYPE=MyISAM");

// PART 1: This does the upgrade for the CTLModuleMaker part
// Unfortunately, this is only backward compatible to CTLModuleMaker 1.6.3. I couldn\'t go any further behind because at that time the maker version wasn\'t saved anywhere

$oldmaker = $this->GetPreference("makerversion", "veryold");

switch($oldmaker){
	// BEGIN SWITCH($oldmaker)

	case "veryold":
	case "1.6.4":
	case "1.7":
	case "1.7.1":
	case "1.7.2":
	case "1.7.3":
	case "1.7.4":
		';
if($tmplists != ""){
	$method .= '
		// option values for dynamic list fields are now all in the same table

		// Create the new fieldoptions table
		$newtable = cms_db_prefix()."module_'.$infos['nameofmodule'].'_fieldoptions";
		$flds = "
			id I,
			field C(128),
			name C(32),
			item_order I
			";
		$sqlarray = $dict->CreateTableSQL($newtable, $flds, $tabopt);
		$dict->ExecuteSQLArray($sqlarray);
		$db->CreateSequence($newtable."_seq");

		// transfer the existing options
		$listfields = array('.$tmplists.');
		$maxid = 0;
		foreach($listfields as $field){
			$oldtable = cms_db_prefix()."module_'.$modulename.'_".$field[0]."_".$field[2]."_options";
			$dbresult = $db->Execute("SELECT * FROM ".$oldtable);
			$item_order = 1;
			while($dbresult && $row = $dbresult->FetchRow()){
				if($row["id"] > $maxid)	$maxid = $row["id"];
				$newid = $db->GenID($newtable."_seq");
				$query = "INSERT INTO ".$newtable." SET id=?, field=?, name=?, item_order=?";
				$db->Execute($query, array($row["id"], $field[1]."_".$field[2], $row["name"], $item_order));
				$item_order++;
			}
			$sqlarray = $dict->DropTableSQL($oldtable);
			$dict->ExecuteSQLArray($sqlarray);
			$db->DropSequence($oldtable."_seq");
		}
		// we need to make sure that the next options won\'t have the same ids
		$dummyid = $db->GenID($newtable."_seq");
		while($dummyid <= $maxid){
			$dummyid = $db->GenID($newtable."_seq");
		}';
}
$method .= ';
	case "1.8.1":
	case "1.8.2":
	case "1.8.2.2":
	case "1.8.3":
	case "1.8.3.1":
		// Creates the queries table
		$flds = "
			id I,
			name C(64),
			what C(32),
			whereclause C(255),
			wherevalues C(255),
			queryorder C(32)
			";
		$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_'.$infos['nameofmodule'].'_saved_queries", $flds, $tabopt);
		$dict->ExecuteSQLArray($sqlarray);
		$db->CreateSequence(cms_db_prefix()."module_'.$infos['nameofmodule'].'_saved_queries_seq");
		$this->CreatePermission("module_'.$infos['nameofmodule'].'_advanced", "'.$infos['nameofmodule'].': Advanced");
	case "1.8.4":
	case "1.8.4.1":
	case "1.8.5":
	case "1.8.5.1":
		// activating default preferences
		$defprefs = array('.$defprefs.'"restrict_permissions","display_filter","display_instantsearch");
		foreach($defprefs as $onepref)	$this->SetPreference($onepref,true);
	case "1.8.6":
	case "1.8.6.1":
	case "1.8.7":
	case "1.8.7.1":
		if($db->dbtype == "mysql" || $db->dbtype == "mysqli"){
			// msyql
			$queries = array();
			';
foreach($levels as $level){
	$method .= '$queries[] = "ALTER TABLE ".cms_db_prefix()."module_'.$modulename.'_'.$level[0].' ADD COLUMN date_created DATETIME";
			$queries[] = "UPDATE ".cms_db_prefix()."module_'.$modulename.'_'.$level[0].' SET date_created=date_modified";
			';
}
$method .= 'foreach($queries as $query)		mysql_query($query);

		}else{
			// non-mysql
			';
foreach($levels as $level){
	$method .= '$dict->AddColumnSQL(cms_db_prefix()."module_'.$modulename.'_'.$level[0].'", "date_created ".CMS_ADODB_DT);
			';
}
$method .= '
		}
	case "1.8.7.2":
	case "1.8.8":
	case "1.8.8.1":
	case "1.8.8.2":
	case "1.8.8.3":
	case "1.8.8.4":
		$this->SetPreference("adminpages",0);
		$this->SetPreference("load_nbchildren",true);
		$this->SetPreference("load_nextprevious",false);
	case "1.8.9":
		$this->SetPreference("use_session",true);
		$this->SetPreference("defemptytemplate","**");
		
	// END SWITCH($oldmaker)
}

$this->SetPreference("makerversion", "'.$this->GetVersion().'");
';

if(isset($infos['doupgrade']) && $infos['doupgrade'] && isset($infos['upgradequeries']) && count($infos['upgradequeries']) > 0){
	$method .= '
	
// ########################################################################
// PART 2 : This does the upgrade for field changes
// This is written automatically... so it\'s a mess.
// YOU SHOULD CHECK THIS CODE BEFORE USING IT!

if(!isset($current_version)) $current_version = $oldversion;

if($db->dbtype == "mysql" || $db->dbtype == "mysqli"){
	// BEGIN OF UPGRADE CODE FOR MYSQL...
switch($current_version){
';

	foreach($infos['upgradequeries'] as $version=>$queries){
		$method .= '	case "'.$version.'":
			$queries = array();
			';
		foreach($queries as $pre){
			$tablename = 'module_'.$infos['nameofmodule'].'_'.$pre[0];
			$pre[3] = $this->toMysqlType($pre[3]);
			switch($pre[1]){
				case 'Add':
					if($pre[3])	$method .= '$queries[] = "ALTER TABLE ".cms_db_prefix()."'.$tablename.' ADD COLUMN '.$pre[2].' '.$pre[3].'";
			';
					break;
				case 'Alter':
					if($pre[3])	$method .= '$queries[] = "ALTER TABLE ".cms_db_prefix()."'.$tablename.' ALTER COLUMN '.$pre[2].' '.$pre[2].' '.$pre[3].'";
			';
					break;
				case 'Drop':
					$method .= '$queries[] = "ALTER TABLE ".cms_db_prefix()."'.$tablename.' DROP COLUMN '.$pre[2].'";
			';
					break;
				case 'List':
					$method .= '
		// Create the fieldoptions table
		$newtable = cms_db_prefix()."module_'.$infos['nameofmodule'].'_fieldoptions";
		$flds = "
			id I,
			field C(128),
			name C(32),
			item_order I
			";
		$sqlarray = $dict->CreateTableSQL($newtable, $flds, $tabopt);
		$dict->ExecuteSQLArray($sqlarray);
		$db->CreateSequence($newtable."_seq");
		';
					break;
			}
		}
		$method .= 'foreach($queries as $query)		mysql_query($query);
			';
	}
		
$method .= '
	// end switch
	}
}else{
	// BEGIN OF UPGRADE CODE FOR NON-MYSQL (using ADODB, which may likely not be fully supported)
$dict = NewDataDictionary($db);
switch($current_version){
	// BEGIN SWITCH($current_version)
';

	foreach($infos['upgradequeries'] as $version=>$queries){
		$method .= '	case "'.$version.'":
		';
		foreach($queries as $pre){
			$tablename = 'module_'.$infos['nameofmodule'].'_'.$pre[0];
			switch($pre[1]){
				case 'Add':
					$method .= '
			$dict->AddColumnSQL(cms_db_prefix()."'.$tablename.'", "'.$pre[2].' '.$pre[3].'");';
					break;
				case 'Alter':
				case 'Drop':
				
$ourlevel = false;
foreach($levels as $level) if($level[0] == $pre[0]) $ourlevel = $level;
$method .= '
		$flds = "';
$firstfield = true;
foreach($ourlevel[4] as $field){
	$fieldtype = $this->GetDBFieldType($field[1], $field[5]['listmode']);
	if($field[0] == "id") $fieldtype = "I NOTNULL AUTOINCREMENT KEY";
	if($field[0] == "active" || $field[0] == "isdefault") $fieldtype = "L";
	if($field[1] != 11){
		$method .= ($firstfield?"":",").'
		'.$field[0].' '.$fieldtype;
		$firstfield = false;
	}
}
$method .= ',
 	   date_modified ".CMS_ADODB_DT."
		";

		$sqlarray = $dict->'.$pre[1].'ColumnSQL(cms_db_prefix()."'.$tablename.'", "'.$pre[2].' '.$pre[3].'", $flds, $tabopt);';
					break;
				case 'List':
			$method .= '
		// Create the fieldoptions table
		$newtable = cms_db_prefix()."module_'.$infos['nameofmodule'].'_fieldoptions";
		$flds = "
			id I,
			field C(128),
			name C(32),
			item_order I
			";
		$sqlarray = $dict->CreateTableSQL($newtable, $flds, $tabopt);
		$dict->ExecuteSQLArray($sqlarray);
		';
					break;
			}
			$method .= '
		$dict->ExecuteSQLArray($sqlarray);';
		}
			$method .= '
		break;';
	}
	$method .= '	
	// END SWITCH($current_version)
	}
}
';
}

$method .= '?'.'>';
?>
