<?php

$actionswitch = '
		switch($action){
			case "link":
				$toaction = isset($params["toaction"])?$params["toaction"]:"default";
				echo $this->CreateLink($id,$toaction,$returnid,"",$params,"",true);
				break;';
if($infos["dofeadd"]){
	$actionswitch .= '
			case "frontend_edit":
				$what = isset($params["what"])?$params["what"]:"'.$levels[count($levels)-1][0].'";
				switch($what){
					';
	foreach($levels as $level){
		$actionswitch .= 'case "'.$level[0].'":
						require "action.FEadd'.$level[1].'.php";
						break;
					';
	}
	$actionswitch .= '
				}
				break;';
}
$actionswitch .= '
			case "changepreferences":
				if(!$this->CheckPermission("module_'.$infos['nameofmodule'].'_advanced")){
					$this->Redirect($id, "defaultadmin", $returnid, array("module_message"=>$this->Lang("error_denied")));
					break;
				}
				$prefs = array("restrict_permissions","use_hierarchy","orderbyname","display_filter","display_instantsearch","display_instantsort","editable_aliases","autoincrement_alias","allow_sql","force_list","delete_files","tabdisplay_templates","tabdisplay_fieldoptions","tabdisplay_queries","showthumbnails","fe_wysiwyg","fe_decodeentities","fe_allowfiles","fe_allownamechange","fe_allowaddnew","fe_usecaptcha","allow_complex_order","load_nextprevious","load_nbchildren","use_session","retrievetree");
				foreach($this->get_levelarray() as $level){
					$prefs[] = "newitemsfirst_".$level;
					$prefs[] = "searchmodule_index_".$level;
					$prefs[] = "tabdisplay_".$level;
				}
				foreach($prefs as $pref)	$this->SetPreference($pref, isset($params[$pref]));
				if(isset($params["fe_aftersubmit"]))	$this->SetPreference("fe_aftersubmit", $params["fe_aftersubmit"]);
				if(isset($params["fe_maxfilesize"]))	$this->SetPreference("fe_maxfilesize", $params["fe_maxfilesize"]);
				$maxshownpages = (int)$params["maxshownpages"];
				$adminpages = (int)$params["adminpages"];
				foreach($this->get_levelarray() as $level){
					$nbperpage = (int) (isset($params[$level."_pagination"])?$params[$level."_pagination"]:0);
					$this->SetPreference($level."_pagination", $nbperpage);
				}
				$this->SetPreference("maxshownpages", $maxshownpages);
				$this->SetPreference("adminpages", $adminpages);
				$this->Redirect($id, "defaultadmin", $returnid, array("active_tab"=>"preferences", "module_message"=>$this->Lang("message_modified")));
				break;
			case "changedeftemplates":
				foreach($params as $key=>$value){
				    if($key != "submit")	   $this->setPreference($key, $value);
				}
				$this->Redirect($id, "defaultadmin", $returnid, array("active_tab"=>"templates", "module_message"=>$this->Lang("message_modified")));
				break;
			case "deletetpl":
				$newparams = array("active_tab"=>"templates");
				$deftemplates = $this->getDefaultTemplates();
			    if(isset($params["tplname"]) && !in_array($params["tplname"], $deftemplates)){
				    if($this->DeleteTemplate($params["tplname"]))	   $newparams["module_message>"] = $this->Lang("message_modified");
				}
				$this->Redirect($id, "defaultadmin", $returnid, $newparams);
				break;
			case "deletequery":
				$newparams = array("active_tab"=>"queries");
				if(isset($params["queryid"])){
					$db =& $this->GetDb();
					if($db->Execute("DELETE FROM ".cms_db_prefix()."module_'.$infos['nameofmodule'].'_saved_queries WHERE id=? LIMIT 1", array($params["queryid"]))){
						$newparams["module_message"] = $this->Lang("message_deleted");
					}
				}
				$this->Redirect($id, "defaultadmin", $returnid, $newparams);
				break;
			case "testquery":
				if(isset($params["queryid"]) && $query = $this->get_queries(array("id"=>$params["queryid"]))){
					$query = $query[0];
					echo "<p><b>".$this->Lang("query").":</b> ".$query->name;
					if($query->order != "")	echo " ORDER BY ".$query->order;
					echo "</p>";
					$getfunction = "get_level_".$query->what;
					$itemlist = $this->$getfunction(array(), false, "", "", 0, 0, $query->whereclause, $query->wherevalues, ($query->queryorder == ""?false:$query->queryorder));
					echo "<p><b>".$this->Lang("results")." (".count($itemlist).") :</b></p>";
					echo "<ul>";
					foreach($itemlist as $item)	echo "<li>".$item->name."</li>";
					echo "</ul>";
				}
				echo "<p>".$this->CreateLink($id, "defaultadmin", $returnid, lang("back"), array("active_tab"=>"queries"))."</p>";
				break;
					
';

if($hasdynamiclists){
	$actionswitch .= '
			case "add_optionvalue":
				if(isset($params["field"]) && isset($params["optionname"]) && $params["optionname"] != ""){
					$tablename = cms_db_prefix()."module_'.$infos['nameofmodule'].'_fieldoptions";
					$db =& $this->GetDb();
					$itemid = $db->GenID($tablename."_seq");
					$itemorder = $this->countsomething("fieldoptions", "id", array("field"=>$params["field"])) + 1;
					$query = "INSERT INTO ".$tablename." SET id=?, field=?, name=?, item_order=?";
					$db->Execute($query,array($itemid,$params["field"],$params["optionname"],$itemorder));
				}
				$this->Redirect($id, "defaultadmin", $returnid, array("active_tab"=>"fieldoptions") );
				break;
			case "delete_optionvalue":
				if(isset($params["field"]) && isset($params["optionid"]) && $params["optionid"] > 0){
					$tablename = cms_db_prefix()."module_'.$infos['nameofmodule'].'_fieldoptions";
					$db =& $this->GetDb();
					$query = "DELETE FROM $tablename WHERE id=? AND field=? LIMIT 1";
					if($db->Execute($query,array($params["optionid"], $params["field"]))){
						$query = "UPDATE $tablename SET item_order=(item_order-1) WHERE field=? AND item_order > ?";
						$db->Execute($query, array($params["field"], $params["currentorder"]));
					}
				}
				$this->Redirect($id, "defaultadmin", $returnid, array("active_tab"=>"fieldoptions") );
				break;
			case "move_optionvalue":
				$db =& $this->GetDb();
				$tablename = cms_db_prefix()."module_'.$infos['nameofmodule'].'_fieldoptions";
				if(isset($params["field"]) && isset($params["optionid"]) && isset($params["move"]) && isset($params["currentorder"])){
					if($params["move"] == "up" && $params["currentorder"] != 1){
						$query = "UPDATE $tablename SET item_order=(item_order+1) WHERE field=? AND item_order = ? LIMIT 1;";
						$db->Execute($query, array($params["field"], $params["currentorder"] -1));
						$query = "UPDATE $tablename SET item_order=(item_order-1) WHERE field=? AND id = ? LIMIT 1;";
						$db->Execute($query, array($params["field"], $params["optionid"]));
					}elseif($params["move"] == "down"){
						$query = "UPDATE $tablename SET item_order=(item_order-1) WHERE field=? AND item_order = ? LIMIT 1;";
						if($db->Execute($query, array($params["field"], $params["currentorder"] +1))){
							$query = "UPDATE $tablename SET item_order=(item_order+1) WHERE field=? AND id = ? LIMIT 1;";
							$db->Execute($query, array($params["field"], $params["optionid"]));
						}
					}
				}
				$this->Redirect($id, "defaultadmin", $returnid, array("active_tab"=>"fieldoptions") );
				break;
			case "rename_optionvalue":
				if(!isset($params["cancel"]) && isset($params["optionid"]) && isset($params["field"]) && isset($params["optionname"]) ){
					if(isset($params["submit"]) && $params["optionname"] != "" ){
						$tablename = cms_db_prefix()."module_'.$infos['nameofmodule'].'_fieldoptions";
						$db =& $this->GetDb();
						$query = "UPDATE $tablename SET name=? WHERE field=? AND id=?";
						$db->Execute($query,array($params["optionname"], $params["field"], $params["optionid"]));
						$this->Redirect($id, "defaultadmin", $returnid, array("active_tab"=>"fieldoptions") );
					}else{
						echo "<h2>".$this->Lang("modifyanoption")."</h2>";
						echo $this->CreateFormStart($id, "rename_optionvalue", $returnid);
						echo "<p>".$this->CreateInputText($id,"optionname",$params["optionname"],40,64)."</p>";
						echo "<p>".$this->CreateInputSubmit($id, "submit", lang("submit")).$this->CreateInputSubmit($id, "cancel", lang("cancel"))."</p>";
						echo $this->CreateInputHidden($id, "optionid", $params["optionid"]).$this->CreateInputHidden($id, "field", $params["field"]);
						echo $this->CreateFormEnd();
					}
				}else{
					$this->Redirect($id, "defaultadmin", $returnid, array("active_tab"=>"fieldoptions") );
				}
				break;';
}
if($multiplefiles){
	$actionswitch .= '
			case "viewaddimgs":
				$db = $this->GetDb();
				if(!isset($params["itemid"]) || !isset($params["field"]) || !isset($params["prefix"]))	$this->Redirect($id, "defaultadmin", $returnid);
				if( isset($params["deletefile"]) && $params["deletefile"] > 0 ){
					$query = "DELETE FROM ".cms_db_prefix()."module_'.$infos['nameofmodule'].'_multiplefilesfields WHERE fileid=? LIMIT 1";
					if($db->Execute( $query, array($params["deletefile"]) ))	echo $this->ShowMessage($this->Lang("message_modified"));
				}
				echo "<p>".$this->CreateLink($id, "edit".$params["prefix"], $returnid, lang("cancel"), array($params["prefix"]."id"=>$params["itemid"]))."</p>";
				$addfiles = $this->getaddfiles($params["field"], $params["itemid"], true);
				echo "<table>";
				$admintheme = $gCms->variables["admintheme"];
				foreach($addfiles as $addfile){
					echo "<tr><td><b>".$addfile->filepath."</b><br/>".$this->Lang("imagesize").": ".$addfile->imagesize."<br/>".$this->Lang("filesize").": ".$addfile->size_wformat."<br/>".$this->Lang("lastmod").": ".$addfile->modified;
					$newparams = $params;
					$newparams["deletefile"] = $addfile->fileid;
					echo "<br/>".$this->CreateLink($id, $action, $returnid, $admintheme->DisplayImage("icons/system/delete.gif", $this->lang("delete"),"","","systemicon"), $newparams)."</td>";
					echo \'<td><img src="\'.$addfile->url.\'" alt="\'.$addfile->url.\'"/></td></tr>\';
				}
				echo "<table>";
				echo "<p>".$this->CreateLink($id, "edit".$params["prefix"], $returnid, lang("cancel"), array($params["prefix"]."id"=>$params["itemid"]))."</p>";
				break;';
}
$actionswitch .= '
			case "default":
			default:
				parent::DoAction($action, $id, $params, $returnid);
				break;
		}
';

?>
