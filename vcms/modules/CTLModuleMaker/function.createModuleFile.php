<?php

$moduleFile = '<?php
#-------------------------------------------------------------------------
# Module: '.$infos['nameofmodule'].'
# Version: '.$infos['version'].', 
#
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2008 by Ted Kulp (wishy@cmsmadesimple.org)
# This project"s homepage is: http://www.cmsmadesimple.org
#
# This module was created with CTLModuleMaker '.$makerversion.'
# CTLModuleMaker was created by Pierre-Luc Germain and is released under GNU
# http://dev.cmsmadesimple.org/projects/ctlmodulemaker
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------

class '.$infos['nameofmodule'].' extends CMSModule
{
	var $currenttree = false;
	var $currentpageindex = 1;
	var $plcurrent = array();

	function GetName()
	{
		return "'.$infos['nameofmodule'].'";
	}

	// wx ==> return current path
	function GetModulePath()
	{
		return dirname(__FILE__);
	}

	/*---------------------------------------------------------
	   GetFriendlyName()
	   This can return any string, preferably a localized name
	   of the module. This is the name that"s shown in the
	   Admin Menus and section pages (if the module has an admin
	   component).
	   
	   See the note on localization at the top of this file.
	  ---------------------------------------------------------*/
	function GetFriendlyName()
	{
		return $this->Lang("friendlyname");
	}

	/*---------------------------------------------------------
		>> wx
		>> get level name
	---------------------------------------------------------*/
	function getLevelNameFromPrefix($moduleInfo)
    {
		
		// get the prefix
		$moduleInfo = explode(",", $moduleInfo);
		$prefix = str_replace("edit", "", $moduleInfo[2]);

		// setup level array
		$return = array();';

		foreach($levels as $level){
		$moduleFile .= '
		$return["'.$level[1].'"] = "'.$level[0].'";';
		}

		$moduleFile .= '

		// return
		return $return[$prefix];

    }

	/*---------------------------------------------------------
		>> wx
		>> get table name for the level
	---------------------------------------------------------*/
	function getTableName($level)
    {

		// setup level array
		$return = array();';

		foreach($levels as $level){
		$moduleFile .= '
		$return["'.$level[0].'"] = $this->GetName() . "_'.$level[0].'";';
		}

		$moduleFile .= '

		// return
		return isset($return[$level]) ? $return[$level] : false;

    }

	/*---------------------------------------------------------
		>> wx
		>> get table name for the level
	---------------------------------------------------------*/
	function getLevelPrefix($level)
    {

		// setup level array

		$return = array();';

		foreach($levels as $level){
		$moduleFile .= '
		$return["'.$level[0].'"] = "'.$level[1].'";';
		}

		$moduleFile .= '

		// return
		return isset($return[$level]) ? $return[$level] : false;

    }

	/*---------------------------------------------------------
		>> wx
		>> should new items be added to bottom or top
	---------------------------------------------------------*/

	function newItemsFirst($level) {

		// new items by default are added on bottom
		$param = array(';
		foreach($levels as $level){
		$moduleFile .= '
			"'.$level[0].'"		=> false,';
		}

		$moduleFile .= '
		);

		// return
		return isset($param[$level]) ? $param[$level] : false;

	}

	/*---------------------------------------------------------
		>> wx
		>> show or hide tab from users
	---------------------------------------------------------*/
	function showTab($tab) {
		
		// get module name
		$module_name = $this->GetName();

		// check advance perms
		$perm = $this->CheckPermission("module_" . $module_name . "_advanced");

		// setup tab access
		$showTab = array(';

		foreach($levels as $level){
		$moduleFile .= '
			"'.$level[0].'" => true,';
		}

		$moduleFile .= '
		);

		// return
		if(isset($showTab[$tab])) {
			return $showTab[$tab];
		} else {
			return $perm;
		}

		return false;

	}

	/*---------------------------------------------------------
		>> wx
		>> show buttons on top of the edit form
	---------------------------------------------------------*/
	function showFormButtonsTop() {
		return true;
	}
	
	/*---------------------------------------------------------
	   GetVersion()
	   This can return any string, preferably a number or
	   something that makes sense for designating a version.
	   The CMS will use this to identify whether or not
	   the installed version of the module is current, and
	   the module will use it to figure out how to upgrade
	   itself if requested.	   
	  ---------------------------------------------------------*/
	function GetVersion()
	{
		return "'.$infos['version'].'";
	}


	/*---------------------------------------------------------
	   GetDependencies()
	   Your module may need another module to already be installed
	   before you can install it.
	   This method returns a list of those dependencies and
	   minimum version numbers that this module requires.
	   
	   It should return an hash, eg.
	   return array("somemodule"=>"1.0", "othermodule"=>"1.1");
	  ---------------------------------------------------------*/
	function GetDependencies()
	{
		return array();
	}

	/*---------------------------------------------------------
	   GetHelp()
	   This returns HTML information on the module.
	   Typically, you"ll want to include information on how to
	   use the module.
	   
	   See the note on localization at the top of this file.
	  ---------------------------------------------------------*/
	function GetHelp()
	{
		return $this->Lang("help");
	}

	/*---------------------------------------------------------
	   GetAuthor()
	   This returns a string that is presented in the Module
	   Admin if you click on the "About" link.
	  ---------------------------------------------------------*/
	function GetAuthor()
	{
		return "CTLModuleMaker '.$makerversion.'";
		// of course you may change this, but it would be nice
		// to keep a mention of the CTLModuleMaker somewhere
	}


	/*---------------------------------------------------------
	   GetAuthorEmail()
	   This returns a string that is presented in the Module
	   Admin if you click on the "About" link. It helps users
	   of your module get in touch with you to send bug reports,
	   questions, cases of beer, and/or large sums of money.
	  ---------------------------------------------------------*/
	function GetAuthorEmail()
	{
		return "";
	}


	/*---------------------------------------------------------
	   IsPluginModule()
	   This function returns true or false, depending upon
	   whether users can include the module in a page or
	   template using a smarty tag of the form
	   {cms_module module="Prod" param1=val param2=val...}
	   
	   If your module does not get included in pages or
	   templates, return "false" here.
	  ---------------------------------------------------------*/
	function IsPluginModule()
	{
		return true;
	}


	/*---------------------------------------------------------
	   HasAdmin()
	   This function returns a boolean value, depending on
	   whether your module adds anything to the Admin area of
	   the site. For the rest of these comments, I"ll be calling
	   the admin part of your module the "Admin Panel" for
	   want of a better term.
	  ---------------------------------------------------------*/
	function HasAdmin() {	return true;	}
	function GetAdminSection() {return "content";}
	function GetAdminDescription() {return $this->Lang("admindescription");}
	';

if(!isset($infos['is_shared']) || !$infos['is_shared']){
	$moduleFile .= '
	/*---------------------------------------------------------
	   Module Constructor 
	---------------------------------------------------------*/
	function __construct()
	{
		global $gCms;
		parent::CMSModule();
	}

	// wx: keep backwards compatibility
	function '.$infos['nameofmodule'].'()
	{
		self::__construct();
	}
	';
}

$moduleFile .= '
	/*---------------------------------------------------------
	   SetParameters()
	   This function enables you to create mappings for
	   your module when using "Pretty Urls".
	   
	   Typically, modules create internal links that have
	   big ugly strings along the lines of:
	   index.php?mact=ModName,cntnt01,actionName,0&cntnt01param1=1&cntnt01param2=2&cntnt01returnid=3
	   
	   You might prefer these to look like:
	   /ModuleFunction/2/3
	   
	   To do this, you have to register routes and map
	   your parameters in a way that the API will be able
	   to understand.

	   Also note that any calls to CreateLink will need to
	   be updated to pass the pretty url parameter.
	   
	   Since the Skeleton doesn"t really create any links,
	   the section below is commented out, but you can
	   use it to figure out pretty urls.
	   
	   ---------------------------------------------------------*/
	function SetParameters()
	{
';
$modname = '['.substr($infos['nameofmodule'],0,1).strtoupper(substr($infos['nameofmodule'],0,1)).']'.substr($infos['nameofmodule'],1);

$moduleFile .= '
		// these are for internal pretty URLS.
		// you may change these, but you will also need to change the BuildPrettyURLs function below accordingly
		// see FAQ for more info on this
		$defact = array("action"=>"default");
		
		$this->RestrictUnknownParams();
		
		$this->CreateParameter("action", "default", $this->Lang("phelp_action"));
		$this->CreateParameter("what", "", $this->Lang("phelp_what"));
		$this->SetParameterType("what",CLEAN_STRING);
		$this->CreateParameter("alias", "", $this->Lang("phelp_alias"));
		$this->SetParameterType("alias",CLEAN_STRING);
		$this->CreateParameter("showdefault", false, $this->Lang("phelp_showdefault"));
		$this->SetParameterType("showdefault",CLEAN_INT);
		$this->CreateParameter("parent", "", $this->Lang("phelp_parent"));
		$this->SetParameterType("parent",CLEAN_STRING);
		$this->CreateParameter("limit", 0, $this->Lang("phelp_limit"));
		$this->SetParameterType("limit",CLEAN_INT);
		$this->CreateParameter("nbperpage", 0, $this->Lang("phelp_nbperpage"));
		$this->SetParameterType("nbperpage",CLEAN_STRING);
		$this->CreateParameter("orderby", 0, $this->Lang("phelp_orderby"));
		$this->SetParameterType("orderby",CLEAN_STRING);
		$this->CreateParameter("detailpage", "", $this->Lang("phelp_detailpage"));
		$this->SetParameterType("detailpage",CLEAN_STRING);
		$this->CreateParameter("random", 0, $this->Lang("phelp_random"));
		$this->SetParameterType("random",CLEAN_INT);
		$this->CreateParameter("listtemplate", "", $this->lang("phelp_listtemplate"));
		$this->SetParameterType("listtemplate",CLEAN_STRING);
		$this->CreateParameter("finaltemplate", "", $this->lang("phelp_finaltemplate"));
		$this->SetParameterType("finaltemplate",CLEAN_STRING);
		$this->CreateParameter("forcelist", "0", $this->lang("phelp_forcelist"));
		$this->SetParameterType("forcelist",CLEAN_STRING);
		$this->CreateParameter("inline", 0, $this->lang("phelp_inline"));
		$this->SetParameterType("inline",CLEAN_STRING);
		$this->CreateParameter("searchmode", "advanced", $this->lang("phelp_searchmode"));
		$this->SetParameterType("searchmode",CLEAN_STRING);
		$this->CreateParameter("query", 0, $this->lang("phelp_query"));
		$this->SetParameterType("query",CLEAN_STRING);
		$this->CreateParameter("toaction", "", $this->Lang("phelp_toaction"));
		$this->SetParameterType("toaction",CLEAN_STRING);
		$this->SetParameterType("pageindex",CLEAN_INT);

		// for the search form (trick from ikulis) :
		$this->SetParameterType(CLEAN_REGEXP."/date_.*/",CLEAN_STRING);
		$this->SetParameterType(CLEAN_REGEXP."/field_.*/",CLEAN_STRING);
		$this->SetParameterType(CLEAN_REGEXP."/compare_.*/",CLEAN_STRING);
		$this->SetParameterType("submitsearch",CLEAN_STRING);
		$this->SetParameterType("searchfield",CLEAN_STRING);
		';
		
if($infos["dofeadd"])	$moduleFile .= '
		// for the frontend add action
		$this->SetParameterType(CLEAN_REGEXP."/feadd.*/",CLEAN_STRING);
		$this->SetParameterType(CLEAN_REGEXP."/fefile.*/",CLEAN_STRING);
		$this->SetParameterType("captcha_input", CLEAN_STRING);
		';
		
$moduleFile .= '
	}
    
	function InstallPostMessage()
	{
		return $this->Lang("postinstall");
	}
	function UninstallPostMessage()
	{
		return $this->Lang("postuninstall");
	}
	function UninstallPreMessage()
	{
		return $this->Lang("really_uninstall");
	}


	/*---------------------------------------------------------
	   Install()
	   When your module is installed, you may need to do some
	   setup. Typical things that happen here are the creation
	   and prepopulation of database tables, database sequences,
	   permissions, preferences, etc.
	   	   
	   For information on the creation of database tables,
	   check out the ADODB Data Dictionary page at
	   http://phplens.com/lens/adodb/docs-datadict.htm
	   
	   This function can return a string in case of any error,
	   and CMS will not consider the module installed.
	   Successful installs should return FALSE or nothing at all.
	  ---------------------------------------------------------*/
	function Install()
	{
		global $gCms;
		require "method.install.php";
	}

	/*---------------------------------------------------------
	   Uninstall()
	   Sometimes, an exceptionally unenlightened or ignorant
	   admin will wish to uninstall your module. While it
	   would be best to lay into these idiots with a cluestick,
	   we will do the magnanimous thing and remove the module
	   and clean up the database, permissions, and preferences
	   that are specific to it.
	   This is the method where we do this.
	  ---------------------------------------------------------*/
	function Uninstall()
	{
		global $gCms;
		require "method.uninstall.php";
	}


    function SearchResult($returnid, $itemid, $level = "")
    {
		$result = array();
		$wantedparam = false;
		$newparams = array();
		if($level == "'.$levels[count($levels)-1][0].'"){
			// we seek an element of the last level, and will display the detail view
			$wantedparam = "alias";
		}else{
			if($newparams["what"] = $this->get_nextlevel($level)){
			// we seek an element of another level, and will display the list view of its children
				$wantedparam = "parent";
			}
		}
		if ($wantedparam){
			$tablename = cms_db_prefix()."module_'.$infos['nameofmodule'].'_".$level;
			$db =& $this->GetDb();
			$query = "SELECT name, alias FROM $tablename WHERE id = ?";
			$dbresult = $db->Execute( $query, array( $itemid ) );
			if ($dbresult){
				$row = $dbresult->FetchRow();
				$newparams[$wantedparam] = $row["alias"];

				//0 position is the prefix displayed in the list results.
				$result[0] = $this->GetFriendlyName();

				//1 position is the title
				$result[1] = $row["name"];
		
				//2 position is the URL to the title.
				$result[2] = $this->CreateLink($id, "default", $returnid, "", $newparams, "", true, false, "", false, $this->BuildPrettyUrls($newparams, $returnid));
			}
		}

		return $result;
	}
	
	function SearchReindex(&$module)
    {
		$db =& $this->GetDb();
		';
foreach($levels as $level){
	$indexedfields = '';
	foreach($level[4] as $field){
		if($field[3] == 1) $indexedfields .= ($indexedfields == ''?'':' ').'$item->'.$field[0];
	}
	$moduleFile .= 'if($this->GetPreference("searchmodule_index_'.$level[0].'",false)){
			$itemlist = $this->get_level_'.$level[0].'();
			foreach($itemlist as $item){
				$text = "'.$indexedfields.'";
				$module->AddWords($this->GetName(), $item->id, "'.$level[0].'", $text, NULL);
			}
		}
		';
}
$moduleFile .= '
    }	

/* ---------------------------------------------
NOT PART OF THE NORMAL MODULE API
----------------------------------------------*/

    function getDefaultTemplates(){
    	// returns an array of the templates that are selected as default (just so that we don\'t delete them)
	   $result = array();
	   $result[] = $this->GetPreference("finaltemplate");
	   $result[] = $this->GetPreference("searchresultstemplate");
	   ';
foreach($levels as $level){
	 $moduleFile .= '$result[] = $this->GetPreference("listtemplate_'.$level[0].'");
	   ';
}
$moduleFile .= 'return $result;
    }
	
    function getOrderType($what){
		// returns whether a level is ordered by parent or not
		';
$tmp = '';
foreach($levels as $level){
	 $tmp .= ($tmp==''?'':',').'"'.$level[0].'"=>'.($level[6]?'true':'false');
}
$moduleFile .= '$return = array('.$tmp.');
		return (isset($return[$what])?$return[$what]:false);
    }

	function DoAction($action, $id, $params, $returnid=-1){
		global $gCms;
		';

$moduleFile .= $this->createActions($infos, $levels, $hasdynamiclists, $multiplefiles);
$moduleFile .= '

	}
';

$moduleFile .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'plcreatealias.php');
$moduleFile .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'checkalias.php');
$moduleFile .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'BuildPrettyUrls.php');
$moduleFile .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'DoCheckboxes.php');
$moduleFile .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'parsekeywords.php');

if($infos["dofeadd"])	$moduleFile .= '
	function feadd_permcheck($what, $itemid, $alias){
		global $gCms;
		$return = true;
		require "function.feadd_permcheck.php";
		return $return;
	}
';

$moduleFile .= $this->createDBGetFunctions($infos, $levels, $predefinedlists, $multiplefiles);

if($infos['hasfilefields'])	$moduleFile .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'filefunctions.php');

$moduleFile .= $this->getFileContent('functions'.DIRECTORY_SEPARATOR.'getFileContent.php');

$moduleFile .= '
}

?>
';

?>
