<?php
if(!isset($old) || !isset($new))	exit;

$oldfields = array();
$newfields = array();
$created_fields = array();
$deleted_fields = array();
$modified_fields = array();
$oldlistfields = false;
$newlistfields = false;

foreach($new[4] as $field)	$newfields[$field[0]] = $field;
foreach($old[4] as $field){
	if($field[1] == 7)	$oldlistfields = true;
	$oldfields[$field[0]] = $field;
	if(!isset($newfields[$field[0]]))	$deleted_fields[] = $field;
}
foreach($new[4] as $newfield){
	if($field[1] == 7)	$newlistfields = true;
	if(isset($oldfields[$newfield[0]])){
		// field already existed
		$oldfield = $oldfields[$newfield[0]];
		$oldtype = $this->GetDBFieldType($oldfield[1], $oldfield[5]['listmode']);
		$newtype = $this->GetDBFieldType($newfield[1], $newfield[5]['listmode']);
		if($oldtype != $newtype)	$modified_fields[] = $newfield;
	}else{
		$created_fields[] = $newfield;
	}
}

$queries = array();
if($newlistfields && !$oldlistfields)	$queries[] = array('List', 'List','');
foreach($modified_fields as $field){
	$newtype = $this->GetDBFieldType($field[1], $field[5]['listmode']);
	$queries[] = array($new[0], 'Alter', $field[0],$newtype);
}
foreach($deleted_fields as $field){
	$newtype = $this->GetDBFieldType($field[1], $field[5]['listmode']);
	$queries[] = array($new[0], 'Drop', $field[0],$newtype);
}
foreach($created_fields as $field){
	$newtype = $this->GetDBFieldType($field[1], $field[5]['listmode']);
	$queries[] = array($new[0], 'Add', $field[0],$newtype);
}

$changes = array(count($created_fields), count($modified_fields), count($deleted_fields));

?>
