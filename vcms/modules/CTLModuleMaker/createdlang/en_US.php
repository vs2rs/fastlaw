<?php
// this file is used by the CTLModuleMaker to create the en_US language file.
// THIS FILE IS NECESSARY
// to create other language file, simple copy, rename and edit this file. (you may also share it!)

$mylang['Add'] = 'Add';
$mylang['Edit'] = 'Edit';

$mylang['general'] = '
// strings for general fields
$lang["id"] = "id";
$lang["name"] = "Name";
$lang["alias"] = "URL";
$lang["alias_comment"] = "URL is a unique identifier for this object.";
$lang["isdefault"] = "Is default?";
$lang["active"] = "Active";
$lang["parent"] = "Parent";
$lang["submit"] = "Submit";
$lang["cancel"] = "Cancel";
$lang["nbchildren"] = "Nb of items";
$lang["date_modified"] = "Last Modified";
$lang["date_created"] = "Date Created";
';

?>