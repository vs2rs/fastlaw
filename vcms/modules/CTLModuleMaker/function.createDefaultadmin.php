<?php

$modulename = $infos['nameofmodule'];
$modulfriendlyname = $infos['friendlyname'];

$defaultadmin = '<?php
if (!isset($gCms)) exit;
$themeObject = $gCms->variables["admintheme"];
$active_tab = isset($params["active_tab"])?$params["active_tab"]:"'.$levels[0][0].'";

$has_advanced_perm = $this->CheckPermission("module_'.$modulename.'_advanced");

// check if module message is set in url
if($module_message = $this->get_module_message($params)) {
	$message = $themeObject->ShowMessage($module_message);
}

/**

 * wx/vcms
 * we check for post

*/

// get the levels
$all_levels = $this->get_levelarray();

// check for post
if(isset($_POST)) {

	// set active tab
	if(isset($_POST["active_tab"])
	&& in_array($_POST["active_tab"], $all_levels)) {
		$active_tab = $_POST["active_tab"];
	}

	// check post
	foreach ($all_levels as $post_level) {
		if(isset($_POST["add-new-".$post_level."-name"])) {
			// wx includ all the post stuff
			include_once($gCms->config["modules_system"] . "/CTLModuleMaker/includes/create.gallery.php");
			// end foreach
			break;
		}
	}

}

// end post

';

// count tabs

$defaultadmin .= '// count tabs
$tab_count = 0;';
foreach($levels as $level){
	$defaultadmin .= 'if($this->showTab("'.$level[0].'")) $tab_count++;';
}

// start tab header

$defaultadmin .= '
if($tab_count > 1) {
	echo $this->StartTabHeaders();';

$tabdisplay = '';
$shared = false;
foreach($levels as $level){
	if($level[2])	$shared = true;
	$tabdisplay .= '"'.$level[0].'",';
	$defaultadmin .= "\t" . 'if($this->showTab("'.$level[0].'")) {
		echo $this->SetTabHeader("'.strtolower($level[0]).'", $this->Lang("'.$level[0].'_plural"), "'.$level[0].'" == $active_tab ? true : false);
	}';
}
if(count($listfields) > 0){
	$tabdisplay .= '"fieldoptions",';
	$defaultadmin .= '
	if($this->showTab("fieldoptions")) {
		echo $this->SetTabHeader("fieldoptions", $this->Lang("fieldoptions"), "fieldoptions" == $active_tab ? true : false);		
	}';
}
$defaultadmin .= '
	echo $this->EndTabHeaders();
}

echo $this->StartTabContent();
';

#####################################
## CONTENT TABS

// $i = 0;
// while($i < count($levels)) {
foreach ($levels as $i => $level) {

// $level = $levels[$i];
$defaultadmin .= '
if($this->showTab("'.$level[0].'")) {

	echo $this->StartTab("'.strtolower($level[0]).'"); ' . "\n";

	// for levels with parents need to check for parents
	if($i != 0) $defaultadmin .= '
	if($this->countsomething("'.$levels[$i - 1][0].'") > 0){' . "\n";

		$defaultadmin .= '
		// get items
		$itemlist = $this->get_level_'.$level[0].'(
			isset($where)?$where:array(), true, $id, $returnid, false
		);

		// setup cells
		$cells = array(
			array(
				"key"    => "item_order",
				"action" => "item_order",
				"label"  => lang("order"),
				"class"  => "  list__cell--id"
			),
			array(
				"key"    => "editlink",
				"action" => "edit",
				"label"  => $this->lang("'.$level[0].'"),
				"class"  => "  list__cell--fill"
			),
			array(
				"key"    => "editbutton",
				"action" => "edit",
				"label"  => lang("edit"),
				"class"  => "  list__cell--icon"
			),
			array(
				"key"    => "toggleactive",
				"action" => "active",
				"label"  => lang("active"),
				"class"  => "  list__cell--icon"
			),
			array(
				"key"    => "deletelink",
				"action" => "delete",
				"label"  => lang("delete"),
				"class"  => "  list__cell--icon"
			),
		);

		// table
		echo $themeObject->CreateTable(array(
			// content
			"items"			 => $itemlist,
			"cells"			 => $cells,
			"message"		 => isset($message) && $active_tab == "'.$level[0].'" ? $message : "",
			
			// buttons
			"buttons"		 => $this->add_button($id, "'.$level[0].'"),
			"buttons_top"	 => true,
			"buttons_bottom" => true,

			// parent
			"show_parent"	 => false
		));
		';

	if($i != 0) $defaultadmin .= '
	} else {

		echo "<p>".$this->Lang("error_noparent")."</p>";

	}';

$defaultadmin .= '
	echo $this->EndTab();
}';

// $i++;
}

#####################################

// ----->>>>>>>>>>>
// wx todo -- change this
// this the old stuff..
// for now it stays...

$defaultadmin .= '
echo $this->EndTabContent();

?'.'>';

?>
