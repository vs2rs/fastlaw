<?php
$template = '<h2>{$searchtitle}: {$what}</h2>
<table>
';

foreach($level[4] as $field){
	$isindexed = (isset($field[5]['innersearch']) && !$field[5]['innersearch'])?false:true;
	if($isindexed && !in_array($field[0],array('id','item_order','isdefault','active','alias')) && ( !in_array($field[1],array(8,9,11,13)) ) ) {
		if($field[1] == '6' || $field[1] == '7'){
			if(false && ($field[5]['listmode'] == 2 || $field[5]['listmode'] == 4)){
				// do nothing - we don't support search in lists with multiple choices yet
			}else{
				$template .= '<tr><td>{$'.$field[0].'_label}</td><td>{$'.$field[0].'_input}</td></tr>
';
			} 
		}else{
			$template .= '<tr><td>{$'.$field[0].'_label}</td><td>{$'.$field[0].'_input}</td></tr>
';
		}
	}
}
$template .= '<tr><td>{$date_modified_label}</td><td>{$date_modified_input}</td></tr>
';
$template .= '</table>
<p>{$submit}</p>';
?>
