<?php
	$lang["friendlyname"] = "Sociālie tīkli";
	$lang["moddescription"] = "Sociālie tīkli";
	$lang["admindescription"] = "Sociālie tīkli";

	$lang["pagemenudelimiter"] = "&nbsp;&#124;&nbsp;";
	$lang["pagemenuoverflow"] = "&nbsp;...&nbsp;";

// advance permission name
	$lang["permissions_advanced"] = "%s - %s - Full Access";
	
// strings for category
	$lang["permissions_manage_category"] = "%s - %s - Labot sociālo tīklu kategorijas";
	$lang["category"] = "Kategorija";
	$lang["category_plural"] = "Kategorijas";
	$lang["add_category"] = "Pievienot kategoriju";
	$lang["add_category_title"] = "Pievienot kategoriju";
	$lang["edit_category"] = "Labot kategoriju";
	$lang["filterby_category"] = "Filter by Category";
	$lang["prompt_deletecategory"] = "You are about to delete this Category (%s)? All children will be lost. Do you wish to continue?";
	
// strings for items
	$lang["permissions_manage_items"] = "%s - %s - Labot sociālos tīklus";
	$lang["items"] = "Sociālais tīkls";
	$lang["items_plural"] = "Sociālie tīkli";
	$lang["items_item_order"] = "Secība";
	$lang["items_item_order_comment"] = "Atstājot tukšu, secība aizpildīsies automātiski";
	$lang["add_items"] = "Pievienot jaunu sociālo tīklu";
	$lang["add_items_title"] = "Pievienot jaunu sociālo tīklu";
	$lang["edit_items"] = "Labot sociālo tīklu";
	$lang["items_link"] = "Sociālā tīkla adrese";
	$lang["items_network"] = "Sociālais tīkls";

	$lang["filterby_items"] = "Filter by Network";
	$lang["prompt_deleteitems"] = "You are about to delete this Network (%s)? Do you wish to continue?";

// strings for general fields
	$lang["id"] = "id";
	$lang["name"] = "Nosaukums";
	$lang["alias"] = "URL";
	$lang["alias_comment"] = "URL is a unique identifier for this object.";
	$lang["isdefault"] = "Is default?";
	$lang["active"] = "Active";
	$lang["parent"] = "Parent";
	$lang["submit"] = "Submit";
	$lang["cancel"] = "Cancel";
	$lang["nbchildren"] = "Nb of items";
	$lang["date_modified"] = "Last Modified";
	$lang["date_created"] = "Date Created";
	
// GENERAL
$lang["pages"] = "Pages: ";
$lang["nextpage"] = "Next page";
$lang["previouspage"] = "Previous page";
$lang["activate"] = "Activate";
$lang["unactivate"] = "Turn off";
$lang["searchthistable"] = "Search this table for:";
$lang["resetorder"] = "Reset order";
$lang["Yes"] = "Yes";
$lang["No"] = "No";
$lang["Actions"] = "Actions";
$lang["reorder"] = "Reorder";
$lang["listtemplate"] = "List template for";
$lang["templates"] = "Templates";
$lang["template"] = "Template";
$lang["defaulttemplates"] = "Default templates";
$lang["templatevars"] = "Template variables";
$lang["edittemplate"] = "Edit template";
$lang["deftemplatefor"] = "Default list template for level ";
$lang["defdetailtemplate"] = "Default detail template";
$lang["defsearchresultstemplate"] = "Default template for search results";
$lang["defemptytemplate"] = "Template for empty result sets";
$lang["uselevellisttpl"] = "Use the list template of the appropriate level";
$lang["addtemplate"] = "Add template";
$lang["filterby"] = "Filter by";
$lang["showingonly"] = "Filter: ";
$lang["showall"] = "Show all (no filter)";
$lang["fieldoptions"] = "Field options";
$lang["addoption"] = "Add an option";
$lang["modifyanoption"] = "Modify an option";
$lang["message_deleted"] = "Element deleted";
$lang["message_modified"] = "Modification saved";
$lang["warning_tab"] = "Notice: Save changes in other tabs before working in this one...";
$lang["error_missginvalue"] = "One or more necessary values have not been entered.";
$lang["error_missginlevel"] = "Level parameter not set in the add form.";
$lang["error_alreadyexists"] = "There is already an element bearing that name.";
$lang["error_aliasexists"] = "There is already an element that has the same URL. Please check and fix these fields:";
$lang["error_date"] = "The date you have entered is invalid.";
$lang["error_noparent"] = "No parent is defined!";
$lang["error_notfound"] = "The item could not be found.";
$lang["error_noitemfound"] = "No item found.";
$lang["error_denied"] = "Permission denied";
$lang["error_wrongquery"] = "Invalid query.";
$lang["error_feadddenied"] = "You do not have sufficient access to continue.";
$lang["error_wrongfiletype"] = "This type of file is not allowed here.";
$lang["error_captcha"] = "The text given does not match the captcha image.";
$lang["error_filetoobig"] = "The file is to big to be uploaded here.";
$lang["givenerror"] = "Error: ";
$lang["finaltemplate"] = "Display template for final level (items)";
$lang["prompt_deleteoption"] = "Do you really want to delete this option?";
$lang["prompt_generaldelete"] = "Do you really want to delete this?";
$lang["prompt_captcha"] = "Enter the text from the image.";
$lang["queries"] = "Queries";
$lang["query"] = "Query";
$lang["results"] = "Results";
$lang["createquery"] = "Create a new query";
$lang["prompt_query"] = "Create a query for which level?";
$lang["orderbyfield"] = "Order by";
$lang["date_modified"] = "Date modified";
$lang["frontend_submit"] = "Submit";


?>