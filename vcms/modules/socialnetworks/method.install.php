<?php
if(!isset($gCms)) exit;

// Typical Database Initialization
$db = &$this->cms->db;
$dict = NewDataDictionary($db);
		
// mysql-specific, but ignored by other database
$taboptarray = array("mysql" => "TYPE=MyISAM");
		

// Creates the category table
$flds = "
	id I NOTNULL AUTOINCREMENT KEY,
	name C(255),
	alias C(255),
	item_order I,
	active L,
	isdefault L,
    date_modified ".CMS_ADODB_DT.",
	date_created ".CMS_ADODB_DT."
	";

$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_socialnetworks_category", $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);


// Creates the items table
$flds = "
	link C(255),
	network C(255),
	parent I,
	id I NOTNULL AUTOINCREMENT KEY,
	name C(255),
	alias C(255),
	item_order I,
	active L,
	isdefault L,
    date_modified ".CMS_ADODB_DT.",
	date_created ".CMS_ADODB_DT."
	";

$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_socialnetworks_items", $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);

// CREATING PERMISSIONS :

// permissions
	$this->CreatePermission("module_socialnetworks_advanced", "Module - Social Networks (Advanced)");
	$this->CreatePermission("module_socialnetworks_manage_category", "Module - Social Networks (Category)");
	$this->CreatePermission("module_socialnetworks_manage_items", "Module - Social Networks (Items)");
	
// activating default preferences
	$defprefs = array("tabdisplay_category","searchmodule_index_category","tabdisplay_items","searchmodule_index_items","restrict_permissions","orderbyname","display_filter","display_instantsearch","display_instantsort","showthumbnails","load_nbchildren","use_session");
	foreach($defprefs as $onepref)	$this->SetPreference($onepref,true);
	$this->SetPreference("emptytemplate","**");
	
// prepare information for an eventual upgrade
	$this->SetPreference("makerversion","1.8.9.3");

// put mention into the admin log
	$this->Audit( 0, $this->Lang("friendlyname"), $this->Lang("installed",$this->GetVersion()));

?>
