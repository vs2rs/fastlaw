<?php
if (!isset($gCms)) exit;
$themeObject = $gCms->variables["admintheme"];
$active_tab = isset($params["active_tab"])?$params["active_tab"]:"items";

$has_advanced_perm = $this->CheckPermission("module_socialnetworks_advanced");

// check if module message is set in url
if($module_message = $this->get_module_message($params)) {
	$message = $themeObject->ShowMessage($module_message);
}

/**

 * wx/vcms
 * we check for post

*/

// get the levels
$all_levels = $this->get_levelarray();

// check for post
if(isset($_POST)) {

	// set active tab
	if(isset($_POST["active_tab"])
	&& in_array($_POST["active_tab"], $all_levels)) {
		$active_tab = $_POST["active_tab"];
	}

	// check post
	foreach ($all_levels as $post_level) {
		if(isset($_POST["add-new-".$post_level."-name"])) {
			// wx includ all the post stuff
			include_once($gCms->config["modules_system"] . "/CTLModuleMaker/includes/create.gallery.php");
			// end foreach
			break;
		}
	}

}

// end post

$tab_count = 0;if($this->showTab("category")) $tab_count++;if($this->showTab("items")) $tab_count++;
if($tab_count > 1) {
	echo $this->StartTabHeaders();

	if($this->showTab("items")) {
		echo $this->SetTabHeader("items", $this->Lang("items_plural"), "items" == $active_tab ? true : false);
	}
	
	if($this->showTab("category")) {
		echo $this->SetTabHeader("category", $this->Lang("category_plural"), "category" == $active_tab ? true : false);
	}

	echo $this->EndTabHeaders();

}

echo $this->StartTabContent();

if($this->showTab("items")) {

	echo $this->StartTab("items"); 

	if($this->countsomething("category") > 0){

		// get items
		$itemlist = $this->get_level_items(
			isset($where)?$where:array(), true, $id, $returnid, false
		);

		// setup cells
		$cells = array(
			array(
				"key"    => "item_order",
				"action" => "item_order",
				"label"  => lang("order"),
				"class"  => "  list__cell--id"
			),
			array(
				"key"    => "editlink",
				"action" => "edit",
				"label"  => $this->lang("items"),
				"class"  => "  list__cell--fill"
			),
			array(
				"key"    => "editbutton",
				"action" => "edit",
				"label"  => lang("edit"),
				"class"  => "  list__cell--icon"
			),
			array(
				"key"    => "toggleactive",
				"action" => "active",
				"label"  => lang("active"),
				"class"  => "  list__cell--icon"
			),
			array(
				"key"    => "deletelink",
				"action" => "delete",
				"label"  => lang("delete"),
				"class"  => "  list__cell--icon"
			),
		);

		// table
		echo $themeObject->CreateTable(array(
			// content
			"items"			 => $itemlist,
			"cells"			 => $cells,
			"message"		 => isset($message) && $active_tab == "items" ? $message : "",
			
			// buttons
			"buttons"		 => $this->add_button($id, "items"),
			"buttons_top"	 => true,
			"buttons_bottom" => true
		));
		
	} else {

		echo "<p>".$this->Lang("error_noparent")."</p>";

	}
	echo $this->EndTab();
}

if($this->showTab("category")) {

	echo $this->StartTab("category"); 

		// get items
		$itemlist = $this->get_level_category(
			isset($where)?$where:array(), true, $id, $returnid, false
		);

		// setup cells
		$cells = array(
			array(
				"key"    => "item_order",
				"action" => "item_order",
				"label"  => lang("order"),
				"class"  => "  list__cell--id"
			),
			array(
				"key"    => "editlink",
				"action" => "edit",
				"label"  => $this->lang("category"),
				"class"  => "  list__cell--fill"
			),
			array(
				"key"    => "editbutton",
				"action" => "edit",
				"label"  => lang("edit"),
				"class"  => "  list__cell--icon"
			),
			array(
				"key"    => "toggleactive",
				"action" => "active",
				"label"  => lang("active"),
				"class"  => "  list__cell--icon"
			),
			array(
				"key"    => "deletelink",
				"action" => "delete",
				"label"  => lang("delete"),
				"class"  => "  list__cell--icon"
			),
		);

		// table
		echo $themeObject->CreateTable(array(
			// content
			"items"			 => $itemlist,
			"cells"			 => $cells,
			"message"		 => isset($message) && $active_tab == "category" ? $message : "",
			
			// buttons
			"buttons"		 => $this->add_button($id, "category"),
			"buttons_top"	 => true,
			"buttons_bottom" => true
		));
		
	echo $this->EndTab();
}

echo $this->EndTabContent();

?>