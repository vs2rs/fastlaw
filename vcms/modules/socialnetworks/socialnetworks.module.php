<?php
#-------------------------------------------------------------------------
# Module: socialnetworks
# Version: 1.0, 
#
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2008 by Ted Kulp (wishy@cmsmadesimple.org)
# This project"s homepage is: http://www.cmsmadesimple.org
#
# This module was created with CTLModuleMaker 1.8.9.3
# CTLModuleMaker was created by Pierre-Luc Germain and is released under GNU
# http://dev.cmsmadesimple.org/projects/ctlmodulemaker
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------

class socialnetworks extends CMSModule
{
	var $currenttree = false;
	var $currentpageindex = 1;
	var $plcurrent = array();

	function GetName()
	{
		return "socialnetworks";
	}

	// wx ==> return current path
	function GetModulePath()
	{
		return dirname(__FILE__);
	}

	/*---------------------------------------------------------
	   GetFriendlyName()
	   This can return any string, preferably a localized name
	   of the module. This is the name that"s shown in the
	   Admin Menus and section pages (if the module has an admin
	   component).
	   
	   See the note on localization at the top of this file.
	  ---------------------------------------------------------*/
	function GetFriendlyName()
	{
		return $this->Lang("friendlyname");
	}

	/*---------------------------------------------------------
		>> wx
		>> get level name
	---------------------------------------------------------*/
	function getLevelNameFromPrefix($moduleInfo)
    {
		
		// get the prefix
		$moduleInfo = explode(",", $moduleInfo);
		$prefix = str_replace("edit", "", $moduleInfo[2]);

		// setup level array
		$return = array();
		$return["cat_"] = "category";
		$return["item_"] = "items";

		// return
		return $return[$prefix];

    }

	/*---------------------------------------------------------
		>> wx
		>> get table name for the level
	---------------------------------------------------------*/
	function getTableName($level)
    {

		// setup level array
		$return = array();
		$return["category"] = $this->GetName() . "_category";
		$return["items"] = $this->GetName() . "_items";

		// return
		return isset($return[$level]) ? $return[$level] : false;

    }

	/*---------------------------------------------------------
		>> wx
		>> get table name for the level
	---------------------------------------------------------*/
	function getLevelPrefix($level)
    {

		// setup level array

		$return = array();
		$return["category"] = "cat_";
		$return["items"] = "item_";

		// return
		return isset($return[$level]) ? $return[$level] : false;

    }

	/*---------------------------------------------------------
		>> wx
		>> show or hide tab from users
	---------------------------------------------------------*/
	function showTab($tab) {
		
		// get module name
		$module_name = $this->GetName();

		// check advance perms
		$perm = $this->CheckPermission("module_" . $module_name . "_advanced");

		// setup tab access
		$showTab = array(
			"category" 		=> $perm,
			"items" 		=> true,
		);

		// return
		if(isset($showTab[$tab])) {
			return $showTab[$tab];
		} else {
			return $perm;
		}

		return false;

	}

	/*---------------------------------------------------------
		>> wx
		>> show buttons on top of the edit form
	---------------------------------------------------------*/
	function showFormButtonsTop() {
		return true;
	}
	
	/*---------------------------------------------------------
	   GetVersion()
	   This can return any string, preferably a number or
	   something that makes sense for designating a version.
	   The CMS will use this to identify whether or not
	   the installed version of the module is current, and
	   the module will use it to figure out how to upgrade
	   itself if requested.	   
	  ---------------------------------------------------------*/
	function GetVersion()
	{
		return "1.0";
	}


	/*---------------------------------------------------------
	   GetDependencies()
	   Your module may need another module to already be installed
	   before you can install it.
	   This method returns a list of those dependencies and
	   minimum version numbers that this module requires.
	   
	   It should return an hash, eg.
	   return array("somemodule"=>"1.0", "othermodule"=>"1.1");
	  ---------------------------------------------------------*/
	function GetDependencies()
	{
		return array();
	}

	/*---------------------------------------------------------
	   GetHelp()
	   This returns HTML information on the module.
	   Typically, you"ll want to include information on how to
	   use the module.
	   
	   See the note on localization at the top of this file.
	  ---------------------------------------------------------*/
	function GetHelp()
	{
		return $this->Lang("help");
	}

	/*---------------------------------------------------------
	   GetAuthor()
	   This returns a string that is presented in the Module
	   Admin if you click on the "About" link.
	  ---------------------------------------------------------*/
	function GetAuthor()
	{
		return "CTLModuleMaker 1.8.9.3";
		// of course you may change this, but it would be nice
		// to keep a mention of the CTLModuleMaker somewhere
	}


	/*---------------------------------------------------------
	   GetAuthorEmail()
	   This returns a string that is presented in the Module
	   Admin if you click on the "About" link. It helps users
	   of your module get in touch with you to send bug reports,
	   questions, cases of beer, and/or large sums of money.
	  ---------------------------------------------------------*/
	function GetAuthorEmail()
	{
		return "";
	}


	/*---------------------------------------------------------
	   IsPluginModule()
	   This function returns true or false, depending upon
	   whether users can include the module in a page or
	   template using a smarty tag of the form
	   {cms_module module="Prod" param1=val param2=val...}
	   
	   If your module does not get included in pages or
	   templates, return "false" here.
	  ---------------------------------------------------------*/
	function IsPluginModule()
	{
		return true;
	}


	/*---------------------------------------------------------
	   HasAdmin()
	   This function returns a boolean value, depending on
	   whether your module adds anything to the Admin area of
	   the site. For the rest of these comments, I"ll be calling
	   the admin part of your module the "Admin Panel" for
	   want of a better term.
	  ---------------------------------------------------------*/
	function HasAdmin() {	return true;	}
	function GetAdminSection() {return "content";}
	function GetAdminDescription() {return $this->Lang("admindescription");}
	
	/*---------------------------------------------------------
	   Module Constructor 
	---------------------------------------------------------*/
	function __construct()
	{
		global $gCms;
		parent::CMSModule();
	}
    
	function InstallPostMessage()
	{
		return $this->Lang("postinstall");
	}
	function UninstallPostMessage()
	{
		return $this->Lang("postuninstall");
	}
	function UninstallPreMessage()
	{
		return $this->Lang("really_uninstall");
	}

	/*---------------------------------------------------------
	   Install()
	   When your module is installed, you may need to do some
	   setup. Typical things that happen here are the creation
	   and prepopulation of database tables, database sequences,
	   permissions, preferences, etc.
	   	   
	   For information on the creation of database tables,
	   check out the ADODB Data Dictionary page at
	   http://phplens.com/lens/adodb/docs-datadict.htm
	   
	   This function can return a string in case of any error,
	   and CMS will not consider the module installed.
	   Successful installs should return FALSE or nothing at all.
	  ---------------------------------------------------------*/
	function Install()
	{
		global $gCms;
		require "method.install.php";
	}

	/*---------------------------------------------------------
	   Uninstall()
	   Sometimes, an exceptionally unenlightened or ignorant
	   admin will wish to uninstall your module. While it
	   would be best to lay into these idiots with a cluestick,
	   we will do the magnanimous thing and remove the module
	   and clean up the database, permissions, and preferences
	   that are specific to it.
	   This is the method where we do this.
	  ---------------------------------------------------------*/
	function Uninstall()
	{
		global $gCms;
		require "method.uninstall.php";
	}	

	/* ---------------------------------------------
	NOT PART OF THE NORMAL MODULE API
	----------------------------------------------*/

    function getDefaultTemplates(){
    	// returns an array of the templates that are selected as default (just so that we don't delete them)
	   $result = array();
	   $result[] = $this->GetPreference("finaltemplate");
	   $result[] = $this->GetPreference("searchresultstemplate");
	   $result[] = $this->GetPreference("listtemplate_category");
	   $result[] = $this->GetPreference("listtemplate_items");
	   return $result;
    }
	
    function getOrderType($what){
		// returns whether a level is ordered by parent or not
		$return = array("category"=>true,"items"=>true);
		return (isset($return[$what])?$return[$what]:false);
    }
    
	function plcreatealias($name){
		// transforms $name into a url-friendly alias
		
		// as a suggestion from AMT, the first part deals with smart quotes
 		$search = array(chr(0xe2) . chr(0x80) . chr(0x98),
						  chr(0xe2) . chr(0x80) . chr(0x99),
						  chr(0xe2) . chr(0x80) . chr(0x9c),
						  chr(0xe2) . chr(0x80) . chr(0x9d),
						  chr(0xe2) . chr(0x80) . chr(0x93),
						  chr(0xe2) . chr(0x80) . chr(0x94));
 		$name = str_replace($search, "", $name);
		
 		// the second part uses the cms version
 		$alias = munge_string_to_url($name, false);
 		return $alias;
	}

	function checkalias($dbtable, $alias, $itemid=false, $idfield="id", $aliasfield="alias"){
		// checks if this alias already exists in the level
		$query = "SELECT ".$idfield." FROM ".cms_db_prefix().$dbtable." WHERE ".$aliasfield." = ?";
		if($itemid) $query .= " AND ".$idfield."!=".$itemid;
		$db = $this->GetDb();
		$dbresult = $db->Execute($query,array($alias));
		$targetid = 0;
		if($dbresult && $row = $dbresult->FetchRow()) $targetid = $row["id"];
		return ($targetid == 0);
	}

	function get_levelarray(){
		// returns an array of the levels (top to bottom)
		return array("category","items");
	}

	function get_modulehierarchy($level=false){
		$hierarchy = array();
			$hierarchy[1] = "category";
			$hierarchy[2] = "items";
		return $hierarchy;
	}
	
	function countsomething($tablename,$what="id",$where=array(),$wherestring=false,$wherevalues=array(),$parent=false){
		// returns the number of elements in a table corresponding to criterias
		if(!$parent && isset($where["parent"]))	unset($where["parent"]);
		$db =& $this->GetDb();
		if($wherestring){
			$wherestring = " WHERE ".$wherestring;
		}else{
			$wherestring = "";
			$wherevalues = array();
			foreach($where as $key=>$value){
				if($key == "parent"){
					$wherestring .= ($wherestring == ""?" WHERE ":" AND ")."A.parent=B.id AND B.alias=?";
				}else{
					$wherestring .= ($wherestring == ""?" WHERE ":" AND ")."A.".$key."=?";
				}
				$wherevalues[] = $value;
			}
		}
		$query = "SELECT COUNT(A.$what) ourcount FROM ".cms_db_prefix()."module_".$this->GetName()."_".$tablename." A";
		if($parent && isset($what["parent"]))	$query .= ", ".cms_db_prefix()."module_".$this->GetName()."_".$parent." B";
		$query .= $wherestring;
		$dbresult = $db->Execute($query,$wherevalues);
		if ($dbresult && $row = $dbresult->FetchRow()){
			return $row["ourcount"];
		}else{
			return 0;
		}
	}

	function get_nextlevel($curlevel,$findchild=true){
		// return the name of the level below ($findchild=true) or above ($findchild=false)
		$levels = $this->get_levelarray();
		$i = 0;
		$wantedlevel = false;
		while($i < count($levels)){
			$next = $findchild?$i+1:$i-1;
			if($levels[$i] == $curlevel && isset($levels[$next])) $wantedlevel = $levels[$next];
			$i++;
		}
		return $wantedlevel;
	}

	function get_parents($parentname,$childname,$childid){
		// when using the sharechildren option, retrieves the parents of a given element
		$db =& $this->GetDb();
		$query = "SELECT ".$parentname."_id parentid FROM ".cms_db_prefix()."module_".$this->GetName()."_".$parentname."_has_".$childname." WHERE ".$childname."_id=?";
		$dbresult = $db->Execute($query,array($childid));
		$parents = array();
		while ($dbresult && $row = $dbresult->FetchRow()){
			$parents[] = $row["parentid"];
		}
		return $parents;
	}

	function addadminlinks($item,$params,$id,$returnid){
		// adds the admin links to the level items...
		/* $params : the base parameters for the admin links
				prefix
				tablename
				child (if the level has a child level, the name of the child level)
				levelname
				parentdefault (bool... whether or not there is a default element for each parent)
				orderbyparent (bool)
				addfiles
				sharechildren (bool)
				sharedbyparents (bool)
				files (string,csv)
		*/

		global $gCms;
		$admintheme = $gCms->variables["admintheme"];
		
		if( $this->GetPreference("restrict_permissions",false) && !$this->CheckPermission("module_".$this->GetName()."_advanced") && !$this->CheckPermission("module_".$this->GetName()."_manage_".$params["levelname"]) ){
			// no access - we don't create links
			$item->editlink = $item->name;
			$item->deletelink = "";
			$item->movelinks = "";
			$item->toggleactive = $item->active==1?$admintheme->DisplayImage("icons/system/true.gif","","","","systemicon"):$admintheme->DisplayImage("icons/system/false.gif","","","","systemicon");
			$item->toggledefault = $item->isdefault==1?$admintheme->DisplayImage("icons/system/true.gif","","","","systemicon"):$admintheme->DisplayImage("icons/system/false.gif","","","","systemicon");
			return $item;
		}
		
		$prefix = $params["prefix"];
		$moveparams = $params;
		$moveparams[$prefix."id"] = $item->id;
		$moveparams["currentorder"] = $item->item_order;
		if(isset($moveparams["files"]))	unset($moveparams["files"]);
		if($moveparams["orderbyparent"] && isset($item->parent_id)) $moveparams["parent"] = $item->parent_id;

		// edit button
		$item->editlink = $this->CreateLink(
			$id,
			"edit" . $prefix,
			$returnid,
			$item->name,
			array(
				$prefix. "id" => $item->id,
				"class" => "list__item--link  color-one-text"
			)
		);

		// edit button
		$item->editbutton = $this->CreateLink(
			$id,
			"edit" . $prefix,
			$returnid,
			$admintheme->DisplayIcon("edit"),
			array(
				$prefix. "id" => $item->id,
				"class" => "button-icon"
			)
		);

		// delete button
		$item->deletelink = $this->CreateLink(
			$id,
			"movesomething",
			$returnid,
			$admintheme->DisplayIcon("delete"),
			array_merge(
				array(
					"move" => "delete",
					"files" => $params["files"],
					"class" => "button-icon  button-icon--delete"
				),
				$moveparams
			),
			$this->Lang(
				"prompt_delete" . $params["levelname"],
				str_replace("'", "\'", $item->name)
			)
		);

		// move item buttons
		$item->moveuplink = $this->CreateLink($id, "movesomething", $returnid, $admintheme->DisplayImage("icons/system/arrow-u.png",lang("up"),"","","systemicon"), array_merge(array("move"=>"up"),$moveparams));
		$item->movedownlink = $this->CreateLink($id, "movesomething", $returnid, $admintheme->DisplayImage("icons/system/arrow-d.png",lang("down"),"","","systemicon"), array_merge(array("move"=>"down"),$moveparams));
		$item->movelinks = $item->moveuplink ." ". $item->movedownlink;
		
		// toggle buttons

		// active

		// we rebuild the params, because we don't need so many for the toggle action
		$toggleparams = array(
			$prefix."id"=>$item->id,
			"prefix"=>$prefix,
			"tablename"=>$params["tablename"],
			"levelname"=>$params["levelname"],
			"class" => "button-icon"
		);
		if($params["parentdefault"] && isset($item->parent_id)){
			$toggleparams["parent"] = $item->parent_id;
			$toggleparams["parentdefault"] = 1;
		}
		if($item->active == 1) {
			$item->toggleactive = $this->CreateLink(
				$id,
				"toggle",
				$returnid,
				$admintheme->DisplayIcon("true"),
				array_merge(
					array("what" => "active", "newval" => 0),
					$toggleparams
				)
			);
		} else {
			$item->toggleactive = $this->CreateLink(
				$id,
				"toggle",
				$returnid,
				$admintheme->DisplayIcon("false"),
				array_merge(
					array("what" => "active", "newval" => 1),
					$toggleparams
				)
			);
		}

		// default

		if ($item->isdefault == 1){
			$item->toggledefault = $this->CreateLink($id, "toggle", $returnid, $admintheme->DisplayImage("icons/system/true.gif",lang("setfalse"),"","","systemicon"), array_merge(array("what"=>"default","newval"=>0),$toggleparams));
		}else{
			$item->toggledefault = $this->CreateLink($id, "toggle", $returnid, $admintheme->DisplayImage("icons/system/false.gif",lang("settrue"),"","","systemicon"), array_merge(array("what"=>"default","newval"=>1),$toggleparams));
		}

		return $item;

	}

	function get_level_category($where=array(),$admin=false,$id="",$returnid="",$order=false,$limit=0,$customwhere=false,$customvalues=array(),$customorder=false){
		global $gCms;
		$load_nbchildren = $this->GetPreference("load_nbchildren",true);
		if($admin)	$admintheme = $gCms->variables["admintheme"];
		if(!$order)	$order = "";
		$db =& $this->GetDb();
		
		// wx
		$fields = $this->get_level_category_fields("keys");
		
		$wherestring = "";
		$wherevalues = array();
		

		if($customwhere){
			$wherestring = $customwhere;
			$wherevalues = $customvalues;
		}else{
			foreach($where as $key=>$value){
				if(in_array(strtolower($key), $fields)){
					$wherestring .= ($wherestring == ""?"":" AND ").$key."=?";
					$wherevalues[] = $value;
				}
			}
		}
		$query = "SELECT * FROM ".cms_db_prefix()."module_socialnetworks_category A ".($wherestring == ""?"":" WHERE ".$wherestring);
		$query .= ($customorder?" ORDER BY ".$customorder:" ORDER BY item_order");

		if($limit && $limit != "0" && $limit != "")	$query .= " LIMIT ".$limit;

		$dbresult = $db->Execute($query,$wherevalues);
		$itemlist = array();
		$idlist = "";
		while ($dbresult && $row = $dbresult->FetchRow()){
			$item = new stdClass();
			$item->__what = "category";
			foreach($row as $key=>$value){
				$item->$key = $value;
			}

			// wx get the text input fields
			$input_list = $this->get_level_category_fields("text_input_keys");

			// wx strip slashes from the input text fields
			foreach ($input_list as $input_name) {
				$item->$input_name = stripslashes($item->$input_name);
			}

			// wx parent name strip slashes
			if(isset($item->parent_name)) {
				$item->parent_name = stripslashes($item->parent_name);
			}
			


			if($admin == true){
				// $parms will be the base for parameters of the admin links
				$parms = array(
					"prefix"=>"cat_",
					"tablename"=>"socialnetworks_category",
					"levelname"=>"category",
					"child"=>"socialnetworks_items",
					"parentdefault"=>false,
					"orderbyparent"=>true,
					"addfiles"=>"",
					"sharechildren"=>false,
					"sharedbyparents"=>false,
					"files"=>""
					);
				$item = $this->addadminlinks($item,$parms,$id,$returnid);
			}
			$idlist .= ($idlist==""?"":" OR ")." parent='".$item->id."'";
			array_push($itemlist,$item);
		}

		if($admin || $load_nbchildren){
			$nbchildrens = array();
			$query = "SELECT parent, count(id) nbchildren FROM ".cms_db_prefix()."module_socialnetworks_items WHERE (".$idlist.") ".($admin?"":"AND active=1 ")."GROUP BY parent";
			$dbresult = $db->Execute($query);
			while($dbresult && $row = $dbresult->FetchRow())	$nbchildrens[$row["parent"]] = $row["nbchildren"];
			$newlist = array();
			foreach($itemlist as $item){
				$item->nbchildren = isset($nbchildrens[$item->id])?$nbchildrens[$item->id]:false;
				array_push($newlist, $item);
			}
			$itemlist = $newlist;
		}
		return $itemlist;
	}

	// wx function
	function get_level_category_fields($output=false) {

		/*

		Addition settings:
		
		// set to true or false or check permisions
		, "readonly" => $this->CheckPermission("module_".$this->GetName()."_advanced") ? false : true

		// text input
		"???" => array(
			"type" => "input",
			"size" => 50,
			"length" => 255,
			"validate" => array(
				"not_empty" => true,
				"value" => "name"
			)
		),

		// date
		"???" => array(
			"type" 		 => "date",
			"year_start" => 1990 // optional
		),

		// checkbox
		"???" => array(
			"type" 		=> "checkbox"
			"label"		=> $this->lang("items_"),
		),

		// select
		"???" => array(
			"type"		=> "select",
			"options"	=> array()
		),

		// image
		"img" => array(
			"type"			=> "image",
			"folder"		=> "properties",
			"class"			=> "",
			"button_select" => $this->lang("items_image_button_select"),
			"button_remove" => $this->lang("items_image_button_remove"),
			"tab"			=> 0,
			"col"			=> 1
		),

		// date
		"date" => array(
			"type"		=> "date",
			"year_end"	=> date("Y") + 1,
			"tab"		=> 1
		),

		// info
		"info" => array(
			"type"		=> "textarea",
			"height"	=> 200,
			"class"		=> "  text-editor"
		),

		*/

		$fields = array(

			// fields
			"id" => false,
			"name" => array(
					"type" => "input",
					"size" => 50,
					"length" => 255,
					// label is optional, will look for label in module lang file
					"label" => $this->Lang("name"),
					// validate is optional
					// name will be validated by default
					"validate" => array(
						"not_empty" => true,
						"value" => "name" // check function validate_string() for options
					)
				),

			"alias" => array(
					"type" => "input",
					"size" => 50,
					"length" => 255,
					// label is optional, will look for label in module lang file
					"label" => $this->Lang("alias"),
					// comment is optional
					"comment" => $this->Lang("alias_comment")
				),

			"item_order" => array(
					"type" => "input",
					"size" => 10,
					"length" => 10
				),

			"active" => false,
			"isdefault" => false,
			"date_modified" => false,
			"date_created" => false

		);

		return $this->get_level_fields("category", $fields, $output);

	}


	function get_level_items($where=array(),$admin=false,$id="",$returnid="",$order=false,$limit=0,$customwhere=false,$customvalues=array(),$customorder=false){
		global $gCms;
		$load_nbchildren = $this->GetPreference("load_nbchildren",true);
		if($admin)	$admintheme = $gCms->variables["admintheme"];
		if(!$order)	$order = "";
		$db =& $this->GetDb();
		
		// wx
		$fields = $this->get_level_items_fields("keys");
		
		$wherestring = "";
		$wherevalues = array();
		

		if($customwhere){
			$wherestring = $customwhere;
			$wherevalues = $customvalues;
		}else{
			foreach($where as $key=>$value){
				if($key == "parent"){
					$wherestring .= ($wherestring == ""?"":" AND ")."B.alias=?";
					$wherevalues[] = $value;
				}elseif($key == "parent_id"){
					$wherestring .= ($wherestring == ""?"":" AND ")."B.id=?";
					$wherevalues[] = $value;
				}elseif(in_array(strtolower($key), $fields)){
					$wherestring .= ($wherestring == ""?"":" AND ")."A.".$key."=?";
					$wherevalues[] = $value;
				}
			}
		}
		
		$query = "SELECT A.*, B.id parent_id, B.name parent_name, B.alias parent_alias FROM ".cms_db_prefix()."module_socialnetworks_items A, ".cms_db_prefix()."module_socialnetworks_category B WHERE A.parent = B.id ".($wherestring == ""?"":" AND ").$wherestring;
		if($customorder){
			 $query .= " ORDER BY A.".$customorder;
		}elseif($order == "modified"){
			 $query .= " ORDER BY A.date_modified DESC";
		}elseif($order == "created"){
			 $query .= " ORDER BY A.id DESC";
		}elseif( $order != "" && ($this->GetPreference("allow_complex_order",false) || in_array(strtolower($order), $fields)) ){
			$query .= " ORDER BY A.".$order;
		}else{
			 $query .= " ORDER BY B.item_order, A.item_order";
		}
		if($limit && $limit != "0" && $limit != "")	$query .= " LIMIT ".$limit;

		$dbresult = $db->Execute($query,$wherevalues);
		$itemlist = array();
		$idlist = "";
		while ($dbresult && $row = $dbresult->FetchRow()){
			$item = new stdClass();
			$item->__what = "items";
			foreach($row as $key=>$value){
				$item->$key = $value;
			}

			// wx get the text input fields
			$input_list = $this->get_level_items_fields("text_input_keys");

			// wx strip slashes from the input text fields
			foreach ($input_list as $input_name) {
				$item->$input_name = stripslashes($item->$input_name);
			}

			// wx parent name strip slashes
			if(isset($item->parent_name)) {
				$item->parent_name = stripslashes($item->parent_name);
			}
			

			$item->nbchildren = false;
		
			if($admin == true){
				// $parms will be the base for parameters of the admin links
				$parms = array(
					"prefix"=>"item_",
					"tablename"=>"socialnetworks_items",
					"levelname"=>"items",
					"child"=>false,
					"parentdefault"=>false,
					"orderbyparent"=>true,
					"addfiles"=>"",
					"sharechildren"=>false,
					"sharedbyparents"=>false,
					"files"=>""
					);
				$item = $this->addadminlinks($item,$parms,$id,$returnid);
			}
			$idlist .= ($idlist==""?"":" OR ")." parent='".$item->id."'";
			array_push($itemlist,$item);
		}

		return $itemlist;
	}

	// wx function
	function get_level_items_fields($output=false) {

		/*

		Addition settings:
		
		// set to true or false or check permisions
		, "readonly" => $this->CheckPermission("module_".$this->GetName()."_advanced") ? false : true

		// text input
		"???" => array(
			"type" => "input",
			"size" => 50,
			"length" => 255,
			"validate" => array(
				"not_empty" => true,
				"value" => "name"
			)
		),

		// date
		"???" => array(
			"type" 		 => "date",
			"year_start" => 1990 // optional
		),

		// checkbox
		"???" => array(
			"type" 		=> "checkbox"
			"label"		=> $this->lang("items_"),
		),

		// select
		"???" => array(
			"type"		=> "select",
			"options"	=> array()
		),

		// image
		"img" => array(
			"type"			=> "image",
			"folder"		=> "properties",
			"class"			=> "",
			"button_select" => $this->lang("items_image_button_select"),
			"button_remove" => $this->lang("items_image_button_remove"),
			"tab"			=> 0,
			"col"			=> 1
		),

		// date
		"date" => array(
			"type"		=> "date",
			"year_end"	=> date("Y") + 1,
			"tab"		=> 1
		),

		// info
		"info" => array(
			"type"		=> "textarea",
			"height"	=> 200,
			"class"		=> "  text-editor"
		),

		*/

		// get parent list
		$parent_list = $this->get_parent_list("items");	

		$fields = array(

			"network" => array(
				"type"		=> "select-social",
				"options"	=> array(
					array(
						"value" => "behance",
						"name"	=> "Bēhance"
					),
					array(
						"value" => "discord",
						"name"	=> "Discord"
					),
					array(
						"value" => "draugiem",
						"name"	=> "Draugiem"
					),
					array(
						"value" => "dribbble",
						"name"	=> "Dribbble"
					),
					array(
						"value" => "facebook",
						"name"	=> "Facebook"
					),
					array(
						"value" => "instagram",
						"name"	=> "Instagram"
					),
					array(
						"value" => "linkedin",
						"name"	=> "LinkedIn"
					),
					array(
						"value" => "pinterest",
						"name"	=> "Pinterest"
					),
					array(
						"value" => "reddit",
						"name"	=> "Reddit"
					),
					array(
						"value" => "skype",
						"name"	=> "Skype"
					),
					array(
						"value" => "snapchat",
						"name"	=> "Snapchat"
					),
					array(
						"value" => "tumblr",
						"name"	=> "Tumblr"
					),
					array(
						"value" => "tripadvisor",
						"name"	=> "TripAdvisor"
					),
					array(
						"value" => "twitter",
						"name"	=> "Twitter"
					),
					array(
						"value" => "vimeo",
						"name"	=> "Vimeo"
					),
					array(
						"value" => "youtube",
						"name"	=> "YouTube"
					)
				)
			),

			"name" => array(
				"type" => "input",
				"size" => 50,
				"length" => 255,
				// label is optional, will look for label in module lang file
				"label" => $this->Lang("name"),
				// validate is optional
				// name will be validated by default
				"validate" => array(
					"not_empty" => true,
					"value" => "title" // check function validate_string() for options
				)
			),

			// fields
			"link" => array(
				"type" => "input",
				"size" => 50,
				"length" => 255,
				"validate" => array(
					"not_empty" => true
				)
			),

			"item_order" => array(
				"type" 		=> "item-order",
				"size" 		=> 10,
				"length" 	=> 10,
				"label"		=> lang("order"),
				"comment" 	=> lang("order_comment"),
				"validate" => array(
					"not_empty" => false,
					"value" => "number"
				)
			),

			"parent" => $parent_list ? array(
				"class" 	=> "  form__row--hidden",
				"type"		=> "select",
				"options"	=> $parent_list,
				"label"		=> $this->Lang("items")
			) : false,

			"id" => false,

			"alias" => array(
				"type" => "hidden",
				"size" => 50,
				"length" => 255,
				// label is optional, will look for label in module lang file
				"label" => $this->Lang("alias"),
				// comment is optional
				"comment" => $this->Lang("alias_comment"),
				// overwrite the preferances
				"autoincrement_alias" => true
			),

			"active" => false,
			"isdefault" => false,
			"date_modified" => false,
			"date_created" => false

		);

		return $this->get_level_fields("items", $fields, $output);

	}



	// wx create input for smarty
	// rertun an input object
	function createInputForSmarty($input) {
		require $this->config["modules_system"] . "/CTLModuleMaker/includes/create.input.for.smarty.php";
		return $result;
	}

	function getFileContent($filename){
		// returns the content of a file
		$filepath = dirname(__FILE__).DIRECTORY_SEPARATOR.$filename;
		if(file_exists($filepath)){
			$fhandle = fopen($filepath, "r+");
			$content = fread($fhandle, filesize($filepath));
			return $content;
		}else{
			return false;
		}
	}

}

?>
