<?php

// ------------------------------------
// -- globals
// ------------------------------------

$lang['admin']['move']      = 'Pārvietot';
$lang['admin']['id']        = 'ID';
$lang['admin']['name']      = 'Vārds';
$lang['admin']['name_new']  = 'Jauns vārds';
$lang['admin']['surname']   = 'Uzvārds';
$lang['admin']['no']        = 'Nr.';
$lang['admin']['email']     = 'E-pasts';
$lang['admin']['phone']     = 'Telefons';
$lang['admin']['active']    = 'Aktīvs';
$lang['admin']['username']	= 'Lietotājvārds';
$lang['admin']['edit'] 		= 'Labot';
$lang['admin']['date']		= 'Datums';
$lang['admin']['default']	= 'Galvenais';
$lang['admin']['delete']	= 'Dzēst';
$lang['admin']['firstname'] = 'Vārds';
$lang['admin']['lastname']  = 'Uzvārds';
$lang['admin']['title'] = 'Virsraksts';
$lang['admin']['alias'] = 'URL';
$lang['admin']['image'] = 'Bilde';
$lang['admin']['content'] = 'Saturs';
$lang['admin']['copy'] = 'Kopēt';

$lang['admin']['no_access'] = "Jums nav piekļuves šai administrācijas sekcijai.";
$lang['admin']['uncheck_all'] = 'Noņemt visus ķekšus';
$lang['admin']['deleteconfirm'] = 'Vai tiešām vēlaties izdzēst - %s - ?';

$lang['admin']['apply']   = 'Apstiprināt (un palikt šajā logā)';
$lang['admin']['submit']  = 'Apstiprināt';
$lang['admin']['cancel']  = 'Atcelt';

$lang['admin']['dashboard'] = 'Saturs';

$lang['admin']['permission']  = 'Piekļuve';
$lang['admin']['permissions'] = 'Piekļuves';

$lang['admin']['module'] = 'Modulis';

$lang['admin']['pages'] = 'Sadaļas un struktūra';

$lang['admin']['order'] = 'Secība';
$lang['admin']['order_comment'] = 'Atstājot tukšu, secība aizpildīsies automātiski';

$lang['admin']['file_select'] = 'Izvēlēties';
$lang['admin']['file_remove'] = 'Nodzēst';

$lang['admin']['template'] = 'Veidne';
$lang['admin']['templates'] = 'Veidnes';

$lang['admin']['menutext'] = 'Sadaļas nosaukums galvenājā izvēlnē';

// ------------------------------------
// -- positions
// ------------------------------------

$lang['admin']['left'] = 'Pa kreisi';
$lang['admin']['right'] = 'Pa labi';
$lang['admin']['top'] = 'Augša';
$lang['admin']['middle'] = 'Vidus';
$lang['admin']['bottom'] = 'Apakša';
$lang['admin']['img_position'] = 'Bildes novietojums';
$lang['admin']['img_alignment'] = 'Bildes nolīdzināšana';

// ------------------------------------
// -- languages
// ------------------------------------

$lang['admin']['language'] = 'Administrācijas paneļa valoda';
$lang['admin']['language_default'] = 'Administrācijas paneļa valoda pēc noklusējuma';
$lang['admin']['language_code'] = 'Valodas kods';
$lang['admin']['latvian'] = 'Latviešu valoda';
$lang['admin']['english'] = 'Angļu valoda';

// ------------------------------------
// -- password
// ------------------------------------

$lang['admin']['password_new']    = 'Parole';
$lang['admin']['password_again']  = 'Parole atkārtoti';
$lang['admin']['password_change'] = 'Mainīt paroli';
$lang['admin']['password_show']   = 'Parādīt paroles ievades laukus';
$lang['admin']['password_hide']   = 'Paslēpt paroles ievades laukus';
$lang['admin']['password_no_match'] = 'Ievadītās paroles nesakrīt!';

// ------------------------------------
// -- months
// ------------------------------------

$lang['admin']['months'] = array(
  '01' => 'Janvāris',
  '02' => 'Februāris',
  '03' => 'Marts',
  '04' => 'Aprīlis',
  '05' => 'Maijs',
  '06' => 'Jūnijs',
  '07' => 'Jūlijs',
  '08' => 'Augusts',
  '09' => 'Septembris',
  '10' => 'Oktobris',
  '11' => 'Novembris',
  '12' => 'Decembris'
);

// ------------------------------------
// -- errors
// ------------------------------------

$lang['admin']['error_empty'] = 'Ievades lauks "%s" nevar būt tukšs.';
$lang['admin']['error_not_valid_alias'] = 'Ievades lauks "%s" drīkst saturēt tikai latīņu burtus un ciparus (bez atstarpēm).';
$lang['admin']['error_not_valid_name'] = 'Ievades lauks "%s" drīkst saturēt tikai burtus un ciparus.';
$lang['admin']['error_not_valid_number'] = 'Ievades lauks "%s" drīkst būt tikai vesels skaitls.';
$lang['admin']['error_not_valid_text'] = 'Ievades lauks "%s" satur neatļautus simbolus vai arī nav ievadītīs neviens vārds.';
$lang['admin']['error_not_valid_title'] = 'Ievades lauks "%s" satur neatļautus simbolus vai arī nav ievadītīs neviens vārds.';
$lang['admin']['error_not_valid_subject'] = 'Ievades lauks "%s" satur neatļautus simbolus vai arī nav ievadītīs neviens vārds.';
$lang['admin']['error_not_valid_letters'] = 'Ievades lauks "%s" drīkst saturēt tikai latīņu burtus, defises un atstarpes.';
$lang['admin']['error_not_valid_letters_utf8'] = 'Ievades lauks "%s" drīkst saturēt tikai burtus, defises un atstarpes.';
$lang['admin']['error_not_valid_email'] = 'Ievades lauks "%s" satur neatbilstošu e-pasta adresi.';
$lang['admin']['error_not_valid_color'] = 'Ievades lauks "%s" drīkst saturēt tikai HEX krāsas kodu.';
$lang['admin']['error_unique'] = 'Ievades lauks "%s" satur vērtību, kas jau ir definēta datubāzē.';
$lang['admin']['error_not_valid_url'] = 'Ievades lauks "%s" neatbilst intereneta adresei.';

// ------------------------------------
// -- added, modified, deleted
// ------------------------------------

$lang['admin']['added']     = 'Ieraksts <span class="success-message__name">"%s"</span> tika veiksmīgi izveidots.';
$lang['admin']['modified']  = 'Ieraksts <span class="success-message__name">"%s"</span> tika veiksmīgi saglabāts.';
$lang['admin']['deleted']   = 'Ieraksts <span class="success-message__name">"%s"</span> tika veiksmīgi izdzēsts.';

// ------------------------------------
// -- users & groups
// ------------------------------------

// ------------------------------------
// -- users
// ------------------------------------

$lang['admin']['myprefs'] = 'Lietotāja dati';

$lang['admin']['user']      = 'Lietotājs';
$lang['admin']['user_added'] = 'Lietotājs <span class="success-message__name">"%s"</span> tika veiksmīgi izveidots.';
$lang['admin']['user_modified']  = 'Izmaiņas lietotājā <span class="success-message__name">"%s"</span> tika veiksmīgi saglabātas.';
$lang['admin']['user_added_error']  = 'Lietotāju neizdevās pievienot. Mēģiniet vēlāk.';
$lang['admin']['user_edited_error'] = 'Izmaiņas lietotājā neizdevās saglabāt. Mēģiniet vēlāk.';
$lang['admin']['user_save_error'] = 'Kļūda saglabājot lietotāju. Mēģiniet vēlāk.';
$lang['admin']['user_exists'] = 'Lietotājvārds "%s" ir jau aizņemts. Izvēlaties citu!';

$lang['admin']['users']      = 'Lietotāji';
$lang['admin']['users_add']		= 'Pievienot lietotāju';
$lang['admin']['users_edit']	= 'Labot lietotāju';
$lang['admin']['users_active']  = 'Aktīvs (lietotājs var piekļūt administrācijas panelim)';
$lang['admin']['users_group']	= 'Lietotājs pieder sekojošām grupām';

$lang['admin']['usernameincorrect'] = 'Kļūda lietotājvārdā vai parolē.';

$lang['admin']['user_profile'] = 'Mans profils';
$lang['admin']['user_prefs'] = 'Mani iestatījumi';
$lang['admin']['user_prefs_updated'] = 'Lietotāja iestatījumi veiksmīgi tika saglabāti.';
$lang['admin']['admintheme'] = 'Administrācijas vizuālais izskatas';

// ------------------------------------
// -- groups
// ------------------------------------

$lang['admin']['group'] = 'Lietotāju grupa';
$lang['admin']['group_name'] = 'Lietotāju grupas nosaukums';
$lang['admin']['group_active'] = 'Lietotāji, kas ir šajā grupā, var piekļūt administrācijas panelim';
$lang['admin']['group_add'] = 'Pievienot lietotāju grupu';
$lang['admin']['group_edit'] = 'Labot lietotāju grupu';
$lang['admin']['group_added'] = 'Lietotāju grupa <span class="success-message__name">"%s"</span> tika veiksmīgi izveidota.';
$lang['admin']['group_modified'] = 'Lietotāju grupa <span class="success-message__name">"%s"</span> tika veiksmīgi saglabāta.';
$lang['admin']['group_perms'] = 'Lietotāju grupu piekļuves';
$lang['admin']['group_added_error'] = 'Lietotāju grupu neizdevās pievienot. Mēģiniet vēlāk.';
$lang['admin']['group_edited_error'] = 'Izmaiņas lietotāju grupā neizdevās saglabāt. Mēģiniet vēlāk.';
$lang['admin']['group_save_error'] = 'Kļūda saglabājot lietotāju grupu. Mēģiniet vēlāk.';
$lang['admin']['group_exists'] = 'Lietotāju grupa ar nosaukumu "%s" jau eksistē. Izvēlaties citu nosaukumu!';
$lang['admin']['groups'] = 'Lietotāju grupas';

// ------------------------------------
// -- permissions
// ------------------------------------

$lang['admin']['permissions_modified'] = 'Izmaiņas lietotāju tiesības veiksmīgi tika saglabātās.';

$lang['admin']['admin_clear_admin_log'] 		= 'Administrators - Iztīrīt administrācijas LOG failu';
$lang['admin']['admin_modify_events'] 			= 'Administrators - Labot notikumus (Events)';
$lang['admin']['admin_modify_modules'] 			= 'Administrators - Labot moduļus (Modules)';
$lang['admin']['admin_modify_site_preferences'] = 'Administrators - Mainīt administrācijas iestatījumus';
$lang['admin']['admin_use_ctl_module_maker'] 	= 'Administrators - Veidot moduļus (CTL Module Maker)';
$lang['admin']['admin_pages_view']    = 'Administrators - Sadaļas - Ikona "View" sadaļu sarakstā';
$lang['admin']['admin_pages_copy']    = 'Administrators - Sadaļas - Ikona "Copy" sadaļu sarakstā';

$lang['admin']['pages_add'] 		= 'Sadaļas - Pievienot';
$lang['admin']['pages_delete'] 		= 'Sadaļas - Dzēst';
$lang['admin']['pages_edit'] 		= 'Sadaļas - Labot';
$lang['admin']['pages_move'] 		= 'Sadaļas - Mainīt secību';
$lang['admin']['pages_set_active']	= 'Sadaļas - Mainīt statusu (Aktīvs/Neaktīvs)';
$lang['admin']['pages_set_default'] = 'Sadaļas - Uzstādīt galveno sadaļu pēc noklusējuma';

$lang['admin']['templates_remove'] 				= 'Sagataves - Nodzēst';
$lang['admin']['templates_add'] 				= 'Sagataves - Pievienot';
$lang['admin']['templates_modify'] 				= 'Sagataves - Labot';
$lang['admin']['users_add_remove'] 				= 'Lietotāji - Pievienot, dzēst un labot';
$lang['admin']['users_edit_prefs']        = 'Lietotāji - Labot lietotāja iestatījumus';
$lang['admin']['users_groups_add_remove']		= 'Lietotāji - Grupas - Pievienot, dzēst un labot';
$lang['admin']['users_groups_change_default']	= 'Lietotāji - Grupas - Mainīt galveno pēc noklusējuma';

// ------------------------------------
// -- Settings
// ------------------------------------

$lang['admin']['sitename'] = 'Administrācijas paneļa nosaukums';
$lang['admin']['siteprefs'] = 'Iestatījumi';
$lang['admin']['siteprefs_modified'] = 'Iestatījumi tika veiksmīgi saglabāti';

// ------------------------------------
// -- Load extra lang file if it exists
// ------------------------------------

if(file_exists(PROJECT_PATH . '/admin/lang/lv.php')) {
  include_once(PROJECT_PATH . '/admin/lang/lv.php');
}




