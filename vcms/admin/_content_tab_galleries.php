<?php

/* ------------------------------
// Galleries
------------------------------ */

$galleries = $contentobj->SetupGalleries($pageTemplate->content);
$gallery_output = array();

// run through this
if(count($galleries) > 0) {
	foreach ($galleries as $key => $gallery) {
		
		$gallery_output[$gallery['block']] = '
		<div class="module-wrap  module-wrap--edit-page">
			<div class="module-container  module-container--no-tabs  dropzone-upload-container">
				
				<!-- title -->
				<div class="module-title">'.$gallery['label'].'</div>

				<!-- form -->
				<form action="uploader/index.php?action=upload" class="dropzone" id="'.$gallery['block'].'">';
						
					if(isset($gallery['db']) && $gallery['db'] != "") {
						$gallery_output[$gallery['block']] .= '
						<input type="hidden" name="module"
						value="' . cms_db_prefix() . $gallery['db'] . '" />';
					}

					if(isset($gallery['folder']) && $gallery['folder'] != "") {
						$gallery_output[$gallery['block']] .= '
						<input type="hidden" name="folder"
						value="' . $gallery['folder'] . '" />';
					}

					if(isset($gallery['max_mb']) && $gallery['max_mb'] != "") {
						$gallery_output[$gallery['block']] .= '
						<input type="hidden" name="max_mb"
						value="' . $gallery['max_mb'] . '" />';
					}

					if(isset($gallery['file_types']) && $gallery['file_types'] != "") {
						$gallery_output[$gallery['block']] .= '
						<input type="hidden" name="file_types"
						value="' . $gallery['file_types'] . '" />';
					}

					$gallery_output[$gallery['block']] .= '
					<input type="hidden" name="parent"
					value="pages_' . $contentobj->Id() . '_' . $gallery['block'] . '" />';

				// end
				$gallery_output[$gallery['block']] .=
				'</form>
			</div>
		</div>';

	}
}

foreach ($gallery_output as $gallery) {
	echo $gallery;
}

?>