<?php

// ------------------------------------
// -- header
// ------------------------------------

$header = get_vcms_file("/admin/header.php");

// ------------------------------------
// -- output
// ------------------------------------

// if we have access
if($access) {

	// ------------------------------------
	// -- update actions
	// ------------------------------------

	// error list
	$errors = array();

	// send --> action name --> template id --> current operations obj
	if(isset($_GET['action'])) {
		content_action_json($_GET['action'], floor($_GET['id']), $operations);
	}

	// ------------------------------------
	// -- header
	// ------------------------------------

	// header
	include_once($header);

	// ------------------------------------
	// -- check message in the url
	// ------------------------------------

	if(isset($_GET["message"])) {

		// check for id in the message
		$current_oebject_name = false;
		if(isset($_GET["id"])) {
			$current_oebject = $operations->LoadByID(floor($_GET["id"]));
			$current_oebject_name = $current_oebject->Name();
		}

		// set the message
		$message = $themeObject->ShowMessage(lang($_GET['message'], $current_oebject_name));

	}

	if(isset($_GET["error"])) {
		$message = $themeObject->ShowMessage(lang($_GET['error']), 'error');
	}

	// ------------------------------------
	// -- check for erros set by post
	// ------------------------------------

	// check for errors
	if(count($errors) > 0) {
		$message = $themeObject->ShowMessage($errors, 'error');
	}

	// ------------------------------------
	// -- output list table
	// ------------------------------------

	// if we have groups
	if(count($itemlist) > 0) {

		// table
		echo $themeObject->CreateTable(array(

			// items
			"items"   => $itemlist,
			"cells"   => $cells,

			// messages
			"message" => isset($message) ? $message : "",

			// buttons
			"buttons" => $add_button,
			"buttons_top" => $add_button_top,
			"buttons_bottom" => $add_button_bottom,

		));

	}

} else {

	// ------------------------------------
	// -- no access
	// ------------------------------------

	// header
	include_once($header);

	echo get_html(array(
			
		// error/success messages
		'message'	=> $themeObject->ShowMessage(lang('no_access'), 'error'),

		// template
		'template'	=> VCMS_PATH . '/admin/templates/no-access.html'

	));

}

// ------------------------------------
// -- footer
// ------------------------------------

include_once(get_vcms_file("/admin/footer.php"));





// -- list.php