<?php
#CMS - CMS Made Simple
#(c)2004 by Ted Kulp (wishy@users.sf.net)
#This project's homepage is: http://cmsmadesimple.sf.net
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#$Id: addcontent.php 6811 2010-11-22 15:15:12Z calguy1000 $

global $CMS_ADMIN_PAGE;
$CMS_ADMIN_PAGE=1;

require_once(dirname(dirname(__FILE__)) . "/include.php");

vcmsAuth::is_loged_in();
$userid = get_userid();

require_once(dirname(__FILE__) . '/editcontent_extra.php');

/* ------------------------------

	If cancel redirect

------------------------------ */

if (isset($_POST["cancel"])) {
	redirect("pages.php");
}

/* ------------------------------

	Set some values

------------------------------ */

$error = FALSE;

$firsttime = "1"; #Flag to make sure we're not trying to fill params on the first display
if (isset($_POST["firsttime"])) $firsttime = $_POST["firsttime"];

$submit = false;
if (isset($_POST["submitbutton"])) $submit = true;

$apply = false;
if (isset($_POST["applybutton"])) $apply = true;

// if($submit) {
// 	print_it($_POST);
// 	exit;
// }

$contentobj = '';

#Get current userid and make sure they have permission to add something
$access = (check_permission($userid, 'pages_add'));

// global
global $gCms;

/* ------------------------------

	Get a list of content types
	and pick a default if
	necessary

------------------------------ */

$contentops =& $gCms->GetContentOperations();
$existingtypes = $contentops->ListContentTypes();
$content_type = "";
if (isset($_POST["content_type"])) {
	$content_type = $_POST["content_type"];
} else {
	if (isset($existingtypes) && count($existingtypes) > 0) {
		$content_type = 'content';
	} else {
		$error = "No content types loaded!";
	}
}

/* ------------------------------

	Some post stuff or smthn

------------------------------ */

$contentobj = "";

if (isset($_POST["serialized_content"])) {
	$contentobj = unserialize(base64_decode($_POST["serialized_content"]));
	$contentobj->SetAddMode();
	if (strtolower(get_class($contentobj)) != $content_type)
	{
		#Fill up the existing object with values in form
		#Create new object
		#Copy important fields to new object
		#Put new object on top of old on
		copycontentobj($contentobj, $content_type);
	}
}

else {

	$page_secure = get_site_preference('page_secure',0);
	$page_cachable = ((get_site_preference('page_cachable',"1")=="1")?true:false);
	$active = ((get_site_preference('page_active',"1")=="1")?true:false);
	$showinmenu = ((get_site_preference('page_showinmenu',"1")=="1")?true:false);
	$metadata = get_site_preference('page_metadata');

	$parent_id = get_preference($userid, 'default_parent', -2);
	if (isset($_GET["parent_id"])) $parent_id = $_GET["parent_id"];

	$contentops =& $gCms->GetContentOperations();
	$contentobj = $contentops->CreateNewContent($content_type);
	$contentobj->SetAddMode();
	$contentobj->SetOwner($userid);
	$contentobj->SetCachable($page_cachable);
	$contentobj->SetActive($active);
	$contentobj->SetShowInMenu($showinmenu);
	$contentobj->SetLastModifiedBy($userid);

	$templateops =& $gCms->GetTemplateOperations();
	$dflt = $templateops->LoadDefaultTemplate();
	if(isset($dflt)) {
		$contentobj->SetTemplateId($dflt->id);
	}

	if ($parent_id != -1) $contentobj->SetParentId($parent_id);

}

/* ------------------------------

	ja ir acces un submit|apply

------------------------------ */

if ($access) {

	if ($submit || $apply) {
		//Fill contentobj with parameters
		$contentobj->SetAddMode();
		$contentobj->FillParams($_POST);
		$contentobj->SetOwner($userid);

		$error = $contentobj->ValidateData();
		if ($error === FALSE) {
			$contentobj->Save();
			global $gCms;
			$contentops =& $gCms->GetContentOperations();
			$contentops->SetAllHierarchyPositions();
			if ($submit) {
				audit($contentobj->Id(), $contentobj->Name(), 'Added Content');
				redirect('pages.php?message=contentadded');
			}
		}
	}
	else if ($firsttime == "0") { //Either we're a preview or a template postback
		$contentobj->FillParams($_POST);
	}
	else {
		$firsttime = "0";
	}

}

/* ------------------------------

	HEADER

------------------------------ */

$header_file = get_vcms_file("/admin/header.php");
include_once($header_file);

/* ------------------------------

	Pārbaudam access
	Ja nav tad error
	Ja ir tad aiziet aiziet!!

------------------------------ */

if (!$access) {
	echo "<div class=\"pageerrorcontainer pageoverflow\"><p class=\"pageerror\">".lang('noaccessto',array(lang('addcontent')))."</p></div>";
}

else {

	/* ------------------------------

		Get a list of content_types
		and build the dropdown to select one

	------------------------------ */

	$typesdropdown = '<select name="content_type" onchange="document.Edit_Content.submit()" class="standard">';
	$cur_content_type = '';
	$content_types = $contentops->ListContentTypes();
	foreach ($content_types as $onetype => $onetypename)
	{
	  	if( $onetype == 'errorpage' ) {
	    	continue;
	    }
		// wx/vcms edited 
		// added new var to check for only to types that are allowed for other users
		$wx_allowed_type = false;
		if($userid == 1) { // main admin is 1
			$wx_allowed_type = true;
		}
		else {
			if( $onetype == "content" || $onetype == "pagelink" ) {
			  	$wx_allowed_type = true;
			}
		}
		if($wx_allowed_type) { // if allowed
			$typesdropdown .= '<option value="' . $onetype . '"';
			if ($onetype == $content_type) {
				$typesdropdown .= ' selected="selected" ';
				$cur_content_type = $onetype;
			}
			$typesdropdown .= ">".$onetypename."</option>";
		} // end allowed
	}
	$typesdropdown .= "</select>";
	// end type dropdown

	/* ------------------------------

		Tabu setups

	------------------------------ */
	
	// get tabs
	$tabnames = $contentobj->TabNames();
	
	$numberoftabs = count($tabnames);
	$tab_contents_array = array();
	for ($currenttab = 0; $currenttab < $numberoftabs; $currenttab++) {
	    $contentarray = $contentobj->EditAsArray(false, $currenttab, $access);
	    $tab_contents_array[$currenttab] = $contentarray;
	    if($tmp = $contentobj->GetError()) {
			echo $themeObject->ShowErrors($tmp);
			break;
		}
	}

?>

<div>
	
	<form method="post" name="Edit_Content" enctype="multipart/form-data" id="Edit_Content"##FORMSUBMITSTUFFGOESHERE##>

		<input type="hidden" id="serialized_content" name="serialized_content" value="<?php echo SerializeObject($contentobj); ?>" />
		<input type="hidden" name="firsttime" value="0" />
		<input type="hidden" name="orig_content_type" value="<?php echo $cur_content_type; ?>" />

		<div class="module-wrap  module-wrap--edit-page">

			<?php
			
			if(FALSE == empty($error)) {
				echo $themeObject->ShowErrors($error);
			}

			// submit buttons
			$submit_buttons = '<input type="submit" name="submitbutton" value="'.lang('submit').'" class="button  color-ok-bg  margin-r" />';
			$submit_buttons .= '<input id="m1_cancel" type="submit" name="cancel" value="'.lang('cancel').'" class="button  color-cancel-bg" />';

			echo '<div class="margin-b">';
			echo $submit_buttons;
			echo '</div>';

			?>

			<!-- Tabs -->
			<div class="module-tabs">
				<?php
				$count = 0;
				#We have preview, but no tabs
				if (count($tabnames) == 0) {
					echo '<span onclick="navigateTab(this);" data-tab="editab0" class="module-tabs__item  tab-nav  tab-nav-active">'.lang('content').'</span>';
				}
				else {
					foreach ($tabnames as $onetab) {
						echo '<span onclick="navigateTab(this);" data-tab="editab'.$count.'" class="module-tabs__item  tab-nav'. ( $count == 0 ? '  tab-nav-active' : '' ) . '">'.$onetab.'</span>';
						$count++;
					}
				}
				?>
			</div>

			<?php

			/* ------------------------------

				TABS
				WX uzdarbojās

			------------------------------ */

			// set some shit up
			$showtabs = 1;
			if($numberoftabs == 0) {
				$numberoftabs = 1;
				$showtabs = 1;
			}

			// run through the tabs
			for ($currenttab = 0; $currenttab < $numberoftabs; $currenttab++) {

				// ieliekam attiecīgā taba saturu
				if($currenttab == 0) require_once("_content_tab_main_vcms.php");
				if($currenttab == 1) require_once("_content_tab_options_vcms.php");

			}
			
			echo '<div class="margin-v">';
			echo $submit_buttons;
			echo '</div>';
			
			?>

		</div> <!-- //page content -->
		<?php require_once("_content_tab_galleries.php"); ?>
	</form>
</div>

<?php
} // -- end else -- ja ir access pieejams

// footer
include_once("footer.php");

?>
