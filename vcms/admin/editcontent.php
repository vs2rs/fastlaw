<?php
#CMS - CMS Made Simple
#(c)2004 by Ted Kulp (wishy@users.sf.net)
#This project's homepage is: http://cmsmadesimple.sf.net
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#$Id: editcontent.php 6904 2011-02-20 19:42:44Z calguy1000 $

global $CMS_ADMIN_PAGE;
$CMS_ADMIN_PAGE=1;

require_once(dirname(dirname(__FILE__)) . "/include.php");

vcmsAuth::is_loged_in();
$userid = get_userid();

include_once(dirname(dirname(__FILE__)) . "/lib/classes/class.admintheme.inc.php");

$dateformat = get_preference(get_userid(),'date_format_string','%x %X');

require_once(dirname(__FILE__).'/editcontent_extra.php');

/* ------------------------------

	If cancel redirect

------------------------------ */

if (isset($_POST["cancel"])) {
	redirect("pages.php");
}

/* ------------------------------

	Set some values

------------------------------ */

$error = FALSE;

$content_id = "";
if (isset($_REQUEST["content_id"])) $content_id = $_REQUEST["content_id"];

$pagelist_id = "1";
if (isset($_REQUEST["page"])) $pagelist_id = $_REQUEST["page"];

$submit = false;
if (isset($_POST["submitbutton"])) $submit = true;

$apply = false;
if (isset($_POST["apply"])) $apply = true;

$ajax = false;
if (isset($_POST['ajax']) && $_POST['ajax']) $ajax = true;

if ($apply) {
	$CMS_EXCLUDE_FROM_RECENT=1;
}

#Get a list of content types and pick a default if necessary
$gCms = cmsms();
$contentops =& $gCms->GetContentOperations();
$existingtypes = $contentops->ListContentTypes();

/* ------------------------------

	Set access

------------------------------ */

#Get current userid and make sure they have permission to add something
$userid = get_userid();
$access = check_ownership($userid, $content_id) || check_permission($userid, 'pages_edit');
$adminaccess = $access;
if(!$access) {
	$access = check_authorship($userid, $content_id);
}

/* ------------------------------

	If access

------------------------------ */

if($access) {
  
	// get the content object.
	$contentobj = "";
	$content_type = 'content'; // default content type.

	if(!is_object($contentobj) && $content_id != -1) {
		// load the content object from the database.
		$contentobj = $contentops->LoadContentFromId($content_id);
		$content_type = $contentobj->Type();
    }

	if(isset($_POST['content_type'])) {
		$content_type = $_POST['content_type'];
    }

	// validate the content type we want...
	if(isset($existingtypes) && count($existingtypes) > 0 && in_array($content_type,array_keys($existingtypes))) {
		// woot, it's a valid content type
    } else {
		$error = '<p>'.lang('error_contenttype').'</p>';
    }

	if($content_id != -1 && strtolower(get_class($contentobj)) != strtolower($content_type)) {
		// content type change...
		// this also updates the content object with the POST params.
		copycontentobj($contentobj, $content_type);
	}
	elseif(strtoupper($_SERVER['REQUEST_METHOD']) == 'POST') {
		// we posted... so update the content object.
		updatecontentobj($contentobj);
	}

    /* ------------------------------

	SUBMIT / APPLY

	------------------------------ */

	if ($submit || $apply) {
		
		//Fill contentobj with parameters
		$contentobj->FillParams($_POST,true);
		$error = $contentobj->ValidateData();

		if($error === FALSE) {

			$contentobj->SetLastModifiedBy(get_userid());
			$contentobj->Save();
			$contentops =& $gCms->GetContentOperations();
			$contentops->SetAllHierarchyPositions();
			audit($contentobj->Id(), $contentobj->Name(), 'Edited Content');

			if($apply) {
				$success_msg = lang('contentupdated');
			}

			if($submit) {
				redirect("pages.php?page=".$pagelist_id.'&message=contentupdated');
			}

		}

	} // -- end submit || apply

}

/* ------------------------------

	HEADER

------------------------------ */

// set subtitle
if (strlen($contentobj->Name()) > 0) { $CMS_ADMIN_SUBTITLE = $contentobj->Name(); }

// header
$header_file = get_vcms_file("/admin/header.php");
include_once($header_file);

/* ------------------------------

	Pārbaudam access
	Ja nav tad error
	Ja ir tad aiziet aiziet!!

------------------------------ */

if (!$access) {
	echo "<div class=\"pageerrorcontainer\"><p class=\"pageerror\">".lang('noaccessto',array(lang('editpage')))."</p></div>";
}

else {

	/* ------------------------------

		Get a list of content_types
		and build the dropdown to select one

	------------------------------ */

	$typesdropdown = '<select name="content_type" onchange="document.Edit_Content.submit()" class="standard">';
	$cur_content_type = '';
	foreach ($existingtypes as $onetype=>$onetypename) {
		if($onetype == 'errorpage') {
			continue;
	    }
		// wx/vcms edited 
		// added new var to check for only to types that are allowed for other users
		// $wx_allowed_type = false;
		// if($userid == 1) { // main admin is 1
		// 	$wx_allowed_type = true;
		// }
		// else { // parastajiem atļauti tikai saturs un pagelink
		// 	if($onetype == "content" || $onetype == "pagelink") {
		// 	  	$wx_allowed_type = true;
		// 	}
		// }
		// TODO: jāuztaisa dropdown (ja ir permision mainīt type) vai hidden input ja nav
		$wx_allowed_type = true;
		if($wx_allowed_type) { // if allowed
			$typesdropdown .= '<option value="' . $onetype . '"';
			if ($onetype == $content_type) {
				$typesdropdown .= ' selected="selected" ';
				$cur_content_type = $onetype;
			}
			$typesdropdown .= ">".$onetypename."</option>";
		} // end allowed
	}
	$typesdropdown .= "</select>";
	// end type dropdown

	/* ------------------------------

		Tabu setups

	------------------------------ */

	// get tabs
	$tabnames = $contentobj->TabNames();

	// count the tabs
	$numberoftabs = count($tabnames);
	
	// wx - i guess we collect tabs in array
	$tab_contents_array = array();

	for ($currenttab = 0; $currenttab < $numberoftabs; $currenttab++) {

		// get the tab content and add it to an array
	    $contentarray = $contentobj->EditAsArray(false, $currenttab, $adminaccess);
	    $tab_contents_array[$currenttab] = $contentarray;
	    
	    // check for errors
	    if($tmp = $contentobj->GetError()) {
			echo $themeObject->ShowErrors($tmp);
			break;
		}

	}

?>

<div>

	<form method="post" name="Edit_Content" id="Edit_Content" enctype="multipart/form-data">

		<input type="hidden" id="serialized_content" name="serialized_content" value="<?php echo SerializeObject($contentobj); ?>" />
		<input type="hidden" name="content_id" value="<?php echo $content_id?>" />
		<input type="hidden" name="page" value="<?php echo $pagelist_id; ?>" />
		<input type="hidden" name="orig_content_type" value="<?php echo $cur_content_type ?>" />

		<div class="module-wrap  module-wrap--edit-page">

			<?php

				// Pārbauda errorus
				if(FALSE == empty($error)) {
					echo $themeObject->ErrorMessage($error);
				}

				// check vai apply ir bijis un ir success msg
				if(isset($success_msg)) {
					echo $themeObject->SuccessMessage($success_msg);
				}

				// submit buttons
				$submit_buttons = '<input type="submit" name="submitbutton" value="'.lang('submit').'" class="button  color-ok-bg" title="'.lang('submitdescription').'" />';
				$submit_buttons .= ' <input type="submit" name="apply" value="'.lang('apply').'" class="button  color-ok-bg" title="'.lang('applydescription').'" />';
				$submit_buttons .= ' <input id="m1_cancel" type="submit" name="cancel" value="'.lang('cancel').'" class="button  color-cancel-bg" title="'.lang('canceldescription').'" />';
				$submit_buttons .= '';

				echo '<div class="margin-b">';
				echo $submit_buttons;
				echo '</div>';

			?>

			<!-- <div id="page_tabs" class="white-tabs"> -->
			<!-- Tabs -->
			<div class="module-tabs">
				<?php
				$count = 0;
				#We have preview, but no tabs
				if (count($tabnames) == 0) {
					echo '<span onclick="navigateTab(this);" data-tab="editab0" class="module-tabs__item  tab-nav  tab-nav-active">'.lang('content').'</span>';
				}
				else {
					
					// wx array to output
					$output_tabs = array(
						'Main' => '',
						'Meta' => '',
						'Options' => ''
					);

					// setup
					foreach ($tabnames as $onetab) {

						$output_tabs[$onetab] = '<span onclick="navigateTab(this);" data-tab="editab'.$count.'" class="module-tabs__item  tab-nav'. ( $count == 0 ? '  tab-nav-active' : '' ) . '">'.$onetab.'</span>';

						$count++;

					}

					foreach ($output_tabs as $output_tab) {
						if($output_tab != '') {
							echo $output_tab;
						}
					}

				}
				?>
			</div>

			<?php

			/* ------------------------------

				TABS
				WX uzdarbojās

			------------------------------ */

			// set some shit up
			$showtabs = 1;
			if($numberoftabs == 0) {
				$numberoftabs = 1;
				$showtabs = 1;
			}

			// run through the tabs
			for ($currenttab = 0; $currenttab < $numberoftabs; $currenttab++) {

				// ieliekam attiecīgā taba saturu
				if($currenttab == 0) require_once("_content_tab_main_vcms.php");
				if($currenttab == 1) require_once("_content_tab_options_vcms.php");

			}

			// end run through tabs
			echo '<div class="margin-b">';
			echo $submit_buttons;
			echo '</div>';

			?>
		
		</div> <!-- //module-wrap -->
	</form>
	<?php require_once("_content_tab_galleries.php"); ?>
</div>

<?php
} // -- end else -- ja ir access pieejams

// footer
include_once("footer.php");

?>
