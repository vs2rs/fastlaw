<?php

// ------------------------------------
// -- create a csv file
// ------------------------------------

global $CMS_ADMIN_PAGE;
$CMS_ADMIN_PAGE = 1;

// includes
require_once(dirname(dirname(__FILE__)) . "/include.php");

// login
vcmsAuth::is_loged_in();

// ------------------------------------
// -- check for post
// ------------------------------------

// is module set?
if(isset($_POST)
&& isset($_POST['export_module'])
&& isset($_POST['export_order'])
&& isset($_POST['export_colls'])
&& isset($_POST['csv_header'])
&& isset($_POST['csv_file'])) {

	// get db and config
	global $gCms;
	$config = $gCms->GetConfig();
	$db = $gCms->GetDb();

	// ------------------------------------
	// -- validate colls and order
	// ------------------------------------

	// set
	$export_colls = $_POST['export_colls'];
	$export_order = $_POST['export_order'];

	// check for unsuported chars
	if(!preg_match('/^[a-zA-Z0-9\,\s\_\-]+$/', $export_colls)) {
		die("Export cols (" . $export_colls . ") are not valid!");
	}

	// check for unsuported chars
	if(!preg_match('/^[a-zA-Z0-9\,\s\_\-]+$/', $export_order)) {
		die("Export order (" . $export_order . ") is not valid!");
	}

	// ------------------------------------
	// -- validate filename
	// ------------------------------------

	// set
	$csv_file = $_POST['csv_file'];

	// get extension and check it
	$extension = strtolower(pathinfo($csv_file, PATHINFO_EXTENSION));
	if($extension != "csv") { $extension = "csv"; }

	// update file name
	$csv_file = pathinfo($csv_file, PATHINFO_FILENAME);
	$csv_file = munge_string_to_url($csv_file) . "." . $extension;

	// ------------------------------------
	// -- validate header
	// ------------------------------------

	$csv_header = $_POST['csv_header'];
	
	// remove ; if set at the end
	if(preg_match('/;$/', $csv_header)) {
		$csv_header = substr($csv_header, 0, -1);
	}

	// ------------------------------------
	// -- validate our db table
	// ------------------------------------

	$export_module = cms_db_prefix() . $_POST['export_module'];
	$db_name = $config['db_name'];

	// run the query to check table
	$check_table = $db->GetOne("
		SELECT table_name FROM information_schema.tables
		WHERE table_schema = '$db_name' AND table_name = '$export_module' LIMIT 1"
	);

	// if no table report an error
	if($check_table === false) {
		die("Table " . $export_module . " dose not exists in the database!");
	}

	// ------------------------------------
	// -- get data
	// ------------------------------------

	// set csv output header
	$csv_file_content = "$csv_header\r\n";

	// get the data
	$db_query = "SELECT $export_colls FROM $export_module ORDER BY $export_order";

	// run it
	if($dbresult = $db->Execute($db_query)) {

		while ($dbresult && $row = $dbresult->FetchRow()) {

			foreach ($row as $key => $value) {

				// remove any unnecesary ; from the end of the value
				// it can break our csv file structure
				if(preg_match('/;$/', $value)) {
					$value = substr($value, 0, -1);
				}

				// last key remove ; and add line break
				if(array_key_last($row) == $key) $csv_file_content .= "$value\r\n";
				else $csv_file_content .= "$value;";

			}

		}

	}

	// output csv
	header('Pragma: public');
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	header('Cache-Control: private', false);
	header('Content-Type: text/csv');
	header('Content-Disposition: attachment; filename="' . $csv_file . '"');
	echo $csv_file_content;

} else {

	// post error
	die("Missing some fields in the $_POST. Check your form!");

}





// -- csv.php