<?php

$orig_memory = (function_exists('memory_get_usage')?memory_get_usage():0);
$starttime = microtime();
if (!(isset($USE_OUTPUT_BUFFERING) && $USE_OUTPUT_BUFFERING == false))
{
	@ob_start();
}
include_once($config['root_path'] . "/lib/classes/class.admintheme.inc.php");

$config = $gCms->GetConfig();

if (isset($USE_THEME) && $USE_THEME == false)
{
  //echo '<!-- admin theme disabled -->';
}
else
{
	// get the theme object
	
	$themeName=get_preference(get_userid(), 'admintheme', 'default');
	$themeObjectName = $themeName."Theme";
	$userid = get_userid();

	debug_buffer('before theme load');

	// wx check theme file

	$default_theme_file = dirname(__FILE__) . "/themes/${themeName}/${themeObjectName}.php";

	$custom_theme_file = PROJECT_PATH . "/admin/themes/${themeName}/${themeObjectName}.php";

	if(file_exists($custom_theme_file)) {
	    $theme_file = $custom_theme_file;
	} else if(file_exists($default_theme_file)) {
	    $theme_file = $default_theme_file;
	} else {
	    $theme_file = false;
	}

	if($theme_file) {
		include($theme_file);
		$themeObject = new $themeObjectName($gCms, $userid, $themeName);
	} else {
		$themeObject = new AdminTheme($gCms, $userid, $themeName);
	}

	debug_buffer('after theme load');

	$gCms->variables['admintheme'] = $themeObject;
	
	debug_buffer('before populate admin navigation');
	
	$themeObject->PopulateAdminNavigation(isset($CMS_ADMIN_SUBTITLE)?$CMS_ADMIN_SUBTITLE:'');
	
	debug_buffer('after populate admin navigation');
	
	debug_buffer('before theme-y stuff');

	$themeObject->DisplayDocType();
	$themeObject->DisplayHTMLStartTag();
	$themeObject->DisplayHTMLHeader(false, isset($headtext)?$headtext:'');
	$themeObject->DisplayBodyTag();
	$themeObject->DoTopMenu();
	$themeObject->DisplayMainDivStart();
	
	debug_buffer('after theme-y stuff');

	// and display the dashboard.
	$themeObject->DisplayNotifications(3); // todo, a preference.

}
?>
