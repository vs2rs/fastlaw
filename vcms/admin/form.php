<?php

// ------------------------------------
// -- admin header
// ------------------------------------

$header = get_vcms_file("/admin/header.php");

// ------------------------------------
// -- if cancel do a redirect
// ------------------------------------

// if cancel then redirect
if(isset($_POST["cancel"])) {
	redirect($redirect); exit;
}

// ------------------------------------
// -- form for editing and adding
// ------------------------------------

// errors
$errors = array();

// access to form
$form_access = false;

// we are editng
if($id && isset($form) && $form->access_edit) {

	if($item = $form->operations->LoadByID($id)) {
		
		// update vars
		$form_access = true;
		$form_action = $form_action . '?id=' . $item->id;

	} else {

		// no itme
		$item_not_loaded = true;

	}

} else {

	// if user has access to add or id is his id
	if(isset($form) && $form->access_add) {
		$form_access = true;
	}

}

if($form_access) {

	// header
	include_once($header);

	// ------------------------------------
	// -- update values of our fileds
	// ------------------------------------

	if(isset($_POST['apply']) || isset($_POST['submit'])) {
		
		// set messages
		$audit = $ClassName . ' Modified';
		$message = strtolower($ClassName . '_modified');

		// check for item
		if(!isset($item)) {
			$item = new $ClassName();
			$audit = $ClassName . ' Added';
			$message = strtolower($ClassName . '_added');
		}

		foreach ($_POST as $post_key => $post_value) {

			// first check if key is in our input list
			if(isset($form->inputs[$post_key])) {

				// if this is a password field we do a different checkup
				if($post_key == 'password') {

					// check if field is empty or a new user
					if($_POST['password'] != '' || $item->id == -1) {

						// adding new user password can't be empty
						if($item->id == -1 && $_POST['password'] == '') {
							$errors['password'] = lang('error_empty', lang('password'));
						}

						// changing password so check if both mach
						if(!isset($errors['password'])
						&& $_POST['password'] != $_POST['passwordagain']) {
							$errors['password'] = lang('password_no_match');
						}

						// if no error update password
						if(!isset($errors['password'])
						&& method_exists($item, 'SetPassword')) {
							$item->SetPassword($_POST['password']);
						}

					}

				} else {

					// validation
					if(isset($form->inputs[$post_key]["validate"])) {
						
						$validate = $form->inputs[$post_key]["validate"];

						// get the values
						$validate_empty = isset($validate["not_empty"]) ? $validate["not_empty"] : false;
						$validate_value = isset($validate["value"]) ? true : false;

						// check if we have the value
						if($validate_empty && $post_value == "") {
							$errors[$post_key] = lang('error_empty', $form->inputs[$post_key]['label']);
						}

						// check if value valid
						if($validate_value && $post_value != "") {
							// if float replace , with .
							if($validate["value"] == "float") {
								$post_value = str_replace(",", ".", $post_value);
							}
							// validate
							if(!validate_string($post_value, $validate["value"])) {
								$errors[$post_key] = lang('error_not_valid_alias', $form->inputs[$post_key]['label']);
							}
						}

					}

					// update value
					$item->$post_key = $post_value;

				}

			}

		}

		// we need to update checkboxes that are not part of group
		// we need to check if checkbox is in the post if not we need to set value to 0
		foreach ($form->inputs as $form_input_name => $value) {
			if($value['type'] == 'checkbox' && !preg_match('/\[\]$/', $form_input_name)) {
				if(!isset($_POST[$form_input_name])) {
					$item->$form_input_name = 0;
				}
			}
		}

		// if no errors update item
		if(count($errors) == 0) {

			// get save result
			$save_status = $item->Save();

			// if save is true and nothing but the true
			// if not true then it's an error message
			if($save_status === true) {

				// check if we have id set
				if(!isset($id)) {
					$id = $item->id;
					$form_action = $form_action . '?id=' . $item->id;
				}

				// admin log
				audit($item->id, $item->Name(), $audit);

				// redirect if submit
				if(isset($_POST["submit"])) {

					// do the redirect
					redirect($redirect . "?message=" . $message . '&id=' . $item->id);

				} else {

					// success message
					$message = lang($message, $item->Name());

					// update the form
					$form = form_setup($item->id);

				}

			} else {

				// something went wrong while saving user

				// error message
				$errors['update'] = $save_status;

				// update the form
				$form = form_setup($item->id);

			}

		}

	}

	if(isset($item) && $item) {

		// update our tab date with values form item
		foreach ($form->tabs as $form_tabs_key => $form_tab) {

			// set up our values
			foreach ($form_tab['content'] as $form_cols_key => $form_col) {

				// is it an array?
				if(is_array($form_col)) {

					foreach ($form_col as $input_key => $input) {

						// ignore passwords and hidden inputs
						if(in_array($input['type'], array('hidden', 'button'))) continue;

						// set the param to check for
						$item_param = $input['name'];

						// update value but not for checkboxes and passwords
						if(isset($item->$item_param)
						&& $input['type'] != 'checkbox'
						&& $input['type'] != 'password') {
							$form->tabs[$form_tabs_key]['content'][$form_cols_key][$input_key]['value'] = $item->$item_param;
						}

						// setup checked for checkboxes that are not in a group
						if($input['type'] == 'checkbox'
						&& !preg_match('/\[\]$/', $item_param)) {
							$form->tabs[$form_tabs_key]['content'][$form_cols_key][$input_key]['checked'] = $item->$item_param ? true : false;
						}

						// checkbox groups
						if(preg_match('/\[\]$/', $item_param) && $input['type'] == 'checkbox') {

							// check if 'checked' value is a method we need to call
							// but do this only if we are editing not adding new item
							if(isset($id) && method_exists($form->operations, $input['checked'])) {

								// set the method
								$methodIsChecked =  $input['checked'];

								// this method takes curent id and this input value
								if($form->operations->$methodIsChecked($id, $input['value'])) {

									// update checked value
									$form->tabs[$form_tabs_key]['content'][$form_cols_key][$input_key]['checked'] = true;

								}

							}

							// for new objects
							if(!isset($id)) {

								// set group name
								$checkbox_group = str_replace('[]', '', $item_param);

								// check if group set and it's an array
								if(isset($_POST[$checkbox_group])
								&& is_array($_POST[$checkbox_group])) {

									// update checked value
									$form->tabs[$form_tabs_key]['content'][$form_cols_key][$input_key]['checked'] = in_array($input['value'], $_POST[$checkbox_group]);

								}

							}
							
						}

						// check for password field and password button
						if($input['type'] == 'password' && isset($form->password_button)) {

							// update class for passwords to hidden
							$form->tabs[$form_tabs_key]['content'][$form_cols_key][$input_key]['class'] = '  form__row--hidden';

							// insert the button before password fields
							if($input['name'] != 'passwordagain') {
								$password_button_col = $form_cols_key;
								$password_button_postion = $input_key;
							}

						}

					} // -- end inputs

					// insert the passoword button if we have one
					if(isset($form->password_button)
					&& isset($password_button_col)
					&& isset($password_button_postion)) {
						array_splice(
							$form->tabs[$form_tabs_key]['content'][$password_button_col],
							$password_button_postion, 0,
							array($form->password_button)
						);
					}

				}

			}

		}

	}

	// ------------------------------------
	// -- setup error message
	// ------------------------------------

	if(count($errors) > 0) {
		$message = $themeObject->ShowMessage($errors, 'error');
	} else {
		$message = isset($message) ? $themeObject->ShowMessage($message) : '';
	}

	// ------------------------------------
	// -- create the form
	// ------------------------------------

	// create the content
	$form_content = $themeObject->CreateTabs($form->tabs);

	// submit
	$button_submit = $form->button_submit ? get_html(array(
		'name'		=> 'submit',
		'value'		=> lang('submit'),
		'template'	=> $config['root_path'] . '/admin/templates/button-submit.html'
	)) : '';

	// submit
	$button_apply = $form->button_apply ? get_html(array(
		'name'		=> 'apply',
		'value'		=> $form->button_submit ? lang('apply') : lang('submit'),
		'template'	=> $config['root_path'] . '/admin/templates/button-submit.html'
	)) : '';

	// cancel
	$button_cancel = $form->button_cancel ? get_html(array(
		'name'		=> 'cancel',
		'value'		=> lang('cancel'),
		'template'	=> $config['root_path'] . '/admin/templates/button-cancel.html'
	)) : '';

	// add button
	echo get_html(array(

		// where to submit
		'action'	=> $form_action,
		
		// error/success messages
		'message'	=> $message,

		// content
		'content'	=> $form_content,

		// buttons
		'submit'	=> $button_submit,
		'apply'		=> $button_apply,
		'cancel'	=> $button_cancel,
		'buttons_top' => true,

		// template
		'template'	=> VCMS_PATH . '/admin/templates/form.html'

	));

} else {

	// header
	include_once($header);

	// ------------------------------------
	// -- no access message
	// ------------------------------------

	// set the message
	$message_text = $themeObject->ShowMessage(lang('no_access'), 'error');
	if(isset($item_not_loaded)) {
		$message_text = $themeObject->ShowMessage(lang('item_not_loaded'), 'error');
	}

	// template
	echo get_html(array(
		'message'	=> $message_text,
		'template'	=> VCMS_PATH . '/admin/templates/no-access.html'
	));

}

// ------------------------------------
// -- footer
// ------------------------------------

include_once(get_vcms_file("/admin/footer.php"));





// -- form.php