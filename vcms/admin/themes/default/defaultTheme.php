<?php
#Modern Theme - A theme for CMS Made Simple
#(c)2005 by Alexander Endresen - alexander@endresen.org
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

class defaultTheme extends AdminTheme
{
  function renderMenuSection($section, $depth, $maxdepth)
  {
    if ($maxdepth > 0 && $depth > $maxdepth)
      {
	return;
      }
    if (! $this->menuItems[$section]['show_in_menu'])
      {
	return;
      }
    if (strlen($this->menuItems[$section]['url']) < 1)
      {
	echo "<li>".$this->menuItems[$section]['title']."</li>";
	return;
      }
    echo "<li><a href=\"";
    echo $this->menuItems[$section]['url'];
    echo "\"";
    if (array_key_exists('target', $this->menuItems[$section]))
      {
	echo ' rel="external"';
      }
    $class = array();
    if ($this->menuItems[$section]['selected'])
      {
	$class[] = 'selected';
      }
    if (isset($this->menuItems[$section]['firstmodule']))
      {
	$class[] = 'first_module';
      }
    else if (isset($this->menuItems[$section]['module']))
      {
	$class[] = 'module';
      }
    if (count($class) > 0)
      {
	echo ' class="';
	for($i=0;$i<count($class);$i++)
	  {
	    if ($i > 0)
	      {
		echo " ";
	      }
	    echo $class[$i];
	  }
	echo '"';
      }
    echo ">";
    echo $this->menuItems[$section]['title'];
    echo "</a>";
    if ($this->HasDisplayableChildren($section))
      {
	echo "<ul>";
	foreach ($this->menuItems[$section]['children'] as $child)
	  {
	    $this->renderMenuSection($child, $depth+1, $maxdepth);
	  }
	echo "</ul>";
      }
    echo "</li>";
    return;
  }

    function DisplayTopMenu()
    {
	echo '<div><p class="logocontainer"><img src="themes/default/images/logo.gif" alt="" /><span class="logotext">'.lang('adminpaneltitle').' - '. $this->cms->siteprefs['sitename'] .' &nbsp;&nbsp; '.lang('welcome_user').': '.$this->cms->variables['username'].'</span></p></div>';
        echo "<div class=\"topmenucontainer\">\n\t<ul id=\"nav\">";
        foreach ($this->menuItems as $key=>$menuItem) {
        	if ($menuItem['parent'] == -1) {
        	    echo "\n\t\t";
        		$this->renderMenuSection($key, 0, -1);
        	}
        }
        echo "\n\t</ul>\n";
		//ICON VIEW SITE
		echo "\n\t<div id=\"nav-icons_all\"><ul id=\"nav-icons\">\n";
		echo "\n\t<li class=\"viewsite-icon\"><a  rel=\"external\" title=\"".lang('viewsite')."\"  href=\"../\">".lang('viewsite')."</a></li>\n";
		//ICON LAGOUT
		echo "\n\t<li class=\"logout-icon\"><a  title=\"".lang('logout')."\"  href=\"logout.php\">".lang('logout')."</a></li>\n";
		echo "\n\t</ul></div>\n";
		//END ICONS
		echo "\t<div class=\"clearb\"></div>\n";
		echo "</div>\n";
    }
   	
}
?>
