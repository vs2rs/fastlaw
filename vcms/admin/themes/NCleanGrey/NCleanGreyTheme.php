<?php
#NCleanGrey Theme - A theme for CMS Made Simple
#(c)2008 by Author: Nuno Costa - nuno.mfcosta@sapo.pt  /  criacaoweb.net / http://dev.cmsmadesimple.org/users/nuno/
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

class NCleanGreyTheme extends AdminTheme
{
  function renderMenuSection($section, $depth, $maxdepth)
  {
    if ($maxdepth > 0 && $depth > $maxdepth)
    {
      return;
    }
    if (! $this->menuItems[$section]['show_in_menu'])
    {
      return;
    }
    if (strlen($this->menuItems[$section]['url']) < 1)
    {
      echo "<li>".$this->menuItems[$section]['title']."</li>";
      return;
    }
      echo "<li><a href=\"";
      echo $this->menuItems[$section]['url'];
      echo "\"";
      if (array_key_exists('target', $this->menuItems[$section]))
      {
        echo ' rel="external"';
      }
      $class = array();
      if ($this->menuItems[$section]['selected']) { $class[] = 'selected'; }
      if (isset($this->menuItems[$section]['firstmodule'])) { $class[] = 'first_module'; }
      else if (isset($this->menuItems[$section]['module'])) { $class[] = 'module'; }
      
      if (count($class) > 0)
      {
        echo ' class="';
        for($i=0;$i<count($class);$i++){
          if($i > 0) { echo " "; }
  	   	  echo $class[$i];
        }
        echo '"';
      }
      
      echo ">";
      echo $this->menuItems[$section]['title'];
      echo "</a>";
      if ($this->HasDisplayableChildren($section))
      {
        echo "<ul>";
        foreach ($this->menuItems[$section]['children'] as $child)
        {
          $this->renderMenuSection($child, $depth+1, $maxdepth);
        }
        echo "</ul>";
      }
      echo "</li>";


    return;
  }

    function DisplayTopMenu() {

		// vcms ==> get user data
		global $gCms;
		$userops =& $gCms->GetUserOperations();
		$thisuser = $userops->LoadUserByID($this->userid);

		echo '

			<div class="header  padding-h  color-one-bg">

				<div class="header__logo">
					<svg class="color-light-fill" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 13" xml:space="preserve"><path d="M17.7,7c0-3.1,2.3-5.6,5.2-5.6c0.7,0,1.4,0.2,2.1,0.5c0.5,0.2,0.8,0.7,0.8,1.2c0,0.2,0,0.4-0.1,0.6 c-0.2,0.5-0.7,0.8-1.2,0.8c-0.2,0-0.4,0-0.6-0.1c-0.3-0.1-0.6-0.2-0.9-0.2c-1.3,0-2.4,1.2-2.4,2.8c0,1.5,1.1,2.8,2.4,2.8 c0.3,0,0.6-0.1,0.9-0.2c0.2-0.1,0.4-0.1,0.6-0.1c0.5,0,1,0.3,1.3,0.8c0.1,0.2,0.1,0.4,0.1,0.6c0,0.5-0.3,1-0.8,1.3 c-0.7,0.3-1.4,0.5-2.1,0.5C20,12.5,17.7,10,17.7,7z"/><path d="M40.4,5.7v5.4c0,0.8-0.6,1.4-1.4,1.4c-0.8,0-1.4-0.6-1.4-1.4V5.7c0-0.1,0-0.6-0.2-0.9c-0.2-0.4-0.4-0.6-0.9-0.6 s-0.7,0.1-0.9,0.6c-0.2,0.4-0.2,0.8-0.2,0.9v5.4c0,0.8-0.6,1.4-1.4,1.4c-0.8,0-1.4-0.6-1.4-1.4V5.7c0-0.1,0-0.6-0.2-0.9 c-0.2-0.4-0.5-0.6-0.9-0.6s-0.7,0.2-0.9,0.6c-0.2,0.4-0.2,0.8-0.2,0.9v5.4c0,0.8-0.6,1.4-1.4,1.4c-0.8,0-1.4-0.6-1.4-1.4V2.8 c0-0.8,0.6-1.4,1.4-1.4c0.4,0,0.7,0.1,0.9,0.3c0.5-0.2,1-0.3,1.6-0.3c1.1,0,1.9,0.4,2.5,0.9c0.6-0.5,1.4-0.9,2.5-0.9 C39.3,1.4,40.4,4,40.4,5.7z"/><path d="M50,8.5v0.2c0,0.9-0.3,1.7-0.9,2.4c-0.8,0.9-2,1.3-3.3,1.3c-1.5,0-2.9-0.7-3.5-1.9c-0.1-0.2-0.2-0.5-0.2-0.7 c0-0.5,0.2-0.9,0.7-1.1c0.2-0.1,0.4-0.2,0.7-0.2c0.5,0,0.9,0.3,1.2,0.7c0.2,0.4,0.8,0.6,1.2,0.6c0.6,0,1.1-0.2,1.3-0.5 c0.2-0.2,0.3-0.4,0.3-0.6c-0.1-0.3-1.1-0.7-1.9-0.9c0,0-0.1,0-0.1,0c-1.3-0.4-3-1.3-3-3.3c0-1.8,1.6-3.1,3.8-3.1 c0.7,0,1.5,0.2,2.1,0.6C48.8,2.3,49,2.7,49,3.2c0,0.3-0.1,0.5-0.2,0.7c-0.2,0.4-0.7,0.6-1.1,0.6c-0.3,0-0.5-0.1-0.7-0.2 c-0.2-0.1-0.5-0.2-0.7-0.2c-0.8,0-1.2,0.3-1.2,0.4c0,0.2,0,0.5,1,0.8c0.2,0,0.9,0.2,1.6,0.6C49.5,6.7,49.9,7.8,50,8.5z"/><path d="M6.1,13c-0.3,0-0.6-0.1-0.8-0.3L0.3,7.7c-0.5-0.5-0.5-1.2,0-1.7c0.5-0.5,1.2-0.5,1.7,0l4.1,4.1L16,0.3 c0.5-0.5,1.2-0.5,1.7,0c0.5,0.5,0.5,1.2,0,1.7L7,12.7C6.7,12.9,6.4,13,6.1,13"/></svg>
				</div>

				<div class="header__menu">

					<a class="header__text  color-one-light-text  color-light-text-hover  margin-r"
					href="'.$this->menuItems['myprefs']['url'].'">
						' . ( $thisuser->firstname != "" ? $thisuser->firstname . ' ' . $thisuser->lastname : $thisuser->username ) . '
					</a>

					<a class="header__text  color-one-light-text  color-light-text-hover  margin-r"
					href="'.$this->cms->config['root_url_2'].'" target="_blank">
						'.$this->cms->config['production_www'].'
					</a>

					<!--

						# View site

					-->

					<a class="header__icon  header__icon--view  color-one-dark-bg  color-dark-bg-hover  margin-r"
					href="'.$this->cms->config['root_url_2'].'" target="_blank">
						<svg class="color-light-fill" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 511.999 511.999" xml:space="preserve"> <g> <g> <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035 c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201 C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418 c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418 C447.361,287.923,358.746,385.406,255.997,385.406z"/> </g> </g> <g> <g> <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275 s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516 s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"/> </g> </g> </svg>
					</a>

					<!--

						# Log out

					-->

					<a class="header__icon  header__icon--power  color-one-dark-bg  color-dark-bg-hover"
					href="logout.php">
						<svg class="color-light-fill" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 489.888 489.888" xml:space="preserve"> <path d="M25.383,290.5c-7.2-77.5,25.9-147.7,80.8-192.3c21.4-17.4,53.4-2.5,53.4,25l0,0c0,10.1-4.8,19.4-12.6,25.7 c-38.9,31.7-62.3,81.7-56.6,136.9c7.4,71.9,65,130.1,136.8,138.1c93.7,10.5,173.3-62.9,173.3-154.5c0-48.6-22.5-92.1-57.6-120.6 c-7.8-6.3-12.5-15.6-12.5-25.6l0,0c0-27.2,31.5-42.6,52.7-25.6c50.2,40.5,82.4,102.4,82.4,171.8c0,126.9-107.8,229.2-236.7,219.9 C122.183,481.8,35.283,396.9,25.383,290.5z M244.883,0c-18,0-32.5,14.6-32.5,32.5v149.7c0,18,14.6,32.5,32.5,32.5 s32.5-14.6,32.5-32.5V32.5C277.383,14.6,262.883,0,244.883,0z"/> </svg>
					</a>

				</div>

			</div>

		';

	echo '<div id="ncleangrey-container">';
	

	//MENU
        echo "<div class=\"topmenucontainer\">\n\t<ul id=\"nav\">";
        foreach ($this->menuItems as $key=>$menuItem) {
        	if ($menuItem['parent'] == -1) {
        	  echo "\n\t\t";
        		$this->renderMenuSection($key, 0, -1);
        	}
        }
		echo "\n\t</ul>\n";
		echo "\t<div class=\"clearb\"></div>\n";
		echo "</div>\n";

    } // end DisplayTopMenu
   
}
?>
