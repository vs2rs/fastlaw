<?php

#NCleanGrey Theme - Theme for CMS Made Simple admin side.
#(c)2008/10 by Author: Nuno Costa - nuno.mfcosta@sapo.pt  /  criacaoweb.net / http://dev.cmsmadesimple.org/users/nuno/
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#$Id: login.php 5517 2009-04-02 16:42:15Z nuno $
global $gCms;
$config = $gCms->GetConfig();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title><?php echo $gCms->siteprefs['sitename']; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo get_encoding() ?>" />
<meta name="robots" content="noindex, nofollow" />
<link rel="shortcut icon" href="favicon_cms.ico" />
<link rel="Bookmark" href="favicon_cms.ico" />
<link rel="stylesheet" type="text/css" media="all" href="themes/NCleanGrey/css/login.css" />
<base href="<?php echo $config['root_url'] . '/' . $config['admin_dir'] . '/'; ?>" />
</head>
<body>			

	  <div class="site__container">

	  	  <p class="text--center  login-text">
	  	  	<?php echo $gCms->siteprefs['sitename']; ?>
	  	  </p>

	  	  <p>
	  	  	<?php 
				debug_buffer('Debug in the page is: ' . $error);
				if (isset($error) && $error != '')
				{
					echo '<div class="erroLogin  text--center">'.$error.'</div>';
				}
				else if (isset($warningLogin) && $warningLogin != '')
				{
					echo '<div class="warningLogin  text--center">'.$warningLogin.'</div>';
				}
				else if (isset($acceptLogin) && $acceptLogin != '')
				{
					echo '<div class="acceptLogin  text--center">'.$acceptLogin.'</div>';
				}
			?>	
	  	  </p>

	      <form method="post" class="form form--login">
	      	
	        <div class="form__field">
	          <label class="fontawesome-user" for="login__username"><span class="hidden">Username</span></label><!--
	       --><input id="lbusername" name="username" type="text" class="form__input" placeholder="Username" required />
	        </div>

	        <div class="form__field">
	          <label class="fontawesome-lock" for="login__password"><span class="hidden">Password</span></label><!--
	       --><input id="lbpassword" name="password" type="password" class="form__input" placeholder="Password" required />
	        </div>

	        <div class="form__field">
	          <input type="submit" value="Sign In" name="loginsubmit" class="loginsubmit" />
	        </div>

	      </form>

	      <p class="text--center  text--small">
	      	<strong><?php echo lang('login_info'); ?></strong>
	      	<ol class="text--small"> 
			  <li>Cookies must be enabled in your browser</li> 
			  <li>Javascript must be enabled in your browser</li> 
			  <li>Popup windows must be allowed for the following address: <?php echo $config['root_url_2']; ?></li> 
			</ol>
	      </p>

	  </div>

</body>
</html>
