<?php


// ------------------------------------
// -- users add and edit
// ------------------------------------

global $CMS_ADMIN_PAGE;
$CMS_ADMIN_PAGE=1;

// includes
require_once(dirname(dirname(__FILE__)) . "/include.php");

// login
vcmsAuth::is_loged_in();

// form action
$redirect = 'users_prefs.php';
$form_action = 'users_prefs.php';

// check access
$access = check_permission(get_userid(), 'users_edit_prefs');

// ------------------------------------
// -- if cancel do a redirect
// ------------------------------------

// if cancel then redirect
if(isset($_POST["cancel"])) {
	redirect($redirect); exit;
}

// ------------------------------------
// -- output
// ------------------------------------

$header = get_vcms_file("/admin/header.php");

if($access) {

	// header
	include_once($header);

	// ------------------------------------
	// -- POST
	// ------------------------------------

	if((isset($_POST["submit"]) || isset($_POST["apply"]))
	&& isset($_POST["default_cms_language"])
	&& isset($_POST["admintheme"])) {

		// get from post
		$default_cms_lang = $_POST["default_cms_language"];
		$admintheme = $_POST["admintheme"];

		// check languages and set default if something else sent
		if($default_cms_lang != 'lv_LV'
		&& $default_cms_lang != 'en_US') {
			$default_cms_lang = 'lv_LV';
		}

		// check theme and set default if something else sent
		if($admintheme != 'NCleanGrey') {
			$admintheme = 'NCleanGrey';
		}

		// do the updated
		set_preference(get_userid(), 'default_cms_language', $default_cms_lang);
		set_preference(get_userid(), 'admintheme', $admintheme);
		
		// admin log
		audit(get_userid(), 'User Preferences', 'Edited User Preferences');

		// redirect
		redirect($redirect . "&message=user_prefs_updated"); exit;

	}

	// ------------------------------------
	// -- check message in the url
	// ------------------------------------

	if(isset($_GET["message"])) {
		$message = $themeObject->ShowMessage(lang($_GET['message']));
	}

	// ------------------------------------
	// -- define tabs
	// ------------------------------------

	// create the tabs
	$form_content = $themeObject->CreateTabs(array(
		
		0 => array(

			'name' 		=> lang('user_prefs'),

			'content'	=> array(

				'col-1' => array(

					array(
						'type'	=> 'select',
						'label'	=> lang('language'),
						'name' 	=> 'default_cms_language',
						'options' => array(
							array('value' => 'lv_LV', 'name' => lang('latvian')),
							array('value' => 'en_US', 'name' => lang('english'))
						),
						'selected' => get_preference(get_userid(), 'default_cms_language')
					),

					array(
						'type'	=> 'select',
						'label'	=> lang('admintheme'),
						'name' 	=> 'admintheme',
						'options' => array(
							array('value' => 'NCleanGrey', 'name' => 'NCleanGrey')
						),
						'selected' => get_preference(get_userid(), 'admintheme'),
						'class'	=> '  form__row--hidden'
					)

				),

				'col-2' => ''

			)

		)

	));

	// ------------------------------------
	// -- create the form
	// ------------------------------------

	// submit
	$button_submit = get_html(array(
		'name'		=> 'submit',
		'value'		=> lang('submit'),
		'template'	=> $config['root_path'] . '/admin/templates/button-submit.html'
	));

	// cancel
	$button_cancel = get_html(array(
		'name'		=> 'cancel',
		'value'		=> lang('cancel'),
		'template'	=> $config['root_path'] . '/admin/templates/button-cancel.html'
	));

	// add button
	echo get_html(array(

		// where to submit
		'action'	=> $form_action,
		
		// error/success messages
		'message'	=> isset($message) ? $message : '',

		// content
		'content'	=> $form_content,
		'submit'	=> $button_submit,
		'apply'		=> '',
		'cancel'	=> $button_cancel,
		'buttons_top' => true,

		// template
		'template'	=> VCMS_PATH . '/admin/templates/form.html'

	));

} else {
	
	// ------------------------------------
	// -- no access message
	// ------------------------------------

	echo get_html(array(
		
		// error/success messages
		'message'	=> $themeObject->ShowMessage(lang('no_access'), 'error'),

		// template
		'template'	=> $config['root_path'] . '/admin/templates/no-access.html'

	));

}

// ------------------------------------
// -- footer
// ------------------------------------

include_once("footer.php");





// -- users_prefs.php