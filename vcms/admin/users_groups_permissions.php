<?php

// ------------------------------------
// -- users add and edit
// ------------------------------------

global $CMS_ADMIN_PAGE;
$CMS_ADMIN_PAGE=1;

// includes
require_once(dirname(dirname(__FILE__)) . "/include.php");
require_once(dirname(dirname(__FILE__)) . "/lib/classes/class.group.inc.php");

// login
vcmsAuth::is_loged_in();

// ------------------------------------
// -- setup
// ------------------------------------

// get main operations
global $gCms;
$groupops = $gCms->GetGroupOperations();
$userops = $gCms->GetUserOperations();
$db = $gCms->GetDb();

// redirect page after submit
$redirect = 'users_groups_permissions.php';

// form action
$form_action = 'users_groups_permissions.php';

// check access
$access = check_permission(get_userid(), 'users_groups_add_remove');

// ------------------------------------
// -- permision list
// ------------------------------------

// load groups
$group_list = $groupops->LoadGroups();

// tables
$permision_table = cms_db_prefix() . "permissions";
$group_perm_table = cms_db_prefix() . "group_perms";

// query
$query = "SELECT p.permission_id, p.permission_name, p.permission_text, up.group_id FROM $permision_table p
LEFT JOIN $group_perm_table up ON p.permission_id = up.permission_id ORDER BY p.permission_name";
$result = $db->Execute($query);

// results
$perm_struct = array();
while($result && $row = $result->FetchRow()) {
    
    if(isset($perm_struct[$row['permission_id']])) {
		
		$str = &$perm_struct[$row['permission_id']];
		$str->group[$row['group_id']] = 1;

	} else {

		$thisPerm = new stdClass();
		$thisPerm->group = array();

		if(!empty($row['group_id'])) {
			$thisPerm->group[$row['group_id']] = 1;
		}

		$thisPerm->id = $row['permission_id'];
		$thisPerm->name = $row['permission_name'];
		$thisPerm->text = lang($row['permission_name']);
		$perm_struct[$row['permission_id']] = $thisPerm;

		// check if it's module
		if(preg_match('/^module_/', $row['permission_name'], $results)) {

			// remove module_ text
			$module_permission_name = str_replace($results[0], '', $row['permission_name']);

			// check for manage
			if(preg_match('/_manage_/', $module_permission_name)) {
				$module_name = substr(
					$module_permission_name, 0,
					strpos($module_permission_name, "_manage")
				);
			}

			// check for advanced
			if(preg_match('/_advanced$/', $module_permission_name)) {
				$module_name = substr(
					$module_permission_name, 0,
					strpos($module_permission_name, "_advanced")
				);
			}

			// get module instance
			$module = cmsms()->GetModuleInstance($module_name);

			if(!$module) continue;

			// get module permision form lang file
			$thisPerm->text = $module->lang(
				str_replace($module_name, 'permissions', $module_permission_name),
				array(
					lang('module'),
					$module->lang('friendlyname')
				)
			);

		}

	}

}

// ------------------------------------
// -- update permision list based on user
// ------------------------------------

foreach ($perm_struct as $perm_id => $perm) {

	// show only permisions that user has
	$show_this_perm = false;
	foreach ($perm->group as $group_id => $value) {
		if($userops->UserInGroup(get_userid(), $group_id)) {
			$show_this_perm = true;
			break;
		}
	}

	// remove it from permision list but not for admin
	if(!$show_this_perm && !$userops->UserIsAdmin()) {
		unset($perm_struct[$perm_id]);
	}

	// remove admin permision for non admin users
	if(preg_match('/^admin_/', $perm->name) && !$userops->UserIsAdmin()) {
		unset($perm_struct[$perm_id]);
	}

}

// ------------------------------------
// -- post (important: after permission setup)
// ------------------------------------

if($access && (isset($_POST['submit']) || isset($_POST['apply']))) {

	// time for the query
	$now = $db->DbTimeStamp(time());

	// first delete the old perms from db
	foreach($group_list as $group) {

		// set the id
		$group_id = $group->id;

		// skip admin and groups user belongs to
		if($group_id == 1) continue;
		if($userops->UserInGroup(get_userid(), $group_id)) continue;
		
		// go through the permissions and remove
		foreach ($perm_struct as $old_perm_id => $old_perm) {
			$db->Execute(
				"DELETE FROM $group_perm_table
				 WHERE group_id = $group_id
				 AND permission_id = $old_perm_id"
			);
		}

	}

	// check post
	if(isset($_POST['groups'])) {

		// go through the post
		foreach($_POST['groups'] as $group_id => $new_permissions) {

			// skip admin and groups user belongs to
			if($group_id == 1) continue;
			if($userops->UserInGroup(get_userid(), $group_id)) continue;

			// insert new permissions
			foreach ($new_permissions as $new_permission_id) {

				// check if user has access to this permission and insert it
				if(isset($perm_struct[$new_permission_id])) {
					$db->Execute(
						"INSERT INTO $group_perm_table
						 (group_id, permission_id, create_date, modified_date) VALUES
						 ($group_id, $new_permission_id, $now, $now)"
					);
				}

			}

		}

	}

	// write into admin log
	audit(get_userid(), 'Group Permissions', "Modified User Group Permissions");

	// redirect with success message
	redirect($redirect . "&message=permissions_modified"); exit;

}

// ------------------------------------
// -- if cancel do a redirect
// ------------------------------------

// if cancel then redirect
if(isset($_POST["cancel"])) {
	redirect($redirect); exit;
}

// ------------------------------------
// -- output
// ------------------------------------

$header = get_vcms_file("/admin/header.php");

if($access) {

	// header
	include_once($header);

	// ------------------------------------
	// -- check message in the url
	// ------------------------------------

	if(isset($_GET["message"])) {
		$message = $themeObject->ShowMessage(lang($_GET['message']));
	}

	// ------------------------------------
	// -- setup our table
	// ------------------------------------

	// content
	$table_content = '';

	// go thorugh permisions
	foreach ($perm_struct as $key => $permission) {

		// setup header
		if(!isset($table_header)) {
			
			$table_header = '';

			foreach ($group_list as $group) {
				$table_header .= get_html(array(
					'content'	=> '<div>' . $group->name . '</div>',
					'class'		=> '',
					'action'	=> '',
					'template'	=> VCMS_PATH . '/admin/templates/table-cell.html'
				));
			}

			$table_header .= get_html(array(
				'content'	=> '',
				'class'		=> '  list__cell--permisions',
				'action'	=> '',
				'template'	=> VCMS_PATH . '/admin/templates/table-cell.html'
			));

		}

		$cells = '';

		foreach ($group_list as $group) {

			// checkbox checked attr
			$checkbox_checked = ' checked="checked"';

			$readonly = '';
			if($group->id == 1
			|| ($userops->UserInGroup(get_userid(), $group->id) && !$userops->UserIsAdmin())) {
				$readonly = '-static';
				$checkbox_checked = ' data-checked="checked"';
			}

			// only admins can change user perms
			if(preg_match('/^users_/', $permission->name)
			&& !$userops->UserIsAdmin()) {
				$readonly = '-static';
				$checkbox_checked = ' data-checked="checked"';
				$text_readonly = '  list__cell--permisions-readonly';
			}
			
			// group checkbox cell
			$cells .= get_html(array(
				'content'	=> get_html(array(
					'class'		=> '',
					'id'		=> 'group-' . $group->id . '-perm-' . $permission->id,
					'name'		=> 'groups[' . $group->id . '][]',
					'value'		=> $permission->id,
					'label'		=> '',
					'comment'	=> '',
					'checked'	=> isset($permission->group[$group->id]) || $group->id == 1 ? $checkbox_checked : '',
					'template'	=> VCMS_PATH . '/admin/templates/form-checkbox'.$readonly.'.html'
				)),
				'class'		=> '  list__cell--permisions-checkbox',
				'action'	=> '',
				'template'	=> VCMS_PATH . '/admin/templates/table-cell.html'
			));

		}

		// name cell
		$cells .= get_html(array(
			'content'	=> $permission->text,
			'class'		=> '  list__cell--permisions' . ( isset($text_readonly) ? $text_readonly : '' ),
			'action'	=> '',
			'template'	=> VCMS_PATH . '/admin/templates/table-cell.html'
		));
		
		$table_content .= get_html(array(
			'cells'		=> $cells,
			'class'		=> '',
			'id'		=> 'permission-id-' . $permission->id,
			'template'	=> VCMS_PATH . '/admin/templates/table-row.html'
		));

	}

	// submit
	$button_submit = get_html(array(
		'name'		=> 'submit',
		'value'		=> lang('submit'),
		'template'	=> $config['root_path'] . '/admin/templates/button-submit.html'
	));

	// cancel
	$button_cancel = get_html(array(
		'name'		=> 'cancel',
		'value'		=> lang('cancel'),
		'template'	=> $config['root_path'] . '/admin/templates/button-cancel.html'
	));

	// table
	$permissions_list = get_html(array(
		'message'	=> '',
		'class'		=> '  list--simple  list--permisions',
		'header'	=> $table_header,
		'content'	=> $table_content,

		// buttons
		'buttons'	=> '',
		'buttons_top' => false,
		'buttons_bottom' => false,

		'template'	=> VCMS_PATH . '/admin/templates/table.html'
	));

	// table
	echo get_html(array(
		'action'	=> $form_action,
		'message'	=> isset($message) ? $message : '',
		'content'	=> $permissions_list,

		// buttons
		'submit'	=> $button_submit,
		'apply'		=> '',
		'cancel'	=> $button_cancel,
		'buttons_top' => false,

		'template'	=> VCMS_PATH . '/admin/templates/form.html'
	));

} else {

	// header
	include_once($header);

	// ------------------------------------
	// -- no access message
	// ------------------------------------

	// set the message
	$message_text = $themeObject->ShowMessage(lang('no_access'), 'error');

	// template
	echo get_html(array(
		'message'	=> $message_text,
		'template'	=> VCMS_PATH . '/admin/templates/no-access.html'
	));

}

// ------------------------------------
// -- footer
// ------------------------------------

include_once(get_vcms_file("/admin/footer.php"));





// -- users_groups_permissions.php 