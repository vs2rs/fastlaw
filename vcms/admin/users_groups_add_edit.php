<?php

// ------------------------------------
// -- users add and edit
// ------------------------------------

global $CMS_ADMIN_PAGE;
$CMS_ADMIN_PAGE=1;

// includes
require_once(dirname(dirname(__FILE__)) . "/include.php");
require_once(dirname(dirname(__FILE__)) . "/lib/classes/class.group.inc.php");

// login
vcmsAuth::is_loged_in();

// ------------------------------------
// -- setup
// ------------------------------------

// class name
$ClassName = 'Group';

// redirect page after submit
$redirect = 'users_groups.php';

// form action
$form_action = 'users_groups_add_edit.php';

// check for id
$id = false;
if(isset($_GET['id'])) $id = floor($_GET['id']);

// ------------------------------------
// -- form setup
// ------------------------------------

function form_setup($id) {

	// new form
	$form = new stdClass();

	// get operations
	global $gCms;
	$form->operations = $gCms->GetGroupOperations();

	// check access
	$form->access_add  = check_permission(get_userid(), 'users_groups_add_remove');
	$form->access_edit = $form->access_add;

	// show buttons or not
	$form->button_submit = $form->access_edit;
	$form->button_apply  = true;
	$form->button_cancel = $form->access_edit;

	// ------------------------------------
	// -- input fields
	// ------------------------------------

	// col 1
	$form_col_1 = array(

		array(
			'type'	=> 'input',
			'label'	=> lang('group_name'),
			'name' 	=> 'name',
			'value' => '',
			'validate' => array(
				'not_empty' => true,
				'value' => 'name'
			)
		),

	);

	// active
	if($form->access_add || ($id && $form->operations->IsToggleActivePossible($id))) {
		
		$userops = $gCms->GetUserOperations();

		$form_col_1[] = array(
			'type'		=> 'checkbox',
			'label'		=> lang('group_active'),
			'name'		=> 'active',
			'value'		=> 1,
			'checked'	=> 0,
			'readonly'  => $id && $userops->UserInGroup(get_userid(), $id) || $id && $id == 1
		);

	}

	// ------------------------------------
	// -- hidden fields
	// ------------------------------------

	// if we are editing user we need a hidden id field
	if($id) {
		$form_col_1[] = array(
			'type'		=> 'hidden',
			'name'		=> 'id',
			'value'		=> $id
		);
	}

	// ------------------------------------
	// -- merge groups
	// ------------------------------------

	// col 2
	$form_col_2 = '';

	// merg cols for latter use in POST
	$form_inputs_merge = array_merge(
		$form_col_1,
		( is_array($form_col_2) ? $form_col_2 : array() )
	);

	// create array of input names
	$form->inputs = create_form_input_list($form_inputs_merge);

	// ------------------------------------
	// -- define form and tabs
	// ------------------------------------

	// create the tabs
	$form->tabs = array(
		0 => array(
			'name' 		=> isset($id) ? lang('group_edit') : lang('group_add'),
			'content'	=> array(
				'col-1' => $form_col_1,
				'col-2' => $form_col_2
			)
		)
	);

	// return the form
	return $form;

}

// ------------------------------------
// -- form
// ------------------------------------

// get the form
$form = form_setup($id);

include_once(get_vcms_file("/admin/form.php"));





// -- users_groups_add_edit.php