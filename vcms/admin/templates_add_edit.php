<?php
#CMS - CMS Made Simple
#(c)2004 by Ted Kulp (wishy@users.sf.net)
#This project's homepage is: http://cmsmadesimple.sf.net
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#$Id: templates_add_edit.php 6363 2010-06-06 15:46:39Z calguy1000 $

global $CMS_ADMIN_PAGE;
$CMS_ADMIN_PAGE = 1;

require_once(dirname(dirname(__FILE__)) . "/include.php");
require_once(dirname(dirname(__FILE__)) . "/lib/classes/class.template.inc.php");

vcmsAuth::is_loged_in();

// get user and check access
$userid = get_userid();
$access = check_permission($userid, 'templates_add');

// ------------------------------------
// -- default variables
// ------------------------------------

// templates list page
$templates_list = 'templates.php';

// error variable
$errors = array();

// default post vars
$template_name = "";
$template_content = "";
$template_default = 0;
$template_active = 1;
$template_users = "{}";

// action url for form
$form_action = 'templates_add_edit.php';

// ------------------------------------
// -- if cancel redirect
// ------------------------------------

// if cancel then redirect
if (isset($_POST["cancel"])) {
	redirect($templates_list);
	exit;
}

// ------------------------------------
// -- get db vars and template ops
// ------------------------------------

global $gCms;
$db =& $gCms->GetDb();
$templateops =& $gCms->GetTemplateOperations();

// ------------------------------------
// -- check for id and load data
// ------------------------------------

if(isset($_GET["template_id"])) {
	
	// get it
	$template_id = floor($_GET["template_id"]);

	// load data
	$onetemplate = $templateops->LoadTemplateByID($template_id);

	// if we have the template
	if($onetemplate) {

		// update vards
		$template_name = $onetemplate->name;
		$template_content = $onetemplate->content;
		$template_users = $onetemplate->users;
		$template_default = $onetemplate->default;
		$template_active = $onetemplate->active;

		// update url
		$form_action = $form_action . '&template_id=' . $template_id;

	}

}

// ------------------------------------
// -- POST
// ------------------------------------

if((isset($_POST["submit"]) || isset($_POST["apply"]))
&& isset($_POST["template_name"])
&& isset($_POST["template_content"])) {

	// get name and content from post
	$template_content = $_POST["template_content"];

	// set the user access to default
	$template_users = "{}";

	// validate user access post
	if(isset($_POST["user_access"]) && $_POST["user_access"] != "") {
		$template_users = validate_user_access_post($_POST["user_access"]);
	}

	// if no active in post then template not active
	$template_active = 0;
	if(isset($_POST["active"])) {
		$template_active = 1;
	}

	// validate template name
	if($_POST["template_name"] == "") {
		
		// no name given
		$errors['template_name'] = lang("nofieldgiven", array(lang('name')));

	} else {

		// var
		$name_exists = false;

		// if id set and name has changed
		if(isset($template_id)
		&& $_POST["template_name"] != $template_name
		&& $templateops->CheckExistingTemplateName($_POST["template_name"])) {
			$name_exists = true;
		}

		// if no id then new object
		if(!isset($template_id)
		&& $templateops->CheckExistingTemplateName($_POST["template_name"])) {
			$name_exists = true;
		}

		// name exists or not
		if($name_exists) {
			$errors['template_name'] = lang('templateexists');
		} else {
			$template_name = $_POST["template_name"];
		}

	}

	// no content
	if($template_content == "") {
		$errors['template_content'] = lang('nofieldgiven', array(lang('content')))."</li>";
	}

	// if no error
	if(count($errors) == 0) {

		// result
		$result = false;

		// update or add
		if(isset($template_id)
		&& isset($onetemplate)
		&& $onetemplate) {

			// create template object
			$current_template = $onetemplate;

			// audit text
			$audit_text = 'Edited Template';
			$message_text = 'template_updated';

		} else {

			// create template object
			$current_template = new PageTemplate();

			// audit text
			$audit_text = 'Added Template';
			$message_text = 'template_added';

		}

		// update properties
		$current_template->name 	= $template_name;
		$current_template->content 	= $template_content;
		$current_template->users 	= $template_users;
		$current_template->active 	= $template_active;
		$current_template->default 	= $template_default;

		// add to the db
		$result = $current_template->Save();

		// if success
		if($result) {

			// admin log
			audit($current_template->id, $template_name, $audit_text);

			// on submit we redirect
			if(isset($_POST["submit"])) {
				redirect($templates_list . "&message=$message_text"); exit;
			} else {
				$message = $message_text;
			}

		} else {
			
			// something went wrong
			$errors['add'] = lang('template_error_inserting');

		}

	}
	
}

// ------------------------------------
// -- admin header
// ------------------------------------

$header_file = get_vcms_file("/admin/header.php");
include_once($header_file);

// if we have access
if($access && (!isset($onetemplate)
|| (isset($onetemplate) && $onetemplate))) {

	// ------------------------------------
	// -- setup error message
	// ------------------------------------

	if(count($errors) > 0) {
		$message = $themeObject->ShowMessage($errors, 'error');
	} else {
		$message = isset($message) ? $themeObject->ShowMessage(lang($message)) : '';
	}

	// ------------------------------------
	// -- define tabs
	// ------------------------------------

	// create the tabs
	$form_content = $themeObject->CreateTabs(array(
		
		0 => array(

			'name' 		=> isset($template_id) ? lang('template_edit') : lang('template_add'),

			'content'	=> array(

				'col-1' => array(

					array(
						'type'	=> 'input',
						'label'	=> lang('name') . ' *',
						'name' 	=> 'template_name',
						'value' => $template_name
					),

					array(
						'type'	=> 'textarea',
						'label'	=> lang('content') . ' *',
						'name' 	=> 'template_content',
						'value' => $template_content,
						"editor" => false,
						"height" => 600
					),

					array(
						'type'	=> 'checkbox',
						'label'	=> lang('active'),
						'name' 	=> 'active',
						'value' => 1,
						'checked' => $template_active == 1 ? true : false
					)

				)

			)

		),

		1 => array(

			'name' 		=> lang('template_users'),

			'content'	=> array(

				'col-1' => array(

					array(
						'type'	 => 'user_access_checkboxes',
						'values' => json_decode($template_users)
					)

				)

			)

		)

	));

	// ------------------------------------
	// -- create the form
	// ------------------------------------

	// submit
	$button_submit = get_html(array(
		'name'		=> 'submit',
		'value'		=> lang('submit'),
		'template'	=> $config['root_path'] . '/admin/templates/button-submit.html'
	));

	// submit
	$button_apply = get_html(array(
		'name'		=> 'apply',
		'value'		=> lang('apply'),
		'template'	=> $config['root_path'] . '/admin/templates/button-submit.html'
	));

	// cancel
	$button_cancel = get_html(array(
		'name'		=> 'cancel',
		'value'		=> lang('cancel'),
		'template'	=> $config['root_path'] . '/admin/templates/button-cancel.html'
	));

	// add button
	echo get_html(array(

		// where to submit
		'action'	=> $form_action,
		
		// error/success messages
		'message'	=> $message,

		// content
		'content'	=> $form_content,
		'submit'	=> $button_submit,
		'apply'		=> $button_apply,
		'cancel'	=> $button_cancel,
		'buttons_top' => true,

		// template
		'template'	=> VCMS_PATH . '/admin/templates/form.html'

	));

} else {
	
	// ------------------------------------
	// -- setup no access message
	// ------------------------------------

	if(!$access) {

		$message = get_html(array(
			
			// error/success messages
			'message'	=> $themeObject->ShowMessage(lang('no_access'), 'error'),

			// template
			'template'	=> $config['root_path'] . '/admin/templates/no-access.html'

		));

	} else {

		$message = get_html(array(
			
			// error/success messages
			'message'	=> $themeObject->ShowMessage(lang('template_error_retrieving'), 'error'),

			// template
			'template'	=> $config['root_path'] . '/admin/templates/no-access.html'

		));

	}

	// output form
	echo $message;

}

// ------------------------------------
// -- footer
// ------------------------------------

include_once("footer.php");

# vim:ts=4 sw=4 noet
?>
