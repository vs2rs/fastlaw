<?php
#CMS - CMS Made Simple
#(c)2004 by Ted Kulp (wishy@users.sf.net)
#This project's homepage is: http://cmsmadesimple.sf.net
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#$Id: templates.php 6484 2010-07-09 08:28:36Z ronnyk $

global $CMS_ADMIN_PAGE;
$CMS_ADMIN_PAGE=1;

require_once(dirname(dirname(__FILE__)) . "/include.php");
require_once(dirname(dirname(__FILE__)) . "/lib/classes/class.template.inc.php");

vcmsAuth::is_loged_in();

$userid = get_userid();

$access = check_permission($userid, "templates_add")
	   || check_permission($userid, "templates_modify")
	   || check_permission($userid, "templates_remove");

if(!$access) {
	die('Permission Denied');
	return;
}

// ------------------------------------
// -- globals
// ------------------------------------

// get cms stuff
global $gCms;

// database
$db =& $gCms->GetDb();

// template operations
$templateops =& $gCms->GetTemplateOperations();

// ------------------------------------
// -- header
// ------------------------------------

$header_file = get_vcms_file("/admin/header.php");
include_once($header_file);

// ------------------------------------
// -- set success/error message
// ------------------------------------

if(isset($_GET["message"])) {
	$message = $themeObject->ShowMessage(lang($_GET['message']));
}

if(isset($_GET["error"])) {
	$message = $themeObject->ShowMessage(lang($_GET['error']), 'error');
}

// ------------------------------------
// -- set default
// ------------------------------------

if(isset($_GET['setdefault'])) {

	// get the template list
	$templatelist = $templateops->LoadTemplates();

	// update default template
	foreach ($templatelist as $onetemplate) {
		if($onetemplate->id == $_GET['setdefault']) {
			$onetemplate->default = 1;
			$onetemplate->active = 1;
			$onetemplate->Save();
		} else {
			$onetemplate->default = 0;
			$onetemplate->Save();
		}
	}

}

// ------------------------------------
// -- set active or inactive
// ------------------------------------

// if(isset($_GET['active'])
// && isset($_GET['id'])) {

// 	// get id
// 	$theid = floor($_GET['id']);

// 	// get template
// 	$thetemplate = $templateops->LoadTemplateByID($theid);

// 	// check if template is set 
// 	if(isset($thetemplate)) {
// 		$thetemplate->active = $_GET['active'] == 1 ? 1 : 0;
// 		$thetemplate->Save();
// 	}

// }

// ------------------------------------
// -- delete template
// ------------------------------------

if(isset($_GET["delete_template_id"])
&& floor($_GET["delete_template_id"] > 0)) {
	
	// load the template in a var
	if($delete_template = $templateops->LoadTemplateByID($_GET["delete_template_id"])) {

		// check if in use before delete
		if($templateops->CountPagesUsingTemplateByID($delete_template->id) > 0) {
			
			// show error message
			$message = $themeObject->ShowMessage(
				lang('template_in_use'), 'error'
			);

		} else {
			
			// do the delete
			if($result = $templateops->DeleteTemplateByID($delete_template->id)) {
				
				// show the success message
				$message = $themeObject->ShowMessage(
					lang('template_deleted', array($delete_template->name))
				);

			}

		}

	}

}

// ------------------------------------
// -- update actions
// ------------------------------------

// send --> action name --> template id --> template operations obj
if(isset($_GET['action'])) {
	content_action_json($_GET['action'], floor($_GET['id']), $templateops);
}

// ------------------------------------
// -- template list
// ------------------------------------

$templatelist = $templateops->LoadTemplates();

if ($templatelist && count($templatelist) > 0) {

	$templates_rows = '';

	foreach ($templatelist as $onetemplate) {

		// ------------------------------------
		// -- edit
		// ------------------------------------

		// link
		$edit_link = 'templates_add_edit.php?template_id='.$onetemplate->id;

		// icon edit
		$edit = get_html(array(
			'link'		=> $edit_link,
			'template'	=> $config['root_path'] . '/admin/templates/action-edit.html'
		));

		// ------------------------------------
		// -- default
		// ------------------------------------

		if($onetemplate->default == 1) {
			$default = get_html(array(
				'template'	=> $config['root_path'] . '/admin/templates/action-active-static.html'
			));
		} else {
			$default = get_html(array(
				'link'		=> 'templates.php?setdefault=' . $onetemplate->id,
				'onclick'	=> '',
				'template'	=> $config['root_path'] . '/admin/templates/action-active-off.html'
			));
		}

		// ------------------------------------
		// -- active
		// ------------------------------------

		// setup active/unactive
		if($onetemplate->default) {
			$active = get_html(array(
				'template'	=> $config['root_path'] . '/admin/templates/action-active-static.html'
			));
		} else {
			if($onetemplate->active == 1) {
				if($templateops->CountPagesUsingTemplateByID($onetemplate->id) > 0) {
					// if in use
					$active = get_html(array(
						'template'	=> $config['root_path'] . '/admin/templates/action-active-static.html'
					));
				} else {
					// can be changed
					$active = get_html(array(
						'link'		=> 'templates.php?active=0&id=' . $onetemplate->id,
						'onclick'	=> '',
						'template'	=> $config['root_path'] . '/admin/templates/action-active-on.html'
					));
				}
			} else {
				$active = get_html(array(
					'link'		=> 'templates.php?active=1&id=' . $onetemplate->id,
					'onclick'	=> '',
					'template'	=> $config['root_path'] . '/admin/templates/action-active-off.html'
				));
			}				
		}

		// ------------------------------------
		// -- copy
		// ------------------------------------

		$copy = get_html(array(
			'link'		=> 'templates_copy.php?template_id='.$onetemplate->id,
			'template'	=> $config['root_path'] . '/admin/templates/action-copy.html'
		));

		// get the delete button
		$active_button = $themeObject->getIconButton('active', $onetemplate);

		// get the delete button
		$delete_button = $themeObject->getIconButton('delete', $onetemplate);

		// ------------------------------------
		// -- row
		// ------------------------------------

		// get the template for the row
		$templates_rows .= get_html(array(
			
			// template vars
			'id' 		=> $onetemplate->id,
			'name' 		=> $onetemplate->name,
			'date' 		=> $onetemplate->modified_date,
			'link' 		=> $edit_link,
			'edit'		=> $edit,
			'default'	=> $default,
			'active'	=> $active_button,
			'copy'		=> $copy,
			'delete'	=> $delete_button,

			// some extra classes
			'class'		=> $onetemplate->active != 1 ? '  list__item--off' : '',

			// template html file
			'template'	=> $config['root_path'] . '/admin/templates/templates-row.html'

		));
		
	}

	// add button
	$add_button = get_html(array(

		'link'		=> 'templates_add_edit.php',
		'text'		=> lang('template_add'),

		'template'	=> $config['root_path'] . '/admin/templates/button-ok.html'

	));

	// get the template list template
	echo get_html(array(

		// message
		'message'	=> isset($message) ? $message : '',

		// add button
		'button'	=> $add_button,
		
		// header
		'id' 		=> lang('id'),
		'name' 		=> lang('template'),
		'date'		=> lang('date'),
		'edit'		=> lang('edit'),
		'default'	=> lang('default'),
		'active'	=> lang('active'),
		'copy'		=> lang('copy'),
		'delete'	=> lang('delete'),

		// content
		'content'	=> $templates_rows,

		// template html file
		'template'	=> $config['root_path'] . '/admin/templates/templates-list.html'

	));

}

include_once("footer.php");

# vim:ts=4 sw=4 noet
?>
