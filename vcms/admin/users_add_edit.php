<?php

// ------------------------------------
// -- users add and edit
// ------------------------------------

global $CMS_ADMIN_PAGE;
$CMS_ADMIN_PAGE=1;

// includes
require_once(dirname(dirname(__FILE__)) . "/include.php");
require_once(dirname(dirname(__FILE__)) . "/lib/classes/class.user.inc.php");

// login
vcmsAuth::is_loged_in();

// ------------------------------------
// -- setup
// ------------------------------------

// class name
$ClassName = 'User';

// redirect page after submit and form action
$redirect = 'users.php';
$form_action = 'users_add_edit.php';

// check for id
$id = false;
if(isset($_GET['id'])) $id = floor($_GET['id']);

// ------------------------------------
// -- form setup
// ------------------------------------

function form_setup($id) {

	// new form
	$form = new stdClass();

	// get main operations
	global $gCms;
	$form->operations = $gCms->GetUserOperations();

	// get access
	$form->access_edit = check_permission(get_userid(), 'users_add_remove') || ($id && $id == get_userid());
	$form->access_add  = check_permission(get_userid(), 'users_add_remove');

	// show buttons or not
	$form->button_submit = ($form->access_edit && ($id && $id != get_userid())) || $form->access_add || $form->operations->UserIsAdmin();
	$form->button_apply  = true;
	$form->button_cancel = ($form->access_edit && ($id && $id != get_userid())) || $form->access_add || $form->operations->UserIsAdmin();

	// ------------------------------------
	// -- input fields
	// ------------------------------------

	// col 1
	$form_col_1 = array(

		array(
			'type'	=> $id && $form->operations->IsUsernameChangable($id) || !$id && $form->access_add ? 'input' : 'readonly',
			'label'	=> lang('username'),
			'name' 	=> 'username',
			'value' => '',
			'validate' => array(
				'not_empty' => true,
				'value' => 'username'
			)
		),

		array(
			'type'	=> 'input',
			'label'	=> lang('firstname'),
			'name' 	=> 'firstname',
			'value' => '',
			'validate' => array(
				'value' => 'name'
			)
		),

		array(
			'type'	=> 'input',
			'label'	=> lang('lastname'),
			'name' 	=> 'lastname',
			'value' => '',
			'validate' => array(
				'value' => 'name'
			)
		),

		array(
			'type'	=> 'input',
			'label'	=> lang('email'),
			'name' 	=> 'email',
			'value' => '',
			'validate' => array(
				'value' => 'email'
			)
		)

	);

	// setup password button only if we are editing
	if($id && $id > -1) {
		$form->password_button = array(
			'type'	=> 'button',
			'label'	=> lang('password_change'),
			'text'	=> lang('password_show'),
			'name' 	=> 'changing_password',
			'value' => false,
			'class' => '  color-one-bg',
			'data'  => ' data-show-text="'.lang('password_show').'" data-hide-text="'.lang('password_hide').'"',
			'onclick' => ' onclick="showPasswordInputs.call(this); return false;"',
		);
	}

	$form_col_1[] = array(
		'type'	=> 'password',
		'label'	=> lang('password_new'),
		'name' 	=> 'password',
		'value' => '',
		'autocomplete' => 'new-password'
	);

	$form_col_1[] = array(
		'type'	=> 'password',
		'label'	=> lang('password_again'),
		'name' 	=> 'passwordagain',
		'value' => '',
		'autocomplete' => 'new-password'
	);

	// active
	if($form->access_add || ($id && $form->operations->IsToggleActivePossible($id))) {

		$form_col_1[] = array(
			'type'		=> 'checkbox',
			'label'		=> lang('users_active'),
			'name'		=> 'active',
			'value'		=> 1,
			'class'		=> '  form__row--border  form__row--extra-margin',
			'checked'	=> 0,
			'readonly'  => $id && $id == get_userid() || $id && $id == 1
		);

	}

	// ------------------------------------
	// -- load groups and add to tabs
	// ------------------------------------

	// check permissions
	$assign_groups = check_permission(get_userid(), 'users_groups_add_remove');

	// can't change groups for himself if not admin
	if($id && get_userid() == $id) $assign_groups = false;

	// if it's the boss admin all good
	if($form->operations->UserIsAdmin()) $assign_groups = true;

	// if all good setup checkboxes
	if($assign_groups) {

		$groupops = $gCms->GetGroupOperations();
		$user_groups = $groupops->LoadGroups();

		$form_col_1[] = array(
			'type'	=> 'label',
			'name' 	=> lang('users_group'),
			'class'	=> '  form__row--extra-margin',
		);

		foreach ($user_groups as $group) {

			// default is we can change user groups
			$group_readonly = false;

			// check if we can change administration group
			if($group->id == 1 && get_userid() != 1) {
				$group_readonly = true;
			}
			
			$form_col_1[] = array(
				'id'	=> 'group-id-' . $group->id,
				'type'	=> 'checkbox',
				'label'	=> $group->name,
				'name' 	=> 'groups[]',
				'value' => $group->id,
				'checked' => 'UserInGroup',
				
				// only if we are editing user we check if active toggle is possible
				'readonly'	=> $group_readonly
			);

		}
		
	} else {

		// create a hidden group field

		// get groups
		$groupops = $gCms->GetGroupOperations();
		$user_groups = $groupops->LoadGroups();

		// if we are editing
		if($id) {

			foreach ($user_groups as $group) {
				if($form->operations->UserInGroup($id, $group->id)) {
					$form_col_1[] = array(
						'type'		=> 'hidden',
						'name'		=> 'groups[]',
						'value'		=> $group->id
					);
				}
			}

		} else {

			// if we are adding create a hidden field for default group
			foreach ($user_groups as $group) {
				if($group->default) {
					$form_col_1[] = array(
						'type'		=> 'hidden',
						'name'		=> 'groups[]',
						'value'		=> $group->id
					);
				}
			}
			
		}

	}

	// ------------------------------------
	// -- hidden fields
	// ------------------------------------

	// if we are editing user we need a hidden id field
	if($id) {
		$form_col_1[] = array(
			'type'		=> 'hidden',
			'name'		=> 'id',
			'value'		=> $id
		);
	}

	// ------------------------------------
	// -- merge groups
	// ------------------------------------

	// col 2
	$form_col_2 = '';

	// merg cols for latter use in POST
	$form_inputs_merge = array_merge(
		$form_col_1,
		( is_array($form_col_2) ? $form_col_2 : array() )
	);

	// create array of input names
	$form->inputs = create_form_input_list($form_inputs_merge);

	// ------------------------------------
	// -- define form and tabs
	// ------------------------------------

	// create the tabs
	$form->tabs = array(
		0 => array(
			'name' 		=> $id ? lang('users_edit') : lang('users_add'),
			'content'	=> array(
				'col-1' => $form_col_1,
				'col-2' => $form_col_2
			)
		)
	);

	// return the form
	return $form;

}

// ------------------------------------
// -- form output
// ------------------------------------

// get the form
$form = form_setup($id);

// include the processing file
include_once(get_vcms_file("/admin/form.php"));





// -- users_add_edit.php