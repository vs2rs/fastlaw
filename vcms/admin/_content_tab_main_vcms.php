<?php

error_reporting(-1);

/* ------------------------------
// Get the template
------------------------------ */

// read the template
$templateops =& $gCms->GetTemplateOperations();
$pageTemplate = $templateops->LoadTemplateByID($contentobj->TemplateId());

// get the list of fields
$getContentInfo = $contentobj->SetupContentBlocks($pageTemplate->content);

// setup vars
$hiddenFileds = $getContentInfo["hide"];
$image_rows = $getContentInfo["images"];
$file_rows = $getContentInfo["files"];
$right_col_inputs = $getContentInfo["right_col"];

$google_inputs = array(
	'Meta Title:',
	'Meta Description:',
	'Meta Keywords:'
);

$facebook_inputs = array(
	'Facebook Description:',
	'Facebook Title:'
);

// the individual col after all content
// todo - šis nav nodefinēts templeitā
// bilžu upload apakšā kontentam
$image_col_inputs = array();

/* ------------------------------
// Inputu outputs
------------------------------ */

function outputContentRow($input, $contentobj = false) {

	$input[2] = $input[2] == '' ? $input[2] : '<div class="module-comment">' . $input[2] . '</div>';

	// check label language
	if(isset($input['field_name'])
	&& !preg_match('/^-- Add Me/', lang($input['field_name']))) {
		$label = lang($input['field_name']) . ':';
	}

	// update label if alias and our content type is block
	if(isset($input['field_name']) && $input['field_name'] == 'alias'
	&& $contentobj && $contentobj->Type() == 'block') {
		$label = 'ID #';
	}

	return '
	<div class="module-row'. ( $input[3] ? '' : '  module-row--hidden' ) . '">
		<div class="module-label">' . ( isset($label) ? $label : $input[0] ) . ' </div>
		<div class="module-input">'.$input[1].'</div>
		'.$input[2].'
	</div>';

}

function outputUploadForm($input, $params = false) {

	$type = false;
	$folder = false;

	if(isset($params)) {
		if(isset($params['type'])
		&& $params['type'] != "") {
			$type = $params['type'];
		}
		if(isset($params['folder'])
		&& $params['folder'] != "") {
			$folder = $params['folder'];
		}
	}

	// extract the id
	$inputElement = new DOMDocument();
	$inputElement->loadHTML($input[1]);
	$inputID = $inputElement->getelementsbytagname('input');
	$inputInfo = $inputID->item(0);
	
	// get folder
	$folder = $inputInfo->getAttribute('data-dir');
	
	// setup folder if type file
	if($folder != '') {	
		$folder = $inputInfo->getAttribute('data-dir');
	}

	$imageUpload = setup_upload_form(
		$type ? $type : "image",
		$input,
		$folder ? $folder : "pages",
		$inputInfo->getAttribute('value'),
		$inputInfo->getAttribute('name')
	);

	return $imageUpload; 

}

function check_by_field_name($input, $array) {
	return isset($input['field_name']) && in_array($input['field_name'], $array);
}

/* ------------------------------
// Satura tabs
------------------------------ */

if(isset($tab_contents_array[$currenttab])) {

	$extra_comments = array(
		// 'Meta Title:' => 'Max 70 caharacters. If not set default page title and site name is used.',
		// 'Meta Description:' => 'If not set Google will fill automaticly based on the page content.',
		// 'Meta Keywords:' => 'Google does not use the keywords meta tag in web ranking (since 2009).',
		// 'Facebook Title:' => '(Count <span></span>). If your title is longer than 90 characters, Facebook will start to truncate it. Try to keep your title shorter than 70 characters for best results on all devices.',
		// 'Facebook Description:' => '(Count <span></span>). Facebook will start to truncate your description around 250 chars. Depending on what devices your post is appearing on, your description might not even show at all (particularly on mobile). Aim to keep it under 150 chars.',
		// 'Facebook Image:' => 'Use images that are 1200x630 pixels for the best display on high resolution devices (min. 600x315px).'
	);

	$right_col = array();
	$left_col  = array();
	$google_col  = array();
	$facebook_col  = array();

	// ievadam galvenajā tabā manuāli satura inputu
	array_unshift($tab_contents_array[0], array(
		0 => lang('contenttype') . ':',
		1 => $typesdropdown,
		'field_name' => 'contenttype'
	));

	// set array for image upload inputs
	$image_uploads = array();

	foreach ($tab_contents_array[$currenttab] as $key => $input) {
		
		// ja nav array izlaižam
		if(!is_array($input)) continue;

		// pārbaudam vai ir extra commenti
		if(isset($extra_comments[$input[0]]) && !isset($input[2])) {
			$input[2] = $extra_comments[$input[0]];
		}

		// lai pareizi array būtu uzliekam input[2]
		$input[2] = isset($input[2]) ? $input[2] : '';

		// rādam inputu lietotājam
		$input[3] = true;

		// pārbaudam vai mēs rādam šo inputu lietotājam
		if(in_array($input[0], $hiddenFileds)
		|| check_by_field_name($input, $hiddenFileds)) {
			$input[3] = false;
		}

		// clear
		$input_output = '';

		// setup inputs
		if(in_array($input[0], $image_rows)
		|| check_by_field_name($input, $image_rows)) {
			if(in_array($input[0], $image_col_inputs)
			|| in_array($input['field_name'], $image_col_inputs)) {
				$image_uploads[$input[0]] =
					'<div class="module-row">' .
						outputUploadForm($input) .
					'</div>';
			} else {
				$input_output =
					'<div class="module-row">' .
						outputUploadForm($input) .
					'</div>';
			}
		} else if(in_array($input[0], $file_rows)
		|| check_by_field_name($input, $file_rows)) {
			$input_output =
				'<div class="module-row">' .
					outputUploadForm(
						$input, array(
							"type" => "file"
						)
					) .
				'</div>';
		} else {
			$input_output = outputContentRow($input);
		}

		// setup the cols
		if(in_array($input[0], $right_col_inputs)
		|| check_by_field_name($input, $right_col_inputs)) {
			$right_col[$input[0]] = $input_output;
		} elseif(in_array($input[0], $google_inputs)) {
			$google_col[$input[0]] = $input_output;
		} elseif(in_array($input[0], $facebook_inputs)) {
			$facebook_col[$input[0]] = $input_output;
		} elseif($input[0] == "Facebook Image:") {
			$facebook_image = $input;
		} else {
			$left_col[$input[0]] = $input_output;
		}

	}

	/* ------------------------------
	// satura izvade
	------------------------------ */

	echo '<div class="module-container  tab-content' . ( $currenttab == 0 ? '  tab-content-active' : '' ) . '" id="editab'.$currenttab.'">';

		/* ------------------------------
		// left column
		------------------------------ */

		echo '<div class="module-col">';
			foreach ($left_col as $input) { echo $input; }
		echo '</div>';
		
		/* ------------------------------
		// right column
		------------------------------ */

		echo '<div class="module-col">';
			foreach ($right_col as $input_name => $input) { echo $input; }
		echo '</div>';

		/* ------------------------------
		// images on the bottom
		------------------------------ */

		if(count($image_uploads) > 0) {

			echo '<div class="module-col-images">';
				foreach ($image_uploads as $image_input) {
					echo $image_input;
				}
			echo '</div>';

		}
	
	echo '</div>';

	/* ------------------------------
	// meta tab
	------------------------------ */

	echo '<div class="module-container  tab-content" id="editab2">';

		if(count($google_col) > 0
		|| count($facebook_col) > 0) {

			// split up hierarchy and get the first one
			// first one is the alias of the parent
			// and we need to check if it's a language
			$content_parent = explode("/", $contentobj->HierarchyPath());
			$content_parent = $content_parent[0];

			// use parent alias in the url or not
			$use_parent_url = false;

			// get the parent object form alias
			$content_parent = $contentops->LoadContentFromAlias($content_parent);
			$content_parent_name = $content_parent != NULL ? $content_parent->Name() : '';
			$content_parent_alias = $content_parent != NULL ? $content_parent->Alias() : '';

			// $site_name =  $content_parent != NULL ? $content_parent->Name() : '';
			$site_name = $content_parent_name;
			if($content_parent && $content_parent->GetPropertyValue('site_name')) {
				$site_name = $content_parent->GetPropertyValue('site_name');
			}

			// use site name from config if set
			if(Config::read("site_name")
			&& Config::read("site_name") != '') {
				$content_parent_name = Config::read("site_name");
				$site_name = Config::read("site_name");
			}

			if(count($google_col) > 0) {

				echo '<div class="module-col">';

					foreach ($google_col as $input_name => $input) {

						// google
						
						if($input_name == "Meta Title:") {

							echo '<div class="module-row">';
								echo '<div class="module-label">Google Search Preview:</div>';
								echo '<div class="google-search-preview"
									data-site-name="' . $site_name . '"
									data-seperator="|"
									data-url="' . $config['root_url_2'] . '/' . ( $use_parent_url ? $content_parent_alias . '/' : '' ) . '"
									data-default-title="title"
									data-meta-title="meta_title"
									data-meta-description="meta_description"
									data-alias="alias"
								></div>';
							echo '</div>';

						}

						echo $input;

					}

				echo '</div>';

			}

			if(count($facebook_col) > 0) {

				echo '<div class="module-col">';

					foreach ($facebook_col as $input_name => $input) {

						// facebook

						if($input_name == "Facebook Title:") {

							echo '<div class="module-row">';
								echo '<div class="module-label">Facebook Share Preview:</div>';
								echo '<div class="facebook-preview-container">';
									echo '<div class="facebook-share-preview"
										data-site-name="' . $content_parent_name . '"
										data-seperator="|"
										data-url="' . $config['production_www'] . '"
										data-default-title="title"
										data-meta-title="meta_title"
										data-facebook-title="facebook_title"
										data-facebook-description="facebook_description"
										data-facebook-image="facebook_image"
										data-default-image="img"
									></div>';
									echo outputUploadForm($facebook_image, array(
										'select_text' => 'Select Facebook Image',
										'remove_text' => 'Use Default Image'
									));
								echo '</div>';
							echo '</div>';

						}

						echo $input;

					}

				echo '</div>';

			}

		} else {

			echo '<div class="module-col">';
				echo '<div class="module-label">No metadata for this content page!</div>';
			echo '</div>';
			echo '<div class="module-col">';
			echo '</div>';

		}

	echo '</div>';

}

?>