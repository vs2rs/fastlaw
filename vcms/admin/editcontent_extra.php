<?php

function updatecontentobj(&$contentobj, $preview = false, $params = null)
{

	if ($params == null)
		$params = $_POST;

	$userid = get_userid();
		
	#Fill contentobj with parameters
	$contentobj->FillParams($params,true);
	if ($preview)
	{
		$error = $contentobj->ValidateData();
	}

	if (isset($params["ownerid"]))
	{
		$contentobj->SetOwner($params["ownerid"]);
	}

	$contentobj->SetLastModifiedBy($userid);

}

function copycontentobj(&$contentobj, $content_type, $params = null)
{

	global $gCms;
	$contentops =& $gCms->GetContentOperations();
	
	if ($params == null)
		$params = $_POST;

	$newcontenttype = strtolower($content_type);
	$tmpobj = $contentops->CreateNewContent($newcontenttype);
	$tmpobj->SetId($contentobj->Id());
	$tmpobj->SetName($contentobj->Name());
	$tmpobj->SetMenuText($contentobj->MenuText());
	$tmpobj->SetTemplateId($contentobj->TemplateId());
	$tmpobj->SetParentId($contentobj->ParentId());
	$tmpobj->SetOldParentId($contentobj->OldParentId());
	$tmpobj->SetAlias($contentobj->Alias());
	$tmpobj->SetOwner($contentobj->Owner());
	$tmpobj->SetActive($contentobj->Active());
	$tmpobj->SetItemOrder($contentobj->ItemOrder());
	$tmpobj->SetOldItemOrder($contentobj->OldItemOrder());
	$tmpobj->SetShowInMenu($contentobj->ShowInMenu());
	//Some content types default to false for a reason... don't override it
	if (!(!$tmpobj->mCachable && $contentobj->Cachable()))
		$tmpobj->SetCachable($contentobj->Cachable());
	$tmpobj->SetHierarchy($contentobj->Hierarchy());
	$tmpobj->SetLastModifiedBy($contentobj->LastModifiedBy());
	$tmpobj->Properties();
	$contentobj = $tmpobj;
	$contentobj->FillParams($params);
}

?>