<?php

// ------------------------------------
// -- pages add and edit
// ------------------------------------

global $CMS_ADMIN_PAGE;
$CMS_ADMIN_PAGE=1;

// includes
require_once(get_vcms_file("/include.php"));
require_once(get_vcms_file("/lib/classes/class.pageoperations.inc.php"));

// login
vcmsAuth::is_loged_in();

// ------------------------------------
// -- setup
// ------------------------------------

// class name
// $ClassName = 'User';

// redirect page after submit and form action
$redirect = 'pages.php';
$form_action = 'pages_add_edit.php';

// ------------------------------------
// -- test
// ------------------------------------

// new item
$item = new Page();

// check id
if(isset($_GET['id'])) {
	$item->id = floor($_GET['id']);
}

// ------------------------------------
// -- form for editing and adding
// ------------------------------------

if($item->load()) {

	// all good show add/edit form
	echo 'all good';

	print_it($item);
		
} else {

	echo 'no good';

	// we have no access or object failed to load
	// $item->no_access();

}







// ------------------------------------
// -- form output
// ------------------------------------

// get the form
// $form = form_setup($id);

// include the processing file
// include_once(get_vcms_file("/admin/form.php"));