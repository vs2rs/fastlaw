<?php

header("Connection: close");

/* ------------------------------

	CMS STUFF

------------------------------ */

$CMS_ADMIN_PAGE=1;

// get main vcms config
require_once(dirname(dirname(dirname(__FILE__))) . '/include.php');
require_once(dirname(dirname(dirname(__FILE__))) . '/lib/misc.functions.php');

global $gCms;
$db =& $gCms->GetDb();

/* ------------------------------

	Setup Folder

------------------------------ */

$parent = $_GET['folder'];

$get_folder_query = "SELECT * FROM ".cms_db_prefix()."images_folders where id = ?";
$folder = $db->GetRow($get_folder_query, array($parent));

// check if file types is set
if(!isset($folder['file_types'])
|| $folder['file_types'] == "") {
	$file_types = "jpg,png,gif,svg,webp";
} else {
	$file_types = $folder['file_types'];
}

$module = cms_db_prefix() . "images";

// setup upload size
if(isset($folder['max_upload_size'])
&& $folder['max_upload_size'] != ""
&& $folder['max_upload_size'] != null) {
	$upload_size = $folder['max_upload_size'];
} else {
	$upload_size = $config['max_upload_size'];
}

/* ------------------------------

	Setup Input ID

------------------------------ */

// todo - need to validate this in the js script
$inputId = strip_tags($_GET['inputid']);

/* ------------------------------

	Check for CKEditor

------------------------------ */

$ckeditorNum = "";
if(isset($_GET['inputid']) && $_GET['inputid'] == "ckeditor"
&& isset($_GET['CKEditorFuncNum']) && $_GET['CKEditorFuncNum'] != "") {
	$ckeditorNum = ' data-ckeditor-num="'.$_GET['CKEditorFuncNum'].'"';
}

?><!DOCTYPE html>
<html>
	<head>
		<title>Uploading to: <?php echo isset($folder['folder']) ? $folder['folder'] : "No folder set"; ?></title>
		<!-- meta -->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!-- global stuff -->
		<link href="../style.php" type="text/css" rel="stylesheet" />
		<link href="../themes/NCleanGrey/css/006-module.css" type="text/css" rel="stylesheet" />
		<!-- jquery -->
		<script type="text/javascript" src="//code.jquery.com/jquery-latest.min.js"></script>
		<!-- dropzone js -->
		<script src="dropzone.js" type="text/javascript"></script>
		<script src="uploads.js" type="text/javascript"></script>
		<!-- dropzone css -->
		<link href="dropzone.css" type="text/css" rel="stylesheet" />
	</head>
	<body>

		<div class="module-wrap">

			<div class="module-container  module-container--no-tabs  dropzone-upload-container">

				<div class="dropzone-upload-info">
					<div>
						<!-- Max upload size is <?php echo $upload_size; ?> MB -->
						Memmory limit is
						<?php echo str_replace("M", "", ini_get("memory_limit")); ?> MB
						<br/>
						Post limit
						<?php echo ini_get('post_max_size'); ?>
						|
						Upload limit
						<?php echo ini_get('upload_max_filesize'); ?>
						<br/>
						Max input time:
						<?php echo ini_get('max_input_time'); ?>
						|
						Max Execution time:
						<?php echo ini_get('max_execution_time'); ?>
						
					</div>

					<?php

						// resize dropdown

						if(isset($config['image_sizes'])
						&& is_array($config['image_sizes'])
						&& count($config['image_sizes']) > 0) {
							
							echo '<div class="dropzone-dropdown">
							<select onchange="dropzoneSetResizeValue(this);">
								<option value="0">Do not resize images</option>
							';

							foreach ($config['image_sizes'] as $key => $value) {
								echo '<option value="'.$key.'">Resize: '.$value['size'].' '.$value['label'].'</option>';
							}

							echo '</select></div>';

						}

					?>
				</div>
				
				<form action="index.php?action=upload" class="dropzone" id="gallery" data-input-id="<?php echo $inputId; ?>"<?php echo $ckeditorNum; ?>>
					<input type="hidden" name="module" value="<?php echo $module; ?>" />
					<input type="hidden" name="parent" value="<?php echo $parent; ?>" />
					<input type="hidden" name="folder" value="<?php echo isset($folder['folder']) ? $folder['folder'] : ''; ?>" />
					<input type="hidden" name="max_mb" value="<?php echo $upload_size; ?>" />
					<input type="hidden" name="file_types" value="<?php echo $file_types; ?>" />
					<input type="hidden" name="sort" value="date_created DESC" />
					<input type="hidden" name="resize" value="0" />
				</form>

			</div>

		</div>

	</body>
</html>