<?php

/* ------------------------------

	CMS STUFF

------------------------------ */

$CMS_ADMIN_PAGE=1;

// get main vcms config
require_once(dirname(dirname(dirname(__FILE__))) . '/lib/misc.functions.php');
require_once(dirname(dirname(dirname(__FILE__))) . '/include.php');

global $gCms;
$db =& $gCms->GetDb();





/* ------------------------------

	UPLOAD STUFF

------------------------------ */

// set time zone
date_default_timezone_set('Europe/Riga');

// response function
require_once("functions/outputResponse.php");
// check file type function
require_once("functions/checkFileType.php");
// resize image function
require_once("functions/resizeImage.php");





/* ------------------------------

	Validate:
	- module

------------------------------ */

// validate module
if(!isset($_POST["module"]) || (isset($_POST["module"]) && $_POST["module"] == "")) {
	
	outputResponse(array(
		"status" => "error",
		"message" => "Missing database inforamation in the Dropzone settings!"
	));

} else {
	
	// set module
	$module = $_POST["module"];
	$db_name = $config['db_name'];

	// run the query to check table
	$checkTable = $db->GetOne("
		SELECT table_name FROM information_schema.tables
		WHERE table_schema = '$db_name' AND table_name = '$module' LIMIT 1"
	);

	// if no table report an error
	if($checkTable === false) {
		outputResponse(array(
			"status" => "error",
			"message" => "Table " . $module . " dose not exists in the database!"
		));
	}

}

// validate parent
if(!isset($_POST["parent"]) || (isset($_POST["parent"]) && $_POST["parent"] == "")) {

	outputResponse(array(
		"status" => "error",
		"message" => "Missing Gallery ID in the Dropzone settings!"
	));
	
} else {

	// todo: need to think of way to validate
	$parent = $_POST["parent"];

}

// validate folder
if(!isset($_POST["folder"]) || (isset($_POST["folder"]) && $_POST["folder"] == "")) {

	outputResponse(array(
		"status" => "error",
		"message" => "Missing upload folder in the Dropzone settings!"
	));

} else {

	// set folder
	$folder = $_POST["folder"];
	$uploads_path = $config['uploads_path'] . $folder;

	// check if folder exists
	if(!file_exists($uploads_path)) {

		outputResponse(array(
			"status" => "error",
			"message" => "Upload directory " . $uploads_path . " dose not exists!"
		));

	} else {

		// for upload and delete we need to check write permisions
		if((isset($_POST["upload"]) && $_POST["upload"] == "")
		|| (isset($_POST["delete"]) && $_POST["delete"] == "")) {
			if(!is_writable($uploads_path)) {
				outputResponse(array(
					"status" => "error",
					"message" => "Upload directory " . $uploads_path . " has no write access!"
				));
			}
		}

	}

}

// check if there is limit on uploads
if(isset($_POST["file_limit"])) {

	// get the param
	$file_limit = floor($_POST["file_limit"]);

	// set the count and grab the files
	$file_count = 0;
	$check_file_count = glob($uploads_path . "*");

	// check if files
	if($check_file_count) {
		$file_count = count($check_file_count);
	}

	// check the count
	if($file_count >= $file_limit) {
		outputResponse(array(
			"status" => "error",
			"message" => "Maximum number of files exceeded! You can only upload $file_limit files in this directory!"
		));
	}

}

// setup sort
if(isset($_POST["sort"])
&& ($_POST["sort"] == "src ASC"
|| $_POST["sort"] == "src DESC"
|| $_POST["sort"] == "date_created ASC"
|| $_POST["sort"] == "date_created DESC"
|| $_POST["sort"] == "item_order ASC"
|| $_POST["sort"] == "item_order DESC"
|| $_POST["sort"] == "item_order ASC, date_created ASC"
|| $_POST["sort"] == "item_order ASC, date_created DESC"
|| $_POST["sort"] == "item_order DESC, date_created ASC"
|| $_POST["sort"] == "item_order DESC, date_created DESC"
)) {
	$order_by = $_POST["sort"];
} else {
	$order_by = 'date_created DESC';
}

// setup sort
if(isset($_POST["resize"]) && floor($_POST["resize"]) > 0) {
	$resize_index = floor($_POST["resize"]);
	$resize = explode("x", $config['image_sizes'][$resize_index]['size']);
} else {
	$resize = false;
}





/* ------------------------------

	ACTIONS

------------------------------ */

// supported action and current action
$supported_actions = array("list", "upload", "update", "delete", "update-order");
$action = isset($_GET["action"]) ? $_GET["action"] : false;

// load the actions
if($action && in_array($action, $supported_actions)) {
	require_once("actions/$action.php");
} else {
	outputResponse(array(
		"status" => "error",
		"message" => "Wrong URL!",
		"url" => $action
	));
}





?>