<?php

if(!empty($_FILES)) {

	if($_FILES["file"]['error'] == 0) {

		/* ------------------------------

			SETUP

		------------------------------ */

		// setup max size
		$maxsize = isset($_POST["max_mb"])
				 ? floor($_POST["max_mb"])
				 : str_replace("M", "", ini_get('post_max_size'));

		// setup max size
		$filetypes = isset($_POST["file_types"])
				   ? explode(",", $_POST["file_types"])
				   : array("jpg","png","gif","svg");

		// setup stuff
		$file = $_FILES["file"]["tmp_name"];
		$extension = strtolower(pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION));
		// reset jpeg to jpg
		if($extension == "jpeg") { $extension = "jpg"; }
		$filename = munge_string_to_url(pathinfo($_FILES['file']['name'], PATHINFO_FILENAME)) . "." . $extension;

		/* ------------------------------

			CHECK FILE SIZE

		------------------------------ */

		// check file size
		if(filesize($file) / 1024 / 1024 > $maxsize) {
			outputResponse(array(
				"status" => "error",
				"message" => "Exceeded filesize limit of ".$maxsize."mb!"
			));
	    }

	    /* ------------------------------

			CHECK FILE TYPE

		------------------------------ */

		$type = checkFileType($file, $extension, $filetypes);
		if(!in_array($type, $filetypes)) {
			outputResponse(array(
				"status" => "error",
				"message" => $type // we return the error text
			));
		}

	    /* ------------------------------

			CHECK IF FILE EXISTS

		------------------------------ */

		if(file_exists($uploads_path . $filename)) {
			outputResponse(array(
				"status" => "error",
				"message" => "File with this name already exists!"
			));
		}

	} else {

		/* ------------------------------

			$_FILES errors

		------------------------------ */

		$file_errors = array(
			0 => 'There is no error, the file uploaded with success',
		    1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
		    2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
		    3 => 'The uploaded file was only partially uploaded',
		    4 => 'No file was uploaded',
		    5 => 'Error', // 5 missing from php.net info
		    6 => 'Missing a temporary folder',
		    7 => 'Failed to write file to disk.',
		    8 => 'A PHP extension stopped the file upload.'
		);

		error_log($file_errors[$_FILES["file"]['error']]);

		outputResponse(array(
			"status" => "error",
			"message" => $file_errors[$_FILES["file"]['error']]
		));

	}

} else {

	/* ------------------------------

		$_FILES empty

	------------------------------ */

	if(isset($_SERVER['CONTENT_LENGTH']) && intval($_SERVER['CONTENT_LENGTH']) > 0 && count($_POST) === 0){
	    outputResponse(array(
			"status" => "error",
			"message" => "PHP discarded POST data because of request exceeding post_max_size."
		));

	} else {

		outputResponse(array(
			"status" => "error",
			"message" => "No file received. Try uploading again!"
		));

	}

}

/* ------------------------------

	UPLOAD

------------------------------ */

if(move_uploaded_file($file, $uploads_path . $filename)) {

	$time = str_replace("'","",$db->DBTimeStamp(time()));
	$query = "INSERT INTO $module SET parent = ?, src = ?, date_created = ?, active = ?";
	$result = $db->Execute($query, array($parent, $filename, $time, 1));

	$row = $db->GetRow("SELECT * FROM $module WHERE src = '$filename' AND parent = '$parent' LIMIT 1");

	if($result && $row) {

		if($extension == "zip" || $extension == "pdf" || $extension == "mp4") {

			// alt tags false
			$alt_tags = false;

		} else {

			// check if resize and we have an image to resize
			if($resize && in_array($type, array("jpg","gif","png"))) {
				$resizeImage = resizeImage(
					$uploads_path . $filename,
					$type,
					$resize[0],
					$resize[1]
				);
			}

			// add $row to an array
			$alt_tags = array();

		}

		// setup alt tags for the response (used in js to setup inputs)
		foreach ($row as $row_name => $row_value) {
			// check if alt tags
			if(substr($row_name, 0, 3) == "alt") {
				if(in_array($type, array("jpg","gif","png","svg"))) {
					$alt_tags[$row_name] = $row_value;
				}
			}
		}

		outputResponse(array(
			"status" => "success",
			"message" => "File was uploaded successfully.",
			"image" => array(
				"full_url" => APP_URL . '/' . $config["uploads_url"] . $folder . $filename,
				"folder" => $folder,
				"src" => $filename,
				"id" => $row["id"],
				"parent" => $parent,
				"module" => $module,
				"date_created" => $time,
				"alt_tags" => $alt_tags,
				"extension"	=> $extension
			)
		));

	} else {
		
		// remove the file becouse db update failed
		unlink($uploads_path . $filename);

		outputResponse(array(
			"status" => "error",
			"message" => "Could not update the database! "
		));

	}

} else {

	outputResponse(array(
		"status" => "error",
		"message" => "Could not move the file to the upload directory!"
	));

}

?>