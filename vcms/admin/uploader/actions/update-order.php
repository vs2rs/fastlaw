<?php

$response = array(
	"status" => "error",
	"message" => "Error updating order! Try again latter!"
);

// check if post is set
if(isset($_POST["images"])) {

	// decode the jason
	$images = json_decode($_POST["images"]);

	if(is_array($images) && count($images) > 0) {

		$updatedImages = 0;

		foreach ($images as $image) {
			$query = "UPDATE $module SET item_order = ? WHERE id = ?";
			$dbresult = $db->Execute($query, array($image->item_order, $image->image_id));
			if($dbresult) {
				$updatedImages++;
			}
		}

		$response = array(
			"status" => "success",
			"message" => "Updated order for $updatedImages images!"
		);

	} else {

		$response = array(
			"status" => "error",
			"message" => "No images posted!"
		);

	}

} else {

	$response = array(
		"status" => "error",
		"message" => "No images posted!"
	);

}

// set header
header('Content-type: application/json; charset=utf-8');

// encode json
$response = json_encode($response);

// output
echo $response;

?>