<?php

// alt tag query and values
$alt_tag_query = "";
$alt_tag_value = array();

// go through posts and collect all alt tags
foreach ($_POST as $key => $value) {
	if(substr($key, 0, 3) == "alt") {
		// we need to add coma if more then one tag
		if($alt_tag_query != "") $alt_tag_query .= ",";
		// setup values
		$alt_tag_query .= " $key = ?";
		$alt_tag_value[] = $value;
	}
}

// add id to the value list
$alt_tag_value[] = floor($_POST["id"]);

$query = "UPDATE $module SET $alt_tag_query WHERE id = ?";
$dbresult = $db->Execute($query, $alt_tag_value);

if($dbresult) {
	$response = array(
		"status" => "success",
		"message" => "Image alt tags where successfuly updated!"
	);
} else {
	$response = array(
		"status" => "error",
		"message" => "Something went wrong, refresh the page and try again!"
	);
}

// set header
header('Content-type: application/json; charset=utf-8');

// encode json
$response = json_encode($response);

// output
echo $response;

?>