<?php

$id = floor($_POST["id"]);
$src = $_POST["src"];
$parent = $_POST["parent"];

/* ------------------------------

	DELETE

------------------------------ */

// set path to file
$path_to_file = $uploads_path . $src;

// check if exists
if(file_exists($path_to_file)) {

	// delete from db
	$query = "DELETE FROM $module WHERE id = $id AND parent = '$parent'";
	$dbresult = $db->Execute($query);

	if($dbresult) {

		// remove the file if query is a success
		unlink($path_to_file);
		
		// set response
		$response = array(
			"status" => "success",
			"message" => "Image deleted!",
			"query"	=> $query
		);

	} else {
		
		// set response
		$response = array(
			"status" => "error",
			"message" => "Something went wrong, refresh the page and try again!"
		);

	}

} else {

	// if file doesnt exist, delete from db if it's still there
	$query = "DELETE FROM $module WHERE id = $id AND parent = '$parent'";
	$dbresult = $db->Execute($query);

	// set response
	$response = array(
		"status" => "error",
		"message" => "The file you are trying to delete does not exists!",
		"query"	=> $query
	);

}

// output
outputResponse($response);

?>