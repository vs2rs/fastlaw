<?php

$query = "SELECT * FROM $module WHERE parent = '$parent' ORDER BY $order_by, id DESC";
$dbresult = $db->Execute($query);

$images = array();

while($dbresult && $row = $dbresult->FetchRow()) {

	// create image
	$image = array();

	// get the extension
	$extension = strtolower(pathinfo($row["src"], PATHINFO_EXTENSION));

	// check file type
	// todo => failus vajag savādāk pārbaudīt
	if($extension == "zip" || $extension == "pdf" || $extension == "mp4") {

		// alt tags false
		$image["alt_tags"] = false;

	} else {

		// alt tags array
		$image["alt_tags"] = array();

	}

	// add $row to the $image array
	foreach ($row as $row_name => $row_value) {
		// check if alt tags
		if(substr($row_name, 0, 3) == "alt") {
			if(in_array($extension, array("jpg","gif","png","svg","webp"))) {
				$image["alt_tags"][$row_name] = $row_value;
			}
		} else {
			$image[$row_name] = $row_value;
		}
	}

	// full url to the image
	$image["full_url"] = APP_URL . '/' . $config["uploads_dir"] . '/' . $folder . '/' . $row["src"];

	// set module and folder
	$image["folder"] = $folder;
	$image["module"] = $module;
	$image["extension"] = $extension;

	// add to images array
	$images[] = $image;

}

// output

outputResponse(array(
	"status" => "success",
	"message" => "Images: " . count($images),
	"image_count" => count($images),
	"images" => $images
));

?>