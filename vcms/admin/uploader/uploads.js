/* ------------------------------

	Activate Dropzone

------------------------------ */

// turn off discover
Dropzone.autoDiscover = false;

// check for dropzones on load
document.addEventListener('DOMContentLoaded', function() {

	// get dropzones
	const myDropzones = document.querySelectorAll(".dropzone");

	// setup
	myDropzones.forEach(function(currentDropzone){

		new Dropzone(currentDropzone, {

			// maxFilesize: currentDropzone.querySelector("input[name=max_mb]").value, // MB

			init: function() {
				
				// get the dropdown container
				const dropzoneContainer = findDropzoneContainer(this.element);
				const dropzoneUploadForm = dropzoneContainer.querySelector("form");

				// get input id, only present in popup window, we validate in setupImagesList() call
				const inputId = dropzoneContainer.querySelector(".dropzone").dataset.inputId;

				// create necessary divs
				const errorsContainer = document.createElement("div");
				const successContainer = document.createElement("div");
				const imagesContainer = document.createElement("div");
				errorsContainer.classList.add("dropzone-error");
				successContainer.classList.add("dropzone-success");
				imagesContainer.classList.add("dropzone-images");
				// insert after form
				dropzoneUploadForm.insertAdjacentElement('afterend', errorsContainer);
				dropzoneUploadForm.insertAdjacentElement('afterend', successContainer);
				// just apend at the end
				dropzoneContainer.appendChild(imagesContainer);

				// get the inputs
				const galleryParentInput = dropzoneContainer.querySelector("input[name=parent]");
				const galleryModuleInput = dropzoneContainer.querySelector("input[name=module]");
				const galleryFolderInput = dropzoneContainer.querySelector("input[name=folder]");
				const gallerySortbyInput = dropzoneContainer.querySelector("input[name=sort]");

				// setup the list of images
				setupImagesList({
					parent: 	galleryParentInput === null ? '' : galleryParentInput.value,
					module: 	galleryModuleInput === null ? '' : galleryModuleInput.value,
					folder: 	galleryFolderInput === null ? '' : galleryFolderInput.value,
					sort: 		gallerySortbyInput === null ? '' : gallerySortbyInput.value,
					inputId: 	inputId === undefined || inputId === null || inputId == "" ? false : inputId,
					container: 	imagesContainer
				});

				// on upload success
				this.on("success", function(file, response) {

					if(response.status == "error") {

						// create error message
						outputStatusMessage(dropzoneContainer, {
							status: response.status,
							text: 	response.message,
							file: 	file.name
						});

					}

					if(response.status == "success") {
						
						// create success message
						outputStatusMessage(dropzoneContainer, {
							status: response.status,
							text: 	response.message,
							file: 	file.name
						});

						// add image to the list
						const imageHTML = setupImage(response.image, inputId);
						imagesContainer.insertAdjacentElement("afterbegin", imageHTML);

					}

					// remove after 2 seconds so that animation finishes
					setTimeout(() => {
						this.removeFile(file);
					}, 2000);

				});

			}

		});

	});

});





/* ------------------------------

	Helper Functions

------------------------------ */

// find the main div of the dropzone
function findDropzoneContainer(element) {
    while ((element = element.parentElement) && !element.classList.contains("dropzone-upload-container"));
    return element;
};

// find the image container
function findDropzoneImageContainer(element) {
    while ((element = element.parentElement) && !element.classList.contains("dropzone-images__item"));
    return element;
};

// create satus message for the upload
function outputStatusMessage(dropzone, message) {

	// check if remove button is set
	if(message.removeButton === undefined) {
		message.removeButton = true; // default is true
	}

	// get container and check if it's there
	const container = dropzone.querySelector(".dropzone-" + message.status);
	if(container === null || message.status != "success" && message.status != "error") {
		return false;
	}

	// create the message
	const msg = document.createElement("div");
	if(message.file !== undefined && message.file !== null && message.file) {
		msg.innerHTML = "<div><strong>" + message.file + "</strong> - " + message.text + "</div>";
	} else {
		msg.innerHTML = "<div><strong>" + message.text + "</strong></div>";
	}
	
	// add extra button to remove the message
	if(message.status == "error" && message.removeButton === true) {
		const remove = document.createElement("button");
		remove.innerHTML = 'Remove this message!';
		remove.addEventListener("click", function(event) {
			event.preventDefault();
			this.parentNode.remove();
		});
		msg.appendChild(remove);
	}
	
	// append to the container
	container.appendChild(msg);

	// if success message: remove after 2 seconds
	if(message.status == "success") {
		setTimeout(() => {
			msg.remove();
		}, 2000);
	}

};





/* ------------------------------

	Image List

------------------------------ */

function setupImagesList(gallery) {

	// setup dir of uploader
	// we need this for the requests
	// if popup window we have inputID
	// and popup is in the uploader dir
	let uploaderDir = "uploader/"
	if(gallery.inputId) {
		uploaderDir = "";
	}

	// get data
	const data = new FormData();
	data.append("parent", gallery.parent);
	data.append("module", gallery.module);
	data.append("folder", gallery.folder);
	data.append("sort", gallery.sort);

	// send get request for files
	const xhr = new XMLHttpRequest;
	xhr.addEventListener("load", function(){

		// set response as json
		const response = JSON.parse(this.response);

		if(response.status == "success") {

			if(response.image_count > 0) {
			
				// setup images
				response.images.forEach(function(image){
					const imageHTML = setupImage(image, gallery.inputId);
					gallery.container.appendChild(imageHTML);
				});

			}

			// no files message
			const noImages = document.createElement("div");
			noImages.classList.add("dropzone-images__noimages");
			noImages.innerHTML = "No files have been uploaded!";
			gallery.container.appendChild(noImages);

		}

		if(response.status == "error") {

			// get dropzone
			const dropzoneContainer = findDropzoneContainer(gallery.container);
			const thisDropzone = dropzoneContainer.querySelector(".dropzone").dropzone;

			// create error message
			outputStatusMessage(dropzoneContainer, {
				status: response.status,
				text: 	response.message,
				file: 	"Error",
				removeButton: false
			});

			// disable uploads
			thisDropzone.disable();

		}

	});
	xhr.open("POST", uploaderDir + "index.php?action=list", true);
	xhr.send(data);

};





/* ------------------------------

	Create Image

------------------------------ */

function setupImage(image, inputId) {

	// setup dir of uploader
	// we need this for the requests
	// if popup window we have inputID
	// and popup is in the uploader dir
	let uploaderDir = "uploader/"
	if(inputId) {
		uploaderDir = "";
	}

	/*
		
		* Main containers

	*/

	// the main container for the image
	const result = document.createElement("div");
	result.classList.add("dropzone-images__item");
	result.dataset.id = image.id;

	// info container of the image
	const info = document.createElement("div");
	info.classList.add("dropzone-images__info");

	/*	
		
		* Image preview
		* Attach it to info div

	*/

	// image preview div
	const imagePreview = document.createElement("div");
	if(image.extension == "pdf"
	|| image.extension == "zip"
	|| image.extension == "xls"
	|| image.extension == "xlsx"
	|| image.extension == "mp4") {
		imagePreview.style.backgroundImage = "url('icons/" + image.extension + ".svg')";
	} else {
		imagePreview.style.backgroundImage = "url('" + image.full_url + "')";
		if(image.extension == 'svg') {
			imagePreview.classList.add('dropzone-images__svg');
		}
	}

	// attach to info
	info.appendChild(imagePreview);

	/*
		
		* Create image info div
		* date, file name & delete button
		* select button (only in popup window)

	*/

	// create date
	const date = document.createElement("div");
	date.innerHTML = "Date added: " + image.date_created;
	date.classList.add("dropzone-images__date");

	// create title
	const title = document.createElement("a");
	const title_inner = document.createElement("span");
	title_inner.innerHTML = image.src;
	title.href = image.full_url;
	title.target = "_blank";
	title.classList.add("dropzone-images__title");
	title.appendChild(title_inner);

	// delete button
	const deleteButton = document.createElement("button");
	deleteButton.classList.add("dropzone-images__delete");
	deleteButton.innerHTML = "Delete";
	deleteButton.addEventListener("click", function(event) {
		
		// prevent default
		event.preventDefault();

		// ask to confirm delete
		if(confirm("Do you realy want to delete this image? " + image.src)) {

			// containing div
			const imageContainer = findDropzoneImageContainer(this);
			const dropzoneContainer = findDropzoneContainer(this);
			
			// setu send data
			const data = new FormData();
			data.append("id", image.id);
			data.append("src", image.src);
			data.append("parent", image.parent);
			data.append("module", image.module);
			data.append("folder", image.folder);

			// send post to delete image
			const xhr = new XMLHttpRequest;
			xhr.addEventListener("load", function(){

				// set response as json
				const response = JSON.parse(this.response);

				if(response.status == "success") {

					// create success message
					const success = dropzoneContainer.querySelector(".dropzone-success");
					const msg = document.createElement("div");
					msg.innerHTML = "<strong>" + image.src + "</strong> - " + response.message;
					success.appendChild(msg);

					// remove after 2 seconds so that animation finishes
	    			setTimeout(() => {
						msg.remove();
					}, 2000);

					// remove the div
					imageContainer.remove();

				}

				if(response.status == "error") {

					// create error message
					const errors = dropzoneContainer.querySelector(".dropzone-error");
					const msg = document.createElement("div");
					msg.innerHTML = "<strong>" + image.src + "</strong> - " + response.message;
					errors.appendChild(msg);

				}

			});
			xhr.open("POST", uploaderDir + "index.php?action=delete", true);
			xhr.send(data);

		}

	});

	// select button
	let selectButton = false;
	if(inputId) {

		selectButton = document.createElement("button");
		selectButton.classList.add("dropzone-images__select");
		selectButton.innerHTML = "Select";
		selectButton.addEventListener("click", function(event) {
			
			// prevent default
			event.preventDefault();

			// get dropzone container and upload form
			const dropzone = findDropzoneContainer(this);
			const dropzoneUploadForm = dropzone.querySelector(".dropzone");

			// setup ckeditor num
			let ckeditorNum = false;
			if(dropzoneUploadForm.dataset.ckeditorNum !== null
			&& dropzoneUploadForm.dataset.ckeditorNum !== undefined
			&& dropzoneUploadForm.dataset.ckeditorNum != "") {
				ckeditorNum = dropzoneUploadForm.dataset.ckeditorNum;
			}

			if(ckeditorNum) {
				// console.log(image.folder);
				// console.log(image.src);
				window.opener.CKEDITOR.tools.callFunction(ckeditorNum, "uploads/" + image.folder + image.src);
    			window.close();
				return;
			}

			// log window that opend this shit
			const input = window.opener.document.getElementsByName(inputId)[0];

			// set the value
			input.value = image.id;

			// get image container
			const imageContainer = input.parentNode.parentNode.querySelector(".image-container");

			// clear prev nodes
			while (imageContainer.firstChild) { imageContainer.removeChild(imageContainer.firstChild); }

			// check if image or something else
			if(image.alt_tags !== false) {

				// create a new img tag
				const imageNew = document.createElement("img");
				imageNew.src = image.full_url;
				imageContainer.appendChild(imageNew);
				
			} else {

				// add src to the container
				imageContainer.innerHTML = "<span>" + image.src + "</span>";

			}

			// create a new change event
			const inputChangeEvent = new Event('change');
			input.dispatchEvent(inputChangeEvent);
			
			// close the window
			window.close();

		});

	} // -- end select button

	// create container for all file info blocks
	const file_info = document.createElement("div");
	file_info.appendChild(date);
	file_info.appendChild(title);
	file_info.appendChild(deleteButton);
	if(selectButton) { file_info.appendChild(selectButton); }

	// apend to main info
	info.appendChild(file_info);

	// append to the result
	result.appendChild(info);

	/*
		
		* Alt tag form
			* inputs
			* update button

	*/

	if(image.alt_tags !== false) {

		// set the count of alt tags
		const altTagCount = Object.keys(image.alt_tags).length;

		// update text before the form
		// this is also used for success/error message for alt tags
		const updateText = document.createElement("div");
		updateText.classList.add("dropzone-images__message");

		// set title based on count
		if(altTagCount == 1) {
			updateText.innerHTML = 'Update Image ALT tag:';
		} else {
			updateText.innerHTML = 'Update Image ALT tags:';
		}

		// create alt tag form
		const altTagForm = document.createElement("form");
		altTagForm.action = "#";
		altTagForm.method = "post";
		altTagForm.addEventListener("submit", function(event) {
			
			// prevent default action
			event.preventDefault();

			// setup form and data
			const form = this;
			const data = new FormData(this);
			data.append("id", image.id);
			data.append("parent", image.parent);
			data.append("module", image.module);
			data.append("folder", image.folder);

			// send get request for files
			const xhr = new XMLHttpRequest;
			xhr.addEventListener("load", function(){

				// set response as json
				const response = JSON.parse(this.response);

				// set response
				const msg = document.createElement("div");
				msg.classList.add("dropzone-images__update");
				msg.classList.add("dropzone-images__update--" + response.status)
				msg.innerHTML = response.message;

				updateText.appendChild(msg);

				setTimeout(() => {
					msg.remove();
				}, 3000);


			});
			xhr.open("POST", uploaderDir + "index.php?action=update", true);
			xhr.send(data);

		});

		Object.keys(image.alt_tags).forEach(function(tag) {

			// input containing div
			const inputContainer = document.createElement("div");
			inputContainer.classList.add("dropzone-images__inputs");

			// label for the input only if more then one alt tag
			if(altTagCount > 1) {
				const inputLabel = document.createElement("span");
				inputLabel.innerHTML = tag.replace("alt_", "");
				inputContainer.appendChild(inputLabel);
			}

			// the input
			const input = document.createElement("input");
			input.type = "text";
			input.name = tag;
			input.value = image.alt_tags[tag];

			inputContainer.appendChild(input);
			altTagForm.appendChild(inputContainer);

		});

		// create update alt tags button
		const updateAltButton = document.createElement("button");
		updateAltButton.innerHTML = "Update ALT tags";
		updateAltButton.classList.add("dropzone-images__submit");
		altTagForm.appendChild(updateAltButton);

		// append
		result.appendChild(updateText);
		result.appendChild(altTagForm);

	} // end alt tags

	/*
		
		* return

	*/

	return result;

};





/* ------------------------------

	Dropdown to resize
	or not resize images

------------------------------ */

function dropzoneSetResizeValue(select) {

	// get the dropzone
	const dropzone = findDropzoneContainer(select);

	// get the input
	const resizeInput = dropzone.querySelector('input[name="resize"]');

	// set the value
	resizeInput.value = select.value;

};





/* ------------------------------

	Sortable
	https://github.com/SortableJS/Sortable

------------------------------ */

function dropzoneSortable(button) {

	// get the dropzone and button container
	const dropzoneContainer = findDropzoneContainer(button);
	const buttonContainer = button.parentNode;

	// enable
	dropzoneContainer.classList.add("dropzone-sortable");

	const images = dropzoneContainer.querySelector(".dropzone-images");

	// List with handle
	const sortable = Sortable.create(images, {
		animation: 500
	});

	// hide the button
	button.style.display = 'none';

	// update button
	const update = document.createElement("button");
	update.innerHTML = 'Update Order';
	update.classList.add("button", "color-ok-bg", "margin-r");
	buttonContainer.appendChild(update);

	// event listener on updated
	update.addEventListener("click", function(){

		// setup dir of uploader
		const uploaderDir = "uploader/";

		// hide the buttons
		update.style.display = 'none';
		cancel.style.display = 'none';

		// create spinner
		const spinner = document.createElement("div");
		spinner.classList.add("dropzone-updating-spinner", "color-ok-bg");
		buttonContainer.appendChild(spinner);

		// create updating text
		const spinner_info = document.createElement("div");
		spinner_info.classList.add("color-one-text");
		spinner_info.innerHTML = "Updating...";
		buttonContainer.appendChild(spinner_info);

		// add overlay to images while loading active
		images.classList.add("dropzone-images--updating");

		const clearLoading = function() {
			images.classList.remove("dropzone-images--updating");
			spinner.remove();
			spinner_info.remove();
		};

		// get the images
		const imagesUpdateList = images.querySelectorAll(".dropzone-images__item");
		const imagesData = [];
		imagesUpdateList.forEach(function(image, index) {
			const imageInfo = { item_order: index + 1, image_id: image.dataset.id };
			imagesData.push(imageInfo);
		});

		// get the input values from the dropzone
		const galleryParentInput = dropzoneContainer.querySelector("input[name=parent]");
		const galleryModuleInput = dropzoneContainer.querySelector("input[name=module]");
		const galleryFolderInput = dropzoneContainer.querySelector("input[name=folder]");
			
		// setu send data
		const data = new FormData();
		data.append("parent", galleryParentInput.value);
		data.append("module", galleryModuleInput.value);
		data.append("folder", galleryFolderInput.value);
		data.append("images", JSON.stringify(imagesData));

		// send post to delete image
		const xhr = new XMLHttpRequest;
		xhr.addEventListener("load", function(){

			// set response as json
			const response = JSON.parse(this.response);

			if(response.status == "success") {

				// all good

				// remove loading
				clearLoading();
				
				// remove class
				dropzoneContainer.classList.remove("dropzone-sortable");

				// destroy sortable
				sortable.destroy();

				// remove the buttons
				update.remove();
				cancel.remove();

				// add back button
				button.removeAttribute("style");

				// create success message
				outputStatusMessage(dropzoneContainer, {
					status: response.status,
					text: 	response.message
				});

			}

			if(response.status == "error") {

				// create error message
				outputStatusMessage(dropzoneContainer, {
					status: response.status,
					text: 	response.message
				});

				// add back buttons
				update.removeAttribute("style");
				cancel.removeAttribute("style");

				// remove loading
				clearLoading();

			}

		});
		xhr.open("POST", uploaderDir + "index.php?action=update-order", true);
		xhr.send(data);

	});

	const cancel = document.createElement("button");
	cancel.innerHTML = 'Cancel';
	cancel.classList.add("button", "color-cancel-bg");
	cancel.addEventListener("click", function(){

		// remove class
		dropzoneContainer.classList.remove("dropzone-sortable");

		// destroy sortable
		sortable.destroy();

		// remove buttons
		update.remove();
		cancel.remove();

		// add back button
		button.removeAttribute("style");

	});
	buttonContainer.appendChild(cancel);

};





/* ------------------------------

	Select & Remove Buttons

------------------------------ */

document.addEventListener('DOMContentLoaded', function() {

	/***
	 * Select Image Buttons
	*/

	// get all the select buttons
	const selectImageButtons = document.querySelectorAll(".select-image");

	// setup select buttons
	selectImageButtons.forEach(function(selectImage){

		// add click listener
		selectImage.addEventListener("click", function(event){

			// prevent default action
			event.preventDefault();

			// get the parent div
			const parent = this.parentNode;

			// setup input
			const input = parent.querySelector(".image-input input");
			const inputid = input.name;

			// get the folder
			const folder = this.dataset.folder;

			// open window
			window.open(
				"uploader/popup.php?folder="+folder+"&inputid="+inputid,
				"_blank",
				"height=700,width=1000,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes"
			);

		});

	});

	/***
	 * Remove Image Buttons
	*/

	// get all the remove buttons
	const removeImageButtons = document.querySelectorAll(".remove-image");

	// setup remove buttons
	removeImageButtons.forEach(function(removeImage){

		// add click listener
		removeImage.addEventListener("click", function(event){

			// prevent default action
			event.preventDefault();

			// get elements
			const parent = this.parentNode.querySelector(".image-container");
			const input = this.parentNode.querySelector(".image-input input");

			parent.innerHTML = '';
			input.value = '';

			// Create a new 'change' event
			const changeEvent = new Event('change');

			// Dispatch it.
			input.dispatchEvent(changeEvent);

		});

	});

});




