<?php

// response function
function outputResponse($response) {
	// set header
	header('Content-type: application/json; charset=utf-8');
	// encode json
	$response = json_encode($response);
	// output
	echo $response; exit;
}

?>