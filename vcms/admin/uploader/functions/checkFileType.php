<?php

function checkFileType($file, $extension, $filetypes) {

	// result
	$result = false;

	// lower case extension
	$extension = strtolower($extension);

	// set jpeg to be jpg
	if($extension == "jpeg") { $extension = "jpg"; }

	// first check if the extension is good
	if(!in_array($extension, $filetypes)) {
		return "This file extension is not supported! ($extension)";
	}
	
	// get the mime type with finfo
	$check_mime = new finfo();
	$check_mime = $check_mime->file($file, FILEINFO_MIME_TYPE);

	// lets do some more advance check up
	foreach ($filetypes as $type) {

		// images
		if($type == "jpg" || $type == "png" || $type == "gif") {

			// we support
			$mimetypes = array(
				"jpg" => "image/jpeg",
				"png" => "image/png",
				"gif" => "image/gif"
			);

			if($extension == $type) {
				// check default mimetype
				if($check_mime !== $mimetypes[$extension]) {
					return "Extension (.$extension) doesn't mach file type - " . $check_mime;
				}
				// check mimetype form exif data
				if(filesize($file) > 11) {
					// 1 IMAGETYPE_GIF
					// 2 IMAGETYPE_JPEG
					// 3 IMAGETYPE_PNG
					$mimetype = exif_imagetype($file);
					if($mimetype != 1 && $mimetype != 2 && $mimetype != 3) {
						return "This file type is not supported! (" . $check_mime . ")";
					}
				} else {
				    return "This file type is not supported! (" . $check_mime . ")";
				}
				$result = $type;
			}
		}

		// svg
		if($type == "svg" && $extension == "svg") {
			if($check_mime !== 'image/svg+xml' && $check_mime !== 'image/svg') {
				return "This file type is not supported! (" . $check_mime . ")";
			}
			$result = "svg";
		}

		// zip
		if($type == "zip" && $extension == "zip") {
			
			if($check_mime !== 'application/zip') {
				return "This file type is not supported! (" . $check_mime . ")";
			}

			// todo - reading the file and check
			// if it's a zip or not
			// https://stackoverflow.com/questions/9098678/check-if-a-file-is-archive-zip-or-rar-using-php

			// open file
			// $fielOpen = @fopen($file, "r");

			// if (!$fielOpen) {
			// 	return false;
			// }

			// $fileContent = fgets($fielOpen, 5);

			// if(strpos($fileContent, 'PK') === 0) {
			// 	return false;
			// }

			$result = "zip";

		}

		// pdf
		if($type == "pdf" && $extension == "pdf") {
			
			if($check_mime !== 'application/pdf') {
				return "This file type is not supported! (" . $check_mime . ")";
			}

			$result = "pdf";

		}

		// pdf
		if($type == "mp4" && $extension == "mp4") {
			
			if($check_mime !== 'video/mp4') {
				return "This file type is not supported! (" . $check_mime . ")";
			}

			$result = "mp4";

		}

		// pdf
		if($type == "webp" && $extension == "webp") {
			
			if($check_mime !== 'image/webp') {
				return "This file type is not supported! (" . $check_mime . ")";
			}

			$result = "webp";

		}

		// pdf
		if($type == "xls" && $extension == "xls") {
			
			if($check_mime !== 'application/msexcel'
			&& $check_mime !== 'application/vnd.ms-excel') {
				return "This file type is not supported! (" . $check_mime . ")";
			}

			$result = "xls";

		}

		// pdf
		if($type == "xlsx" && $extension == "xlsx") {
			
			if($check_mime !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
				return "This file type is not supported! (" . $check_mime . ")";
			}

			$result = "xlsx";

		}

	}

	return $result;

}

?>