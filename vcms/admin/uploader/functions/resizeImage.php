<?php

// https://stackoverflow.com/a/14649689

function resizeImage($file, $type, $resizeWidth = 1200, $resizeHeight = 630) {

	// end this if it's not suported type
	if(!in_array($type, array("jpg","gif","png"))) {
		return false;
	}

	// get the current sizes
	list($currentWidth, $currentHeight) = getimagesize($file);

	// check if landscape or portrait
	if($currentHeight > $currentWidth) {
		// portrait ->
		// get the old target size
		$oldTargetWidth = $resizeWidth;
		$oldTargetHeight = $resizeHeight;
		// and flip it
		$resizeWidth = $oldTargetHeight;
		$resizeHeight = $oldTargetWidth;
	}

	// check the witdth and height
	if($currentWidth != $resizeWidth
	|| $currentHeight != $resizeHeight) {

		// calculate the new height
		$newHeight = floor($resizeWidth * $currentHeight / $currentWidth);

		// new width is set to target width
		$newWidth = $resizeWidth;

		// now check the calcuated height
		// it has to be equal or larger then target
		if($newHeight < $resizeHeight) {

			// recalculate the width
			$newWidth = floor($resizeHeight * $resizeWidth / $newHeight);
			
			// set newHeight to target height
			$newHeight = $resizeHeight;

		}

		// get the current image
		if($type == "jpg") {
			$src = imagecreatefromjpeg($file);
		} else if($type == "gif") {
			$src = imagecreatefromgif($file);
		} else if($type == "png") {
			$src = imagecreatefrompng($file);
		}

		// scale the image to the new calculated size
		$src = imagescale($src, $newWidth);
		$src_w = $newWidth;
		$src_h = $newHeight;

		// calculate the middle for the image
		if($newWidth == $resizeWidth) {
			$src_x = 0;
			$src_y = ( $newHeight - $resizeHeight ) / 2;
		} else {
	    	$src_x = ( $newWidth - $resizeWidth ) / 2;
	    	$src_y = 0;	
		}

		// create a new canavas using target size
    	$dst = imagecreatetruecolor($resizeWidth, $resizeHeight);
    	$dst_x = 0;
    	$dst_y = 0;
    	$dst_w = $resizeWidth;
    	$dst_h = $resizeHeight;

    	// preserve alpha aka transparancy
    	if($type == "png") {
			imagealphablending($dst, false);
			imagesavealpha($dst, true);
    	}

    	// copy resized image to the new canavas
    	imagecopy(
    		$dst, $src,
    		$dst_x, $dst_y,
    		$src_x, $src_y, $src_w, $src_h
    	);
    	
    	// save the file
    	if($type == "jpg") {
			imagejpeg($dst, $file, 90);
		} else if($type == "gif") {
			imagegif($dst, $file, 90);
		} else if($type == "png") {
			imagepng($dst, $file);
		}

	}

    return true;

}

?>