<?php

// ------------------------------------
// -- users
// ------------------------------------

global $CMS_ADMIN_PAGE;
$CMS_ADMIN_PAGE = 1;

// includes
require_once(dirname(dirname(__FILE__)) . "/include.php");
require_once(dirname(dirname(__FILE__)) . "/lib/classes/class.user.inc.php");

// login
vcmsAuth::is_loged_in();

// ------------------------------------
// -- access
// ------------------------------------

$access_add_remove = check_permission(get_userid(), 'users_add_remove');
$access = $access_add_remove || check_permission(get_userid(), 'users_add_remove');

// ------------------------------------
// -- user ops
// ------------------------------------

// load operations
global $gCms;
$operations = $gCms->GetUserOperations();

// load users
$itemlist = $operations->LoadUsers();

// ------------------------------------
// -- setup add button
// ------------------------------------

$add_button = '';
$add_button_top = false;
$add_button_bottom = false;

// check for access to add users
if($access_add_remove) {

	// add button
	$add_button = get_html(array(
		'link'		=> 'users_add_edit.php',
		'text'		=> lang('users_add'),
		'template'	=> VCMS_PATH . '/admin/templates/button-ok.html'
	));

	// show the button
	$add_button_top = true;
	$add_button_bottom = true;

}

// ------------------------------------
// -- setup list cells
// ------------------------------------

$cells = array(
	array(
		"key"    => "edit_link",
		"action" => "edit",
		"label"  => lang("user"),
		"class"  => "  list__cell--fill  list__cell--first"
	),
	array(
		"key"    => "fullname",
		"action" => "fullname",
		"label"  => lang("name") . ' ' . lang('surname'),
		"class"  => "  list__cell--fill"
	),
	array(
		"key"    => "email",
		"action" => "email",
		"label"  => lang("email"),
		"class"  => "  list__cell--fill"
	),
	array(
		"key"    => "edit",
		"action" => "edit",
		"label"  => lang("edit"),
		"class"  => "  list__cell--icon"
	),
	array(
		"key"    => "toggle_active",
		"action" => "active",
		"label"  => lang("active"),
		"class"  => "  list__cell--icon"
	)
);

// can we delete users?
if($access_add_remove) {
	$cells[] = array(
		"key"    => "delete",
		"action" => "delete",
		"label"  => lang("delete"),
		"class"  => "  list__cell--icon"
	);
}

// ------------------------------------
// -- process list
// ------------------------------------

include_once(get_vcms_file("/admin/list.php"));





// -- users.php