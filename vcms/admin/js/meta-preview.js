/* ------------------------------

	On load

------------------------------ */

// onload setup keydown listeners
window.addEventListener('load', function() {

	/* ------------------------------

		Return the title

	------------------------------ */

	const getTitle = function(meta_title, defaul_title, seperator, site_name) {

		// set the var
		let result;

		// check if meta title is set
		if(meta_title != "") {
			result = meta_title;
		} else {
			result = defaul_title + 
					 ( seperator ? ' ' + seperator + ' ' : '' ) +
					 ( site_name ? site_name : '' );
		}

		return result;

	}

	/* ------------------------------

		Setup google preview

	------------------------------ */

	// get all the previews
	const googlePreviews = document.querySelectorAll(".google-search-preview");

	// set them up
	googlePreviews.forEach(function(google) {

		// get site name and seperator
		const site_name = google.dataset.siteName;
		const seperator = google.dataset.seperator;
		
		// get the inputs
		const title = document.querySelector("[name='"+google.dataset.defaultTitle+"']");
		const meta_title = document.querySelector("input[name='"+google.dataset.metaTitle+"']");
		const meta_description = document.querySelector("textarea[name='"+google.dataset.metaDescription+"']");
		const page_alias = document.querySelector("input[name='"+google.dataset.alias+"']");

		// create divs
		const meta_title_preview = document.createElement("div");
		const meta_url_preview = document.createElement("div");
		const meta_description_preview = document.createElement("div");
		const meta_seperator_line = document.createElement("div");
		const meta_title_count = document.createElement("div");
		const meta_description_count = document.createElement("div");
		const meta_description_info = document.createElement("div");
		meta_title_preview.classList.add("google-search-preview__title");
		meta_url_preview.classList.add("google-search-preview__url");
		meta_description_preview.classList.add("google-search-preview__description");
		meta_seperator_line.classList.add("google-search-preview__seperator");
		meta_title_count.classList.add("google-search-preview__lenght");
		meta_description_count.classList.add("google-search-preview__lenght");
		meta_description_info.classList.add("google-search-preview__info");

		// append them
		google.appendChild(meta_title_preview);
		google.appendChild(meta_url_preview);
		google.appendChild(meta_description_preview);
		google.appendChild(meta_seperator_line);
		google.appendChild(meta_title_count);
		google.appendChild(meta_description_count);
		google.appendChild(meta_description_info);

		// set the url
		meta_url_preview.innerHTML = google.dataset.url + page_alias.value;

		// info about meta desc
		meta_description_info.innerHTML = 'If Description tag is not set Google will fill it automaticly based on the page content. Description and Keywords meta tags are not used in web rankings (since 2009).';

		// set the title
		const setMetaTitle = function(){
			const new_meta_title = getTitle(
				meta_title.value,
				title.value,
				seperator,
				site_name
			);
			meta_title_preview.innerHTML = new_meta_title;
			meta_title_count.innerHTML = 'Title length: ' + new_meta_title.length + ' (Max ~ 70 characters)';
		}

		// set the description
		const setMetaDescription = function(){
			meta_description_preview.innerHTML = meta_description.value;
			meta_description_count.innerHTML = 'Description length: ' + meta_description.value.length + ' (Max ~ 160 characters)';
		}

		setMetaTitle();
		setMetaDescription();

		// event listener for the meta title
		meta_title.addEventListener('keyup', setMetaTitle);
		meta_title.addEventListener('change', setMetaTitle);

		// event listener for the default title
		title.addEventListener('keyup', setMetaTitle);
		title.addEventListener('change', setMetaTitle);

		// event listener for the meta description
		meta_description.addEventListener('keyup', setMetaDescription);
		meta_description.addEventListener('change', setMetaDescription);

	});

	/* ------------------------------

		Setup facebook preview

	------------------------------ */

	// get all the previews
	const facebookPreviews = document.querySelectorAll(".facebook-share-preview");

	// set them up
	facebookPreviews.forEach(function(facebook) {

		// get site name and seperator
		const site_name = facebook.dataset.siteName;
		const seperator = facebook.dataset.seperator;
		
		// get the text inputs
		const title = document.querySelector("[name='"+facebook.dataset.defaultTitle+"']");
		const meta_title = document.querySelector("input[name='"+facebook.dataset.metaTitle+"']");
		const facebook_title = document.querySelector("input[name='"+facebook.dataset.facebookTitle+"']");
		const facebook_description = document.querySelector("textarea[name='"+facebook.dataset.facebookDescription+"']");

		// get the image inputs
		const facebook_image_input = document.querySelector("input[name='"+facebook.dataset.facebookImage+"']");
		const default_image_input = document.querySelector("input[name='"+facebook.dataset.defaultImage+"']");

		// create divs
		const facebook_s_preview = document.createElement("div"); // share div
		const facebook_h_preview = document.createElement("div"); // header
		const facebook_i_preview = document.createElement("div"); // image
		const facebook_t_preview = document.createElement("div"); // title
		const facebook_u_preview = document.createElement("div"); // url
		const facebook_d_preview = document.createElement("div"); // description
		const facebook_b_preview = document.createElement("div"); // debug
		facebook_s_preview.classList.add("facebook-share-preview__share");
		facebook_h_preview.classList.add("facebook-share-preview__header");
		facebook_i_preview.classList.add("facebook-share-preview__image");
		facebook_u_preview.classList.add("facebook-share-preview__url");
		facebook_t_preview.classList.add("facebook-share-preview__title");
		facebook_d_preview.classList.add("facebook-share-preview__description");
		facebook_b_preview.classList.add("facebook-share-preview__debug");

		// append them
		facebook_s_preview.appendChild(facebook_h_preview);
		facebook_s_preview.appendChild(facebook_i_preview);
		facebook_s_preview.appendChild(facebook_u_preview);
		facebook_s_preview.appendChild(facebook_t_preview);
		facebook_s_preview.appendChild(facebook_d_preview);
		facebook_s_preview.appendChild(facebook_b_preview);
		facebook.appendChild(facebook_s_preview);

		// set the url
		facebook_u_preview.innerHTML = facebook.dataset.url;
		
		// debugger text
		facebook_h_preview.innerHTML = '<span>' + site_name + '</span><span>Facebook Share</span>';

		// debugger text
		facebook_b_preview.innerHTML = 'Use <a href="https://developers.facebook.com/tools/debug/" target="_blank">Facebook Debugger</a> for more acurate results.';

		// set the title
		const setFacebookTitle = function(){
			const new_meta_title = getTitle(
				facebook_title.value,
				meta_title.value == '' ? title.value : meta_title.value,
				meta_title.value == '' ? seperator : false,
				meta_title.value == '' ? site_name : false
			);
			facebook_t_preview.innerHTML = new_meta_title.replace(/(<([^>]+)>)/ig, " ");
		}

		// set the description
		const setFacebookDescription = function(){
			if(facebook_description === null || facebook_description === undefined) {
				facebook_d_preview.innerHTML = '';
			} else {
				facebook_d_preview.innerHTML = facebook_description.value;
			}
		}

		const setFacebookImage = function(){

			// get the facebook img tags
			const facebook_image = document.querySelector("." + facebook.dataset.facebookImage + " img");

			// get the default image based on input
			let default_image = document.querySelector('[name="' + facebook.dataset.defaultImage + '"]');
			if(default_image !== undefined && default_image !== null) {
				default_image = default_image.parentNode.parentNode;
				default_image = default_image.querySelector("img");
			}

			// check if there is default image
			const default_img = default_image !== undefined && default_image !== null ? default_image.src : "";

			// set the value
			const image = facebook_image !== null && facebook_image !== undefined ? facebook_image.src : default_img;

			// update background of the preview
			if(image != '') {
				facebook_i_preview.style.backgroundImage = 'url(' + image + ')';
				facebook_i_preview.classList.remove("facebook-share-preview__image--no-image");
				facebook.classList.remove("facebook-share-preview--no-image");
			} else {
				facebook_i_preview.removeAttribute("style");
				facebook_i_preview.classList.add("facebook-share-preview__image--no-image");
				facebook.classList.add("facebook-share-preview--no-image");
			}

		}

		// set shit up on load
		setFacebookTitle();
		setFacebookDescription();
		setFacebookImage();

		// event listener for meta title
		meta_title.addEventListener('keyup', setFacebookTitle);
		meta_title.addEventListener('change', setFacebookTitle);

		// event listener for facebook title
		facebook_title.addEventListener('keyup', setFacebookTitle);
		facebook_title.addEventListener('change', setFacebookTitle);

		// event listener for the default title
		title.addEventListener('keyup', setFacebookTitle);
		title.addEventListener('change', setFacebookTitle);

		// event listener for description
		if(facebook_description !== null && facebook_description !== undefined) {
			facebook_description.addEventListener('keyup', setFacebookDescription);
			facebook_description.addEventListener('change', setFacebookDescription);
		}

		// event listeners for images
		if(facebook_image_input !== undefined && facebook_image_input !== null) facebook_image_input.addEventListener('change', setFacebookImage);
		if(default_image_input !== undefined && default_image_input !== null) default_image_input.addEventListener('change', setFacebookImage);

	});

});




