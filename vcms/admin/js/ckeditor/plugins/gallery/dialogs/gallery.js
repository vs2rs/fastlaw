CKEDITOR.dialog.add( 'galleryDialog', function( editor ) {
    
    return {

        title: 'Insert Gallery',
        minWidth: 400,
        minHeight: 200,
        contents: [
            {
                id: 'tab-basic',
                label: 'Basic Settings',
                elements: [
                    {
                        type: 'text',
                        id: 'code',
                        label: 'Paste Gallery Code:',
                        validate: CKEDITOR.dialog.validate.notEmpty("Gallery Code cannot be empty."),

                        setup: function( element ) {
                            this.setValue(element.getText());
                        },

                        commit: function( element ) {
                            element.setText(this.getValue());
                        }
                    }
                ]
            }
        ],

        onShow: function() {

            // get the element
            let element = editor.getSelection().getStartElement();

            // check if we edit or insert
            if (!element || !element.hasClass('code')) {
                // create the element
                element = editor.document.createElement('div');
                element.addClass("code");
                // set the insert mode to true
                this.insertMode = true;
            } else {
                // we editing
                this.insertMode = false;
            }

            // set to default
            this.element = element;

            // if we edit run the setup
            if (!this.insertMode) {
                this.setupContent(this.element);
            }

        }, // end onShow

        onOk: function() {

            // get the dialog
            const dialog = this;

            // run the commit function on the element
            const galleryDiv = this.element;
            this.commitContent(galleryDiv);

            // if insert then we add it to editor
            if (this.insertMode) {
                editor.insertElement(galleryDiv);
            }

        } // end onOk

    }; // end return

});