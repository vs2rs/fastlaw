// https://ckeditor.com/docs/ckeditor4/latest/guide/plugin_sdk_sample_1.html
// icon: https://www.flaticon.com/free-icon/edit-image_1666551#term=image&page=1&position=65

CKEDITOR.plugins.add( 'gallery', {
    icons: 'gallery', // #4e4e4e
    init: function( editor ) {

        /**

         * Main definitions

        */

        // create the button for the toolbar
        editor.ui.addButton( 'Gallery', {
            label: 'Insert Gallery',
            command: 'gallery',
            toolbar: 'insert'
        });
        
        // insert dialog command
        editor.addCommand( 'gallery', new CKEDITOR.dialogCommand( 'galleryDialog' ) );
        
        // remove gallery command
        editor.addCommand( 'gallery_remove', {
            exec : function( editor ) {
                const element = editor.getSelection().getStartElement();
                element.remove();
            }
        });

        // set the dialog script
        CKEDITOR.dialog.add( 'galleryDialog', this.path + 'dialogs/gallery.js' );

        /**

         * Context menu ->
         * Right click to edit and remove gallery

        */

        // check if it's there
        if(editor.contextMenu) {
            
            // create the group to add buttons to
            editor.addMenuGroup('galleryGroup');

            // edit gallery
            editor.addMenuItem( 'editGallery', {
                label: 'Edit Gallery',
                icon: this.path + 'icons/gallery.png',
                command: 'gallery',
                group: 'galleryGroup'
            });

            // remove gallery
            editor.addMenuItem( 'removeGallery', {
                label: 'Remove Gallery',
                icon: this.path + 'icons/gallery.png',
                command: 'gallery_remove',
                group: 'galleryGroup'
            });

            // listener for edit
            editor.contextMenu.addListener( function( element ) {
                if(element.hasClass('code')) {
                    return { editGallery: CKEDITOR.TRISTATE_OFF };
                }
            });

            // listener for remove
            editor.contextMenu.addListener( function( element ) {
                if(element.hasClass('code')) {
                    return { removeGallery: CKEDITOR.TRISTATE_OFF };
                }
            });

            // not sure if there is an options to combine these listeners
            // can't find anything about it in documentation

        }

    }
});