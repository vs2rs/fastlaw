/**
 * Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

// This file contains style definitions that can be used by CKEditor plugins.
//
// The most common use for it is the "stylescombo" plugin, which shows a combo
// in the editor toolbar, containing all styles. Other plugins instead, like
// the div plugin, use a subset of the styles on their feature.
//
// If you don't have plugins that depend on this file, you can simply ignore it.
// Otherwise it is strongly recommended to customize this file to match your
// website requirements and design properly.

CKEDITOR.stylesSet.add( 'default', [

	/* H2 */

	{ name: 'H2 (large)',			element: 'h2', attributes: { 'class': 'text-large' } },
	{ name: 'H2 (medium) <hr/>',			element: 'h2', attributes: { 'class': 'text-medium' } },

	/* H3 */

	{ name: 'H3 (large)',			element: 'h3', attributes: { 'class': 'text-large' } },
	{ name: 'H3 (medium) <hr/>',			element: 'h3', attributes: { 'class': 'text-medium' } },

	/* Text */
	
	{ name: 'Text (medium)',		element: 'p', attributes: { 'class': 'text-medium' } },
	{ name: 'Text (default)',		element: 'p', attributes: { 'class': 'text-default' } },
	{ name: 'Text (small)',			element: 'p', attributes: { 'class': 'text-small' } },

	/* colors */

	{ name: 'Beetroot Red',			element: 'span', attributes: { 'class': 'color-main-text' } },

	/* code */

	{ name: 'Gallery Code',			element: 'div', attributes: { 'class': 'code' } }


] );

