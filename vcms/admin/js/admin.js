/* ------------------------------

	Find parent element
	by class name

------------------------------ */

function findParentFromClass(element, parent_class) {
    while ((element = element.parentElement) && !element.classList.contains(parent_class));
    return element;
}

function removeChildren(element) {
	while (element.lastChild) {
		element.removeChild(element.lastChild);
	}
}

/* ------------------------------

	$_GET

------------------------------ */

function getData(url, callback, debug = false) {

	// add debug to url
	url = url + (debug ? '&debug=1' : '');

	// do the post
	const xhr = new XMLHttpRequest;

	// on load attach function
	xhr.addEventListener("load", function(){

		// todo xhr.status == 200 etc.
		// https://javascript.info/xmlhttprequest

		// if we debuging
		if(debug) {

			// url
			console.log(url);
			
			// log response
			console.log(this.response);

			// end the script
			return;

		}

		// set response as json
		const response = JSON.parse(this.response);

		// if success do the callback
		if(response.status == "success") {
			callback(response);
		}

		// if error then alert it
		if(response.status == "error") {
			alert(response.message);
		}

	});

	// open
	xhr.open('GET', url);

	// send
	xhr.send();

}

/* ------------------------------

	Seting status:
	- default
	- active

------------------------------ */

// update icons funciont
function updateIcon(element, name, html) {

	// get the cells
	const cell = element.querySelector('[data-action="' + name + '"]');

	// check for cell in dom
	if(cell !== null) {

		// remove children
		removeChildren(cell);

		// insert teh new html
		cell.innerHTML = html;

	}

};

// update icons funciont
function changeIcon(list, item) {

	// get the parent row
	const row = list.querySelector('[data-id="' + item.id + '"]');

	// check for row
	if(row !== null) {

		// if we are changing active state
		if(item.status !== null && item.action == 'active') {

			// add or remove the off class
			if(item.status) {
				row.classList.remove("list__item--off");
			} else {
				row.classList.add("list__item--off");
			}

		}

		// get the cells
		const cell = row.querySelector('[data-action="' + item.action + '"]');

		// check for cell in dom
		if(cell !== null) {

			// remove children
			removeChildren(cell);

			// insert teh new html
			cell.innerHTML = item.icon;

		}

	}

};

function setDefault() {

	// get current list, row and id
	const list = findParentFromClass(this, "list");

	// send the data
	getData(this.href, (response) => {

		// set t
		response.data.forEach((item) => {

			Object.keys(item.icons).forEach((icon_name) => {

				// html
				const icon_html = item.icons[icon_name];

				// run the update function
				updateIcon(
					list.querySelector('[data-id="' + item.id + '"]'),
					icon_name,
					icon_html
				);

			});

		});

	});

};

function toggleActive() {

	// send the data
	getData(this.href, (response) => {

		// get the parent row
		const parent = findParentFromClass(this, "list__item");

		// add class if unactive
		if(response.active_status) {
			parent.classList.remove("list__item--off");
		} else {
			parent.classList.add("list__item--off");
		}
		
		// update view
		updateIcon(parent, 'view', response.view);

		// update active icon
		this.insertAdjacentHTML('afterend', response.active);
		this.remove();

	});

};

function doAction() {

	// send the data
	getData(this.href, (response) => {

		// console.log(response);

		// close any open messages
		messageAllClose();

		// check for icons
		if(response.icons !== undefined
		&& response.icons !== null
		&& response.icons.length > 0) {

			// get the current list we working on
			const list = findParentFromClass(this, "list");

			// go thorugh icons and update them in dom
			response.icons.forEach((item) => changeIcon(list, item));

		}

		// check for delete
		if(response.deleted !== undefined
		&& response.deleted !== null
		&& response.deleted) {

			// get the current list we working on
			const row = findParentFromClass(this, "list__item");

			// remove
			row.remove();

		}

	});

};

/* ------------------------------

	Expanding a list

------------------------------ */

function expandList() {

	// get the parent row
	const parent = findParentFromClass(this, 'list__item');

	// set the class name
	const expnadClassName = 'list__item--expanded';

	// default expand value
	let expand = 0;

	// check the class and update it
	if(parent.classList.contains(expnadClassName)) {

		// set the val
		expand = 0;

		// remove the class
		parent.classList.remove(expnadClassName);

	} else {

		// set the val
		expand = 1;

		// add the class
		parent.classList.add(expnadClassName);

	}

	// send the data
	getData(this.href + '&expand=' + expand, (response) => {
		if(response.status == 'error') {
			console.error('Something went wrong while updating expand status in the database.')
		}
	});

}

/* ------------------------------

	Move Up or Down

------------------------------ */

function moveAction(element, action) {

	// get sibling function
	const getSibling = function() {

		let sibling = null;

		// up
		if(action == "up") {

			// get prev
			sibling = this.previousElementSibling;

			// need to check if this is not a group of children
			if(sibling !== null && sibling.classList.contains("list__group")) {
				sibling = sibling.previousElementSibling;
			}

		}

		// down
		if(action == "down") {

			sibling = this.nextElementSibling;

			// need to check if this is not a group of children
			if(sibling !== null && sibling.classList.contains("list__group")) {
				sibling = sibling.nextElementSibling;
			}

		}
		
		return sibling;

	};

	// check for children function
	const hasChildren = function() {
		
		if(this.classList.contains("list__item--has-children")) {
			return this.nextElementSibling;
		}
		
		return false;

	};

	// change class of the last element
	const changeLastClass = function(elementToCheck, elementToChange) {

		// css class name for the last child
		const lalstChildClassName = 'list__item--last-child';

		// if it's last element
		if(elementToCheck.classList.contains(lalstChildClassName)) {

			// change classes
			elementToCheck.classList.remove(lalstChildClassName);
			elementToChange.classList.add(lalstChildClassName);

		}

	};

	// change the element in place
	const changeElements = function(elementMoving, elementStaying) {
		
		// get the parent container
		const parent = elementMoving.parentNode;

		// check for children
		const children = hasChildren.call(elementMoving);

		// do the move
		parent.insertBefore(elementMoving, elementStaying);
		
		// if have children we need to move them too
		if(children) { parent.insertBefore(children, elementStaying); }

		// change the last class
		changeLastClass(elementMoving, elementStaying);

	};

	// get the parent row
	const currentRow = findParentFromClass(element, 'list__item');

	// lets go up
	if(action == 'up') {

		// get the sibling
		const sibling = getSibling.call(currentRow);

		// check for sibling do the change
		if(sibling !== null) {
			changeElements(currentRow, sibling);
		}

	}

	// lets go down
	if(action == 'down') {

		// get the sibling
		const sibling = getSibling.call(currentRow);

		// check for sibling do the change
		if(sibling !== null) {
			changeElements(sibling, currentRow);
		}

	}

}

function moveIconsUpdate(element, response) {

	// get current list, row and id
	const list = findParentFromClass(element, "list");

	// update current icon
	updateIcon(
		list.querySelector('[data-id="' + response.current_id + '"]'),
		"move",
		response.current_icon
	);

	// update sibling icon
	updateIcon(
		list.querySelector('[data-id="' + response.sibling_id + '"]'),
		"move",
		response.sibling_icon
	);

}

function moveUp() {

	// send the data
	getData(this.href, (response) => {
		if(response.status == 'success') {
			moveAction(this, 'up');
			moveIconsUpdate(this, response);
		} else {
			console.error('Something went wrong while moving item.')
		}
	});

}

function moveDown() {

	// send the data
	getData(this.href, (response) => {
		if(response.status == 'success') {
			moveAction(this, 'down');
			moveIconsUpdate(this, response);
		} else {
			console.error('Something went wrong while moving item.')
		}
	});

}

/* ------------------------------

	Navigation for the tabs

------------------------------ */

// onload setup keydown listeners
window.addEventListener('DOMContentLoaded', function() {

	// get the tabs
	const tabContainers = document.querySelectorAll(".tabs");

	// is there tab blocks?
	if(tabContainers !== null
	&& tabContainers.length > 0) {

		// go thorugh blocks
		tabContainers.forEach((tab) => {

			// get the tabs and tab content
			const tabList = tab.querySelector(".tabs__list");
			const tabListItems = tab.querySelectorAll(".tabs__list > div");
			const tabContent = tab.querySelectorAll(".tabs__content > div");

			// is there more then one tab
			if(tabListItems !== null
			&& tabListItems.length > 1) {

				// setup click events
				tabListItems.forEach((button, button_index) => {

					// add the click event
					button.addEventListener("click", function(){

						// remove all actives
						tabListItems.forEach((button) => button.classList.remove("active"));
						tabContent.forEach((content) => content.classList.remove("active"));

						// set this to new active
						this.classList.add("active");

						// update active data attr
						tabList.dataset.active = button_index + 1;

						// need to set active active content
						tabContent.forEach((content, content_index) => {

							// find the match with clicked button
							if(button_index == content_index) {
								content.classList.add("active");
							}

						});

					});

				});

			}

		});

	}

});

/* ------------------------------

	Reset user access checkboxes

------------------------------ */

function resetUserAccessCheckboxes() {

	// get the parent
	const parent = this.parentNode;

	const checkbox = parent.querySelectorAll('input[type="checkbox"]');

	checkbox.forEach((checkbox) => checkbox.checked = false);

}

/* ------------------------------

	Show the password inputs

------------------------------ */

function showPasswordInputs() {

	// get the form and passwords
	const form = findParentFromClass(this, "form");
	const passwordInputs = form.querySelectorAll('input[type="password"]');

	// go through password fields
	passwordInputs.forEach((password) => {

		// get the row
		const row = findParentFromClass(password, "form__row");

		// add class if unactive
		if(row.classList.contains("form__row--hidden")) {

			// remove hidden class
			row.classList.remove("form__row--hidden");
			
			// change button text
			if(this.dataset.hideText !== undefined) {
				this.innerHTML = this.dataset.hideText;
			}

		} else {
			
			// add hidden class
			row.classList.add("form__row--hidden");
			
			// change button text
			if(this.dataset.hideText !== undefined) {
				this.innerHTML = this.dataset.showText;
			}

		}

	});

}

/* ------------------------------

	Update days when we change
	months and years

------------------------------ */

document.addEventListener("DOMContentLoaded", () => {

	// get the selects and check them
	const monthSelects = document.querySelectorAll(".month-select-js");
	if(monthSelects !== null
	&& monthSelects !== undefined
	&& monthSelects.length > 0) {

		// is this year a leap year?
		const leapYear = function(year) {
			return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
		}

		// check days in the month
		const updateDays = function(){

			// set month
			const month = this.value;

			// get the parent and get the year dropdown
			const parent = this.parentNode.parentNode;
			const yearSelect = parent.querySelector(".year-select-js");
			const daysSelect = parent.querySelector(".days-select-js");
			const daysOptions = daysSelect.querySelectorAll("option");

			let days = 31;

			// check year and day selects
			if(yearSelect !== null
			&& yearSelect !== undefined) {

				switch (month) {

					case "02":
						days = leapYear(yearSelect.value) ? 29 : 28;
						break;
					
					case "04":
					case "06":
					case "09":
					case "11":
						days = 30;
						break;

				}

			}

			// days select
			if(daysSelect !== null
			&& daysSelect !== undefined
			&& daysOptions.length != days) {

				// we need to match days with index
				const days_to_index = days - 1;

				// remove unacasary days
				if(daysOptions.length > days) {
					daysOptions.forEach((option, index) => {
						if(index > days_to_index) option.remove();
					});
				}

				// add mising days
				if(daysOptions.length < days) {

					for (var i = daysOptions.length + 1; i <= days; i++) {
						
						// create the option
						const option = document.createElement("option");
						daysSelect.appendChild(option);

						// set the values
						option.value = i;
						option.innerHTML = i;

					}

				}

			}

		};

		monthSelects.forEach((month) => {

			// add listener
			month.addEventListener("change", updateDays);

			// triger change
			month.dispatchEvent(new Event('change'));

		});

	}

	// get the year selects and check them
	const yearSelects = document.querySelectorAll(".year-select-js");
	if(yearSelects !== null
	&& yearSelects !== undefined
	&& yearSelects.length > 0) {

		yearSelects.forEach((yearSelect) => {

			yearSelect.addEventListener("change", function(){

				// get the parent and get the month select
				const parent = this.parentNode.parentNode;
				const monthSelect = parent.querySelector(".month-select-js");

				// triger change on month if month is february
				// this is to check if we are in a leap year
				if(monthSelect !== undefined
				&& monthSelect !== null
				&& monthSelect.value == '02') {
					monthSelect.dispatchEvent(new Event('change'));
				}

			});

		});

	}

});









/* ------------------------------

	Old.....

------------------------------ */

function navigateTab(element) {

	const classContent = "tab-content";
	const classContentActive = classContent + "-active";
	const classTab = "tab-nav";
	const classTabActive = classTab + "-active";

	// get elements
	const newActiveContent = document.getElementById(element.dataset.tab);
	const currentActiveContent = document.getElementsByClassName(classContentActive)[0];
	const currentActiveTab = document.getElementsByClassName(classTabActive)[0];

	// remove classes
	currentActiveContent.classList.remove(classContentActive);
	currentActiveTab.classList.remove(classTabActive);

	// add active classes
	newActiveContent.classList.add(classContentActive);
	element.classList.add(classTabActive);

};

// this is temp way to remove empty tabs for now
document.addEventListener("DOMContentLoaded", () => {

	// get the tabs
	const tabs = document.querySelectorAll(".module-tabs__item");

	// is there tab blocks?
	tabs.forEach((tab) => {
		const tab_content = document.getElementById(tab.dataset.tab);
		if(tab_content === null || tab_content === undefined) {
			tab.remove();
		}
	});
	
});

/* ------------------------------

	Close message

------------------------------ */

function messageClose(element) {

	// update url
	removeMessageParamsFromURL();

	//remove dome element
	element.parentNode.remove();

};

function messageAllClose() {

	// get all messages and remove them
	const messages = document.querySelectorAll(".message");
	messages.forEach((message) => message.remove());

	// clear url params
	removeMessageParamsFromURL();

};

// update URL for close message
function removeMessageParamsFromURL() {

	// parse current url
	const parser = new URL(window.location.href);

	// first check the url for message query param
	// message param is only set after presing submit and not apply
	// we don't want to remove any params if we are still
	// editing current object
	if(parser.search.match(/message=/)) {

		// create an array of query params
		const params = parser.search.split("&");

		// create a new url
		let new_url = parser.origin + parser.pathname;

		// go thorugh params and add to the new url
		params.forEach((param, index) => {
			if(!param.match(/^message=/)
			&& !param.match(/^id=/)
			&& !param.match(/^m1_item_id=/)
			&& !param.match(/^m1_message=/)) {
				new_url = new_url + ( index > 0 ? '&' : '' ) + param;
			}
		});

		// update url without adding it to the history
		if(window.history.replaceState) {
		   window.history.replaceState(null, '', new_url);
		}

	}

};





/* ------------------------------

	Count chars

------------------------------ */

function countChars(el) {

	// get the containers
	const parent = el.parentNode;
	const lengthContainer = parent.querySelector(".module-input-length");

	// set the count
	lengthContainer.innerHTML = el.value.length;

}




