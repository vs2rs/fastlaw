<?php

$CMS_ADMIN_PAGE=1;

require_once($config['root_path'] . "/include.php");
require_once($config['root_path'] . "/lib/misc.functions.php");

global $gCms;
$config = $gCms->GetConfig();

if(isset($_GET["what"]) && isset($_GET["tables"])) {

	$tables = explode(",", $_GET["tables"]);
	$filename = $_GET["what"] . "-" . date('Y-m-d--H-i-s') . ".sql";

	$table_option = '--tables';
	foreach ($tables as $table) {
		$table_option .= " " . cms_db_prefix() . $table;
	}

	$database = $config['db_name'];
	$user = $config['db_username'];
	$pass = $config['db_password'];
	$host = $config['db_hostname'];
	
	// if in need to see errors
	// https://stackoverflow.com/a/40018165
	// add 2>&1 at the end "(mysldump ..) 2>&1"
	if($_SERVER['SERVER_NAME'] == 'localhost') {
		exec("/Applications/MAMP/Library/bin/mysqldump --compact --add-drop-table {$database} {$table_option}", $output);
	} else {
		exec("mysqldump --user={$user} --password={$pass} --host={$host} --compact --add-drop-table {$database} {$table_option}", $output, $result);
	} 

	//--skip-add-drop-table, --skip-add-locks, --skip-comments, --skip-disable-keys, and --skip-set-charset

	header('Content-Type: application/octet-stream');
	header('Content-Transfer-Encoding: Binary'); 
	header('Content-disposition: attachment; filename="' . $filename . '"');

	foreach ($output as $line) {
		echo $line . "\n";
	}

}

?>