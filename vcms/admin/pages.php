<?php

global $CMS_ADMIN_PAGE;
$CMS_ADMIN_PAGE=1;

require_once(dirname(dirname(__FILE__)) . "/include.php");
require_once(dirname(dirname(__FILE__)) . "/lib/classes/class.admintheme.inc.php");

vcmsAuth::is_loged_in();

$userid = get_userid();

$hierarchy = $gCms->GetHierarchyManager();
$db = $gCms->GetDb();
$contentops = $gCms->GetContentOperations();

$content_table = cms_db_prefix() . "content";

// ------------------------------------
// -- array of default page parents
// ------------------------------------

// array of parrents
$default_page_parents = $contentops->GetDefaultContentParents();

// ------------------------------------
// -- set default
// ------------------------------------

if(isset($_GET['setdefault'])) {

	// debug
	$debug = isset($_GET['debug']) && floor($_GET['debug']) == 1 ? true : false;

	// did we succsseed?
	$success = false;

	// ------------------------------------
	// -- remove old default
	// ------------------------------------

	// get the old default element
	$query = "SELECT content_id FROM $content_table WHERE default_content = 1";
	$old_default_id = $db->GetOne($query);

	// array to collect all itmes that need to be updated
	$items_to_update = array();

	// if success
	if(isset($old_default_id)) {
		
		// load it by id
		$old_default = $hierarchy->getNodeById($old_default_id);

		// if it's loaded
		if(isset($old_default)) {

			// get the content
			$old_default_content = $old_default->getContent();

			// if we have content updated default to false
			if(isset($old_default_content)) {

				// update
				$old_default_content->SetDefaultContent(false);
				$old_default_content->Save();

				// add to the array new icons for old default
				$items_to_update[] = array(
					'id'	=> $old_default_content->Id(),
					'icons' => array(
						'default' => getIcon('default', $old_default_content),
						'active'  => getIcon('active', $old_default_content),
						'delete'  => getIcon('delete', $old_default_content)
					)
				);

			}

		}

	}

	// ------------------------------------
	// -- create the new default
	// ------------------------------------

	// load the new default element by id
	$default_new = $hierarchy->getNodeById(floor($_GET['setdefault']));
	
	// check the element
	if(isset($default_new)) {

		// get the content
		$default_new_content = $default_new->getContent();
		
		// check the content
		if(isset($default_new_content)) {

			// set to default and active
			// default element can't be unactive
			$default_new_content->SetActive(true);
			$default_new_content->SetDefaultContent(true);
			$default_new_content->Save();

			// create array for the new default page
			$items_to_update[] = array(
				'id'	=> $default_new_content->Id(),
				'icons' => array(
					'default' => getIcon('default', $default_new_content),
					'active'  => getIcon('active', $default_new_content),
					'view'	  => getIcon('view', $default_new_content),
					'delete'  => getIcon('delete', $default_new_content)
				)
			);

			// update global variable
			$gCms->variables['default_content_id'] = $default_new_content->Id();

			// all good
			$success = true;

		}

	}

	// ------------------------------------
	// -- old parents
	// ------------------------------------

	// go though the list of the old default page parents
	foreach ($default_page_parents as $default_parent_id) {

		// get by id
		$default_parent = $hierarchy->getNodeById($default_parent_id);

		// got it?
		if($default_parent
		&& $default_parent_content = $default_parent->getContent()) {

			// set to active
			if($default_parent_content) {

				// add to the array new icons for old default
				$items_to_update[] = array(
					'id'	=> $default_parent_content->Id(),
					'icons' => array(
						'active'  => getIcon('active', $default_parent_content)
					)
				);

			}

		}

	}

	// ------------------------------------
	// -- new parents
	// ------------------------------------

	// update default parent list
	$new_default_page_parents = $contentops->GetDefaultContentParents();

	// go thorugh parents and set them to active
	foreach ($new_default_page_parents as $default_parent_id) {

		// get by id
		$default_parent = $hierarchy->getNodeById($default_parent_id);

		// got it?
		if($default_parent) {
			
			// get the content
			$default_parent_content = $default_parent->getContent();

			// set to active
			if($default_parent_content) {
				
				// update active
				$default_parent_content->SetActive(true);
				$default_parent_content->Save();

				// is the parent same as old default?
				if(isset($items_to_update[0]['id'])
				&& $items_to_update[0]['id'] == $default_parent_content->Id()) {

					// update existing icons
					$items_to_update[0]['icons']['active'] = getIcon('active', $default_parent_content);
					$items_to_update[0]['icons']['view'] = getIcon('view', $default_parent_content);

				} else {

					// create array for the new default page
					$items_to_update[] = array(
						'id'	=> $default_parent_content->Id(),
						'icons' => array(
							'active' => getIcon('active', $default_parent_content),
							'view' => getIcon('view', $default_parent_content)
						)
					);

				}

			}

		}

	}

	// ------------------------------------
	// -- send the results
	// ------------------------------------

	if($success) {

		// send the success with the old default id
		$response = array(
			'status' => 'success',
			'data' => $items_to_update
		);

	} else {

		// send the success with the old default id
		$response = array(
			'status' => 'error',
			'message' => lang('default_update_error')
		);

	}

	// if debug
	if($debug) {

		// just print response
		print_r($response);

	} else {

		// set header
		header('Content-type: application/json; charset=utf-8');

		// encode json
		$response = json_encode($response);

		// output json
		echo $response;

	}

	// exit
	exit;

}

// ------------------------------------
// -- toggle active
// ------------------------------------

if(isset($_GET['active'])
&& isset($_GET['id'])) {

	// scuccess
	$success = false;

	// set the value
	$active = $_GET['active'] == 1 ? true : false;

	// load the current item
	$node = $hierarchy->getNodeById(floor($_GET['id']));

	if(isset($node)) {
	
		// get the content of the node
		$value = $node->getContent();

		if(isset($value)) {

			// set the value
			$value->SetActive($active);
			$value->Save();

			// all good
			$success = true;

		}

	}

	if($success) {

		$active_icon = get_html(array(
			'link'		=> 'pages.php?active=' . ( $active ? 0 : 1 ) . '&id=' . $value->Id(),
			'onclick'	=> ' onclick="toggleActive.call(this); return false;"',
			'template'	=> $config['root_path'] . '/admin/templates/action-active-' . ( $active ? 'on' : 'off' ) . '.html'
		));

		// send the success with the old default id
		$response = array(
			'status' => 'success',
			'active' => $active_icon,
			'view'	 => getIcon('view', $value),
			'active_status' => $active ? true : false
		);

	} else {

		// send the success with the old default id
		$response = array(
			'status' => 'error',
			'message' => lang('active_update_error')
		);

	}

	// set header
	header('Content-type: application/json; charset=utf-8');

	// encode json
	$response = json_encode($response);

	// output json
	echo $response; exit;

}

// ------------------------------------
// -- colapse / expand content
// ------------------------------------

if(isset($_GET['expand'])
&& isset($_GET['id'])) {

	// load the current item
	$node = $hierarchy->getNodeById(floor($_GET['id']));
	
	// only do something if we have the node and it has children
	// if no children - there is no expand
	if($node && $node->hasChildren()) {

		// set the id
		$collapse_id = $_GET['id'];

		// do we add or remove this item from prefs?
		if(floor($_GET['expand']) == 1) {
			$collapse_add = true;
		} else {
			$collapse_add = false;
		}

		// get the prefs
		$collapse_prefs = get_preference($userid, 'collapse');

		// set it to array
		$collapse_prefs = json_decode($collapse_prefs);

		// do we have anything in the prefs?
		if(is_array($collapse_prefs)
		&& count($collapse_prefs) > 0) {

			// check what to do
			if($collapse_add) {
				// if it's not in the array add it in
				if(!in_array($collapse_id, $collapse_prefs)) {
					$collapse_prefs[] = $collapse_id;
				}
			} else {
				
				// go thorugh array and remove the node if it
				foreach ($collapse_prefs as $collapse_node_key => $collapse_node_id) {
					if($collapse_node_id == $collapse_id) {
						unset($collapse_prefs[$collapse_node_key]);
					}
				}

				// reset keys of the array
				$collapse_prefs = array_values($collapse_prefs);

			}

		} else {

			// create new array if add
			if($collapse_add) {
				$collapse_prefs = array($collapse_id);
			}
			
		}

		// set the new prefs
		set_preference($userid, 'collapse', json_encode($collapse_prefs));

		// all good
		$status = 'success';

	} else {

		// there is no item with this id
		$status = 'error';

	}

	// send the success with the old default id
	$response = array(
		'status' => $status
	);

	// set header
	header('Content-type: application/json; charset=utf-8');

	// encode json
	$response = json_encode($response);

	// output json
	echo $response; exit;

}

// ------------------------------------
// -- colapse / expand content
// ------------------------------------

if(isset($_GET['move'])
&& isset($_GET['id'])
&& isset($_GET['parent_id'])) {

	// default status
	$status = 'error';

	// set up direction
	if($_GET['move'] == 'down') { $direction = 'down'; }
	if($_GET['move'] == 'up') { $direction = 'up'; }

	// id of the content to move
	$move_content_id = floor($_GET['id']);
	$move_parent_id  = $_GET['parent_id'] == -1 ? -1 : floor($_GET['parent_id']);

	// grab necessary info for fixing the item_order
	$query = "SELECT item_order FROM $content_table WHERE content_id = $move_content_id";

	// get the order
	if($order = $db->GetOne($query)) {
		
		// get the time stamp
		$time = $db->DBTimeStamp(time());

		// down
		if($direction == "down") {

			// get the next item after current item
			$query = "SELECT content_id FROM $content_table WHERE item_order = $order + 1 AND parent_id = $move_parent_id";

			// if we have it then we do the move
			if($sibling_id = $db->GetOne($query)) {

				// move up next element
				$db->Execute(
					"UPDATE $content_table SET item_order = (item_order - 1), modified_date = $time
					 WHERE content_id = $sibling_id"
				);

				// move down current element
				$db->Execute(
					"UPDATE $content_table SET item_order = (item_order + 1), modified_date = $time
					 WHERE content_id = $move_content_id"
				);

				// all good
				$status = 'success';

			}

		}

		// up
		if($direction == "up" && $order > 1) {

			// get the previous item
			$query = "SELECT content_id FROM $content_table WHERE item_order = $order - 1 AND parent_id = $move_parent_id";
			
			// if we have it then we do the move
			if($sibling_id = $db->GetOne($query)) {

				// move prev element down
				$db->Execute(
					"UPDATE $content_table SET item_order = (item_order + 1), modified_date = $time
					 WHERE content_id = $sibling_id"
				);

				// move current element up
				$db->Execute(
					"UPDATE $content_table SET item_order = (item_order - 1), modified_date = $time
					 WHERE content_id = $move_content_id"
				);

				// all good
				$status = 'success';

			}

		}

	}

	// send the success with the old default id
	$response = array(
		'status' => $status
	);

	if($status = 'success') {
	
		// update hierarchy
		$contentops->SetAllHierarchyPositions();

		// load the current page
		$current_page = $contentops->LoadContentFromId($move_content_id);
		$sibling_page = $contentops->LoadContentFromId($sibling_id);

		// add id's and icons to the response
		$response['sibling_id'] = $sibling_page->Id();
		$response['current_id'] = $current_page->Id();
		$response['sibling_icon'] = getIcon('move', $sibling_page);
		$response['current_icon'] = getIcon('move', $current_page);

	}

	// set header
	header('Content-type: application/json; charset=utf-8');

	// encode json
	$response = json_encode($response);

	// output json
	echo $response; exit;

}

// ------------------------------------
// -- delete content
// ------------------------------------

if(isset($_GET['deletecontent'])) {

	// default status error
	$response = array(
		"status"	=> "error",
		"message"	=> "no_access",
		"deleted"	=> false
	);

	// todo - vai mēs pārbaudam autorus un ļaujam dzēst tikai ja autors?
	// check_ownership($userid, $contentid)
	// quick_check_authorship($contentid, $mypages)

	// check access first
	if(check_permission($userid, 'pages_delete')) {
					
		$response["message"] = 'no_content';

		// load the current item
		if($node = $hierarchy->getNodeById(floor($_GET['deletecontent']))) {

			// $childcount = 0;
			// $parentid = -1;
			
			// if($parent = $node->getParent()) {
				
			// 	$parentContent = $parent->getContent();
				
			// 	if(is_object($parentContent)) {
			// 		$parentid = $parentContent->Id();
			// 		$childcount = $parent->getChildrenCount();
			// 	}

			// }

			if($contentobj = $node->getContent(true)) {
	
				// check for children and default content
				if($contentobj->HasChildren()) {
					
					$response["message"] = 'errorchildcontent';

				} else if($contentobj->DefaultContent()) {
					
					$response["message"] = 'errordefaultpage';

				} else {
				
					// add to admin log
					audit($contentobj->Id(), $contentobj->Name(), 'Deleted Content');

					// delete the object
					$contentobj->Delete();

					// update hierarchy
					$contentops->SetAllHierarchyPositions();
					
					// see if this is the last child... if so, remove
					// the expand for it
					// if ($childcount == 1 && $parentid > -1) {
					// 	toggleexpand($parentid, true);
					// }
					
					// Do the same with this page as well
					// toggleexpand($contentid, true);
				
					// all good set response
					$response["status"] = 'success';
					$response["message"] = 'contentdeleted';
					$response["deleted"] = true;

				}

			}

		}

	}

	// set header
	header('Content-type: application/json; charset=utf-8');

	// encode json
	$response = json_encode($response);

	// output json
	echo $response; exit;

}

// ------------------------------------
// -- Get Icons
// ------------------------------------

function getIcon($type, $node) {

	// get config file
	global $gCms;
	$config = $gCms->GetConfig();

	// default result - empty string
	$result = '';

	switch ($type) {

		// ------------------------------------
		// -- view
		// ------------------------------------

		case 'view':

			// todo -> ko mēs daram ja "parent" nav valoda?
			// HierarchyPath() mums dod visu pilno path parent/parent/child
			// bet ja parent nav vajadzīgs urlā?

			// only content pages can be viewed
			if($node->IsViewable()
			&& $node->Active()) {

				// check for custom url
				$view_link = $node->URL() ? $node->URL() : $node->HierarchyPath();

				// check if default content
				if($node->DefaultContent() || $node->isDefaultContentParent()) $view_link = '';

				// icon
				$result = get_html(array(
					'link'		=> $config['root_url'] . '/' . $view_link,
					'template'	=> $config['root_path'] . '/admin/templates/action-view.html'
				));

			} else {

				// set the type
				$node_type = $node->Type() == 'modulepage' ? 'module' : $node->Type();

				// icon
				$result = get_html(array(
					'text'		=> $node->Type() != "content" ? $node_type : 'not active',
					'template'	=> $config['root_path'] . '/admin/templates/action-error-message.html'
				));

			}

			break;

		// ------------------------------------
		// -- default
		// ------------------------------------

		case 'move':

			// get the hierachu manager
			$hierarchy = $gCms->GetHierarchyManager();

			// and load node by id
			$node_hierarchy = $hierarchy->getNodeById($node->Id());

			// default is active
			$move_up_active = true;
			$move_down_active = true;

			// first item
			if(($node->ItemOrder() - 1) <= 0) {
				$move_up_active = false;
			}

			// last item
			if(($node->ItemOrder() - 1) == $node_hierarchy->getSiblingCount() - 1) {
				$move_down_active = false;
			}

			// move up icon
			$move_up = get_html(array(

				// is the button on or off?
				'on'		=> $move_up_active,
				'off'		=> $move_up_active ? false : true,
				'color'		=> $move_up_active ? 'one' : 'off',

				// link and onclick action
				'onclick'	=> 'moveUp.call(this); return false;',
				'link'		=> 'pages.php?id=' . $node->Id() . '&parent_id=' . $node->ParentId() . '&move=up',

				// template
				'template'	=> $config['root_path'] . '/admin/templates/action-move-up.html'

			));

			// move up icon
			$move_down = get_html(array(

				// is the button on or off?
				'on'		=> $move_down_active,
				'off'		=> $move_down_active ? false : true,
				'color'		=> $move_down_active ? 'one' : 'off',

				// link and onclick action
				'onclick'	=> 'moveDown.call(this); return false;',
				'link'		=> 'pages.php?id=' . $node->Id() . '&parent_id=' . $node->ParentId() . '&move=down',

				// template
				'template'	=> $config['root_path'] . '/admin/templates/action-move-down.html'
				
			));

			// combine both html
			$result = $move_up . $move_down;

			break;

		// ------------------------------------
		// -- default
		// ------------------------------------

		case 'default':

			if($node->Type() == "content") {

				if($node->DefaultContent()) {
					
					$result = get_html(array(
						'template'	=> $config['root_path'] . '/admin/templates/action-active-static.html'
					));

				} else {
					
					$result = get_html(array(
						'link'		=> 'pages.php?setdefault=' . $node->Id(),
						'onclick'	=> ' onclick="setDefault.call(this); return false;"',
						'template'	=> $config['root_path'] . '/admin/templates/action-active-off.html'
					));

				}

			} else {

				$result = get_html(array(
					'text'		=> "-----",
					'template'	=> $config['root_path'] . '/admin/templates/action-error-message.html'
				));

			}

			break;

		// ------------------------------------
		// -- active
		// ------------------------------------

		case 'active':

			// cases where we can't change active status:
			// 1) it's default content
			// 2) it's in default content parent list

			if($node->DefaultContent()
			|| $node->isDefaultContentParent()) {
				
				// static icon with no action
				$result = get_html(array(
					'template'	=> $config['root_path'] . '/admin/templates/action-active-static.html'
				));

			} else {

				// get template operations
				$templateops = $gCms->GetTemplateOperations();

				// check access
				$access = $templateops->TemplateAccess($node->TemplateId(), 'active');

				// is current state active
				if($node->Active()) {

					$result = get_html(array(
						'link'		=> 'pages.php?active=0&id=' . $node->Id(),
						'onclick'	=> ' onclick="toggleActive.call(this); return false;"',
						'template'	=> $config['root_path'] . '/admin/templates/action-active-' . ( $access ? 'on' : 'static' ) . '.html'
					));

				} else {

					$result = get_html(array(
						'link'		=> 'pages.php?active=1&id=' . $node->Id(),
						'onclick'	=> ' onclick="toggleActive.call(this); return false;"',
						'template'	=> $config['root_path'] . '/admin/templates/action-active-' . ( $access ? 'off' : 'off-static' ) . '.html'
					));

				}

			}

			break;

		// ------------------------------------
		// -- delete
		// ------------------------------------

		case 'delete':

			// we can't delete:
			// 1) default
			// 3) if has children

			if($delete_error_message = $node->ContentProtected()) {

				// create icon
				$result = get_html(array(
					'text'		=> $delete_error_message,
					'template'	=> VCMS_PATH . '/admin/templates/action-error-message.html'
				));

			} else {

				// get template operations
				$templateops = $gCms->GetTemplateOperations();

				// check access
				$access = $templateops->TemplateAccess($node->TemplateId(), 'delete');

				// create icon
				$result = get_html(array(
					'link'		=> 'pages.php?deletecontent=' . $node->Id(),
					'confirm'	=> lang('deleteconfirm', $node->Name()),
					'template'	=> VCMS_PATH . '/admin/templates/action-delete' . ( !$access ? '-off' : '' ) . '.html'
				));

			}

			break;

		// ------------------------------------

		case 'expand':

			// create icon
			$result = get_html(array(
				'link'		=> 'pages.php?id=' . $node->Id(),
				'template'	=> $config['root_path'] . '/admin/templates/action-expand.html'
			));

			break;

		// ------------------------------------

		case 'copy':

			// create icon
			$result = get_html(array(
				'link'		=> 'pages_copy.php?id=' . $node->Id(),
				'template'	=> $config['root_path'] . '/admin/templates/action-copy.html'
			));

			break;

		// ------------------------------------

		case 'edit':

			// get template operations
			$templateops = $gCms->GetTemplateOperations();

			// check access
			$access = $templateops->TemplateAccess($node->TemplateId(), 'edit');

			if($access) {

				// icon edit
				$result = get_html(array(
					'link'		=> 'editcontent.php?content_id=' . $node->Id(),
					'template'	=> $config['root_path'] . '/admin/templates/action-edit.html'
				));

			} else {

				// icon edit
				$result = get_html(array(
					'template'	=> $config['root_path'] . '/admin/templates/action-edit-off.html'
				));

			}

			break;

		// ------------------------------------

	}

	return $result;

}

// ------------------------------------
// -- Populate pages list
// ------------------------------------

function popuplatePages($children, $default_page_parents) {

	// get config
	global $gCms;
	$config = $gCms->GetConfig();

	// list row html output
	$rows = '';

	// last key of children
	$children_last_key = count($children) - 1;

	// get the first child
	$first_child = $children[0]->getContent();

	// and check if it's the first in the hierarchy
	if($first_child->Hierarchy() == 1) {

		// just a fake class for header row
		class EmptyHeaderClass {

			public function hasChildren() { 
				return false;
			}

			public function getContent() { 
				$dummy_content =  new Content();
				$dummy_content->mName = 'header';
				return $dummy_content;
			}

		}

		// create it
		$header = new EmptyHeaderClass();

		// add the header child so that we can create header row
		array_unshift($children , $header);

	}

	// go thorugh children
	foreach($children as $child_key => $child) {
			
		// get the content
		$page = $child->getContent();

		// get template operations
		$templateops =& $gCms->GetTemplateOperations();

		// get the template
		$template = $templateops->LoadTemplateById($page->TemplateId());

		// ------------------------------------
		// -- create cells
		// ------------------------------------

		$cells = array();

		// ------------------------------------
		// -- edit link and button
		// ------------------------------------

		if($page->Name() == 'header') {

			$cell_content = lang('pages');

		} else {

			// get the edit access
			$template_access_edit = $templateops->TemplateAccess($page->TemplateId(), 'edit');

			// setup link
			$page_edit_link = 'editcontent.php?content_id=' . $page->Id();

			// menu text
			$page_menu_text = $page->MenuText();

			// check the access and set the template
			if(check_permission(get_userid(), 'pages_edit') && $template_access_edit) {
				$page_edit_text_template = $config['root_path'] . '/admin/templates/pages-row-name-link.html';
			} else {
				$page_edit_text_template = $config['root_path'] . '/admin/templates/pages-row-name.html';
			}

			// if module show link to module
			// todo - we need to check if user has access to this specific module
			if($page->Type() == 'modulepage') {
				$page_edit_text_template = $config['root_path'] . '/admin/templates/pages-row-module.html';
				$page_edit_link = 
					'moduleinterface.php?module=' . $page->GetPropertyValue('module_name');

					// get our module object
					$modulename = $page->GetPropertyValue('module_name');
					if(class_exists($modulename)) {
						$module = new $modulename;
						$page_menu_text = lang('module') . ': ' . $module->lang('friendlyname');
					} else {
						$page_menu_text = lang('module') . ': ' . $page->MenuText();
					}
					
			}

			// check for custom url
			$view_link = $page->URL() ? $page->URL() : $page->HierarchyPath();

			// check if default content
			if($page->DefaultContent() || $page->isDefaultContentParent()) $view_link = '';

			// get the template
			$cell_content = get_html(array(
				'link'		=> $page_edit_link,
				'name'		=> $page_menu_text,
				'extra'		=> $page->IsViewable() ? $config['root_url'] . '/' . $view_link : 'Content block #' . $page->Alias(),
				'template'	=> $page_edit_text_template
			));

		}

		// cell tempalte
		$cells['name'] = get_html(array(
			'class'		=> '  list__cell--fill  list__cell--first',
			'data'		=> '',
			'content'	=> $cell_content,
			'template'	=> $config['root_path'] . '/admin/templates/list-cell.html'
		));

		// unset content
		unset($cell_content);
		unset($cell);

		// ------------------------------------
		// -- template name
		// ------------------------------------

		if($page->Name() != 'header') {

			// check permisions for templates
			if(check_permission(get_userid(), 'templates')) {

				// do we have template
				if($template) {

					// get the template
					$template_link = get_html(array(
						'link'		=> 'templates_add_edit.php?template_id=' . $template->id,
						'name'		=> $template->name,
						'extra'		=> $page->AccessKey() !== null && $page->AccessKey() != '' ? $page->AccessKey() : '----',
						'template'	=> $config['root_path'] . '/admin/templates/pages-row-template.html'
					));

					// cell template
					$cells['template'] = get_html(array(
						'class'		=> '  list__cell--template  color-one-lighter-text  text-right',
						'data'		=> '',
						'content'	=> $template_link,
						'template'	=> $config['root_path'] . '/admin/templates/list-cell.html'
					));

				}
			
			}

		}

		// ------------------------------------
		// -- view
		// ------------------------------------

		// check
		if(check_permission(get_userid(), 'admin_pages_view')) {
			if($page->Name() == 'header') {
				$cell_content = lang('view');
			} else {
				$cell_content = getIcon('view', $page);
			}
		}

		// is it set?
		if(isset($cell_content)) {

			// cell template
			$cells['view'] = get_html(array(
				'class'		=> '  list__cell--icon',
				'data'		=> ' data-action="view"',
				'content'	=> $cell_content,
				'template'	=> $config['root_path'] . '/admin/templates/list-cell.html'
			));

			// unset content
			unset($cell_content);

		}

		// ------------------------------------
		// -- move
		// ------------------------------------

		// check
		if(check_permission(get_userid(), 'pages_move')) {
			if($page->Name() == 'header') {
				$cell_content = lang('move');
			} else {
				$cell_content = getIcon('move', $page);
			}
		}

		// is it set?
		if(isset($cell_content)) {

			// cell template
			$cells['move'] = get_html(array(
				'class'		=> '  list__cell--icon-move',
				'data'		=> ' data-action="move"',
				'content'	=> $cell_content,
				'template'	=> $config['root_path'] . '/admin/templates/list-cell.html'
			));

			// unset content
			unset($cell_content);

		}

		// ------------------------------------
		// -- edit
		// ------------------------------------

		// check
		if(check_permission(get_userid(), 'pages_edit')) {
			if($page->Name() == 'header') {
				$cell_content = lang('edit');
			} else {
				$cell_content = getIcon('edit', $page, $template_access_edit);
			}
		}

		// is it set?
		if(isset($cell_content)) {

			// cell template
			$cells['edit'] = get_html(array(
				'class'		=> '  list__cell--icon',
				'data'		=> ' data-action="edit"',
				'content'	=> $cell_content,
				'template'	=> $config['root_path'] . '/admin/templates/list-cell.html'
			));

			// unset content
			unset($cell_content);

		}

		// ------------------------------------
		// -- default
		// ------------------------------------

		// check
		if(check_permission(get_userid(), 'pages_set_default')) {
			if($page->Name() == 'header') {
				$cell_content = lang('default');
			} else {
				$cell_content = getIcon('default', $page);
			}
		}

		// is it set?
		if(isset($cell_content)) {

			// cell template
			$cells['default'] = get_html(array(
				'class'		=> '  list__cell--icon',
				'data'		=> ' data-action="default"',
				'content'	=> $cell_content,
				'template'	=> $config['root_path'] . '/admin/templates/list-cell.html'
			));

			// unset content
			unset($cell_content);

		}

		// ------------------------------------
		// -- active
		// ------------------------------------

		// check
		if(check_permission(get_userid(), 'pages_set_active')) {
			if($page->Name() == 'header') {
				$cell_content = lang('active');
			} else {
				$cell_content = getIcon('active', $page);
			}
		}

		// is it set?
		if(isset($cell_content)) {

			// cell template
			$cells['active'] = get_html(array(
				'class'		=> '  list__cell--icon',
				'data'		=> ' data-action="default"',
				'content'	=> $cell_content,
				'template'	=> $config['root_path'] . '/admin/templates/list-cell.html'
			));

			// unset content
			unset($cell_content);

		}

		// ------------------------------------
		// -- copy - (removed)
		// ------------------------------------

		// check
		if(check_permission(get_userid(), 'admin_pages_copy')) {
			if($page->Name() == 'header') {
				$cell_content = lang('copy');
			} else {
				$cell_content = getIcon('copy', $page);
			}
		}

		// is it set?
		if(isset($cell_content)) {

			$cells['copy'] = get_html(array(
				'class'		=> '  list__cell--icon',
				'data'		=> ' data-action="copy"',
				'content'	=> $cell_content,
				'template'	=> $config['root_path'] . '/admin/templates/list-cell.html'
			));

			// unset content
			unset($cell_content);

		}

		// ------------------------------------
		// -- delete
		// ------------------------------------

		// check
		if(check_permission(get_userid(), 'pages_delete')) {
			if($page->Name() == 'header') {
				$cell_content = lang('delete');
			} else {
				$cell_content = getIcon('delete', $page);
			}
		}

		// is it set?
		if(isset($cell_content)) {

			$cells['delete'] = get_html(array(
				'class'		=> '  list__cell--icon',
				'data'		=> ' data-action="delete"',
				'content'	=> $cell_content,
				'template'	=> $config['root_path'] . '/admin/templates/list-cell.html'
			));

			// unset content
			unset($cell_content);

		}

		// ------------------------------------
		// -- css classes
		// ------------------------------------

		// extra classes
		$extra_class = '';

		// check for header row
		if($page->Name() == 'header') {

			// add header class
			$extra_class .= '  list__item--header  color-one-text  color-bg-darker';

		} else {

			// do we have children?
			if($child->hasChildren()) {
				$extra_class .= '  list__item--has-children';
			}

			// is this the last child?
			if($child_key == $children_last_key) {
				$extra_class .= '  list__item--last-child';
			}

			// is this the last child?
			if(!$page->Active()) {
				$extra_class .= '  list__item--off';
			}

		}

		// ------------------------------------
		// -- expanded or collapsed?
		// ------------------------------------

		// get the prefs
		$user_collapse_prefs = json_decode(get_preference(get_userid(), 'collapse'));

		if($child->hasChildren()
		&& is_array($user_collapse_prefs)
		&& in_array($page->Id(), $user_collapse_prefs)) {
			$extra_class .= '  list__item--expanded';
		}

		// get the expand icon for the row
		$expand_cell = $child->hasChildren() ? getIcon('expand', $page) : '';

		// ------------------------------------
		// -- row
		// ------------------------------------

		$cells_output = '';

		foreach ($cells as $cell) {
			$cells_output .= $cell;
		}

		// get the template for the row
		$rows .= get_html(array(

			// extra class
			'class'		=> $extra_class,
			
			// default template vars
			'id' 		=> $page->Name() != 'header' ? $page->Id() : 'list-header',
			'expand'	=> $expand_cell,
			'cells'		=> $cells_output,

			// template html file
			'template'	=> $config['root_path'] . '/admin/templates/pages-row-temp.html'

		));

		// do we have children
		if($child->hasChildren()) {

			// call up this function again to setup rows
			$children_pages = popuplatePages($child->getChildren(false, true), $default_page_parents);

			// group them up
			$rows .= get_html(array(
				'rows'		=> $children_pages,
				'template'	=> $config['root_path'] . '/admin/templates/pages-list-group.html'
			));

		}

	}

	return $rows;

}

// ------------------------------------
// -- header
// ------------------------------------

$header_file = get_vcms_file("/admin/header.php");
include_once($header_file);

// ------------------------------------
// -- page list
// ------------------------------------

// add button
$add_button = check_permission($userid, 'pages_add') ? get_html(array(

	'link'		=> 'addcontent.php',
	'text'		=> lang('addcontent'),

	'template'	=> $config['root_path'] . '/admin/templates/button-ok.html'

)) : "";

// set row var
$rows = '';

// call the population
if($hierarchy->hasChildren()) {
	$rows = popuplatePages($hierarchy->getChildren(false, true), $default_page_parents);
}

// get the template list template
$pages = get_html(array(

	// message
	'message'	=> isset($message) ? $message : '',

	// add button
	'button'	=> $add_button,

	// content
	'content'	=> $rows,

	// template html file
	'template'	=> $config['root_path'] . '/admin/templates/pages-list.html'

));

echo $pages;

// ------------------------------------
// -- footer
// ------------------------------------

include_once("footer.php");

?>