<?php

/* ------------------------------
// Opciju tabs
------------------------------ */
	
if(isset($tab_contents_array[$currenttab])) {

	// nočekojam vai users ir priviliģēts darīt lietas
	// vai arī users ir parastais mirstīgais
	if(check_permission($userid, 'templates_add')) {
		// templetius pievienot var tikai main admins
		// ir piekļuve visam
		$allowed_content = "all";
	} else {
		// piekļuve tikai dažiem
		if($contentobj->AccessKey() == "default") {
			$allowed_content = array(
				'Last modified at:',
				'Last modified by:'
			);
		} else {
			$allowed_content = array(
				'Active:',
				'Show in Menu:',
				'Page Alias:',
				'Last modified at:',
				'Last modified by:'
			);
		}
	}

	/* ------------------------------
	// setupojam kurā kollonā ko mums jārādā
	------------------------------ */

	$right_col_inputs = array(
		'Template:',
		'Veidne:',
		'Show in Menu:',
		'Active:',
		'Aktīvs:',
		'Page Alias:',
		'Last modified at:',
		'Last modified by:',
		'Extra Page Attribute 1:',
		'Extra Page Attribute 2:',
		'Extra Page Attribute 3:',
		'Additional Editors'
	);

	$summary_inputs = array(
		'Access Key:',
		'Owner:',
		'Page Specific Metadata:',
		'Smarty data or logic that is specific to this page:'
	);

	$right_col = array();
	$left_col  = array();

	foreach ($tab_contents_array[$currenttab] as $key => $input) {
		
		// ja nav array izlaižam
		if(!is_array($input)) continue;

		// pārbaudam vai ir extra commenti
		if(isset($extra_comments[$input[0]]) && !isset($input[2])) {
			$input[2] = $extra_comments[$input[0]];
		}

		// lai pareizi array būtu uzliekam input[2]
		$input[2] = isset($input[2]) ? $input[2] : '';

		// rādam inputu lietotājam
		$input[3] = true;

		// pārbaudam vai mēs rādam šo inputu lietotājam
		if(in_array($input[0], $hiddenFileds)
		|| check_by_field_name($input, $hiddenFileds)) {
			$input[3] = false;
		}

		// visādi citāti setupojam
		if($allowed_content == "all" || in_array($input[0], $allowed_content)) {
			if(!in_array($input[0], $right_col_inputs)) {
				$right_col[$input[0]] = outputContentRow($input, $contentobj);
			} else {
				$left_col[$input[0]] = outputContentRow($input, $contentobj);
			}
		}

	}

	/* ------------------------------
	// satura izvade
	------------------------------ */


	echo '<div class="module-container  tab-content' . ( $currenttab == 0 ? '  tab-content-active' : '' ) . '" id="editab'.$currenttab.'">';

		/* ------------------------------
		// left column
		------------------------------ */

		echo '<div class="module-col">';
			foreach ($left_col as $input) { echo $input; }
		echo '</div>';

		/* ------------------------------
		// right column
		------------------------------ */

		echo '<div class="module-col">';
			foreach ($right_col as $input) { echo $input; }
		echo '</div>';

	echo '</div>';

}

?>