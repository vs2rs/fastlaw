<?php

global $CMS_ADMIN_PAGE;
$CMS_ADMIN_PAGE = 1;

require_once(dirname(dirname(__FILE__)) . "/include.php");

vcmsAuth::is_loged_in();

// get user and check access
$access = check_permission(get_userid(), 'admin_pages_copy');

// ------------------------------------
// -- default variables
// ------------------------------------

// templates list page
$pages_list = 'pages.php';

// error variable
$errors = array();

// action url for form
$form_action = 'pages_copy.php';

// db
$db = Database::getInstance();

// content operations
$contentops = $gCms->GetContentOperations();

// ------------------------------------
// -- if cancel redirect
// ------------------------------------

// if cancel then redirect
if (isset($_POST["cancel"])) {
	redirect($pages_list);
	exit;
}

$submit = false;
if(isset($_POST["submit"])) {
	$submit = true;
}

// ------------------------------------
// -- check for id and set query
// ------------------------------------

$page_id = -1;
if(isset($_GET["id"])) {
	$page_id = floor($_GET["id"]);
	$form_action = 'pages_copy.php?id=' . $page_id;
}

// get the page and the props
$page = $db->get_row("SELECT * FROM {{prefix}}content WHERE content_id = $page_id LIMIT 1");
$props = $db->get_rows("SELECT * FROM {{prefix}}content_props WHERE content_id = $page_id");

// ------------------------------------
// -- POST
// ------------------------------------

if($submit) {
	
	$contentobj = $contentops->CreateNewContent($page->type);
	$contentobj->SetAddMode();
	$contentobj->SetOwner(get_userid());
	$contentobj->SetCachable(1);
	$contentobj->SetActive(1);
	$contentobj->SetShowInMenu(1);
	$contentobj->SetLastModifiedBy(get_userid());
	$contentobj->SetParentId(-1);

	$templateops = $gCms->GetTemplateOperations();
	$dflt = $templateops->LoadDefaultTemplate();
	if(isset($dflt)) {
		$contentobj->SetTemplateId($dflt->id);
	}

	//Fill contentobj with parameters
	$contentobj->SetAddMode();
	$contentobj->FillParams($_POST);
	$contentobj->SetOwner(get_userid());

	$error = $contentobj->ValidateData();
	if ($error === FALSE) {
		$contentobj->Save();
		global $gCms;
		$contentops =& $gCms->GetContentOperations();
		$contentops->SetAllHierarchyPositions();
		if ($submit) {
			audit($contentobj->Id(), $contentobj->Name(), 'Content copied');
			redirect('pages.php?message=contentadded');
		}
	}

}

// ------------------------------------
// -- admin header
// ------------------------------------

$header_file = get_vcms_file("/admin/header.php");
include_once($header_file);

// if we have access
if($access && $page) {

	// ------------------------------------
	// -- setup error message
	// ------------------------------------

	if(count($errors) > 0) {
		$message = $themeObject->ShowMessage($errors, 'error');
	} else {
		if(FALSE == empty($error)) {
			$message = $themeObject->ShowMessage($error, 'error');
		} else {
			$message = isset($message) ? $themeObject->ShowMessage(lang($message)) : '';
		}
	}

	// ------------------------------------
	// -- define tabs
	// ------------------------------------

	// setup parent dropdown
	$parent_dropdown = $contentops->CreateHierarchyDropdown();
	$parent_dropdown = '
	<div class="form__row">
		<label class="form__label" for="input-content_name">' . lang('parent') . ' <span class="color-cancel-text">*</span></label>
		<div class="form__input">
			'.$parent_dropdown.'
		</div>
	</div>';

	// info
	$info_col = '
		<div class="form__row">
			<label class="form__label">Name</label>
			<div class="form__input">
				<span>' . $page->content_name . '</span>
			</div>
		</div>
		<div class="form__row">
			<label class="form__label">Menu Text</label>
			<div class="form__input">
				<span>' . $page->menu_text . '</span>
			</div>
		</div>
	';

	$inputs = array(

		array(
			'type'	=> 'input',
			'label'	=> lang('title'),
			'name' 	=> 'title',
			'validate' => array(
				'not_empty' => true
			)
		),

		array(
			'type'	=> 'input',
			'label'	=> lang('menutext'),
			'name' 	=> 'menutext',
			'validate' => array(
				'not_empty' => true
			)
		),

		$parent_dropdown,

		array(
			'type'	=> 'checkbox',
			'label'	=> lang('active'),
			'name' 	=> 'active',
			'value' => 1,
			'checked' => false
		),

		// hidden

		array(
			'type'	=> 'hidden',
			'name' 	=> 'content_en',
			'value' => '',
		),

		array(
			'type'	=> 'hidden',
			'name' 	=> 'content_type',
			'value' => $page->type,
		),

		array(
			'type'	=> 'hidden',
			'name' 	=> 'alias',
			'value' => '',
		),

		array(
			'type'	=> 'hidden',
			'name' 	=> 'showinmenu',
			'value' => $page->show_in_menu,
		),

		array(
			'type'	=> 'hidden',
			'name' 	=> 'template_id',
			'value' => $page->template_id,
		),

		array(
			'type'	=> 'hidden',
			'name' 	=> 'page_url',
			'value' => ''
		),

		array(
			'type'	=> 'hidden',
			'name' 	=> 'accesskey',
			'value' => $page->accesskey,
		),

		array(
			'type'	=> 'hidden',
			'name' 	=> 'ownerid',
			'value' => get_userid(),
		)

	);

	// add props to the list
	foreach($props as $prop) {
		$inputs[] = array(
			'type'	=> 'hidden',
			'label'	=> $prop->prop_name,
			'name' 	=> $prop->prop_name,
			'value'	=> htmlspecialchars($prop->content, ENT_QUOTES, 'UTF-8')
		);
	}

	// create the tabs
	$form_content = $themeObject->CreateTabs(array(
		
		0 => array(

			// tab name
			'name' => lang('copy'),

			// content
			'content' => array(

				// setup cols
				'col-1' => $inputs,
				'col-2' => $info_col

			)

		),

		// 1 => array(

		// 	'name' 		=> lang('template_users'),

		// 	'content'	=> array(

		// 		'col-1' => array(

		// 			array(
		// 				'type'	 => 'user_access_checkboxes',
		// 				'values' => json_decode($template_users)
		// 			)

		// 		)

		// 	)

		// )

	));

	// ------------------------------------
	// -- create the form
	// ------------------------------------

	// submit
	$button_submit = get_html(array(
		'name'		=> 'submit',
		'value'		=> lang('copy'),
		'template'	=> $config['root_path'] . '/admin/templates/button-submit.html'
	));

	// submit
	$button_apply = get_html(array(
		'name'		=> 'apply',
		'value'		=> lang('apply'),
		'template'	=> $config['root_path'] . '/admin/templates/button-submit.html'
	));

	// cancel
	$button_cancel = get_html(array(
		'name'		=> 'cancel',
		'value'		=> lang('cancel'),
		'template'	=> $config['root_path'] . '/admin/templates/button-cancel.html'
	));

	// add button
	echo get_html(array(

		// where to submit
		'action'	=> $form_action,
		
		// error/success messages
		'message'	=> $message,

		// content
		'content'	=> $form_content,
		'submit'	=> $button_submit,
		'apply'		=> '',
		'cancel'	=> $button_cancel,
		'buttons_top' => true,

		// template
		'template'	=> VCMS_PATH . '/admin/templates/form.html'

	));

} else {
	
	// ------------------------------------
	// -- setup no access message
	// ------------------------------------

	if(!$access) {

		$message = get_html(array(
			
			// error/success messages
			'message'	=> $themeObject->ShowMessage(lang('no_access'), 'error'),

			// template
			'template'	=> $config['root_path'] . '/admin/templates/no-access.html'

		));

	} else {

		$message = get_html(array(
			
			// error/success messages
			'message'	=> $themeObject->ShowMessage(lang('template_error_retrieving'), 'error'),

			// template
			'template'	=> $config['root_path'] . '/admin/templates/no-access.html'

		));

	}

	// output form
	echo $message;

}

// ------------------------------------
// -- footer
// ------------------------------------

include_once("footer.php");

# vim:ts=4 sw=4 noet
?>
