<?php

// ------------------------------------
// -- groups
// ------------------------------------

global $CMS_ADMIN_PAGE;
$CMS_ADMIN_PAGE=1;

// includes
require_once(dirname(dirname(__FILE__)) . "/include.php");
require_once(dirname(dirname(__FILE__)) . "/lib/classes/class.group.inc.php");

// login
vcmsAuth::is_loged_in();

// ------------------------------------
// -- access
// ------------------------------------

$access_add_remove = check_permission(get_userid(), 'users_groups_add_remove');
$access = $access_add_remove;

// ------------------------------------
// -- setup
// ------------------------------------

// load operations
global $gCms;
$operations = $gCms->GetGroupOperations();

// load groups
$itemlist = $operations->LoadGroups();

// ------------------------------------
// -- setup add button
// ------------------------------------

$add_button = '';
$add_button_top = false;
$add_button_bottom = false;

// check for access to add users
if($access_add_remove) {

	// add button
	$add_button = get_html(array(
		'link'		=> 'users_groups_add_edit.php',
		'text'		=> lang('group_add'),
		'template'	=> VCMS_PATH . '/admin/templates/button-ok.html'
	));

	// show the button
	$add_button_top = true;
	$add_button_bottom = true;

}

// ------------------------------------
// -- setup list cells
// ------------------------------------

// setup cells
$cells = array();
$cells[] = array(
	"key"    => "edit_link",
	"action" => "edit",
	"label"  => lang("group"),
	"class"  => "  list__cell--fill  list__cell--first"
);

if(check_permission(get_userid(), 'users_groups_add_remove')) {
	$cells[] = array(
		"key"    => "permissions",
		"action" => "permissions",
		"label"  => lang("permissions"),
		"class"  => "  list__cell--icon-move"
	);
}

$cells[] = array(
	"key"    => "edit",
	"action" => "edit",
	"label"  => lang("edit"),
	"class"  => "  list__cell--icon"
);

if(check_permission(get_userid(), 'users_groups_change_default')) {
	$cells[] = array(
		"key"    => "toggle_default",
		"action" => "default",
		"label"  => lang("default"),
		"class"  => "  list__cell--icon"
	);
}

$cells[] = array(
	"key"    => "toggle_active",
	"action" => "active",
	"label"  => lang("active"),
	"class"  => "  list__cell--icon"
);


// can we delete user groups?
if($access_add_remove) {
	$cells[] = array(
		"key"    => "delete",
		"action" => "delete",
		"label"  => lang("delete"),
		"class"  => "  list__cell--icon"
	);
}

// ------------------------------------
// -- process list
// ------------------------------------

include_once(get_vcms_file("/admin/list.php"));





// -- groups.php