<?php
#CMS - CMS Made Simple
#(c)2004 by Ted Kulp (wishy@users.sf.net)
#This project's homepage is: http://cmsmadesimple.sf.net
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#$Id: templates_copy.php 6404 2010-06-25 01:17:04Z calguy1000 $

global $CMS_ADMIN_PAGE;
$CMS_ADMIN_PAGE = 1;

require_once(dirname(dirname(__FILE__)) . "/include.php");
require_once(dirname(dirname(__FILE__)) . "/lib/classes/class.template.inc.php");

// login?
vcmsAuth::is_loged_in();

// get user and check access
$userid = get_userid();
$access = check_permission($userid, 'templates_add');

// ------------------------------------
// -- default variables
// ------------------------------------

// templates list page
$templates_list = 'templates.php';

// error variable
$errors = array();

// default post vars
$template_id = -1;
$template_name = "";
$template_active = 1;

// action url for form
$form_action = 'templates_copy.php';

// ------------------------------------
// -- if cancel redirect
// ------------------------------------

// if cancel then redirect
if (isset($_POST["cancel"])) {
	redirect($templates_list);
	exit;
}

// ------------------------------------
// -- get template ops
// ------------------------------------

global $gCms;
$templateops =& $gCms->GetTemplateOperations();

// ------------------------------------
// -- check for id and load data
// ------------------------------------

// set to false by default
$onetemplate = false;

// get from url
if(isset($_GET["template_id"])) {
	
	// get it
	$template_id = floor($_GET["template_id"]);

	// load data
	$onetemplate = $templateops->LoadTemplateByID($template_id);

	// update url
	$form_action = $form_action . '&template_id=' . $template_id;

}

// ------------------------------------
// -- POST
// ------------------------------------

if(isset($_POST["submit"])
&& isset($_POST["template_name"])
&& $onetemplate) {

	// if no active in post then template not active
	if(!isset($_POST["active"])) { $template_active = 0; }

	// validate template name
	if($_POST["template_name"] == "") {
		
		// no name given
		$errors['template_name'] = lang("nofieldgiven", array(lang('name')));

	} else {

		// set the name
		$template_name = $_POST["template_name"];

		// check name
		if($templateops->CheckExistingTemplateName($template_name)) {
			$errors['template_name'] = lang('template_exists');
		}

	}

	// if we have no errors
	if(count($errors) == 0) {

		// reset id so it will insert a new record
		$onetemplate->id = -1;

		// change name
		$onetemplate->name = $template_name;

		// active and default
		$onetemplate->default = 0;
		$onetemplate->active = $template_active;

		// do the insert
		if($result = $onetemplate->Save()) {

			// admin log
			audit($onetemplate->id, $onetemplate->name, 'Copied Template');
			
			// redirect
			redirect($templates_list . "&message=template_copied");

			// exit
			exit;

		} else {

			// something went wrong
			$errors['copy'] = lang('template_error_copying');

		}
	}

	
}

// ------------------------------------
// -- output template
// ------------------------------------

$header_file = get_vcms_file("/admin/header.php");
include_once($header_file);

// if access and template is loaded
if($access && $onetemplate) {

	// ------------------------------------
	// -- setup error message
	// ------------------------------------

	if(count($errors) > 0) {
		$message = $themeObject->ShowMessage($errors, 'error');
	} else {
		$message = "";
	}

	// ------------------------------------
	// -- define tabs for form
	// ------------------------------------

	// create the tabs
	$form_content = $themeObject->CreateTabs(array(
		
		0 => array(

			'name' 		=> lang('template_copy'),

			'content'	=> array(

				'col-1' => array(

					'no-input' => array(
						'type'	=> 'readonly',
						'label'	=> lang('template_to_copy'),
						'name'	=> 'template_old',
						'value' => $onetemplate->name
					),

					'input' => array(
						'type'	=> 'input',
						'label'	=> lang('name_new') . ' *',
						'name' 	=> 'template_name',
						'value' => $template_name
					),

					'checkbox' => array(
						'type'	=> 'checkbox',
						'label'	=> lang('active'),
						'name' 	=> 'active',
						'value' => $template_active
					)

				),

				'col-2' => ''

			)

		)

	));

	// ------------------------------------
	// -- create the form
	// ------------------------------------

	// submit
	$button_submit = get_html(array(
		'name'		=> 'submit',
		'value'		=> lang('submit'),
		'template'	=> $config['root_path'] . '/admin/templates/button-submit.html'
	));

	// cancel
	$button_cancel = get_html(array(
		'name'		=> 'cancel',
		'value'		=> lang('cancel'),
		'template'	=> $config['root_path'] . '/admin/templates/button-cancel.html'
	));

	// add button
	echo get_html(array(

		// where to submit
		'action'	=> $form_action,
		
		// error/success messages
		'message'	=> $message,

		// content
		'content'	=> $form_content,
		'submit'	=> $button_submit,
		'cancel'	=> $button_cancel,
		'apply'		=> '',
		'buttons_top' => true,

		// template
		'template'	=> VCMS_PATH . '/admin/templates/form.html'

	));

} else {
	
	// ------------------------------------
	// -- setup no access message
	// ------------------------------------

	if(!$access) {

		echo get_html(array(
			
			// error/success messages
			'message'	=> $themeObject->ShowMessage(lang('no_access'), 'error'),

			// template
			'template'	=> $config['root_path'] . '/admin/templates/no-access.html'

		));

	} else {

		echo get_html(array(
			
			// error/success messages
			'message'	=> $themeObject->ShowMessage(lang('template_error_retrieving'), 'error'),

			// template
			'template'	=> $config['root_path'] . '/admin/templates/no-access.html'

		));

	}

}

// ------------------------------------
// -- footer
// ------------------------------------

include_once("footer.php");

# vim:ts=4 sw=4 noet
?>
