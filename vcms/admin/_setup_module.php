<!DOCTYPE html>
<html>
<head>
	<title>DNA CREATION</title>
	<style type="text/css">
		html, body {
			font-family: monospace;
			font-size: 10px;
			margin: 0;
			padding: 25px;
		}
	</style>
</head>
<body>

</body>
</html>
<?php

$module_name 		  = 'my_module';
$module_friendly_name = 'Module';
$module_description   = 'Module';
$module_cat_names	  = array('Category', 'Categories');
$module_item_names	  = array('Item', 'Items');
$module_verison 	  = '1.0';

$module_levels = array(
	0 => array(
		// basic
		'name' 			=> 'category',
		'prefix' 		=> 'cat_',
		'display_names' => $module_cat_names,
		'admin_cols' 	=> 'item_order,name,active,movelinks',
		// fields
		'field_number'  => 0, // we update with count bellow
		'fields'		=> array(
			// 0 => array(
			// 	'name'			=> 'info',
			// 	'display_name'	=> 'Info',
			// 	'type'			=> 'input'
			// )
		)
	),
	1 => array(
		// basic
		'name' 			=> 'items',
		'prefix' 		=> 'item_',
		'display_names' => $module_item_names,
		'admin_cols' 	=> 'item_order,name,active,isdefault,movelinks',
		// fields
		'field_number'  => 0, // we update with count bellow
		'fields'		=> array()
	)
);

// $module_levels[1]["fields"][] = array(
// 	'name' 			=> 'info',
// 	'display_name' 	=> 'Info',
// 	'type' 			=> 'input'
// );

// $module_levels[1]["fields"][] = array(
// 	'name' 			=> 'area',
// 	'display_name' 	=> 'Area',
// 	'type' 			=> 'input'
// );

// $module_levels[1]["fields"][] = array(
// 	'name' 			=> 'price_start',
// 	'display_name' 	=> 'Price (Start)',
// 	'type' 			=> 'input'
// );

// $module_levels[1]["fields"][] = array(
// 	'name' 			=> 'status',
// 	'display_name' 	=> 'Status',
// 	'type' 			=> 'input'
// );

// $module_levels[1]["fields"][] = array(
// 	'name' 			=> 'date_end',
// 	'display_name' 	=> 'Date (End)',
// 	'type' 			=> 'input'
// );

// $module_levels[1]["fields"][] = array(
// 	'name' 			=> 'date_start',
// 	'display_name' 	=> 'Date (Start)',
// 	'type' 			=> 'input'
// );


// $module_levels[1]["fields"][] = array('name' => 'answer', 'display_name' => 'Correct Answer', 'type' => 'input');
// $module_levels[1]["fields"][] = array('name' => 'name_ru', 'display_name' => 'Name (RU)', 'type' => 'input');

// $module_levels[1]["fields"][] = array('name' => 'pdf', 'display_name' => 'PDF', 'type' => 'input');
// $module_levels[1]["fields"][] = array('name' => 'img', 'display_name' => 'Image', 'type' => 'input');





/* ------------------------------------------------  

	SETUP OF THE DNA ARRAY

------------------------------------------------ */

// update field count
$module_levels[0]["field_number"] = count($module_levels[0]["fields"]);
$module_levels[1]["field_number"] = count($module_levels[1]["fields"]);

$module = array(

	// main module info

	'infos' => array(
		'name' => $module_name,
		'friendlyname' => $module_friendly_name,
		'description' => $module_description,
		'nblevels' => 2,
		'templatelevel' => 0,
		'doupgrade' => '',
		'upgradequeries' => array(),
		'modifications' => array(),
		'nameofmodule' => $module_name,
		'version' => $module_verison,
		'deletefiles' => '',
		'editablealias' => ''
	),

	// creating the levels

	'levels' => array(
	)

);

$get_field_type = array(
	"input" => 4,
	"text" => 5
);

foreach ($module_levels as $level) {
	
	$setup = array(
		0 => $level["name"],
		1 => $level["prefix"],
		2 => 0, // not sure what is this
		3 => $level["field_number"], // number of fields
		6 => 1, // not sure what is this
		7 => 0, // not sure what is this
		8 => 0, // not sure what is this
		9 => $level["display_names"], // display names
		4 => array(),
		5 => $level["admin_cols"]
	);

	foreach ($level["fields"] as $field) {
		$setup[4][] = array(
			0 => $field['name'],
			7 => $field['display_name'],
			1 => $get_field_type[$field['type']],
			2 => 0,
			3 => 0,
			4 => '',
			5 => array(
				'listoptions' => '',
				'listmode' => 1,
				'upfolder' => '',
				'thumb' => '',
				'size' => '',
				'innersearch' => 0,
				'crop' => 0,
				'fileext' => '',
				'cropthumb' => 0
			),
			6 => array()
		);
	}

	$module['levels'][] = $setup;

}

foreach ($module['levels'] as $level) {

}

echo serialize($module);

?>

</body>
</html>