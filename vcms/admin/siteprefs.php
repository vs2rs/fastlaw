<?php

// ------------------------------------
// -- site prefs
// ------------------------------------

global $CMS_ADMIN_PAGE;
$CMS_ADMIN_PAGE=1;

// includes
require_once(dirname(dirname(__FILE__)) . "/include.php");
require_once(dirname(dirname(__FILE__)) . "/lib/classes/class.group.inc.php");

// login
vcmsAuth::is_loged_in();

// ------------------------------------
// -- setup
// ------------------------------------

// class name
$ClassName = 'Siteprefs';

// redirect page after submit
$redirect = 'siteprefs.php';

// form action
$form_action = 'siteprefs.php';

// set our id to whatever value (we need it to be true)
$id = 1;

// ------------------------------------
// -- form setup
// ------------------------------------

function form_setup($id) {

	// new form
	$form = new stdClass();

	// get operations
	global $gCms;
	$form->operations = (new class {

		public function LoadByID($id) {

			// default
			$result = false;

			// get cms and db
			global $gCms;
			$db = &$gCms->GetDb();

			// table
			$siteprefs_table = cms_db_prefix() . "siteprefs";
 
			// set query
			$query = "SELECT sitepref_value, sitepref_name
					  FROM $siteprefs_table
					  WHERE sitepref_name = 'sitename'
					  OR sitepref_name = 'logintheme'
					  OR sitepref_name = 'frontendlang'
					  ORDER BY sitepref_name ASC";

			if($dbresult = $db->Execute($query)) {

				$result = (new class {

					// params
					public $id = '';
					public $sitename = '';
					public $logintheme = '';
					public $frontendlang = '';

					// save function
					public function Save() {
						
						// get db
						global $gCms;
						$db = $gCms->GetDb();

						// timestamps
						$time = $db->DBTimeStamp(time());
						
						// table
						$siteprefs_table = cms_db_prefix() . "siteprefs";

						// sitename
						$query = "UPDATE $siteprefs_table
						SET sitepref_value = ?, modified_date = $time
						WHERE sitepref_name = 'sitename'";
						$dbresult = $db->Execute($query, array($this->sitename));

						// logintheme
						$query = "UPDATE $siteprefs_table
						SET sitepref_value = ?, modified_date = $time
						WHERE sitepref_name = 'logintheme'";
						$dbresult = $db->Execute($query, array($this->logintheme));

						// frontendlang
						$query = "UPDATE $siteprefs_table
						SET sitepref_value = ?, modified_date = $time
						WHERE sitepref_name = 'frontendlang'";
						$dbresult = $db->Execute($query, array($this->frontendlang));

						// return true
						return true;

					}

					public function Name() {
						return 'Siteprefs';
					}

				});
				
				// create the group
				// $result = new stdClass();
				$result->id = $id;

				while ($row = $dbresult->FetchRow()) {
					$sitepref_name = $row['sitepref_name'];
					$result->$sitepref_name = $row['sitepref_value'];
				}

			}

			// return
			return $result;

		}

	});

	// get the current settings
	$settings = $form->operations->LoadByID($id);

	// check access
	$form->access_add  = check_permission(get_userid(), 'admin_modify_site_preferences');
	$form->access_edit = $form->access_add;

	// show buttons or not
	$form->button_submit = $form->access_edit;
	$form->button_apply  = true;
	$form->button_cancel = $form->access_edit;

	// ------------------------------------
	// -- input fields
	// ------------------------------------

	// col 1
	$form_col_1 = array(

		array(
			'type'	=> 'input',
			'label'	=> lang('sitename'),
			'name' 	=> 'sitename',
			'value' => '',
			'validate' => array(
				'not_empty' => true,
				'value' => 'name'
			)
		),

		array(
			'type'	=> 'select',
			'label'	=> lang('language_default'),
			'name' 	=> 'frontendlang',
			'options' => array(
				array('value' => 'lv_LV', 'name' => lang('latvian')),
				array('value' => 'en_US', 'name' => lang('english'))
			),
			'selected' => $settings->frontendlang
		),

		array(
			'type'	=> 'select',
			'label'	=> lang('admintheme'),
			'name' 	=> 'logintheme',
			'options' => array(
				array('value' => 'NCleanGrey', 'name' => 'NCleanGrey')
			),
			'selected' => $settings->logintheme
		),

	);

	// ------------------------------------
	// -- merge groups
	// ------------------------------------

	// col 2
	$form_col_2 = '';

	// merg cols for latter use in POST
	$form_inputs_merge = array_merge(
		$form_col_1,
		( is_array($form_col_2) ? $form_col_2 : array() )
	);

	// create array of input names
	$form->inputs = create_form_input_list($form_inputs_merge);

	// ------------------------------------
	// -- define form and tabs
	// ------------------------------------

	$form->button_submit = false;

	// create the tabs
	$form->tabs = array(
		0 => array(
			'name' 		=> lang('siteprefs'),
			'content'	=> array(
				'col-1' => $form_col_1,
				'col-2' => $form_col_2
			)
		)
	);

	// return the form
	return $form;

}

// ------------------------------------
// -- form
// ------------------------------------

// get the form
$form = form_setup($id);

include_once(get_vcms_file("/admin/form.php"));





// -- siteprefs.php