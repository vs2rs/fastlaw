<?php

/* --------------------------------- *\
 
	Setup

\* --------------------------------- */

// our project
define('APP_NAME', 'fastlaw');

// load our app
require_once('app/app.php');

/* --------------------------------- *\
 
	Check Admin Login

\* --------------------------------- */

$CMS_ADMIN_PAGE = 1;

if(APP_HOSTNAME == "_localhost"
|| APP_HOSTNAME == "_www") {

	// setup some constants we need for checkup
	if(!defined('CONFIG_FILE_LOCATION')) {
		define('CONFIG_FILE_LOCATION', 		APP_CONFIG_FILE);
	}
	
	// include
	require_once(VCMS_PATH . "/include.php");

	// if this page was accessed directly, and the secure param name is not in the URL
	// but it is in the session, assume it is correct.
	if(isset($_SESSION[CMS_USER_KEY])
	&& !isset($_GET[CMS_SECURE_PARAM_NAME])) {
		$_GET[CMS_SECURE_PARAM_NAME] = $_SESSION[CMS_USER_KEY];
	}

	if(!check_login(true)) {
		echo 'No access!';
		exit;
	}

}

/* --------------------------------- *\
 
	Load

\* --------------------------------- */

// load section
Layout::load_section();





// -- index.php --