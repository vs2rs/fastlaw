-- MySQL dump 10.13  Distrib 5.7.26, for osx10.10 (x86_64)
--
-- Host: localhost    Database: work_makit_fastlaw
-- ------------------------------------------------------
-- Server version	5.7.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `fl_adminlog`
--

DROP TABLE IF EXISTS `fl_adminlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_adminlog` (
  `timestamp` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_adminlog`
--

LOCK TABLES `fl_adminlog` WRITE;
/*!40000 ALTER TABLE `fl_adminlog` DISABLE KEYS */;
INSERT INTO `fl_adminlog` VALUES (1636623273,1,'admin',1,'admin','User Login');
/*!40000 ALTER TABLE `fl_adminlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_content`
--

DROP TABLE IF EXISTS `fl_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_content` (
  `content_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_name` varchar(255) DEFAULT NULL,
  `type` varchar(25) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `hierarchy` varchar(255) DEFAULT NULL,
  `default_content` tinyint(4) DEFAULT NULL,
  `menu_text` varchar(255) DEFAULT NULL,
  `content_alias` varchar(255) DEFAULT NULL,
  `show_in_menu` tinyint(4) DEFAULT NULL,
  `collapsed` tinyint(4) DEFAULT NULL,
  `markup` varchar(25) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `cachable` tinyint(4) DEFAULT NULL,
  `id_hierarchy` varchar(255) DEFAULT NULL,
  `hierarchy_path` text,
  `prop_names` text,
  `metadata` text,
  `titleattribute` varchar(255) DEFAULT NULL,
  `tabindex` varchar(10) DEFAULT NULL,
  `accesskey` varchar(255) DEFAULT NULL,
  `last_modified_by` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `secure` tinyint(4) DEFAULT NULL,
  `page_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`content_id`),
  KEY `kab_index_content_by_content_alias_active` (`content_alias`,`active`),
  KEY `kab_index_content_by_default_content` (`default_content`),
  KEY `kab_index_content_by_parent_id` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_content`
--

LOCK TABLES `fl_content` WRITE;
/*!40000 ALTER TABLE `fl_content` DISABLE KEYS */;
INSERT INTO `fl_content` VALUES (1,'English','lang',1,-1,1,1,'00001',0,'ENG','en',1,NULL,'html',1,1,'1','en','content_en,site_name,author,target,image,thumbnail,extra1,extra2,extra3,searchable,pagedata,disable_wysiwyg,params','','','','en',1,'2020-08-06 08:48:52','2021-10-12 15:16:56',0,''),(2,'Home','content',1,1,2,1,'00001.00001',1,'Home','home',0,NULL,'html',1,1,'1.2','en/home','content_en,meta_title,meta_description,meta_keywords,facebook_title,facebook_description,facebook_image','','','','home',1,'2021-05-26 06:51:25','2021-11-04 14:37:28',0,''),(3,'A better way to\r\norganize a law practise','block',1,2,3,1,'00001.00001.00001',0,'Intro','intro',1,NULL,'html',1,1,'1.2.3','en/home/intro','content_en,cta_text,cta_link,img,bg_img','','','','home_intro',1,'2021-08-24 06:59:40','2021-10-12 16:44:35',0,''),(4,'Why choose Fast Law','block',1,2,4,2,'00001.00001.00002',0,'Why choose us','why-choose-us',1,NULL,'html',1,1,'1.2.4','en/home/why-choose-us','content_en','','','','home_why',2,'2021-08-24 07:02:54','2021-10-04 14:03:00',0,''),(5,'A better way to\r\norganize a law\r\npractise ','block',1,2,5,3,'00001.00001.00003',0,'Video','video',1,NULL,'html',1,1,'1.2.5','en/home/video','content_en,cta_text,img_video,video','','','','home_video',1,'2021-08-24 07:04:19','2021-10-17 09:27:31',0,''),(6,'Features','block',1,2,6,4,'00001.00001.00004',0,'Features','top-features',1,NULL,'html',1,1,'1.2.6','en/home/top-features','content_en','','','','home_features',1,'2021-08-24 07:05:15','2021-10-04 13:18:23',0,''),(7,'Flexible Pricing Plans','block',1,2,7,5,'00001.00001.00005',0,'Pricing','pricing-plans',1,NULL,'html',1,1,'1.2.7','en/home/pricing-plans','content_en,footer','','','','home_pricing',1,'2021-08-24 07:06:05','2021-10-04 12:02:16',0,''),(8,'Approved by\r\nprofessionals','block',1,2,8,6,'00001.00001.00006',0,'Professionals','professionals',1,NULL,'html',1,1,'1.2.8','en/home/professionals','content_en','','','','home_people',1,'2021-08-24 07:06:49','2021-10-04 12:02:33',0,''),(10,'Features','content',1,1,12,2,'00001.00002',0,'Features','features',1,NULL,'html',1,1,'1.10','en/features','content_en,meta_title,meta_description,meta_keywords,facebook_title,facebook_description,facebook_image','','','','features',1,'2021-08-24 07:10:36','2021-09-16 08:49:42',0,''),(11,'Pricing plans for everyone ','content',1,1,14,3,'00001.00003',0,'Pricing','pricing',1,NULL,'html',1,1,'1.11','en/pricing','content_en,meta_title,meta_description,meta_keywords,facebook_title,facebook_description,facebook_image','','','','pricing',1,'2021-08-24 07:11:04','2021-10-04 12:18:20',0,''),(12,'Have can we help?','content',1,1,13,4,'00001.00004',0,'FAQ','faq',1,NULL,'html',1,1,'1.12','en/faq','subtitle,content_en,search_text,no_results,valid_term,length,meta_title,meta_description,meta_keywords,facebook_title,facebook_description,facebook_image','','','','faq',1,'2021-08-24 07:11:15','2021-10-13 11:06:08',0,''),(13,'Get in touch ','content',1,1,11,5,'00001.00005',0,'Contacts','contacts',1,NULL,'html',1,1,'1.13','en/contacts','content_en,address,phone,email,meta_title,meta_description,meta_keywords,facebook_title,facebook_description,facebook_image','','','','contacts',1,'2021-08-24 07:11:28','2021-10-04 13:54:25',0,''),(14,'Features Module (EN)','modulepage',1,10,12,1,'00001.00002.00001',0,'Features Module (EN)','features-module-en',1,NULL,'html',1,1,'1.10.14','en/features/features-module-en','module_name,children,content_en','','','','features_module',1,'2021-09-29 07:38:53','2021-09-29 07:43:24',0,''),(15,'FAQ module (EN)','modulepage',1,12,13,1,'00001.00004.00001',0,'FAQ module (EN)','faq-module-en',1,NULL,'html',1,1,'1.12.15','en/faq/faq-module-en','module_name,children,content_en','','','','faq_module',1,'2021-10-01 15:32:32','2021-10-01 15:33:25',0,''),(16,'Get special offers and latest\r\nupdates on your email','block',1,1,15,9,'00001.00009',0,'Subscribe','subscribe',1,NULL,'html',1,1,'1.16','en/subscribe','content_en,img_desktop,img_mob','','','','subscribe',1,'2021-10-02 09:47:41','2021-10-12 08:15:03',0,''),(17,'Footer (EN)','block',1,1,16,10,'00001.00010',0,'Footer (EN)','footer',1,NULL,'html',1,1,'1.17','en/footer','content_en,copyright,menu_title_1,menu_title_2','','','','footer',1,'2021-10-04 06:37:23','2021-10-12 08:15:02',0,''),(20,'Latviešu','lang',1,-1,1,2,'00002',0,'LAT','lv',1,NULL,'html',1,1,'20','lv','content_en,site_name,author','','','','lv',1,'2021-10-04 21:16:06','2021-10-12 15:17:04',0,''),(21,'Footer (LV)','block',1,20,16,10,'00002.00010',0,'Footer (LV)','footer',1,NULL,'html',1,1,'20.21','lv/footer','content_en,copyright,menu_title_1,menu_title_2','','','','footer',1,'2021-10-04 21:17:14','2021-10-12 09:02:15',0,''),(23,'Header Link','block',1,1,17,11,'00001.00011',0,'Sign in','header-link',1,NULL,'html',1,1,'1.23','en/header-link','content_en,link','','','','header_link',1,'2021-10-08 07:03:52','2021-10-12 08:15:01',0,''),(24,'Header CTA','block',1,1,17,12,'00001.00012',0,'Try for free','header-cta',1,NULL,'html',1,1,'1.24','en/header-cta','content_en,link','','','','header_cta',1,'2021-10-08 07:06:49','2021-10-12 08:15:00',0,''),(25,'Header CTA','block',1,20,17,11,'00002.00011',0,'Izmēģini bez maksas','header-cta',1,NULL,'html',1,1,'20.25','lv/header-cta','content_en,link','','','','header_cta',1,'2021-10-08 07:29:47','2021-10-12 09:02:14',0,''),(26,'Sākums','content',1,20,2,1,'00002.00001',0,'Sākums','sakums',0,NULL,'html',1,1,'20.26','lv/sakums','content_en,meta_title,meta_description,meta_keywords,facebook_title,facebook_description,facebook_image','','','','home',1,'2021-10-08 07:37:57','2021-10-08 07:38:02',0,''),(27,'Vislabākā prakse kā organizēt\r\njuristu darbu','block',1,26,3,1,'00002.00001.00001',0,'Ievads','ievads',1,NULL,'html',1,1,'20.26.27','lv/sakums/ievads','content_en,cta_text,cta_link,img,bg_img','','','','home_intro',1,'2021-10-08 07:39:11','2021-10-12 16:48:53',0,''),(28,'Kāpēc izvēlēties Fast Law','block',1,26,4,2,'00002.00001.00002',0,'Kāpēc izvēlēties mūs','kapec-izveleties-mus',1,NULL,'html',1,1,'20.26.28','lv/sakums/kapec-izveleties-mus','content_en','','','','home_why',1,'2021-10-08 07:40:15','2021-10-08 07:40:15',0,''),(29,'Vislabākā prakse\r\nkā organizēt\r\njuristu darbu','block',1,26,5,3,'00002.00001.00003',0,'Video','video',1,NULL,'html',1,1,'20.26.29','lv/sakums/video','content_en,cta_text,img_video,video','','','','home_video',1,'2021-10-08 07:44:27','2021-10-12 16:58:34',0,''),(30,'Funkcijas','block',1,26,6,4,'00002.00001.00004',0,'Funkcijas','top-funkcijas',1,NULL,'html',1,1,'20.26.30','lv/sakums/top-funkcijas','content_en','','','','home_features',1,'2021-10-08 07:45:02','2021-10-08 07:48:07',0,''),(31,'Pielāgojamas cenas vajadzībām','block',1,26,7,5,'00002.00001.00005',0,'Cenas','cenu-plani',1,NULL,'html',1,1,'20.26.31','lv/sakums/cenu-plani','content_en,footer','','','','home_pricing',1,'2021-10-08 07:46:29','2021-10-08 09:02:10',0,''),(32,'Nozares profesionāļu atbalsts','block',1,26,8,6,'00002.00001.00006',0,'Profesionāļi','profesionali',1,NULL,'html',1,1,'20.26.32','lv/sakums/profesionali','content_en','','','','home_people',1,'2021-10-08 07:47:18','2021-10-08 07:47:18',0,''),(33,'Funkcijas','content',1,20,12,2,'00002.00002',0,'Funkcijas','funkcijas',1,NULL,'html',1,1,'20.33','lv/funkcijas','content_en,meta_title,meta_description,meta_keywords,facebook_title,facebook_description,facebook_image','','','','features',1,'2021-10-08 07:48:27','2021-10-08 07:48:33',0,''),(34,'Features Module (LV)','modulepage',1,33,12,1,'00002.00002.00001',0,'Features Module (LV)','features-module-lv',1,NULL,'html',1,1,'20.33.34','lv/funkcijas/features-module-lv','module_name,children,content_en','','','','features_module',1,'2021-10-08 07:49:07','2021-10-08 09:07:30',0,''),(35,'Pricing Module (EN)','modulepage',1,11,14,1,'00001.00003.00001',0,'Pricing Module (EN)','pricing-module-en',1,NULL,'html',1,1,'1.11.35','en/pricing/pricing-module-en','module_name,children,content_en','','','','pricing_module',1,'2021-10-08 08:24:00','2021-10-08 08:24:58',0,''),(36,'Pricing Full Table','block',1,11,18,2,'00001.00003.00002',0,'Pricing Full Table','pricing-full-table',1,NULL,'html',1,1,'1.11.36','en/pricing/pricing-full-table','content_en','','','','pricing_table',1,'2021-10-08 08:27:08','2021-10-08 09:00:59',0,''),(37,'Pricing FAQ','block',1,11,19,3,'00001.00003.00003',0,'Pricing FAQ','pricing-faq',1,NULL,'html',1,1,'1.11.37','en/pricing/pricing-faq','content_en','','','','pricing_faq',1,'2021-10-08 08:29:27','2021-10-08 09:00:53',0,''),(38,'Cenu plāni priekš visiem','content',1,20,14,3,'00002.00003',0,'Cenas','cenas',1,NULL,'html',1,1,'20.38','lv/cenas','content_en,meta_title,meta_description,meta_keywords,facebook_title,facebook_description,facebook_image','','','','pricing',1,'2021-10-08 09:02:40','2021-10-08 09:02:46',0,''),(39,'Pricing Module (LV)','modulepage',1,38,14,1,'00002.00003.00001',0,'Pricing Module (LV)','pricing-module-lv',1,NULL,'html',1,1,'20.38.39','lv/cenas/pricing-module-lv','module_name,children,content_en','','','','pricing_module',1,'2021-10-08 09:03:05','2021-10-08 09:06:56',0,''),(40,'Cenas pilnā tabula','block',1,38,18,2,'00002.00003.00002',0,'Cenas pilnā tabula','cenas-pilna-tabula',1,NULL,'html',1,1,'20.38.40','lv/cenas/cenas-pilna-tabula','content_en','','','','pricing_table',1,'2021-10-08 09:03:40','2021-10-08 09:03:40',0,''),(41,'Cenas BUJ','block',1,38,19,3,'00002.00003.00003',0,'Cenas BUJ','cenas-buj',1,NULL,'html',1,1,'20.38.41','lv/cenas/cenas-buj','content_en','','','','pricing_faq',1,'2021-10-08 09:03:58','2021-10-08 09:03:58',0,''),(42,'Kā mēs varam palīdzēt?','content',1,20,13,4,'00002.00004',0,'BUJ','buj',1,NULL,'html',1,1,'20.42','lv/buj','subtitle,content_en,search_text,no_results,valid_term,length,meta_title,meta_description,meta_keywords,facebook_title,facebook_description,facebook_image','','','','faq',1,'2021-10-08 09:04:39','2021-10-13 11:08:02',0,''),(43,'FAQ module (LV)','modulepage',1,42,13,1,'00002.00004.00001',0,'FAQ module (LV)','faq-module-lv',1,NULL,'html',1,1,'20.42.43','lv/buj/faq-module-lv','module_name,children,content_en','','','','faq_module',1,'2021-10-08 09:05:20','2021-10-08 09:07:47',0,''),(44,'Sazinies ar mums','content',1,20,11,5,'00002.00005',0,'Kontakti','kontakti',1,NULL,'html',1,1,'20.44','lv/kontakti','content_en,address,phone,email,meta_title,meta_description,meta_keywords,facebook_title,facebook_description,facebook_image','','','','contacts',1,'2021-10-08 09:08:20','2021-10-08 09:10:03',0,''),(45,'Saņem īpašos piedāvājumus un\r\naktualitātes e-pastā','block',1,20,15,9,'00002.00009',0,'Pieraksties','pieraksties',1,NULL,'html',1,1,'20.45','lv/pieraksties','content_en,img_desktop,img_mob','','','','subscribe',1,'2021-10-08 09:12:07','2021-10-12 09:02:15',0,''),(46,'Header Link','block',1,20,17,12,'00002.00012',0,'Ienākt','ienakt',1,NULL,'html',1,1,'20.46','lv/ienakt','content_en,link','','','','header_link',1,'2021-10-08 09:12:37','2021-10-12 09:02:13',0,''),(47,'Terms of Service','content',1,1,20,6,'00001.00006',0,'Terms of Service','terms-of-service',0,NULL,'html',1,1,'1.47','en/terms-of-service','content_en,meta_title,meta_description,meta_keywords,facebook_title,facebook_description,facebook_image','','','','terms',1,'2021-10-12 06:20:31','2021-10-12 08:17:43',0,''),(48,'Privacy Policy','content',1,1,20,7,'00001.00007',0,'Privacy Policy','privacy-policy',0,NULL,'html',1,1,'1.48','en/privacy-policy','content_en,meta_title,meta_description,meta_keywords,facebook_title,facebook_description,facebook_image','','','','privacy',1,'2021-10-12 08:12:36','2021-10-12 09:03:08',0,''),(49,'Site Map','content',1,1,21,8,'00001.00008',0,'Site Map','site-map',0,NULL,'html',1,1,'1.49','en/site-map','content_en','','','','sitemap',1,'2021-10-12 08:14:56','2021-10-12 09:03:14',0,''),(50,'Lapas karte','content',1,20,21,8,'00002.00008',0,'Lapas karte','lapas-karte',0,NULL,'html',1,1,'20.50','lv/lapas-karte','content_en','','','','sitemap',1,'2021-10-12 08:21:32','2021-10-12 09:02:17',0,''),(51,'Lietošanas noteikumi','content',1,20,20,6,'00002.00006',0,'Lietošanas noteikumi','lietosanas-noteikumi',0,NULL,'html',1,1,'20.51','lv/lietosanas-noteikumi','content_en,meta_title,meta_description,meta_keywords,facebook_title,facebook_description,facebook_image','','','','terms',1,'2021-10-12 09:01:39','2021-10-12 09:02:11',0,''),(52,'Privātuma politika','content',1,20,20,7,'00002.00007',0,'Privātuma politika','privatuma-politika',0,NULL,'html',1,1,'20.52','lv/privatuma-politika','content_en,meta_title,meta_description,meta_keywords,facebook_title,facebook_description,facebook_image','','','','privacy',1,'2021-10-12 09:01:59','2021-10-12 09:02:55',0,''),(53,'Cookies','content',1,1,22,13,'00001.00013',0,'Cookies','cookies',0,NULL,'html',1,1,'1.53','en/cookies','content_en,cookies_accept','','','','cookies',1,'2021-10-12 11:07:58','2021-10-12 11:51:05',0,''),(54,'Sīkdatnes','content',1,20,22,13,'00002.00013',0,'Sīkdatnes','sikdatnes',0,NULL,'html',1,1,'20.54','lv/sikdatnes','content_en,cookies_accept','','','','cookies',1,'2021-10-12 11:51:25','2021-10-12 11:53:02',0,'');
/*!40000 ALTER TABLE `fl_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_content_props`
--

DROP TABLE IF EXISTS `fl_content_props`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_content_props` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) DEFAULT NULL,
  `type` varchar(25) DEFAULT NULL,
  `prop_name` varchar(255) DEFAULT NULL,
  `param1` varchar(255) DEFAULT NULL,
  `param2` varchar(255) DEFAULT NULL,
  `param3` varchar(255) DEFAULT NULL,
  `content` text,
  `create_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `kab_index_content_props_by_content_id` (`content_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1021 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_content_props`
--

LOCK TABLES `fl_content_props` WRITE;
/*!40000 ALTER TABLE `fl_content_props` DISABLE KEYS */;
INSERT INTO `fl_content_props` VALUES (232,10,'string','content_en','','','','',NULL,'2021-09-16 08:49:42'),(233,10,'string','meta_title','','','','',NULL,'2021-09-16 08:49:42'),(234,10,'string','meta_description','','','','',NULL,'2021-09-16 08:49:42'),(235,10,'string','meta_keywords','','','','',NULL,'2021-09-16 08:49:42'),(236,10,'string','facebook_title','','','','',NULL,'2021-09-16 08:49:42'),(237,10,'string','facebook_description','','','','',NULL,'2021-09-16 08:49:42'),(238,10,'string','facebook_image','','','','',NULL,'2021-09-16 08:49:42'),(263,14,'string','module_name','','','','features',NULL,'2021-09-29 07:43:24'),(264,14,'string','children','','','','',NULL,'2021-09-29 07:43:24'),(265,14,'string','content_en','','','','items',NULL,'2021-09-29 07:43:24'),(276,15,'string','module_name','','','','faq',NULL,'2021-10-01 15:33:25'),(277,15,'string','children','','','','items',NULL,'2021-10-01 15:33:25'),(278,15,'string','content_en','','','','category',NULL,'2021-10-01 15:33:25'),(327,7,'string','content_en','','','','We are Fast Law. We are here to help you organize your law practice in one place from.',NULL,'2021-10-04 12:02:16'),(328,7,'string','footer','','','','For custom pricing plans <a href=\"contacts\">contact us</a>',NULL,'2021-10-04 12:02:16'),(329,8,'string','content_en','','','','We are Fast Law. We are here to help youorganize your law practice in one place fromsales to case management.',NULL,'2021-10-04 12:02:33'),(337,11,'string','content_en','','','','We are Fast Law. We are here to help you organize your law practice in one place from.\r\nHere are some pricing plan comperising.',NULL,'2021-10-04 12:18:20'),(338,11,'string','meta_title','','','','',NULL,'2021-10-04 12:18:20'),(339,11,'string','meta_description','','','','',NULL,'2021-10-04 12:18:20'),(340,11,'string','meta_keywords','','','','',NULL,'2021-10-04 12:18:20'),(341,11,'string','facebook_title','','','','',NULL,'2021-10-04 12:18:20'),(342,11,'string','facebook_description','','','','',NULL,'2021-10-04 12:18:20'),(343,11,'string','facebook_image','','','','',NULL,'2021-10-04 12:18:20'),(344,6,'string','content_en','','','','',NULL,'2021-10-04 13:18:23'),(363,13,'string','content_en','','','','If you have any questions just ask. We are here to help you. We’re happy to answer any questions you may have.',NULL,'2021-10-04 13:54:25'),(364,13,'string','address','','','','Some street name and even longer name 95, Riga, Latvia',NULL,'2021-10-04 13:54:25'),(365,13,'string','phone','','','','+371 28856988',NULL,'2021-10-04 13:54:25'),(366,13,'string','email','','','','random.name@fastlaw.com',NULL,'2021-10-04 13:54:25'),(367,13,'string','meta_title','','','','',NULL,'2021-10-04 13:54:25'),(368,13,'string','meta_description','','','','',NULL,'2021-10-04 13:54:25'),(369,13,'string','meta_keywords','','','','',NULL,'2021-10-04 13:54:25'),(370,13,'string','facebook_title','','','','',NULL,'2021-10-04 13:54:25'),(371,13,'string','facebook_description','','','','',NULL,'2021-10-04 13:54:25'),(372,13,'string','facebook_image','','','','',NULL,'2021-10-04 13:54:25'),(373,4,'string','content_en','','','','We are Fast Law. We are here to help you organize your law practice in one\r\nplace from sales to case management. ',NULL,'2021-10-04 14:03:00'),(396,16,'string','content_en','','','','',NULL,'2021-10-08 06:44:43'),(397,16,'string','img_desktop','','','','18',NULL,'2021-10-08 06:44:43'),(398,16,'string','img_mob','','','','17',NULL,'2021-10-08 06:44:43'),(423,24,'string','content_en','','','','',NULL,'2021-10-08 07:16:49'),(424,24,'string','link','','','','https://app.fastlaw.lv/registration',NULL,'2021-10-08 07:16:49'),(427,23,'string','content_en','','','','',NULL,'2021-10-08 07:16:53'),(428,23,'string','link','','','','https://app.fastlaw.lv/sign-in',NULL,'2021-10-08 07:16:53'),(435,25,'string','content_en','','','','',NULL,'2021-10-08 07:30:25'),(436,25,'string','link','','','','https://app.fastlaw.lv/registration',NULL,'2021-10-08 07:30:25'),(437,26,'string','content_en','','','','',NULL,'2021-10-08 07:37:57'),(438,26,'string','meta_title','','','','',NULL,'2021-10-08 07:37:57'),(439,26,'string','meta_description','','','','',NULL,'2021-10-08 07:37:57'),(440,26,'string','meta_keywords','','','','',NULL,'2021-10-08 07:37:57'),(441,26,'string','facebook_title','','','','',NULL,'2021-10-08 07:37:57'),(442,26,'string','facebook_description','','','','',NULL,'2021-10-08 07:37:57'),(443,26,'string','facebook_image','','','','',NULL,'2021-10-08 07:37:57'),(447,28,'string','content_en','','','','We are Fast Law. We are here to help you organize your law practice in one\r\nplace from sales to case management. ',NULL,'2021-10-08 07:40:15'),(457,32,'string','content_en','','','','We are Fast Law. We are here to help youorganize your law practice in one place fromsales to case management.',NULL,'2021-10-08 07:47:18'),(458,30,'string','content_en','','','','',NULL,'2021-10-08 07:48:07'),(459,33,'string','content_en','','','','',NULL,'2021-10-08 07:48:27'),(460,33,'string','meta_title','','','','',NULL,'2021-10-08 07:48:27'),(461,33,'string','meta_description','','','','',NULL,'2021-10-08 07:48:27'),(462,33,'string','meta_keywords','','','','',NULL,'2021-10-08 07:48:27'),(463,33,'string','facebook_title','','','','',NULL,'2021-10-08 07:48:27'),(464,33,'string','facebook_description','','','','',NULL,'2021-10-08 07:48:27'),(465,33,'string','facebook_image','','','','',NULL,'2021-10-08 07:48:27'),(472,35,'string','module_name','','','','pricing',NULL,'2021-10-08 08:24:58'),(473,35,'string','children','','','','',NULL,'2021-10-08 08:24:58'),(474,35,'string','content_en','','','','items',NULL,'2021-10-08 08:24:58'),(481,37,'string','content_en','','','','',NULL,'2021-10-08 09:00:53'),(483,36,'string','content_en','','','','',NULL,'2021-10-08 09:00:59'),(484,31,'string','content_en','','','','We are Fast Law. We are here to help you organize your law practice in one place from.',NULL,'2021-10-08 09:02:10'),(485,31,'string','footer','','','','For custom pricing plans <a href=\"contacts\">contact us</a>',NULL,'2021-10-08 09:02:10'),(486,38,'string','content_en','','','','We are Fast Law. We are here to help you organize your law practice in one place from.\r\nHere are some pricing plan comperising.',NULL,'2021-10-08 09:02:40'),(487,38,'string','meta_title','','','','',NULL,'2021-10-08 09:02:40'),(488,38,'string','meta_description','','','','',NULL,'2021-10-08 09:02:40'),(489,38,'string','meta_keywords','','','','',NULL,'2021-10-08 09:02:40'),(490,38,'string','facebook_title','','','','',NULL,'2021-10-08 09:02:40'),(491,38,'string','facebook_description','','','','',NULL,'2021-10-08 09:02:40'),(492,38,'string','facebook_image','','','','',NULL,'2021-10-08 09:02:40'),(495,40,'string','content_en','','','','',NULL,'2021-10-08 09:03:40'),(496,41,'string','content_en','','','','',NULL,'2021-10-08 09:03:58'),(521,39,'string','module_name','','','','pricing',NULL,'2021-10-08 09:06:56'),(522,39,'string','children','','','','',NULL,'2021-10-08 09:06:56'),(523,39,'string','content_en','','','','items',NULL,'2021-10-08 09:06:56'),(533,34,'string','module_name','','','','features',NULL,'2021-10-08 09:07:30'),(534,34,'string','children','','','','',NULL,'2021-10-08 09:07:30'),(535,34,'string','content_en','','','','items',NULL,'2021-10-08 09:07:30'),(539,43,'string','module_name','','','','faq',NULL,'2021-10-08 09:07:47'),(540,43,'string','children','','','','items',NULL,'2021-10-08 09:07:47'),(541,43,'string','content_en','','','','category',NULL,'2021-10-08 09:07:47'),(552,17,'string','content_en','','','','We are Fast Law. We are here to help you\r\norganize your law practice in one place from\r\nsales to case management. ',NULL,'2021-10-08 09:08:51'),(553,17,'string','copyright','','','','Fast Law Europe',NULL,'2021-10-08 09:08:51'),(554,17,'string','menu_title_1','','','','Products',NULL,'2021-10-08 09:08:51'),(555,17,'string','menu_title_2','','','','Support',NULL,'2021-10-08 09:08:51'),(566,44,'string','content_en','','','','Ja tev ir jautājumi, droši raksti mums. Mēs esam šeit lai tev palīdzētu. Mūsu komanda atbildēs tev 24h laikā.',NULL,'2021-10-08 09:10:03'),(567,44,'string','address','','','','Kaut kāda adrese gara adrese 95, Rīga, Latvija',NULL,'2021-10-08 09:10:03'),(568,44,'string','phone','','','','+371 28856988',NULL,'2021-10-08 09:10:03'),(569,44,'string','email','','','','random.name@fastlaw.com',NULL,'2021-10-08 09:10:03'),(570,44,'string','meta_title','','','','',NULL,'2021-10-08 09:10:03'),(571,44,'string','meta_description','','','','',NULL,'2021-10-08 09:10:03'),(572,44,'string','meta_keywords','','','','',NULL,'2021-10-08 09:10:03'),(573,44,'string','facebook_title','','','','',NULL,'2021-10-08 09:10:03'),(574,44,'string','facebook_description','','','','',NULL,'2021-10-08 09:10:03'),(575,44,'string','facebook_image','','','','',NULL,'2021-10-08 09:10:03'),(583,46,'string','content_en','','','','',NULL,'2021-10-08 09:12:37'),(584,46,'string','link','','','','https://app.fastlaw.lv/sign-in',NULL,'2021-10-08 09:12:37'),(607,45,'string','content_en','','','','',NULL,'2021-10-11 08:35:41'),(608,45,'string','img_desktop','','','','18',NULL,'2021-10-11 08:35:41'),(609,45,'string','img_mob','','','','17',NULL,'2021-10-11 08:35:41'),(819,47,'string','content_en','','','','<h2>1. Some subtitle just to check how it looks</h2>\r\n\r\n<h3>1.1. Subheader</h3>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur <a href=\"contacts\">adipiscing elit</a>. Donec eleifend, quam ut ornare tristique, dui lorem molestie magna, a placerat orci sem quis mi. Nunc bibendum, ligula in tristique gravida, turpis enim pretium nisl, a bibendum ante nulla ut est. Etiam cursus vestibulum ultrices. Nam a malesuada justo. Donec et metus purus. Maecenas lorem quam, blandit vel ex at, volutpat consectetur nisl. Phasellus ut feugiat felis. Morbi volutpat vestibulum turpis eu bibendum. Cras lobortis, enim non sodales consectetur, nisi dolor semper sem, quis molestie felis enim vel justo. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>\r\n\r\n<h3>1.2 Header number three for this list</h3>\r\n\r\n<ul>\r\n	<li>Maecenas consequat, lacus vitae eleifend hendrerit.</li>\r\n	<li>Mauris orci ultrices magna, ut vulputate.</li>\r\n	<li>Est lorem id lacus. Etiam nisi mi, semper at metus sed.</li>\r\n	<li>Eleifend dapibus ipsum. Nunc hendrerit nisl sed sem finibus.</li>\r\n	<li>Non ullamcorper enim semper.</li>\r\n</ul>\r\n\r\n<h2>2. Don&#39;t be alarmed</h2>\r\n\r\n<p>Aliquam vel ante magna. Vivamus quis justo eros. Integer rhoncus suscipit tellus at sagittis. Nunc magna risus, dictum sed turpis non, efficitur condimentum elit. Proin sagittis sapien vitae magna accumsan posuere.</p>\r\n\r\n<ol>\r\n	<li>Mauris porta pharetra erat, nec pretium tortor pellentesque et. Quisque sapien est, ultrices sit amet ligula eu, scelerisque congue odio. Ut blandit commodo rutrum. Vestibulum porta est in vehicula gravida.</li>\r\n	<li>Aenean mauris leo, pellentesque nec massa non, mollis commodo mi. Duis feugiat aliquam sapien quis efficitur. Vestibulum dignissim nisl euismod venenatis maximus. Nulla facilisi. Nam faucibus orci non justo venenatis placerat.</li>\r\n	<li>Nullam aliquet enim vel purus placerat, ut vulputate orci laoreet. Fusce pulvinar dolor sed pharetra sodales.</li>\r\n</ol>\r\n\r\n<p>Cras eget eros sem. Donec in quam turpis. Pellentesque varius dui in arcu aliquet, at euismod nulla egestas. Sed convallis ante in arcu blandit facilisis. Duis vitae congue ante. Donec sed tristique nisi. Integer porta tellus at fringilla aliquet.</p>\r\n',NULL,'2021-10-12 08:17:43'),(820,47,'string','meta_title','','','','',NULL,'2021-10-12 08:17:43'),(821,47,'string','meta_description','','','','',NULL,'2021-10-12 08:17:43'),(822,47,'string','meta_keywords','','','','',NULL,'2021-10-12 08:17:43'),(823,47,'string','facebook_title','','','','',NULL,'2021-10-12 08:17:43'),(824,47,'string','facebook_description','','','','',NULL,'2021-10-12 08:17:43'),(825,47,'string','facebook_image','','','','',NULL,'2021-10-12 08:17:43'),(827,50,'string','content_en','','','','',NULL,'2021-10-12 08:22:17'),(828,21,'string','content_en','','','','Mēs esam Fast Law. Mēs nodarbojamies ar\r\njuristu darbu atvieglošanu. Palīdzēsim ar\r\nvisu ko tu vien vari iedomāties.',NULL,'2021-10-12 08:40:45'),(829,21,'string','copyright','','','','Fast Law Europe',NULL,'2021-10-12 08:40:45'),(830,21,'string','menu_title_1','','','','Produkti',NULL,'2021-10-12 08:40:45'),(831,21,'string','menu_title_2','','','','Atbalsts',NULL,'2021-10-12 08:40:45'),(832,51,'string','content_en','','','','<h2>1. Some subtitle just to check how it looks</h2>\r\n\r\n<h3>1.1. Subheader</h3>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur <a href=\"contacts\">adipiscing elit</a>. Donec eleifend, quam ut ornare tristique, dui lorem molestie magna, a placerat orci sem quis mi. Nunc bibendum, ligula in tristique gravida, turpis enim pretium nisl, a bibendum ante nulla ut est. Etiam cursus vestibulum ultrices. Nam a malesuada justo. Donec et metus purus. Maecenas lorem quam, blandit vel ex at, volutpat consectetur nisl. Phasellus ut feugiat felis. Morbi volutpat vestibulum turpis eu bibendum. Cras lobortis, enim non sodales consectetur, nisi dolor semper sem, quis molestie felis enim vel justo. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>\r\n\r\n<h3>1.2 Header number three for this list</h3>\r\n\r\n<ul>\r\n	<li>Maecenas consequat, lacus vitae eleifend hendrerit.</li>\r\n	<li>Mauris orci ultrices magna, ut vulputate.</li>\r\n	<li>Est lorem id lacus. Etiam nisi mi, semper at metus sed.</li>\r\n	<li>Eleifend dapibus ipsum. Nunc hendrerit nisl sed sem finibus.</li>\r\n	<li>Non ullamcorper enim semper.</li>\r\n</ul>\r\n\r\n<h2>2. Don&#39;t be alarmed</h2>\r\n\r\n<p>Aliquam vel ante magna. Vivamus quis justo eros. Integer rhoncus suscipit tellus at sagittis. Nunc magna risus, dictum sed turpis non, efficitur condimentum elit. Proin sagittis sapien vitae magna accumsan posuere.</p>\r\n\r\n<ol>\r\n	<li>Mauris porta pharetra erat, nec pretium tortor pellentesque et. Quisque sapien est, ultrices sit amet ligula eu, scelerisque congue odio. Ut blandit commodo rutrum. Vestibulum porta est in vehicula gravida.</li>\r\n	<li>Aenean mauris leo, pellentesque nec massa non, mollis commodo mi. Duis feugiat aliquam sapien quis efficitur. Vestibulum dignissim nisl euismod venenatis maximus. Nulla facilisi. Nam faucibus orci non justo venenatis placerat.</li>\r\n	<li>Nullam aliquet enim vel purus placerat, ut vulputate orci laoreet. Fusce pulvinar dolor sed pharetra sodales.</li>\r\n</ol>\r\n\r\n<p>Cras eget eros sem. Donec in quam turpis. Pellentesque varius dui in arcu aliquet, at euismod nulla egestas. Sed convallis ante in arcu blandit facilisis. Duis vitae congue ante. Donec sed tristique nisi. Integer porta tellus at fringilla aliquet.</p>\r\n',NULL,'2021-10-12 09:01:39'),(833,51,'string','meta_title','','','','',NULL,'2021-10-12 09:01:39'),(834,51,'string','meta_description','','','','',NULL,'2021-10-12 09:01:39'),(835,51,'string','meta_keywords','','','','',NULL,'2021-10-12 09:01:39'),(836,51,'string','facebook_title','','','','',NULL,'2021-10-12 09:01:39'),(837,51,'string','facebook_description','','','','',NULL,'2021-10-12 09:01:39'),(838,51,'string','facebook_image','','','','',NULL,'2021-10-12 09:01:39'),(846,52,'string','content_en','','','','<h2>1. Some subtitle just to check how it looks</h2>\r\n\r\n<h3>1.1. Subheader</h3>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur <a href=\"contacts\">adipiscing elit</a>. Donec eleifend, quam ut ornare tristique, dui lorem molestie magna, a placerat orci sem quis mi. Nunc bibendum, ligula in tristique gravida, turpis enim pretium nisl, a bibendum ante nulla ut est. Etiam cursus vestibulum ultrices. Nam a malesuada justo. Donec et metus purus. Maecenas lorem quam, blandit vel ex at, volutpat consectetur nisl. Phasellus ut feugiat felis. Morbi volutpat vestibulum turpis eu bibendum. Cras lobortis, enim non sodales consectetur, nisi dolor semper sem, quis molestie felis enim vel justo. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>\r\n\r\n<h3>1.2 Header number three for this list</h3>\r\n\r\n<ul>\r\n	<li>Maecenas consequat, lacus vitae eleifend hendrerit.</li>\r\n	<li>Mauris orci ultrices magna, ut vulputate.</li>\r\n	<li>Est lorem id lacus. Etiam nisi mi, semper at metus sed.</li>\r\n	<li>Eleifend dapibus ipsum. Nunc hendrerit nisl sed sem finibus.</li>\r\n	<li>Non ullamcorper enim semper.</li>\r\n</ul>\r\n\r\n<h2>2. Don&#39;t be alarmed</h2>\r\n\r\n<p>Aliquam vel ante magna. Vivamus quis justo eros. Integer rhoncus suscipit tellus at sagittis. Nunc magna risus, dictum sed turpis non, efficitur condimentum elit. Proin sagittis sapien vitae magna accumsan posuere.</p>\r\n\r\n<ol>\r\n	<li>Mauris porta pharetra erat, nec pretium tortor pellentesque et. Quisque sapien est, ultrices sit amet ligula eu, scelerisque congue odio. Ut blandit commodo rutrum. Vestibulum porta est in vehicula gravida.</li>\r\n	<li>Aenean mauris leo, pellentesque nec massa non, mollis commodo mi. Duis feugiat aliquam sapien quis efficitur. Vestibulum dignissim nisl euismod venenatis maximus. Nulla facilisi. Nam faucibus orci non justo venenatis placerat.</li>\r\n	<li>Nullam aliquet enim vel purus placerat, ut vulputate orci laoreet. Fusce pulvinar dolor sed pharetra sodales.</li>\r\n</ol>\r\n\r\n<p>Cras eget eros sem. Donec in quam turpis. Pellentesque varius dui in arcu aliquet, at euismod nulla egestas. Sed convallis ante in arcu blandit facilisis. Duis vitae congue ante. Donec sed tristique nisi. Integer porta tellus at fringilla aliquet.</p>\r\n',NULL,'2021-10-12 09:02:55'),(847,52,'string','meta_title','','','','',NULL,'2021-10-12 09:02:55'),(848,52,'string','meta_description','','','','',NULL,'2021-10-12 09:02:55'),(849,52,'string','meta_keywords','','','','',NULL,'2021-10-12 09:02:55'),(850,52,'string','facebook_title','','','','',NULL,'2021-10-12 09:02:55'),(851,52,'string','facebook_description','','','','',NULL,'2021-10-12 09:02:55'),(852,52,'string','facebook_image','','','','',NULL,'2021-10-12 09:02:55'),(853,48,'string','content_en','','','','<h2>1. Some subtitle just to check how it looks</h2>\r\n\r\n<h3>1.1. Subheader</h3>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur <a href=\"contacts\">adipiscing elit</a>. Donec eleifend, quam ut ornare tristique, dui lorem molestie magna, a placerat orci sem quis mi. Nunc bibendum, ligula in tristique gravida, turpis enim pretium nisl, a bibendum ante nulla ut est. Etiam cursus vestibulum ultrices. Nam a malesuada justo. Donec et metus purus. Maecenas lorem quam, blandit vel ex at, volutpat consectetur nisl. Phasellus ut feugiat felis. Morbi volutpat vestibulum turpis eu bibendum. Cras lobortis, enim non sodales consectetur, nisi dolor semper sem, quis molestie felis enim vel justo. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>\r\n\r\n<h3>1.2 Header number three for this list</h3>\r\n\r\n<ul>\r\n	<li>Maecenas consequat, lacus vitae eleifend hendrerit.</li>\r\n	<li>Mauris orci ultrices magna, ut vulputate.</li>\r\n	<li>Est lorem id lacus. Etiam nisi mi, semper at metus sed.</li>\r\n	<li>Eleifend dapibus ipsum. Nunc hendrerit nisl sed sem finibus.</li>\r\n	<li>Non ullamcorper enim semper.</li>\r\n</ul>\r\n\r\n<h2>2. Don&#39;t be alarmed</h2>\r\n\r\n<p>Aliquam vel ante magna. Vivamus quis justo eros. Integer rhoncus suscipit tellus at sagittis. Nunc magna risus, dictum sed turpis non, efficitur condimentum elit. Proin sagittis sapien vitae magna accumsan posuere.</p>\r\n\r\n<ol>\r\n	<li>Mauris porta pharetra erat, nec pretium tortor pellentesque et. Quisque sapien est, ultrices sit amet ligula eu, scelerisque congue odio. Ut blandit commodo rutrum. Vestibulum porta est in vehicula gravida.</li>\r\n	<li>Aenean mauris leo, pellentesque nec massa non, mollis commodo mi. Duis feugiat aliquam sapien quis efficitur. Vestibulum dignissim nisl euismod venenatis maximus. Nulla facilisi. Nam faucibus orci non justo venenatis placerat.</li>\r\n	<li>Nullam aliquet enim vel purus placerat, ut vulputate orci laoreet. Fusce pulvinar dolor sed pharetra sodales.</li>\r\n</ol>\r\n\r\n<p>Cras eget eros sem. Donec in quam turpis. Pellentesque varius dui in arcu aliquet, at euismod nulla egestas. Sed convallis ante in arcu blandit facilisis. Duis vitae congue ante. Donec sed tristique nisi. Integer porta tellus at fringilla aliquet.</p>\r\n',NULL,'2021-10-12 09:03:08'),(854,48,'string','meta_title','','','','',NULL,'2021-10-12 09:03:08'),(855,48,'string','meta_description','','','','',NULL,'2021-10-12 09:03:08'),(856,48,'string','meta_keywords','','','','',NULL,'2021-10-12 09:03:08'),(857,48,'string','facebook_title','','','','',NULL,'2021-10-12 09:03:08'),(858,48,'string','facebook_description','','','','',NULL,'2021-10-12 09:03:08'),(859,48,'string','facebook_image','','','','',NULL,'2021-10-12 09:03:08'),(860,49,'string','content_en','','','','',NULL,'2021-10-12 09:03:14'),(867,53,'string','content_en','','','','This website uses cookies to ensure you get the best experience on our website.',NULL,'2021-10-12 11:51:05'),(868,53,'string','cookies_accept','','','','Accept',NULL,'2021-10-12 11:51:05'),(875,54,'string','content_en','','','','Šī mājaslapa izmanto sīkdatnes, lai nodrošinātu jums kvalitatīvu un atbilstošu saturu.',NULL,'2021-10-12 11:53:02'),(876,54,'string','cookies_accept','','','','Piekrītu',NULL,'2021-10-12 11:53:02'),(877,1,'string','content_en','','','','',NULL,'2021-10-12 15:16:56'),(878,1,'string','site_name','','','','Fast Law',NULL,'2021-10-12 15:16:56'),(879,1,'string','author','','','','Fast Law',NULL,'2021-10-12 15:16:56'),(880,1,'string','target','','','','',NULL,'2021-10-12 15:16:56'),(881,1,'string','image','','','','',NULL,'2021-10-12 15:16:56'),(882,1,'string','thumbnail','','','','',NULL,'2021-10-12 15:16:56'),(883,1,'string','extra1','','','','',NULL,'2021-10-12 15:16:56'),(884,1,'string','extra2','','','','',NULL,'2021-10-12 15:16:56'),(885,1,'string','extra3','','','','',NULL,'2021-10-12 15:16:56'),(886,1,'string','searchable','','','','1',NULL,'2021-10-12 15:16:56'),(887,1,'string','pagedata','','','','',NULL,'2021-10-12 15:16:56'),(888,1,'string','disable_wysiwyg','','','','0',NULL,'2021-10-12 15:16:56'),(889,1,'string','params','','','','',NULL,'2021-10-12 15:16:56'),(890,20,'string','content_en','','','','',NULL,'2021-10-12 15:17:04'),(891,20,'string','site_name','','','','Fast Law',NULL,'2021-10-12 15:17:04'),(892,20,'string','author','','','','Fast Law',NULL,'2021-10-12 15:17:04'),(902,3,'string','content_en','','','','We are Fast Law. We are here to help you organize your law practice in one\r\nplace from sales to case management.',NULL,'2021-10-12 16:44:35'),(903,3,'string','cta_text','','','','Get started',NULL,'2021-10-12 16:44:35'),(904,3,'string','cta_link','','','','prices',NULL,'2021-10-12 16:44:35'),(905,3,'string','img','','','','1',NULL,'2021-10-12 16:44:35'),(906,3,'string','bg_img','','','','2',NULL,'2021-10-12 16:44:35'),(912,27,'string','content_en','','','','We are Fast Law. We are here to help you organize your law practice in one\r\nplace from sales to case management.',NULL,'2021-10-12 16:48:53'),(913,27,'string','cta_text','','','','Sāc darbu tūlīt',NULL,'2021-10-12 16:48:53'),(914,27,'string','cta_link','','','','lv/cenas',NULL,'2021-10-12 16:48:53'),(915,27,'string','img','','','','1',NULL,'2021-10-12 16:48:53'),(916,27,'string','bg_img','','','','2',NULL,'2021-10-12 16:48:53'),(936,29,'string','content_en','','','','We are Fast Law. We are here to help you organize your law practice in one place from sales to case management.',NULL,'2021-10-12 16:58:34'),(937,29,'string','cta_text','','','','Skaties un uzzini vairāk',NULL,'2021-10-12 16:58:34'),(938,29,'string','img_video','','','','19',NULL,'2021-10-12 16:58:34'),(939,29,'string','video','','','','',NULL,'2021-10-12 16:58:34'),(958,12,'string','subtitle','','','','Still have a questions?',NULL,'2021-10-13 11:06:08'),(959,12,'string','content_en','','','','If you cannot find answer to your question in our FAQ, you can always contact us via this e-mail fast.law@firm.com. We will answer to you shortly.',NULL,'2021-10-13 11:06:08'),(960,12,'string','search_text','','','','Write something...',NULL,'2021-10-13 11:06:08'),(961,12,'string','no_results','','','','No results',NULL,'2021-10-13 11:06:08'),(962,12,'string','valid_term','','','','Please enter a valid serach term',NULL,'2021-10-13 11:06:08'),(963,12,'string','length','','','','Please enter at least 3 characters',NULL,'2021-10-13 11:06:08'),(964,12,'string','meta_title','','','','',NULL,'2021-10-13 11:06:08'),(965,12,'string','meta_description','','','','',NULL,'2021-10-13 11:06:08'),(966,12,'string','meta_keywords','','','','',NULL,'2021-10-13 11:06:08'),(967,12,'string','facebook_title','','','','',NULL,'2021-10-13 11:06:08'),(968,12,'string','facebook_description','','','','',NULL,'2021-10-13 11:06:08'),(969,12,'string','facebook_image','','','','',NULL,'2021-10-13 11:06:08'),(970,42,'string','subtitle','','','','Vai tev ir vēl jautājumi?',NULL,'2021-10-13 11:08:02'),(971,42,'string','content_en','','','','If you cannot find answer to your question in our FAQ, you can always contact us via this e-mail fast.law@firm.com. We will answer to you shortly.',NULL,'2021-10-13 11:08:02'),(972,42,'string','search_text','','','','Ievadi meklēšanas frāzi...',NULL,'2021-10-13 11:08:02'),(973,42,'string','no_results','','','','Nav rezultātu',NULL,'2021-10-13 11:08:02'),(974,42,'string','valid_term','','','','Lūdzu ievadiet atbilstošu meklēšanas frāzi',NULL,'2021-10-13 11:08:02'),(975,42,'string','length','','','','Lūdzu ievadiet vismaz 3 simbolus',NULL,'2021-10-13 11:08:02'),(976,42,'string','meta_title','','','','',NULL,'2021-10-13 11:08:02'),(977,42,'string','meta_description','','','','',NULL,'2021-10-13 11:08:02'),(978,42,'string','meta_keywords','','','','',NULL,'2021-10-13 11:08:02'),(979,42,'string','facebook_title','','','','',NULL,'2021-10-13 11:08:02'),(980,42,'string','facebook_description','','','','',NULL,'2021-10-13 11:08:02'),(981,42,'string','facebook_image','','','','',NULL,'2021-10-13 11:08:02'),(1010,5,'string','content_en','','','','We are Fast Law. We are here to help you organize your law practice in one place from sales to case management.',NULL,'2021-10-17 09:27:31'),(1011,5,'string','cta_text','','','','Watch how it works',NULL,'2021-10-17 09:27:31'),(1012,5,'string','img_video','','','','19',NULL,'2021-10-17 09:27:31'),(1013,5,'string','video','','','','https://vimeo.com/620172276',NULL,'2021-10-17 09:27:31'),(1014,2,'string','content_en','','','','',NULL,'2021-11-04 14:37:28'),(1015,2,'string','meta_title','','','','',NULL,'2021-11-04 14:37:28'),(1016,2,'string','meta_description','','','','',NULL,'2021-11-04 14:37:28'),(1017,2,'string','meta_keywords','','','','',NULL,'2021-11-04 14:37:28'),(1018,2,'string','facebook_title','','','','Fastlaw',NULL,'2021-11-04 14:37:28'),(1019,2,'string','facebook_description','','','','Fastlaw',NULL,'2021-11-04 14:37:28'),(1020,2,'string','facebook_image','','','','2',NULL,'2021-11-04 14:37:28');
/*!40000 ALTER TABLE `fl_content_props` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_group_perms`
--

DROP TABLE IF EXISTS `fl_group_perms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_group_perms` (
  `group_perm_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`group_perm_id`),
  KEY `kab_index_group_perms_by_group_id_permission_id` (`group_id`,`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_group_perms`
--

LOCK TABLES `fl_group_perms` WRITE;
/*!40000 ALTER TABLE `fl_group_perms` DISABLE KEYS */;
INSERT INTO `fl_group_perms` VALUES (84,2,40,'2021-10-24 08:31:55','2021-10-24 08:31:55'),(85,2,41,'2021-10-24 08:31:55','2021-10-24 08:31:55'),(86,2,31,'2021-10-24 08:31:55','2021-10-24 08:31:55'),(87,2,32,'2021-10-24 08:31:55','2021-10-24 08:31:55'),(88,2,28,'2021-10-24 08:31:55','2021-10-24 08:31:55'),(89,2,29,'2021-10-24 08:31:55','2021-10-24 08:31:55'),(90,2,38,'2021-10-24 08:31:55','2021-10-24 08:31:55'),(91,2,53,'2021-10-24 08:31:55','2021-10-24 08:31:55'),(92,2,50,'2021-10-24 08:31:55','2021-10-24 08:31:55'),(93,2,23,'2021-10-24 08:31:55','2021-10-24 08:31:55'),(94,2,47,'2021-10-24 08:31:55','2021-10-24 08:31:55'),(95,2,44,'2021-10-24 08:31:55','2021-10-24 08:31:55'),(96,2,8,'2021-10-24 08:31:55','2021-10-24 08:31:55'),(97,2,11,'2021-10-24 08:31:55','2021-10-24 08:31:55'),(98,2,17,'2021-10-24 08:31:55','2021-10-24 08:31:55');
/*!40000 ALTER TABLE `fl_group_perms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_groups`
--

DROP TABLE IF EXISTS `fl_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `group_default` tinyint(1) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_groups`
--

LOCK TABLES `fl_groups` WRITE;
/*!40000 ALTER TABLE `fl_groups` DISABLE KEYS */;
INSERT INTO `fl_groups` VALUES (1,'Administrator',1,0,'2006-07-25 21:22:32','2021-11-08 09:14:40'),(2,'User',1,1,'2006-07-25 21:22:32','2020-07-17 12:42:52');
/*!40000 ALTER TABLE `fl_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_images`
--

DROP TABLE IF EXISTS `fl_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` varchar(255) NOT NULL,
  `src` varchar(255) NOT NULL,
  `alt_en` varchar(255) DEFAULT NULL,
  `alt_lv` varchar(255) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `active` tinyint(1) NOT NULL,
  `item_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_images`
--

LOCK TABLES `fl_images` WRITE;
/*!40000 ALTER TABLE `fl_images` DISABLE KEYS */;
INSERT INTO `fl_images` VALUES (1,'pages','intro-screen.png',NULL,NULL,'2021-09-24 15:52:20',1,NULL),(2,'pages','patrik-gothe-xitfeni0dmy-unsplash.jpg',NULL,NULL,'2021-09-24 15:52:27',1,NULL),(3,'features','invoices.png','Inovations screenshot from the FastLaw app','Inovāciju skats no FastLaw aplikācijas','2021-09-29 06:55:49',1,NULL),(4,'features','feature-1.png',NULL,NULL,'2021-09-29 09:54:08',1,NULL),(5,'features','invoices-screen-1.png',NULL,NULL,'2021-09-30 09:58:55',1,NULL),(6,'features','invoices-screen-2.png',NULL,NULL,'2021-09-30 09:58:55',1,NULL),(7,'features','iphone-xc-mockup.png',NULL,NULL,'2021-10-01 06:47:13',1,NULL),(8,'icons','icon1.svg',NULL,NULL,'2021-10-01 07:06:17',1,NULL),(9,'icons','icon2.svg',NULL,NULL,'2021-10-01 07:06:17',1,NULL),(10,'icons','icon3.svg',NULL,NULL,'2021-10-01 07:06:17',1,NULL),(11,'icons','icon4.svg',NULL,NULL,'2021-10-01 07:06:17',1,NULL),(12,'icons','icon5.svg',NULL,NULL,'2021-10-01 07:06:17',1,NULL),(13,'icons','icon6.svg',NULL,NULL,'2021-10-01 07:06:17',1,NULL),(14,'team','people-2.png',NULL,NULL,'2021-10-02 08:11:20',1,NULL),(15,'team','people-1.png',NULL,NULL,'2021-10-02 08:11:20',1,NULL),(16,'team','people-3.png',NULL,NULL,'2021-10-02 08:11:20',1,NULL),(17,'pages','subscribe-mob.png',NULL,NULL,'2021-10-08 06:44:31',1,NULL),(18,'pages','subscribe.png',NULL,NULL,'2021-10-08 06:44:31',1,NULL),(19,'pages','campaign-creators-pypeceajezy-unsplash-scaled.jpg',NULL,NULL,'2021-10-08 07:43:06',1,NULL);
/*!40000 ALTER TABLE `fl_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_images_folders`
--

DROP TABLE IF EXISTS `fl_images_folders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_images_folders` (
  `id` varchar(255) NOT NULL,
  `folder` varchar(255) NOT NULL,
  `file_types` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_images_folders`
--

LOCK TABLES `fl_images_folders` WRITE;
/*!40000 ALTER TABLE `fl_images_folders` DISABLE KEYS */;
INSERT INTO `fl_images_folders` VALUES ('features','/features/','jpg,gif,png'),('icons','/icons/','png,svg'),('pages','/pages/',NULL),('team','/team/','jpg,gif,png');
/*!40000 ALTER TABLE `fl_images_folders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_module_deps`
--

DROP TABLE IF EXISTS `fl_module_deps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_module_deps` (
  `parent_module` varchar(25) DEFAULT NULL,
  `child_module` varchar(25) DEFAULT NULL,
  `minimum_version` varchar(25) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_module_deps`
--

LOCK TABLES `fl_module_deps` WRITE;
/*!40000 ALTER TABLE `fl_module_deps` DISABLE KEYS */;
/*!40000 ALTER TABLE `fl_module_deps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_module_faq_category`
--

DROP TABLE IF EXISTS `fl_module_faq_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_module_faq_category` (
  `show_main` tinyint(1) DEFAULT NULL,
  `show_main_lv` tinyint(1) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `title_lv` varchar(255) DEFAULT NULL,
  `info` text,
  `info_lv` text,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `name_lv` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `alias_lv` varchar(255) DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `active_en` tinyint(1) DEFAULT NULL,
  `active_lv` tinyint(1) DEFAULT NULL,
  `isdefault` tinyint(4) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_module_faq_category`
--

LOCK TABLES `fl_module_faq_category` WRITE;
/*!40000 ALTER TABLE `fl_module_faq_category` DISABLE KEYS */;
INSERT INTO `fl_module_faq_category` VALUES (1,1,NULL,NULL,'We are Fast Law. We are here to help you organize your law practice in one place from.','We are Fast Law. We are here to help you organize your law practice in one place from.',1,'Geting started','Pamta lietošana','geting-started','pamta-lietosana',1,1,1,1,0,'2021-10-01 14:59:57','2021-10-01 09:43:46'),(1,1,'Questions about pricing and plans','Jautājumi par cenām un cenu plāniem','We are Fast Law. We are here to help you organize your law practice in one place from.','We are Fast Law. We are here to help you organize your law practice in one place from.',2,'Pricing and Plans','Cenu plāni','pricing-plans','cenu-plani',2,1,1,1,0,'2021-10-01 18:17:29','2021-10-01 09:56:45'),(1,1,NULL,NULL,NULL,NULL,3,'Managing my account','Profila rediģēšana','managing-my-account','profila-redigesana',3,1,1,1,0,'2021-10-01 14:15:48','2021-10-01 09:57:22'),(1,1,NULL,NULL,NULL,NULL,4,'Other topics','Citi jautājumi','other-topics','citi-jautajumi',4,1,1,1,0,'2021-10-01 14:15:54','2021-10-01 09:57:39'),(1,1,'Questions about all our features','Jautājumi par mūsu aplikācijas funkcijām','We are Fast Law. We are here to help you organize your law practice in one place from.','Šeit ir tikai demo teksts, jo pagaidām nav īstie jautājumi.',5,'Features overview','Pamata funkcijas','features-overview','pamata-funkcijas',5,1,1,1,0,'2021-10-01 18:18:37','2021-10-01 09:58:32');
/*!40000 ALTER TABLE `fl_module_faq_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_module_faq_items`
--

DROP TABLE IF EXISTS `fl_module_faq_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_module_faq_items` (
  `info` text,
  `info_lv` text,
  `parent` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `name_lv` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `active_en` tinyint(1) DEFAULT NULL,
  `active_lv` tinyint(1) DEFAULT NULL,
  `isdefault` tinyint(4) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_module_faq_items`
--

LOCK TABLES `fl_module_faq_items` WRITE;
/*!40000 ALTER TABLE `fl_module_faq_items` DISABLE KEYS */;
INSERT INTO `fl_module_faq_items` VALUES ('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.','Latvie&scaron;u. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.',1,1,'Some question about this topic?','Kaut kāds jautājums par šo tematu!','some-question-about-this-topic',1,1,1,1,0,'2021-10-13 08:32:05','2021-10-01 10:19:09'),('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.',1,2,'It uses a dictionary of over 200 Latin words? ','Vai tas ir ok izmantot vārdnīcu vai nav?','it-uses-a-dictionary-of-over-200-latin-words',2,1,1,1,0,'2021-10-01 13:19:55','2021-10-01 13:17:39'),('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.',1,3,'Lorem Ipsum has been the industry\\\'s standard dummy text ever since the 1500s? ','Lorem Ipsum ir industrijas standarts kopš 1500. gada?','lorem-ipsum-has-been-the-industry-s-standard-dummy-text-ever-since-the-1500s',3,1,1,1,0,'2021-10-01 13:20:02','2021-10-01 13:18:19'),('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.',1,4,'Aldus PageMaker including versions of Lorem Ipsum?','Vai Alids LapTaisis zina kas ir Lorem Ipsum? ','aldus-pagemaker-including-versions-of-lorem-ipsum',4,1,1,1,0,'2021-10-01 13:20:13','2021-10-01 13:19:08'),('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.',1,5,'Various versions have evolved over the years? ','Dažādas versijas ir cauri gadu gadiem radušās?','various-versions-have-evolved-over-the-years',5,1,1,1,0,'2021-10-01 13:20:23','2021-10-01 13:19:41'),('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.',5,6,'Some question about this topic?','Kaut kāds jautājums par šo tematu?','some-question-about-this-topic-1',1,1,1,1,0,'2021-10-01 13:23:08','2021-10-01 13:21:22'),('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.',5,7,'It uses a dictionary of over 200 Latin words?','Atkal man ir jautājums par to vārdnīcu un 200 vārdiem?','it-uses-a-dictionary-of-over-200-latin-words-1',2,1,1,1,0,'2021-10-01 13:23:17','2021-10-01 13:21:51'),('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.',5,8,'Some question about this topic and something more?','Kaut kāds cits jautājums par tieši to pašu tēmu ko iepriekš?','some-question-about-this-topic-and-something-more',3,1,1,1,0,'2021-10-01 13:23:28','2021-10-01 13:22:24'),('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.',5,9,'It uses a dictionary of over?','Man ir vārdnīca, ko tālāk?','it-uses-a-dictionary-of-over',4,1,1,1,0,'2021-10-01 13:23:49','2021-10-01 13:22:51'),('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.<br />\r\n&nbsp;',2,10,'Some question about pricing? ','Kaut kāds jautājums par cenām?','some-question-about-pricing',1,1,1,1,0,'2021-10-01 17:58:27','2021-10-01 17:58:27'),('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.',2,11,'If i pay you some money will i get some features?','Ja es iedošu jums bišķi naudiņu vai jums tas patiks?','if-i-pay-you-some-money-will-i-get-some-features',2,1,1,1,0,'2021-10-01 18:00:12','2021-10-01 18:00:12'),('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.',2,12,'Some other question about the pricing?','Šis ir cits jautājums par cenu tabulām?','some-other-question-about-the-pricing',3,1,1,1,0,'2021-10-01 18:00:53','2021-10-01 18:00:53'),('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit mattis dui a laoreet. Aenean eleifend volutpat turpis id mollis. Cras consectetur tempor diam, a mattis ipsum aliquam eu. Phasellus vestibulum accumsan dolor id molestie. Sed pulvinar a erat sit amet facilisis. Sed molestie tincidunt lacus, eget placerat nulla scelerisque vel. Donec vel convallis felis, non fringilla metus. Mauris pellentesque ut lorem convallis dapibus. In porta lorem nec magna venenatis, ac venenatis orci rutrum. Mauris mauris augue, ultrices et mauris in, maximus ultricies tellus. Quisque cursus et ipsum in sollicitudin. Curabitur vel volutpat nulla. Curabitur at placerat arcu, vitae pellentesque felis.<br />\r\n<br />\r\nAenean nibh ipsum, ornare at condimentum in, vestibulum eget leo. Aenean auctor luctus vehicula. Donec vitae dui ac lectus varius sollicitudin. Proin risus mi, congue vitae suscipit eget, suscipit sit amet lorem. In elementum hendrerit odio et placerat. Maecenas efficitur massa odio, ut vulputate urna ornare vel. Etiam congue nibh eget ornare tempor. Mauris vel ullamcorper dolor, at rhoncus risus. Ut ante leo, venenatis eu dui quis, commodo vehicula urna. Sed eleifend mattis risus congue hendrerit. Fusce eu mauris ut massa viverra malesuada.',2,13,'Can i pay with cash?','Vai es varu maksāt ar skaidru naudu?','can-i-pay-with-cash',4,1,1,1,0,'2021-10-01 18:01:42','2021-10-01 18:01:42');
/*!40000 ALTER TABLE `fl_module_faq_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_module_features_category`
--

DROP TABLE IF EXISTS `fl_module_features_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_module_features_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `isdefault` tinyint(4) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_module_features_category`
--

LOCK TABLES `fl_module_features_category` WRITE;
/*!40000 ALTER TABLE `fl_module_features_category` DISABLE KEYS */;
INSERT INTO `fl_module_features_category` VALUES (1,'Features','features',1,1,0,'2021-09-28 13:21:39','2021-09-28 13:21:39');
/*!40000 ALTER TABLE `fl_module_features_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_module_features_icons_category`
--

DROP TABLE IF EXISTS `fl_module_features_icons_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_module_features_icons_category` (
  `img` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `name_lv` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `isdefault` tinyint(4) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_module_features_icons_category`
--

LOCK TABLES `fl_module_features_icons_category` WRITE;
/*!40000 ALTER TABLE `fl_module_features_icons_category` DISABLE KEYS */;
INSERT INTO `fl_module_features_icons_category` VALUES (7,1,'Responsive and mobile\r\nfriendly user experience','Responsīvs un mobilajiem\r\npielāgots risinājums','responsive-and-mobile-friendly-user-experience',1,1,0,'2021-10-01 09:12:19','2021-10-01 06:27:07');
/*!40000 ALTER TABLE `fl_module_features_icons_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_module_features_icons_items`
--

DROP TABLE IF EXISTS `fl_module_features_icons_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_module_features_icons_items` (
  `img` varchar(255) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `name_lv` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `isdefault` tinyint(4) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_module_features_icons_items`
--

LOCK TABLES `fl_module_features_icons_items` WRITE;
/*!40000 ALTER TABLE `fl_module_features_icons_items` DISABLE KEYS */;
INSERT INTO `fl_module_features_icons_items` VALUES ('11',1,1,'Easily create invoices based on the quotes you have sent to your clients.','Kaut kāds teksts latviski šeit tiek ievadīts. Tikai demo teksts šis ir.','easily-create-invoices-based-on-the-quotes-you-have-sent-to-your-clients',1,1,0,'2021-10-01 07:08:43','2021-10-01 07:08:39'),('8',1,2,'Automatically send them by email as a PDF attachment or print and send them by mail. ','Kaut kāds teksts latviski šeit tiek ievadīts. Tikai demo teksts šis ir.','automatically-send-them-by-email-as-a-pdf-attachment-or-print-and-send-them-by-mail',2,1,0,'2021-10-01 07:09:43','2021-10-01 07:09:43'),('9',1,3,'Send professional looking invoices directly to your clients in just a click. ','Kaut kāds teksts latviski šeit tiek ievadīts. Tikai demo teksts šis ir.','send-professional-looking-invoices-directly-to-your-clients-in-just-a-click',3,1,0,'2021-10-01 07:10:12','2021-10-01 07:10:12'),('10',1,4,'We are Fast Law. We are here to help you organize your law. ','Kaut kāds teksts latviski šeit tiek ievadīts. Tikai demo teksts šis ir.','we-are-fast-law-we-are-here-to-help-you-organize-your-law',4,1,0,'2021-10-01 07:10:25','2021-10-01 07:10:25');
/*!40000 ALTER TABLE `fl_module_features_icons_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_module_features_images_category`
--

DROP TABLE IF EXISTS `fl_module_features_images_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_module_features_images_category` (
  `info` text,
  `info_lv` text,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `name_lv` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `isdefault` tinyint(4) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_module_features_images_category`
--

LOCK TABLES `fl_module_features_images_category` WRITE;
/*!40000 ALTER TABLE `fl_module_features_images_category` DISABLE KEYS */;
INSERT INTO `fl_module_features_images_category` VALUES ('Lorem ipsum dolor sit amet duis a velit ipsum. Maecenas congue mollis varius. Vivamus nec eros id arcu interdum ornare.','Lorem ipsum dolor sit amet duis a velit ipsum. Maecenas congue mollis varius. Vivamus nec eros id arcu interdum ornare.',1,'All necessary tools for a law practice','Visas nepieciešamās funkcijas juristiem','all-necessary-tools-for-a-law-practice',1,1,0,'2021-09-29 08:59:00','2021-09-29 08:50:48'),('Lorem ipsum dolor sit amet duis a velit ipsum. Maecenas congue mollis varius. Vivamus nec eros id arcu interdum ornare.','Lorem ipsum dolor sit amet duis a velit ipsum. Maecenas congue mollis varius. Vivamus nec eros id arcu interdum ornare.',2,'Demo feature list for the subpages','Demo feature list for the subpages','demo-feature-list-for-the-subpages',2,1,0,'2021-09-29 09:00:22','2021-09-29 09:00:22');
/*!40000 ALTER TABLE `fl_module_features_images_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_module_features_images_items`
--

DROP TABLE IF EXISTS `fl_module_features_images_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_module_features_images_items` (
  `info` text,
  `info_lv` text,
  `readmore_text` varchar(255) DEFAULT NULL,
  `readmore_text_lv` varchar(255) DEFAULT NULL,
  `readmore_link` varchar(255) DEFAULT NULL,
  `readmore_link_lv` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `name_lv` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `active_en` tinyint(1) DEFAULT NULL,
  `active_lv` tinyint(1) DEFAULT NULL,
  `isdefault` tinyint(4) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_module_features_images_items`
--

LOCK TABLES `fl_module_features_images_items` WRITE;
/*!40000 ALTER TABLE `fl_module_features_images_items` DISABLE KEYS */;
INSERT INTO `fl_module_features_images_items` VALUES ('Duis a velit ipsum. Maecenas congue mollis varius. Vivamus nec eros id arcu interdum ornare.','Duis a velit ipsum. Maecenas congue mollis varius. Vivamus nec eros id arcu interdum ornare.','Learn more','Uzzini vairāk','features','lv/funkcijas','4',1,1,'Let us handle your<br />\r\n<span class=\"color-main-text\">billing system</span>','Mēs palīdzēsim ar<br />\r\n<span class=\"color-main-text\">jūsu norēķiniem</span>','let-us-handle-your-billing-system',1,1,1,1,0,'2021-09-30 12:06:22','2021-09-29 09:25:48'),('Duis a velit ipsum. Maecenas congue mollis varius. Vivamus nec eros id arcu interdum ornare.','Duis a velit ipsum. Maecenas congue mollis varius. Vivamus nec eros id arcu interdum ornare.','Learn more','Uzzini vairāk','features','lv/funkcijas','4',1,6,'Time tracking you<br />\r\nwill <span class=\"color-main-text\">actually use</span>','Laika uzskaita, kas<br />\r\n<span class=\"color-main-text\">patīkami pārsteigs</span>','time-tracking-you-will-actually-use',2,1,1,1,0,'2021-09-30 12:06:27','2021-09-30 09:54:29'),('Duis a velit ipsum. Maecenas congue mollis varius. Vivamus nec eros id arcu interdum ornare.','Duis a velit ipsum. Maecenas congue mollis varius. Vivamus nec eros id arcu interdum ornare.','Learn more','Uzzini vairāk','features','lv/funkcijas','4',1,7,'Analyze and report<br />\r\nbeyond <span class=\"color-main-text\">your imagination</span>','Datu analīzes iespējas<br />\r\nārpus <span class=\"color-main-text\">realitātes robežām</span>','analyze-and-report-beyond-your-imagination',3,1,1,1,0,'2021-09-30 12:06:33','2021-09-30 09:56:14'),('Get your invoicing done in minutes, not hours. Create estimates and invoices on the road or at home. Quickly add details such as discounts, payment instructions, due dates, photos, signatures, shipping details and more. Turn estimates into invoices with one click.','Get your invoicing done in minutes, not hours. Create estimates and invoices on the road or at home. Quickly add details such as discounts, payment instructions, due dates, photos, signatures, shipping details and more. Turn estimates into invoices with one click.<br />\r\n&nbsp;',NULL,NULL,NULL,NULL,'5',2,8,'Create Estimates and<br />\r\nInvoices <span class=\"color-main-text\">Instantly</span>','Rēķini un pavadzīmes<br />\r\naugstākā <span class=\"color-main-text\">pilotāža</span>','create-estimates-and-invoices-instantly',1,1,1,1,0,'2021-09-30 10:01:36','2021-09-30 10:00:57'),('Our estimate and invoice templates are fully brandable to your business. We never add our own watermarks so your invoices always look professional and match your brand. Start with one of our clean, modern template designs, then simply add your logo.','Our estimate and invoice templates are fully brandable to your business. We never add our own watermarks so your invoices always look professional and match your brand. Start with one of our clean, modern template designs, then simply add your logo.',NULL,NULL,NULL,NULL,'6',2,9,'<span><span class=\"color-main-text\">Stay On Brand</span>,</span> Look<br />\r\nProfessional','Tavs <span class=\"color-main-text\">uzņēmuma stils</span><br />\r\nir visam pamatā','stay-on-brand-look-professional',2,1,1,1,0,'2021-09-30 10:02:58','2021-09-30 10:02:58');
/*!40000 ALTER TABLE `fl_module_features_images_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_module_features_items`
--

DROP TABLE IF EXISTS `fl_module_features_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_module_features_items` (
  `title` varchar(255) DEFAULT NULL,
  `title_lv` text,
  `info` varchar(255) DEFAULT NULL,
  `info_lv` text,
  `img` varchar(255) DEFAULT NULL,
  `features_images_id` int(11) DEFAULT NULL,
  `features_icons_id` int(11) DEFAULT NULL,
  `faq_id` int(11) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `facebook_title` varchar(255) DEFAULT NULL,
  `facebook_description` varchar(255) DEFAULT NULL,
  `facebook_image` int(11) DEFAULT NULL,
  `meta_title_lv` varchar(255) DEFAULT NULL,
  `meta_description_lv` varchar(255) DEFAULT NULL,
  `meta_keywords_lv` varchar(255) DEFAULT NULL,
  `facebook_title_lv` varchar(255) DEFAULT NULL,
  `facebook_description_lv` varchar(255) DEFAULT NULL,
  `facebook_image_lv` int(11) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `name_lv` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `alias_lv` varchar(255) DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `active_en` tinyint(1) DEFAULT NULL,
  `active_lv` tinyint(1) DEFAULT NULL,
  `isdefault` tinyint(4) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_module_features_items`
--

LOCK TABLES `fl_module_features_items` WRITE;
/*!40000 ALTER TABLE `fl_module_features_items` DISABLE KEYS */;
INSERT INTO `fl_module_features_items` VALUES ('Your clients will be amazed','Jūsu klienti būs patīkami pārsteigti','We are Fast Law. We are here to help you organize your law practice in one place from.\r\nHere are some pricing plan comperising.','We are Fast Law. We are here to help you organize your law practice in one place from.\r\nHere are some pricing plan comperising.','3',2,1,5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,'Clients','Klienti','clients','klienti',1,1,1,1,0,'2021-10-12 16:16:49','2021-09-29 06:49:20'),('Vivamus blandit finibus turpis','Vivamus blandit finibus turpis','Vivamus blandit finibus turpis, quis suscipit ex venenatis et. Nullam pellentesque\r\norci nec dui feugiat imperdiet. ','Vivamus blandit finibus turpis, quis suscipit ex venenatis et. Nullam pellentesque\r\norci nec dui feugiat imperdiet. ','3',2,1,5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,2,'Cases','Lietas','cases','lietas',2,1,1,1,0,'2021-10-11 08:28:54','2021-09-29 06:57:37'),('Sed viverra venenatis nisi quis egestas','Sed viverra venenatis nisi quis egestas','Sed viverra venenatis nisi quis egestas. Sed pellentesque lorem ut enim pellentesque finibus.\r\nSed rutrum libero et diam molestie, nec maximus sem gravida. ','Sed viverra venenatis nisi quis egestas. Sed pellentesque lorem ut enim pellentesque finibus.\r\nSed rutrum libero et diam molestie, nec maximus sem gravida. ','3',2,1,5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,3,'Time','Laiks','time','laiks',3,1,1,1,0,'2021-10-11 08:41:02','2021-09-29 06:58:19'),('Invoice your customers in seconds','Invoice your customers in seconds','We are Fast Law. We are here to help you organize your law practice in one place from.\r\nHere are some pricing plan comperising.','We are Fast Law. We are here to help you organize your law practice in one place from.\r\nHere are some pricing plan comperising.','3',2,1,5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,4,'Invoices','Rēķini','invoices','rekini',4,1,1,1,0,'2021-10-11 08:41:14','2021-09-29 06:59:11'),('Vestibulum turpis sem','Vestibulum turpis sem','Vestibulum turpis sem, consectetur at tincidunt eu, auctor nec lorem.\r\nIn ut urna in magna gravida vehicula.','Vestibulum turpis sem, consectetur at tincidunt eu, auctor nec lorem.\r\nIn ut urna in magna gravida vehicula.','3',2,1,5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,5,'Team','Komanda','team','komanda',5,1,1,1,0,'2021-10-11 08:41:27','2021-09-29 06:59:57'),('Proin venenatis ligula vitae orci','Proin venenatis ligula vitae orci','Morbi id dolor augue. Proin venenatis ligula vitae orci lacinia bibendum.\r\nSed blandit lorem ut tortor sagittis, vestibulum rutrum nibh dictum. ','Morbi id dolor augue. Proin venenatis ligula vitae orci lacinia bibendum.\r\nSed blandit lorem ut tortor sagittis, vestibulum rutrum nibh dictum. ','3',2,1,5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,6,'Analysis','Statistika','analysis','statistika',6,1,1,1,0,'2021-10-11 08:41:41','2021-09-29 07:01:00'),('Cras hendrerit, ligula porta','Cras hendrerit, ligula porta','Cras hendrerit, ligula porta condimentum fermentum, massa est bibendum arcu, a\r\nplacerat nisl mi vitae metus. Pellentesque rutrum posuere varius.','Cras hendrerit, ligula porta condimentum fermentum, massa est bibendum arcu, a\r\nplacerat nisl mi vitae metus. Pellentesque rutrum posuere varius.','3',2,1,5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,7,'Tasks','Darbi','tasks','darbi',7,1,1,1,0,'2021-10-11 08:41:54','2021-09-29 07:01:38'),('Mauris cursus, nibh non varius suscipit','Mauris cursus, nibh non varius suscipit','Mauris cursus, nibh non varius suscipit, dui ante faucibus orci, in auctor massa magna\r\nscelerisque elit. Nullam tellus nisi.','Mauris cursus, nibh non varius suscipit, dui ante faucibus orci, in auctor massa magna\r\nscelerisque elit. Nullam tellus nisi.','3',2,1,5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,8,'Contacts','Kontakti','contacts','kontakti',8,1,1,1,0,'2021-10-11 08:42:06','2021-09-29 07:02:22'),('Etiam non sem feugiat, fermentum sem eget','Etiam non sem feugiat, fermentum sem eget','Etiam non sem feugiat, fermentum sem eget, ultricies enim. Sed rutrum libero et diam\r\nmolestie, nec maximus sem gravida.','Etiam non sem feugiat, fermentum sem eget, ultricies enim. Sed rutrum libero et diam\r\nmolestie, nec maximus sem gravida.','3',2,1,5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,9,'Administration','Administrācija','administration','administracija',9,1,1,1,0,'2021-10-11 08:42:20','2021-09-29 07:03:07');
/*!40000 ALTER TABLE `fl_module_features_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_module_pricing_category`
--

DROP TABLE IF EXISTS `fl_module_pricing_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_module_pricing_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `isdefault` tinyint(4) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_module_pricing_category`
--

LOCK TABLES `fl_module_pricing_category` WRITE;
/*!40000 ALTER TABLE `fl_module_pricing_category` DISABLE KEYS */;
INSERT INTO `fl_module_pricing_category` VALUES (1,'Pricing','pricing',1,1,0,'2021-10-04 08:07:55','2021-10-04 08:07:55');
/*!40000 ALTER TABLE `fl_module_pricing_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_module_pricing_features_category`
--

DROP TABLE IF EXISTS `fl_module_pricing_features_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_module_pricing_features_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `isdefault` tinyint(4) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_module_pricing_features_category`
--

LOCK TABLES `fl_module_pricing_features_category` WRITE;
/*!40000 ALTER TABLE `fl_module_pricing_features_category` DISABLE KEYS */;
INSERT INTO `fl_module_pricing_features_category` VALUES (1,'Main Features','main-features',1,1,0,'2021-10-04 09:12:36','2021-10-04 08:21:25'),(2,'Extra Featuers','extra-featuers',2,1,0,'2021-10-04 09:12:42','2021-10-04 09:12:42');
/*!40000 ALTER TABLE `fl_module_pricing_features_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_module_pricing_features_items`
--

DROP TABLE IF EXISTS `fl_module_pricing_features_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_module_pricing_features_items` (
  `name_lv` varchar(255) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `isdefault` tinyint(4) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_module_pricing_features_items`
--

LOCK TABLES `fl_module_pricing_features_items` WRITE;
/*!40000 ALTER TABLE `fl_module_pricing_features_items` DISABLE KEYS */;
INSERT INTO `fl_module_pricing_features_items` VALUES ('Viegla laika uzskaite',1,1,'Easy time tracking','easy-time-tracking',1,1,0,'2021-10-04 09:15:56','2021-10-04 08:22:48'),('Aplikācijas priekš visām ierīcēm',1,2,'Apps for all your devices','apps-for-all-your-devices',2,1,0,'2021-10-04 09:16:09','2021-10-04 08:22:55'),('Integrācija ar darba plūsmu',1,4,'Integrations that fit your workflow','integrations-that-fit-your-workflow',3,1,0,'2021-10-04 09:16:28','2021-10-04 08:23:16'),('Dziļāks ieskats projektos un komandā',1,5,'Insight into your projects and team','insight-into-your-projects-and-team',4,1,0,'2021-10-04 09:16:43','2021-10-04 08:23:27'),('Viegli maksājumi un rēķini',1,6,'Seamless invoicing and payments','seamless-invoicing-and-payments',5,1,0,'2021-10-04 09:17:21','2021-10-04 08:24:19'),('Īstu cilvēku atbalsts',1,9,'Support from real humans','support-from-real-humans',6,1,0,'2021-10-04 09:18:05','2021-10-04 08:24:46'),('Kaut kāda cita fīča kas nav pie visiem',2,10,'Some extra feature not in main','some-extra-feature',1,1,0,'2021-10-04 09:18:19','2021-10-04 08:25:43');
/*!40000 ALTER TABLE `fl_module_pricing_features_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_module_pricing_items`
--

DROP TABLE IF EXISTS `fl_module_pricing_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_module_pricing_items` (
  `recomended` tinyint(1) DEFAULT NULL,
  `recomended_lv` tinyint(1) DEFAULT NULL,
  `recomended_text` varchar(255) DEFAULT NULL,
  `recomended_text_lv` varchar(255) DEFAULT NULL,
  `price_month` varchar(255) DEFAULT NULL,
  `price_month_lv` varchar(255) DEFAULT NULL,
  `price_year` varchar(255) DEFAULT NULL,
  `price_year_lv` varchar(255) DEFAULT NULL,
  `button_text` varchar(255) DEFAULT NULL,
  `button_text_lv` varchar(255) DEFAULT NULL,
  `button_link` varchar(255) DEFAULT NULL,
  `button_link_lv` varchar(255) DEFAULT NULL,
  `features` text,
  `parent` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `name_lv` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `active_en` tinyint(1) DEFAULT NULL,
  `active_lv` tinyint(1) DEFAULT NULL,
  `isdefault` tinyint(4) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_module_pricing_items`
--

LOCK TABLES `fl_module_pricing_items` WRITE;
/*!40000 ALTER TABLE `fl_module_pricing_items` DISABLE KEYS */;
INSERT INTO `fl_module_pricing_items` VALUES (NULL,NULL,'Recommended for you','Iesakam šo plānu jums','9.00','9.00','7.00','7.00','Try it for free','Izmēģini bez maksas','features','lv/kontakti','{\"1\":\"1\",\"2\":\"1\",\"4\":\"1\",\"5\":\"1\"}',1,1,'Basic','Pamats','basic',1,1,1,1,0,'2021-10-04 20:07:49','2021-10-04 08:12:44'),(1,1,'Recommended for you','Iesakam šo plānu jums','25.00','25.00','19.00','19.00','Try it for free','Izmēģini bez maksas','features','lv/kontakti','{\"1\":\"1\",\"2\":\"1\",\"4\":\"1\",\"5\":\"1\",\"6\":\"1\",\"10\":\"1\"}',1,2,'Basic + Analytics','Pamats + Analītika','basic-analytics',2,1,1,1,0,'2021-10-04 12:52:40','2021-10-04 08:15:06'),(NULL,NULL,'Recommended for you','Iesakam šo plānu jums','42.00','42.00','32.00','32.00','Try it for free','Izmēģini bez maksas','contacts','lv/kontakti','{\"1\":\"1\",\"2\":\"1\",\"4\":\"1\",\"5\":\"1\",\"8\":\"1\",\"6\":\"1\",\"9\":\"1\",\"10\":\"1\"}',1,3,'Basic + Analytics + AML','Pamats + Analītika + AML','basic-analytics-aml',3,1,1,1,0,'2021-10-04 12:50:58','2021-10-04 08:16:20');
/*!40000 ALTER TABLE `fl_module_pricing_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_module_socialnetworks_category`
--

DROP TABLE IF EXISTS `fl_module_socialnetworks_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_module_socialnetworks_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `isdefault` tinyint(4) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_module_socialnetworks_category`
--

LOCK TABLES `fl_module_socialnetworks_category` WRITE;
/*!40000 ALTER TABLE `fl_module_socialnetworks_category` DISABLE KEYS */;
INSERT INTO `fl_module_socialnetworks_category` VALUES (1,'FastLaw Social','fastlaw-social',1,1,0,'2021-10-04 07:07:20','2021-10-04 07:07:20');
/*!40000 ALTER TABLE `fl_module_socialnetworks_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_module_socialnetworks_items`
--

DROP TABLE IF EXISTS `fl_module_socialnetworks_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_module_socialnetworks_items` (
  `link` varchar(255) DEFAULT NULL,
  `network` varchar(255) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `isdefault` tinyint(4) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_module_socialnetworks_items`
--

LOCK TABLES `fl_module_socialnetworks_items` WRITE;
/*!40000 ALTER TABLE `fl_module_socialnetworks_items` DISABLE KEYS */;
INSERT INTO `fl_module_socialnetworks_items` VALUES ('https://www.facebook.com/fastlaw','facebook',1,1,'Facebook','facebook',1,1,0,'2021-10-04 07:08:10','2021-10-04 07:08:10'),('https://www.twitter.com/fastlaw','twitter',1,2,'Twitter','twitter',2,1,0,'2021-10-04 07:08:34','2021-10-04 07:08:25'),('https://www.linkedin.com/fastlaw','linkedin',1,3,'LinkedIn','linkedin',3,1,0,'2021-10-04 07:08:53','2021-10-04 07:08:53');
/*!40000 ALTER TABLE `fl_module_socialnetworks_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_module_team_category`
--

DROP TABLE IF EXISTS `fl_module_team_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_module_team_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `isdefault` tinyint(4) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_module_team_category`
--

LOCK TABLES `fl_module_team_category` WRITE;
/*!40000 ALTER TABLE `fl_module_team_category` DISABLE KEYS */;
INSERT INTO `fl_module_team_category` VALUES (1,'Homepage','homepage',1,1,0,'2021-10-02 07:56:45','2021-10-02 07:56:45');
/*!40000 ALTER TABLE `fl_module_team_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_module_team_items`
--

DROP TABLE IF EXISTS `fl_module_team_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_module_team_items` (
  `position` varchar(255) DEFAULT NULL,
  `position_lv` text,
  `img` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `isdefault` tinyint(4) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_module_team_items`
--

LOCK TABLES `fl_module_team_items` WRITE;
/*!40000 ALTER TABLE `fl_module_team_items` DISABLE KEYS */;
INSERT INTO `fl_module_team_items` VALUES ('Senior Partner at “Brownwinlaw”','“Brownwinlaw” vecākais partneris','15','https://vimeo.com/437077704',1,1,'Christopher Brown','christopher-brown',1,1,0,'2021-10-17 09:47:51','2021-10-02 08:14:35'),('Senior Partner at “VILGERTS”','“VILGERTS” vecākais partneris','14','https://vimeo.com/437077704',1,2,'Gints Vilgerts','gints-vilgerts',2,1,0,'2021-10-17 09:47:54','2021-10-02 08:15:33'),('Senior Partner at “LawFirm”','“LawFirm” vecākais partneris','16','https://vimeo.com/437077704',1,3,'Alice Smith','alice-smith',3,1,0,'2021-10-17 09:47:57','2021-10-02 08:16:08');
/*!40000 ALTER TABLE `fl_module_team_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_module_templates`
--

DROP TABLE IF EXISTS `fl_module_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_module_templates` (
  `module_name` varchar(160) DEFAULT NULL,
  `template_name` varchar(160) DEFAULT NULL,
  `content` text,
  `create_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  KEY `kab_index_module_templates_by_module_name_template_name` (`module_name`,`template_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_module_templates`
--

LOCK TABLES `fl_module_templates` WRITE;
/*!40000 ALTER TABLE `fl_module_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `fl_module_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_module_whyus_category`
--

DROP TABLE IF EXISTS `fl_module_whyus_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_module_whyus_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `isdefault` tinyint(4) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_module_whyus_category`
--

LOCK TABLES `fl_module_whyus_category` WRITE;
/*!40000 ALTER TABLE `fl_module_whyus_category` DISABLE KEYS */;
INSERT INTO `fl_module_whyus_category` VALUES (1,'Why Us','why-us',1,1,0,'2021-10-02 06:36:35','2021-10-02 06:36:35');
/*!40000 ALTER TABLE `fl_module_whyus_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_module_whyus_items`
--

DROP TABLE IF EXISTS `fl_module_whyus_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_module_whyus_items` (
  `info` varchar(255) DEFAULT NULL,
  `info_lv` text,
  `img` varchar(255) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `name_lv` text,
  `alias` varchar(255) DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `active_en` tinyint(1) DEFAULT NULL,
  `active_lv` tinyint(1) DEFAULT NULL,
  `isdefault` tinyint(4) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_module_whyus_items`
--

LOCK TABLES `fl_module_whyus_items` WRITE;
/*!40000 ALTER TABLE `fl_module_whyus_items` DISABLE KEYS */;
INSERT INTO `fl_module_whyus_items` VALUES ('Sit amet lorem ipsum mollis varius. Vivamus nec ornare eros id arcu interdum.','Sit amet lorem ipsum mollis varius. Vivamus nec ornare eros id arcu interdum.','8',1,1,'Industry experience','Pieredze nozarē','industry-experience',1,1,1,1,0,'2021-10-02 07:23:58','2021-10-02 06:51:33'),('Duis a velit ipsum. Maecenas congue mollis varius. Vivamus nec eros id arcu interdum ornare.','Duis a velit ipsum. Maecenas congue mollis varius. Vivamus nec eros id arcu interdum ornare.','9',1,2,'Security','Drošība','security',2,1,1,1,0,'2021-10-02 06:51:58','2021-10-02 06:51:58'),('Lorem ipsum maecenas congue mollis varius. Dolor sit amet vivamus nec eros id arcu interdum vivamus.','Lorem ipsum maecenas congue mollis varius. Dolor sit amet vivamus nec eros id arcu interdum vivamus.','10',1,3,'User centred','Lietotāji pirmie','user-centred',3,1,1,1,0,'2021-10-02 06:53:06','2021-10-02 06:53:06'),('Sed lacinia tortor id euismod aliquam. Cras vitae ligula nisl. Vestibulum vulputate ac sapien et pulvinar.','Sed lacinia tortor id euismod aliquam. Cras vitae ligula nisl. Vestibulum vulputate ac sapien et pulvinar.','11',1,4,'Adaptable for all','Viegli pielāgot','adaptable-for-all',4,1,1,1,0,'2021-10-02 06:53:52','2021-10-02 06:53:52'),('Sit amet lorem ipsum mollis varius. Vivamus nec ornare eros id arcu interdum.','Sit amet lorem ipsum mollis varius. Vivamus nec ornare eros id arcu interdum.','12',1,5,'Build by law practitioners','Izveidojuši juristi','build-by-law-practitioners',5,1,1,1,0,'2021-10-02 07:00:37','2021-10-02 07:00:37'),('Lorem ipsum maecenas congue mollis varius. Dolor sit amet vivamus nec.','Lorem ipsum maecenas congue mollis varius. Dolor sit amet vivamus nec.','13',1,6,'User centred and more','Lietotāju paradīze','user-centred-and-more',6,1,1,1,0,'2021-10-02 07:02:36','2021-10-02 07:02:36');
/*!40000 ALTER TABLE `fl_module_whyus_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_modules`
--

DROP TABLE IF EXISTS `fl_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_modules` (
  `module_name` varchar(160) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  `admin_only` tinyint(4) DEFAULT '0',
  `active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`module_name`),
  KEY `kab_index_modules_by_module_name` (`module_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_modules`
--

LOCK TABLES `fl_modules` WRITE;
/*!40000 ALTER TABLE `fl_modules` DISABLE KEYS */;
INSERT INTO `fl_modules` VALUES ('CTLModuleMaker','installed','1.8.9.3',0,1),('faq','installed','1.0',0,1),('features','installed','1.0',0,1),('features_icons','installed','1.0',0,1),('features_images','installed','1.0',0,1),('pricing','installed','1.0',0,1),('pricing_features','installed','1.0',0,1),('socialnetworks','installed','1.0',0,1),('team','installed','1.0',0,1),('whyus','installed','1.0',0,1);
/*!40000 ALTER TABLE `fl_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_permissions`
--

DROP TABLE IF EXISTS `fl_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_permissions` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_name` varchar(255) DEFAULT NULL,
  `permission_text` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_permissions`
--

LOCK TABLES `fl_permissions` WRITE;
/*!40000 ALTER TABLE `fl_permissions` DISABLE KEYS */;
INSERT INTO `fl_permissions` VALUES (1,'admin_clear_admin_log','Admin - Clear Admin Log','2006-07-25 21:22:33','2006-07-25 21:22:33'),(2,'admin_modify_modules','Admin - Modify Modules','2006-07-25 21:22:33','2006-07-25 21:22:33'),(3,'admin_modify_site_preferences','Admin - Modify Site Preferences','2006-07-25 21:22:33','2006-07-25 21:22:33'),(4,'admin_use_ctl_module_maker','Admin - Use CTL Module Maker','2018-08-27 18:54:08','2018-08-27 18:54:08'),(5,'admin_pages_view','Admin - Pages - View','2020-04-25 00:00:00','2020-04-25 00:00:00'),(6,'admin_pages_copy','Pages - Copy','2020-04-25 00:00:00','2020-04-25 00:00:00'),(7,'pages_add','Pages - Add','2020-04-25 00:00:00','2020-04-25 00:00:00'),(8,'pages_edit','Pages - Edit','2020-04-25 00:00:00','2020-04-25 00:00:00'),(9,'pages_delete','Pages - Delete','2020-04-25 00:00:00','2020-04-25 00:00:00'),(10,'pages_move','Pages - Move','2020-04-25 00:00:00','2020-04-25 00:00:00'),(11,'pages_set_active','Pages - Set Active','2020-04-25 00:00:00','2020-04-25 00:00:00'),(12,'pages_set_default','Pages - Set Default','2020-04-25 00:00:00','2020-04-25 00:00:00'),(13,'templates_add','Templates - Add','2006-07-25 21:22:33','2006-07-25 21:22:33'),(14,'templates_modify','Templates - Modify','2006-07-25 21:22:33','2006-07-25 21:22:33'),(15,'templates_remove','Templates - Remove','2006-07-25 21:22:33','2006-07-25 21:22:33'),(16,'users_add_remove','Users - Add, Remove and Modify Users','2006-07-25 21:22:33','2006-07-25 21:22:33'),(17,'users_edit_prefs','Users - Edit User Preferences','2020-08-05 12:49:22','2020-08-05 12:49:22'),(18,'users_groups_add_remove','Users - Groups - Add, Remove and Modify User Groups','2006-07-25 21:22:33','2006-07-25 21:22:33'),(19,'users_groups_change_default','Users - Groups - Change the Default User Group','2020-07-15 09:05:52','2020-07-15 09:05:52'),(20,'admin_modify_events','Admin - Modify Events','2006-01-27 20:06:58','2006-01-27 20:06:58'),(21,'module_socialnetworks_advanced','Module - Social Networks (Advanced)','2021-02-04 13:55:59','2021-02-04 13:55:59'),(22,'module_socialnetworks_manage_category','Module - Social Networks (Category)','2021-02-04 13:55:59','2021-02-04 13:55:59'),(23,'module_socialnetworks_manage_items','Module - Social Networks (Items)','2021-02-04 13:55:59','2021-02-04 13:55:59'),(27,'module_features_images_advanced','Module - Features: List with images (Advanced)','2021-09-28 08:08:39','2021-09-28 08:08:39'),(28,'module_features_images_manage_category','Module - Features: List with images (Category)','2021-09-28 08:08:39','2021-09-28 08:08:39'),(29,'module_features_images_manage_items','Module - Features: List with images (Items)','2021-09-28 08:08:39','2021-09-28 08:08:39'),(30,'module_features_icons_advanced','Module - Features: List with icons (Advanced)','2021-09-28 08:10:35','2021-09-28 08:10:35'),(31,'module_features_icons_manage_category','Module - Features: List with icons (Category)','2021-09-28 08:10:35','2021-09-28 08:10:35'),(32,'module_features_icons_manage_items','Module - Features: List with icons (Items)','2021-09-28 08:10:35','2021-09-28 08:10:35'),(36,'module_features_advanced','Module - Features: Main (Advanced)','2021-09-28 13:21:26','2021-09-28 13:21:26'),(37,'module_features_manage_category','Module - Features: Main (Category)','2021-09-28 13:21:26','2021-09-28 13:21:26'),(38,'module_features_manage_items','Module - Features: Main (Items)','2021-09-28 13:21:26','2021-09-28 13:21:26'),(39,'module_faq_advanced','Module - Frequently Asked Questions (Advanced)','2021-10-01 09:35:17','2021-10-01 09:35:17'),(40,'module_faq_manage_category','Module - Frequently Asked Questions (Category)','2021-10-01 09:35:17','2021-10-01 09:35:17'),(41,'module_faq_manage_items','Module - Frequently Asked Questions (Items)','2021-10-01 09:35:17','2021-10-01 09:35:17'),(42,'module_whyus_advanced','Module - Why choose Fast Law (Advanced)','2021-10-02 06:27:46','2021-10-02 06:27:46'),(43,'module_whyus_manage_category','Module - Why choose Fast Law (Category)','2021-10-02 06:27:46','2021-10-02 06:27:46'),(44,'module_whyus_manage_items','Module - Why choose Fast Law (Items)','2021-10-02 06:27:46','2021-10-02 06:27:46'),(45,'module_team_advanced','Module - Home: Professionals (Advanced)','2021-10-02 06:30:51','2021-10-02 06:30:51'),(46,'module_team_manage_category','Module - Home: Professionals (Category)','2021-10-02 06:30:51','2021-10-02 06:30:51'),(47,'module_team_manage_items','Module - Home: Professionals (Items)','2021-10-02 06:30:51','2021-10-02 06:30:51'),(48,'module_pricing_advanced','Module - Pricing: Main (Advanced)','2021-10-04 08:07:27','2021-10-04 08:07:27'),(49,'module_pricing_manage_category','Module - Pricing: Main (Category)','2021-10-04 08:07:27','2021-10-04 08:07:27'),(50,'module_pricing_manage_items','Module - Pricing: Main (Items)','2021-10-04 08:07:27','2021-10-04 08:07:27'),(51,'module_pricing_features_advanced','Module - Pricing: Features (Advanced)','2021-10-04 08:21:05','2021-10-04 08:21:05'),(52,'module_pricing_features_manage_category','Module - Pricing: Features (Category)','2021-10-04 08:21:05','2021-10-04 08:21:05'),(53,'module_pricing_features_manage_items','Module - Pricing: Features (Items)','2021-10-04 08:21:05','2021-10-04 08:21:05');
/*!40000 ALTER TABLE `fl_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_siteprefs`
--

DROP TABLE IF EXISTS `fl_siteprefs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_siteprefs` (
  `sitepref_name` varchar(255) NOT NULL,
  `sitepref_value` text,
  `create_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`sitepref_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_siteprefs`
--

LOCK TABLES `fl_siteprefs` WRITE;
/*!40000 ALTER TABLE `fl_siteprefs` DISABLE KEYS */;
INSERT INTO `fl_siteprefs` VALUES ('additional_editors','',NULL,NULL),('allowparamcheckwarnings','0',NULL,NULL),('clear_vc_cache','0',NULL,NULL),('cms_is_uptodate','1',NULL,NULL),('css_max_age','0',NULL,NULL),('CTLModuleMaker_mapi_pref_checkversion','1',NULL,NULL),('CTLModuleMaker_mapi_pref_usesessions','1',NULL,NULL),('custom404','<p>Page could not be found.</p>','2006-07-25 21:22:33','2006-07-25 21:22:33'),('custom404template','-1','2006-07-25 21:22:33','2006-07-25 21:22:33'),('defaultdateformat','',NULL,NULL),('defaultpagecontent','<!-- Add code here that should appear in the content block of all new pages -->',NULL,NULL),('default_parent_page','-1',NULL,NULL),('disablesafemodewarning','0',NULL,NULL),('enablecustom404','0','2006-07-25 21:22:33','2006-07-25 21:22:33'),('enablenotifications','1',NULL,NULL),('enablesitedownmessage','0','2006-07-25 21:22:33','2006-07-25 21:22:33'),('faq_mapi_pref_makerversion','1.8.9.3',NULL,NULL),('features_icons_mapi_pref_makerversion','1.8.9.3',NULL,NULL),('features_images_mapi_pref_makerversion','1.8.9.3',NULL,NULL),('features_mapi_pref_makerversion','1.8.9.3',NULL,NULL),('frontendlang','lv_LV',NULL,'2021-08-24 06:58:52'),('frontendwysiwyg','',NULL,NULL),('global_umask','022',NULL,NULL),('lastcmsversioncheck','1596747595',NULL,NULL),('logintheme','NCleanGrey','2006-07-25 21:22:33','2021-08-24 06:58:52'),('metadata','<meta name=\"Generator\" content=\"CMS Made Simple - Copyright (C) 2004-10 Ted Kulp. All rights reserved.\" />\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n ','2006-07-25 21:22:33','2006-07-25 21:22:33'),('nogcbwysiwyg','0',NULL,NULL),('page_active','1',NULL,NULL),('page_cachable','1',NULL,NULL),('page_extra1','',NULL,NULL),('page_extra2','',NULL,NULL),('page_extra3','',NULL,NULL),('page_metadata','{* Add code here that should appear in the metadata section of all new pages *}',NULL,NULL),('page_searchable','1',NULL,NULL),('page_showinmenu','1',NULL,NULL),('prices_app_mapi_pref_display_filter','1',NULL,NULL),('prices_app_mapi_pref_display_instantsearch','1',NULL,NULL),('prices_app_mapi_pref_display_instantsort','1',NULL,NULL),('prices_app_mapi_pref_emptytemplate','**',NULL,NULL),('prices_app_mapi_pref_listtemplate_category','list_default',NULL,NULL),('prices_app_mapi_pref_listtemplate_items','list_default',NULL,NULL),('prices_app_mapi_pref_load_nbchildren','1',NULL,NULL),('prices_app_mapi_pref_makerversion','1.8.9.3',NULL,NULL),('prices_app_mapi_pref_orderbyname','1',NULL,NULL),('prices_app_mapi_pref_restrict_permissions','1',NULL,NULL),('prices_app_mapi_pref_searchmodule_index_category','1',NULL,NULL),('prices_app_mapi_pref_searchmodule_index_items','1',NULL,NULL),('prices_app_mapi_pref_showthumbnails','1',NULL,NULL),('prices_app_mapi_pref_tabdisplay_category','1',NULL,NULL),('prices_app_mapi_pref_tabdisplay_items','1',NULL,NULL),('prices_app_mapi_pref_use_session','1',NULL,NULL),('pricing_features_mapi_pref_makerversion','1.8.9.3',NULL,NULL),('pricing_mapi_pref_makerversion','1.8.9.3',NULL,NULL),('pseudocron_lastrun','1613375368',NULL,NULL),('sitedownexcludes','',NULL,NULL),('sitedownmessage','<p>Site is currently down for maintenance.</p>','2006-07-25 21:22:33','2006-07-25 21:22:33'),('sitedownmessagetemplate','-1','2006-07-25 21:22:33','2006-07-25 21:22:33'),('sitename','Fast Law',NULL,'2021-08-24 06:58:52'),('socialnetworks_mapi_pref_display_filter','1',NULL,NULL),('socialnetworks_mapi_pref_display_instantsearch','1',NULL,NULL),('socialnetworks_mapi_pref_display_instantsort','1',NULL,NULL),('socialnetworks_mapi_pref_emptytemplate','**',NULL,NULL),('socialnetworks_mapi_pref_listtemplate_category','list_default',NULL,NULL),('socialnetworks_mapi_pref_listtemplate_items','list_default',NULL,NULL),('socialnetworks_mapi_pref_load_nbchildren','1',NULL,NULL),('socialnetworks_mapi_pref_makerversion','1.8.9.3',NULL,NULL),('socialnetworks_mapi_pref_orderbyname','1',NULL,NULL),('socialnetworks_mapi_pref_restrict_permissions','1',NULL,NULL),('socialnetworks_mapi_pref_searchmodule_index_category','1',NULL,NULL),('socialnetworks_mapi_pref_searchmodule_index_items','1',NULL,NULL),('socialnetworks_mapi_pref_showthumbnails','1',NULL,NULL),('socialnetworks_mapi_pref_tabdisplay_category','1',NULL,NULL),('socialnetworks_mapi_pref_tabdisplay_items','1',NULL,NULL),('socialnetworks_mapi_pref_use_session','1',NULL,NULL),('team_mapi_pref_makerversion','1.8.9.3',NULL,NULL),('thumbnail_height','96',NULL,NULL),('thumbnail_width','96',NULL,NULL),('urlcheckversion','',NULL,NULL),('useadvancedcss','1','2006-07-25 21:22:33','2006-07-25 21:22:33'),('whyus_mapi_pref_makerversion','1.8.9.3',NULL,NULL),('woodpool_users_mapi_pref_display_filter','1',NULL,NULL),('woodpool_users_mapi_pref_display_instantsearch','1',NULL,NULL),('woodpool_users_mapi_pref_display_instantsort','1',NULL,NULL),('woodpool_users_mapi_pref_emptytemplate','**',NULL,NULL),('woodpool_users_mapi_pref_listtemplate_category','list_default',NULL,NULL),('woodpool_users_mapi_pref_listtemplate_items','list_default',NULL,NULL),('woodpool_users_mapi_pref_load_nbchildren','1',NULL,NULL),('woodpool_users_mapi_pref_makerversion','1.8.9.3',NULL,NULL),('woodpool_users_mapi_pref_orderbyname','1',NULL,NULL),('woodpool_users_mapi_pref_restrict_permissions','1',NULL,NULL),('woodpool_users_mapi_pref_searchmodule_index_category','1',NULL,NULL),('woodpool_users_mapi_pref_searchmodule_index_items','1',NULL,NULL),('woodpool_users_mapi_pref_showthumbnails','1',NULL,NULL),('woodpool_users_mapi_pref_tabdisplay_category','1',NULL,NULL),('woodpool_users_mapi_pref_tabdisplay_items','1',NULL,NULL),('woodpool_users_mapi_pref_use_session','1',NULL,NULL),('xmlmodulerepository','','2006-07-25 21:22:33','2006-07-25 21:22:33'),('__listcontent_timelock__','1585735232',NULL,NULL);
/*!40000 ALTER TABLE `fl_siteprefs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_templates`
--

DROP TABLE IF EXISTS `fl_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_templates` (
  `template_id` int(11) NOT NULL AUTO_INCREMENT,
  `template_name` varchar(160) DEFAULT NULL,
  `template_content` text,
  `template_users` text,
  `stylesheet` text,
  `encoding` varchar(25) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `default_template` tinyint(4) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_templates`
--

LOCK TABLES `fl_templates` WRITE;
/*!40000 ALTER TABLE `fl_templates` DISABLE KEYS */;
INSERT INTO `fl_templates` VALUES (1,'lang','{***********************\r\n    DEFAULT\r\n***********************}\r\n\r\n{* setup fields *}\r\n{setup_content\r\nhide=\"contenttype//parent//showinmenu//menutext//title\"\r\nimages=\"\"\r\nfiles=\"\"\r\nright_col=\"\"}\r\n\r\n{content class=\"\" size=\"\" oneline=\"true\"}\r\n{content block=\"site_name\" label=\"Site Name\" oneline=\"true\"}\r\n{content block=\"author\" label=\"Autohor\" oneline=\"true\"}','{}','','',1,0,'2009-05-06 14:20:10','2021-10-12 15:16:46'),(2,'home','{***********************\r\n    HOME TEMPLATE\r\n***********************}\r\n\r\n{* setup fields *}\r\n{setup_content\r\nhide=\"contenttype//parent//content_en\"\r\nimages=\"\"\r\nfiles=\"\"\r\nright_col=\"\"}\r\n\r\n{content size=\"80\" oneline=\"true\"}\r\n\r\n{content block=\"meta_title\" label=\"Meta Title\" oneline=\"true\"}\r\n{content block=\"meta_description\" label=\"Meta Description\" wysiwyg=\"false\" size=\"100\"}\r\n{content block=\"meta_keywords\" label=\"Meta Keywords\" oneline=\"true\"}\r\n{content block=\"facebook_title\" label=\"Facebook Title\" oneline=\"true\"}\r\n{content block=\"facebook_description\" label=\"Facebook Description\" wysiwyg=\"false\" size=\"100\"}\r\n{content block=\"facebook_image\" label=\"Facebook Image\" oneline=\"true\"}','{}','','',1,0,'2020-08-11 13:19:36','2021-10-04 14:01:09'),(3,'home_intro','{***********************\r\n    INTRO\r\n***********************}\r\n\r\n{* setup fields *}\r\n{setup_content\r\nhide=\"contenttype//parent//showinmenu//menutext//alias\"\r\nimages=\"img//bg_img\"\r\nfiles=\"\"\r\nright_col=\"img//bg_img\"}\r\n\r\n{content size=\"93\" wysiwyg=\"false\"}\r\n{content block=\"cta_text\" label=\"CTA Text\" oneline=\"true\"}\r\n{content block=\"cta_link\" label=\"CTA Link\" oneline=\"true\"}\r\n\r\n{content block=\"img\" label=\"Image\" oneline=\"true\"}\r\n{content block=\"bg_img\" label=\"Background Image\" oneline=\"true\"}','{}','','',1,0,'2021-08-24 06:51:16','2021-10-19 07:33:43'),(4,'home_why','{***********************\r\n    WHY\r\n***********************}\r\n\r\n{* setup fields *}\r\n{setup_content\r\nhide=\"contenttype//parent//showinmenu//menutext//alias\"\r\nimages=\"\"\r\nfiles=\"\"\r\nright_col=\"\"}\r\n\r\n{content size=\"93\" wysiwyg=\"false\"}','{}','','',1,0,'2021-08-24 06:52:12','2021-10-04 14:08:23'),(5,'home_video','{***********************\r\n    VIDEO\r\n***********************}\r\n\r\n{* setup fields *}\r\n{setup_content\r\nhide=\"contenttype//parent//showinmenu//menutext//alias\"\r\nimages=\"img_video\"\r\nfiles=\"\"\r\nright_col=\"video//img_video\"}\r\n\r\n{content size=\"93\" wysiwyg=\"false\"}\r\n{content block=\"cta_text\" label=\"CTA Text\" oneline=\"true\"}\r\n\r\n{content block=\"video\" label=\"Video Link\" oneline=\"true\"}\r\n{content block=\"img_video\" label=\"Video Preview\" oneline=\"true\"}','{}','','',1,0,'2021-08-24 06:54:07','2021-10-19 07:56:47'),(6,'home_features','{***********************\r\n    Home: Features\r\n***********************}\r\n\r\n{* setup fields *}\r\n{setup_content\r\nhide=\"contenttype//parent//showinmenu//menutext//content_en\"\r\nimages=\"\"\r\nfiles=\"\"\r\nright_col=\"\"}\r\n\r\n{content size=\"80\" oneline=\"true\"}','{\"group\":{\"edit\":[1],\"active\":[1,2],\"delete\":[1]}}','','',1,0,'2021-08-24 06:55:44','2021-10-04 14:05:33'),(7,'home_pricing','{***********************\r\n    Home: Pricing\r\n***********************}\r\n\r\n{* setup fields *}\r\n{setup_content\r\nhide=\"contenttype//parent//showinmenu//alias//menutext\"\r\nimages=\"\"\r\nfiles=\"\"\r\nright_col=\"\"}\r\n\r\n{content size=\"100\" class=\"text-editor-basic\"}\r\n{content block=\"footer\" label=\"Pricing Plan Footer\" size=\"100\" class=\"text-editor-basic\"}','{}','','',1,0,'2021-08-24 06:56:09','2021-10-04 14:07:12'),(8,'home_people','{***********************\r\n    Home: People\r\n***********************}\r\n\r\n{* setup fields *}\r\n{setup_content\r\nhide=\"contenttype//parent//showinmenu//menutext//alias\"\r\nimages=\"\"\r\nfiles=\"\"\r\nright_col=\"\"}\r\n\r\n{content size=\"93\" wysiwyg=\"false\"}','{}','','',1,0,'2021-08-24 06:56:28','2021-10-04 14:06:00'),(10,'default','{***********************\r\n    DEFAULT TEMPLATE\r\n***********************}\r\n\r\n{* setup fields *}\r\n{setup_content\r\nhide=\"contenttype//parent//showinmenu//menutext//content_en\"\r\nimages=\"\"\r\nfiles=\"\"\r\nright_col=\"\"}\r\n\r\n{content oneline=\"true\"}','{}','','',1,1,'2021-08-24 07:08:50','2021-10-01 15:31:24'),(11,'contacts','{***********************\r\n    CONTACTS\r\n***********************}\r\n\r\n{* setup fields *}\r\n{setup_content\r\nhide=\"contenttype//parent//showinmenu\"\r\nimages=\"\"\r\nfiles=\"\"\r\nright_col=\"\"}\r\n\r\n{content size=\"93\" wysiwyg=\"false\"}\r\n\r\n{content block=\"address\" label=\"Address\" oneline=\"true\"}\r\n{content block=\"phone\" label=\"Phone\" oneline=\"true\"}\r\n{content block=\"email\" label=\"E-mail\" oneline=\"true\"}\r\n\r\n{content block=\"meta_title\" label=\"Meta Title\" oneline=\"true\"}\r\n{content block=\"meta_description\" label=\"Meta Description\" wysiwyg=\"false\" size=\"100\"}\r\n{content block=\"meta_keywords\" label=\"Meta Keywords\" oneline=\"true\"}\r\n{content block=\"facebook_title\" label=\"Facebook Title\" oneline=\"true\"}\r\n{content block=\"facebook_description\" label=\"Facebook Description\" wysiwyg=\"false\" size=\"100\"}\r\n{content block=\"facebook_image\" label=\"Facebook Image\" oneline=\"true\"}','{}','','',1,0,'2021-09-09 06:49:12','2021-10-04 13:47:06'),(12,'features','{***********************\r\n    FEATURES\r\n***********************}\r\n\r\n{* setup fields *}\r\n{setup_content\r\nhide=\"contenttype//parent//showinmenu//content_en\"\r\nimages=\"\"\r\nfiles=\"\"\r\nright_col=\"\"}\r\n\r\n{content size=\"80\" oneline=\"true\"}\r\n\r\n{content block=\"meta_title\" label=\"Meta Title\" oneline=\"true\"}\r\n{content block=\"meta_description\" label=\"Meta Description\" wysiwyg=\"false\" size=\"100\"}\r\n{content block=\"meta_keywords\" label=\"Meta Keywords\" oneline=\"true\"}\r\n{content block=\"facebook_title\" label=\"Facebook Title\" oneline=\"true\"}\r\n{content block=\"facebook_description\" label=\"Facebook Description\" wysiwyg=\"false\" size=\"100\"}\r\n{content block=\"facebook_image\" label=\"Facebook Image\" oneline=\"true\"}','{}','','',1,0,'2021-09-16 08:49:24','2021-10-04 14:09:28'),(13,'faq','{***********************\r\n    FAQ\r\n***********************}\r\n\r\n{* setup fields *}\r\n{setup_content\r\nhide=\"contenttype//parent\"\r\nimages=\"\"\r\nfiles=\"\"\r\nright_col=\"\"}\r\n\r\n{content block=\"subtitle\" label=\"Subtitle\" oneline=\"true\"}\r\n{content size=\"93\" wysiwyg=\"false\"}\r\n{content block=\"search_text\" label=\"Search Text\" oneline=\"true\"}\r\n\r\n{content block=\"no_results\" label=\"Text: No results\" oneline=\"true\"}\r\n{content block=\"valid_term\" label=\"Text: Not valid\" oneline=\"true\"}\r\n{content block=\"length\" label=\"Text: Symbol count\" oneline=\"true\"}\r\n\r\n{content block=\"meta_title\" label=\"Meta Title\" oneline=\"true\"}\r\n{content block=\"meta_description\" label=\"Meta Description\" wysiwyg=\"false\" size=\"100\"}\r\n{content block=\"meta_keywords\" label=\"Meta Keywords\" oneline=\"true\"}\r\n{content block=\"facebook_title\" label=\"Facebook Title\" oneline=\"true\"}\r\n{content block=\"facebook_description\" label=\"Facebook Description\" wysiwyg=\"false\" size=\"100\"}\r\n{content block=\"facebook_image\" label=\"Facebook Image\" oneline=\"true\"}','{}','','',1,0,'2021-09-17 13:59:28','2021-10-13 11:03:10'),(14,'pricing','{***********************\r\n    PRICING\r\n***********************}\r\n\r\n{* setup fields *}\r\n{setup_content\r\nhide=\"contenttype//parent//showinmenu\"\r\nimages=\"\"\r\nfiles=\"\"\r\nright_col=\"\"}\r\n\r\n{content size=\"93\" wysiwyg=\"false\"}\r\n\r\n{content block=\"meta_title\" label=\"Meta Title\" oneline=\"true\"}\r\n{content block=\"meta_description\" label=\"Meta Description\" wysiwyg=\"false\" size=\"100\"}\r\n{content block=\"meta_keywords\" label=\"Meta Keywords\" oneline=\"true\"}\r\n{content block=\"facebook_title\" label=\"Facebook Title\" oneline=\"true\"}\r\n{content block=\"facebook_description\" label=\"Facebook Description\" wysiwyg=\"false\" size=\"100\"}\r\n{content block=\"facebook_image\" label=\"Facebook Image\" oneline=\"true\"}','{}','','',1,0,'2021-09-20 09:03:34','2021-10-04 12:18:09'),(15,'subscribe','{***********************\r\n    SUBSCRIBE\r\n***********************}\r\n\r\n{* setup fields *}\r\n{setup_content\r\nhide=\"contenttype//parent//showinmenu//menutext//content_en//alias\"\r\nimages=\"img_desktop//img_mob\"\r\nfiles=\"\"\r\nright_col=\"\"}\r\n\r\n{content oneline=\"true\"}\r\n{content block=\"img_desktop\" label=\"Image Desktop\" oneline=\"true\"}\r\n{content block=\"img_mob\" label=\"Image Mobile\" oneline=\"true\"}','{}','','',1,0,'2021-10-02 09:51:11','2021-10-08 06:44:07'),(16,'footer','{***********************\r\n    FOOTER\r\n***********************}\r\n\r\n{* setup fields *}\r\n{setup_content\r\nhide=\"contenttype//parent//showinmenu//menutext//title//alias\"\r\nimages=\"\"\r\nfiles=\"\"\r\nright_col=\"\"}\r\n\r\n{content size=\"93\" wysiwyg=\"false\"}\r\n{content block=\"copyright\" label=\"Copyright\" oneline=\"true\"}\r\n\r\n{content block=\"menu_title_1\" label=\"Menu Title 1\" oneline=\"true\"}\r\n{content block=\"menu_title_2\" label=\"Menu Title 2\" oneline=\"true\"}','{}','','',1,0,'2021-10-04 06:37:53','2021-10-04 14:11:35'),(17,'header_button','{***********************\r\n    SUBSCRIBE\r\n***********************}\r\n\r\n{* setup fields *}\r\n{setup_content\r\nhide=\"contenttype//parent//showinmenu//alias//content_en//title\"\r\nimages=\"img_desktop//img_mob\"\r\nfiles=\"\"\r\nright_col=\"\"}\r\n\r\n{content oneline=\"true\"}\r\n{content block=\"link\" label=\"Link\" oneline=\"true\"}','{}','','',1,0,'2021-10-08 07:02:24','2021-10-08 07:08:10'),(18,'pricing_table','{***********************\r\n    PRICING TABLE\r\n***********************}\r\n\r\n{* setup fields *}\r\n{setup_content\r\nhide=\"contenttype//parent//showinmenu//menutext//content_en\"\r\nimages=\"\"\r\nfiles=\"\"\r\nright_col=\"\"}\r\n\r\n{content oneline=\"true\"}','{\"group\":{\"edit\":[1],\"active\":[1,2],\"delete\":[1]}}','','',1,0,'2021-10-08 08:26:30','2021-10-08 08:26:42'),(19,'pricing_faq','{***********************\r\n    PRICING FAQ\r\n***********************}\r\n\r\n{* setup fields *}\r\n{setup_content\r\nhide=\"contenttype//parent//showinmenu//menutext//content_en\"\r\nimages=\"\"\r\nfiles=\"\"\r\nright_col=\"\"}\r\n\r\n{content oneline=\"true\"}','{\"group\":{\"edit\":[1],\"active\":[1,2],\"delete\":[1]}}','','',1,0,'2021-10-08 08:29:11','2021-10-08 08:30:04'),(20,'text','{***********************\r\n    TEXT PAGE\r\n***********************}\r\n\r\n{* setup fields *}\r\n{setup_content\r\nhide=\"contenttype//parent//showinmenu\"\r\nimages=\"\"\r\nfiles=\"\"\r\nright_col=\"\"}\r\n\r\n{content class=\"text-editor-full\" size=\"500\"}\r\n\r\n{content block=\"meta_title\" label=\"Meta Title\" oneline=\"true\"}\r\n{content block=\"meta_description\" label=\"Meta Description\" wysiwyg=\"false\" size=\"100\"}\r\n{content block=\"meta_keywords\" label=\"Meta Keywords\" oneline=\"true\"}\r\n{content block=\"facebook_title\" label=\"Facebook Title\" oneline=\"true\"}\r\n{content block=\"facebook_description\" label=\"Facebook Description\" wysiwyg=\"false\" size=\"100\"}\r\n{content block=\"facebook_image\" label=\"Facebook Image\" oneline=\"true\"}','{}','','',1,0,'2021-10-12 06:28:56','2021-10-12 06:29:57'),(21,'sitemap','{***********************\r\n    SITEMAP\r\n***********************}\r\n\r\n{* setup fields *}\r\n{setup_content\r\nhide=\"contenttype//parent//showinmenu//content_en\"\r\nimages=\"\"\r\nfiles=\"\"\r\nright_col=\"\"}\r\n\r\n{content oneline=\"true\"}','{}','','',1,0,'2021-10-12 08:15:33','2021-10-12 08:15:33'),(22,'cookies','{***********************\r\n    COOKIES\r\n***********************}\r\n\r\n{* setup fields *}\r\n{setup_content\r\nhide=\"contenttype//parent//showinmenu\"\r\nimages=\"\"\r\nfiles=\"\"\r\nright_col=\"\"}\r\n\r\n{content size=\"93\" wysiwyg=\"false\"}\r\n{content block=\"cookies_accept\" label=\"Accept Text\" oneline=\"true\"}','{}','','',1,0,'2021-10-12 11:44:39','2021-10-12 11:44:39');
/*!40000 ALTER TABLE `fl_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_user_groups`
--

DROP TABLE IF EXISTS `fl_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_user_groups` (
  `group_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_user_groups`
--

LOCK TABLES `fl_user_groups` WRITE;
/*!40000 ALTER TABLE `fl_user_groups` DISABLE KEYS */;
INSERT INTO `fl_user_groups` VALUES (2,2,NULL,NULL),(1,1,NULL,NULL);
/*!40000 ALTER TABLE `fl_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_userprefs`
--

DROP TABLE IF EXISTS `fl_userprefs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_userprefs` (
  `user_id` int(11) DEFAULT NULL,
  `preference` varchar(50) DEFAULT NULL,
  `value` text,
  `type` varchar(25) DEFAULT NULL,
  KEY `kab_index_userprefs_by_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_userprefs`
--

LOCK TABLES `fl_userprefs` WRITE;
/*!40000 ALTER TABLE `fl_userprefs` DISABLE KEYS */;
INSERT INTO `fl_userprefs` VALUES (1,'use_wysiwyg','1',NULL),(1,'wysiwyg','',NULL),(1,'default_cms_language','en_US',NULL),(1,'date_format_string','%x %X',NULL),(1,'admintheme','NCleanGrey',NULL),(1,'bookmarks','on',NULL),(1,'recent','on',NULL),(1,'indent','on',NULL),(1,'ajax','0',NULL),(1,'paging','0',NULL),(1,'hide_help_links','0',NULL),(1,'collapse','[\"16\",\"17\",\"27\",\"3\",\"23\",\"69\",\"39\",\"37\",\"53\",\"67\",\"40\",\"63\",\"62\",\"59\",\"24\",\"7\",\"25\",\"43\",\"31\",\"6\",\"5\",\"4\",\"1\",\"20\",\"2\",\"26\"]',NULL),(2,'wysiwyg','TinyMCE',NULL),(2,'default_cms_language','lv_LV',NULL),(2,'admintheme','NCleanGrey',NULL),(2,'bookmarks','on',NULL),(2,'recent','on',NULL),(2,'collapse','[\"16\",\"17\",\"63\",\"7\",\"1\"]',NULL),(1,'gcb_wysiwyg','1',NULL),(1,'syntaxhighlighter','',NULL),(1,'enablenotifications','1',NULL),(1,'default_parent','-1',NULL),(1,'homepage','',NULL),(1,'ignoredmodules','',NULL),(1,'liststylesheets_pagelimit','20',NULL),(1,'listgcbs_pagelimit','20',NULL),(1,'changegroupassign_group','-1',NULL);
/*!40000 ALTER TABLE `fl_userprefs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_users`
--

DROP TABLE IF EXISTS `fl_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `password_2fa` varchar(255) DEFAULT NULL,
  `admin_access` tinyint(4) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `user_image` varchar(255) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_users`
--

LOCK TABLES `fl_users` WRITE;
/*!40000 ALTER TABLE `fl_users` DISABLE KEYS */;
INSERT INTO `fl_users` VALUES (1,'admin','$2y$10$2SF37FCOr1tRyGflk1EAx.JgoTsYucgDnpFDaoWz5C.sg6XsIEX2.','',1,'Admin','','','',1,'2006-07-25 21:22:33','2021-11-11 09:07:18'),(2,'user','$2y$10$syl84et0.VAn6kzC7YfGs.Hlj6FvZgzHeQOPq7EVvWCwrj9BAMquu','',1,'User','','user@localhost.lv','',1,'2020-04-25 09:05:18','2021-11-09 11:35:25');
/*!40000 ALTER TABLE `fl_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_users_banned`
--

DROP TABLE IF EXISTS `fl_users_banned`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_users_banned` (
  `ip` varchar(32) NOT NULL,
  `tries` int(11) NOT NULL,
  UNIQUE KEY `ip` (`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_users_banned`
--

LOCK TABLES `fl_users_banned` WRITE;
/*!40000 ALTER TABLE `fl_users_banned` DISABLE KEYS */;
/*!40000 ALTER TABLE `fl_users_banned` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-11 13:04:52
