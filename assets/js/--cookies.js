/* --------------------------------- *\

	Cookies

\* --------------------------------- */

function cookiesAccept(element) {

	// get the parent
	const findParent = function(element, parent_class) {
		while ((element = element.parentElement) && !element.classList.contains(parent_class));
		return element;
	}

	// remove the popup
	const cookiesPopup = findParent(element, 'cookies');
	cookiesPopup.remove();

	// set local storage var
	localStorage.setItem('fastlaw_web_cookies', 'yes');

};

// on dom ready
window.addEventListener('DOMContentLoaded', function(){

	// get the popup
	const cookiesPopup = document.querySelector(".cookies");

	// if seen then remove the popup else show it
	if(localStorage.getItem('fastlaw_web_cookies') == 'yes') {
		cookiesPopup.remove();
	}

});




