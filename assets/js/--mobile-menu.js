/* --------------------------------- *\

	Toggle Mobile Menu
	
\* --------------------------------- */

// on touchmove prevent default
function handleTouchMove(event) {
	event.preventDefault();
};

// toggle menu
function toggleMobileMenu(burger) {

	// get header
	const header = document.querySelector('.header');
	if(header !== null && header !== undefined) {

		// check class
		if(header.classList.contains('header--open')) {
		
			// close menu		
			header.classList.remove('header--open');

			// remove no scroll
			document.body.classList.remove("no-scroll");
			window.removeEventListener(
				'touchmove',
				handleTouchMove
			);

		} else {

			// open menu
			header.classList.add('header--open');

			// no scroll
			document.body.classList.add("no-scroll");
			window.addEventListener(
				'touchmove',
				handleTouchMove,
				{passive: false}
			);

		}

		// burger class
		if(burger.classList.contains('is-active')) {
			burger.classList.remove('is-active');
		} else {
			burger.classList.add('is-active');
		}

	}

};

// close menu
function closeMenu() {

	// get the header
	const header = getElementByClass('.header');
	const burger = getElementByClass('.hamburger');
	const lang_menu = getElementByClass('.lang');

	// remove all open classes
	if(header) header.classList.remove('header--open');
	if(burger) burger.classList.remove('is-active');
	if(lang_menu) lang_menu.classList.remove('lang--open');

	// remove no scroll
	document.body.classList.remove("no-scroll");
	window.removeEventListener(
		'touchmove',
		handleTouchMove
	);

}

// on window resize close menu
window.addEventListener('resize', function(){
	closeMenu();
});





/* --------------------------------- *\

	Add BG to Header on Scroll
	
\* --------------------------------- */

// on window resize close menu
window.addEventListener('scroll', function() {
	
	// get header
	const header = getElementByClass('.header');
	if(header) {

		// check if we on top or not and add/remove bg
		if(window.scrollY > 0) {
			header.classList.add('header--bg');
		} else {
			header.classList.remove('header--bg');
		}

	}

});





/* --------------------------------- *\

	Lang menu for mobiles
	
\* --------------------------------- */

document.addEventListener('DOMContentLoaded', function() {

	// get the element
	const lang_menu = getElementByClass('.lang');
	if(lang_menu) {

		// add click action
		lang_menu.addEventListener("click", function() {

			// check if header is open
			const header_open = getElementByClass('.header--open');
			if(header_open || isMobile()) {
				if(this.classList.contains('lang--open')) {
					this.classList.remove('lang--open');
				} else {
					this.classList.add('lang--open');
				}
			}

		});

	}

});




