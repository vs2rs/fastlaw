/* --------------------------------- *\

	FAQ Menu
	
\* --------------------------------- */

// hide or show questions based on faq category
const toggleQuestions = function(faq_id) {

	// get all the questions
	const questions = document.querySelectorAll('.question');
	questions.forEach((question) => {

		// check the match
		if(question.dataset.parent == faq_id) {
			question.classList.remove('question--hidden');
		} else {
			question.classList.add('question--hidden');
		}

	});

};

// clear active faq menu class
const faqClearActiveClass = function(items = false) {

	// set items
	let faq_menu_items = items;

	// check if items set
	if(!faq_menu_items) {

		// get menu and items
		const faq_menu = getElementByClass('.faq__menu');
		if(faq_menu) {

			// get menu items
			faq_menu_items = faq_menu.querySelectorAll('a');

		}

	}

	// remove active class
	faq_menu_items.forEach((item) => {
		item.classList.remove('faq__menu__active');
	});

};

// on load do the faq logic
document.addEventListener('DOMContentLoaded', function() {

	// get menu and items
	const faq_menu = document.querySelector('.faq__menu');

	// check menu
	if(faq_menu !== null && faq_menu !== undefined) {

		// get menu items
		const faq_menu_items = faq_menu.querySelectorAll('a');

		// get active and get current id
		const active = faq_menu.querySelector('.faq__menu__active');

		// show hide questions
		toggleQuestions(active.dataset.id);

		// create click listener for faq menu items
		faq_menu_items.forEach((faq_menu_item) => {

			faq_menu_item.addEventListener("click", function(event){
				
				// clear search
				const search_input = getElementByClass('.search__input');
				if(search_input) search_input.value = '';

				// clear search ifno
				const search_info = getElementByClass('.faq__search');
				if(search_info) {
					search_info.innerHTML = '';
					search_info.classList.remove('faq__search--no-results');
				}

				// prevent default action
				event.preventDefault();

				// toggle questuins
				toggleQuestions(this.dataset.id);

				// clear active
				faqClearActiveClass(faq_menu_items);

				// add to current active class
				this.classList.add('faq__menu__active');

				// change url
				window.history.pushState({}, '', this.href);

			});

		});

	}

});





/* --------------------------------- *\

	FAQ Search
	
\* --------------------------------- */

// reset on refresh input value
document.addEventListener('DOMContentLoaded', function() {
	const search_input = getElementByClass('.search__input');
	if(search_input) search_input.value = '';
});

// on keypres function
const faqSearch = function(input) {

	// first we need to close all the open questions
	const questions_open = document.querySelectorAll('.slide-toggle-parent--open');
	questions_open.forEach((q) => {
		const title = q.querySelector('.question__title > div');
		toggleSlide.call(title);
	});

	// then remove active from the menu
	faqClearActiveClass();

	// reset url
	if(input.dataset.url !== undefined && input.dataset.url !== null) {
		window.history.pushState({}, '', input.dataset.url);
	}

	// set the filter
	const search = input.value;

	// get info row to show search term
	const search_info = getElementByClass('.faq__search');
	if(search_info) {

		// set the serach term info
		search_info.innerHTML = `${search_info.dataset.label}: ${search}`;

		// default to no results
		search_info.classList.add('faq__search--no-results');

	}

	// set the valid search
	let search_valid = true;

	// let's check if we have text
	if(!search.match(/\w/g)) {
		search_valid = false;
		search_info.innerHTML = `${search_info.dataset.label}: ${search_info.dataset.valid}`;
	}

	// check length
	if(search_valid && search.length <= 2) {
		search_valid = false;
		search_info.innerHTML = `${search_info.dataset.label}: ${search_info.dataset.length}`;
	}

	// get questions
	const questions = document.querySelectorAll('.question');

	// loop through
	questions.forEach((q) => {

		// get the title
		const title = q.querySelector('.question__title > div').textContent;
		const text = q.querySelector('.question__answer').textContent;

		if(search_valid) {

			// create regexpresion
			const serach_pattern = new RegExp(search, 'gi');
			
			// look for search string
			if(title.match(serach_pattern) || text.match(serach_pattern)) {
					
				// show this question
				q.classList.remove('question--hidden');
				
				// remove no results
				search_info.classList.remove('faq__search--no-results');

			} else {

				// hide the question
				q.classList.add('question--hidden');

			}

		} else {

			// hide question
			q.classList.add('question--hidden');

		}

	});

	// reset the search
	if(search.length == 0) {

		// clear search ifno
		if(search_info) {
			search_info.innerHTML = '';
			search_info.classList.remove('faq__search--no-results');
		}
		
		// get menu and items
		const faq_menu = getElementByClass('.faq__menu');
		if(faq_menu) {

			// get active and get current id
			let active = faq_menu.querySelector('.faq__menu__active');

			// if active not set get the first menu item
			if(active === undefined || active == null) {
				active = faq_menu.querySelector('.faq__menu > a');
				active.classList.add('faq__menu__active');
			}

			// show hide questions
			toggleQuestions(active.dataset.id);

		}

	}

};




