/* --------------------------------- *\

	Swithc
	
\* --------------------------------- */

// document.addEventListener('DOMContentLoaded', function() {
// });

function setSwitchActive(element) {

	// if this is the active on cancel everytihing
	if(element.classList.contains('switch__active')) return;

	// get current active
	const current = element.parentNode.querySelector('.switch__active');
	current.classList.remove('switch__active');

	// add active class to the element
	element.classList.add('switch__active');

};

function changePrices(prices_for) {

	const prices_div = document.querySelectorAll('.pricing__price');

	prices_div.forEach((price_div) => {

		let price = 0;

		if(prices_for == 'm') price = price_div.dataset.priceM;
		if(prices_for == 'y') price = price_div.dataset.priceY;

		price = price.split(".");

		const price_euro = price_div.querySelector('span:nth-child(2)');
		const price_cents = price_div.querySelector('span:nth-child(3)');

		price_euro.innerHTML = price[0];
		price_cents.innerHTML = '.' + price[1];

	});

};