<?php

/* --------------------------------- *\
 
	APP SETUP

\* --------------------------------- */

// one dir up is our project folder
define("APP_PATH", dirname(dirname(__FILE__)));

// check if document root is set
if(isset($_SERVER['DOCUMENT_ROOT'])) {
	$document_root = $_SERVER['DOCUMENT_ROOT'];
} else {
	$document_root = false; // needs to be set manualy
	if(!$document_root) die('Document root directory not found.');
}

// get only the app folder with out the full path
define("APP_FOLDER", str_replace($document_root, "", APP_PATH));

/* --------------------------------- *\
 
	VCMS SETUP

\* --------------------------------- */

// vcms folder
define("VCMS_FOLDER", '/vcms');

// paths to check for vcms folder
$vcms_path_list = array(
	APP_PATH . VCMS_FOLDER, // 1 -- current app dir
	dirname($document_root) . VCMS_FOLDER // 2 -- out of public
);

// go through paths and check them 
foreach ($vcms_path_list as $vcms_path) {
	if(is_dir($vcms_path)) break;
}

// define the path and set the setup file
define("VCMS_PATH", $vcms_path);

/**
 * Load setup files called from index.php
 * @param string $file_name - file name to load
 */
function load_vcms_setup_file($file_name) {
	
	// set the path
	$vcms_setup_file = VCMS_PATH . '/' . $file_name;

	// check if it exists;
	if(file_exists($vcms_setup_file)) require_once($vcms_setup_file);
	else die("File $file_name not found.");

}





// -- setup.php --