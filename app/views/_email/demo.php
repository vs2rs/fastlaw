<?php

// template
$email = file_get_contents('email.html');

// logo
$row = str_replace(
	'{{url}}',
	'https://localhost/makit/fastlaw',
	file_get_contents('email-logo.html')
);

$row .= str_replace(
	array('{{label}}', '{{value}}'),
	array('Iesniegums', 'Atsauksmes'),
	file_get_contents('email-form-row.html')
);

$row .= str_replace(
	array('{{label}}', '{{value}}'),
	array('Kontaktpersona', 'Name Surname<br/>+371 123123</br>email@address.com'),
	file_get_contents('email-form-row.html')
);

$row .= str_replace(
	array('{{label}}', '{{value}}'),
	array('Ziņojums', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel cursus ipsum. Aenean sit amet interdum est, et vestibulum mauris. Praesent est ante, feugiat eget sollicitudin eu, tempus non eros. Vestibulum mollis nisl a nisl placerat, id mollis sapien mollis. Phasellus faucibus hendrerit laoreet.'),
	file_get_contents('email-form-row.html')
);

// output
echo str_replace('{{content}}', $row, $email);