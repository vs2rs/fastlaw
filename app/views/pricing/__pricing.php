<?php

/* --------------------------------- *\
 
	Setup

\* --------------------------------- */

// current page
$page = FrontendContent::get_current_page();

// get the pages
$pages = FrontendContent::get_pages(Config::read('lang_current'));

/* --------------------------------- *\
 
	Pricing

\* --------------------------------- */

// features top
$features_top = FastlawData::module_pricing_features_items(1);

// setup plans
$price_plan1 = FastlawTemplates::price_plan(1, $features_top);
$price_plan2 = FastlawTemplates::price_plan(2, $features_top);
$price_plan3 = FastlawTemplates::price_plan(3, $features_top);

// check if atlest one plan is set
$pricing_plans = $price_plan1 != '' || $price_plan2 != '' || $price_plan3 != '';

// output plans
if($pricing_plans) {

	// output
	echo get_html(array(

		// page title
		'title'				=> line_breaks($page->name),
		'subtitle'			=> line_breaks($page->content),

		// plans
		'pricing_plan1'		=> $price_plan1,
		'pricing_plan2'		=> $price_plan2,
		'pricing_plan3'		=> $price_plan3,

		'monthly'			=> Config::read('lang_current') == 'lv' ? 'Mēnesī' : 'Monthly',
		'yearly'			=> Config::read('lang_current') == 'lv' ? 'Gadā' : 'Yearly',

		// template
		'template' 			=> APP_VIEWS . '/pricing/pricing.html'

	));

} else {

	// output
	echo get_html(array(

		// page title
		'title'				=> line_breaks($page->name),
		'subtitle'			=> line_breaks($page->content),

		// template
		'template' 			=> APP_VIEWS . '/pricing/pricing-no-plans.html'

	));

}

/* --------------------------------- *\
 
	Load Pricing Sections

\* --------------------------------- */

if(isset($pages->pricing->children)) {
	foreach ($pages->pricing->children as $section) {
		if(file_exists(APP_VIEWS . '/pricing/_' . $section->accesskey . '.php')) {
			include_once('_' . $section->accesskey . '.php');
		}
	}
}





/* --------------------------------- *\
 
	Pricing FAQ

\* --------------------------------- */

echo FastlawTemplates::subscribe();





// -- __pricing.php