<?php

/* --------------------------------- *\
 
	Sitemap

\* --------------------------------- */

// get the page
$page_current = FrontendContent::get_current_page();
$pages = FrontendContent::get_pages();

// current language
$lang = Config::read('lang_current');

// sitempa
$sitemap_html = array();
$sitemap_html[$lang] = '';
$sitemap_html['other'] = '';

// setup
foreach($pages as $language) {

	// html
	$lang_html  = '<div>';
	$lang_html .= '<h2>';
	$lang_html .= $language->name;
	$lang_html .= '</h2>';

	if(isset($language->children)) {
		$lang_html .= '<ul class="text-large">';
		foreach($language->children as $page) {
			if($page->type != 'block') {
				$page_url = FrontendContent::get_lang_url($language->accesskey, $page);
				$lang_html .= '<li><a href="' . $page_url . '">' . $page->menu_text . '</a>';
				

				if($page->accesskey == 'features'
				&& $items = FastlawData::module_features_items()) {
					$lang_html .= '<ul>';
					foreach($items as $item) {
						$lang_html .= '<li><a href="' . $page_url . '/' . $item->alias. '">' . $item->name . '</a>';
					}
					$lang_html .= '</ul>';
				}

				$lang_html .= '</li>';


			}
		}
		$lang_html .= '</ul>';
	}

	// close div
	$lang_html .= '</div>';

	// add to the main html var
	if($language->accesskey == $lang) {
		$sitemap_html[$lang] = $lang_html;
	} else {
		$sitemap_html['other'] = $lang_html;
	}

}

// html
echo get_html(array(

	// content
	'title'		=> line_breaks($page_current->name),
	'sitemap'	=> $sitemap_html[$lang] . $sitemap_html['other'],

	// template
	'template'	=> APP_VIEWS . '/sitemap/sitemap.html'

));





// -- __sitemap.php