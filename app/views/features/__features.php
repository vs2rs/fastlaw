<?php

/* --------------------------------- *\
 
	Features Setup

\* --------------------------------- */

// get the current page
$page = FrontendContent::get_current_page();

// get our module item id
if(isset($page->module_item)) {
	$item_id = $page->module_item->id;
} else {
	$item_id = FastlawData::module_features_default_item();
}

// get the item
$item = FastlawData::module_features_items($item_id);

// setup image
$item->img = get_image($item->img);





/* --------------------------------- *\
 
	Features Output

\* --------------------------------- */

echo get_html(array(

	'feature_images'	=> FastlawTemplates::features_images_list($item->features_images_id),
	'feature_icons'		=> FastlawTemplates::features_icons_list($item->features_icons_id),
	'feature_faq'		=> FastlawTemplates::faq_inner_pages($item->faq_id),
	'feature_subscribe'	=> FastlawTemplates::subscribe(),

	// intro title and image
	'title' 			=> line_breaks($item->title),
	'subtitle' 			=> line_breaks($item->info),
	'image'				=> $item->img ? $item->img->src : false,
	'image_alt'			=> $item->img ? $item->img->alt : '',

	// template
	'template' 			=> APP_VIEWS . '/features/features.html'

));





// -- __features.php