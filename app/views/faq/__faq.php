<?php

/* --------------------------------- *\
 
	FAQ

\* --------------------------------- */

// get current page
$page = FrontendContent::get_current_page();
$lang = Config::read('lang_current');

// set active section var
$faq_section = false;

// check for module
if(isset($page->module_item)) {

	// set alias param
	$alias = 'alias';
	if($lang != Config::read('lang_default')) {
		$alias = $alias . '_' . $lang;
	}

	// get alias
	$faq_section = $page->module_item->$alias;

}

// output template
echo FastlawTemplates::faq($faq_section);
echo FastlawTemplates::subscribe();





// -- __features.php