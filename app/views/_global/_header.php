<?php

/* --------------------------------- *\
 
	Get pages

\* --------------------------------- */

// get the pages
$pages_all = FrontendContent::get_pages();
$page = FrontendContent::get_current_page();

// curent language
$lang_current = Config::read('lang_current');





/* --------------------------------- *\
 
	Lang menu

\* --------------------------------- */

// get the lang
$languages = Config::read('languages');
$lang_html = '';

// check if set
if($languages && is_array($languages)) {

	// collect the pages in a array
	$lang_menu = array();

	// go thorugh languages
	foreach($languages as $lang) {
		if(isset($pages_all->$lang->children)) {
			$lang_menu[] = $pages_all->$lang;
		}
	}

	// check how many languages are active
	if(count($lang_menu) > 1) {

		// set item html
		$lang_items_html = '';

		// setup dropdown items
		foreach($lang_menu as $lang) {

			if($lang->accesskey == $lang_current) {
				$lang_items_html .= '<span>' . $lang->name . '</span>';
			} else {

				// setup url
				$lang_url = FrontendContent::get_lang_url($lang->accesskey);

				// get the accesskey of current page
				$page_current = $page->accesskey;

				// check if this language has this page
				if(isset($lang->children->$page_current)) {

					// update url
					$lang_url = FrontendContent::get_lang_url(
						$lang->accesskey,
						$lang->children->$page_current
					);

					// check for module
					if(isset($page->module_item)) {

						// check if item is set in this language
						$item_alias = FastlawData::get_lang_alias(
							$lang->accesskey,
							$page->module_item
						);

						// add to url if true
						if($item_alias) {
							$lang_url .= '/' . $item_alias;
						}

					}

				}

				// link to the language
				$lang_items_html .= '<a href="' . $lang_url . '">' . $lang->name . '</a>';

			}

		}

		// get the html template
		$lang_html .= get_html(array(

			// params
			'name'		=> $pages_all->$lang_current->menu_text,
			'items'		=> $lang_items_html,

			// template
			'template'	=> APP_VIEWS . '/_global/lang.html'

		));

	}

}





/* --------------------------------- *\
 
	Menu

\* --------------------------------- */

// get this language pages
$pages = $pages_all->$lang_current->children;

// html list of our menu items
$menu_html = '';

foreach ($pages as $key => $menu_item) {

	// check if we show in the menu
	if($menu_item->show_in_menu
	&& $menu_item->type != 'block') {

		// active var
		$active = '';

		// check if active page
		if($page->accesskey == $menu_item->accesskey) {
			$active = ' class="menu__active"';
		}

		// create the url
		$menu_url = FrontendContent::get_lang_url(
			$lang_current, $menu_item
		);

		// get the html template
		$menu_html .= get_html(array(

			// menu
			'name'		=> $menu_item->menu_text,
			'link'		=> $menu_url,
			'active'	=> $active,

			// template
			'template'	=> APP_VIEWS . '/_global/nav-item.html'

		));

	}

}





/* --------------------------------- *\
 
	Submenu

\* --------------------------------- */

// submenu html
$submenu_html = '';

if($page->accesskey == 'features') {

	// get our item id from object or get first items id as deafult
	if(isset($page->module_item)) {
		$feature_active_id = $page->module_item->id;
	} else {
		$feature_active_id = FastlawData::module_features_default_item();
	}

	// get features and set html param
	$features = FastlawData::module_features_items();

	// setup submenu
	foreach($features as $feature) {

		// active var
		$active = '';
		$active_svg = '';

		// check if active page
		if($feature->id == $feature_active_id) {
			$active = ' class="submenu__active"';
			$active_svg = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 123.959 123.958" xml:space="preserve"> <path d="M117.979,28.017h-112c-5.3,0-8,6.4-4.2,10.2l56,56c2.3,2.3,6.1,2.3,8.401,0l56-56 C125.979,34.417,123.279,28.017,117.979,28.017z"/></svg>';
		}

		// create the url
		$feature_url = FrontendContent::get_lang_url(
			$lang_current, $page
		);

		// get the html template
		$submenu_html .= get_html(array(

			// menu
			'name'			=> $feature->name,
			'link'			=> $feature_url . '/' . $feature->alias,
			'active'		=> $active,
			'active_svg'	=> $active_svg,

			// template
			'template'		=> APP_VIEWS . '/_global/submenu-item.html'

		));
		
	}

}





/* --------------------------------- *\
 
	Header output

\* --------------------------------- */

// pages with the intro block
$intro = array('home', 'features');

echo get_html(array(

	// header link and cta
	'header_link'		=> isset($pages->header_link) ? $pages->header_link->link : false,
	'header_link_text'	=> isset($pages->header_link) ? $pages->header_link->menu_text : '',
	'header_cta'		=> isset($pages->header_cta) ? $pages->header_cta->link : false,
	'header_cta_text'	=> isset($pages->header_cta) ? $pages->header_cta->menu_text : '',

	// header class
	'class'				=> !in_array($page->accesskey, $intro) ? '  header--black' : '',
	'wrap_full'			=> $page->accesskey == 'features',
	'submenu'			=> $submenu_html != '' ? $submenu_html : false,

	// main menu
	'menu'				=> $menu_html,

	// lang menu
	'lang'				=> $lang_html,

	// template
	'template'			=> APP_VIEWS . '/_global/header.html'

));





// -- _header.php