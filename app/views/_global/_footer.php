<?php

/* --------------------------------- *\
 
	Footer

\* --------------------------------- */

// language
$lang = Config::read('lang_current');
$lang_default = Config::read('lang_default');

// get the pages
$page = FrontendContent::get_current_page();

// get all pages
$pages = FrontendContent::get_pages(Config::read('lang_current'));

// check for footer
if(isset($pages->footer)) {
	$footer = $pages->footer;
}

// features
if(isset($pages->features)) {
	$features = $pages->features;
	$features_link = FrontendContent::get_lang_url(false, $features);
}

// pricing
if(isset($pages->pricing)) {
	$pricing = $pages->pricing;
	$pricing_link = FrontendContent::get_lang_url(false, $pricing);
}

// faq
if(isset($pages->faq)) {
	$faq = $pages->faq;
	$faq_link = FrontendContent::get_lang_url(false, $faq);
}

// contacts
if(isset($pages->contacts)) {
	$contacts = $pages->contacts;
	$contacts_link = FrontendContent::get_lang_url(false, $contacts);
}

// terms
if(isset($pages->terms)) {
	$terms = $pages->terms;
	$terms_link = FrontendContent::get_lang_url(false, $terms);
}

// privacy
if(isset($pages->privacy)) {
	$privacy = $pages->privacy;
	$privacy_link = FrontendContent::get_lang_url(false, $privacy);
}

// sitemap
if(isset($pages->sitemap)) {
	$sitemap = $pages->sitemap;
	$sitemap_link = FrontendContent::get_lang_url(false, $sitemap);
}

// output
echo get_html(array(

	// content
	'text'			=> isset($footer) ? line_breaks($footer->content) : '',
	'title1'		=> isset($footer) ? line_breaks($footer->menu_title_1) : '',
	'title2'		=> isset($footer) ? line_breaks($footer->menu_title_2) : '',

	// footer menu
	'features'		=> isset($features) ? $features->menu_text : false,
	'features_link'	=> isset($features) ? $features_link : '',
	'pricing'		=> isset($pricing) ? $pricing->menu_text : false,
	'pricing_link'	=> isset($pricing) ? $pricing_link : '',
	'faq'			=> isset($faq) ? $faq->menu_text : false,
	'faq_link'		=> isset($faq) ? $faq->menu_text : '',
	'contacts'		=> isset($contacts) ? $contacts->menu_text : false,
	'contacts_link'	=> isset($contacts) ? $contacts->menu_text : '',
	'terms'			=> isset($terms) ? $terms->menu_text : false,
	'terms_link'	=> isset($terms) ? $terms_link : '',
	'privacy'		=> isset($privacy) ? $privacy->menu_text : false,
	'privacy_link'	=> isset($privacy) ? $privacy_link : '',
	'sitemap'		=> isset($sitemap) ? $sitemap->menu_text : false,
	'sitemap_link'	=> isset($sitemap) ? $sitemap_link : '',

	// copyright
	'copyright'		=> isset($footer) ? line_breaks($footer->copyright) : '',
	'year'			=> date('Y'),

	// social icons
	'social'		=> FastlawTemplates::socialnetworks(),

	// need to close wraper
	'wrap_full'		=> $page->accesskey == 'features',

	// template
	'template'		=> APP_VIEWS . '/_global/footer.html'

));





// -- _footer.php