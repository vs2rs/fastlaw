<?php

/* --------------------------------- *\
 
	Get css/js

\* --------------------------------- */

// get css
$css = get_header_element('css', array(
	'slidetoggle/slidetoggle.css'
));

// get javascripts
$js = get_header_element('js', array(
	'helpers/helpers.js',
	'ismobile/ismobile.js',
	'slidetoggle/slidetoggle.js'
));

/* --------------------------------- *\
 
	Meta

\* --------------------------------- */

$pages = FrontendContent::get_pages();
$page  = FrontendContent::get_current_page();
$lang  = Config::read('lang_current');

// some defaults
Config::write('site_name', $pages->$lang->site_name);
Config::write('meta_author', $pages->$lang->author);

// default
$meta = get_meta($page);
$page_title = get_meta_title($page);

// check if module item
if($page->accesskey == 'features'
&& isset($page->module_item)) {

	// get the item
	if($item = FastlawData::module_features_items($page->module_item->id)) {

		// create meta tags
		$item->meta = FrontendContent::set_meta_default($item);
		$item->facebook = FrontendContent::set_meta_facebook($item);

		// use default meta for faq page
		if($page->accesskey == "faq") {
			$meta = get_meta($page);
		} else {
			$meta = get_meta($item);
		}

		// set page title
		$page_title = get_meta_title($item);

	} else {

		$meta = get_meta($page);

	}

}

/* --------------------------------- *\
 
	Head output

\* --------------------------------- */

echo get_html(array(

	// title
	'title'		=> $page_title,

	// meta
	'base'		=> APP_URL,
	'meta'		=> $meta,

	// css and js
	'css'		=> $css,
	'js'		=> $js,

	// template
	'template'	=> APP_VIEWS . '/_global/head.html'

));





// -- _head.php