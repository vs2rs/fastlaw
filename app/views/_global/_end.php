<?php

/* --------------------------------- *\
 
	End

\* --------------------------------- */

// get the pages
$pages = FrontendContent::get_pages(Config::read('lang_current'));
$cookies = isset($pages->cookies) ? $pages->cookies : false;
$cookies_popup = '';

// privacy
if(isset($pages->privacy)) {
	$privacy = $pages->privacy;
	$privacy_link = FrontendContent::get_lang_url(false, $privacy);
}

// check for cookies
if($cookies) {

	// html
	$cookies_popup = get_html(array(

		// text
		'cookies_text'		=> $cookies ? $cookies->content : false,
		'cookies_accept'	=> $cookies ? $cookies->cookies_accept : '',

		'privacy_text'		=> $privacy ? $privacy->name : false,
		'privacy_link'		=> $privacy ? $privacy_link : '',

		// template
		'template'		=> APP_VIEWS . '/_global/cookies-popup.html'

	));

}

// output
echo get_html(array(

	// cookies
	'cookies'		=> $cookies_popup,

	// template
	'template'		=> APP_VIEWS . '/_global/end.html'

));





// -- _end.php