<?php

/* --------------------------------- *\
 
	Home Intro

\* --------------------------------- */

// get the image
$section->img = get_image($section->img);
$section->bg_img = get_image($section->bg_img);

echo get_html(array(

	// text
	'title'		=> line_breaks($section->name),
	'subtitle'	=> line_breaks($section->content),
	'cta_text'	=> $section->cta_text != '' && $section->cta_link ? $section->cta_text : false,
	'cta_link'	=> $section->cta_text != '' && $section->cta_link ? $section->cta_link : '',

	// image
	'image'		=> $section->img ? $section->img->src : false,
	'image_alt'	=> $section->img ? $section->img->alt : false,
	'bg_image'	=> $section->bg_img ? $section->bg_img->src : '',

	// template
	'template'	=> APP_VIEWS . '/home/home-intro.html'

));





// -- _home_intro.php