<?php

/* --------------------------------- *\
 
	Home Pricing

\* --------------------------------- */

// get the current page
$page = FrontendContent::get_current_page();
$page = isset($page->children->home_pricing) ? $page->children->home_pricing : false;

// features top
$features_top = FastlawData::module_pricing_features_items(1);

// setup plans
$price_plan1 = FastlawTemplates::price_plan(1, $features_top);
$price_plan2 = FastlawTemplates::price_plan(2, $features_top);
$price_plan3 = FastlawTemplates::price_plan(3, $features_top);

// check if atlest one plan is set
$pricing_plans = $price_plan1 != '' || $price_plan2 != '' || $price_plan3 != '';

// output plans
if($pricing_plans) {

	echo get_html(array(

		// title of the block
		'title'		=> $page ? line_breaks($page->name) : false,
		'text'		=> $page && $page->content != '' ? $page->content : false,
		'footer'	=> $page && $page->footer != '' ? $page->footer : false,

		// plans
		'pricing_plan1'		=> FastlawTemplates::price_plan(1, $features_top),
		'pricing_plan2'		=> FastlawTemplates::price_plan(2, $features_top),
		'pricing_plan3'		=> FastlawTemplates::price_plan(3, $features_top),

		// template
		'template' => APP_VIEWS . '/home/home-pricing.html'

	));

}





// -- _home_pricing.php