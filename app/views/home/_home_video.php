<?php

/* --------------------------------- *\
 
	Home Video

\* --------------------------------- */

// check for youtube
$video = parse_youtube_url($section->video);

// create link if youtube
if($video) {

	// set the url
	$video = 'https://www.youtube.com/watch?v=' . $video;

} else {

	// check for vimeo
	$video = parse_vimeo_url($section->video);
	if($video) $video = 'https://vimeo.com/' . $video;

}

// get the image
$section->img_video = get_image($section->img_video);

// html
echo get_html(array(

	// text
	'title'			=> line_breaks($section->name),
	'subtitle'		=> line_breaks($section->content),
	'cta_text'		=> $section->cta_text,
	'video_link'	=> $video ? $video : false,
	'video_bg'		=> $section->img_video
					 ? " style=\"background-image: url('" . $section->img_video->src . "');\""
					 : '',

	// template
	'template' 		=> APP_VIEWS . '/home/home-video.html'

));





// -- _home_video.php