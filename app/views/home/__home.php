<?php

// set the language and home accesskey
$lang = Config::read('lang_current');
$home_accesskey = 'home';

// get the pages
$pages = FrontendContent::get_pages();
$pages = array_values((array)$pages->$lang->children->$home_accesskey->children);
$pages_last_key = count($pages) - 1;

/* --------------------------------- *\
 
	Load Home Section

\* --------------------------------- */

foreach ($pages as $key => $section) {

	$section_next = false;
	$section_no = $key + 1;
	if($key != $pages_last_key) {
		$section_next = $pages[$key + 1];
	}

	// include template
	if(file_exists(APP_VIEWS . '/home/_' . $section->accesskey . '.php')) {
		include_once('_' . $section->accesskey . '.php');
	}

}

// subscribe block
echo FastlawTemplates::subscribe();





// -- __home.php