<?php

/* --------------------------------- *\
 
	Setup the Form

\* --------------------------------- */

// set current page
$page = FrontendContent::get_current_page();

// get the inputs list
$inputs = FastlawForm::inputs('contacts');

// set up some params
$errors = array();
$valid_post_data = array();
$input_post_data = array();
$form_html = array();
$form_radio_html = array();

foreach ($inputs as $input) {

	// set the input type
	$input_type = $input['input_type'];
	$input_name = $input['name'];
	
	// get the input template and stuff
	$input_data = FastlawForm::$input_type($input);

	// check for error
	if($input_data['error']) {
		$errors[$input_name] = $input_data['error'];
	} else {
		$valid_post_data[] = $input['name'];
	}

	// set the update version of input data with values
	// this is used later to create email template
	$input_post_data[] = $input_data['params'];

	if($input_type == 'input_radio') {

		// create the var
		if(!isset($form_radio_html[$input_name])) {
			$form_radio_html[$input_name] = '';
		}

		// add to html
		$form_radio_html[$input_name] .= $input_data['html'];

	} else {

		// add to html
		$form_html[$input_name] = $input_data['html'];

	}

}

/* --------------------------------- *\
 
	Contacts

\* --------------------------------- */

if(isset($_GET['send']) && $_GET['send'] == 'success') {

	echo get_html(array(

		// template
		'template'	=> APP_VIEWS . '/contacts/contacts-success.html'

	));

} else if(isset($_GET['send']) && $_GET['send'] == 'error') {

	echo get_html(array(

		// template
		'template'	=> APP_VIEWS . '/contacts/contacts-error.html'

	));

} else {

	echo get_html(array(

		'title'			=> $page->name,
		'subtitle'		=> $page->content,

		'c_address'		=> isset($page->address) && $page->address != '' ? $page->address : false,
		'c_phone'		=> isset($page->phone) && $page->phone != '' ? $page->phone : false,
		'c_email'		=> isset($page->email) && $page->email != '' ? $page->email : false,

		// form fields
		'name'			=> $form_html['fastlaw_name'],
		'email'			=> $form_html['fastlaw_email'],
		'phone'			=> $form_html['fastlaw_phone'],
		'company'		=> $form_html['fastlaw_company'],
		'position'		=> $form_html['fastlaw_position'],
		'text'			=> $form_html['fastlaw_text'],
		'company_size'	=> FastlawForm::lang(Config::read('lang_current'), 'company_size'),
		'submit_button'	=> FastlawForm::lang(Config::read('lang_current'), 'submit_button'),

		'size'			=> $form_radio_html['fastlaw_enterprise_size'],

		// template
		'template'		=> APP_VIEWS . '/contacts/contacts.html'

	));

}





// -- __contacts.php