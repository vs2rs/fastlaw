<?php

/* --------------------------------- *\
 
	Terms of Services

\* --------------------------------- */

// get the page
$page = FrontendContent::get_current_page();

// html
echo get_html(array(

	// content
	'title'		=> line_breaks($page->name),
	'text'		=> $page->content,

	// template
	'template'	=> APP_VIEWS . '/text/text.html'

));





// -- __terms.php