<?php

// setup file
require_once('setup.php');

/* --------------------------------- *\
 
	Setup Hostname

\* --------------------------------- */

load_vcms_setup_file("vcms_setup_host.php");

// we can do somthing with APP_HOSTNAME before
// we resume the rest of the setup

/* --------------------------------- *\
 
	Rest of the setup..

\* --------------------------------- */

load_vcms_setup_file("vcms_setup.php");

// define our app server 
define("APP_SERVER", Config::read("dev") ? 'dev' : 'production');





// -- setup_project.php